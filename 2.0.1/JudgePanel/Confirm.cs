﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Ghost
{
    public partial class Confirm : Form
    {
        static public  bool confirmOk = false;
        static public  bool cancel = true;

        public Confirm()
        {
            InitializeComponent();      
        }

        private void Confirm_Load(object sender, EventArgs e)
        {
            try
            {
                lv.Items.Clear();
                com.Text = "";
                error.Visible = false;
                XElement xmlElement = XElement.Parse(FormJudge.elementsXML);
                foreach (XElement xe in xmlElement.Elements())
                {
                    lv.Items.Add(xe.Attribute("NUM").Value.Trim());
                }
                int i = 0;
                foreach (XElement xe in xmlElement.Elements())
                {
                    lv.Items[i].SubItems.Add(xe.Attribute("NAME").Value);
                    lv.Items[i].SubItems.Add(xe.Attribute("QOE").Value);
                    if (xe.Attribute("QOE").Value.Equals(""))
                        lv.Items[i].ForeColor = Color.Red;
                    else lv.Items[i].ForeColor = Color.Lime;
                    i++;
                }
                xmlElement = XElement.Parse(FormJudge.compXML);
                
                com.Text += xmlElement.Attribute("comp1").Value + "\r\n\r\n" + 
                    xmlElement.Attribute("comp2").Value + "\r\n\r\n" + 
                    xmlElement.Attribute("comp3").Value + "\r\n\r\n" + 
                    xmlElement.Attribute("comp4").Value + "\r\n\r\n" + 
                    xmlElement.Attribute("comp5").Value;

                if (xmlElement.Attribute("comp1").Value.Equals("") || xmlElement.Attribute("comp2").Value.Equals("")
                || xmlElement.Attribute("comp3").Value.Equals("") || xmlElement.Attribute("comp4").Value.Equals("") || FormJudge.notready)
               // || xmlElement.Attribute("comp5").Value.Equals("") 
                {
                    com.ForeColor = Color.Red;
                    error.Visible = true;
                    confirmOk = false;
                    confirmBtn.Enabled = false;
                }
                else 
                {
                    confirmOk = true;
                    com.ForeColor = Color.Lime;
                    confirmBtn.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            cancel = false;
            Close();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            cancel = true;
            Close();
        }

        private void Confirm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                confirmBtn.PerformClick();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void lv_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}

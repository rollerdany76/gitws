﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RollartSystemJudge
{
    public partial class ipForm : Form
    {
        public ipForm()
        {
            InitializeComponent();
        }

        private void ipForm_Load(object sender, EventArgs e)
        {
            try
            {
                label2.Visible = false;
                //maskedTextBox1.ValidatingType = typeof(System.Net.IPAddress);
                ipT.Text = Properties.Settings.Default.TechIP;
                judge.Value = Properties.Settings.Default.CurrentJudge;
            }
            catch (Exception)
            {
                label2.Visible = true;
            }
        }

        private void quit_Click(object sender, EventArgs e)
        {
            try
            {
                label2.Visible = false;
                Properties.Settings.Default.TechIP = ipT.Text;
                Properties.Settings.Default.CurrentJudge = int.Parse(judge.Value.ToString());
                Properties.Settings.Default.Save();
                Dispose();
            }
            catch (Exception)
            {
                label2.Visible = true;
                Dispose();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}

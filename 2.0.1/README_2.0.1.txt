﻿"RollArt" is a registered trademark

Version 2.0.1
Release date: 01/04/2019

Contents of this README:
1.  Changes since version 2.0.0.10
2.  Known Issues at Release

----------------------------------------------------------------------------------------------------------------------------------------------

1. Changes since version 2.0.0.10: 

Bug fixes for:

 * TechPanel: 
    EVENT MANAGEMENT
	- New event: during a new event insertion, the category ID was valued by combobox index instead of the database value
	- New event: the xml file created at the end of a new event insertion process was not well formatted, for Dance and Solo Dance events.
	  This issue didn't allow a correct event import 

Changes and Improvements:

	FREE SKATING/PAIRS/DANCE/PRECISION
	- Falls and deductions buttons remain enabled when the DO press STOP 

	EVENT MANAGEMENT
	- New Button to open a new database file
	- New button to delete all the events
	- Skating Order Window: for all segments not completed you can export to pdf or send to the connected printer the 
      "Skating Order" report and the new "Referee Sheet" report
	- Skater info: you can insert for each skater information about music, coach or coreographer, or program title (for precision skating)
	
	SCORE DISPLAY
	- New mode to display score through VideoScreen external software
	- New management of "DisplayScore" mode in case of VideoScreen not connected. Rollart, if displayswitch option enabled, 
	  doesn't clone the display anymore but sends the score to a second window. The "extended desktop" mode should be set 
	  before the Rollart execution. 

	REPORTS
	- New naming convention used: replaced the 'square' character with 'underscore' 
	- "Export all" button allows to export to .pdf and .doc format
	- New report "Referee Sheet"
	
 * Database
	- Table "Participants": new fields InfoField1 (TEXT), InfoField2 (TEXT), InfoField3 (TEXT)
	  The db is automatically updated by Rollart, through an ALTER TABLE of "Participants" 
	
 * Resource file resource.en.txt
	- event15=Select a backup folder before deleting all events...
	- event16=A second monitor was found. Switch to 'Extended' display mode?

 * JudgePanel
	* End of a segment: When the last skater has skated, the Judge panel left all showing the last skater and it didn't 
	 re-initialize the judge screen until the next event. Now, all judge screen are re-intialized when the Data Entry panel is closed.

----------------------------------------------------------------------------------------------------------------------------------------------

2. Known Issues in 2.0.0.10:

For known issues please send mail to:
   rollart@worldskate.org

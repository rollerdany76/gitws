﻿using Calculator.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class SkaterList : Form
    {
        bool completed = false;
        Waiting w = null;
        

        public SkaterList()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
        }

        private void SkaterList_Load(object sender, EventArgs e)
        {
            LoadSkaterList();
            w = new Waiting();
            RecuperoFactor();
        }

        private void LoadSkaterList()
        {
            try
            {
                SQLiteDataReader dr2 = null, dr = null;
                SQLiteCommand cmd = new SQLiteCommand(
                            "select S.Name, Completed, LastPartecipant, Partecipants, NumJudges, A.Name, C.Name, P.Name, A.Factor" +
                            " from GaraParams as A, Segments as S, Category as C, Specialita as P " +
                            " where ID_GaraParams = " + EventList.currentEvent.idevent +
                            " AND A.ID_SEGMENT = S.ID_SEGMENTS" +
                            " AND A.ID_CATEGORY = C.ID_CATEGORY" +
                            " AND A.ID_SPECIALITA = P.ID_SPECIALITA" +
                            " AND A.ID_SEGMENT = " + EventList.currentSegment.idsegment, Definizioni.conn);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    this.Text = dr[5].ToString();
                    // status
                    if (dr[1].ToString().Equals("Y"))
                    {
                        status.Text = "     Completed";
                        completed = true;
                        l1.Text = "" + dr[7].ToString() + " " + dr[6].ToString() + " " + dr[0].ToString().ToUpper(); // segment name
                    }
                    else if (dr[1].ToString().Equals("N") && !dr[2].ToString().Equals("0"))
                    {
                        status.Text = "     In progress";
                        l1.Text = "" + dr[7].ToString() + " " + dr[6].ToString() + " " + dr[0].ToString().ToUpper(); // segment name
                    }
                    else
                    {
                        status.Text = "     Not Started";
                        l1.Text = "" + dr[7].ToString() + " " + dr[6].ToString() + " " + dr[0].ToString().ToUpper(); // segment name
                    }

                    string query = "select NumStartingList, B.Name, A.ID_Atleta, Societa, Country, B.ID_Specialita" +
                                " from Participants as A, Athletes as B" +
                                " where ID_GaraParams = " + EventList.currentEvent.idevent +
                                " AND A.ID_SEGMENT = " + EventList.currentSegment.idsegment +
                                " AND A.ID_Atleta = B.ID_Atleta";

                    if (dr[1].ToString().Equals("Y"))
                    {
                        query = "select NumStartingList, B.Name, A.ID_Atleta, Societa, Country, B.ID_Specialita, Position" +
                                " from Participants as A, Athletes as B, GaraFinal as G" +
                                " where A.ID_GaraParams = " + EventList.currentEvent.idevent +
                                " AND A.ID_SEGMENT = G.ID_SEGMENT" +
                                " AND A.ID_GaraParams = G.ID_GaraParams" +
                                " AND A.ID_SEGMENT = " + EventList.currentSegment.idsegment +
                                " AND B.ID_Atleta = G.ID_Atleta" +
                                " AND A.ID_Atleta = B.ID_Atleta ORDER BY Position";
                        lbCompetitors.Columns[0].Text = "Rank";
                        lbCompetitors.Columns[10].Width = 0;
                        lbCompetitors.HotTracking = true;
                    }
                    else // non iniziato
                    {
                        lbCompetitors.Columns[0].Text = "Order";
                        lbCompetitors.Columns[10].Width = 0;
                        lbCompetitors.HotTracking = true;
                    }

                    cmd = new SQLiteCommand(query, Definizioni.conn);
                    dr = cmd.ExecuteReader();
                    int numAtleti = 0;
                    while (dr.Read())
                    {
                        lbCompetitors.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" }, -1));
                        lbCompetitors.Items[numAtleti].SubItems[0].Text = "" + (numAtleti + 1);
                        lbCompetitors.Items[numAtleti].SubItems[1].Text = dr[1].ToString().ToUpper(); // name
                        lbCompetitors.Items[numAtleti].SubItems[2].Text = dr[2].ToString(); // ID_Atleta
                        lbCompetitors.Items[numAtleti].SubItems[3].Text = dr[3].ToString().ToUpper(); // societa
                        lbCompetitors.Items[numAtleti].SubItems[4].Text = dr[4].ToString().ToUpper(); // country
                        lbCompetitors.Items[numAtleti].SubItems[11].Text = EventList.currentEvent.discipline.iddiscipline;
                        lbCompetitors.Items[numAtleti].SubItems[12].Text = dr[0].ToString().ToUpper(); // numPart

                        cmd = new SQLiteCommand(
                                        "select BaseTech, FinalTech, BaseArtistic, Deductions, Total, B.Position, TotGara, C.Position " +
                                        " from Participants as A, GaraFinal as B, GaraTotal as C" +
                                        " where A.ID_GaraParams = " + EventList.currentEvent.idevent +
                                        "   AND A.ID_GaraParams = B.ID_GaraParams" +
                                        "   AND A.ID_GaraParams = C.ID_GaraParams" +
                                        "   AND A.ID_SEGMENT = " + EventList.currentSegment.idsegment +
                                        "   AND A.ID_Atleta = " + dr[2].ToString() +
                                        "   AND A.ID_Segment = B.ID_Segment" +
                                        "   AND A.ID_Atleta = B.ID_Atleta" +
                                        "   AND A.ID_Atleta = C.ID_Atleta", Definizioni.conn);
                        dr2 = cmd.ExecuteReader();
                        while (dr2.Read())
                        {
                            lbCompetitors.Items[numAtleti].SubItems[5].Text = Utility.FormattaOggettoDecInString(dr2[0].ToString()); // BaseTech
                            lbCompetitors.Items[numAtleti].SubItems[6].Text = Utility.FormattaOggettoDecInString(dr2[1].ToString()); // FinalTech
                            lbCompetitors.Items[numAtleti].SubItems[7].Text = Utility.FormattaOggettoDecInString(dr2[2].ToString()); // BaseArtistic
                            lbCompetitors.Items[numAtleti].SubItems[8].Text = String.Format("{0:0}", dr2[3].ToString()); // Deductions
                            lbCompetitors.Items[numAtleti].SubItems[9].Text = Utility.FormattaOggettoDecInString(dr2[4].ToString()); // Total
                            lbCompetitors.Items[numAtleti].SubItems[10].Text = dr2[5].ToString(); // Position

                            if (dr2[5].ToString().Equals("1")) lbCompetitors.Items[numAtleti].ImageIndex = 0;
                            else if (dr2[5].ToString().Equals("2")) lbCompetitors.Items[numAtleti].ImageIndex = 1;
                            else if (dr2[5].ToString().Equals("3")) lbCompetitors.Items[numAtleti].ImageIndex = 2;
                            lbCompetitors.Items[numAtleti].ForeColor = Color.Blue;

                            lbCompetitors.Items[numAtleti].SubItems[13].Text = dr2[7].ToString().ToUpper(); // Final Pos
                            lbCompetitors.Items[numAtleti].SubItems[13].BackColor = Color.LemonChiffon;
                            lbCompetitors.Items[numAtleti].SubItems[14].Text = Utility.FormattaOggettoDecInString(dr2[6].ToString()); // Final Score
                        }
                        if (dr2.HasRows) // se segmento completato
                        {
                            lbCompetitors.Items[numAtleti].ImageIndex = 0;
                            lbCompetitors.Items[numAtleti].BackColor = Color.LightGray;
                        }
                        /*************************************************************************/

                        numAtleti++;
                    }
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex.Message, "ERROR");
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lbCompetitors_ItemActivate(object sender, EventArgs e)
        {
            try
            {
                if (!MainMDI.loginAS)
                {
                    // Login del Controller
                    SplashScreen sc = new SplashScreen(1);
                    sc.ShowDialog();

                    if (!MainMDI.loginAS)
                        return;
                }
                //tsDO.Image = Calculator.Properties.Resources.bullet_green;
                if (completed)
                {
                    if (lbCompetitors.SelectedItems.Count > 0)
                    {
                        var item = lbCompetitors.SelectedItems[0];
                        if (item.Text.Equals("")) return;

                        //Thread workerThread = new Thread(LoadWaitingGif);
                        //workerThread.Start();

                        lbCompetitors.UseWaitCursor = true;
                        lbCompetitors.Refresh();
                        pb.Visible = true;
                        pb.Refresh();
                        pb.Update();

                        EventList.currentSegment.currentSkater = new SkaterInfo();

                        EventList.currentSegment.currentSkater.name = item.SubItems[1].Text;
                        EventList.currentSegment.currentSkater.idskater = item.SubItems[2].Text;
                        EventList.currentSegment.currentSkater.club = item.SubItems[3].Text;
                        EventList.currentSegment.currentSkater.nation = item.SubItems[4].Text;
                        EventList.currentSegment.currentSkater.order = item.SubItems[12].Text;
                        EventList.currentSegment.currentSkater.final_rank = item.SubItems[13].Text;
                        EventList.currentSegment.currentSkater.final_score = item.SubItems[14].Text;
                        if (item.ForeColor == Color.Black) // non ancora iniziato
                        {
                            EventList.currentSegment.currentSkater.seg_rank = "";
                            EventList.currentSegment.currentSkater.base_value = "0.00";
                            EventList.currentSegment.currentSkater.tech_score = "0.00";
                            EventList.currentSegment.currentSkater.pres_score = "0.00";
                            EventList.currentSegment.currentSkater.deduction = "0.00";
                            EventList.currentSegment.currentSkater.seg_score = "0.00";
                        } else
                        {
                            EventList.currentSegment.currentSkater.seg_rank = item.SubItems[0].Text;
                            EventList.currentSegment.currentSkater.base_value = item.SubItems[5].Text;
                            EventList.currentSegment.currentSkater.tech_score = item.SubItems[6].Text;
                            EventList.currentSegment.currentSkater.pres_score = item.SubItems[7].Text;
                            EventList.currentSegment.currentSkater.deduction = item.SubItems[8].Text;
                            EventList.currentSegment.currentSkater.seg_score = item.SubItems[9].Text;                            
                        }
                        CheckScores cs = new CheckScores();

                        cs.ShowDialog();
                        pb.Visible = false;
                        lbCompetitors.UseWaitCursor = false;

                        // ricarico gli atleti
                        LoadSkaterList();

                        foreach (ListViewItem lvi in lbCompetitors.Items)
                        {
                            if (lvi.SubItems[12].Text == EventList.currentSegment.currentSkater.order)
                            {
                                if (EventList.currentSegment.currentSkater.updated)
                                    lvi.ForeColor = Color.Red;
                            }
                        }
                        this.Cursor = Cursors.Default;
                    }
                }
            }
            catch (Exception)
            {
                pb.Visible = false;
                this.Cursor = Cursors.Default;
                lbCompetitors.UseWaitCursor = false;
            }
        }

        public void RecuperoFactor()
        {
            try
            {
                string spec = "SegmentParams", idSepc = EventList.currentEvent.discipline.iddiscipline;

                if (idSepc.Equals("1") || idSepc.Equals("2")) spec = "SegmentParams";
                else if (idSepc.Equals("3")) spec = "SegmentParamsPairs";
                else if (idSepc.Equals("4") || idSepc.Equals("5") || idSepc.Equals("6")) spec = "SegmentParamsDance";
                else if (idSepc.Equals("7")) spec = "SegmentParamsPrecision";

                SQLiteCommand cmd = new SQLiteCommand("SELECT Factor FROM SegmentParams where id_segment = @idSegment " +
                              " and id_specialita = @idSpecialita and id_category = @idCategoria", Definizioni.conn);
                cmd.CommandText = cmd.CommandText.Replace("SegmentParams", spec)
                                     .Replace("@idSegment", "" + EventList.currentSegment.idsegment)
                                     .Replace("@idSpecialita", "" + EventList.currentEvent.discipline.iddiscipline)
                                     .Replace("@idCategoria", "" + EventList.currentEvent.category.idcategory);
                SQLiteDataReader sqdr = cmd.ExecuteReader();
                while (sqdr.Read())
                {
                    EventList.currentSegment.factor = decimal.Parse(sqdr[0].ToString());
                }
            } 
            catch (Exception ex)
            {
                Utility.WriteLog(ex.Message, "ERROR");
            }
}

        public void LoadWaitingGif()
        {
            //lbCompetitors.UseWaitCursor = true;
            //lbCompetitors.Refresh();
            //pb.Visible = true;
            //pb.Refresh();
            //pb.Update();
            //w.ShowDialog();
        }

    }
}

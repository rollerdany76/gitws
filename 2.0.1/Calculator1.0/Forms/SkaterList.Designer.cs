﻿namespace Calculator
{
    partial class SkaterList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SkaterList));
            this.panel1 = new System.Windows.Forms.Panel();
            this.status = new System.Windows.Forms.Label();
            this.l1 = new System.Windows.Forms.Label();
            this.lbCompetitors = new System.Windows.Forms.ListView();
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.id_specialita = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.numPart = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.finalRank = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.finalScore = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.pb = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.Controls.Add(this.status);
            this.panel1.Controls.Add(this.l1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(934, 41);
            this.panel1.TabIndex = 1;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // status
            // 
            this.status.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.status.AutoSize = true;
            this.status.BackColor = System.Drawing.Color.Transparent;
            this.status.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.status.ForeColor = System.Drawing.Color.Black;
            this.status.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.status.Location = new System.Drawing.Point(820, 9);
            this.status.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(129, 23);
            this.status.TabIndex = 138;
            this.status.Tag = "";
            this.status.Text = "      Completed";
            this.status.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // l1
            // 
            this.l1.AutoSize = true;
            this.l1.BackColor = System.Drawing.Color.Transparent;
            this.l1.Font = new System.Drawing.Font("Segoe UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l1.ForeColor = System.Drawing.Color.DimGray;
            this.l1.Location = new System.Drawing.Point(11, 9);
            this.l1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.l1.Name = "l1";
            this.l1.Size = new System.Drawing.Size(95, 28);
            this.l1.TabIndex = 137;
            this.l1.Tag = "";
            this.l1.Text = "Segment";
            // 
            // lbCompetitors
            // 
            this.lbCompetitors.Activation = System.Windows.Forms.ItemActivation.TwoClick;
            this.lbCompetitors.AllowColumnReorder = true;
            this.lbCompetitors.BackColor = System.Drawing.Color.White;
            this.lbCompetitors.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader3,
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11,
            this.id_specialita,
            this.numPart,
            this.finalRank,
            this.finalScore});
            this.lbCompetitors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbCompetitors.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCompetitors.ForeColor = System.Drawing.Color.Black;
            this.lbCompetitors.FullRowSelect = true;
            this.lbCompetitors.GridLines = true;
            this.lbCompetitors.HideSelection = false;
            this.lbCompetitors.HoverSelection = true;
            this.lbCompetitors.Location = new System.Drawing.Point(0, 41);
            this.lbCompetitors.Margin = new System.Windows.Forms.Padding(2);
            this.lbCompetitors.MultiSelect = false;
            this.lbCompetitors.Name = "lbCompetitors";
            this.lbCompetitors.ShowItemToolTips = true;
            this.lbCompetitors.Size = new System.Drawing.Size(934, 496);
            this.lbCompetitors.TabIndex = 87;
            this.lbCompetitors.UseCompatibleStateImageBehavior = false;
            this.lbCompetitors.View = System.Windows.Forms.View.Details;
            this.lbCompetitors.ItemActivate += new System.EventHandler(this.lbCompetitors_ItemActivate);
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Order";
            this.columnHeader4.Width = 48;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Name";
            this.columnHeader5.Width = 132;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "";
            this.columnHeader3.Width = 0;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Club";
            this.columnHeader1.Width = 150;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Nation";
            this.columnHeader2.Width = 55;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Base Tech";
            this.columnHeader6.Width = 70;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Final Tech";
            this.columnHeader7.Width = 72;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Components";
            this.columnHeader8.Width = 81;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Deductions";
            this.columnHeader9.Width = 76;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Total";
            this.columnHeader10.Width = 53;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Rank";
            this.columnHeader11.Width = 53;
            // 
            // id_specialita
            // 
            this.id_specialita.Text = "";
            this.id_specialita.Width = 0;
            // 
            // numPart
            // 
            this.numPart.Text = "numPart";
            this.numPart.Width = 0;
            // 
            // finalRank
            // 
            this.finalRank.Text = "Final Rank";
            this.finalRank.Width = 66;
            // 
            // finalScore
            // 
            this.finalScore.Text = "Total Score";
            this.finalScore.Width = 70;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "loading24x24.gif");
            // 
            // pb
            // 
            this.pb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pb.BackColor = System.Drawing.Color.White;
            this.pb.Image = global::RollartReview.Properties.Resources.loading24x24;
            this.pb.Location = new System.Drawing.Point(417, 237);
            this.pb.Name = "pb";
            this.pb.Size = new System.Drawing.Size(24, 24);
            this.pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb.TabIndex = 143;
            this.pb.TabStop = false;
            this.pb.Visible = false;
            // 
            // SkaterList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 537);
            this.Controls.Add(this.pb);
            this.Controls.Add(this.lbCompetitors);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "SkaterList";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Classifica";
            this.Load += new System.EventHandler(this.SkaterList_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListView lbCompetitors;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader id_specialita;
        private System.Windows.Forms.Label status;
        private System.Windows.Forms.Label l1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ColumnHeader numPart;
        private System.Windows.Forms.ColumnHeader finalRank;
        private System.Windows.Forms.ColumnHeader finalScore;
        public System.Windows.Forms.PictureBox pb;
    }
}
﻿namespace Calculator
{
    partial class CheckScores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckScores));
            this.panel1 = new System.Windows.Forms.Panel();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.baseValue = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.elementsScore = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.deductions = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.comp = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel5 = new System.Windows.Forms.ToolStripLabel();
            this.total = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel6 = new System.Windows.Forms.ToolStripLabel();
            this.rank = new System.Windows.Forms.ToolStripTextBox();
            this.rankimage = new System.Windows.Forms.ToolStripLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.name = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgElements = new System.Windows.Forms.DataGridView();
            this.gb = new System.Windows.Forms.GroupBox();
            this.panelJudges = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.j9 = new System.Windows.Forms.ComboBox();
            this.j8 = new System.Windows.Forms.ComboBox();
            this.j7 = new System.Windows.Forms.ComboBox();
            this.j6 = new System.Windows.Forms.ComboBox();
            this.j5 = new System.Windows.Forms.ComboBox();
            this.j4 = new System.Windows.Forms.ComboBox();
            this.j3 = new System.Windows.Forms.ComboBox();
            this.j2 = new System.Windows.Forms.ComboBox();
            this.j1 = new System.Windows.Forms.ComboBox();
            this.l9 = new System.Windows.Forms.Label();
            this.l8 = new System.Windows.Forms.Label();
            this.l7 = new System.Windows.Forms.Label();
            this.l6 = new System.Windows.Forms.Label();
            this.l5 = new System.Windows.Forms.Label();
            this.l4 = new System.Windows.Forms.Label();
            this.l3 = new System.Windows.Forms.Label();
            this.l2 = new System.Windows.Forms.Label();
            this.l1 = new System.Windows.Forms.Label();
            this.labelNew = new System.Windows.Forms.Label();
            this.nomeElemento = new System.Windows.Forms.TextBox();
            this.codice = new System.Windows.Forms.TextBox();
            this.undo = new System.Windows.Forms.Button();
            this.updateElement = new System.Windows.Forms.Button();
            this.num = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.bomusMeno = new System.Windows.Forms.Button();
            this.element = new System.Windows.Forms.Label();
            this.cb4 = new System.Windows.Forms.CheckBox();
            this.rb4 = new System.Windows.Forms.RadioButton();
            this.cb3 = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboPerc = new System.Windows.Forms.ComboBox();
            this.cb1 = new System.Windows.Forms.CheckBox();
            this.rb3 = new System.Windows.Forms.RadioButton();
            this.rb2 = new System.Windows.Forms.RadioButton();
            this.rb1 = new System.Windows.Forms.RadioButton();
            this.cbElements = new System.Windows.Forms.ComboBox();
            this.bonusPiu = new System.Windows.Forms.Button();
            this.addElement = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.newElement = new System.Windows.Forms.Button();
            this.save = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.deletelast = new System.Windows.Forms.Button();
            this.saveAndClose = new System.Windows.Forms.Button();
            this.close = new System.Windows.Forms.Button();
            this.message = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgElements)).BeginInit();
            this.gb.SuspendLayout();
            this.panelJudges.SuspendLayout();
            this.panel3.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.toolStrip2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(931, 85);
            this.panel1.TabIndex = 0;
            // 
            // toolStrip2
            // 
            this.toolStrip2.AutoSize = false;
            this.toolStrip2.BackColor = System.Drawing.Color.White;
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.baseValue,
            this.toolStripSeparator4,
            this.toolStripLabel2,
            this.elementsScore,
            this.toolStripSeparator5,
            this.toolStripLabel3,
            this.deductions,
            this.toolStripSeparator6,
            this.toolStripLabel4,
            this.comp,
            this.toolStripSeparator7,
            this.toolStripLabel5,
            this.total,
            this.toolStripSeparator8,
            this.toolStripLabel6,
            this.rank,
            this.rankimage});
            this.toolStrip2.Location = new System.Drawing.Point(0, 45);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(931, 30);
            this.toolStrip2.TabIndex = 130;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(84, 27);
            this.toolStripLabel1.Text = "Base Value:";
            // 
            // baseValue
            // 
            this.baseValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.baseValue.Name = "baseValue";
            this.baseValue.ReadOnly = true;
            this.baseValue.Size = new System.Drawing.Size(60, 30);
            this.baseValue.Text = "0.00";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.toolStripLabel2.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(108, 27);
            this.toolStripLabel2.Text = "Element Score:";
            // 
            // elementsScore
            // 
            this.elementsScore.BackColor = System.Drawing.Color.Gainsboro;
            this.elementsScore.Name = "elementsScore";
            this.elementsScore.ReadOnly = true;
            this.elementsScore.Size = new System.Drawing.Size(60, 30);
            this.elementsScore.Text = "0.00";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.toolStripLabel3.IsLink = true;
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(87, 27);
            this.toolStripLabel3.Text = "Deductions:";
            this.toolStripLabel3.Click += new System.EventHandler(this.toolStripLabel3_Click);
            // 
            // deductions
            // 
            this.deductions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.deductions.Name = "deductions";
            this.deductions.ReadOnly = true;
            this.deductions.Size = new System.Drawing.Size(40, 30);
            this.deductions.Text = "0";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.toolStripLabel4.IsLink = true;
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(97, 27);
            this.toolStripLabel4.Text = "Components:";
            this.toolStripLabel4.Click += new System.EventHandler(this.toolStripLabel4_Click);
            // 
            // comp
            // 
            this.comp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.comp.Name = "comp";
            this.comp.ReadOnly = true;
            this.comp.Size = new System.Drawing.Size(60, 30);
            this.comp.Text = "0.00";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripLabel5
            // 
            this.toolStripLabel5.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.toolStripLabel5.Name = "toolStripLabel5";
            this.toolStripLabel5.Size = new System.Drawing.Size(46, 27);
            this.toolStripLabel5.Text = "Total:";
            // 
            // total
            // 
            this.total.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.total.Name = "total";
            this.total.ReadOnly = true;
            this.total.Size = new System.Drawing.Size(60, 30);
            this.total.Text = "0.00";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripLabel6
            // 
            this.toolStripLabel6.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.toolStripLabel6.Name = "toolStripLabel6";
            this.toolStripLabel6.Size = new System.Drawing.Size(110, 27);
            this.toolStripLabel6.Text = "Segment Rank:";
            // 
            // rank
            // 
            this.rank.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.rank.Name = "rank";
            this.rank.ReadOnly = true;
            this.rank.Size = new System.Drawing.Size(30, 30);
            this.rank.Click += new System.EventHandler(this.rank_Click);
            // 
            // rankimage
            // 
            this.rankimage.Name = "rankimage";
            this.rankimage.Size = new System.Drawing.Size(0, 27);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label3.Font = new System.Drawing.Font("Trebuchet MS", 4F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 75);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(931, 10);
            this.label3.TabIndex = 1;
            this.label3.Text = "    ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.name);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(931, 41);
            this.panel4.TabIndex = 132;
            // 
            // name
            // 
            this.name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.name.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name.ForeColor = System.Drawing.Color.White;
            this.name.Location = new System.Drawing.Point(0, 0);
            this.name.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(931, 41);
            this.name.TabIndex = 0;
            this.name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgElements);
            this.panel2.Controls.Add(this.gb);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(0, 85);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(931, 500);
            this.panel2.TabIndex = 1;
            // 
            // dgElements
            // 
            this.dgElements.AllowUserToAddRows = false;
            this.dgElements.AllowUserToDeleteRows = false;
            this.dgElements.AllowUserToResizeRows = false;
            this.dgElements.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgElements.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.dgElements.BackgroundColor = System.Drawing.Color.Silver;
            this.dgElements.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dgElements.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgElements.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgElements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(206)))), ((int)(((byte)(56)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgElements.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgElements.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgElements.EnableHeadersVisualStyles = false;
            this.dgElements.GridColor = System.Drawing.Color.Silver;
            this.dgElements.Location = new System.Drawing.Point(0, 0);
            this.dgElements.Margin = new System.Windows.Forms.Padding(2);
            this.dgElements.MultiSelect = false;
            this.dgElements.Name = "dgElements";
            this.dgElements.ReadOnly = true;
            this.dgElements.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgElements.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgElements.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgElements.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgElements.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgElements.Size = new System.Drawing.Size(931, 347);
            this.dgElements.TabIndex = 10;
            this.dgElements.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgElements_CellFormatting);
            this.dgElements.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgElements_DataError);
            this.dgElements.SelectionChanged += new System.EventHandler(this.dgElements_SelectionChanged);
            // 
            // gb
            // 
            this.gb.Controls.Add(this.panelJudges);
            this.gb.Controls.Add(this.labelNew);
            this.gb.Controls.Add(this.nomeElemento);
            this.gb.Controls.Add(this.codice);
            this.gb.Controls.Add(this.undo);
            this.gb.Controls.Add(this.updateElement);
            this.gb.Controls.Add(this.num);
            this.gb.Controls.Add(this.label4);
            this.gb.Controls.Add(this.bomusMeno);
            this.gb.Controls.Add(this.element);
            this.gb.Controls.Add(this.cb4);
            this.gb.Controls.Add(this.rb4);
            this.gb.Controls.Add(this.cb3);
            this.gb.Controls.Add(this.label2);
            this.gb.Controls.Add(this.comboPerc);
            this.gb.Controls.Add(this.cb1);
            this.gb.Controls.Add(this.rb3);
            this.gb.Controls.Add(this.rb2);
            this.gb.Controls.Add(this.rb1);
            this.gb.Controls.Add(this.cbElements);
            this.gb.Controls.Add(this.bonusPiu);
            this.gb.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gb.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb.Location = new System.Drawing.Point(0, 347);
            this.gb.Margin = new System.Windows.Forms.Padding(0);
            this.gb.Name = "gb";
            this.gb.Size = new System.Drawing.Size(931, 153);
            this.gb.TabIndex = 136;
            this.gb.TabStop = false;
            // 
            // panelJudges
            // 
            this.panelJudges.BackColor = System.Drawing.Color.LightCyan;
            this.panelJudges.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelJudges.Controls.Add(this.label1);
            this.panelJudges.Controls.Add(this.j9);
            this.panelJudges.Controls.Add(this.j8);
            this.panelJudges.Controls.Add(this.j7);
            this.panelJudges.Controls.Add(this.j6);
            this.panelJudges.Controls.Add(this.j5);
            this.panelJudges.Controls.Add(this.j4);
            this.panelJudges.Controls.Add(this.j3);
            this.panelJudges.Controls.Add(this.j2);
            this.panelJudges.Controls.Add(this.j1);
            this.panelJudges.Controls.Add(this.l9);
            this.panelJudges.Controls.Add(this.l8);
            this.panelJudges.Controls.Add(this.l7);
            this.panelJudges.Controls.Add(this.l6);
            this.panelJudges.Controls.Add(this.l5);
            this.panelJudges.Controls.Add(this.l4);
            this.panelJudges.Controls.Add(this.l3);
            this.panelJudges.Controls.Add(this.l2);
            this.panelJudges.Controls.Add(this.l1);
            this.panelJudges.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelJudges.Location = new System.Drawing.Point(3, 105);
            this.panelJudges.Name = "panelJudges";
            this.panelJudges.Size = new System.Drawing.Size(925, 45);
            this.panelJudges.TabIndex = 156;
            this.panelJudges.Visible = false;
            this.panelJudges.Paint += new System.Windows.Forms.PaintEventHandler(this.panelJudges_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 19);
            this.label1.TabIndex = 169;
            this.label1.Text = "Judges QOEs:";
            // 
            // j9
            // 
            this.j9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.j9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j9.ForeColor = System.Drawing.Color.Red;
            this.j9.FormattingEnabled = true;
            this.j9.Items.AddRange(new object[] {
            "",
            "+3",
            "+2",
            "+1",
            "0",
            "-1",
            "-2",
            "-3"});
            this.j9.Location = new System.Drawing.Point(652, 11);
            this.j9.Name = "j9";
            this.j9.Size = new System.Drawing.Size(44, 27);
            this.j9.TabIndex = 159;
            this.j9.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // j8
            // 
            this.j8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.j8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j8.ForeColor = System.Drawing.Color.Red;
            this.j8.FormattingEnabled = true;
            this.j8.Items.AddRange(new object[] {
            "",
            "+3",
            "+2",
            "+1",
            "0",
            "-1",
            "-2",
            "-3"});
            this.j8.Location = new System.Drawing.Point(587, 11);
            this.j8.Name = "j8";
            this.j8.Size = new System.Drawing.Size(44, 27);
            this.j8.TabIndex = 158;
            this.j8.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // j7
            // 
            this.j7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.j7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j7.ForeColor = System.Drawing.Color.Red;
            this.j7.FormattingEnabled = true;
            this.j7.Items.AddRange(new object[] {
            "+3",
            "+2",
            "+1",
            "0",
            "-1",
            "-2",
            "-3"});
            this.j7.Location = new System.Drawing.Point(522, 11);
            this.j7.Name = "j7";
            this.j7.Size = new System.Drawing.Size(44, 27);
            this.j7.TabIndex = 157;
            this.j7.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // j6
            // 
            this.j6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.j6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j6.ForeColor = System.Drawing.Color.Red;
            this.j6.FormattingEnabled = true;
            this.j6.Items.AddRange(new object[] {
            "",
            "+3",
            "+2",
            "+1",
            "0",
            "-1",
            "-2",
            "-3"});
            this.j6.Location = new System.Drawing.Point(457, 11);
            this.j6.Name = "j6";
            this.j6.Size = new System.Drawing.Size(44, 27);
            this.j6.TabIndex = 156;
            this.j6.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // j5
            // 
            this.j5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.j5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j5.ForeColor = System.Drawing.Color.Red;
            this.j5.FormattingEnabled = true;
            this.j5.Items.AddRange(new object[] {
            "",
            "+3",
            "+2",
            "+1",
            "0",
            "-1",
            "-2",
            "-3"});
            this.j5.Location = new System.Drawing.Point(392, 11);
            this.j5.Name = "j5";
            this.j5.Size = new System.Drawing.Size(44, 27);
            this.j5.TabIndex = 155;
            this.j5.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // j4
            // 
            this.j4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.j4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j4.ForeColor = System.Drawing.Color.Red;
            this.j4.FormattingEnabled = true;
            this.j4.Items.AddRange(new object[] {
            "",
            "+3",
            "+2",
            "+1",
            "0",
            "-1",
            "-2",
            "-3"});
            this.j4.Location = new System.Drawing.Point(327, 11);
            this.j4.Name = "j4";
            this.j4.Size = new System.Drawing.Size(44, 27);
            this.j4.TabIndex = 154;
            this.j4.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // j3
            // 
            this.j3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.j3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j3.ForeColor = System.Drawing.Color.Red;
            this.j3.FormattingEnabled = true;
            this.j3.Items.AddRange(new object[] {
            "",
            "+3",
            "+2",
            "+1",
            "0",
            "-1",
            "-2",
            "-3"});
            this.j3.Location = new System.Drawing.Point(262, 11);
            this.j3.Name = "j3";
            this.j3.Size = new System.Drawing.Size(44, 27);
            this.j3.TabIndex = 153;
            this.j3.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // j2
            // 
            this.j2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.j2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j2.ForeColor = System.Drawing.Color.Red;
            this.j2.FormattingEnabled = true;
            this.j2.Items.AddRange(new object[] {
            "",
            "+3",
            "+2",
            "+1",
            "0",
            "-1",
            "-2",
            "-3"});
            this.j2.Location = new System.Drawing.Point(197, 11);
            this.j2.Name = "j2";
            this.j2.Size = new System.Drawing.Size(44, 27);
            this.j2.TabIndex = 152;
            this.j2.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // j1
            // 
            this.j1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.j1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j1.ForeColor = System.Drawing.Color.Red;
            this.j1.FormattingEnabled = true;
            this.j1.Items.AddRange(new object[] {
            "",
            "+3",
            "+2",
            "+1",
            "0",
            "-1",
            "-2",
            "-3"});
            this.j1.Location = new System.Drawing.Point(132, 11);
            this.j1.Name = "j1";
            this.j1.Size = new System.Drawing.Size(44, 27);
            this.j1.TabIndex = 151;
            this.j1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // l9
            // 
            this.l9.AutoSize = true;
            this.l9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l9.Location = new System.Drawing.Point(635, 14);
            this.l9.Name = "l9";
            this.l9.Size = new System.Drawing.Size(22, 19);
            this.l9.TabIndex = 168;
            this.l9.Text = "J9";
            // 
            // l8
            // 
            this.l8.AutoSize = true;
            this.l8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l8.Location = new System.Drawing.Point(570, 14);
            this.l8.Name = "l8";
            this.l8.Size = new System.Drawing.Size(22, 19);
            this.l8.TabIndex = 167;
            this.l8.Text = "J8";
            // 
            // l7
            // 
            this.l7.AutoSize = true;
            this.l7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l7.Location = new System.Drawing.Point(505, 14);
            this.l7.Name = "l7";
            this.l7.Size = new System.Drawing.Size(22, 19);
            this.l7.TabIndex = 166;
            this.l7.Text = "J7";
            // 
            // l6
            // 
            this.l6.AutoSize = true;
            this.l6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l6.Location = new System.Drawing.Point(440, 14);
            this.l6.Name = "l6";
            this.l6.Size = new System.Drawing.Size(22, 19);
            this.l6.TabIndex = 165;
            this.l6.Text = "J6";
            // 
            // l5
            // 
            this.l5.AutoSize = true;
            this.l5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l5.Location = new System.Drawing.Point(375, 14);
            this.l5.Name = "l5";
            this.l5.Size = new System.Drawing.Size(22, 19);
            this.l5.TabIndex = 164;
            this.l5.Text = "J5";
            // 
            // l4
            // 
            this.l4.AutoSize = true;
            this.l4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l4.Location = new System.Drawing.Point(310, 14);
            this.l4.Name = "l4";
            this.l4.Size = new System.Drawing.Size(22, 19);
            this.l4.TabIndex = 163;
            this.l4.Text = "J4";
            // 
            // l3
            // 
            this.l3.AutoSize = true;
            this.l3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l3.Location = new System.Drawing.Point(245, 14);
            this.l3.Name = "l3";
            this.l3.Size = new System.Drawing.Size(22, 19);
            this.l3.TabIndex = 162;
            this.l3.Text = "J3";
            // 
            // l2
            // 
            this.l2.AutoSize = true;
            this.l2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l2.Location = new System.Drawing.Point(180, 14);
            this.l2.Name = "l2";
            this.l2.Size = new System.Drawing.Size(22, 19);
            this.l2.TabIndex = 161;
            this.l2.Text = "J2";
            // 
            // l1
            // 
            this.l1.AutoSize = true;
            this.l1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l1.Location = new System.Drawing.Point(115, 14);
            this.l1.Name = "l1";
            this.l1.Size = new System.Drawing.Size(22, 19);
            this.l1.TabIndex = 160;
            this.l1.Text = "J1";
            // 
            // labelNew
            // 
            this.labelNew.AutoSize = true;
            this.labelNew.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNew.Location = new System.Drawing.Point(12, 65);
            this.labelNew.Name = "labelNew";
            this.labelNew.Size = new System.Drawing.Size(157, 19);
            this.labelNew.TabIndex = 155;
            this.labelNew.Text = "Insert the element Code";
            this.labelNew.Visible = false;
            // 
            // nomeElemento
            // 
            this.nomeElemento.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.nomeElemento.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.nomeElemento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nomeElemento.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nomeElemento.Location = new System.Drawing.Point(106, 39);
            this.nomeElemento.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.nomeElemento.MaxLength = 50;
            this.nomeElemento.Name = "nomeElemento";
            this.nomeElemento.ReadOnly = true;
            this.nomeElemento.Size = new System.Drawing.Size(181, 26);
            this.nomeElemento.TabIndex = 154;
            this.nomeElemento.Visible = false;
            this.nomeElemento.TextChanged += new System.EventHandler(this.nomeElemento_TextChanged);
            // 
            // codice
            // 
            this.codice.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.codice.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.codice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.codice.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.codice.Location = new System.Drawing.Point(15, 39);
            this.codice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.codice.MaxLength = 50;
            this.codice.Name = "codice";
            this.codice.Size = new System.Drawing.Size(85, 26);
            this.codice.TabIndex = 153;
            this.codice.Visible = false;
            this.codice.TabIndexChanged += new System.EventHandler(this.codice_TabIndexChanged);
            this.codice.TextChanged += new System.EventHandler(this.codice_TextChanged);
            this.codice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.codice_KeyDown);
            this.codice.Leave += new System.EventHandler(this.codice_Leave);
            // 
            // undo
            // 
            this.undo.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.undo.ForeColor = System.Drawing.Color.Red;
            this.undo.Location = new System.Drawing.Point(793, 65);
            this.undo.Name = "undo";
            this.undo.Size = new System.Drawing.Size(126, 34);
            this.undo.TabIndex = 145;
            this.undo.Text = "Undo";
            this.undo.UseVisualStyleBackColor = true;
            this.undo.Click += new System.EventHandler(this.button2_Click);
            // 
            // updateElement
            // 
            this.updateElement.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateElement.ForeColor = System.Drawing.Color.Green;
            this.updateElement.Location = new System.Drawing.Point(793, 24);
            this.updateElement.Name = "updateElement";
            this.updateElement.Size = new System.Drawing.Size(126, 35);
            this.updateElement.TabIndex = 144;
            this.updateElement.Text = "Update element";
            this.updateElement.UseVisualStyleBackColor = true;
            this.updateElement.Click += new System.EventHandler(this.button1_Click);
            // 
            // num
            // 
            this.num.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num.Location = new System.Drawing.Point(600, 59);
            this.num.Margin = new System.Windows.Forms.Padding(2);
            this.num.Name = "num";
            this.num.ReadOnly = true;
            this.num.Size = new System.Drawing.Size(52, 26);
            this.num.TabIndex = 152;
            this.num.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(552, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 19);
            this.label4.TabIndex = 151;
            this.label4.Text = "Bonus";
            // 
            // bomusMeno
            // 
            this.bomusMeno.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bomusMeno.Enabled = false;
            this.bomusMeno.FlatAppearance.BorderSize = 0;
            this.bomusMeno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bomusMeno.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bomusMeno.Image = global::RollartReview.Properties.Resources.round_minus_icon_16;
            this.bomusMeno.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.bomusMeno.Location = new System.Drawing.Point(572, 57);
            this.bomusMeno.Margin = new System.Windows.Forms.Padding(0);
            this.bomusMeno.Name = "bomusMeno";
            this.bomusMeno.Size = new System.Drawing.Size(24, 24);
            this.bomusMeno.TabIndex = 148;
            this.bomusMeno.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.bomusMeno.UseVisualStyleBackColor = true;
            this.bomusMeno.Click += new System.EventHandler(this.bomusMeno_Click);
            // 
            // element
            // 
            this.element.AutoSize = true;
            this.element.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.element.Location = new System.Drawing.Point(251, 13);
            this.element.Name = "element";
            this.element.Size = new System.Drawing.Size(45, 19);
            this.element.TabIndex = 146;
            this.element.Text = "label1";
            this.element.Visible = false;
            // 
            // cb4
            // 
            this.cb4.AutoSize = true;
            this.cb4.Enabled = false;
            this.cb4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb4.ForeColor = System.Drawing.Color.Black;
            this.cb4.Location = new System.Drawing.Point(319, 78);
            this.cb4.Name = "cb4";
            this.cb4.Size = new System.Drawing.Size(38, 23);
            this.cb4.TabIndex = 143;
            this.cb4.Text = "T";
            this.cb4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cb4.UseVisualStyleBackColor = true;
            // 
            // rb4
            // 
            this.rb4.AutoSize = true;
            this.rb4.Enabled = false;
            this.rb4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb4.ForeColor = System.Drawing.Color.Black;
            this.rb4.Location = new System.Drawing.Point(410, 25);
            this.rb4.Name = "rb4";
            this.rb4.Size = new System.Drawing.Size(89, 23);
            this.rb4.TabIndex = 142;
            this.rb4.TabStop = true;
            this.rb4.Text = "Complete";
            this.rb4.UseVisualStyleBackColor = true;
            // 
            // cb3
            // 
            this.cb3.AutoSize = true;
            this.cb3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb3.ForeColor = System.Drawing.Color.Black;
            this.cb3.Location = new System.Drawing.Point(319, 53);
            this.cb3.Name = "cb3";
            this.cb3.Size = new System.Drawing.Size(37, 23);
            this.cb3.TabIndex = 140;
            this.cb3.Text = "*";
            this.cb3.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 19);
            this.label2.TabIndex = 139;
            this.label2.Text = "label2";
            this.label2.Visible = false;
            // 
            // comboPerc
            // 
            this.comboPerc.BackColor = System.Drawing.Color.White;
            this.comboPerc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboPerc.Enabled = false;
            this.comboPerc.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboPerc.FormattingEnabled = true;
            this.comboPerc.Items.AddRange(new object[] {
            "0%",
            "5%",
            "10%",
            "15%",
            "20%",
            "25%",
            "30%",
            "35%",
            "40%",
            "45%",
            "50%",
            "55%",
            "60%",
            "65%",
            "70%",
            "75%",
            "80%",
            "85%",
            "90%",
            "95%",
            "100%"});
            this.comboPerc.Location = new System.Drawing.Point(600, 27);
            this.comboPerc.Name = "comboPerc";
            this.comboPerc.Size = new System.Drawing.Size(52, 27);
            this.comboPerc.TabIndex = 138;
            // 
            // cb1
            // 
            this.cb1.AutoSize = true;
            this.cb1.Enabled = false;
            this.cb1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb1.Location = new System.Drawing.Point(319, 28);
            this.cb1.Name = "cb1";
            this.cb1.Size = new System.Drawing.Size(76, 23);
            this.cb1.TabIndex = 136;
            this.cb1.Text = "Combo";
            this.cb1.UseVisualStyleBackColor = true;
            // 
            // rb3
            // 
            this.rb3.AutoSize = true;
            this.rb3.Enabled = false;
            this.rb3.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb3.ForeColor = System.Drawing.Color.Black;
            this.rb3.Location = new System.Drawing.Point(410, 82);
            this.rb3.Name = "rb3";
            this.rb3.Size = new System.Drawing.Size(143, 23);
            this.rb3.TabIndex = 135;
            this.rb3.TabStop = true;
            this.rb3.Text = "<<< Downgraded";
            this.rb3.UseVisualStyleBackColor = true;
            // 
            // rb2
            // 
            this.rb2.AutoSize = true;
            this.rb2.Enabled = false;
            this.rb2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb2.ForeColor = System.Drawing.Color.Black;
            this.rb2.Location = new System.Drawing.Point(410, 63);
            this.rb2.Name = "rb2";
            this.rb2.Size = new System.Drawing.Size(129, 23);
            this.rb2.TabIndex = 134;
            this.rb2.TabStop = true;
            this.rb2.Text = "<< Half-rotated";
            this.rb2.UseVisualStyleBackColor = true;
            // 
            // rb1
            // 
            this.rb1.AutoSize = true;
            this.rb1.Enabled = false;
            this.rb1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb1.ForeColor = System.Drawing.Color.Black;
            this.rb1.Location = new System.Drawing.Point(410, 44);
            this.rb1.Name = "rb1";
            this.rb1.Size = new System.Drawing.Size(133, 23);
            this.rb1.TabIndex = 133;
            this.rb1.TabStop = true;
            this.rb1.Text = "< Under-rotated";
            this.rb1.UseVisualStyleBackColor = true;
            // 
            // cbElements
            // 
            this.cbElements.BackColor = System.Drawing.SystemColors.Info;
            this.cbElements.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbElements.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbElements.FormattingEnabled = true;
            this.cbElements.Location = new System.Drawing.Point(15, 29);
            this.cbElements.Name = "cbElements";
            this.cbElements.Size = new System.Drawing.Size(272, 27);
            this.cbElements.TabIndex = 132;
            this.cbElements.SelectedIndexChanged += new System.EventHandler(this.cbElements_SelectedIndexChanged);
            // 
            // bonusPiu
            // 
            this.bonusPiu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bonusPiu.Enabled = false;
            this.bonusPiu.FlatAppearance.BorderSize = 0;
            this.bonusPiu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bonusPiu.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bonusPiu.Image = global::RollartReview.Properties.Resources.round_plus_icon_16;
            this.bonusPiu.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.bonusPiu.Location = new System.Drawing.Point(549, 57);
            this.bonusPiu.Margin = new System.Windows.Forms.Padding(0);
            this.bonusPiu.Name = "bonusPiu";
            this.bonusPiu.Size = new System.Drawing.Size(24, 24);
            this.bonusPiu.TabIndex = 149;
            this.bonusPiu.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bonusPiu.UseVisualStyleBackColor = true;
            this.bonusPiu.Click += new System.EventHandler(this.bonusPiu_Click);
            // 
            // addElement
            // 
            this.addElement.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addElement.Location = new System.Drawing.Point(135, 3);
            this.addElement.Name = "addElement";
            this.addElement.Size = new System.Drawing.Size(109, 41);
            this.addElement.TabIndex = 156;
            this.addElement.Text = "Add element";
            this.addElement.UseVisualStyleBackColor = true;
            this.addElement.Visible = false;
            this.addElement.Click += new System.EventHandler(this.addElement_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.flowLayoutPanel1);
            this.panel3.Controls.Add(this.message);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 585);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(931, 77);
            this.panel3.TabIndex = 2;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.newElement);
            this.flowLayoutPanel1.Controls.Add(this.addElement);
            this.flowLayoutPanel1.Controls.Add(this.save);
            this.flowLayoutPanel1.Controls.Add(this.cancel);
            this.flowLayoutPanel1.Controls.Add(this.deletelast);
            this.flowLayoutPanel1.Controls.Add(this.saveAndClose);
            this.flowLayoutPanel1.Controls.Add(this.close);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(931, 47);
            this.flowLayoutPanel1.TabIndex = 148;
            this.flowLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.flowLayoutPanel1_Paint);
            // 
            // newElement
            // 
            this.newElement.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newElement.Location = new System.Drawing.Point(3, 3);
            this.newElement.Name = "newElement";
            this.newElement.Size = new System.Drawing.Size(126, 41);
            this.newElement.TabIndex = 148;
            this.newElement.Text = "New element";
            this.newElement.UseVisualStyleBackColor = true;
            this.newElement.Click += new System.EventHandler(this.newElement_Click);
            // 
            // save
            // 
            this.save.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.save.ForeColor = System.Drawing.Color.Green;
            this.save.Location = new System.Drawing.Point(250, 3);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(62, 41);
            this.save.TabIndex = 150;
            this.save.Text = "Save";
            this.save.UseVisualStyleBackColor = true;
            this.save.Visible = false;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // cancel
            // 
            this.cancel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancel.ForeColor = System.Drawing.Color.Red;
            this.cancel.Location = new System.Drawing.Point(318, 3);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(62, 41);
            this.cancel.TabIndex = 149;
            this.cancel.Text = "Cancel";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Visible = false;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // deletelast
            // 
            this.deletelast.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deletelast.Location = new System.Drawing.Point(386, 3);
            this.deletelast.Name = "deletelast";
            this.deletelast.Size = new System.Drawing.Size(114, 41);
            this.deletelast.TabIndex = 157;
            this.deletelast.Text = "Delete last";
            this.deletelast.UseVisualStyleBackColor = true;
            this.deletelast.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // saveAndClose
            // 
            this.saveAndClose.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveAndClose.ForeColor = System.Drawing.Color.Green;
            this.saveAndClose.Location = new System.Drawing.Point(506, 3);
            this.saveAndClose.Name = "saveAndClose";
            this.saveAndClose.Size = new System.Drawing.Size(124, 41);
            this.saveAndClose.TabIndex = 146;
            this.saveAndClose.Text = "Save and Close";
            this.saveAndClose.UseVisualStyleBackColor = true;
            this.saveAndClose.Click += new System.EventHandler(this.button3_Click);
            // 
            // close
            // 
            this.close.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.close.Location = new System.Drawing.Point(636, 3);
            this.close.Name = "close";
            this.close.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.close.Size = new System.Drawing.Size(56, 41);
            this.close.TabIndex = 147;
            this.close.Text = "Close";
            this.close.UseVisualStyleBackColor = true;
            this.close.Click += new System.EventHandler(this.button4_Click);
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.message.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.message.ForeColor = System.Drawing.Color.Red;
            this.message.Location = new System.Drawing.Point(0, 58);
            this.message.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(0, 19);
            this.message.TabIndex = 0;
            // 
            // CheckScores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(931, 662);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "CheckScores";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "CheckScores";
            this.Load += new System.EventHandler(this.CheckScores_Load);
            this.panel1.ResumeLayout(false);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgElements)).EndInit();
            this.gb.ResumeLayout(false);
            this.gb.PerformLayout();
            this.panelJudges.ResumeLayout(false);
            this.panelJudges.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.DataGridView dgElements;
        private System.Windows.Forms.GroupBox gb;
        private System.Windows.Forms.Button undo;
        private System.Windows.Forms.Button updateElement;
        private System.Windows.Forms.CheckBox cb4;
        private System.Windows.Forms.RadioButton rb4;
        private System.Windows.Forms.CheckBox cb3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboPerc;
        private System.Windows.Forms.CheckBox cb1;
        private System.Windows.Forms.RadioButton rb3;
        private System.Windows.Forms.RadioButton rb2;
        private System.Windows.Forms.RadioButton rb1;
        private System.Windows.Forms.ComboBox cbElements;
        private System.Windows.Forms.Label element;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox baseValue;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripTextBox elementsScore;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripTextBox deductions;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripTextBox comp;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripLabel toolStripLabel5;
        private System.Windows.Forms.ToolStripTextBox total;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripLabel toolStripLabel6;
        private System.Windows.Forms.ToolStripTextBox rank;
        private System.Windows.Forms.Button bomusMeno;
        private System.Windows.Forms.Button bonusPiu;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label name;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button close;
        private System.Windows.Forms.Button saveAndClose;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TextBox num;
        private System.Windows.Forms.Button newElement;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.TextBox nomeElemento;
        private System.Windows.Forms.TextBox codice;
        private System.Windows.Forms.Label labelNew;
        private System.Windows.Forms.Button addElement;
        private System.Windows.Forms.Panel panelJudges;
        private System.Windows.Forms.ComboBox j1;
        private System.Windows.Forms.ComboBox j2;
        private System.Windows.Forms.ComboBox j4;
        private System.Windows.Forms.ComboBox j3;
        private System.Windows.Forms.ComboBox j8;
        private System.Windows.Forms.ComboBox j7;
        private System.Windows.Forms.ComboBox j6;
        private System.Windows.Forms.ComboBox j5;
        private System.Windows.Forms.ComboBox j9;
        private System.Windows.Forms.Label l9;
        private System.Windows.Forms.Label l8;
        private System.Windows.Forms.Label l7;
        private System.Windows.Forms.Label l6;
        private System.Windows.Forms.Label l5;
        private System.Windows.Forms.Label l4;
        private System.Windows.Forms.Label l3;
        private System.Windows.Forms.Label l2;
        private System.Windows.Forms.Label l1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripLabel rankimage;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button deletelast;
    }
}
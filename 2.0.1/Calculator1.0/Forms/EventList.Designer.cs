﻿namespace Calculator
{
    partial class EventList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EventList));
            this.tableCompetition = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.skaters = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.place = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.date = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.judges = new System.Windows.Forms.TextBox();
            this.discipline = new System.Windows.Forms.TextBox();
            this.category = new System.Windows.Forms.TextBox();
            this.seg1 = new System.Windows.Forms.Label();
            this.seg2 = new System.Windows.Forms.Label();
            this.seg3 = new System.Windows.Forms.Label();
            this.status1 = new System.Windows.Forms.Button();
            this.status2 = new System.Windows.Forms.Button();
            this.status3 = new System.Windows.Forms.Button();
            this.tv = new System.Windows.Forms.TreeView();
            this.tableCompetition.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableCompetition
            // 
            this.tableCompetition.ColumnCount = 4;
            this.tableCompetition.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableCompetition.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableCompetition.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableCompetition.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableCompetition.Controls.Add(this.label6, 0, 3);
            this.tableCompetition.Controls.Add(this.skaters, 3, 2);
            this.tableCompetition.Controls.Add(this.label1, 0, 0);
            this.tableCompetition.Controls.Add(this.name, 1, 0);
            this.tableCompetition.Controls.Add(this.place, 1, 1);
            this.tableCompetition.Controls.Add(this.label2, 0, 1);
            this.tableCompetition.Controls.Add(this.label3, 2, 1);
            this.tableCompetition.Controls.Add(this.date, 3, 1);
            this.tableCompetition.Controls.Add(this.label4, 0, 2);
            this.tableCompetition.Controls.Add(this.label5, 2, 2);
            this.tableCompetition.Controls.Add(this.label7, 2, 3);
            this.tableCompetition.Controls.Add(this.judges, 1, 2);
            this.tableCompetition.Controls.Add(this.discipline, 1, 3);
            this.tableCompetition.Controls.Add(this.category, 3, 3);
            this.tableCompetition.Controls.Add(this.seg1, 1, 5);
            this.tableCompetition.Controls.Add(this.seg2, 2, 5);
            this.tableCompetition.Controls.Add(this.seg3, 3, 5);
            this.tableCompetition.Controls.Add(this.status1, 1, 6);
            this.tableCompetition.Controls.Add(this.status2, 2, 6);
            this.tableCompetition.Controls.Add(this.status3, 3, 6);
            this.tableCompetition.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableCompetition.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableCompetition.Location = new System.Drawing.Point(0, 387);
            this.tableCompetition.Name = "tableCompetition";
            this.tableCompetition.RowCount = 7;
            this.tableCompetition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableCompetition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableCompetition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableCompetition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableCompetition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 11F));
            this.tableCompetition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableCompetition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableCompetition.Size = new System.Drawing.Size(486, 184);
            this.tableCompetition.TabIndex = 1;
            this.tableCompetition.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 25);
            this.label6.TabIndex = 10;
            this.label6.Text = "Discipline";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // skaters
            // 
            this.skaters.Location = new System.Drawing.Point(366, 53);
            this.skaters.Name = "skaters";
            this.skaters.ReadOnly = true;
            this.skaters.Size = new System.Drawing.Size(53, 22);
            this.skaters.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Competition";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // name
            // 
            this.tableCompetition.SetColumnSpan(this.name, 3);
            this.name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.name.Location = new System.Drawing.Point(124, 3);
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.Size = new System.Drawing.Size(359, 22);
            this.name.TabIndex = 1;
            // 
            // place
            // 
            this.place.Dock = System.Windows.Forms.DockStyle.Fill;
            this.place.Location = new System.Drawing.Point(124, 28);
            this.place.Name = "place";
            this.place.ReadOnly = true;
            this.place.Size = new System.Drawing.Size(115, 22);
            this.place.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "Place";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(245, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "Date from";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // date
            // 
            this.date.Dock = System.Windows.Forms.DockStyle.Fill;
            this.date.Location = new System.Drawing.Point(366, 28);
            this.date.Name = "date";
            this.date.ReadOnly = true;
            this.date.Size = new System.Drawing.Size(117, 22);
            this.date.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 25);
            this.label4.TabIndex = 6;
            this.label4.Text = "Judges";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(245, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 25);
            this.label5.TabIndex = 7;
            this.label5.Text = "Competitors";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(245, 75);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 25);
            this.label7.TabIndex = 11;
            this.label7.Text = "Category";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // judges
            // 
            this.judges.Location = new System.Drawing.Point(124, 53);
            this.judges.Name = "judges";
            this.judges.ReadOnly = true;
            this.judges.Size = new System.Drawing.Size(47, 22);
            this.judges.TabIndex = 8;
            // 
            // discipline
            // 
            this.discipline.Dock = System.Windows.Forms.DockStyle.Fill;
            this.discipline.Location = new System.Drawing.Point(124, 78);
            this.discipline.Name = "discipline";
            this.discipline.ReadOnly = true;
            this.discipline.Size = new System.Drawing.Size(115, 22);
            this.discipline.TabIndex = 18;
            // 
            // category
            // 
            this.category.Dock = System.Windows.Forms.DockStyle.Fill;
            this.category.Location = new System.Drawing.Point(366, 78);
            this.category.Name = "category";
            this.category.ReadOnly = true;
            this.category.Size = new System.Drawing.Size(117, 22);
            this.category.TabIndex = 17;
            // 
            // seg1
            // 
            this.seg1.AutoSize = true;
            this.seg1.Dock = System.Windows.Forms.DockStyle.Left;
            this.seg1.Location = new System.Drawing.Point(124, 111);
            this.seg1.Name = "seg1";
            this.seg1.Size = new System.Drawing.Size(0, 29);
            this.seg1.TabIndex = 19;
            this.seg1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // seg2
            // 
            this.seg2.AutoSize = true;
            this.seg2.Dock = System.Windows.Forms.DockStyle.Left;
            this.seg2.Location = new System.Drawing.Point(245, 111);
            this.seg2.Name = "seg2";
            this.seg2.Size = new System.Drawing.Size(0, 29);
            this.seg2.TabIndex = 20;
            this.seg2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // seg3
            // 
            this.seg3.AutoSize = true;
            this.seg3.Dock = System.Windows.Forms.DockStyle.Left;
            this.seg3.Location = new System.Drawing.Point(366, 111);
            this.seg3.Name = "seg3";
            this.seg3.Size = new System.Drawing.Size(0, 29);
            this.seg3.TabIndex = 21;
            this.seg3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // status1
            // 
            this.status1.Dock = System.Windows.Forms.DockStyle.Left;
            this.status1.Image = ((System.Drawing.Image)(resources.GetObject("status1.Image")));
            this.status1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.status1.Location = new System.Drawing.Point(124, 143);
            this.status1.Name = "status1";
            this.status1.Size = new System.Drawing.Size(115, 38);
            this.status1.TabIndex = 22;
            this.status1.Text = " ";
            this.status1.UseVisualStyleBackColor = true;
            this.status1.TextChanged += new System.EventHandler(this.status1_TextChanged);
            this.status1.Click += new System.EventHandler(this.status1_Click);
            // 
            // status2
            // 
            this.status2.Dock = System.Windows.Forms.DockStyle.Left;
            this.status2.Image = ((System.Drawing.Image)(resources.GetObject("status2.Image")));
            this.status2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.status2.Location = new System.Drawing.Point(245, 143);
            this.status2.Name = "status2";
            this.status2.Size = new System.Drawing.Size(115, 38);
            this.status2.TabIndex = 23;
            this.status2.Text = " ";
            this.status2.UseVisualStyleBackColor = true;
            this.status2.TextChanged += new System.EventHandler(this.status1_TextChanged);
            this.status2.Click += new System.EventHandler(this.status1_Click);
            // 
            // status3
            // 
            this.status3.Dock = System.Windows.Forms.DockStyle.Left;
            this.status3.Image = ((System.Drawing.Image)(resources.GetObject("status3.Image")));
            this.status3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.status3.Location = new System.Drawing.Point(366, 143);
            this.status3.Name = "status3";
            this.status3.Size = new System.Drawing.Size(116, 38);
            this.status3.TabIndex = 24;
            this.status3.Text = " ";
            this.status3.UseVisualStyleBackColor = true;
            this.status3.TextChanged += new System.EventHandler(this.status1_TextChanged);
            this.status3.Click += new System.EventHandler(this.status1_Click);
            // 
            // tv
            // 
            this.tv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tv.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tv.Location = new System.Drawing.Point(0, 0);
            this.tv.Name = "tv";
            this.tv.Size = new System.Drawing.Size(486, 387);
            this.tv.TabIndex = 2;
            this.tv.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tv_AfterSelect);
            this.tv.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tv_NodeMouseClick);
            // 
            // EventList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 571);
            this.Controls.Add(this.tv);
            this.Controls.Add(this.tableCompetition);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EventList";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Competitions";
            this.Load += new System.EventHandler(this.Competitions_Load);
            this.tableCompetition.ResumeLayout(false);
            this.tableCompetition.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableCompetition;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.TextBox place;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox date;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox skaters;
        private System.Windows.Forms.TextBox judges;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox category;
        private System.Windows.Forms.TextBox discipline;
        private System.Windows.Forms.Label seg1;
        private System.Windows.Forms.Label seg2;
        private System.Windows.Forms.Label seg3;
        private System.Windows.Forms.Button status1;
        private System.Windows.Forms.Button status2;
        private System.Windows.Forms.Button status3;
        private System.Windows.Forms.TreeView tv;
    }
}
﻿namespace Calculator
{
    partial class Deductions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.table = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.total = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ded1 = new System.Windows.Forms.NumericUpDown();
            this.lab1 = new System.Windows.Forms.Label();
            this.ded2 = new System.Windows.Forms.NumericUpDown();
            this.ded3 = new System.Windows.Forms.NumericUpDown();
            this.ded4 = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ded5 = new System.Windows.Forms.NumericUpDown();
            this.ded6 = new System.Windows.Forms.NumericUpDown();
            this.lab4 = new System.Windows.Forms.Label();
            this.lab2 = new System.Windows.Forms.Label();
            this.lab5 = new System.Windows.Forms.Label();
            this.lab3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lab6 = new System.Windows.Forms.Label();
            this.close = new System.Windows.Forms.Button();
            this.saveAndClose = new System.Windows.Forms.Button();
            this.message = new System.Windows.Forms.Label();
            this.table.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ded1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded6)).BeginInit();
            this.SuspendLayout();
            // 
            // table
            // 
            this.table.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.table.ColumnCount = 3;
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 134F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.table.Controls.Add(this.label4, 0, 4);
            this.table.Controls.Add(this.label3, 0, 3);
            this.table.Controls.Add(this.label2, 0, 2);
            this.table.Controls.Add(this.total, 1, 0);
            this.table.Controls.Add(this.label1, 0, 1);
            this.table.Controls.Add(this.ded1, 2, 1);
            this.table.Controls.Add(this.lab1, 1, 1);
            this.table.Controls.Add(this.ded2, 2, 2);
            this.table.Controls.Add(this.ded3, 2, 3);
            this.table.Controls.Add(this.ded4, 2, 4);
            this.table.Controls.Add(this.label6, 0, 5);
            this.table.Controls.Add(this.label7, 0, 6);
            this.table.Controls.Add(this.ded5, 2, 5);
            this.table.Controls.Add(this.ded6, 2, 6);
            this.table.Controls.Add(this.lab4, 1, 4);
            this.table.Controls.Add(this.lab2, 1, 2);
            this.table.Controls.Add(this.lab5, 1, 5);
            this.table.Controls.Add(this.lab3, 1, 3);
            this.table.Controls.Add(this.label5, 0, 0);
            this.table.Controls.Add(this.lab6, 1, 6);
            this.table.Location = new System.Drawing.Point(9, 11);
            this.table.Margin = new System.Windows.Forms.Padding(0);
            this.table.Name = "table";
            this.table.RowCount = 7;
            this.table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.table.Size = new System.Drawing.Size(247, 158);
            this.table.TabIndex = 0;
            this.table.Paint += new System.Windows.Forms.PaintEventHandler(this.table_Paint);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 89);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Illegal Element";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 67);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Time Violation";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 45);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Music Violation";
            // 
            // total
            // 
            this.total.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.total.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total.ForeColor = System.Drawing.Color.Blue;
            this.total.Location = new System.Drawing.Point(138, 1);
            this.total.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(52, 21);
            this.total.TabIndex = 59;
            this.total.Text = "0";
            this.total.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.total.TextChanged += new System.EventHandler(this.total_TextChanged);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Falls";
            // 
            // ded1
            // 
            this.ded1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ded1.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded1.Location = new System.Drawing.Point(195, 25);
            this.ded1.Margin = new System.Windows.Forms.Padding(2);
            this.ded1.Name = "ded1";
            this.ded1.Size = new System.Drawing.Size(47, 19);
            this.ded1.TabIndex = 6;
            this.ded1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            this.ded1.Validating += new System.ComponentModel.CancelEventHandler(this.numericUpDown1_Validating);
            // 
            // lab1
            // 
            this.lab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lab1.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab1.Location = new System.Drawing.Point(138, 23);
            this.lab1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lab1.Name = "lab1";
            this.lab1.Size = new System.Drawing.Size(52, 21);
            this.lab1.TabIndex = 55;
            this.lab1.Text = "0";
            this.lab1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lab1.Click += new System.EventHandler(this.comp1_Click);
            // 
            // ded2
            // 
            this.ded2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ded2.DecimalPlaces = 1;
            this.ded2.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded2.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.ded2.Location = new System.Drawing.Point(195, 47);
            this.ded2.Margin = new System.Windows.Forms.Padding(2);
            this.ded2.Name = "ded2";
            this.ded2.Size = new System.Drawing.Size(47, 19);
            this.ded2.TabIndex = 56;
            this.ded2.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            this.ded2.Validating += new System.ComponentModel.CancelEventHandler(this.numericUpDown1_Validating);
            // 
            // ded3
            // 
            this.ded3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ded3.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded3.Location = new System.Drawing.Point(195, 69);
            this.ded3.Margin = new System.Windows.Forms.Padding(2);
            this.ded3.Name = "ded3";
            this.ded3.Size = new System.Drawing.Size(47, 19);
            this.ded3.TabIndex = 57;
            this.ded3.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            this.ded3.Validating += new System.ComponentModel.CancelEventHandler(this.numericUpDown1_Validating);
            // 
            // ded4
            // 
            this.ded4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ded4.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded4.Location = new System.Drawing.Point(195, 91);
            this.ded4.Margin = new System.Windows.Forms.Padding(2);
            this.ded4.Name = "ded4";
            this.ded4.Size = new System.Drawing.Size(47, 19);
            this.ded4.TabIndex = 58;
            this.ded4.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            this.ded4.Validating += new System.ComponentModel.CancelEventHandler(this.numericUpDown1_Validating);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 111);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 20);
            this.label6.TabIndex = 59;
            this.label6.Text = "Costume Violation";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 133);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 20);
            this.label7.TabIndex = 60;
            this.label7.Text = "Missing Element";
            // 
            // ded5
            // 
            this.ded5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ded5.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded5.Location = new System.Drawing.Point(195, 113);
            this.ded5.Margin = new System.Windows.Forms.Padding(2);
            this.ded5.Name = "ded5";
            this.ded5.Size = new System.Drawing.Size(47, 19);
            this.ded5.TabIndex = 61;
            this.ded5.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            this.ded5.Validating += new System.ComponentModel.CancelEventHandler(this.numericUpDown1_Validating);
            // 
            // ded6
            // 
            this.ded6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ded6.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded6.Location = new System.Drawing.Point(195, 135);
            this.ded6.Margin = new System.Windows.Forms.Padding(2);
            this.ded6.Name = "ded6";
            this.ded6.Size = new System.Drawing.Size(47, 19);
            this.ded6.TabIndex = 62;
            this.ded6.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            this.ded6.Validating += new System.ComponentModel.CancelEventHandler(this.numericUpDown1_Validating);
            // 
            // lab4
            // 
            this.lab4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lab4.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab4.Location = new System.Drawing.Point(138, 89);
            this.lab4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lab4.Name = "lab4";
            this.lab4.Size = new System.Drawing.Size(52, 21);
            this.lab4.TabIndex = 65;
            this.lab4.Text = "0";
            this.lab4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab2
            // 
            this.lab2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lab2.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab2.Location = new System.Drawing.Point(138, 45);
            this.lab2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lab2.Name = "lab2";
            this.lab2.Size = new System.Drawing.Size(52, 21);
            this.lab2.TabIndex = 66;
            this.lab2.Text = "0";
            this.lab2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab5
            // 
            this.lab5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lab5.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab5.Location = new System.Drawing.Point(138, 111);
            this.lab5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lab5.Name = "lab5";
            this.lab5.Size = new System.Drawing.Size(52, 21);
            this.lab5.TabIndex = 67;
            this.lab5.Text = "0";
            this.lab5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab3
            // 
            this.lab3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lab3.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab3.Location = new System.Drawing.Point(138, 67);
            this.lab3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lab3.Name = "lab3";
            this.lab3.Size = new System.Drawing.Size(52, 21);
            this.lab3.TabIndex = 68;
            this.lab3.Text = "0";
            this.lab3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label5.Location = new System.Drawing.Point(3, 1);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(130, 21);
            this.label5.TabIndex = 4;
            this.label5.Text = "DEDUCTIONS";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lab6
            // 
            this.lab6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lab6.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab6.Location = new System.Drawing.Point(138, 133);
            this.lab6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lab6.Name = "lab6";
            this.lab6.Size = new System.Drawing.Size(52, 24);
            this.lab6.TabIndex = 64;
            this.lab6.Text = "0";
            this.lab6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // close
            // 
            this.close.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.close.Location = new System.Drawing.Point(9, 173);
            this.close.Margin = new System.Windows.Forms.Padding(2);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(70, 33);
            this.close.TabIndex = 149;
            this.close.Text = "Close";
            this.close.UseVisualStyleBackColor = true;
            this.close.Click += new System.EventHandler(this.close_Click);
            // 
            // saveAndClose
            // 
            this.saveAndClose.Enabled = false;
            this.saveAndClose.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveAndClose.Location = new System.Drawing.Point(105, 173);
            this.saveAndClose.Margin = new System.Windows.Forms.Padding(2);
            this.saveAndClose.Name = "saveAndClose";
            this.saveAndClose.Size = new System.Drawing.Size(151, 33);
            this.saveAndClose.TabIndex = 148;
            this.saveAndClose.Text = "Update deductions";
            this.saveAndClose.UseVisualStyleBackColor = true;
            this.saveAndClose.Click += new System.EventHandler(this.saveAndClose_Click);
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.message.ForeColor = System.Drawing.Color.Red;
            this.message.Location = new System.Drawing.Point(13, 208);
            this.message.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(0, 13);
            this.message.TabIndex = 150;
            // 
            // Deductions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(267, 238);
            this.ControlBox = false;
            this.Controls.Add(this.message);
            this.Controls.Add(this.close);
            this.Controls.Add(this.saveAndClose);
            this.Controls.Add(this.table);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Deductions";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Deductions";
            this.Load += new System.EventHandler(this.Components_Load);
            this.table.ResumeLayout(false);
            this.table.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ded1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel table;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button close;
        private System.Windows.Forms.Button saveAndClose;
        private System.Windows.Forms.NumericUpDown ded1;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.Label lab1;
        private System.Windows.Forms.Label total;
        private System.Windows.Forms.NumericUpDown ded2;
        private System.Windows.Forms.NumericUpDown ded3;
        private System.Windows.Forms.NumericUpDown ded4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown ded5;
        private System.Windows.Forms.NumericUpDown ded6;
        private System.Windows.Forms.Label lab6;
        private System.Windows.Forms.Label lab4;
        private System.Windows.Forms.Label lab2;
        private System.Windows.Forms.Label lab5;
        private System.Windows.Forms.Label lab3;
    }
}
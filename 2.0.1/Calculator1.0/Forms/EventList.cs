﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class EventList : Form
    {
        string listaDiscipline = "";
        SQLiteDataReader dr = null;
        public static Event currentEvent;
        public static Segment currentSegment;
        Segment seg;

        // disc = 1,2,3...
        public EventList(string disc, string nomeDisciplina)
        {
            InitializeComponent();
            listaDiscipline = disc;
            this.Text += " - " + nomeDisciplina;
        }

        private void Competitions_Load(object sender, EventArgs e)
        {
            LoadEvents();

            // inizializzo tutte le classi necessarie
            currentEvent = new Event();
            currentEvent.judges = new Judges();
            currentEvent.segment = new List<Segment>();
            currentEvent.category = new Category();
            currentEvent.discipline = new Discipline();

        }

        public void LoadEvents()
        {
            try
            {
                //tv.Nodes[0].Nodes.Clear();
                
                int numEvents = 0;
                //using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    SQLiteCommand command = Definizioni.conn.CreateCommand();
                    command.CommandText = "SELECT G.*, S.Name, C.Name, M.Name, M.Code " +
                        " FROM GaraParams G, Specialita S, Category C, Segments M" +
                        " WHERE G.ID_Specialita = S.ID_Specialita AND G.ID_Category = C.ID_Category" +
                            " AND G.ID_Segment = M.ID_Segments";
                    if (!listaDiscipline.Equals("0"))
                        command.CommandText += " AND S.ID_Specialita IN (" + listaDiscipline + ")";

                    dr = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(dr);

                    TreeNode nodeEvent = null;
                    TreeNode nodeSegme = null;
                    int countEvents = 0;
                    foreach (DataRow row in dataTable.Rows)
                    {
                        nodeEvent = new TreeNode();
                        nodeSegme = new TreeNode();
                        nodeEvent.Name = row[0].ToString(); // id_Garaparams
                        nodeEvent.Text = row[1].ToString(); // name 
                        //nodeEvent.ToolTipText = dr[1].ToString() + " - " + dr[2].ToString();

                        // se evento già inserito inserisco gli altri segmenti di gara
                        TreeNode[] tns = tv.Nodes.Find(nodeEvent.Name, true);
                        if (tns.Length > 0)
                        {
                            nodeSegme.Text = row[16].ToString(); // nome segmento
                            countEvents = tv.Nodes.Count;
                        } else
                        {
                            nodeEvent.Tag = row;
                            tv.Nodes.Add(nodeEvent);
                            nodeSegme.Text = row[16].ToString(); // nome segmento
                            countEvents = tv.Nodes.Count;
                        }
                        nodeSegme.Tag = row;
                        tv.Nodes[countEvents - 1].Nodes.Add(nodeSegme);

                        numEvents++;
                    }

                    if (numEvents > 0)
                    {
                        tv.SelectedNode = tv.Nodes[0];
                        //tv.Nodes[0].Expand();
                        tv.SelectedNode.EnsureVisible();
                        tv.Focus();
                    }

                    //tv.ExpandAll();
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex.Message, "ERROR");
            }
        }

        private void tv_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                if (e != null)
                {
                    tv.SelectedNode = e.Node;
                }
                tableCompetition.Visible = true;
                TreeNode nodoSelezionato = null;
                if (e != null) nodoSelezionato = e.Node;

                HideControls();
                DataRow dataRow = ((DataRow)nodoSelezionato.Tag);
                tableCompetition.Tag = dataRow;

                name.Text = dataRow[1].ToString(); // name
                currentEvent.name = name.Text;
                place.Text = dataRow[2].ToString(); // place
                currentEvent.place = place.Text;
                date.Text = dataRow[3].ToString(); // date
                currentEvent.date = date.Text;
                judges.Text = dataRow[12].ToString(); // num judges
                currentEvent.judges.numJudges = judges.Text;
                skaters.Text = dataRow[8].ToString(); // num part
                discipline.Text = dataRow[14].ToString(); // disc
                currentEvent.discipline.name = discipline.Text;
                currentEvent.discipline.iddiscipline = dataRow[6].ToString(); // idSpec
                category.Text = dataRow[15].ToString(); // cat
                currentEvent.category.idcategory = dataRow[7].ToString(); // idCate
                currentEvent.category.name = category.Text;
                currentEvent.idevent = dataRow[0].ToString();
                currentEvent.segment.Clear();

                int idSegment = 0;
                 
                if (nodoSelezionato.Level == 0) // evento padre
                {
                    foreach (TreeNode nodoSegmento in nodoSelezionato.Nodes)
                    {
                        CompongoEvento(dataRow, nodoSegmento, idSegment);
                    }
                }
                else if (nodoSelezionato.Level == 1) // singoli segmenti
                {
                    // recupero il nodo padre 
                    foreach (TreeNode nodoSegmento in nodoSelezionato.Parent.Nodes)
                    {
                        CompongoEvento(dataRow, nodoSegmento, idSegment);
                    }

                    dataRow = ((DataRow)nodoSelezionato.Tag);
                    idSegment = int.Parse(dataRow[5].ToString());
                    if (idSegment == 1 || idSegment == 12 || idSegment == 4)
                    {
                        ((Label)this.Controls.Find("seg2", true)[0]).ForeColor = Color.Red;
                        ((Button)this.Controls.Find("status2", true)[0]).Enabled = true;

                    } else if (idSegment == 2 || idSegment == 5 || idSegment == 6)
                    {
                        ((Label)this.Controls.Find("seg3", true)[0]).ForeColor = Color.Red;
                        ((Button)this.Controls.Find("status3", true)[0]).Enabled = true;
                    }
                    else if (idSegment == 11)
                    {
                        ((Label)this.Controls.Find("seg1", true)[0]).ForeColor = Color.Red;
                        ((Button)this.Controls.Find("status1", true)[0]).Enabled = true;
                    }
                    // indice del segmento
                    currentEvent.currentSegment = nodoSelezionato.Index + "";
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex.Message, "ERROR");
            }
        }

        private void CompongoEvento(DataRow dataRow, TreeNode nodoSegmento, int idSegment)
        {
            seg = new Segment();
            seg.idsegment = dataRow[5].ToString(); // idSeg
            seg.name = dataRow[16].ToString(); // segment
            seg.code = dataRow[17].ToString(); // code segment

            dataRow = ((DataRow)nodoSegmento.Tag);
            idSegment = int.Parse(dataRow[5].ToString());
            FillButtons(idSegment, nodoSegmento.Text, dataRow[10].ToString());

            currentEvent.segment.Add(seg);
        }

        private void FillButtons(int idSegment, string testo, string completed)
        {
            string button = "", status = "";
            if (idSegment == 1 || idSegment == 12 || idSegment == 4)
            {
                button = "status2";
                status = "seg2";
            }
            else if (idSegment == 2 || idSegment == 5 || idSegment == 6)
            {
                button = "status3";
                status = "seg3";
            }
            else if (idSegment == 11)
            {
                button = "status1";
                status = "seg1";
            }
            ((Label)this.Controls.Find(status, true)[0]).Text = testo;
            ((Button)this.Controls.Find(button, true)[0]).Text = completed;
            ((Button)this.Controls.Find(button, true)[0]).Visible = true;
            //((Button)this.Controls.Find(button, true)[0]).Tag = seg;
        }

        private void HideControls()
        {
            status1.Text = ""; status2.Text = ""; status3.Text = "";
            status1.Enabled = false; status2.Enabled = false; status3.Enabled = false;
            status1.Visible = false; status2.Visible = false; status3.Visible = false;
            seg1.Text = ""; seg2.Text = ""; seg3.Text = "";
            seg1.ForeColor = Color.Black; seg2.ForeColor = Color.Black; seg3.ForeColor = Color.Black;
        }

        private void status1_TextChanged(object sender, EventArgs e)
        {
            Button testo = (Button)sender;
            if (testo.Text.Equals("Y"))
            {
                testo.Text = "Results";
                seg.status = "Completed";
            }
            else if (testo.Text.Equals("N"))
            {
                testo.Text = "Skating Order";
                seg.status = "Not Completed";
            }
        }

        private void status1_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnGara = (Button)sender;
                currentSegment = currentEvent.segment[int.Parse(currentEvent.currentSegment)];
                
                this.LayoutMdi(System.Windows.Forms.MdiLayout.Cascade);

                SkaterList sl = new SkaterList();
                //sl.ShowDialog();
                sl.MdiParent = MainMDI.ActiveForm;
                //SkaterList.ActiveForm.LayoutMdi(System.Windows.Forms.MdiLayout.TileVertical);
                sl.Show();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex.Message, "ERROR");
            }
        }

        private void tv_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                if (e != null)
                {
                    tv.SelectedNode = e.Node;
                }
                TreeNode nodoSelezionato = null;
                if (e != null) nodoSelezionato = e.Node;

                HideControls();
                DataRow dataRow = ((DataRow)nodoSelezionato.Tag);
                tableCompetition.Tag = dataRow;

                name.Text = dataRow[1].ToString(); // name
                currentEvent.name = name.Text;
                place.Text = dataRow[2].ToString(); // place
                currentEvent.place = place.Text;
                date.Text = dataRow[3].ToString(); // date
                currentEvent.date = date.Text;
                judges.Text = dataRow[12].ToString(); // num judges
                currentEvent.judges.numJudges = judges.Text;
                skaters.Text = dataRow[8].ToString(); // num part
                discipline.Text = dataRow[14].ToString(); // disc
                currentEvent.discipline.name = discipline.Text;
                currentEvent.discipline.iddiscipline = dataRow[6].ToString(); // idSpec
                category.Text = dataRow[15].ToString(); // cat
                currentEvent.category.idcategory = dataRow[7].ToString(); // idCate
                currentEvent.category.name = category.Text;
                currentEvent.idevent = dataRow[0].ToString();
                currentEvent.segment.Clear();

                int idSegment = 0;

                if (nodoSelezionato.Level == 0) // evento padre
                {
                    foreach (TreeNode nodoSegmento in nodoSelezionato.Nodes)
                    {
                        CompongoEvento(dataRow, nodoSegmento, idSegment);
                    }
                }
                else if (nodoSelezionato.Level == 1) // singoli segmenti
                {
                    // recupero il nodo padre 
                    foreach (TreeNode nodoSegmento in nodoSelezionato.Parent.Nodes)
                    {
                        CompongoEvento(dataRow, nodoSegmento, idSegment);
                    }

                    dataRow = ((DataRow)nodoSelezionato.Tag);
                    idSegment = int.Parse(dataRow[5].ToString());
                    if (idSegment == 1 || idSegment == 12 || idSegment == 4)
                    {
                        ((Label)this.Controls.Find("seg2", true)[0]).ForeColor = Color.Red;
                        ((Button)this.Controls.Find("status2", true)[0]).Enabled = true;

                    }
                    else if (idSegment == 2 || idSegment == 5 || idSegment == 6)
                    {
                        ((Label)this.Controls.Find("seg3", true)[0]).ForeColor = Color.Red;
                        ((Button)this.Controls.Find("status3", true)[0]).Enabled = true;
                    }
                    else if (idSegment == 11)
                    {
                        ((Label)this.Controls.Find("seg1", true)[0]).ForeColor = Color.Red;
                        ((Button)this.Controls.Find("status1", true)[0]).Enabled = true;
                    }
                    // indice del segmento
                    currentEvent.currentSegment = nodoSelezionato.Index + "";
                    
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex.Message, "ERROR");
            }
        }
    }
}

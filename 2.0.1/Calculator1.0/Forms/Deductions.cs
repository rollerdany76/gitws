﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Deductions : Form
    {
        SQLiteDataReader sqdr = null;
        int numJudges = 0;

        public Deductions()
        {
            InitializeComponent();
            
        }

        private void close_Click(object sender, EventArgs e)
        {
            Dispose();
        }

//     If({ Gara1.Element} = "ded1") Then "Falls:" + CStr({ Gara1.ValueFinal}, 0)
//Else If({ Gara1.Element} = "ded2") Then "Music Violation:" + CStr({ Gara1.ValueFinal}, 1)
//Else If({ Gara1.Element} = "ded3") Then "Time Violation:" + CStr({ Gara1.ValueFinal}, 0)
//Else If({ Gara1.Element} = "ded4") Then "Illegal Elements:" + CStr({ Gara1.ValueFinal}, 0)
//Else If({ Gara1.Element} = "ded5") Then "Costume Violation:" + CStr({ Gara1.ValueFinal}, 0)
//Else If({ Gara1.Element} = "ded6") Then "Missing Elements:" + CStr({ Gara1.ValueFinal}, 0)

        private void Components_Load(object sender, EventArgs e)
        {
            try
            {
                GetDeductions();
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        // recupero deductions
        private void GetDeductions()
        {
            try
            {
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    cmd.CommandText =
                        " SELECT * FROM Gara WHERE ID_GARAPARAMS = " + EventList.currentEvent.idevent +
                        " AND ID_SEGMENT = " + EventList.currentSegment.idsegment + "" +
                        " AND NumPartecipante = '" + EventList.currentSegment.currentSkater.order +
                        "' AND Element like 'ded%'";

                    sqdr = cmd.ExecuteReader();
                    if (!sqdr.HasRows)
                    {

                    }
                    else
                    {
                        int countDed = 1;
                        while (sqdr.Read())
                        {
                            string ded = sqdr[4].ToString();
                            countDed = int.Parse(ded.Substring(3));
                            ((NumericUpDown)this.Controls.Find(ded, true)[0]).Value =
                                decimal.Parse(sqdr[7].ToString());
                            ((Label)this.Controls.Find("lab" + countDed, true)[0]).Text = "-" + 
                                sqdr[7].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        public void AggiornoDeductions()
        {
            try
            {
                decimal valueDed = 0;
                decimal[] comps = new decimal[6];
                using (SQLiteCommand commandUpdate = Definizioni.conn.CreateCommand())
                {
                    for (int j = 1; j < 7; j++)
                    {
                        valueDed = ((NumericUpDown)this.Controls.Find("ded" + j, true)[0]).Value;
                        commandUpdate.CommandText = (
                                "UPDATE Gara SET ValueFinal = '" + valueDed +
                                "' WHERE ID_GaraParams = " + EventList.currentEvent.idevent +
                                " AND ID_Segment = " + EventList.currentSegment.idsegment +
                                " AND NumPartecipante = " + EventList.currentSegment.currentSkater.order +
                                " AND Element = 'ded" + j + "'").Replace(',', '.');
                        commandUpdate.ExecuteNonQuery();
                    }
                }
                
                sqdr.Close();

                EventList.currentSegment.currentSkater.deduction = total.Text;
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        public void InseriscoDeductions()
        {
            try
            {
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "DELETE FROM Gara WHERE ID_GaraParams = @param1 AND " +
                        " NumPartecipante = @param3 AND Element = @element AND" +
                        " ID_Segment = @param2";
                    command.Parameters.AddWithValue("@param1", EventList.currentEvent.idevent);
                    command.Parameters.AddWithValue("@param2", EventList.currentSegment.idsegment);
                    command.Parameters.AddWithValue("@param3", EventList.currentSegment.currentSkater.order);
                    for (int i = 1; i < 7; i++)
                    {
                        //if (((NumericUpDown)this.Controls.Find("ded" + i, true)[0]).Value != 0)
                        {
                            command.Parameters.AddWithValue("@element", "ded" + i);
                            command.ExecuteNonQuery();
                        }
                    }

                    command.ExecuteNonQuery();
                }
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "INSERT INTO Gara(ID_GaraParams,ID_Segment,NumPartecipante,Element)" +
                                            " VALUES(@param1,@param2,@param3,@element)";
                    command.Parameters.AddWithValue("@param1", EventList.currentEvent.idevent);
                    command.Parameters.AddWithValue("@param2", EventList.currentSegment.idsegment);
                    command.Parameters.AddWithValue("@param3", EventList.currentSegment.currentSkater.order);
                    for (int i = 1; i < 7; i++)
                    {
                        if (((NumericUpDown)this.Controls.Find("ded" + i, true)[0]).Value != 0)
                        {
                            command.Parameters.AddWithValue("@element", "ded" + i);
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (SQLiteException ex)
            {
                message.Text = "***InseriscoComponents: " + ex.Message;
            }
        }

        public void CalcoloTotale()
        {
            try
            {
                decimal totaleDed = 0m;
                totaleDed =  ded1.Value + ded2.Value + ded3.Value + ded4.Value + ded5.Value + ded6.Value;
                if (totaleDed==0) total.Text = totaleDed + "";
                else total.Text = "-" + totaleDed + "";
                //total.Text = Utility.FormattaOggettoDecInString(totaleDed);
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                decimal valDec = ((NumericUpDown)sender).Value;
                int riga = table.GetRow((NumericUpDown)sender);
                if (valDec == 0)
                    ((Label)this.Controls.Find(
                        "lab" + riga, true)[0]).Text = "" + valDec;
                else
                {
                    ((Label)this.Controls.Find(
                            "lab" + riga, true)[0]).Text = "-" + valDec; 
                }
                ((Label)this.Controls.Find(
                                "lab" + riga, true)[0]).ForeColor = Color.Blue;
                CalcoloTotale();
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void numericUpDown1_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                //string val = ((NumericUpDown)sender).Text;
                //string last2digits = val.Substring(val.Length - 2);
                //if (!last2digits.Equals("50") && !last2digits.Equals("00"))
                //    e.Cancel = true;
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void table_Paint(object sender, PaintEventArgs e)
        {

        }
        
        // cambia il valore dei components
        private void total_TextChanged(object sender, EventArgs e)
        {
            if (!total.Text.Equals(EventList.currentSegment.currentSkater.deduction))
            {
                saveAndClose.Enabled = true;
            }
        }

        private void comp1_Click(object sender, EventArgs e)
        {

        }

        private void saveAndClose_Click(object sender, EventArgs e)
        {
            try
            {
                // salvo nel db
                if (!total.Text.Equals(EventList.currentSegment.currentSkater.deduction))
                {
                    InseriscoDeductions();
                    AggiornoDeductions();
                }

                Dispose();
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }
    }
}

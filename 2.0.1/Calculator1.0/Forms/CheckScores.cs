﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Calculator
{
    public partial class CheckScores : Form
    {
        int currentSkater = 1, totalSkaters = 0, numJudges = 0, idSpec = 0, idCat = 0, catElement = 0, numComboNew = 1, rigaSelezionata = 0;
        string code = "", cat, valore, bonus = "", pen = "", valoreBase = "", numCombo = "";
        object elementTag;
        DataTable dtSkaters = null, dtElements = null;
        SkaterInfo currentSkaterClass = null;
        SQLiteDataReader sqdr = null, sqdrElementNew = null;
        public static SQLiteTransaction transaction = null;
        //public static SQLiteTransaction transactionNewElement = null;

        public CheckScores()
        {
            try
            {
                InitializeComponent();
                Control.CheckForIllegalCrossThreadCalls = false;
                //CultureInfo.CurrentUICulture.NumberFormat.NumberDecimalSeparator = ".";

                System.Globalization.CultureInfo customCulture = 
                    (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
                customCulture.NumberFormat.NumberDecimalSeparator = ".";

                System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void CheckScores_Load(object sender, EventArgs e)
        {
            try
            {
                this.Text = EventList.currentEvent.name + " - " +
                    EventList.currentSegment.name.ToUpper() + " " +
                    EventList.currentEvent.category.name.ToUpper();

                
                transaction = Definizioni.conn.BeginTransaction();
                //transactionNewElement = Definizioni.conn.BeginTransaction();

                comboPerc.SelectedIndex = 0;
                numJudges = int.Parse(EventList.currentEvent.judges.numJudges);
                GetSkater(currentSkater, false);

                string disc = EventList.currentEvent.discipline.iddiscipline;
                if (disc.Equals("1") || disc.Equals("2")) GetAllElements("1,2,3");
                else if (disc.Equals("3")) GetAllElements("1,2,3,4,5,6,7,8");
                else if (disc.Equals("4") || disc.Equals("5") || disc.Equals("6")) GetAllElements("9,10,11,12,13");
                else if (disc.Equals("6")) GetAllElements("14");

                if (Definizioni.elementi != null)
                {
                    var source = new AutoCompleteStringCollection();
                    string[] names = (string[])Definizioni.elementi.ToArray(typeof(string));
                    source.AddRange(names);
                    codice.AutoCompleteCustomSource = source;
                }

                if (total.Text.Equals("0.00"))
                {
                    InseriscoGaraFinalTotal();
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        // recupero dettaglio segmento skater
        private void GetSkater(int numSkater, bool newElement)
        {
            try
            {
                rank.Text = EventList.currentSegment.currentSkater.seg_rank;
                name.Text = EventList.currentSegment.currentSkater.name.ToUpper() + "";
                if (!EventList.currentSegment.currentSkater.club.Equals(""))
                    name.Text += " (" + EventList.currentSegment.currentSkater.club.ToUpper() + ") ";
                if (!EventList.currentSegment.currentSkater.nation.Equals(""))
                    name.Text += " - " + EventList.currentSegment.currentSkater.nation.ToUpper() + "";
                name.Text += " [" + EventList.currentSegment.name + "] ";

                // recupero dettaglio segmento
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    if (!newElement)
                    {
                        dtElements = new DataTable();
                        cmd.CommandText =
                            " SELECT * FROM Gara WHERE ID_GARAPARAMS = " + EventList.currentEvent.idevent +
                            " AND ID_SEGMENT = " + EventList.currentSegment.idsegment + "" +
                            " AND NumPartecipante = '" + EventList.currentSegment.currentSkater.order +
                            "' AND ProgressivoEl not like 'comp%'";

                        sqdr = cmd.ExecuteReader();
                        if (!sqdr.HasRows)
                        {
                            message.Text = "No elements found";
                            updateElement.Enabled = false; undo.Enabled = false; //saveAndClose.Enabled = false;
                            dtElements.Load(sqdr);
                            dgElements.Columns.Clear();
                        }
                        else
                        {
                            updateElement.Enabled = true; undo.Enabled = true; saveAndClose.Enabled = true;
                            dtElements.Load(sqdr);
                            dgElements.Columns.Clear();
                        }
                    }

                    EventList.currentSegment.currentSkater.elements = dtElements;
                    dgElements.DataSource = dtElements;

                    for (int i = 0; i < 29; i++)
                    {
                        dgElements.Columns.Add("" + (i + 29), "" + (i + 29));
                        dgElements.Columns[29 + i].Visible = false;
                    }

                    // formatto le colonne
                    //dgElements.Columns[6].DefaultCellStyle.Format = "0.00";
                    //dgElements.Columns[7].DefaultCellStyle.Format = "N2";
                    //dgElements.Columns[29 + 6].DefaultCellStyle.Format = "N2";
                    //dgElements.Columns[29 + 7].DefaultCellStyle.Format = "N2";
                    dgElements.Columns[0].Visible = false;
                    dgElements.Columns[1].Visible = false;
                    dgElements.Columns[2].Visible = false;
                    dgElements.Columns[8].Visible = false;
                    for (int i = 0; i < 9; i++)
                    {
                        dgElements.Columns[9 + i].Visible = false;
                        dgElements.Columns[9 + i].HeaderText = "J" + (i + 1);
                        dgElements.Columns[18 + i].Visible = false;
                    }
                    for (int i = 0; i < numJudges; i++)
                    {
                        dgElements.Columns[9 + i].Visible = true;
                        dgElements.Columns[9 + i].DefaultCellStyle.BackColor = Color.AliceBlue;
                    }

                    dgElements.Columns[3].HeaderText = "#";
                    dgElements.Columns[4].HeaderText = "Element";
                    dgElements.Columns[5].HeaderText = "Penalty";
                    dgElements.Columns[6].HeaderText = "Base Value";
                    dgElements.Columns[7].HeaderText = "Final Value";
                    dgElements.Columns[27].HeaderText = "Bonus/*";

                    // aggiungo altra colonna con ValueFinal 
                    int count = 0, countElements = 1;
                    {
                        foreach (DataRow dr in dtElements.Rows)
                        {
                            // progressivo elemento
                            if (int.Parse(dgElements.Rows[count].Cells[8].Value.ToString()) < 2)
                            {
                                dgElements.Rows[count].Cells[3].Value = countElements;
                                dgElements.Rows[count].Cells[29 + 3].Value = countElements;
                                countElements++;
                            }
                            else
                            {
                                dgElements.Rows[count].Cells[3].Value = DBNull.Value;
                                dgElements.Rows[count].Cells[29 + 3].Value = DBNull.Value;
                            }
                            // copio tutti i valori nelle colonne aggiunte
                            for (int i = 0; i < 29; i++)
                                dgElements.Rows[count].Cells[29 + i].Value =
                                    dgElements.Rows[count].Cells[i].Value;
                            count++;
                        }
                    }
                    dgElements.Columns[28].Visible = false;
                }

                // recupero i totali
                baseValue.Text = EventList.currentSegment.currentSkater.base_value;
                elementsScore.Text = EventList.currentSegment.currentSkater.tech_score;
                comp.Text = EventList.currentSegment.currentSkater.pres_score;
                deductions.Text = EventList.currentSegment.currentSkater.deduction;
                total.Text = EventList.currentSegment.currentSkater.seg_score;
                rank.Text = EventList.currentSegment.currentSkater.seg_rank;

                for (int i = 0; i < 57; i++)
                {
                    dgElements.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                }
                //waiting.Visible = false;
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                //waiting.Visible = false;
            }
        }

        // recupero il dettaglio dell'elemento selezionato
        private void dgElements_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgElements.SelectedRows.Count > 0)
                {
                    rb4.Checked = true;
                    code = dgElements.SelectedRows[0].Cells[4].Value.ToString();
                    pen = dgElements.SelectedRows[0].Cells[5].Value.ToString();
                    bonus = dgElements.SelectedRows[0].Cells[27].Value.ToString();
                    numCombo = dgElements.SelectedRows[0].Cells[8].Value.ToString();
                    valore = dgElements.SelectedRows[0].Cells[6].Value.ToString();

                    GetElementDetails(code, pen, false);

                    elementTag = dgElements.SelectedRows[0].Tag;
                    // recupero il valore base dal database 
                    valoreBase = ((ArrayList)elementTag)[7].ToString();
                    if (catElement == 1 || catElement == 4 || catElement == 8)
                    {
                        if (numCombo.Equals("0") && pen.ToString().TrimEnd().Equals("<"))
                            valoreBase = ((ArrayList)elementTag)[8].ToString();
                        else if (numCombo.Equals("0") && pen.ToString().TrimEnd().Equals("<<"))
                            valoreBase = ((ArrayList)elementTag)[9].ToString();
                        else if (!numCombo.Equals("0") && pen.ToString().TrimEnd().Equals(""))
                            valoreBase = ((ArrayList)elementTag)[13].ToString();
                        else if (!numCombo.Equals("0") && pen.ToString().TrimEnd().Equals("<"))
                            valoreBase = ((ArrayList)elementTag)[14].ToString();
                        else if (!numCombo.Equals("0") && pen.ToString().TrimEnd().Equals("<<"))
                            valoreBase = ((ArrayList)elementTag)[15].ToString();
                        // penalty
                        if (pen.Equals("<")) rb1.Checked = true;
                        else if (pen.Equals("<<")) rb2.Checked = true;
                        else if (pen.Equals("<<<")) rb3.Checked = true;
                        else rb4.Checked = true;
                    }
                    
                    // bonus
                    if (bonus.Contains("*")) cb3.Checked = true; else cb3.Checked = false;
                    if (bonus.Contains("T")) cb4.Checked = true; else cb4.Checked = false;
                    // combo
                    if (!numCombo.Equals("0"))
                    {
                        cb1.Checked = true;
                        cb1.Text = "Combo " + numCombo;
                    }
                    else
                    {
                        cb1.Checked = false;
                        cb1.Text = "Combo ";
                    }

                    DisableControls();
                    switch (catElement)
                    {
                        case 1: // jumps
                        case 4: // throw jumps
                        case 8: // twist jumps
                            EnableControls();
                            break;
                        case 2:// spins
                        case 7://contact spins
                            cb1.Enabled = true; comboPerc.Enabled = true;
                            bomusMeno.Enabled = true; bonusPiu.Enabled = true;
                            break;
                        default:
                            break;
                    }

                    GetElementsSameCategory();

                    if (bonus.Contains("%"))
                    {
                        comboPerc.SelectedItem = (Utility.GetPercentage(
                            decimal.Parse(valore), decimal.Parse(valoreBase), 0)) + "%";
                    }
                    else
                    {
                        comboPerc.SelectedItem = "0%";
                    }

                    //
                    num.Text = "0";
                    //numComboNew = 1;
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        // recupero tutti gli elementi della stessa categoria
        private void GetElementsSameCategory()
        {
            // recupero tutti gli elementi della stessa categoria
            using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
            {
                cmd.CommandText =
                    " SELECT Name, Code, Base FROM Elements WHERE" +
                    " ID_ElementsCat = " + catElement + "";
                sqdr = cmd.ExecuteReader();
                cbElements.Items.Clear();
                while (sqdr.Read())
                {
                    cbElements.Items.Add(sqdr[1].ToString() + " - " + sqdr[0].ToString());
                }
                sqdr.Close();
                cbElements.SelectedItem = element.Text;
            }
        }

        // seleziono un nuovo elemento dalla combo e mi salvo nel Tag 
        // il DataReader dell'elemento
        private void cbElements_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                num.Text = "0";
                switch (catElement)
                {
                    case 1: // jumps
                    case 4: // throw jumps
                    case 8: // twist jumps
                        // elemento nullo
                        if (cbElements.SelectedItem.ToString().StartsWith("N"))
                        {
                            cb4.Enabled = false;
                            comboPerc.Enabled = false;
                            rb1.Enabled = false; rb2.Enabled = false;
                            rb3.Enabled = false; rb4.Enabled = false;
                        }
                        else
                        {
                            cb4.Enabled = true;
                            comboPerc.Enabled = true;
                            rb1.Enabled = true; rb2.Enabled = true;
                            rb3.Enabled = true; rb4.Enabled = true;
                        }
                        break;
                    case 2:
                    case 7:
                        // elemento nullo
                        if (cbElements.SelectedItem.ToString().StartsWith("NL"))
                        {
                            cb4.Enabled = false; 
                            comboPerc.Enabled = false;
                        }
                        else
                        {
                            comboPerc.Enabled = true;
                        }
                        break;
                    default:
                        break;
                }
                // recupero dettaglio elemento
                //using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                //{
                //    if (cbElements.SelectedIndex > 0)
                //    {
                //        cmd.CommandText =
                //            " SELECT * FROM Elements WHERE" +
                //            " Code = '" + cbElements.SelectedItem.ToString().Split('-')[0].TrimEnd() + "'";
                //        sqdrElementNew = cmd.ExecuteReader();
                //        while (sqdrElementNew.Read())
                //        {
                //            element.Tag = sqdrElementNew;
                //            element.Text = sqdrElementNew[7].ToString();
                //        }
                //        //sqdr.Close();
                //    }
                //}

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        #region Bonus trottola
        // Gestione bonus
        private void bonusPiu_Click(object sender, EventArgs e)
        {
            try
            {
                num.Text = int.Parse(num.Text) + 1 + "";
                //string valueOld = dgElements.SelectedRows[0].Cells[6].Value.ToString();
                //string finalOld = dgElements.SelectedRows[0].Cells[7].Value.ToString();
                //// aggiungo 1 sia al valore base che final
                //dgElements.SelectedRows[0].Cells[6].Value =
                //    (decimal.Parse(valueOld) + 1.0m);
                //dgElements.SelectedRows[0].Cells[7].Value =
                //    (decimal.Parse(finalOld) + 1.0m);

                //if (decimal.Parse(valueOld) < decimal.Parse(dgElements.SelectedRows[0].Cells[7].Value.ToString()))
                //{
                //    if (num.Value > 0)
                //        dgElements.SelectedRows[0].Cells[27].Value = "+";
                //    else dgElements.SelectedRows[0].Cells[27].Value = "";
                //}
                //else dgElements.SelectedRows[0].Cells[27].Value = "";

                //ImpostoNuoviColori(27);
                //ImpostoNuoviColori(6);
                //ImpostoNuoviColori(7);
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void bomusMeno_Click(object sender, EventArgs e)
        {
            try
            {
                if (int.Parse(num.Text) > 0) num.Text = int.Parse(num.Text) - 1 + "";
                //string valueOld = dgElements.SelectedRows[0].Cells[6].Value.ToString();
                //string finalOld = dgElements.SelectedRows[0].Cells[7].Value.ToString();
                //// tolgo 1 sia al valore base che final
                //if ((decimal.Parse(valueOld) - 1.0m) > 0)
                //{
                //    dgElements.SelectedRows[0].Cells[6].Value = (decimal.Parse(valueOld) - 1.0m);
                //    if (num.Value > 1) num.Value = num.Value - 1;
                //}
                //if ((decimal.Parse(valueOld) - 1.0m) > 0)
                //    dgElements.SelectedRows[0].Cells[7].Value = (decimal.Parse(finalOld) - 1.0m);

                //if (decimal.Parse(valueOld) < decimal.Parse(dgElements.SelectedRows[0].Cells[7].Value.ToString()))
                //{
                //    if (num.Value > 0)
                //        dgElements.SelectedRows[0].Cells[27].Value = "+";
                //    else dgElements.SelectedRows[0].Cells[27].Value = "";
                //}
                //ImpostoNuoviColori(27);
                //ImpostoNuoviColori(6);
                //ImpostoNuoviColori(7);
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }
        #endregion

        // update Element
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                
                message.Text = "";
                if (dgElements.SelectedRows.Count == 0) return;
                string bonusOld = dgElements.SelectedRows[0].Cells[56].Value.ToString();
                string bonusNew = dgElements.SelectedRows[0].Cells[27].Value.ToString();
                string valueOld = dgElements.SelectedRows[0].Cells[35].Value.ToString();
                string valueNew = dgElements.SelectedRows[0].Cells[6].Value.ToString();
                string finalOld = dgElements.SelectedRows[0].Cells[36].Value.ToString();
                string finalNew = "";

                if (!dgElements.SelectedRows[0].Cells[7].Value.ToString().Equals(""))
                    finalNew = dgElements.SelectedRows[0].Cells[7].Value.ToString();
                else finalNew = null;
                decimal diff = 0.00m;

                // combo
                if (cb1.Checked && dgElements.SelectedRows[0].Cells[8].Value.ToString().Equals("0"))
                {
                    cb1.Text = "Combo " + numComboNew;
                    dgElements.SelectedRows[0].Cells[8].Value = numComboNew;
                    numComboNew++;
                } else if (!cb1.Checked && !dgElements.SelectedRows[0].Cells[8].Value.ToString().Equals("0"))
                {
                    cb1.Text = "Combo ";
                    dgElements.SelectedRows[0].Cells[8].Value = 0;
                    numComboNew = 1;
                    int count = 1;
                    for (int i= 0; i< dgElements.RowCount;  i++)
                    {
                        if (dgElements.Rows[i].Cells[8].Value.ToString().Equals("0") ||
                            dgElements.Rows[i].Cells[8].Value.ToString().Equals("1"))
                        {
                            dgElements.Rows[i].Cells[3].Value = count;
                            count++;
                        }
                     }
                }

                ChangeElement();

                #region percentuale trottola, bonus
                    
                //if (comboPerc.SelectedIndex > 0)
                //{
                //    bonusNew = "%";
                        
                //    // calcolo la differenza tra valore base e finale
                //    diff = decimal.Parse(finalNew) - decimal.Parse(valueNew);
                      
                //    decimal valoreDB = decimal.Parse(((ArrayList)elementTag)[7].ToString());
                //    decimal perc = decimal.Parse(comboPerc.SelectedItem.ToString().Split('%')[0].TrimEnd());
                //    if ((valoreDB + Math.Round(((valoreDB * perc) / 100), 2)) != decimal.Parse(valueNew))
                //    {
                //        dgElements.SelectedRows[0].Cells[6].Value = valoreDB + Math.Round(((valoreDB * perc) / 100), 2);
                //    }
                //    decimal valoreFinal = decimal.Parse(dgElements.SelectedRows[0].Cells[6].Value.ToString()) + diff;
                //    if (valoreFinal != decimal.Parse(finalNew))
                //    {
                //        dgElements.SelectedRows[0].Cells[7].Value = valoreFinal;
                //    }

                //    dgElements.SelectedRows[0].Cells[27].Value = bonusNew;
                //}
                //else // tolgo percentuale
                //{
                //    if (bonusNew.Contains("%"))
                //    {
                //        bonusNew = bonusNew.Replace("%", "");

                //        diff = decimal.Parse(finalNew) - decimal.Parse(valueNew);

                //        // rimuovo la percentuale al base value e finale
                //        decimal valoreDB = decimal.Parse(((ArrayList)elementTag)[7].ToString());
                //        if ((valoreDB) != decimal.Parse(valueOld))
                //        {
                //            dgElements.SelectedRows[0].Cells[6].Value = valoreDB;
                //        }

                //        if ((valoreDB + diff) != decimal.Parse(finalNew))
                //        {
                //            dgElements.SelectedRows[0].Cells[7].Value = String.Format("{0:0.00}", (valoreDB + diff));
                //        }
                //    }
                //}

                // Bonus +1, +2, per le trottole
                if (catElement == 2 || catElement == 7)
                {
                    dgElements.SelectedRows[0].Cells[6].Value =
                        (decimal.Parse(dgElements.SelectedRows[0].Cells[6].Value.ToString()) + int.Parse(num.Text));
                    dgElements.SelectedRows[0].Cells[7].Value =
                        (decimal.Parse(dgElements.SelectedRows[0].Cells[7].Value.ToString()) + int.Parse(num.Text));

                    if (int.Parse(num.Text) > 0)
                    {
                        dgElements.SelectedRows[0].Cells[27].Value += "+"; 
                    }
                    else dgElements.SelectedRows[0].Cells[27].Value = dgElements.SelectedRows[0].Cells[27].Value.ToString().Replace("+","");
                }
                #endregion
                
                // Inserisco *
                if (cb3.Checked)
                {
                    dgElements.SelectedRows[0].Cells[27].Value = "*";
                    dgElements.SelectedRows[0].Cells[7].Value = DBNull.Value;
                }
                else // tolgo asterisco
                {
                    if (bonusNew.Contains("*"))
                    {
                        dgElements.SelectedRows[0].Cells[27].Value = bonusNew.Replace("*", "");
                        dgElements.SelectedRows[0].Cells[7].Value = decimal.Parse(dgElements.SelectedRows[0].Cells[7].Value.ToString());
                    }
                }

                // cambio colori celle 6,7,27
                if (decimal.Parse(dgElements.SelectedRows[0].Cells[6].Value.ToString()) != decimal.Parse(valueOld))
                    ImpostoNuoviColori(6);
                else RipristinoColori(6);
                if (dgElements.SelectedRows[0].Cells[7].Value == DBNull.Value || finalOld.Equals(""))
                {
                    if (dgElements.SelectedRows[0].Cells[7].Value.ToString() != finalOld)
                        ImpostoNuoviColori(7);
                    else RipristinoColori(7);
                }
                else
                {
                    if (decimal.Parse(dgElements.SelectedRows[0].Cells[7].Value.ToString()) != decimal.Parse(finalOld))
                        ImpostoNuoviColori(7);
                    else RipristinoColori(7);
                }
                if (dgElements.SelectedRows[0].Cells[27].Value.ToString() != bonusOld)
                    ImpostoNuoviColori(27);
                else RipristinoColori(27);

                // aggiorno l'elemento sul database
                UpdateTableGara(false);
                // aggiorno i totali
                UpdateTotals();

                NuovaClassificaSegmento();

                // aggiorno GaraFinal senza fare commit
                UpdateTableGaraFinal();

                dgElements.SelectedRows[0].Selected = true;
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        // recupero i dettagli dell'elemento selezionato dalla combo o dal datagrid
        // elCode = "2Ax"
        // penalty = "<<<", "" in tutti gli altri casi
        private void GetElementDetails(string elCode, string penalty, bool newElement)
        {
            try
            {
                message.Text = "";
                ArrayList alValues = new ArrayList();
                string numCombo = "";
                string descElement = "";

                if (!newElement)
                    numCombo = dgElements.SelectedRows[0].Cells[8].Value.ToString();

                SQLiteCommand cmd = Definizioni.conn.CreateCommand();
                if (penalty.Equals("<<<"))
                {
                    // recupero prima il CODICE dell'elemento intero 
                    cmd.CommandText =
                    " SELECT * FROM Elements, ElementsCat WHERE Elements.Code = '" + elCode + "'" +
                    " AND Elements.ID_ElementsCat = ElementsCat.ID_ElementsCat";
                    sqdr = cmd.ExecuteReader();
                    while (sqdr.Read())
                    {
                        descElement = sqdr[1].ToString();
                    }
                    cmd = Definizioni.conn.CreateCommand();
                    if (!elCode.StartsWith("1"))
                    {
                        int rotazioneMenoUno = int.Parse(elCode.Substring(0, 1)) - 1;
                        elCode = rotazioneMenoUno + elCode.Substring(1);
                    }
                }
                cmd.CommandText =
                    " SELECT * FROM Elements, ElementsCat WHERE Elements.Code = '" + elCode + "'" +
                    " AND Elements.ID_ElementsCat = ElementsCat.ID_ElementsCat";
                sqdr = cmd.ExecuteReader();
                
                while (sqdr.Read())
                {
                    catElement = int.Parse(sqdr[17].ToString());
                    label2.Text = catElement + "";
                    if (!penalty.Equals("<<<"))
                    {
                        descElement = sqdr[1].ToString();
                    }
                    if (!newElement)
                        element.Text = dgElements.SelectedRows[0].Cells[4].Value + " - " + descElement;
                    for (int i=0; i<sqdr.FieldCount; i++)
                    {
                        alValues.Add(sqdr[i].ToString());
                    }

                    if (!newElement)
                    {
                        if (catElement == 1)
                        {
                            // solo jump
                            if (numCombo.Equals("0"))
                                gb.Text = "Solo " + sqdr[20].ToString();
                            else
                                gb.Text = "Combo " + sqdr[20].ToString() + " " +
                                    numCombo;

                        }
                        else if (catElement == 2 || catElement == 7)
                        {
                            // solo spin
                            if (numCombo.Equals("0"))
                                gb.Text = "Solo " + sqdr[20].ToString();
                            else
                                gb.Text = "Combo " + sqdr[20].ToString() + " " +
                                    numCombo;
                        }
                        else
                        {
                            gb.Text = sqdr[20].ToString();
                        }
                    }
                }

                if (!newElement)
                    dgElements.Rows[dgElements.SelectedRows[0].Index].Tag = alValues;
                else
                    elementTag = alValues;

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void ChangeElement()
        {
            try
            {
                // codice dell'elemento
                string elCodeOld = dgElements.SelectedRows[0].Cells[33].Value.ToString();
                string elCode = cbElements.SelectedItem.ToString().Split('-')[0].TrimEnd();
                string elCodNew = elCode;
                string elBase = "";
                string elUnde = "";
                string elHalf = "";
                string elBaseCombo = "";
                string elUndeCombo = "";
                string elHalfCombo = "";
                string bonusOld = dgElements.SelectedRows[0].Cells[27].Value.ToString();
                string bonusNew = dgElements.SelectedRows[0].Cells[27 + 29].Value.ToString();
                string numCombo = dgElements.SelectedRows[0].Cells[8].Value.ToString();
                object valueBase = dgElements.SelectedRows[0].Cells[6].Value;
                object valueFinal = dgElements.SelectedRows[0].Cells[7].Value;
                object valuePenalty = dgElements.SelectedRows[0].Cells[5].Value;
                string finalNew = dgElements.SelectedRows[0].Cells[7 + 29].Value.ToString();
                string valueNew = dgElements.SelectedRows[0].Cells[6 + 29].Value.ToString();
                string penalOld = dgElements.SelectedRows[0].Cells[5].Value.ToString();
                string penalNew = dgElements.SelectedRows[0].Cells[5 + 29].Value.ToString();
                int percentualeDaAggiungere = 0;
                decimal diff = 0.00m;

                // calcolo la differenza tra valore base e finale
                if (!valueFinal.ToString().Equals(""))
                    diff = decimal.Parse(valueFinal.ToString()) -
                               decimal.Parse(valueBase.ToString());

                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    if (rb3.Checked)
                    {
                        if (elCode.StartsWith("1"))
                        {
                            dgElements.SelectedRows[0].Cells[6].Value = String.Format("{0:0.00}", 0.00m);
                            valuePenalty = "<<<";
                            return;
                        } else
                        {
                            
                            int rotazioneMenoUno = int.Parse(elCode.Substring(0, 1)) - 1;
                            elCode = rotazioneMenoUno + elCode.Substring(1);
                        }
                    }
                    cmd.CommandText =
                        " SELECT Base, BaseDown, BaseHalf, ElementsCombo, ElementsComboDown," + 
                        " ElementsComboHalf FROM Elements WHERE Code = '" + elCode + "'";
                    sqdr = cmd.ExecuteReader();
                    while (sqdr.Read())
                    {
                        elBase = String.Format("{0:0.00}", decimal.Parse(sqdr[0].ToString()));
                        elUnde = String.Format("{0:0.00}", decimal.Parse(sqdr[1].ToString()));
                        elHalf = String.Format("{0:0.00}", decimal.Parse(sqdr[2].ToString()));
                        elBaseCombo = String.Format("{0:0.00}", decimal.Parse(sqdr[3].ToString()));
                        elUndeCombo = String.Format("{0:0.00}", decimal.Parse(sqdr[4].ToString()));
                        elHalfCombo = String.Format("{0:0.00}", decimal.Parse(sqdr[5].ToString()));
                    }
                    sqdr.Close();
                }

                if (catElement == 1 || catElement == 4 || catElement == 8) // salti
                {
                    if (rb4.Checked) // completed
                    {
                        if (numCombo.Equals("0") && !cb1.Checked) valueBase = String.Format("{0:0.00}", decimal.Parse(elBase));
                        else valueBase = String.Format("{0:0.00}", decimal.Parse(elBaseCombo));
                        valuePenalty = "";
                    }
                    else if (rb1.Checked) // <
                    {
                        if (numCombo.Equals("0") && !cb1.Checked) valueBase = String.Format("{0:0.00}", decimal.Parse(elUnde));
                        else valueBase = String.Format("{0:0.00}", decimal.Parse(elUndeCombo));
                        valuePenalty = "<";
                    }
                    else if (rb2.Checked) // <<
                    {
                        if (numCombo.Equals("0") && !cb1.Checked) valueBase = String.Format("{0:0.00}", decimal.Parse(elHalf));
                        else valueBase = String.Format("{0:0.00}", decimal.Parse(elHalfCombo));
                        valuePenalty = "<<";
                    }
                    else if (rb3.Checked) // <<<
                    {
                        if (numCombo.Equals("0") && !cb1.Checked) valueBase = String.Format("{0:0.00}", decimal.Parse(elBase));
                        else valueBase = String.Format("{0:0.00}", decimal.Parse(elBaseCombo));
                        valuePenalty = "<<<";
                    }
                } 
                else // tutti gli altri elementi
                {
                    valueBase = String.Format("{0:0.00}", decimal.Parse(elBase));
                }

                GetElementDetails(elCodNew, valuePenalty.ToString(), false);
                elementTag = dgElements.SelectedRows[0].Tag;

                if (valueFinal.ToString().Equals("") || rb3.Checked || penalOld == "<<<" 
                    || elCodeOld != elCode) // elemento inserito in gara con *, oppure cambio elemento nella combo 
                {
                    // calcolo il valueFinal 
                    valueFinal = CalcoloValueFinal(valueBase, dgElements.SelectedRows[0]);
                    diff = decimal.Parse(valueFinal.ToString()) -
                               decimal.Parse(valueBase.ToString());
                }

                // il valueBase è il valore corrente
                decimal decValueBase = decimal.Parse(valueBase.ToString());
                decimal decValueFinal = 0.00m;

                #region percentuale, Time
                if (comboPerc.SelectedIndex > 0)
                {
                    if (!bonusOld.Contains("%"))
                    {
                        dgElements.SelectedRows[0].Cells[27].Value += "%";
                    }
                    percentualeDaAggiungere = int.Parse(comboPerc.SelectedItem.ToString().Split('%')[0].TrimEnd());
                    //decValueFinal = decimal.Parse(dgElements.SelectedRows[0].Cells[6].Value.ToString()) + diff;
                }
                
                // Inserisco T (solo salti)
                if (cb4.Checked)
                {
                    if (!bonusOld.Contains("T"))
                        dgElements.SelectedRows[0].Cells[27].Value += "T";
                    percentualeDaAggiungere += 10;

                }

                #endregion

                dgElements.SelectedRows[0].Cells[6].Value = decValueBase + Math.Round(((decValueBase * percentualeDaAggiungere) / 100), 2);
                decimal valoreFinal = decimal.Parse(dgElements.SelectedRows[0].Cells[6].Value.ToString()) + diff;
                dgElements.SelectedRows[0].Cells[7].Value = valoreFinal; 

                // Element code
                if (elCodNew != dgElements.SelectedRows[0].Cells[4].Value.ToString()) ImpostoNuoviColori(4);
                else RipristinoColori(4);
                dgElements.SelectedRows[0].Cells[4].Value = elCodNew;

                // penalty (solo salti)
                dgElements.SelectedRows[0].Cells[5].Value = valuePenalty.ToString().TrimEnd();
                if (!valuePenalty.ToString().TrimEnd().Equals(penalOld.TrimEnd())) ImpostoNuoviColori(5);
                else RipristinoColori(5);

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        #region Aggiorno Totali, Calcolo ValueFinal, Aggiorno Gara
        private void UpdateTotals()
        {
            decimal newElementBaseScore = 0.00m;
            decimal newElementScore = 0.00m;
            decimal newTotalScore = 0.00m;
            decimal newDeductionsScore = 0.00m;

            baseValue.BackColor = Color.Gainsboro; baseValue.ForeColor = Color.Black;
            elementsScore.BackColor = Color.Gainsboro; elementsScore.ForeColor = Color.Black;
            total.BackColor = Color.Gainsboro; total.ForeColor = Color.Black;

            // base value
            for (int i = 0; i < dgElements.Rows.Count; ++i)
            {
                // non sommo l'elemento con asterisco
                if (!dgElements.Rows[i].Cells[27].Value.Equals("*"))
                    newElementBaseScore += Convert.ToDecimal(dgElements.Rows[i].Cells[6].Value);
            }
            baseValue.Text = Utility.FormattaOggettoDecInString(newElementBaseScore.ToString());
            //baseValue.Text = newElementBaseScore.ToString();
            if (Utility.FormattaOggettoDecInString(newElementBaseScore) != EventList.currentSegment.currentSkater.base_value)
            //if (newElementBaseScore.ToString() != EventList.currentSegment.currentSkater.base_value)
            {
                baseValue.BackColor = Color.Yellow; baseValue.ForeColor = Color.Red;
            } else
            {
                baseValue.BackColor = Color.Gainsboro; baseValue.ForeColor = Color.Black;
            }

            for (int i = 0; i < dgElements.Rows.Count; ++i)
            {
                if (!dgElements.Rows[i].Cells[7].Value.Equals(DBNull.Value))
                {
                    if (!dgElements.Rows[i].Cells[27].Value.Equals("*"))
                        newElementScore += Convert.ToDecimal(dgElements.Rows[i].Cells[7].Value);
                }      
            }
            elementsScore.Text = Utility.FormattaOggettoDecInString(newElementScore.ToString());
            if (Utility.FormattaOggettoDecInString(newElementScore) != EventList.currentSegment.currentSkater.tech_score)
            //elementsScore.Text = newElementScore.ToString();
            //if (newElementScore.ToString() != EventList.currentSegment.currentSkater.tech_score)
            {
                elementsScore.BackColor = Color.Yellow; elementsScore.ForeColor = Color.Red;
            } else
            {
                elementsScore.BackColor = Color.Gainsboro; elementsScore.ForeColor = Color.Black;
            }

            // total
            //if (Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator==",")
            //{

            //} else
            //if (Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator == ",")
            //{

            //}
            newTotalScore += decimal.Parse(elementsScore.Text, System.Globalization.NumberStyles.Any);
            if (!comp.Text.Equals(""))
                newTotalScore += decimal.Parse(comp.Text, System.Globalization.NumberStyles.Any);
            if (!deductions.Text.Equals(""))
                newTotalScore += decimal.Parse(deductions.Text, System.Globalization.NumberStyles.Any);
            total.Text = Utility.FormattaOggettoDecInString(newTotalScore);
            if (Utility.FormattaOggettoDecInString(newTotalScore) != EventList.currentSegment.currentSkater.seg_score)
            //total.Text = newTotalScore.ToString();
            //if (newTotalScore.ToString() != EventList.currentSegment.currentSkater.seg_score)
            {
                total.BackColor = Color.Yellow; total.ForeColor = Color.Red;
            } else
            {
                total.BackColor = Color.Gainsboro; total.ForeColor = Color.Black;
            }
        }

        private string CalcoloValueFinal(object valueBase, DataGridViewRow row)
        {
            decimal decValueFinal = 0.00m;
            decimal[] elems = new decimal[numJudges];

            // se fino a 3 giudici faccio la media
            if (numJudges < 4)
            {
                for (int i = 0; i < numJudges; i++)
                {
                    // 4,5,6,10,11,12
                    if (row.Cells[38 + i].Value.Equals("+3"))
                        elems[i] = Decimal.Parse(((ArrayList)elementTag)[4].ToString());
                    else if (row.Cells[38 + i].Value.Equals("+2"))
                        elems[i] = Decimal.Parse(((ArrayList)elementTag)[5].ToString());
                    else if (row.Cells[38 + i].Value.Equals("+1"))
                        elems[i] = Decimal.Parse(((ArrayList)elementTag)[6].ToString());
                    else if (row.Cells[38 + i].Value.Equals("0"))
                        elems[i] = 0.00m;
                    else if (row.Cells[38 + i].Value.Equals("-1"))
                        elems[i] = Decimal.Parse(((ArrayList)elementTag)[10].ToString());
                    else if (row.Cells[38 + i].Value.Equals("-2"))
                        elems[i] = Decimal.Parse(((ArrayList)elementTag)[11].ToString());
                    else if (row.Cells[38 + i].Value.Equals("-3"))
                        elems[i] = Decimal.Parse(((ArrayList)elementTag)[12].ToString());
                    decValueFinal += elems[i];
                    if (elems[i] == 0.00m) row.Cells[18 + i].Value = 0;
                    else row.Cells[18 + i].Value = elems[i];
                }
                decValueFinal = decValueFinal / numJudges;
            }
            else  //da 4 giudici in su
            {
                for (int i = 0; i < numJudges; i++)
                {
                    // 4,5,6,10,11,12
                    if (row.Cells[38 + i].Value.Equals("+3"))
                        elems[i] = Decimal.Parse(((ArrayList)elementTag)[4].ToString());
                    else if (row.Cells[38 + i].Value.Equals("+2"))
                        elems[i] = Decimal.Parse(((ArrayList)elementTag)[5].ToString());
                    else if (row.Cells[38 + i].Value.Equals("+1"))
                        elems[i] = Decimal.Parse(((ArrayList)elementTag)[6].ToString());
                    else if (row.Cells[38 + i].Value.Equals("0"))
                        elems[i] = 0.00m;
                    else if (row.Cells[38 + i].Value.Equals("-1"))
                        elems[i] = Decimal.Parse(((ArrayList)elementTag)[10].ToString());
                    else if (row.Cells[38 + i].Value.Equals("-2"))
                        elems[i] = Decimal.Parse(((ArrayList)elementTag)[11].ToString());
                    else if (row.Cells[38 + i].Value.Equals("-3"))
                        elems[i] = Decimal.Parse(((ArrayList)elementTag)[12].ToString());
                    decValueFinal += elems[i];
                    if (elems[i]==0.00m) row.Cells[18 + i].Value = 0;
                    else row.Cells[18 + i].Value = elems[i];
                }
                decimal min = Utility.Lowest(elems); // minimo
                decimal max = Utility.Highest(elems); // max
                decValueFinal = decValueFinal - min - max;
                decValueFinal = Decimal.Round(decValueFinal / (numJudges - 2), 2); // aggiungo il valore base 
            }

            return (Decimal.Parse(valueBase.ToString()) + decValueFinal).ToString();
        }

        private void NuovaClassificaSegmento()
        {
            int position = 1;
            using (SQLiteCommand command = Definizioni.conn.CreateCommand())
            {
                command.CommandText =  "SELECT Total FROM GaraFinal WHERE ID_GaraParams = " + EventList.currentEvent.idevent;
                command.CommandText += " AND ID_Segment = " + EventList.currentSegment.idsegment + " ";
                command.CommandText += " AND Total > '" + total.Text.Replace(',', '.') + "' ";
                command.CommandText += " AND ID_Atleta <> " + EventList.currentSegment.currentSkater.idskater + " ";

                // gestione pari meriti - 13/03/2018
                if (EventList.currentSegment.idsegment.Equals("1") || 
                    EventList.currentSegment.idsegment.Equals("4")) // SP e SD
                    command.CommandText += "ORDER BY Total DESC, FinalTech DESC"; // vince il tecnico più alto
                else
                    command.CommandText += "ORDER BY Total DESC, BaseArtistic DESC"; // vincono i components più alti

                SQLiteDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    position++;
                }

                if (!EventList.currentSegment.currentSkater.seg_rank.Equals(""))
                {
                    if (position == int.Parse(EventList.currentSegment.currentSkater.seg_rank))
                        rankimage.Image = RollartReview.Properties.Resources.equal;
                    else if (position > int.Parse(EventList.currentSegment.currentSkater.seg_rank))
                        rankimage.Image = RollartReview.Properties.Resources.down;
                    else if (position < int.Parse(EventList.currentSegment.currentSkater.seg_rank))
                        rankimage.Image = RollartReview.Properties.Resources.up;
                    rankimage.Text = "(" + EventList.currentSegment.currentSkater.seg_rank + ")";
                    rankimage.ToolTipText = "Previous rank before updating: " +
                        EventList.currentSegment.currentSkater.seg_rank;
                    rank.Text = position + "";
                } else
                {
                    rank.Text = position + "";
                    EventList.currentSegment.currentSkater.seg_rank = position + "";
                }
            }
        }

        public void UpdateTableGara(bool delete)
        {
            try
            {
                int numLVElem = 0;
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.Transaction = transaction;

                    if (delete) // elimino ultimo record
                    {
                        numLVElem = dgElements.Rows[dgElements.RowCount -1].Index;
                        command.CommandText = "DELETE FROM Gara WHERE ID_GaraParams = @param1 AND " +
                            " NumPartecipante = @param2 AND ProgressivoEl = @param3 AND" +
                            " ID_Segment = @param4";
                        command.Parameters.AddWithValue("@param1", EventList.currentEvent.idevent);
                        command.Parameters.AddWithValue("@param2", EventList.currentSegment.currentSkater.order);
                        command.Parameters.AddWithValue("@param3", "" + (numLVElem + 1));
                        command.Parameters.AddWithValue("@param4", EventList.currentSegment.idsegment);
                        command.ExecuteNonQuery();
                        dgElements.Rows.RemoveAt(dgElements.Rows[dgElements.RowCount - 1].Index);

                    }
                    else // aggiorno record perchè ho cliccato su "edit"
                    {
                        numLVElem = dgElements.SelectedRows[0].Index;
                        command.CommandText = "UPDATE Gara SET Element=@param5, Value=@param6, pen=@param7, Bonus=@param8, ValueFinal = @param9 " +
                            "WHERE ID_GaraParams = @param1 AND ID_Segment = @param2 AND NumPartecipante = @param3 AND ProgressivoEl = @param4";
                        command.Parameters.AddWithValue("@param1", EventList.currentEvent.idevent);
                        command.Parameters.AddWithValue("@param2", EventList.currentSegment.idsegment);
                        command.Parameters.AddWithValue("@param3", EventList.currentSegment.currentSkater.order);
                        command.Parameters.AddWithValue("@param4", "" + (numLVElem + 1));
                        command.Parameters.AddWithValue("@param5", dgElements.SelectedRows[0].Cells[4].Value.ToString().Replace("<", "").TrimEnd()); // nome el
                        command.Parameters.AddWithValue("@param6", dgElements.SelectedRows[0].Cells[6].Value.ToString().Replace(',', '.')); // value
                        command.Parameters.AddWithValue("@param7", dgElements.SelectedRows[0].Cells[5].Value.ToString().TrimEnd()); // pen
                        command.Parameters.AddWithValue("@param8", dgElements.SelectedRows[0].Cells[27].Value.ToString().TrimEnd()); // bonus
                        if (dgElements.SelectedRows[0].Cells[7].Value == DBNull.Value)
                            command.Parameters.AddWithValue("@param9", DBNull.Value); // valuefinal
                        else command.Parameters.AddWithValue("@param9", dgElements.SelectedRows[0].Cells[7].Value.ToString().Replace(',', '.')); // valuefinal
                        command.ExecuteNonQuery();
                    }
                    
                }
            }
            catch (SQLiteException ex)
            {
                message.Text = "***UpdateTableGara: " + ex.Message;
            }
        }

        public void InsertTableGara(DataGridViewRow dgvr)
        {
            try
            {
                int numLVElem = dgElements.Rows.Count;
                int count = 1;
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.Transaction = transaction;
                    command.CommandText = "INSERT INTO Gara VALUES (";
                    foreach (DataGridViewCell cella in dgvr.Cells)
                    {
                        command.CommandText += "@param" + count + ",";
                        if (count == 29) break;
                        count++;
                    }
                    command.CommandText = command.CommandText.TrimEnd(',');
                    command.CommandText += ")";

                    count = 1;
                    foreach (DataGridViewCell cella in dgvr.Cells)
                    {
                        if (count == 4)
                            command.Parameters.AddWithValue("@param" + count, 
                                dgElements.RowCount);
                        else if (cella.Value==DBNull.Value)
                            command.Parameters.AddWithValue("@param" + count, 0);
                        else
                            command.Parameters.AddWithValue("@param" + count,
                                    cella.Value);
                        if (count == 29) break;
                        count++;
                    }

                    command.ExecuteNonQuery();
                }
            }
            catch (SQLiteException ex)
            {
                message.Text = "***UpdateTableGara: " + ex.Message;
            }
        }

        public void DeleteFromTableGara(DataGridViewRow dgvr)
        {
            try
            {
                int numLVElem = dgElements.Rows.Count;
                int count = 1;
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.Transaction = transaction;
                    command.CommandText = "DELETE FROM Gara VALUES (";
                    foreach (DataGridViewCell cella in dgvr.Cells)
                    {
                        command.CommandText += "@param" + count + ",";
                        if (count == 29) break;
                        count++;
                    }
                    command.CommandText = command.CommandText.TrimEnd(',');
                    command.CommandText += ")";

                    count = 1;
                    foreach (DataGridViewCell cella in dgvr.Cells)
                    {
                        if (count == 4)
                            command.Parameters.AddWithValue("@param" + count,
                                dgElements.RowCount);
                        else if (cella.Value == DBNull.Value)
                            command.Parameters.AddWithValue("@param" + count, 0);
                        else
                            command.Parameters.AddWithValue("@param" + count,
                                    cella.Value);
                        if (count == 29) break;
                        count++;
                    }

                    command.ExecuteNonQuery();
                }
            }
            catch (SQLiteException ex)
            {
                message.Text = "***UpdateTableGara: " + ex.Message;
            }
        }

        public void UpdateTableGaraFinal()
        {
            try
            {
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.Transaction = transaction;

                    command.CommandText = "UPDATE GaraFinal SET BaseTech = @param4, FinalTech = @param5, BaseArtistic = @param6, Deductions=@param7, Total = @param8 " +
                        "WHERE ID_GaraParams = @param1 AND ID_Segment = @param2 AND NumPartecipante = @param3";
                    command.Parameters.AddWithValue("@param1", EventList.currentEvent.idevent);
                    command.Parameters.AddWithValue("@param2", EventList.currentSegment.idsegment);
                    command.Parameters.AddWithValue("@param3", EventList.currentSegment.currentSkater.order);
                    command.Parameters.AddWithValue("@param4", baseValue.Text.Replace(",", ".").TrimEnd()); // base value
                    command.Parameters.AddWithValue("@param5", elementsScore.Text.Replace(",", ".").TrimEnd()); // final tech
                    command.Parameters.AddWithValue("@param6", comp.Text.Replace(",", ".").TrimEnd()); // baseartistic
                    command.Parameters.AddWithValue("@param7", deductions.Text.Replace(",", ".").TrimEnd()); // deductions
                    command.Parameters.AddWithValue("@param8", total.Text.Replace(",", ".").TrimEnd()); // total
                    //command.Parameters.AddWithValue("@param9", rank.Text.Replace(",", ".").TrimEnd()); // rank

                    command.ExecuteNonQuery();
                }
            }
            catch (SQLiteException ex)
            {
                message.Text = "***UpdateTableGaraFinal: " + ex.Message;
            }
        }

        public void CalcoloClassificaSegmento()
        {
            try
            {
                int classifica = 1;
                SQLiteCommand commandUpdate = Definizioni.conn.CreateCommand();
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT NumPartecipante FROM GaraFinal WHERE ID_GaraParams = " + EventList.currentEvent.idevent
                        + " AND ID_Segment = " + EventList.currentSegment.idsegment + " ORDER BY Total DESC";

                    // gestione pari meriti - 13/03/2018
                    if (EventList.currentSegment.idsegment.Equals("1") || EventList.currentSegment.idsegment.Equals("4")) // SP e SD
                        command.CommandText += ", FinalTech DESC"; // vince il tecnico più alto
                    else
                        command.CommandText += ", BaseArtistic DESC"; // vincono i components più alti

                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        commandUpdate.CommandText = "UPDATE GaraFinal SET Position = " +
                            "@param1 WHERE ID_GaraParams = " + EventList.currentEvent.idevent +
                            " AND ID_Segment = " + EventList.currentSegment.idsegment + " AND NumPartecipante = @param2";
                        commandUpdate.Parameters.AddWithValue("@param1", classifica); // position
                        commandUpdate.Parameters.AddWithValue("@param2", dr[0].ToString()); // num partecipante
                        commandUpdate.ExecuteNonQuery();
                        classifica++;
                    }

                    dr.Close();
                }
                //// classifica parziale della singola gara
                //using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                //{
                //    command.CommandText = "SELECT Position FROM GaraFinal WHERE ID_GaraParams = "
                //        + EventList.currentEvent.idevent + " AND ID_Segment = " + EventList.currentSegment.idsegment
                //        + " AND NumPartecipante = " + EventList.currentSegment.currentSkater.order;
                //    SQLiteDataReader dr = command.ExecuteReader();
                //    while (dr.Read())
                //    {
                //        classifica = int.Parse(dr[0].ToString());
                //    }
                //    dr.Close();
                //}

                return;
            }
            catch (SQLiteException ex)
            {
                message.Text = "CalcoloClassificaSegmento: " + ex.Message;
            }
            return;
        }

        public void CalcoloClassificaGeneraleParziale()
        {
            try
            {
                // recupero l'ID_Atleta, Name Atleta, Ordine entrata, totale e posizione gara in corso 
                int position = 1, posSegment = 1;
                string nameAtleta = "", total = "";
                SQLiteCommand commandUpdate = Definizioni.conn.CreateCommand();

                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT A.[ID_Atleta], C.[Name], SUM(Total) as total " +
                                         " FROM GaraFinal A, Athletes C" +
                                         " WHERE A.ID_GaraParams = " + EventList.currentEvent.idevent +
                                         " AND A.[ID_Atleta] = " + EventList.currentSegment.currentSkater.idskater +
                                         " AND A.[ID_Atleta] = C.[ID_Atleta] " +
                                         " GROUP BY C.[ID_Atleta]  ORDER BY total DESC, A.ID_GaraParams";

                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        nameAtleta = dr[1].ToString();
                        total = dr[2].ToString().ToString().Replace(",", ".");

                        using (SQLiteCommand command2 = Definizioni.conn.CreateCommand())
                        {
                            commandUpdate.CommandText = "UPDATE GaraTotal SET TotGara = '" + total + "'";
                            commandUpdate.CommandText += " WHERE ID_GaraParams = " + EventList.currentEvent.idevent + " AND ID_Atleta = " + EventList.currentSegment.currentSkater.idskater;
                            commandUpdate.ExecuteNonQuery();
                        }
                    }
                    dr.Close();
                }

                // aggiorno posizione del segmento
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT ID_Atleta FROM GaraFinal WHERE ID_GaraParams = "
                        + EventList.currentEvent.idevent + " AND ID_Segment = " + EventList.currentSegment.idsegment
                        + " ORDER BY Total DESC";
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        if (EventList.currentSegment.idsegment.Equals("1") || EventList.currentSegment.idsegment.Equals("4") || EventList.currentSegment.idsegment.Equals("11") || EventList.currentSegment.idsegment.Equals("12")) // CD, SP e SD - PosSegment1
                        {
                            commandUpdate.CommandText = "UPDATE GaraTotal SET PosSegment1 = " + posSegment +
                            " WHERE ID_GaraParams = " + EventList.currentEvent.idevent +
                            " AND ID_Atleta = " + dr[0].ToString();
                        }
                        else if (EventList.currentSegment.idsegment.Equals("2") || EventList.currentSegment.idsegment.Equals("5") || EventList.currentSegment.idsegment.Equals("6")) // LP, FD, Synchro - PosSegment2
                        {
                            commandUpdate.CommandText = "UPDATE GaraTotal SET PosSegment2 = " + posSegment +
                            " WHERE ID_GaraParams = " + EventList.currentEvent.idevent +
                            " AND ID_Atleta = " + dr[0].ToString();
                        }

                        commandUpdate.ExecuteNonQuery();
                        posSegment++;
                    }

                    dr.Close();
                }

                // aggiorno posizione generale totale
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT ID_Atleta FROM GaraTotal WHERE ID_GaraParams = " + EventList.currentEvent.idevent;

                    // recupero solo la classifica di chi ha eseguito fino a quel momento
                    // modifica del 25/06/2018
                    // Per Obbl danza, style dance, short program e synchro non cambio nulla
                    // Per LP, FD
                    // *** modifica del 17/09/2018 ***//
                    if (EventList.currentSegment.idsegment.Equals("2") || EventList.currentSegment.idsegment.Equals("5"))
                        command.CommandText += " AND PosSegment2 <> 0";

                    // gestione parimeriti - 13/03/2018
                    command.CommandText += " ORDER BY TotGara DESC, PosSegment2 ASC"; // vince il LP/FD più alto

                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        commandUpdate.CommandText = "UPDATE GaraTotal SET Position = " +
                            "@param1 WHERE ID_GaraParams = " + EventList.currentEvent.idevent +
                            " AND ID_Atleta = @param2";

                        if (EventList.currentSegment.idsegment.Equals("11") || EventList.currentSegment.idsegment.Equals("12")) // per gli obbliga
                        {
                            commandUpdate.CommandText = "UPDATE GaraTotal SET Position = " +
                            "@param1, PosSegment1 = @param3 WHERE ID_GaraParams = " + EventList.currentEvent.idevent +
                            " AND ID_Atleta = @param2";
                            commandUpdate.Parameters.AddWithValue("@param3", position); // position
                        }

                        commandUpdate.Parameters.AddWithValue("@param1", position); // position
                        commandUpdate.Parameters.AddWithValue("@param2", dr[0].ToString()); // id_atleta
                        commandUpdate.ExecuteNonQuery();

                        position++;
                    }

                    dr.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion

        // faccio UNDO, ripristino datarow originale
        private void button2_Click(object sender, EventArgs e)
        {
            int i = 0;
            try
            {
                message.Text = "";
                int indexElement = dgElements.SelectedRows[0].Index;
                for (i=0;i< 29; i++)
                {
                    dgElements.Rows[indexElement].Cells[i].Value =
                        dgElements.Rows[indexElement].Cells[29 + i].Value;
                    RipristinoColori(i);
                }

                int count = 1;
                for (i = 0; i < dgElements.RowCount; i++)
                {
                    if (dgElements.Rows[i].Cells[8].Value.ToString().Equals("0") ||
                        dgElements.Rows[i].Cells[8].Value.ToString().Equals("1"))
                    {
                        dgElements.Rows[i].Cells[3].Value = count;
                        count++;
                    }
                }

                // aggiorno l'elemento sul database
                UpdateTableGara(false);
                // aggiorno GaraFinal
                UpdateTableGaraFinal();

                UpdateTotals();

                NuovaClassificaSegmento();

                // riseleziono l'elemento
                dgElements.Rows[indexElement].Selected = false;
                dgElements.Rows[indexElement].Selected = true;
                dgElements.SelectedRows[0].Frozen = false;
            }
            catch (Exception ex)
            {
                message.Text = "Element " + i + ": " + ex.Message;
            }
        }

        private void rank_Click(object sender, EventArgs e)
        {

        }

        // chiudo senza salvare (non faccio commit)
        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                if ((baseValue.ForeColor == Color.Red) ||
                    (elementsScore.ForeColor == Color.Red) ||
                    (deductions.ForeColor == Color.Red) ||
                    (total.ForeColor == Color.Red))
                {
                    DialogResult res = MessageBox.Show(
                        RollartReview.Properties.Resources.Close, "RollartSystem",
                        MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning,
                        MessageBoxDefaultButton.Button2);

                    if (res == DialogResult.Yes)
                    {
                        transaction.Dispose();
                        Dispose();
                    }
                } else
                {
                    transaction.Dispose();
                    Dispose();
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        // salvo, faccio commit e chiudo
        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                message.Text = "";
                //if ((baseValue.ForeColor == Color.Red) ||
                //    (elementsScore.ForeColor == Color.Red) ||
                //    (deductions.ForeColor == Color.Red) ||
                //    (total.ForeColor == Color.Red))
                {
                    DialogResult res = MessageBox.Show(
                        RollartReview.Properties.Resources.Save, "RollartSystem", 
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button2);

                    if (res == DialogResult.Yes)
                    {
                        EventList.currentSegment.currentSkater.updated = true;

                        CalcoloClassificaSegmento();
                        CalcoloClassificaGeneraleParziale();
                        transaction.Commit();
                        
                    } else
                    {
                        transaction.Rollback();
                    }
                }
                Dispose();
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void save_Click(object sender, EventArgs e)
        {
            gb.Enabled = true;
            try
            {
                message.Text = "";
                // aggiorno i totali e la classifica
                UpdateTotals();
                NuovaClassificaSegmento();
                // aggiorno GaraFinal senza fare commit
                UpdateTableGaraFinal();

                cancel.Visible = false;
                save.Visible = false;
                updateElement.Enabled = true;
                undo.Enabled = true;
                close.Enabled = true;
                saveAndClose.Enabled = true;
                newElement.Enabled = true;
                cbElements.Visible = true;
                codice.Visible = false;
                nomeElemento.Visible = false;
                addElement.Visible = false;
                dgElements.Enabled = true;
                labelNew.Visible = false;
                deletelast.Visible = true;
                if (cb1.Checked)
                    numComboNew++;
                else numComboNew = 1;

                dgElements.DefaultCellStyle.SelectionBackColor = Color.FromArgb(166, 206, 56);
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                dgElements.DefaultCellStyle.SelectionBackColor = Color.FromArgb(166, 206, 56);
            }
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            try
            {
                message.Text = "";
                cancel.Visible = false;
                
                if (dgElements.RowCount > 0)
                {
                    updateElement.Enabled = true;
                    undo.Enabled = true;
                } else
                {
                    updateElement.Enabled = false;
                    undo.Enabled = false;
                }
                close.Enabled = true;
                saveAndClose.Enabled = true;
                newElement.Enabled = true;
                cbElements.Visible = true;
                codice.Visible = false;
                labelNew.Visible = false;
                nomeElemento.Visible = false;
                addElement.Visible = false;
                dgElements.Enabled = true;
                deletelast.Visible = true;
                gb.Enabled = true;
                foreach (DataGridViewRow dgvr in dgElements.Rows)
                {
                    //dgvr.DefaultCellStyle.BackColor = Color.White;
                    dgvr.Frozen = false;
                }
                //if (dgElements.RowCount > 0)
                //    dgElements.Rows[rigaSelezionata].Selected = true;
                // se elemento aggiunto lo rimuovo dal DataTable
                if (save.Visible )
                {
                    dtElements.Rows.RemoveAt(dtElements.Rows.Count - 1);
                    EventList.currentSegment.currentSkater.elements = dtElements;
                    dgElements.DataSource = dtElements;
                    dgElements.Enabled = true;
                }
                save.Visible = false;
                panelJudges.Visible = false;
                dgElements.DefaultCellStyle.SelectionBackColor = Color.FromArgb(166, 206, 56);

                //transactionNewElement.Rollback();
                //transactionNewElement = Definizioni.conn.BeginTransaction();
            }
            catch (Exception ex)
            {
                gb.Enabled = true;
                message.Text = ex.Message;
                dgElements.DefaultCellStyle.SelectionBackColor = Color.FromArgb(166, 206, 56);
            }
            
        }

        private void nomeElemento_TextChanged(object sender, EventArgs e)
        {

        }

        #region New Element
        private void newElement_Click(object sender, EventArgs e)
        {
            try
            {
                if (!MainMDI.loginRE)
                {
                    // Login del Referee
                    SplashScreen sc = new SplashScreen(2);
                    sc.ShowDialog();

                    if (!MainMDI.loginRE)
                        return;
                }

                message.Text = "";
                cancel.Visible = true;
                save.Visible = false;
                updateElement.Enabled = false;
                undo.Enabled = false;
                close.Enabled = false;
                saveAndClose.Enabled = false;
                newElement.Enabled = false;
                cbElements.Visible = false;
                deletelast.Visible = false;
                codice.Visible = true;
                nomeElemento.Visible = true;
                labelNew.Visible = true;
                addElement.Visible = true;
                dgElements.Enabled = false;
                if (dgElements.RowCount > 0)
                {
                    if (dgElements.SelectedRows.Count > 0)
                    {
                        rigaSelezionata = dgElements.SelectedRows[0].Index;
                        dgElements.Rows[0].Selected = false;
                    }
                }
                dgElements.ReadOnly = true;
                codice.Focus();
                codice.Text = "";
                nomeElemento.Text = "";
                if (numComboNew > 0)
                    cb1.Text = "Combo " + numComboNew;
                else cb1.Text = "Combo ";

                cb1.Enabled = false; cb1.Checked = false;
                cb3.Enabled = true; cb3.Checked = false;
                cb4.Enabled = false; cb4.Checked = false;
                rb1.Enabled = false; rb1.Checked = false;
                rb2.Enabled = false; rb2.Checked = false;
                rb3.Enabled = false; rb3.Checked = false;
                rb4.Enabled = false; rb4.Checked = false;
                num.Text = "0";
                bonusPiu.Enabled = false; bomusMeno.Enabled = false;
                comboPerc.Enabled = false;
                panelJudges.Visible = true;
                for (int i = 0; i < 9; i++)
                {
                    ((ComboBox)this.Controls.Find("j" + (i + 1), true)[0]).Visible = false;
                    ((Label)this.Controls.Find("l" + (i + 1), true)[0]).Visible = false;
                }
                for (int i=0; i<int.Parse(EventList.currentEvent.judges.numJudges); i++)
                {
                    ((ComboBox)this.Controls.Find("j" + (i + 1), true)[0]).Visible = true;
                    ((Label)this.Controls.Find("l" + (i + 1), true)[0]).Visible = true;
                    ((Label)this.Controls.Find("l" + (i + 1), true)[0]).ForeColor = Color.Red;
                    ((ComboBox)this.Controls.Find("j" + (i + 1), true)[0]).SelectedIndex = 0;
                }
                gb.Text = "New Element";
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void codice_TabIndexChanged(object sender, EventArgs e)
        {

        }

        private void codice_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!cancel.Focused)
                    RecuperoElemento();
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void codice_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
                {
                    RecuperoElemento();
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void codice_TextChanged(object sender, EventArgs e)
        {
            try
            {
                
                if (codice.TextLength == 1)
                    codice.Text = codice.Text.ToUpper();

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void j1_Validating(object sender, CancelEventArgs e)
        {
            //try
            //{
            //    string qoe = ((TextBox)sender).Text;
            //    if (qoe != "0" || qoe != "1" || qoe != "2" || qoe != "3" || qoe != "-" || qoe != "+")
            //        e.Cancel = true;
            //}
            //catch (Exception ex)
            //{
            //    message.Text = ex.Message;
            //}
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ComboBox cx = ((ComboBox)sender);
                if (cx.SelectedIndex > 0)
                    ((Label)this.Controls.Find(cx.Name.Replace("j", "l"), true)[0]).ForeColor = Color.Black;
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void panelJudges_Paint(object sender, PaintEventArgs e)
        {

        }

        private void toolStripLabel3_Click(object sender, EventArgs e)
        {
            if (!MainMDI.loginRE)
            {
                // Login del Referee
                SplashScreen sc = new SplashScreen(2);
                sc.ShowDialog();

                if (!MainMDI.loginRE)
                    return;
            }
            message.Text = "";
            string previuosValue = EventList.currentSegment.currentSkater.deduction;
            Deductions ded = new Deductions();
            ded.ShowDialog();
            if (previuosValue != EventList.currentSegment.currentSkater.deduction)
            {
                deductions.Text = EventList.currentSegment.currentSkater.deduction;
                deductions.BackColor = Color.Yellow;
                deductions.ForeColor = Color.Red;

                UpdateTotals();

                UpdateTableGaraFinal();

                NuovaClassificaSegmento();
            }
        }

        // aggiungo l'elemento alla griglia
        private void addElement_Click(object sender, EventArgs e)
        {
            try
            {
                string valuePenalty = "", valueBonus = "", valueCombo = "", valueFinal = "";
                decimal percentualeT = 0, bonusTrottola = 0, valueBase = 0;
                message.Text = "";
                //RecuperoElemento();
                if (nomeElemento.Text.Equals("Unknown element"))
                {
                    codice.Focus();
                    return;
                }

                // controllo i giudici
                valueBase = decimal.Parse(((ArrayList)elementTag)[7].ToString());
                if (valueBase == 0)
                {
                    // imposto tutti i giudici a zero
                    for (int i = 0; i < int.Parse(EventList.currentEvent.judges.numJudges); i++)
                    {
                        ((ComboBox)this.Controls.Find("j" + (i + 1), true)[0]).SelectedIndex = 4;
                        ((Label)this.Controls.Find("l" + (i + 1), true)[0]).ForeColor = Color.Black;
                    }
                }
                else
                {
                    bool controlloGiudici = true;
                    for (int i = 0; i < int.Parse(EventList.currentEvent.judges.numJudges); i++)
                    {
                        if (((ComboBox)this.Controls.Find("j" + (i + 1), true)[0]).SelectedIndex == 0)
                        {
                            ((ComboBox)this.Controls.Find("j" + (i + 1), true)[0]).Focus();
                            ((Label)this.Controls.Find("l" + (i + 1), true)[0]).ForeColor = Color.Red;
                            controlloGiudici = false;
                            break;
                        }
                        else
                        {
                            ((Label)this.Controls.Find("l" + (i + 1), true)[0]).ForeColor = Color.Black;
                        }
                    }
                    if (!controlloGiudici) return;
                }

                // progresivoEl
                int progressivoElnew = dgElements.Rows.Count + 1;
                
                // verifico l'ultimo numero degli elementi
                int numElemento = 1;
                foreach (DataGridViewRow dr in dgElements.Rows)
                {
                    if (!dr.Cells[3].Value.Equals(DBNull.Value))
                    {
                        numElemento = int.Parse(dr.Cells[3].Value.ToString()) + 1;
                    }
                }

                DataRow newElementRow = dtElements.NewRow();
                newElementRow[0] = EventList.currentEvent.idevent;
                newElementRow[1] = EventList.currentSegment.idsegment;
                newElementRow[2] = EventList.currentSegment.currentSkater.order;
                if (numComboNew == 1)
                    newElementRow[3] = numElemento; 

                // codice del nuovo elemento
                newElementRow[4] = codice.Text; 

                // SALTI
                if (rb4.Checked && rb4.Enabled) valuePenalty = "";// completed
                else if (rb3.Checked && rb3.Enabled) valuePenalty = "<<<";
                else if (rb2.Checked && rb2.Enabled) valuePenalty = "<<";
                else if (rb1.Checked && rb1.Enabled) valuePenalty = "<";
                else valuePenalty = "";

                newElementRow[5] = valuePenalty;

                // COMBO
                if (cb1.Checked)
                {
                    newElementRow[8] = "" + numComboNew;
                }
                else newElementRow[8] = 0;

                // "T" time
                if (cb4.Checked)
                {
                    percentualeT = 10;
                    valueBonus = "T";
                }

                // Percentuale trottola o salto
                if (comboPerc.SelectedIndex > 0)
                {
                    percentualeT += decimal.Parse(comboPerc.SelectedItem.ToString().Split('%')[0].TrimEnd());
                    valueBonus += "%";
                }

                // Bonus trottola
                if (num.Text != "0")
                {
                    bonusTrottola = int.Parse(num.Text);
                    valueBonus += "+";
                }

                // "*" out
                if (cb3.Checked) valueBonus = "*";

                newElementRow[27] = valueBonus;

                // ProgrCombo
                newElementRow[28] = 0;

                if (rb4.Checked && rb1.Enabled) // completed
                {
                    if (!cb1.Checked) valueBase = decimal.Parse(((ArrayList)elementTag)[7].ToString());
                    else valueBase = decimal.Parse(((ArrayList)elementTag)[13].ToString());
                }
                else if (rb1.Checked && rb1.Enabled) // <
                {
                    if (!cb1.Checked) valueBase = decimal.Parse(((ArrayList)elementTag)[8].ToString());
                    else valueBase = decimal.Parse(((ArrayList)elementTag)[14].ToString());
                }
                else if (rb2.Checked && rb2.Enabled) // <<
                {
                    if (!cb1.Checked) valueBase = decimal.Parse(((ArrayList)elementTag)[9].ToString());
                    else valueBase = decimal.Parse(((ArrayList)elementTag)[15].ToString());
                }
                else if (rb3.Checked && rb3.Enabled) // <<<
                {
                    GetElementDetails(codice.Text, "<<<", true);
                    if (!cb1.Checked) valueBase = decimal.Parse(((ArrayList)elementTag)[7].ToString());
                    else valueBase = decimal.Parse(((ArrayList)elementTag)[13].ToString());
                } else
                    valueBase = decimal.Parse(((ArrayList)elementTag)[7].ToString());

                // Valore Base
                valueBase = valueBase + Math.Round(((valueBase * percentualeT) / 100), 2) + bonusTrottola;
                newElementRow[6] = String.Format("{0:0.00}", valueBase);

                // giudici
                for (int i = 0; i < numJudges; i++)
                {
                    newElementRow[9 + i] = ((ComboBox)this.Controls.Find("j" + (i + 1), true)[0]).SelectedItem.ToString();
                }

                dtElements.Rows.Add(newElementRow);

                EventList.currentSegment.currentSkater.elements = dtElements;
                dgElements.DataSource = dtElements;

                newElementRow[27] += "(new)";

                // Replico i valori 
                for (int i = 0; i < 29; i++)
                    dgElements.Rows[dgElements.Rows.Count - 1].Cells[29 + i].Value =
                        dgElements.Rows[dgElements.Rows.Count - 1].Cells[i].Value;

                valueFinal = CalcoloValueFinal(valueBase, dgElements.Rows[dgElements.Rows.Count - 1]);
                newElementRow[7] = valueFinal;

                // Replico i valori 
                for (int i = 0; i < 29; i++)
                    dgElements.Rows[dgElements.Rows.Count - 1].Cells[29 + i].Value =
                        dgElements.Rows[dgElements.Rows.Count - 1].Cells[i].Value;
                
                dgElements.Rows[dgElements.Rows.Count - 1].Selected = true;
                dgElements.FirstDisplayedScrollingRowIndex = dgElements.SelectedRows[0].Index;
                dgElements.DefaultCellStyle.SelectionBackColor = Color.FromArgb(255, 255, 128);
                // 166; 206; 56
                // Inserisco nel db senza fare commit
                InsertTableGara(dgElements.Rows[dgElements.Rows.Count - 1]);

                save.Visible = true;
                addElement.Visible = false;
                panelJudges.Visible = false;
                updateElement.Enabled = false; undo.Enabled = false; saveAndClose.Enabled = false;
                gb.Enabled = false;
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }
        // Delete last element
        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {

                message.Text = "";
                if (dgElements.RowCount == 0) return;
                DialogResult res = MessageBox.Show(
                        RollartReview.Properties.Resources.Delete, "RollartSystem",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button2);

                if (res == DialogResult.Yes)
                {
                    UpdateTableGara(true);
                    // aggiorno i totali e la classifica
                    UpdateTotals();
                    NuovaClassificaSegmento();
                    // aggiorno GaraFinal senza fare commit
                    UpdateTableGaraFinal();
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

       
        private void dgElements_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (e == null) return;
                if (e.Value == null) return;
                if (e.ColumnIndex==6 || e.ColumnIndex==7 || e.ColumnIndex == 35 || e.ColumnIndex == 36)
                {
                    e.Value = Utility.FormattaOggettoDecInString(e.Value.ToString());
                }
            }
            catch (Exception ex)
            {
                message.Text = "dgElements_CellFormatting: " + ex.Message;
            }
        }

        private void toolStripLabel4_Click(object sender, EventArgs e)
        {
            try
            {
                if (!MainMDI.loginRE)
                {
                    // Login del Referee
                    SplashScreen sc = new SplashScreen(2);
                    sc.ShowDialog();

                    if (!MainMDI.loginRE)
                        return;
                }

                message.Text = "";
                string previuosValue = EventList.currentSegment.currentSkater.pres_score;
                Components components = new Components();
                components.ShowDialog();
                if (previuosValue != EventList.currentSegment.currentSkater.pres_score)
                {
                    //transaction.Commit();
                    
                    comp.Text = EventList.currentSegment.currentSkater.pres_score;
                    comp.BackColor = Color.Yellow;
                    comp.ForeColor = Color.Red;

                    UpdateTotals();

                    UpdateTableGaraFinal();

                    NuovaClassificaSegmento();

                    //transaction.Commit();
                }

            }
            catch (Exception ex)
            {
                message.Text = "Components: " + ex.Message;
            }
        }

        private void RecuperoElemento()
        {
            nomeElemento.Text = "";
            nomeElemento.ForeColor = Color.Black;

            GetElementDetails(codice.Text, "", true);

            if (((ArrayList)elementTag).Count > 0)
                nomeElemento.Text = ((ArrayList)elementTag)[1].ToString();
            else
            {
                nomeElemento.Text = "Unknown element";
                nomeElemento.ForeColor = Color.Red;
                return;
            }

            catElement = int.Parse(((ArrayList)elementTag)[17].ToString());

            // se elemento nullo abilito solo * 
            if (((ArrayList)elementTag)[7].ToString().Equals("0"))
            {
                rb1.Enabled = false; rb2.Enabled = false; rb3.Enabled = false; rb4.Enabled = false;
                cb1.Enabled = false; cb4.Enabled = false; comboPerc.Enabled = false; cb3.Enabled = true;
                bonusPiu.Enabled = false; bomusMeno.Enabled = false;
                if (catElement == 1) // salti
                    cb1.Enabled = true;// abilito le combo
            }
            // in base alla categoria abilito/disabilito le opzioni
            else if (catElement == 1) // salti singoli
            {
                rb1.Enabled = true; rb2.Enabled = true; rb3.Enabled = true; rb4.Enabled = true;
                cb1.Enabled = true; cb4.Enabled = true; comboPerc.Enabled = true; cb3.Enabled = true;
                bonusPiu.Enabled = false; bomusMeno.Enabled = false; rb4.Checked = true;
            }
            else if (catElement == 4 || catElement == 8) // salti coppia
            {
                rb1.Enabled = true; rb2.Enabled = true; rb3.Enabled = true; rb4.Enabled = true;
                cb1.Enabled = false; cb4.Enabled = false; comboPerc.Enabled = false; cb3.Enabled = true;
                bonusPiu.Enabled = false; bomusMeno.Enabled = false; rb4.Checked = true;
            }
            else if (catElement == 2 || catElement == 7) // trottole 
            {
                rb1.Enabled = false; rb2.Enabled = false; rb3.Enabled = false; rb4.Enabled = false;
                cb1.Enabled = true; cb4.Enabled = false; comboPerc.Enabled = true; cb3.Enabled = true;
                bonusPiu.Enabled = true; bomusMeno.Enabled = true; 
            } 
            else // tutti gli altri elementi
            {
                rb1.Enabled = false; rb2.Enabled = false; rb3.Enabled = false; rb4.Enabled = false;
                cb1.Enabled = false; cb4.Enabled = false; comboPerc.Enabled = false; cb3.Enabled = true;
                bonusPiu.Enabled = false; bomusMeno.Enabled = false; 
            }

            
        }

        public void GetAllElements(string idElementsCat)
        {
            try
            {
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT Code FROM Elements";
                    command.CommandText += " WHERE ID_ElementsCat IN (" + idElementsCat.TrimEnd(',') + ")";
                    SQLiteDataReader dr = command.ExecuteReader();
                    if (dr.HasRows)
                    {
                        Definizioni.elementi = new ArrayList();
                    }
                    while (dr.Read())
                    {
                        Definizioni.elementi.Add(dr[0]);
                    }
                    dr.Close();
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        #endregion

        private void dgElements_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                
                
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        public void InseriscoGaraFinalTotal()
        {
            try
            {
                using (SQLiteCommand command2 = Definizioni.conn.CreateCommand())
                {
                    command2.CommandText = "SELECT ID_Atleta FROM GaraFinal WHERE ID_GaraParams = " + EventList.currentEvent.idevent +
                                 " AND ID_Atleta = " + EventList.currentSegment.currentSkater.idskater +
                                 " AND ID_Segment = " + EventList.currentSegment.idsegment;
                    SQLiteDataReader dr2 = command2.ExecuteReader();
                    if (!dr2.HasRows) // se in GaraTotal il record non c'è faccio INSERT
                    {
                        using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                        {
                            command.Transaction = transaction;

                            command.CommandText = "INSERT INTO GaraFinal(ID_GaraParams,ID_Segment,NumPartecipante,BaseTech," +
                                "FinalTech,BaseArtistic,Deductions,Total,ID_Atleta) VALUES(@param1,@param8,@param2,@param3,@param4,@param5,@param6,@param7,@param9)";
                            command.Parameters.AddWithValue("@param1", EventList.currentEvent.idevent); // ID_GaraParams
                            command.Parameters.AddWithValue("@param8", EventList.currentSegment.idsegment); // ID_Segment
                            command.Parameters.AddWithValue("@param2", EventList.currentSegment.currentSkater.order); //NumPartecipante
                            command.Parameters.AddWithValue("@param3", 0); //BaseTech
                            command.Parameters.AddWithValue("@param4", 0); //FinalTech
                            command.Parameters.AddWithValue("@param5", 0); //Components
                            command.Parameters.AddWithValue("@param6", 0); //Deductions               
                            command.Parameters.AddWithValue("@param7", 0); //Totale
                            command.Parameters.AddWithValue("@param9", EventList.currentSegment.currentSkater.idskater); //ID_Atleta
                            command.ExecuteNonQuery();
                        }
                    }
                }

                using (SQLiteCommand command2 = Definizioni.conn.CreateCommand())
                {
                    command2.CommandText = "SELECT ID_Atleta, TotGara FROM GaraTotal WHERE ID_GaraParams = " + EventList.currentEvent.idevent +
                                 " AND ID_Atleta = " + EventList.currentSegment.currentSkater.idskater;
                    SQLiteDataReader dr2 = command2.ExecuteReader();
                    if (!dr2.HasRows) // se in GaraTotal il record non c'è faccio INSERT
                    {
                        using (SQLiteCommand command3 = Definizioni.conn.CreateCommand())
                        {
                            command3.Transaction = transaction;

                            command3.CommandText = "INSERT INTO GaraTotal" +
                            " VALUES(@param1,@param2,@param3,@param4,@param5,0,0)";
                            command3.Parameters.AddWithValue("@param1", EventList.currentEvent.idevent); // ID_GaraParams
                            command3.Parameters.AddWithValue("@param2", EventList.currentSegment.currentSkater.idskater); // ID_Atleta
                            command3.Parameters.AddWithValue("@param3", 0); // position
                            command3.Parameters.AddWithValue("@param4", EventList.currentSegment.currentSkater.name); //Atleta
                            command3.Parameters.AddWithValue("@param5", 0); // totale
                            command3.ExecuteNonQuery();
                        }
                    }
                }

            }
            catch (SQLiteException ex)
            {
                Utility.WriteLog(ex.Message, "ERROR");
            }
        }


        #region Formattazione


        public static bool IsOdd(int value)
        {
            return value % 2 != 0;
        }

        private void RipristinoColori(int cella)
        {
            dgElements.SelectedRows[0].Cells[cella].Style.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dgElements.SelectedRows[0].Cells[cella].Style.SelectionForeColor = Color.White;
            dgElements.SelectedRows[0].Cells[cella].Style.ForeColor = Color.Black;
        }

        private void ImpostoNuoviColori(int cella)
        {
            dgElements.SelectedRows[0].Cells[cella].Style.SelectionBackColor = Color.Yellow;
            dgElements.SelectedRows[0].Cells[cella].Style.SelectionForeColor = Color.Red;
            dgElements.SelectedRows[0].Cells[cella].Style.ForeColor = Color.Red;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            currentSkater = 1;
            GetSkater(currentSkater, false);
        }
        
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (currentSkater == totalSkaters) return;
            currentSkater++;
            GetSkater(currentSkater, false);
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            currentSkater = totalSkaters;
            GetSkater(currentSkater, false);
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (currentSkater == 1) return;
            currentSkater--;
            GetSkater(currentSkater, false);

        }

        private void EnableControls()
        {
            rb1.Enabled = true; rb2.Enabled = true; rb3.Enabled = true;
            cb1.Enabled = true; comboPerc.Enabled = true;
            cb4.Enabled = true; rb4.Enabled = true;
        }

        private void DisableControls()
        {
            rb1.Enabled = false; rb2.Enabled = false; rb3.Enabled = false;
            cb1.Enabled = false; comboPerc.Enabled = false;
            cb4.Enabled = false; rb4.Enabled = false;
            bomusMeno.Enabled = false; bonusPiu.Enabled = false;
        }
        #endregion
    }

}

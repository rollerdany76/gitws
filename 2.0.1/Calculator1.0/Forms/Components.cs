﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Components : Form
    {
        SQLiteDataReader sqdr = null;
        int numJudges = 0;

        public Components()
        {
            InitializeComponent();
            
        }

        private void close_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void Components_Load(object sender, EventArgs e)
        {
            try
            {
                numJudges = int.Parse(EventList.currentEvent.judges.numJudges);

                for (int i = numJudges; i < 9; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        table.Controls.Remove(table.GetControlFromPosition(2 + i, 1 + j));
                    }
                }

                GetComponents();
                factor.Text = "Factor " + Utility.FormattaOggettoDecInString(EventList.currentSegment.factor);
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        // recupero components
        private void GetComponents()
        {
            try
            {
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    cmd.CommandText =
                        " SELECT * FROM Gara WHERE ID_GARAPARAMS = " + EventList.currentEvent.idevent +
                        " AND ID_SEGMENT = " + EventList.currentSegment.idsegment + "" +
                        " AND NumPartecipante = '" + EventList.currentSegment.currentSkater.order +
                        "' AND Element like 'comp%'";

                    sqdr = cmd.ExecuteReader();
                    if (!sqdr.HasRows)
                    {

                    }
                    else
                    {
                        int countComp = 1;
                        while (sqdr.Read())
                        {
                            string comp = sqdr[4].ToString();
                            if (countComp == 5) break;
                            for (int i = 1; i < numJudges + 1; i++)
                            {
                                ((NumericUpDown)this.Controls.Find("comp" + countComp + "" + i, true)[0]).Value =
                                    decimal.Parse(sqdr[17 + i].ToString());
                            }

                            ((Label)this.Controls.Find("comp" + countComp, true)[0]).Text = sqdr[7].ToString();
                                //Utility.FormattaOggettoDecInString(sqdr[7].ToString());
                            countComp++;
                        }
                        // calcolo il totale

                    }
                }
                //CalcoloTotale();
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        public void AggiornoComponents()
        {
            try
            {
                decimal valueComp = 0;
                decimal[] comps = new decimal[numJudges];
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    cmd.CommandText =
                        " SELECT * FROM Gara WHERE ID_GARAPARAMS = " + EventList.currentEvent.idevent +
                        " AND ID_SEGMENT = " + EventList.currentSegment.idsegment + "" +
                        " AND NumPartecipante = '" + EventList.currentSegment.currentSkater.order +
                        "' AND Element like 'comp%'";

                    sqdr = cmd.ExecuteReader();
                    if (!sqdr.HasRows)
                    {
                        // inserisco nuovo
                        using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                        {
                            command.CommandText = "INSERT INTO Gara(ID_GaraParams,ID_Segment,NumPartecipante,Element)" +
                                                    " VALUES(@param1,@param2,@param3,@element)";
                            command.Parameters.AddWithValue("@param1", EventList.currentEvent.idevent);
                            command.Parameters.AddWithValue("@param2", EventList.currentSegment.idsegment);
                            command.Parameters.AddWithValue("@param3", EventList.currentSegment.currentSkater.order);
                            for (int i = 1; i < 5; i++)
                            {
                                command.Parameters.AddWithValue("@element", "comp" + i);
                                command.ExecuteNonQuery();
                            }
                        }
                    } 

                    {
                        using (SQLiteCommand commandUpdate = Definizioni.conn.CreateCommand())
                        {
                            commandUpdate.Transaction = CheckScores.transaction;
                            for (int i = 0; i < numJudges; i++)
                            {
                                for (int j = 1; j < 5; j++)
                                {
                                    valueComp = ((NumericUpDown)this.Controls.Find("comp" + j + "" + (i + 1), true)[0]).Value;
                                    commandUpdate.CommandText = (
                                            "UPDATE Gara SET qoev_" + (i + 1) + " = '" + valueComp +
                                            "' WHERE ID_GaraParams = " + EventList.currentEvent.idevent +
                                            " AND ID_Segment = " + EventList.currentSegment.idsegment +
                                            " AND NumPartecipante = " + EventList.currentSegment.currentSkater.order +
                                            " AND Element = 'comp" + j + "'").Replace(',', '.');
                                    commandUpdate.ExecuteNonQuery();
                                }
                            }

                            // aggiorno il valuefinal
                            for (int j = 1; j < 5; j++)
                            {
                                commandUpdate.CommandText = (
                                        "UPDATE Gara SET ValueFinal = '" + ((Label)this.Controls.Find("comp" + j, true)[0]).Text +
                                        "' WHERE ID_GaraParams = " + EventList.currentEvent.idevent +
                                        " AND ID_Segment = " + EventList.currentSegment.idsegment +
                                        " AND NumPartecipante = " + EventList.currentSegment.currentSkater.order +
                                        " AND Element = 'comp" + j + "'").Replace(',', '.');
                                commandUpdate.ExecuteNonQuery();
                            }
                        }

                    }
                }
                
                sqdr.Close();

                EventList.currentSegment.currentSkater.pres_score = total.Text;
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        public void CalcoloTotale()
        {
            try
            {
                decimal totaleComp = 0m;

                totaleComp = decimal.Parse(comp1.Text, System.Globalization.NumberStyles.Any);
                totaleComp += decimal.Parse(comp2.Text, System.Globalization.NumberStyles.Any);
                totaleComp += decimal.Parse(comp3.Text, System.Globalization.NumberStyles.Any);
                totaleComp += decimal.Parse(comp4.Text, System.Globalization.NumberStyles.Any);

                // moltiplico per il factor
                totaleComp = totaleComp * EventList.currentSegment.factor;
                total.Text = Utility.FormattaOggettoDecInString(totaleComp);
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                decimal valDec = ((NumericUpDown)sender).Value;
                int riga = table.GetRow((NumericUpDown)sender);
                CalcoloSpecificaComponent("comp" + (riga), valDec);
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void CalcoloSpecificaComponent(string comp, decimal val)
        {
            decimal valueComp = 0;
            decimal[] comps = new decimal[numJudges];
            object ob;
            if (numJudges <= 3) // fino a 3 giudici
            {
                for (int i = 1; i < numJudges + 1; i++)
                {
                    valueComp += ((NumericUpDown)this.Controls.Find(
                        comp + i, true)[0]).Value;
                }
                valueComp = Decimal.Round(valueComp / numJudges, 2);
            }
            else // da 4 giudici in su
            {
                for (int i = 1; i < numJudges + 1; i++)
                {
                    ob = ((NumericUpDown)this.Controls.Find(
                        comp + i, true)[0]).Value;
                    comps[i - 1] = ((NumericUpDown)this.Controls.Find(
                        comp + i, true)[0]).Value;
                    valueComp += comps[i - 1];
                }
                decimal min = Utility.Lowest(comps); // minimo
                decimal max = Utility.Highest(comps); // max
                valueComp = valueComp - min - max; //tolgo il minimo e il massimo
                valueComp = Decimal.Round(valueComp / (numJudges - 2), 2);
            }

            ((Label)this.Controls.Find(
                        comp, true)[0]).Text = Utility.FormattaOggettoDecInString(valueComp);
            ((Label)this.Controls.Find(
                        comp, true)[0]).ForeColor = Color.Blue;
            CalcoloTotale();
        }

        private void numericUpDown1_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                string val = ((NumericUpDown)sender).Text;
                string last2digits = val.Substring(val.Length - 2);
                if (!last2digits.Equals("25") && !last2digits.Equals("75") && !last2digits.Equals("50") && !last2digits.Equals("00"))
                    e.Cancel = true;
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void table_Paint(object sender, PaintEventArgs e)
        {

        }
        
        // cambia il valore dei components
        private void total_TextChanged(object sender, EventArgs e)
        {
            if (!total.Text.Equals(EventList.currentSegment.currentSkater.pres_score))
            {
                saveAndClose.Enabled = true;
            }
        }

        private void comp1_Click(object sender, EventArgs e)
        {

        }

        private void saveAndClose_Click(object sender, EventArgs e)
        {
            try
            {
                // salvo nel db
                if (!total.Text.Equals(EventList.currentSegment.currentSkater.pres_score))
                {
                    //if (total.Text.Equals("0.00"))
                    //    InseriscoComponents();
                    //else
                    AggiornoComponents();
                    
                }

                Dispose();
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }
    }
}

﻿namespace Calculator
{
    partial class Components
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.table = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.comp19 = new System.Windows.Forms.NumericUpDown();
            this.comp29 = new System.Windows.Forms.NumericUpDown();
            this.comp39 = new System.Windows.Forms.NumericUpDown();
            this.comp49 = new System.Windows.Forms.NumericUpDown();
            this.comp17 = new System.Windows.Forms.NumericUpDown();
            this.comp28 = new System.Windows.Forms.NumericUpDown();
            this.comp38 = new System.Windows.Forms.NumericUpDown();
            this.comp48 = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.numericUpDown9 = new System.Windows.Forms.NumericUpDown();
            this.comp27 = new System.Windows.Forms.NumericUpDown();
            this.comp37 = new System.Windows.Forms.NumericUpDown();
            this.comp47 = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.comp16 = new System.Windows.Forms.NumericUpDown();
            this.comp26 = new System.Windows.Forms.NumericUpDown();
            this.comp36 = new System.Windows.Forms.NumericUpDown();
            this.comp46 = new System.Windows.Forms.NumericUpDown();
            this.comp15 = new System.Windows.Forms.NumericUpDown();
            this.comp25 = new System.Windows.Forms.NumericUpDown();
            this.comp35 = new System.Windows.Forms.NumericUpDown();
            this.comp45 = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comp14 = new System.Windows.Forms.NumericUpDown();
            this.comp24 = new System.Windows.Forms.NumericUpDown();
            this.comp34 = new System.Windows.Forms.NumericUpDown();
            this.comp44 = new System.Windows.Forms.NumericUpDown();
            this.comp13 = new System.Windows.Forms.NumericUpDown();
            this.comp23 = new System.Windows.Forms.NumericUpDown();
            this.comp33 = new System.Windows.Forms.NumericUpDown();
            this.comp43 = new System.Windows.Forms.NumericUpDown();
            this.comp42 = new System.Windows.Forms.NumericUpDown();
            this.comp32 = new System.Windows.Forms.NumericUpDown();
            this.comp22 = new System.Windows.Forms.NumericUpDown();
            this.comp12 = new System.Windows.Forms.NumericUpDown();
            this.comp11 = new System.Windows.Forms.NumericUpDown();
            this.comp21 = new System.Windows.Forms.NumericUpDown();
            this.comp31 = new System.Windows.Forms.NumericUpDown();
            this.comp41 = new System.Windows.Forms.NumericUpDown();
            this.comp1 = new System.Windows.Forms.Label();
            this.comp2 = new System.Windows.Forms.Label();
            this.comp3 = new System.Windows.Forms.Label();
            this.comp4 = new System.Windows.Forms.Label();
            this.total = new System.Windows.Forms.Label();
            this.factor = new System.Windows.Forms.Label();
            this.close = new System.Windows.Forms.Button();
            this.saveAndClose = new System.Windows.Forms.Button();
            this.message = new System.Windows.Forms.Label();
            this.table.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comp19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp41)).BeginInit();
            this.SuspendLayout();
            // 
            // table
            // 
            this.table.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.table.ColumnCount = 11;
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 53F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 53F));
            this.table.Controls.Add(this.label4, 0, 4);
            this.table.Controls.Add(this.label3, 0, 3);
            this.table.Controls.Add(this.label2, 0, 2);
            this.table.Controls.Add(this.label1, 0, 1);
            this.table.Controls.Add(this.label13, 10, 0);
            this.table.Controls.Add(this.comp19, 10, 1);
            this.table.Controls.Add(this.comp29, 10, 2);
            this.table.Controls.Add(this.comp39, 10, 3);
            this.table.Controls.Add(this.comp49, 10, 4);
            this.table.Controls.Add(this.comp17, 9, 1);
            this.table.Controls.Add(this.comp28, 9, 2);
            this.table.Controls.Add(this.comp38, 9, 3);
            this.table.Controls.Add(this.comp48, 9, 4);
            this.table.Controls.Add(this.label12, 9, 0);
            this.table.Controls.Add(this.label11, 8, 0);
            this.table.Controls.Add(this.numericUpDown9, 8, 1);
            this.table.Controls.Add(this.comp27, 8, 2);
            this.table.Controls.Add(this.comp37, 8, 3);
            this.table.Controls.Add(this.comp47, 8, 4);
            this.table.Controls.Add(this.label10, 7, 0);
            this.table.Controls.Add(this.comp16, 7, 1);
            this.table.Controls.Add(this.comp26, 7, 2);
            this.table.Controls.Add(this.comp36, 7, 3);
            this.table.Controls.Add(this.comp46, 7, 4);
            this.table.Controls.Add(this.comp15, 6, 1);
            this.table.Controls.Add(this.comp25, 6, 2);
            this.table.Controls.Add(this.comp35, 6, 3);
            this.table.Controls.Add(this.comp45, 6, 4);
            this.table.Controls.Add(this.label9, 6, 0);
            this.table.Controls.Add(this.label8, 5, 0);
            this.table.Controls.Add(this.label7, 4, 0);
            this.table.Controls.Add(this.label6, 3, 0);
            this.table.Controls.Add(this.label5, 2, 0);
            this.table.Controls.Add(this.comp14, 5, 1);
            this.table.Controls.Add(this.comp24, 5, 2);
            this.table.Controls.Add(this.comp34, 5, 3);
            this.table.Controls.Add(this.comp44, 5, 4);
            this.table.Controls.Add(this.comp13, 4, 1);
            this.table.Controls.Add(this.comp23, 4, 2);
            this.table.Controls.Add(this.comp33, 4, 3);
            this.table.Controls.Add(this.comp43, 4, 4);
            this.table.Controls.Add(this.comp42, 3, 4);
            this.table.Controls.Add(this.comp32, 3, 3);
            this.table.Controls.Add(this.comp22, 3, 2);
            this.table.Controls.Add(this.comp12, 3, 1);
            this.table.Controls.Add(this.comp11, 2, 1);
            this.table.Controls.Add(this.comp21, 2, 2);
            this.table.Controls.Add(this.comp31, 2, 3);
            this.table.Controls.Add(this.comp41, 2, 4);
            this.table.Controls.Add(this.comp1, 1, 1);
            this.table.Controls.Add(this.comp2, 1, 2);
            this.table.Controls.Add(this.comp3, 1, 3);
            this.table.Controls.Add(this.comp4, 1, 4);
            this.table.Location = new System.Drawing.Point(9, 11);
            this.table.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.table.Name = "table";
            this.table.RowCount = 5;
            this.table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.table.Size = new System.Drawing.Size(649, 147);
            this.table.TabIndex = 0;
            this.table.Paint += new System.Windows.Forms.PaintEventHandler(this.table_Paint);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 117);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Choreography";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 88);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Performance";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 59);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Transitions";
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "Skating Skills";
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label13.Location = new System.Drawing.Point(602, 1);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 28);
            this.label13.TabIndex = 15;
            this.label13.Text = "J9";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comp19
            // 
            this.comp19.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp19.DecimalPlaces = 2;
            this.comp19.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp19.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp19.Location = new System.Drawing.Point(602, 32);
            this.comp19.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp19.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp19.Name = "comp19";
            this.comp19.Size = new System.Drawing.Size(48, 19);
            this.comp19.TabIndex = 24;
            this.comp19.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp29
            // 
            this.comp29.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp29.DecimalPlaces = 2;
            this.comp29.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp29.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp29.Location = new System.Drawing.Point(602, 61);
            this.comp29.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp29.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp29.Name = "comp29";
            this.comp29.Size = new System.Drawing.Size(48, 19);
            this.comp29.TabIndex = 34;
            this.comp29.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp39
            // 
            this.comp39.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp39.DecimalPlaces = 2;
            this.comp39.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp39.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp39.Location = new System.Drawing.Point(602, 90);
            this.comp39.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp39.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp39.Name = "comp39";
            this.comp39.Size = new System.Drawing.Size(48, 19);
            this.comp39.TabIndex = 44;
            this.comp39.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp49
            // 
            this.comp49.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp49.DecimalPlaces = 2;
            this.comp49.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp49.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp49.Location = new System.Drawing.Point(602, 119);
            this.comp49.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp49.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp49.Name = "comp49";
            this.comp49.Size = new System.Drawing.Size(48, 19);
            this.comp49.TabIndex = 54;
            this.comp49.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp17
            // 
            this.comp17.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp17.DecimalPlaces = 2;
            this.comp17.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp17.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp17.Location = new System.Drawing.Point(549, 32);
            this.comp17.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp17.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp17.Name = "comp17";
            this.comp17.Size = new System.Drawing.Size(47, 19);
            this.comp17.TabIndex = 23;
            this.comp17.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp28
            // 
            this.comp28.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp28.DecimalPlaces = 2;
            this.comp28.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp28.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp28.Location = new System.Drawing.Point(549, 61);
            this.comp28.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp28.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp28.Name = "comp28";
            this.comp28.Size = new System.Drawing.Size(47, 19);
            this.comp28.TabIndex = 33;
            this.comp28.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp38
            // 
            this.comp38.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp38.DecimalPlaces = 2;
            this.comp38.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp38.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp38.Location = new System.Drawing.Point(549, 90);
            this.comp38.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp38.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp38.Name = "comp38";
            this.comp38.Size = new System.Drawing.Size(47, 19);
            this.comp38.TabIndex = 43;
            this.comp38.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp48
            // 
            this.comp48.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp48.DecimalPlaces = 2;
            this.comp48.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp48.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp48.Location = new System.Drawing.Point(549, 119);
            this.comp48.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp48.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp48.Name = "comp48";
            this.comp48.Size = new System.Drawing.Size(47, 19);
            this.comp48.TabIndex = 53;
            this.comp48.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label12.Location = new System.Drawing.Point(549, 1);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 28);
            this.label12.TabIndex = 14;
            this.label12.Text = "J8";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label11.Location = new System.Drawing.Point(496, 1);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 28);
            this.label11.TabIndex = 13;
            this.label11.Text = "J7";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numericUpDown9
            // 
            this.numericUpDown9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numericUpDown9.DecimalPlaces = 2;
            this.numericUpDown9.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown9.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.numericUpDown9.Location = new System.Drawing.Point(496, 32);
            this.numericUpDown9.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.numericUpDown9.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown9.Name = "numericUpDown9";
            this.numericUpDown9.Size = new System.Drawing.Size(47, 19);
            this.numericUpDown9.TabIndex = 22;
            this.numericUpDown9.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp27
            // 
            this.comp27.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp27.DecimalPlaces = 2;
            this.comp27.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp27.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp27.Location = new System.Drawing.Point(496, 61);
            this.comp27.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp27.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp27.Name = "comp27";
            this.comp27.Size = new System.Drawing.Size(47, 19);
            this.comp27.TabIndex = 32;
            this.comp27.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp37
            // 
            this.comp37.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp37.DecimalPlaces = 2;
            this.comp37.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp37.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp37.Location = new System.Drawing.Point(496, 90);
            this.comp37.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp37.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp37.Name = "comp37";
            this.comp37.Size = new System.Drawing.Size(47, 19);
            this.comp37.TabIndex = 42;
            this.comp37.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp47
            // 
            this.comp47.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp47.DecimalPlaces = 2;
            this.comp47.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp47.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp47.Location = new System.Drawing.Point(496, 119);
            this.comp47.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp47.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp47.Name = "comp47";
            this.comp47.Size = new System.Drawing.Size(47, 19);
            this.comp47.TabIndex = 52;
            this.comp47.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label10.Location = new System.Drawing.Point(443, 1);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 28);
            this.label10.TabIndex = 12;
            this.label10.Text = "J6";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comp16
            // 
            this.comp16.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp16.DecimalPlaces = 2;
            this.comp16.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp16.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp16.Location = new System.Drawing.Point(443, 32);
            this.comp16.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp16.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp16.Name = "comp16";
            this.comp16.Size = new System.Drawing.Size(47, 19);
            this.comp16.TabIndex = 21;
            this.comp16.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp26
            // 
            this.comp26.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp26.DecimalPlaces = 2;
            this.comp26.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp26.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp26.Location = new System.Drawing.Point(443, 61);
            this.comp26.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp26.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp26.Name = "comp26";
            this.comp26.Size = new System.Drawing.Size(47, 19);
            this.comp26.TabIndex = 31;
            this.comp26.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp36
            // 
            this.comp36.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp36.DecimalPlaces = 2;
            this.comp36.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp36.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp36.Location = new System.Drawing.Point(443, 90);
            this.comp36.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp36.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp36.Name = "comp36";
            this.comp36.Size = new System.Drawing.Size(47, 19);
            this.comp36.TabIndex = 41;
            this.comp36.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp46
            // 
            this.comp46.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp46.DecimalPlaces = 2;
            this.comp46.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp46.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp46.Location = new System.Drawing.Point(443, 119);
            this.comp46.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp46.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp46.Name = "comp46";
            this.comp46.Size = new System.Drawing.Size(47, 19);
            this.comp46.TabIndex = 51;
            this.comp46.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp15
            // 
            this.comp15.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp15.DecimalPlaces = 2;
            this.comp15.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp15.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp15.Location = new System.Drawing.Point(390, 32);
            this.comp15.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp15.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp15.Name = "comp15";
            this.comp15.Size = new System.Drawing.Size(47, 19);
            this.comp15.TabIndex = 20;
            this.comp15.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp25
            // 
            this.comp25.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp25.DecimalPlaces = 2;
            this.comp25.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp25.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp25.Location = new System.Drawing.Point(390, 61);
            this.comp25.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp25.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp25.Name = "comp25";
            this.comp25.Size = new System.Drawing.Size(47, 19);
            this.comp25.TabIndex = 30;
            this.comp25.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp35
            // 
            this.comp35.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp35.DecimalPlaces = 2;
            this.comp35.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp35.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp35.Location = new System.Drawing.Point(390, 90);
            this.comp35.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp35.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp35.Name = "comp35";
            this.comp35.Size = new System.Drawing.Size(47, 19);
            this.comp35.TabIndex = 40;
            this.comp35.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp45
            // 
            this.comp45.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp45.DecimalPlaces = 2;
            this.comp45.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp45.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp45.Location = new System.Drawing.Point(390, 119);
            this.comp45.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp45.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp45.Name = "comp45";
            this.comp45.Size = new System.Drawing.Size(47, 19);
            this.comp45.TabIndex = 50;
            this.comp45.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label9.Location = new System.Drawing.Point(390, 1);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 28);
            this.label9.TabIndex = 11;
            this.label9.Text = "J5";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label8.Location = new System.Drawing.Point(337, 1);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 28);
            this.label8.TabIndex = 10;
            this.label8.Text = "J4";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label7.Location = new System.Drawing.Point(284, 1);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 28);
            this.label7.TabIndex = 9;
            this.label7.Text = "J3";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label6.Location = new System.Drawing.Point(231, 1);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 28);
            this.label6.TabIndex = 5;
            this.label6.Text = "J2";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label5.Location = new System.Drawing.Point(178, 1);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 28);
            this.label5.TabIndex = 4;
            this.label5.Text = "J1";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comp14
            // 
            this.comp14.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp14.DecimalPlaces = 2;
            this.comp14.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp14.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp14.Location = new System.Drawing.Point(337, 32);
            this.comp14.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp14.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp14.Name = "comp14";
            this.comp14.Size = new System.Drawing.Size(47, 19);
            this.comp14.TabIndex = 19;
            this.comp14.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp24
            // 
            this.comp24.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp24.DecimalPlaces = 2;
            this.comp24.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp24.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp24.Location = new System.Drawing.Point(337, 61);
            this.comp24.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp24.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp24.Name = "comp24";
            this.comp24.Size = new System.Drawing.Size(47, 19);
            this.comp24.TabIndex = 29;
            this.comp24.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp34
            // 
            this.comp34.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp34.DecimalPlaces = 2;
            this.comp34.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp34.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp34.Location = new System.Drawing.Point(337, 90);
            this.comp34.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp34.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp34.Name = "comp34";
            this.comp34.Size = new System.Drawing.Size(47, 19);
            this.comp34.TabIndex = 39;
            this.comp34.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp44
            // 
            this.comp44.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp44.DecimalPlaces = 2;
            this.comp44.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp44.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp44.Location = new System.Drawing.Point(337, 119);
            this.comp44.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp44.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp44.Name = "comp44";
            this.comp44.Size = new System.Drawing.Size(47, 19);
            this.comp44.TabIndex = 49;
            this.comp44.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp13
            // 
            this.comp13.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp13.DecimalPlaces = 2;
            this.comp13.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp13.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp13.Location = new System.Drawing.Point(284, 32);
            this.comp13.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp13.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp13.Name = "comp13";
            this.comp13.Size = new System.Drawing.Size(47, 19);
            this.comp13.TabIndex = 18;
            this.comp13.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp23
            // 
            this.comp23.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp23.DecimalPlaces = 2;
            this.comp23.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp23.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp23.Location = new System.Drawing.Point(284, 61);
            this.comp23.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp23.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp23.Name = "comp23";
            this.comp23.Size = new System.Drawing.Size(47, 19);
            this.comp23.TabIndex = 28;
            this.comp23.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp33
            // 
            this.comp33.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp33.DecimalPlaces = 2;
            this.comp33.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp33.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp33.Location = new System.Drawing.Point(284, 90);
            this.comp33.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp33.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp33.Name = "comp33";
            this.comp33.Size = new System.Drawing.Size(47, 19);
            this.comp33.TabIndex = 38;
            this.comp33.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp43
            // 
            this.comp43.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp43.DecimalPlaces = 2;
            this.comp43.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp43.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp43.Location = new System.Drawing.Point(284, 119);
            this.comp43.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp43.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp43.Name = "comp43";
            this.comp43.Size = new System.Drawing.Size(47, 19);
            this.comp43.TabIndex = 48;
            this.comp43.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp42
            // 
            this.comp42.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp42.DecimalPlaces = 2;
            this.comp42.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp42.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp42.Location = new System.Drawing.Point(231, 119);
            this.comp42.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp42.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp42.Name = "comp42";
            this.comp42.Size = new System.Drawing.Size(47, 19);
            this.comp42.TabIndex = 47;
            this.comp42.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp32
            // 
            this.comp32.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp32.DecimalPlaces = 2;
            this.comp32.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp32.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp32.Location = new System.Drawing.Point(231, 90);
            this.comp32.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp32.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp32.Name = "comp32";
            this.comp32.Size = new System.Drawing.Size(47, 19);
            this.comp32.TabIndex = 37;
            this.comp32.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp22
            // 
            this.comp22.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp22.DecimalPlaces = 2;
            this.comp22.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp22.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp22.Location = new System.Drawing.Point(231, 61);
            this.comp22.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp22.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp22.Name = "comp22";
            this.comp22.Size = new System.Drawing.Size(47, 19);
            this.comp22.TabIndex = 27;
            this.comp22.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp12
            // 
            this.comp12.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp12.DecimalPlaces = 2;
            this.comp12.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp12.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp12.Location = new System.Drawing.Point(231, 32);
            this.comp12.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp12.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp12.Name = "comp12";
            this.comp12.Size = new System.Drawing.Size(47, 19);
            this.comp12.TabIndex = 17;
            this.comp12.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp11
            // 
            this.comp11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp11.DecimalPlaces = 2;
            this.comp11.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp11.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp11.Location = new System.Drawing.Point(178, 32);
            this.comp11.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp11.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp11.Name = "comp11";
            this.comp11.Size = new System.Drawing.Size(47, 19);
            this.comp11.TabIndex = 6;
            this.comp11.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            this.comp11.Validating += new System.ComponentModel.CancelEventHandler(this.numericUpDown1_Validating);
            // 
            // comp21
            // 
            this.comp21.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp21.DecimalPlaces = 2;
            this.comp21.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp21.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp21.Location = new System.Drawing.Point(178, 61);
            this.comp21.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp21.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp21.Name = "comp21";
            this.comp21.Size = new System.Drawing.Size(47, 19);
            this.comp21.TabIndex = 26;
            this.comp21.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp31
            // 
            this.comp31.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp31.DecimalPlaces = 2;
            this.comp31.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp31.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp31.Location = new System.Drawing.Point(178, 90);
            this.comp31.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp31.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp31.Name = "comp31";
            this.comp31.Size = new System.Drawing.Size(47, 19);
            this.comp31.TabIndex = 36;
            this.comp31.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp41
            // 
            this.comp41.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comp41.DecimalPlaces = 2;
            this.comp41.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp41.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.comp41.Location = new System.Drawing.Point(178, 119);
            this.comp41.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comp41.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.comp41.Name = "comp41";
            this.comp41.Size = new System.Drawing.Size(47, 19);
            this.comp41.TabIndex = 46;
            this.comp41.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comp1
            // 
            this.comp1.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp1.Location = new System.Drawing.Point(124, 30);
            this.comp1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.comp1.Name = "comp1";
            this.comp1.Size = new System.Drawing.Size(49, 24);
            this.comp1.TabIndex = 55;
            this.comp1.Text = "0.00";
            this.comp1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.comp1.Click += new System.EventHandler(this.comp1_Click);
            // 
            // comp2
            // 
            this.comp2.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp2.Location = new System.Drawing.Point(124, 59);
            this.comp2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.comp2.Name = "comp2";
            this.comp2.Size = new System.Drawing.Size(49, 24);
            this.comp2.TabIndex = 56;
            this.comp2.Text = "0.00";
            this.comp2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // comp3
            // 
            this.comp3.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp3.Location = new System.Drawing.Point(124, 88);
            this.comp3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.comp3.Name = "comp3";
            this.comp3.Size = new System.Drawing.Size(49, 24);
            this.comp3.TabIndex = 57;
            this.comp3.Text = "0.00";
            this.comp3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // comp4
            // 
            this.comp4.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp4.Location = new System.Drawing.Point(124, 117);
            this.comp4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.comp4.Name = "comp4";
            this.comp4.Size = new System.Drawing.Size(49, 24);
            this.comp4.TabIndex = 58;
            this.comp4.Text = "0.00";
            this.comp4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // total
            // 
            this.total.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total.ForeColor = System.Drawing.Color.Blue;
            this.total.Location = new System.Drawing.Point(124, 160);
            this.total.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(64, 24);
            this.total.TabIndex = 59;
            this.total.Text = "0.00";
            this.total.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.total.TextChanged += new System.EventHandler(this.total_TextChanged);
            // 
            // factor
            // 
            this.factor.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.factor.Location = new System.Drawing.Point(9, 159);
            this.factor.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.factor.Name = "factor";
            this.factor.Size = new System.Drawing.Size(111, 25);
            this.factor.TabIndex = 61;
            this.factor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // close
            // 
            this.close.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.close.Location = new System.Drawing.Point(9, 198);
            this.close.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(70, 33);
            this.close.TabIndex = 149;
            this.close.Text = "Close";
            this.close.UseVisualStyleBackColor = true;
            this.close.Click += new System.EventHandler(this.close_Click);
            // 
            // saveAndClose
            // 
            this.saveAndClose.Enabled = false;
            this.saveAndClose.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveAndClose.Location = new System.Drawing.Point(83, 198);
            this.saveAndClose.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.saveAndClose.Name = "saveAndClose";
            this.saveAndClose.Size = new System.Drawing.Size(151, 33);
            this.saveAndClose.TabIndex = 148;
            this.saveAndClose.Text = "Update Components";
            this.saveAndClose.UseVisualStyleBackColor = true;
            this.saveAndClose.Click += new System.EventHandler(this.saveAndClose_Click);
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.message.ForeColor = System.Drawing.Color.Red;
            this.message.Location = new System.Drawing.Point(83, 243);
            this.message.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(0, 13);
            this.message.TabIndex = 150;
            // 
            // Components
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 262);
            this.ControlBox = false;
            this.Controls.Add(this.message);
            this.Controls.Add(this.close);
            this.Controls.Add(this.saveAndClose);
            this.Controls.Add(this.table);
            this.Controls.Add(this.total);
            this.Controls.Add(this.factor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Components";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Components";
            this.Load += new System.EventHandler(this.Components_Load);
            this.table.ResumeLayout(false);
            this.table.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comp19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp41)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel table;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button close;
        private System.Windows.Forms.Button saveAndClose;
        private System.Windows.Forms.NumericUpDown comp49;
        private System.Windows.Forms.NumericUpDown comp48;
        private System.Windows.Forms.NumericUpDown comp47;
        private System.Windows.Forms.NumericUpDown comp46;
        private System.Windows.Forms.NumericUpDown comp45;
        private System.Windows.Forms.NumericUpDown comp44;
        private System.Windows.Forms.NumericUpDown comp43;
        private System.Windows.Forms.NumericUpDown comp42;
        private System.Windows.Forms.NumericUpDown comp41;
        private System.Windows.Forms.NumericUpDown comp39;
        private System.Windows.Forms.NumericUpDown comp38;
        private System.Windows.Forms.NumericUpDown comp37;
        private System.Windows.Forms.NumericUpDown comp36;
        private System.Windows.Forms.NumericUpDown comp35;
        private System.Windows.Forms.NumericUpDown comp34;
        private System.Windows.Forms.NumericUpDown comp33;
        private System.Windows.Forms.NumericUpDown comp32;
        private System.Windows.Forms.NumericUpDown comp31;
        private System.Windows.Forms.NumericUpDown comp29;
        private System.Windows.Forms.NumericUpDown comp28;
        private System.Windows.Forms.NumericUpDown comp27;
        private System.Windows.Forms.NumericUpDown comp26;
        private System.Windows.Forms.NumericUpDown comp25;
        private System.Windows.Forms.NumericUpDown comp24;
        private System.Windows.Forms.NumericUpDown comp23;
        private System.Windows.Forms.NumericUpDown comp22;
        private System.Windows.Forms.NumericUpDown comp21;
        private System.Windows.Forms.NumericUpDown comp19;
        private System.Windows.Forms.NumericUpDown comp17;
        private System.Windows.Forms.NumericUpDown numericUpDown9;
        private System.Windows.Forms.NumericUpDown comp16;
        private System.Windows.Forms.NumericUpDown comp15;
        private System.Windows.Forms.NumericUpDown comp14;
        private System.Windows.Forms.NumericUpDown comp13;
        private System.Windows.Forms.NumericUpDown comp12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown comp11;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.Label comp1;
        private System.Windows.Forms.Label comp2;
        private System.Windows.Forms.Label comp3;
        private System.Windows.Forms.Label comp4;
        private System.Windows.Forms.Label total;
        private System.Windows.Forms.Label factor;
    }
}
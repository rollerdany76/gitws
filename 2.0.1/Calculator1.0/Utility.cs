﻿//using NLog;
using Calculator.Forms;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    class Utility
    {
        public static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public static bool stop = false;
        ListBox lbLog = null;

        public Utility()
        {
            Trace.Listeners.Add(new TextWriterTraceListener("RollArtCalc.log"));
            Trace.AutoFlush = true;
            //lbLog = (ListBox)main.Controls.Find("log", true)[0];
            
        }

        ///<summary> Leggo la password di Login per il data operator </summary>
        /// 0=password ok, 1=no righe, 2=password errata, 3=database error
        /// ROLE: 0 [Data Operator], 1 [Controller], 2[REFEREE]
        public static int CheckPassword(string username, string password, int role)
        {
            try
            {
                string passwordDB = "";
                string sql = "SELECT Password FROM AccessControl WHERE Username = '" + 
                    username.ToLower() + "' AND Role = '" + role + "'";
                // recupero password
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = sql;
                    SQLiteDataReader dr = command.ExecuteReader();
                    if (!dr.HasRows) return 1;
                    while (dr.Read())
                    {
                        passwordDB = dr[0].ToString(); // password

                        var passwordBytes = Encoding.UTF8.GetBytes(username.ToLower() + password);

                        // Hash la password con SHA256 
                        passwordBytes = SHA256.Create().ComputeHash(passwordBytes);
                        string hashed = BitConverter.ToString(passwordBytes).Replace("-","").ToLower();

                        if (!passwordDB.Equals(hashed))
                            return 2;
                    }

                    if (role == 0) MainMDI.loginDO = true;
                    else if (role == 1) MainMDI.loginAS = true;
                    else if (role == 2) MainMDI.loginRE = true;
                }
                return 0;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex.Message, "ERROR");
                return 3;
            }
        }


        public static void WriteLog(string text, string messageType)
        {
            try
            {
                if (messageType.Equals("ERROR"))
                {
                    Trace.TraceError(text);
                    logger.Log(LogLevel.Error, text);
                }
                else if (messageType.Equals("WARNING"))
                {
                    Trace.TraceWarning(text);
                    logger.Log(LogLevel.Warn, text);
                }
                else if (messageType.Equals("Ok"))
                {
                    Trace.TraceInformation(text);
                    logger.Log(LogLevel.Info, text);
                }
                else if (messageType.Equals("DEBUG"))
                {
                    logger.Log(LogLevel.Debug, text);
                }
                else logger.Log(LogLevel.Trace, text);
            }
            catch (Exception)
            { }
        }

        public static String GetPercentage(Decimal value, Decimal total, Int32 places)
        {
            Decimal percent = 0;
            String retval = string.Empty;
            String strplaces = new String('0', places);

            if (value == 0 || total == 0)
            {
                percent = 0;
            }

            else
            {
                percent = Decimal.Divide(value, total) * 100 - 100;
                if (places > 0)
                {
                    strplaces = "." + strplaces;
                }
            }

            retval = percent.ToString("#" + strplaces);
            return retval;
        }

        public static string FormattaOggettoDecInString(object valoreDecimale)
        {
            string valoreFormattato = "";
            double dbValue;
            try
            {
                NumberFormatInfo nfi = new NumberFormatInfo();
                nfi.PercentDecimalDigits = 2;
                decimal numDecimale;
                bool res = decimal.TryParse(valoreDecimale.ToString(), out numDecimale);
                if (res == false)
                {
                    // String is not a number or empty
                    return valoreFormattato;
                }
                dbValue = double.Parse(valoreDecimale.ToString());
                valoreFormattato = dbValue.ToString("N2", nfi);
                return valoreFormattato;
            }
            catch (Exception)
            {
                valoreFormattato = valoreDecimale.ToString().Replace(",", ".");
                return valoreFormattato;
            }
        }

        public static decimal FormattaOggettoDec(object valoreDecimale)
        {
            string valoreFormattato;
            double dbValue;
            try
            {
                NumberFormatInfo nfi = new NumberFormatInfo();
                nfi.PercentDecimalDigits = 2;
                decimal numDecimale;
                //bool res = decimal.TryParse(valoreDecimale.ToString(), out numDecimale);
                //if (res == false)
                //{
                //    // String is not a number or empty
                //    return valoreFormattato;
                //}
                dbValue = double.Parse(valoreDecimale.ToString());
                valoreFormattato = dbValue.ToString("N2", nfi);
                return decimal.Parse(valoreFormattato);
            }
            catch (Exception)
            {
                //valoreFormattato = valoreDecimale.ToString().Replace(",", ".");
                return 0;
            }
        }

        public static decimal Highest(params decimal[] inputs)
        {
            return inputs.Max();
        }

        public static decimal Lowest(params decimal[] inputs)
        {
            return inputs.Min();
        }

    }

    public class SkaterInfo
    {
        ///<summary> identificativo atleta </summary>
        public string idskater { get; set; }
        ///<summary> ordine d'entrata segmento in corso </summary>
        public string order { get; set; }
        public string name { get; set; }
        public string club { get; set; }
        public string nation { get; set; }
        public string region { get; set; }
        public string discipline { get; set; }

        public string base_value { get; set; }
        public string tech_score { get; set; }
        public string pres_score { get; set; }
        public string deduction { get; set; }
        public string seg_score { get; set; }
        public string seg_rank { get; set; }
        public string final_score { get; set; }
        public string final_rank { get; set; }

        public string new_tech_score { get; set; }
        public string new_pres_score { get; set; }
        public string new_deduction { get; set; }
        public string new_seg_score { get; set; }
        public string new_seg_rank { get; set; }
        public string new_final_score { get; set; }
        public string new_final_rank { get; set; }

        public DataTable elements { get; set; }

        public bool updated { get; set; }
    }

    public class Skaters
    {
        public string numskaters { get; set; }
        public List<SkaterInfo> list { get; set; }
        public string currentIdSkater { get; set; }
    }

    public class Segment
    {
        public string idsegment { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string status { get; set; }
        public SkaterInfo currentSkater { get; set; }
        public decimal factor { get; set; }
    }

    public class Giudice
    {
        public string idjudge { get; set; }
        public string num { get; set; }
        public string name { get; set; }
        public string role { get; set; }
        public string nation { get; set; }
    }

    public class Judges
    {
        public string numJudges { get; set; }
        public List<Giudice> list { get; set; }
    }

    public class Category
    {
        public string idcategory { get; set; }
        public string name { get; set; }
    }

    public class Discipline
    {
        public string iddiscipline { get; set; }
        public string name { get; set; }
        public string code { get; set; }
    }

    public class Event
    {
        public string name { get; set; }
        public string place { get; set; }
        public string date { get; set; }
        public string idevent { get; set; }
        public string description { get; set; }
        public Discipline discipline { get; set; }
        public Category category { get; set; }
        public List<Segment> segment { get; set; }
        public Judges judges { get; set; }
        public string currentSegment { get; set; }

    }

}

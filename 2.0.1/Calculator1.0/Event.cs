﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SQLite;

namespace RollartSystemTech
{
    class Event
    {
        SQLiteDataReader dr = null;

        public Event()
        {

        }

        public ArrayList GetSpecialita()
        {
            try
            {
                ArrayList spec = new ArrayList();
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT * FROM Specialita";
                    dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        spec.Add(dr[2]); // ID_Specialità;Name
                    }
                }
                return spec;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex.Message, "ERROR");
                return null;
            }
        }

        public ArrayList GetSegments()
        {
            try
            {
                ArrayList alSegm = new ArrayList();
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT ID_Segments, Name FROM Segments";
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        alSegm.Add(dr[1]);
                    }
                    dr.Close();
                }
                return alSegm;
            }
            catch (SQLiteException ex)
            {
                Utility.WriteLog("GetSegments: " + ex.Message, "ERROR");
                return null;
            }
        }

        public ArrayList GetCategory()
        {
            try
            {
                ArrayList alCat = new ArrayList();
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT Name FROM Category";
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        alCat.Add(dr[0]);
                    }
                    dr.Close();
                }
                return alCat;
            }
            catch (SQLiteException ex)
            {
                Utility.WriteLog("GetCategory: " + ex.Message, "ERROR");
                return null;
            }
        }

        public ArrayList GetCatElements()
        {
            try
            {
                ArrayList alCat = new ArrayList();
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT Name FROM ElementsCat";
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        alCat.Add(dr[0]);
                    }
                    dr.Close();
                }
                return alCat;
            }
            catch (SQLiteException ex)
            {
                Utility.WriteLog("GetCatElements: " + ex.Message, "ERROR");
                return null;
            }
        }
    }

    #region Gara, Skater, SkaterList
    public class Competition
    {
        public int idGara { get; set; }
        public int idDiscipline { get; set; }
        public int idCategory { get; set; }
        public int idSegment1 { get; set; }
        public int idSegment2 { get; set; }
        public int idSegment3 { get; set; }
        public int idSegment4 { get; set; }

        public string name { get; set; }
        public string place { get; set; }
        public string datefrom { get; set; }
        public string dateto { get; set; }
        public string description { get; set; }
        public string sex { get; set; }

        public string discipline { get; set; }
        public string category { get; set; }
        public string segment1 { get; set; }
        public string segment2 { get; set; }
        public string segment3 { get; set; }
        public string segment4 { get; set; }

        public int numJudges { get; set; }
        public List<Official> listOfficials { get; set; }

        public int numSkaters { get; set; }
        public List<Skater> list { get; set; }

    }

    public class Official
    {
        ///<summary> identificativo giudice </summary>
        public int idofficial { get; set; }
        ///<summary> nome giudice </summary>
        public string name { get; set; }
        ///<summary> ruolo giudice </summary>
        public string role { get; set; }
    }

    public class Skater
    {
        ///<summary> identificativo atleta </summary>
        public int idskater { get; set; }
        ///<summary> ordine d'entrata segmento in corso </summary>
        public int order { get; set; }
        public string name { get; set; }
        public string club { get; set; }
        public string nation { get; set; }
        public string region { get; set; }
        public string discipline { get; set; }

    }

    public class SkaterList
    {
        public int numSkater { get; set; }
        public List<Skater> list { get; set; }
        public static Skater GetSkater(int order, SkaterList allSkaters)
        {
            foreach (Skater sk in allSkaters.list)
            {
                if (sk.order == order)
                    return sk;
            }
            return null;
        }
    }
    #endregion

}

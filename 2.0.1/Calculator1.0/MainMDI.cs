﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class MainMDI : Form
    {
        EventList cp = null;
        Utility ut = null;
        Thread workerThread;
        bool flagClose = false;
        //public static bool loginDO = false, loginAS = false, loginRE = false;
        public static bool loginDO = true, loginAS = true, loginRE = true;
        bool verifiedAS = false, verifiedRE = false;

        public MainMDI()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
        }

        // Exit
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            CloseConnection();
            System.Environment.Exit(0);
        }

        private void Main_Load(object sender, EventArgs e)
        {
            try
            {
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

                OpenConnection();

                // Login del Data Operator
                if (!loginDO)
                {
                    SplashScreen sc = new SplashScreen(0);
                    sc.ShowDialog();
                }

                if (!loginDO)
                    Application.Exit();
                tsDO.Image = RollartReview.Properties.Resources.bullet_green;

                ut = new Utility();

                workerThread = new Thread(AggiornaStatus);
                workerThread.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "RollArt System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void AggiornaStatus()
        {
            while (true)
            {
                
                if (!verifiedAS && loginAS)
                {
                    tsCO.Image = RollartReview.Properties.Resources.bullet_green;
                    verifiedAS = true;
                }
                if (!verifiedRE && loginRE)
                {
                    tsRE.Image = RollartReview.Properties.Resources.bullet_green;
                    verifiedRE = true;
                }
                Thread.Sleep(2000);
                if (flagClose)
                    break;
            }
        }

        // Open Competitions
        private void allDisciplinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenCompetitions(((ToolStripMenuItem)sender).Tag.ToString(),
                ((ToolStripMenuItem)sender).Text);
        }

        private void OpenCompetitions(string listaDisc, string nomeDisciplina)
        {
            this.LayoutMdi(System.Windows.Forms.MdiLayout.Cascade);
            cp = new EventList(listaDisc, nomeDisciplina);
            cp.MdiParent = this;

            cp.Show();
        }

        # region DB CONNECTION
        public void OpenConnection()
        {
            try
            {
                string connString = RollartReview.Properties.Settings.Default.dbconnection;
                Definizioni.conn = new SQLiteConnection(connString);
                Definizioni.conn.Open();
                tsDatabase.Text = connString + " [Connected]";
                Utility.WriteLog(Definizioni.conn.DataSource + " - " + Definizioni.conn.ConnectionString, "Ok");
                tsDatabase.Image = RollartReview.Properties.Resources.bullet_green;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex.Message, "ERROR");
                tsDatabase.Text = "Database [Not connected]";
                tsDatabase.Image = RollartReview.Properties.Resources.bullet_red;
            }
        }

        public void CloseConnection()
        {
            try
            {
                Definizioni.conn.Close();
                Utility.WriteLog(Definizioni.conn.DataSource + " - " + Definizioni.conn.ConnectionString, "Ok");
                tsDatabase.Text = "Database [Not connected]";
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex.Message, "ERROR");

            }
        }
        #endregion

        private void listToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void MainMDI_FormClosing(object sender, FormClosingEventArgs e)
        {
            flagClose = true;
        }

        private void logToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }


        // cambio il database
        private void toolStripSplitButton1_ButtonClick(object sender, EventArgs e)
        {
            
            string oldConnString = "", newConnString = "", oldFileDbPath = "";
            try
            {
                //string dsDB = Definizioni.conn.DataSource;
                of1.Filter = "File SQLite| *.s3db";
                of1.FileName = "";
                of1.Title = "Open a Rollart database...";
                if (of1.ShowDialog() == DialogResult.OK)
                {
                    // verifico che sia un database Rollart

                    oldConnString = RollartReview.Properties.Settings.Default.dbconnection;
                    newConnString = "Data Source = " + of1.FileName + "; Version = 3;";
                    try
                    {
                        // modifico la chiave dbConnection delle Settings 
                        RollartReview.Properties.Settings.Default.dbconnection = newConnString;
                        RollartReview.Properties.Settings.Default.Save();
                        oldFileDbPath = oldConnString.Split('=')[1].TrimStart().Split(';')[0].TrimEnd();
                    }
                    catch (Exception)
                    {
                        return;
                    }

                    // chiudo il db rolljudge2
                    Definizioni.conn.Close();
                    Definizioni.conn.Dispose();
                    Definizioni.conn = null;

                    // apro il nuovo database
                    OpenConnection();
                    Thread.Sleep(500);

                    DisposeAllButThis(this);
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("Restore database:" + ex.Message, "ERROR");
            }
        }

        public void DisposeAllButThis(Form form)
        {
            foreach (Form frm in this.MdiChildren)
            {
                frm.Dispose();
                frm.Close();
                
            }
        }
    }
}

﻿namespace Calculator
{
    partial class MainMDI
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMDI));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allDisciplinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.freeSkatingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pairsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.coupleDanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.soloDanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.precisionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.status = new System.Windows.Forms.StatusStrip();
            this.tsDatabase = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsDO = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsCO = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsRE = new System.Windows.Forms.ToolStripStatusLabel();
            this.of1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1.SuspendLayout();
            this.status.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.databaseToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.toolStripMenuItem2});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(977, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(53, 20);
            this.toolStripMenuItem1.Text = "Events";
            // 
            // listToolStripMenuItem
            // 
            this.listToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.allDisciplinesToolStripMenuItem,
            this.freeSkatingToolStripMenuItem,
            this.pairsToolStripMenuItem,
            this.coupleDanceToolStripMenuItem,
            this.soloDanceToolStripMenuItem,
            this.precisionToolStripMenuItem});
            this.listToolStripMenuItem.Name = "listToolStripMenuItem";
            this.listToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.listToolStripMenuItem.Text = "List";
            this.listToolStripMenuItem.Click += new System.EventHandler(this.listToolStripMenuItem_Click);
            // 
            // allDisciplinesToolStripMenuItem
            // 
            this.allDisciplinesToolStripMenuItem.Name = "allDisciplinesToolStripMenuItem";
            this.allDisciplinesToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.allDisciplinesToolStripMenuItem.Tag = "0";
            this.allDisciplinesToolStripMenuItem.Text = "All disciplines";
            this.allDisciplinesToolStripMenuItem.Click += new System.EventHandler(this.allDisciplinesToolStripMenuItem_Click);
            // 
            // freeSkatingToolStripMenuItem
            // 
            this.freeSkatingToolStripMenuItem.Name = "freeSkatingToolStripMenuItem";
            this.freeSkatingToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.freeSkatingToolStripMenuItem.Tag = "1,2";
            this.freeSkatingToolStripMenuItem.Text = "Free Skating";
            this.freeSkatingToolStripMenuItem.Click += new System.EventHandler(this.allDisciplinesToolStripMenuItem_Click);
            // 
            // pairsToolStripMenuItem
            // 
            this.pairsToolStripMenuItem.Name = "pairsToolStripMenuItem";
            this.pairsToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.pairsToolStripMenuItem.Tag = "3";
            this.pairsToolStripMenuItem.Text = "Pairs";
            this.pairsToolStripMenuItem.Click += new System.EventHandler(this.allDisciplinesToolStripMenuItem_Click);
            // 
            // coupleDanceToolStripMenuItem
            // 
            this.coupleDanceToolStripMenuItem.Name = "coupleDanceToolStripMenuItem";
            this.coupleDanceToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.coupleDanceToolStripMenuItem.Tag = "4";
            this.coupleDanceToolStripMenuItem.Text = "Couple Dance";
            this.coupleDanceToolStripMenuItem.Click += new System.EventHandler(this.allDisciplinesToolStripMenuItem_Click);
            // 
            // soloDanceToolStripMenuItem
            // 
            this.soloDanceToolStripMenuItem.Name = "soloDanceToolStripMenuItem";
            this.soloDanceToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.soloDanceToolStripMenuItem.Tag = "5,6";
            this.soloDanceToolStripMenuItem.Text = "Solo Dance";
            this.soloDanceToolStripMenuItem.Click += new System.EventHandler(this.allDisciplinesToolStripMenuItem_Click);
            // 
            // precisionToolStripMenuItem
            // 
            this.precisionToolStripMenuItem.Name = "precisionToolStripMenuItem";
            this.precisionToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.precisionToolStripMenuItem.Tag = "7";
            this.precisionToolStripMenuItem.Text = "Precision";
            this.precisionToolStripMenuItem.Click += new System.EventHandler(this.allDisciplinesToolStripMenuItem_Click);
            // 
            // databaseToolStripMenuItem
            // 
            this.databaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem});
            this.databaseToolStripMenuItem.Name = "databaseToolStripMenuItem";
            this.databaseToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.databaseToolStripMenuItem.Text = "Database";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.newToolStripMenuItem.Text = "New...";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.toolStripSplitButton1_ButtonClick);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Visible = false;
            // 
            // logToolStripMenuItem
            // 
            this.logToolStripMenuItem.Name = "logToolStripMenuItem";
            this.logToolStripMenuItem.Size = new System.Drawing.Size(94, 22);
            this.logToolStripMenuItem.Text = "Log";
            this.logToolStripMenuItem.Click += new System.EventHandler(this.logToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(37, 20);
            this.toolStripMenuItem2.Text = "&Exit";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // status
            // 
            this.status.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.status.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsDatabase,
            this.tsDO,
            this.tsCO,
            this.tsRE});
            this.status.Location = new System.Drawing.Point(0, 605);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(977, 25);
            this.status.TabIndex = 2;
            this.status.Text = "statusStrip1";
            // 
            // tsDatabase
            // 
            this.tsDatabase.Image = global::RollartReview.Properties.Resources.bullet_red;
            this.tsDatabase.Name = "tsDatabase";
            this.tsDatabase.Size = new System.Drawing.Size(75, 20);
            this.tsDatabase.Text = "Database";
            // 
            // tsDO
            // 
            this.tsDO.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.tsDO.Image = ((System.Drawing.Image)(resources.GetObject("tsDO.Image")));
            this.tsDO.Name = "tsDO";
            this.tsDO.Size = new System.Drawing.Size(107, 20);
            this.tsDO.Text = "Data Operator";
            // 
            // tsCO
            // 
            this.tsCO.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.tsCO.Image = ((System.Drawing.Image)(resources.GetObject("tsCO.Image")));
            this.tsCO.Name = "tsCO";
            this.tsCO.Size = new System.Drawing.Size(83, 20);
            this.tsCO.Text = "Controller";
            // 
            // tsRE
            // 
            this.tsRE.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.tsRE.Image = ((System.Drawing.Image)(resources.GetObject("tsRE.Image")));
            this.tsRE.Name = "tsRE";
            this.tsRE.Size = new System.Drawing.Size(73, 20);
            this.tsRE.Text = "Referee";
            // 
            // of1
            // 
            this.of1.FileName = "rolljudge2";
            this.of1.Filter = "File SQLite|*.s3db";
            this.of1.InitialDirectory = "C:\\RollartSystem";
            this.of1.RestoreDirectory = true;
            this.of1.Title = "Restore database";
            // 
            // MainMDI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::RollartReview.Properties.Resources.pastedGraphic;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(977, 630);
            this.Controls.Add(this.status);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainMDI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RollArt Review";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainMDI_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.status.ResumeLayout(false);
            this.status.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.StatusStrip status;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allDisciplinesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem freeSkatingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pairsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem coupleDanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem soloDanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem precisionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripStatusLabel tsDatabase;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog of1;
        private System.Windows.Forms.ToolStripMenuItem databaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel tsDO;
        private System.Windows.Forms.ToolStripStatusLabel tsCO;
        private System.Windows.Forms.ToolStripStatusLabel tsRE;
    }
}


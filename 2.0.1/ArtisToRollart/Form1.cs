﻿using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArtisToRollart
{
    public partial class Form1 : Form
    {
        NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        SQLiteConnection conn = null, connArtis = null;
        SQLiteTransaction transaction = null, transactionArtis = null;
        string[] sqlArtis = null;
        string idManifCurrent = "", messaggio = "", path = "";
        bool garaConclusa = false, garaInserita = false;
        ArrayList arClassifica = null;

        List<GaraParams> listaGare = null;

        int idManif = 0;

        public Form1()
        {
            InitializeComponent();

            Trace.Listeners.Add(new TextWriterTraceListener("ArtisToRollart.log"));
            Trace.AutoFlush = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                OpenConnection();
            }
            catch (Exception ex)
            {
                message.Text += "Carico Evento Artis: " + ex.Message + "\r\n";
            }
        }
        
        #region Esporta gare verso Rollart
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (tvManif.SelectedNode == null) return;
                // se non ci sono manifestazioni nel db
                if (tvManif.Nodes.Count == 0)
                {
                    MessageBox.Show("Non ci sono manifestazioni Artis in archivio. ",
                        "ArtisToRollart", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                // se nessuna gara è stata selezionata
                bool nessunaGaraSelezionata = true;
                foreach (TreeNode tn in tvManif.Nodes)
                {
                    if (tn.Checked)
                    {
                        nessunaGaraSelezionata = false;
                        break;
                    }
                    foreach (TreeNode nodi in tn.Nodes)
                    {
                        if (nodi.Checked)
                        {
                            nessunaGaraSelezionata = false;
                            break;
                        }
                    }
                }
                if (nessunaGaraSelezionata)
                {
                    MessageBox.Show("Seleziona una o più gare da esportare verso Rollart.",
                        "ArtisToRollart", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                messaggio = "";

                transaction = conn.BeginTransaction();
                transactionArtis = connArtis.BeginTransaction();

                garaInserita = false;

                foreach (TreeNode n in tvManif.Nodes)
                {
                    PrintRecursive(n);
                }
                if (garaInserita && !messaggio.Equals(""))
                    MessageBox.Show("La seguenti gare sono state già inserite in precedenza e non verranno quindi esportate in Rollart:\r\n\r\n" +
                                messaggio, "ArtisToRollart", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                transaction.Commit();
                transactionArtis.Commit();
            }
            catch (Exception ex)
            {
                message.Text += "Esporta gara in Rollart: " + ex.Message + "\r\n";
                if (transaction != null) transaction.Rollback();
                if (transactionArtis != null) transactionArtis.Rollback();

            }
        }

        public void ExportToRollart(GaraParams gara, TreeNode tn)
        {
            try
            {
                int idGaraParams = 0;
                garaInserita = false;
                // verifico se una gara con lo stesso nome,categoria,specialita gia esiste
                SQLiteCommand cmd = new SQLiteCommand(
                    "SELECT ID_GaraParams, ID_Segment FROM GaraParams " + 
                    "WHERE Name like '%" + gara.name.Replace('/','-') + "%'" +
                    "  AND Place = '" + gara.place + "'" +
                    "  AND ID_Category = " + gara.idcategory + "" +
                    "  AND ID_Specialita = " + gara.idspecialita + "", conn);
                SQLiteDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        // verifico se gara compreso segmento gia inserita
                        if (dr[1].ToString().Equals(gara.idsegment))
                        {
                            garaInserita = true;
                            messaggio += " - " + gara.nomeDisciplina + " " +
                                         gara.nomeCategoria + " " +
                                         gara.nomeSegmento + "\r\n";
                            tn.Checked = false;
                            tn.BackColor = Color.Orange;
                            
                            return;
                        }
                        idGaraParams = int.Parse(dr[0].ToString());
                        garaInserita = true;
                    }
                }
                else
                {
                    // Recupero l'id
                    cmd = new SQLiteCommand("select MAX(ID_GaraParams) from GaraParams", conn);
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        if (dr[0].ToString().Equals("")) idGaraParams = 1;
                        else idGaraParams = dr.GetInt32(0) + 1;
                    }
                }

                using (SQLiteCommand command = conn.CreateCommand())
                {
                    command.Transaction = transaction;

                    int idCategoria = int.Parse(gara.idcategory);
                    int idSpecialità = int.Parse(gara.idspecialita);
                    int idSegment = int.Parse(gara.idsegment);

                    string dataInizio = "", dataFine = "";
                    if (gara.date.Contains("-"))
                    {
                        dataInizio = gara.date.Split('-')[0].TrimEnd();
                        dataFine = gara.date.Split('-')[1].TrimStart();
                    } else dataInizio = gara.date;
                    command.CommandText = "INSERT INTO GaraParams(ID_GaraParams,Name,Place,Date,DateEnd,ID_Segment,ID_Specialita,ID_Category,Partecipants,Sex,Completed,LastPartecipant,NumJudges)" +
                        " VALUES(@idgara,@name,@place,@date,@dateend,@idsegment,@spec,@category,@part,@sex,'N',0,@judges)";
                    command.Parameters.AddWithValue("@idgara", idGaraParams);
                    //
                    string nomeEvento = ("[" + gara.nomeDisciplina + " "
                                            + gara.nomeCategoria + " "
                                            + "] " + gara.name).TrimEnd();
                    command.Parameters.AddWithValue("@name", nomeEvento.Replace('/', '-'));
                    command.Parameters.AddWithValue("@place", gara.place.TrimEnd());
                    command.Parameters.AddWithValue("@date", dataInizio);
                    command.Parameters.AddWithValue("@dateend", dataFine);
                    command.Parameters.AddWithValue("@part", gara.atleti.Count);
                    command.Parameters.AddWithValue("@judges", gara.numjudges);
                    command.Parameters.AddWithValue("@spec", gara.idspecialita);
                    command.Parameters.AddWithValue("@sex", gara.sex);
                    command.Parameters.AddWithValue("@idsegment", gara.idsegment);
                    command.Parameters.AddWithValue("@category", idCategoria);

                    command.ExecuteNonQuery();

                    // isnerisco gli atleti
                    int[] ids = new int[gara.atleti.Count];
                    
                    command.CommandText = "INSERT INTO Athletes(Name, Societa, Country, Region, ID_Specialita)" +
                            " VALUES(@param1,@param2,@param3,@param4,@param5)";
                   
                    int k = 0;
                    for (int i = 0; i < gara.atleti.Count; i++)
                    {
                        command.Parameters.AddWithValue("@param1", gara.atleti[i].nomeRollart.TrimEnd()); // nome
                        command.Parameters.AddWithValue("@param2", gara.atleti[i].societaRollart.TrimEnd()); // societa
                        command.Parameters.AddWithValue("@param3", "ITA"); // nazione
                        command.Parameters.AddWithValue("@param4", ""); // region
                        command.Parameters.AddWithValue("@param5", gara.idspecialita); // spec
                        try
                        {
                            command.ExecuteNonQuery();
                        }
                        catch (Exception)
                        {
                            //MessageBox.Show("Athlete " + lb.Items[i].SubItems[1].Text + " already inserted.");
                        }

                        // recupero l'ID dell'Atleta
                        
                        cmd = new SQLiteCommand("select ID_Atleta from Athletes " +
                            " WHERE Name = '" + gara.atleti[i].nomeRollart.TrimEnd().Replace("'", "''") + "'" +
                            " AND Societa = '" + gara.atleti[i].societaRollart.TrimEnd().Replace("'", "''") + "'" +
                            " AND Country = 'ITA'" +
                            " AND Region = ''" +
                            " AND ID_Specialita = " + gara.idspecialita + "", conn);
                       
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            ids[k] = int.Parse(dr[0].ToString());
                            k++;
                            break;

                        }
                    }
                    command.CommandText = "INSERT INTO Participants(ID_GaraParams,ID_Segment,ID_Atleta,NumStartingList)" +
                                        " VALUES(@param1,@param2,@param3,@param4)";
                    command.Parameters.AddWithValue("@param1", idGaraParams);
                    command.Parameters.AddWithValue("@param2",idSegment); // id_segment 
                    int ordine = 1;
                    for (int i = 0; i < gara.atleti.Count; i++)
                    {
                        command.Parameters.AddWithValue("@param3", ids[i]); // ID_Atleta
                        if (gara.atleti[i].ordineentrata.Equals("0"))
                            gara.atleti[i].ordineentrata = ordine + "";
                        command.Parameters.AddWithValue("@param4", gara.atleti[i].ordineentrata); // NumStartingList
                        command.ExecuteNonQuery();
                        ordine++;
                    }

                    // inserisco giudici
                    if (!garaInserita)
                    {
                        int[] idGiudici = new int[gara.pannello.Count];

                        command.CommandText = "INSERT INTO Judges(Name, Region, Country)" +
                                " VALUES(@param1,@param2,'')";
                        int b = 0;
                        for (int i = 0; i < gara.pannello.Count; i++)
                        {
                            if (!gara.pannello[i].nome.Equals(""))
                            {
                                cmd = new SQLiteCommand("select ID_Judge from Judges " +
                                    " WHERE Name = '" + gara.pannello[i].nome.TrimEnd().Replace("'", "''") + "'" +
                                    " AND Region = '" + gara.pannello[i].sezione.TrimEnd().Replace("'", "''") + "'", conn);
                                dr = cmd.ExecuteReader();
                                if (dr.HasRows) // se esiste recupero solo idGiudice
                                {
                                    while (dr.Read())
                                    {
                                        idGiudici[b] = int.Parse(dr[0].ToString());
                                        b++;
                                        break;
                                    }
                                }
                                else // altrimenti inserisco e poi recuper id
                                {
                                    dr.Close();
                                    command.Parameters.AddWithValue("@param1", gara.pannello[i].nome.TrimEnd()); // nome giudice
                                    command.Parameters.AddWithValue("@param2", gara.pannello[i].sezione.TrimEnd()); // sezione giudice
                                    try
                                    {
                                        command.ExecuteNonQuery();
                                    }
                                    catch (Exception)
                                    {
                                        //MessageBox.Show("Judge " + Judges.judges[i].ToString() + " already inserted.");
                                    }

                                    // recupero l'ID del Giudice
                                    cmd = new SQLiteCommand("select ID_Judge from Judges " +
                                    " WHERE Name = '" + gara.pannello[i].nome.TrimEnd().Replace("'", "''") + "'" +
                                    " AND Region = '" + gara.pannello[i].sezione.TrimEnd().Replace("'", "''") + "'", conn);
                                    dr = cmd.ExecuteReader();
                                    while (dr.Read())
                                    {
                                        idGiudici[b] = int.Parse(dr[0].ToString());
                                        b++;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                idGiudici[b] = 0;
                                b++;
                            }
                        }

                        command.CommandText = "INSERT INTO PanelJudge(ID_Judge, ID_GaraParams,Role)" +
                            " VALUES(@param3,@param1,@param2)";
                        command.Parameters.AddWithValue("@param1", idGaraParams);
                        for (int i = 0; i < gara.pannello.Count; i++)
                        {
                            if (!gara.pannello[i].nome.Equals(""))
                            {
                                command.Parameters.AddWithValue("@param2", "");
                                if (gara.pannello[i].ruolo == "-90" || gara.pannello[i].ruolo == "-20") command.Parameters.AddWithValue("@param2", "Specialist"); // role
                                else if (gara.pannello[i].ruolo == "-93" || gara.pannello[i].ruolo == "-5") command.Parameters.AddWithValue("@param2", "Data Operator"); // role
                                else if (gara.pannello[i].ruolo == "-91" || gara.pannello[i].ruolo == "-19") command.Parameters.AddWithValue("@param2", "Assistant"); // role
                                else if (gara.pannello[i].ruolo == "-92" || gara.pannello[i].ruolo == "") command.Parameters.AddWithValue("@param2", "Controller"); // role
                                else if (gara.pannello[i].ruolo == "-95" || gara.pannello[i].ruolo == "") command.Parameters.AddWithValue("@param2", "Referee"); // role
                                else if (int.Parse(gara.pannello[i].ruolo) > 0)
                                    command.Parameters.AddWithValue("@param2", "Judge " + gara.pannello[i].ruolo); // role
                                if (!command.Parameters["@param2"].Value.Equals(""))
                                {
                                    command.Parameters.AddWithValue("@param3", idGiudici[i]); // name
                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                    tn.BackColor = Color.LightGreen;
                }

                // inserisco id_garaparams e id_segment in rollart
                using (SQLiteCommand command = connArtis.CreateCommand())
                {
                    command.Transaction = transactionArtis;

                    command.CommandText = "UPDATE art_manif_gare SET ID_GaraParams = @idparams, ID_Segment = @idsegm " +
                        "WHERE id_manif = @param1 AND id = @param2";
                    command.Parameters.AddWithValue("@param1", tn.Parent.Name);//idmanif
                    command.Parameters.AddWithValue("@param2", gara.idattivita);
                    command.Parameters.AddWithValue("@idparams", idGaraParams);
                    command.Parameters.AddWithValue("@idsegm", gara.idsegment);
                    command.ExecuteNonQuery();
                }
            }
            catch (SQLiteException ex)
            {
                message.Text = ex.Message;
                
                if (transaction != null)
                {
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (SQLiteException ex2)
                    {

                    }
                    finally
                    {
                        transaction.Dispose();
                    }
                }
                    
            }
        }

        private void PrintRecursive(TreeNode treeNode)
        {
            // Print each node recursively.  
            foreach (TreeNode tn in treeNode.Nodes)
            {
                if (tn.Checked)
                {
                    ExportToRollart((GaraParams)tn.Tag, tn);
                    PrintRecursive(tn);
                }
            }
        }
        #endregion

        #region Importa Risultati Rollart
        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                if (tvManif.SelectedNode == null) return;
                // se non ci sono manifestazioni nel db
                if (tvManif.Nodes.Count == 0)
                {
                    MessageBox.Show("Non ci sono manifestazioni Artis in archivio. ",
                        "ArtisToRollart", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                // se nessuna gara è stata selezionata
                bool nessunaGaraSelezionata = true;
                foreach (TreeNode tn in tvManif.Nodes)
                {
                    if (tn.Checked)
                    {
                        nessunaGaraSelezionata = false;
                        break;
                    }
                    foreach (TreeNode nodi in tn.Nodes)
                    {
                        if (nodi.Checked)
                        {
                            nessunaGaraSelezionata = false;
                            break;
                        }
                    }
                }
                if (nessunaGaraSelezionata)
                {
                    MessageBox.Show("Seleziona una o più gare.",
                        "ArtisToRollart", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                // creo la directory 
                messaggio = "";
                foreach (TreeNode n in tvManif.Nodes)
                {
                    PrintRecursiveResults(n);
                }
                if (!garaConclusa)
                    MessageBox.Show("Le seguenti gare non si sono ancora concluse:\r\n\r\n" + messaggio,
                               "ArtisToRollart", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                // verifica dei file sql creati
                System.Diagnostics.Process.Start(Application.StartupPath);
            }
            catch (Exception ex)
            {
                message.Text += "Esporta risultati Rollart: " + ex.Message + "\r\n";
            }
        }

        private void PrintRecursiveResults(TreeNode treeNode)
        {
            // Print each node recursively.  
            foreach (TreeNode tn in treeNode.Nodes)
            {
                if (tn.Checked && tn.Level == 1)
                {
                    CreoDirectoryEvento(int.Parse(tn.Parent.Name));
                    ExportResults((GaraParams)tn.Tag, tn);

                    path = Path.Combine(path, ((GaraParams)tn.Tag).idattivita + ".sql");
                    File.WriteAllText(path, String.Empty);
                    using (StreamWriter sw = File.AppendText(path))
                    {
                        foreach (string atleta in arClassifica)
                        {
                            sw.WriteLine(atleta);
                        }
                    }
                    PrintRecursiveResults(tn);
                } 
            }
        }

        public void ExportResults(GaraParams gara, TreeNode tn)
        {
            try
            {
                int idGaraParams = 0, idSeg = 0;
                garaConclusa = false;
                SQLiteDataReader dr = null;
                SQLiteDataReader drArtis = null;
                arClassifica = new ArrayList();

                // recupero id_garaparams e id_segment 
                using (SQLiteCommand command = connArtis.CreateCommand())
                {
                    command.CommandText =
                    "SELECT id_garaparams,id_segment FROM art_manif_gare WHERE " + 
                    " id_manif = " + tn.Parent.Name + " AND id = " + gara.idattivita;

                    drArtis = command.ExecuteReader();
                    while (drArtis.Read())
                    {
                        idGaraParams = int.Parse(drArtis[0].ToString());
                        idSeg = int.Parse(drArtis[1].ToString());
                    }
                }

                using (SQLiteCommand command = conn.CreateCommand())
                {
                    command.CommandText =
                    "SELECT * FROM GaraParams WHERE id_garaparams = " + idGaraParams + 
                    " AND id_segment = " + idSeg + " AND Completed = 'Y'";
                    dr = command.ExecuteReader();
                    if (dr.HasRows)
                        garaConclusa = true;
                }
                if (!garaConclusa)
                { 
                    messaggio += " - " + gara.nomeDisciplina + " " +
                                         gara.nomeCategoria + " " +
                                         gara.nomeSegmento + "\r\n";
                    tn.ImageIndex = 7;
                    return;
                }

                string classifica = "";
                using (SQLiteCommand command = conn.CreateCommand())
                {
                    // se short program, compulsory, style prendo GaraFinal
                    if (idSeg == 1 || idSeg == 3 || idSeg == 4 || idSeg == 11 || idSeg == 12)
                    {
                        command.CommandText = "SELECT B.Position, A.Name, B.Total " +
                                          " FROM Athletes as A, GaraFinal as B" +
                                          " WHERE B.id_garaparams = " + idGaraParams + 
                                          " AND B.id_segment = " + idSeg +
                                          " AND A.ID_Atleta = B.ID_Atleta" +
                                          " ORDER BY B.Position";
                    }
                    else if (idSeg == 2 || idSeg == 5 || idSeg == 6)
                    {
                        command.CommandText = "SELECT Position, Atleta, TotGara FROM GaraTotal " +
                                          "WHERE id_garaparams = " + idGaraParams +
                                          " ORDER BY Position";
                    }
                    
                    dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        classifica =  "(" + dr[0].ToString(); // posizione in classifica
                        classifica += ",@tessera"; // tessera
                        classifica += ",'" + dr[1].ToString().Replace("'", "''") + "'"; // nome
                        classifica += ",@societa1,@societa2,@societa3,@societa4";
                        classifica += "," + FormattaQuestoBenedettoDecimale(dr[2].ToString()).Replace(".","") + ")"; // totale
                        // recupero tessera e societa 
                        using (SQLiteCommand command2 = connArtis.CreateCommand())
                        {
                            command2.CommandText =
                            "SELECT A.*" +
                            " FROM art_manif_atleti as A, art_manif_iscrizioni as B" +
                            " WHERE A.tessera = B.tessera AND A.id_manif = " + tn.Parent.Name +
                            " AND B.id_gara = " + gara.idattivita +
                            " AND A.Cognome || ' ' || A.Nome = '" + dr[1].ToString().Replace("'", "''") + "'";

                            drArtis = command2.ExecuteReader();
                            while (drArtis.Read())
                            {
                                classifica = classifica.Replace("@tessera", drArtis[1].ToString());
                                classifica = classifica.Replace("@societa1", drArtis[6].ToString());
                                classifica = classifica.Replace("@societa2", drArtis[7].ToString());
                                classifica = classifica.Replace("@societa3", drArtis[8].ToString());
                                classifica = classifica.Replace("@societa4", drArtis[9].ToString());
                            }
                        }
                        arClassifica.Add(classifica);
                    }
                    tn.ImageIndex = 1;
                }
            }
            catch (SQLiteException ex)
            {
                message.Text = ex.Message;
            }
        }

        #endregion

        #region Artis
        // Importa Gara Artis
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string fileContent = string.Empty;
                var filePath = string.Empty;
                if (of1.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = of1.FileName;

                    //Read the contents of the file into a stream
                    var fileStream = of1.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        fileContent = reader.ReadToEnd().Replace("\r\n", "");
                        sqlArtis = fileContent.Split(';');
                    }

                    if (sqlArtis != null)
                    {
                        CaricoGaraArtis(sqlArtis);
                        GetTutteLeManifestazioni();
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                message.Text += "Apri file: " + ex.Message + "\r\n";
            }
        }
        public void CaricoGaraArtis(string[] sqlList)
        {
            try
            {
                pb.Visible = true;
                int countRecord = 0;
                bool nessunErrore = true;

                SQLiteCommand cmd = new SQLiteCommand("select MAX(id) from art_manif", connArtis);
                SQLiteDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (dr[0].ToString().Equals("")) idManif = 1;
                    else idManif = dr.GetInt32(0) + 1;
                }

                transactionArtis = connArtis.BeginTransaction();
                // faccio l'ultima query 
                using (SQLiteCommand command = connArtis.CreateCommand())
                {
                    foreach (string sql in sqlList)
                    {
                        if (sql.Contains("art_manif ("))
                        {
                            command.Transaction = transactionArtis;
                            command.CommandText = sql.Replace("$id_manif", idManif + "");
                            command.ExecuteNonQuery();
                        }
                    }
                }

                foreach (string sql in sqlList)
                {
                    if (!sql.Equals(""))
                    {
                        try
                        {
                            using (SQLiteCommand command = connArtis.CreateCommand())
                            {
                                command.Transaction = transactionArtis;

                                // inserisco l'id_manif
                                string temp = sql.Replace("$id_manif", idManif + "");

                                if (sql.Contains("art_manif_atleti") || sql.Contains("art_manif_iscrizioni"))
                                {
                                    // divido la string in due 
                                    string[] temp2 = temp.Replace("values", "$").Split('$');
                                    string temp3 = temp2[0] + "values "; // insert into ...

                                    foreach (string row in temp2[1].Split(')'))
                                    {
                                        countRecord++;
                                        if (!row.Equals(""))
                                        {
                                            command.CommandText = temp3 + row.TrimStart(',') + ")";
                                            //WriteLog(command.CommandText, "INFO");
                                            try
                                            {
                                                command.ExecuteNonQuery();
                                            }
                                            catch (SQLiteException e)
                                            {
                                                // record gia presente
                                            }
                                        }
                                    }
                                }
                                else // tutte le altre tabelle
                                {
                                    command.CommandText = sql.Replace("$id_manif", idManif + "");
                                    try
                                    {
                                        command.ExecuteNonQuery();
                                    }
                                    catch (SQLiteException ex)
                                    {
                                        // record gia presente

                                    }
                                }
                            }
                        }
                        catch (SQLiteException ex)
                        {
                            message.Text += ex.Message + "\r\n";
                            nessunErrore = false;
                        }
                    }
                }
                pb.Visible = false;
                if (nessunErrore)
                {
                    transactionArtis.Commit();
                    MessageBox.Show("Manifestazione inserita correttamente!\r\n"
                        , "ArtisToRollart", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    transactionArtis.Rollback();
                    MessageBox.Show("Manifestazione NON inserita\r\n"
                        , "Errore ArtisToRollart", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (SQLiteException ex)
            {
                pb.Visible = false;
                if (ex.ErrorCode == 19)
                {
                    MessageBox.Show("Manifestazione già inserita\r\n"
                        , "Errore ArtisToRollart", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                } else 
                    message.Text += "Carico Evento Artis: " + ex.Message + "\r\n";
            }
        }
       
        // RIMUOVI Gara Artis
        private void rimuovi_Click(object sender, EventArgs e)
        {
            try
            {
                if (tvManif.SelectedNode == null) return;
                if (tvManif.Nodes.Count == 0)
                {
                    MessageBox.Show("Non ci sono manifestazioni Artis in archivio. ",
                        "ArtisToRollart", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                // se nessuna gara è stata selezionata
                bool nessunaGaraSelezionata = true;
                foreach (TreeNode tn in tvManif.Nodes)
                {
                    if (tn.Checked)
                    {
                        nessunaGaraSelezionata = false;
                        break;
                    }
                    foreach (TreeNode nodi in tn.Nodes)
                    {
                        if (nodi.Checked)
                        {
                            nessunaGaraSelezionata = false;
                            break;
                        }
                    }
                }
                if (nessunaGaraSelezionata)
                {
                    MessageBox.Show("Seleziona una o più gare Artis da rimuovere.",
                        "ArtisToRollart", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                transactionArtis = connArtis.BeginTransaction();
                foreach (TreeNode n in tvManif.Nodes)
                {
                    if (n != null)
                    {
                         DeleteNodi(n);
                    }
                }
                transactionArtis.Commit();
                
                GetTutteLeManifestazioni();
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                if (transactionArtis != null) transactionArtis.Rollback();
            }
        }

        private void DeleteNodi(TreeNode node)
        {
            for (int i = node.Nodes.Count - 1; i >= 0; i--)
            {
                if (node.Nodes[i].Checked)
                {
                    DeleteFromArtis((GaraParams)node.Nodes[i].Tag, node.Nodes[i]);
                    node.Nodes[i].Remove();
                }
               
            }
            if (node.Checked)
            {
                DeleteFromArtis((GaraParams)node.Tag, node);
                node.Remove();
            }
        }

        public void DeleteFromArtis(GaraParams gara, TreeNode tn)
        {
            try
            {
                int idManif = 0;
                // cancello tutta la manifestazione
                if (tn.Level == 0)
                {
                    using (SQLiteCommand command = connArtis.CreateCommand())
                    {
                        command.Transaction = transactionArtis;

                        command.CommandText = "DELETE FROM art_manif WHERE id = " + tn.Name;
                        command.ExecuteNonQuery();
                        command.Cancel();
                        command.CommandText = "DELETE FROM art_manif_atleti WHERE id_manif = " + tn.Name;
                        command.ExecuteNonQuery();
                        command.Cancel();
                        command.CommandText = "DELETE FROM art_manif_fasi WHERE id_manif = " + tn.Name;
                        command.ExecuteNonQuery();
                        command.Cancel();
                        command.CommandText = "DELETE FROM art_manif_gare WHERE id_manif = " + tn.Name;
                        command.ExecuteNonQuery();
                        command.Cancel();
                        command.CommandText = "DELETE FROM art_manif_giurie WHERE id_manif = " + tn.Name;
                        command.ExecuteNonQuery();
                        command.Cancel();
                        command.CommandText = "DELETE FROM art_manif_giurie_n WHERE id_manif = " + tn.Name;
                        command.ExecuteNonQuery();
                        command.Cancel();
                        command.CommandText = "DELETE FROM art_manif_iscrizioni WHERE id_manif = " + tn.Name;
                        command.ExecuteNonQuery();
                        command.Cancel();
                        command.CommandText = "DELETE FROM art_manif_societa WHERE id_manif = " + tn.Name;
                        command.ExecuteNonQuery();
                        command.Cancel();
                    }
                }
                else
                {
                    using (SQLiteCommand command = connArtis.CreateCommand())
                    {
                        command.Transaction = transactionArtis;
                        command.CommandText = "DELETE FROM art_manif_fasi WHERE id_manif = "
                            + tn.Parent.Name + " AND id_gara = " + gara.idattivita;
                        command.ExecuteNonQuery();
                        command.Cancel();
                        command.CommandText = "DELETE FROM art_manif_gare WHERE id_manif = "
                            + tn.Parent.Name + " AND id = " + gara.idattivita;
                        command.ExecuteNonQuery();
                        command.Cancel();
                        command.CommandText = "DELETE FROM art_manif_iscrizioni WHERE id_manif = "
                            + tn.Parent.Name + " AND id_gara = " + gara.idattivita;
                        command.ExecuteNonQuery();
                        command.Cancel();
                    }
                }

                //tvManif.Nodes.Remove(tn);
            }
            catch (SQLiteException ex)
            {
                message.Text = ex.Message;
                if (transaction != null)
                {
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (SQLiteException ex2)
                    {

                    }
                    finally
                    {
                        transaction.Dispose();
                    }
                }

            }
        }

        // RICARICA GARE ARTIS
        private void button3_Click(object sender, EventArgs e)
        {
            GetTutteLeManifestazioni();
        }
        // TUTTE LE MANIFESTAZIONI
        public void GetTutteLeManifestazioni()
        {
            try
            {
                tvManif.Nodes.Clear();
                using (SQLiteCommand command = connArtis.CreateCommand())
                {
                    command.CommandText =
                    "SELECT * FROM art_manif ";

                    SQLiteDataReader dr = command.ExecuteReader();
                    TreeNode tn = null;
                    while (dr.Read())
                    {
                        tn = new TreeNode();
                        string path = @"AW" + int.Parse(dr[0].ToString()).ToString("D5");
                        tn.Text = "[" + path + "] " + dr[4].ToString() + " " + dr[5].ToString() + " " + dr[3].ToString();
                        tn.ToolTipText = dr[4].ToString() + " - " + dr[5].ToString();
                        tn.Name = dr[0].ToString();  // id_manif
                        tn.ImageIndex = 6;
                        //tn.SelectedImageIndex = 0;
                        tn.NodeFont = new Font(tvManif.Font, FontStyle.Bold);
                        tvManif.Nodes.Add(tn);

                        tvManif.SelectedNode = tn;
                        RecuperoEventi();
                        tn.Collapse();
                    }
                    tvManif.Sort();

                }
            }
            catch (Exception ex)
            {
                message.Text += "GetInfoGaraParams: " + ex.Message + "\r\n";
            }
        }
        // MANIFESTAZIONE SPECIFICA
        public void GetInfoGaraParams()
        {
            try
            {
                listaGare = new List<GaraParams>();
                using (SQLiteCommand command = connArtis.CreateCommand())
                {
                    command.CommandText =
                    " SELECT A.descrizione, A.luogo, A.data, D.id_segment, F.ID_Discipline, E.ID_Category, " +
                            " C.sesso, C.categoria, C.id_distanza, C.id, E.categoria, D.descrizione" +
                    " FROM art_manif A, art_manif_gare C, distanze as D, categorie_atleti as E, attivita as F" +
                    " WHERE A.id = " + idManifCurrent +
                      " AND A.id = C.id_manif" +
                      " AND D.id = C.id_distanza" +
                      " AND E.sigla = C.categoria" +
                      " AND F.id = C.id";

                    // recupero tutto tranne le giurie e il numero partecipanti
                    SQLiteDataReader dr = command.ExecuteReader();
                    int numGare = 0;
                    while (dr.Read())
                    {
                        GaraParams eventoSingolo = new GaraParams();
                        eventoSingolo.name = dr[0].ToString();
                        eventoSingolo.place = dr[1].ToString();
                        eventoSingolo.date = dr[2].ToString();
                        eventoSingolo.idsegment = dr[3].ToString();
                        eventoSingolo.idspecialita = dr[4].ToString();
                        eventoSingolo.idcategory = dr[5].ToString();
                        //eventoSingolo.partecipants = dr[6].ToString();
                        eventoSingolo.sex = dr[6].ToString();
                        eventoSingolo.categoria = dr[7].ToString();
                        //eventoSingolo.numjudges = dr[9].ToString();
                        eventoSingolo.iddistanza = dr[8].ToString();
                        //eventoSingolo.idgiuria = dr[11].ToString();
                        eventoSingolo.idattivita = dr[9].ToString();
                        eventoSingolo.nomeCategoria = dr[10].ToString();
                        eventoSingolo.nomeSegmento = dr[11].ToString();
                        eventoSingolo.nomeDisciplina = GetDisciplina(eventoSingolo.idspecialita);

                        // giurie
                        eventoSingolo.pannello = new List<Giudici>();
                        //GetPannelloGiudici(eventoSingolo.idgiuria, eventoSingolo.pannello);

                        // atleti
                        eventoSingolo.atleti = new List<Atleti>();
                        GetListaIscritti(eventoSingolo.idattivita, eventoSingolo.atleti);

                        // recupero l'id giuria
                        using (SQLiteCommand command2 = connArtis.CreateCommand())
                        {
                            command2.CommandText =
                            " SELECT A.id_giuria, A.n_giudici, B.n_iscr FROM art_manif_giurie_n As A, art_manif_fasi As B" +
                            " WHERE A.id_manif = " + idManifCurrent +
                              " AND A.id_giuria = B.id_giuria" +
                              " AND B.id_gara = " + eventoSingolo.idattivita;
                            SQLiteDataReader dr2 = command2.ExecuteReader();
                            // giuria presente e art_manif_fasi presente
                            if (dr2.HasRows)
                            {
                                while (dr2.Read())
                                {
                                    eventoSingolo.idgiuria = dr2[0].ToString();
                                    eventoSingolo.numjudges = dr2[1].ToString();
                                    eventoSingolo.partecipants = dr2[2].ToString();
                                }
                            }
                            else
                            {
                                // imposto il numero di giudici che scelgo
                                eventoSingolo.idgiuria = "0";
                                eventoSingolo.numjudges = "" + numericUpDown1.Value;
                                eventoSingolo.partecipants = "" + eventoSingolo.atleti.Count;
                            }
                            GetPannelloGiudici(eventoSingolo.idgiuria, eventoSingolo.pannello
                                , eventoSingolo.numjudges);
                        }

                        listaGare.Add(eventoSingolo);
                    }
                }
            }
            catch (Exception ex)
            {
                message.Text += "GetInfoGaraParams: " + ex.Message + "\r\n";
            }
        }
        // GIUDICI
        public void GetPannelloGiudici(string idgiuria, List<Giudici> giudici, string numJudges)
        {
            try
            {
                if (idgiuria.Equals("0"))
                {
                    Giudici giudi = new Giudici();
                    giudi.ruolo = "-90"; giudi.nome = "TP"; giudi.sezione = "";
                    giudici.Add(giudi);
                    giudi = new Giudici();
                    giudi.ruolo = "-93"; giudi.nome = "DO"; giudi.sezione = "";
                    giudici.Add(giudi);
                    giudi = new Giudici();
                    giudi.ruolo = "-92"; giudi.nome = "CO"; giudi.sezione = "";
                    giudici.Add(giudi);
                    for (int i=0; i< int.Parse(numJudges); i++)
                    {
                        giudi = new Giudici();
                        giudi.ruolo = "" + (i + 1); giudi.nome = "J" + (i + 1);
                        giudi.sezione = "";
                        giudici.Add(giudi);
                    }
                }
                else
                {
                    using (SQLiteCommand command = connArtis.CreateCommand())
                    {
                        command.CommandText =
                        " SELECT id_ruolo, nome, sezione, tessera" +
                        " FROM art_manif_giurie as A, art_manif_giurie_n as B" +
                        " WHERE A.id_manif = " + idManifCurrent +
                        " AND A.id_giuria = B.id_giuria AND A.id_manif = B.id_manif" +
                        " and A.id_giuria = " + idgiuria;

                        SQLiteDataReader dr = command.ExecuteReader();
                        while (dr.Read())
                        {
                            Giudici giudi = new Giudici();
                            giudi.ruolo = dr[0].ToString();
                            giudi.nome = dr[1].ToString();
                            giudi.sezione = dr[2].ToString();

                            giudici.Add(giudi);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message.Text += "GetPannelloGiudici: " + ex.Message + "\r\n";
            }
        }
        // ATLETI
        public void GetListaIscritti(string idattivita, List<Atleti> atleti)
        {
            try
            {
                using (SQLiteCommand command = connArtis.CreateCommand())
                {
                    command.CommandText =
                    " SELECT D.id_gara, B.tessera, B.cognome, B.nome, B.sesso, C.societa, B.societa, B.societa2, " +
                    " B.societa3, B.societa4, B.partnerA, B.partnerD, D.ord_entrata, A.sesso" +
                    " FROM art_manif_gare as A, art_manif_atleti as B," +
                    " art_manif_iscrizioni as D, art_manif_societa as C" +
                    " WHERE A.id_manif = " + idManifCurrent +
                    " AND C.id_manif = D.id_manif" +
                    " AND A.id_manif = D.id_manif AND B.id_manif = D.id_manif" +
                    " AND A.id = D.id_gara AND D.tessera = B.tessera" +
                    " AND (C.codice = B.societa OR C.codice = B.societa2" + 
                    " OR C.codice = B.societa3 OR C.codice = B.societa4)" + 
                    " AND D.id_gara = " + idattivita+
                    " ORDER BY D.ord_entrata ASC";

                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        Atleti skater = new Atleti();
                        skater.tessera =  dr[1].ToString();
                        skater.cognome =  dr[2].ToString();
                        skater.nome =     dr[3].ToString();
                        skater.nomeRollart = skater.cognome + " " + skater.nome;
                        skater.sesso =    dr[4].ToString();
                        skater.societa1 = dr[5].ToString();
                        skater.societaRollart = dr[6].ToString() + " " + skater.societa1;
                        skater.societa2 = dr[7].ToString();
                        skater.societa3 = dr[8].ToString();
                        skater.societa4 = dr[9].ToString();
                        
                        skater.tesseraA = dr[10].ToString();
                        skater.tesseraD = dr[11].ToString();
                        skater.ordineentrata = dr[12].ToString();
                        string sesso = dr[13].ToString();

                        // se coppie recupero le info della donna
                        if (!sesso.Equals("M") && (skater.tesseraA != "0" || skater.tesseraD != "0"))
                        {
                            using (SQLiteCommand command2 = connArtis.CreateCommand())
                            {
                                command2.CommandText =
                                " SELECT DISTINCT A.cognome, A.nome, A.sesso, B.societa, B.codice" +
                                " FROM art_manif_atleti as A, art_manif_societa as B" +
                                " WHERE A.societa = B.codice AND (A.tessera = " + skater.tesseraA +
                                " OR A.tessera = " + skater.tesseraD + ") AND A.id_manif = B.id_manif";

                                SQLiteDataReader dr2 = command2.ExecuteReader();
                                while (dr2.Read())
                                {
                                    skater.nomeRollart += " - " + dr2[0].ToString() + " " +
                                        dr2[1].ToString();
                                    skater.societaRollart += " - " + dr2[4].ToString() + " " +
                                        dr2[3].ToString();

                                }
                            }
                        }

                        atleti.Add(skater);
                    }
                }
            }
            catch (Exception ex)
            {
                message.Text += "GetPannelloGiudici: " + ex.Message + "\r\n";
            }
        }
        // DISCIPLINA
        public string GetDisciplina(string id)
        {
            string nomeDisc = "";
            switch (id)
            {
                case "1":
                    nomeDisc = "Singolo maschile";
                    break;
                case "2":
                    nomeDisc = "Singolo femminile";
                    break;
                case "3":
                    nomeDisc = "Coppie Artistico";
                    break;
                case "4":
                    nomeDisc = "Coppia Danza";
                    break;
                case "5":
                    nomeDisc = "Solo dance maschile";
                    break;
                case "6":
                    nomeDisc = "Solo dance femminile";
                    break;
                case "7":
                    nomeDisc = "Sincronizzato";
                    break;
            }
            return nomeDisc;
        }
        #endregion

        #region DB CONNECTION
        public void OpenConnection()
        {
            try
            {
                string connString = Properties.Settings.Default.dbconnection;
                conn = new SQLiteConnection(connString);
                conn.Open();
                tsDatabase.Text = "Database " + conn.DataSource + " [Connected]";
                WriteLog(conn.ConnectionString, "Ok");

                string connStringArtis = Properties.Settings.Default.artis;
                connArtis = new SQLiteConnection(connStringArtis);
                connArtis.Open();
                tsDatabase.Text += " - " + connArtis.DataSource + " [Connected]";
                WriteLog(connArtis.ConnectionString, "Ok");

                tsDatabase.Image = Properties.Resources.bullet_green;

                GetTutteLeManifestazioni();
            }
            catch (Exception ex)
            {
                WriteLog(ex.Message, "ERROR");
                tsDatabase.Text = "Database [Not connected]";
                tsDatabase.Image = Properties.Resources.bullet_red;

            }
        }

        public void CloseConnection()
        {
            try
            {
                conn.Close();
                connArtis.Close();

                WriteLog(conn.DataSource + " - " + conn.ConnectionString, "Ok");
                WriteLog(connArtis.DataSource + " - " + connArtis.ConnectionString, "Ok");
                tsDatabase.Text = "Database [Not connected]";
            }
            catch (Exception ex)
            {
                WriteLog(ex.Message, "ERROR");
            }
        }

        #endregion

        #region Utility
        public void WriteLog(string text, string messageType)
        {
            try
            {
                if (messageType.Equals("ERROR"))
                {
                    Trace.TraceError(text);
                    logger.Log(LogLevel.Error, text);
                }
                else if (messageType.Equals("WARNING"))
                {
                    Trace.TraceWarning(text);
                    logger.Log(LogLevel.Warn, text);
                }
                else if (messageType.Equals("Ok"))
                {
                    Trace.TraceInformation(text);
                    logger.Log(LogLevel.Info, text);
                }
                else if (messageType.Equals("DEBUG"))
                {
                    logger.Log(LogLevel.Debug, text);
                }
                else logger.Log(LogLevel.Trace, text);
                //message.Text += text + "\r\n";
            }
            catch (Exception)
            { }
        }
        
        public void HighlightCheckedNodes()
        {
            int countIndex = 0;
            // Check whether the tree node is checked.
            if (tvManif.SelectedNode.Checked && tvManif.SelectedNode.Level > 0)
            {
                // Set the node's backColor.
                tvManif.SelectedNode.BackColor = Color.Yellow;
            }
            else
                tvManif.SelectedNode.BackColor = Color.White;
        }

        private void tvManif_AfterCheck(object sender, TreeViewEventArgs e)
        {
            try
            {
                tvManif.SelectedNode = e.Node;
                HighlightCheckedNodes();
                if (e.Node.Level == 0) // manif
                {
                    foreach (TreeNode tn in e.Node.Nodes)
                    {
                        if (e.Node.Checked)
                            tn.Checked = true;
                        else tn.Checked = false;
                        HighlightCheckedNodes();
                    }
                }
                
            }
            catch (Exception ex)
            {
                WriteLog(ex.Message, "ERROR");
            }
        }

        // doppio click sulla manifestazione
        private void tvManif_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                if (e != null) tvManif.SelectedNode = e.Node; else return;
                if (e.Node.Level == 1) return;

                RecuperoEventi();
            }
            catch (Exception ex)
            {
                message.Text += "Recupero eventi: " + ex.Message + "\r\n";
            }
        }

        public void RecuperoEventi()
        {
            try
            {
                TreeNode manifSelezionata = tvManif.SelectedNode;
                idManifCurrent = manifSelezionata.Name;
                Cursor.Current = Cursors.WaitCursor;
                GetInfoGaraParams();

                manifSelezionata.Nodes.Clear();
                if (listaGare.Count == 0) return;

                TreeNode tnGara = null;
                foreach (GaraParams gara in listaGare)
                {
                    tnGara = new TreeNode();
                    tnGara.Text = gara.nomeDisciplina + " - " +
                        gara.nomeCategoria + " " +
                        gara.nomeSegmento + " (" +
                        gara.atleti.Count + ") ";
                    tnGara.ImageIndex = 2;
                    tnGara.SelectedImageIndex = tnGara.ImageIndex;
                    tnGara.Tag = gara;
                    manifSelezionata.Nodes.Add(tnGara);
                }
                manifSelezionata.Expand();
                Cursor.Current = Cursors.Hand;
            }
            catch (Exception ex)
            {
                message.Text += "Recupero eventi: " + ex.Message + "\r\n";
                Cursor.Current = Cursors.Hand;
            }
        }

        public string FormattaQuestoBenedettoDecimale(object valoreDatabase)
        {
            string valoreFormattato = "";
            double dbValue;
            try
            {
                dbValue = double.Parse(valoreDatabase.ToString());
                return dbValue.ToString("0.00", CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                valoreFormattato = valoreDatabase.ToString().Replace(",", ".");
                return valoreFormattato;
            }
        }

        private bool CreoDirectoryEvento(int idManif)
        {
            path = @"AW" + idManif.ToString("D5");
            try
            {
                if (Directory.Exists(path))
                {
                    message.Text = "La directory " + path + " esiste già.";
                    return true;
                }

                DirectoryInfo di = Directory.CreateDirectory(path);
                return true;
            }
            catch (Exception e)
            {
                message.Text = "The process failed: " + e.ToString();
            }
            finally { }
            return false;
        }
        #endregion
    }

    public class GaraParams
    {
        public string name { get; set; }
        public string place { get; set; }
        public string date { get; set; }
        public string dateEnd { get; set; }
        public string idsegment { get; set; }
        public string idspecialita { get; set; }
        public string idcategory { get; set; }
        public string partecipants { get; set; }
        public string sex { get; set; }
        public string numjudges { get; set; }

        public string categoria { get; set; }
        public string nomeCategoria { get; set; }
        public string nomeSegmento { get; set; }
        public string nomeDisciplina { get; set; }
        public string iddistanza { get; set; }
        public string idgiuria { get; set; }
        public string idattivita { get; set; }
        public List<Giudici> pannello { get; set; }
        public List<Atleti> atleti { get; set; }
    }

    public class Giudici
    {
        public string nome { get; set; }
        public string sezione { get; set; }
        public string ruolo { get; set; }
    }

    public class Atleti
    {
        public string tessera { get; set; }
        public string cognome { get; set; }
        public string nome { get; set; }
        public string nomeRollart { get; set; }
        public string sesso { get; set; }
        public string societaRollart { get; set; }
        public string societa1 { get; set; }
        public string societa2 { get; set; }
        public string societa3 { get; set; }
        public string societa4 { get; set; }
        public string tesseraA { get; set; }
        public string tesseraD { get; set; }
        public string ordineentrata { get; set; }
    }
}

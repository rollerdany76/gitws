﻿namespace Ghost
{
    partial class EventsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EventsForm));
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Events", 21, 21);
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.titolo = new System.Windows.Forms.Button();
            this.imageList3 = new System.Windows.Forms.ImageList(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buTech = new System.Windows.Forms.Button();
            this.panStartEvent = new System.Windows.Forms.Panel();
            this.deletegare = new System.Windows.Forms.Button();
            this.gara = new System.Windows.Forms.Panel();
            this.l4 = new System.Windows.Forms.TextBox();
            this.l1 = new System.Windows.Forms.TextBox();
            this.cbPattern = new System.Windows.Forms.ComboBox();
            this.labPattern = new System.Windows.Forms.Label();
            this.panStyle = new System.Windows.Forms.Panel();
            this.l10 = new System.Windows.Forms.Label();
            this.b3 = new System.Windows.Forms.Button();
            this.b4 = new System.Windows.Forms.Button();
            this.b8 = new System.Windows.Forms.Button();
            this.o2 = new System.Windows.Forms.Button();
            this.l20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.o3 = new System.Windows.Forms.Button();
            this.lv3 = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.b9 = new System.Windows.Forms.Button();
            this.b7 = new System.Windows.Forms.Button();
            this.b6 = new System.Windows.Forms.Button();
            this.b5 = new System.Windows.Forms.Button();
            this.b2 = new System.Windows.Forms.Button();
            this.b1 = new System.Windows.Forms.Button();
            this.l11 = new System.Windows.Forms.Label();
            this.l9 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.l8 = new System.Windows.Forms.Label();
            this.l7 = new System.Windows.Forms.Label();
            this.o1 = new System.Windows.Forms.Button();
            this.l6 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.judges = new System.Windows.Forms.Button();
            this.l5 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.l3 = new System.Windows.Forms.Label();
            this.l2 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.reload = new System.Windows.Forms.Button();
            this.tv = new System.Windows.Forms.TreeView();
            this.menu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.connect = new System.Windows.Forms.Button();
            this.reset = new System.Windows.Forms.Button();
            this.save = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.pb = new System.Windows.Forms.PictureBox();
            this.panReport = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.result = new System.Windows.Forms.Button();
            this.classifica = new System.Windows.Forms.Button();
            this.panel = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.cr = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.deleteall = new System.Windows.Forms.Button();
            this.panNewEvent = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.club = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.naz = new System.Windows.Forms.TextBox();
            this.add = new System.Windows.Forms.Button();
            this.lv2 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label8 = new System.Windows.Forms.Label();
            this.namep = new System.Windows.Forms.TextBox();
            this.delete = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pb1 = new System.Windows.Forms.ProgressBar();
            this.nJudges = new System.Windows.Forms.NumericUpDown();
            this.confirmNewEvent = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.lv1 = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label7 = new System.Windows.Forms.Label();
            this.browse = new System.Windows.Forms.Button();
            this.cb6 = new System.Windows.Forms.CheckBox();
            this.cb5 = new System.Windows.Forms.CheckBox();
            this.cb4 = new System.Windows.Forms.CheckBox();
            this.cb3 = new System.Windows.Forms.CheckBox();
            this.cb2 = new System.Windows.Forms.CheckBox();
            this.cb1 = new System.Windows.Forms.CheckBox();
            this.cbCategory = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.cbEvent = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.place = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.dateFrom = new System.Windows.Forms.DateTimePicker();
            this.dateTo = new System.Windows.Forms.DateTimePicker();
            this.panLog = new System.Windows.Forms.Panel();
            this.log = new System.Windows.Forms.TextBox();
            this.of1 = new System.Windows.Forms.OpenFileDialog();
            this.fd1 = new System.Windows.Forms.FolderBrowserDialog();
            this.message = new System.Windows.Forms.Label();
            this.message2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.FinalReport1 = new RollartSystemTech.Report.FinalReport();
            this.ResultReport1 = new RollartSystemTech.Report.ResultReport();
            this.PanelReport1 = new RollartSystemTech.Report.PanelReport();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panStartEvent.SuspendLayout();
            this.gara.SuspendLayout();
            this.panStyle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb)).BeginInit();
            this.panReport.SuspendLayout();
            this.panNewEvent.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nJudges)).BeginInit();
            this.panLog.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gray;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.titolo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1017, 49);
            this.panel1.TabIndex = 0;
            this.panel1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDoubleClick);
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox1.Location = new System.Drawing.Point(843, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(174, 49);
            this.pictureBox1.TabIndex = 97;
            this.pictureBox1.TabStop = false;
            // 
            // titolo
            // 
            this.titolo.AutoSize = true;
            this.titolo.BackColor = System.Drawing.Color.Transparent;
            this.titolo.Cursor = System.Windows.Forms.Cursors.Default;
            this.titolo.FlatAppearance.BorderColor = System.Drawing.Color.SlateGray;
            this.titolo.FlatAppearance.BorderSize = 0;
            this.titolo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.titolo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.titolo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.titolo.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titolo.ForeColor = System.Drawing.Color.White;
            this.titolo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.titolo.ImageList = this.imageList3;
            this.titolo.Location = new System.Drawing.Point(3, 3);
            this.titolo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.titolo.Name = "titolo";
            this.titolo.Size = new System.Drawing.Size(646, 45);
            this.titolo.TabIndex = 96;
            this.titolo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.titolo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.titolo.UseVisualStyleBackColor = false;
            // 
            // imageList3
            // 
            this.imageList3.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList3.ImageStream")));
            this.imageList3.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList3.Images.SetKeyName(0, "export.png");
            this.imageList3.Images.SetKeyName(1, "file_format_doc.png");
            this.imageList3.Images.SetKeyName(2, "file_format_pdf.png");
            this.imageList3.Images.SetKeyName(3, "acrobat.png");
            this.imageList3.Images.SetKeyName(4, "add_32.png");
            this.imageList3.Images.SetKeyName(5, "arrow_left_32.png");
            this.imageList3.Images.SetKeyName(6, "arrow_right_32.png");
            this.imageList3.Images.SetKeyName(7, "balance_32.png");
            this.imageList3.Images.SetKeyName(8, "checkmark_32.png");
            this.imageList3.Images.SetKeyName(9, "cross_32.png");
            this.imageList3.Images.SetKeyName(10, "diskette_32.png");
            this.imageList3.Images.SetKeyName(11, "gear_32.png");
            this.imageList3.Images.SetKeyName(12, "help_32.png");
            this.imageList3.Images.SetKeyName(13, "home_32.png");
            this.imageList3.Images.SetKeyName(14, "info_32.png");
            this.imageList3.Images.SetKeyName(15, "plan_1_a_32.png");
            this.imageList3.Images.SetKeyName(16, "plan_1_b_32.png");
            this.imageList3.Images.SetKeyName(17, "plan_1_c_32.png");
            this.imageList3.Images.SetKeyName(18, "plan_1_d_32.png");
            this.imageList3.Images.SetKeyName(19, "plan_1_e_32.png");
            this.imageList3.Images.SetKeyName(20, "star_32.png");
            this.imageList3.Images.SetKeyName(21, "trophy_32.png");
            this.imageList3.Images.SetKeyName(22, "warehouse_32.png");
            this.imageList3.Images.SetKeyName(23, "world_32.png");
            this.imageList3.Images.SetKeyName(24, "zoom.png");
            this.imageList3.Images.SetKeyName(25, "da_blu.png");
            this.imageList3.Images.SetKeyName(26, "da_green.png");
            this.imageList3.Images.SetKeyName(27, "da_or.png");
            this.imageList3.Images.SetKeyName(28, "free_blu.png");
            this.imageList3.Images.SetKeyName(29, "free_green.png");
            this.imageList3.Images.SetKeyName(30, "free_or.png");
            this.imageList3.Images.SetKeyName(31, "pa_blu.png");
            this.imageList3.Images.SetKeyName(32, "pa_green.png");
            this.imageList3.Images.SetKeyName(33, "pa_or.png");
            this.imageList3.Images.SetKeyName(34, "sd_blu.png");
            this.imageList3.Images.SetKeyName(35, "sd_green.png");
            this.imageList3.Images.SetKeyName(36, "sd_or.png");
            this.imageList3.Images.SetKeyName(37, "sy_blu.png");
            this.imageList3.Images.SetKeyName(38, "sy_green.png");
            this.imageList3.Images.SetKeyName(39, "sy_or.png");
            this.imageList3.Images.SetKeyName(40, "tick.png");
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Copia di status_invisible_32.png");
            this.imageList1.Images.SetKeyName(1, "Copia di teacher_32.png");
            this.imageList1.Images.SetKeyName(2, "status_invisible_32.png");
            this.imageList1.Images.SetKeyName(3, "teacher_32.png");
            this.imageList1.Images.SetKeyName(4, "status_invisible_32_2.png");
            this.imageList1.Images.SetKeyName(5, "opia di teacher_32.png");
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 49);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.LightBlue;
            this.splitContainer1.Panel1.Controls.Add(this.button4);
            this.splitContainer1.Panel1.Controls.Add(this.button5);
            this.splitContainer1.Panel1.Controls.Add(this.button2);
            this.splitContainer1.Panel1.Controls.Add(this.button1);
            this.splitContainer1.Panel1.Controls.Add(this.buTech);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel2.BackgroundImage = global::RollartSystemTech.Properties.Resources.WS_Logo_OnDark_Horz;
            this.splitContainer1.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.splitContainer1.Panel2.Controls.Add(this.panNewEvent);
            this.splitContainer1.Panel2.Controls.Add(this.panStartEvent);
            this.splitContainer1.Panel2.Controls.Add(this.panLog);
            this.splitContainer1.Size = new System.Drawing.Size(1017, 684);
            this.splitContainer1.SplitterDistance = 144;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 2;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Transparent;
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.Dock = System.Windows.Forms.DockStyle.Top;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gold;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gold;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.ImageIndex = 13;
            this.button4.ImageList = this.imageList3;
            this.button4.Location = new System.Drawing.Point(0, 180);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(144, 45);
            this.button4.TabIndex = 107;
            this.button4.Text = "Exit";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button5_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Transparent;
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.Dock = System.Windows.Forms.DockStyle.Top;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gold;
            this.button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gold;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.Black;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.ImageIndex = 14;
            this.button5.ImageList = this.imageList3;
            this.button5.Location = new System.Drawing.Point(0, 135);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(144, 45);
            this.button5.TabIndex = 105;
            this.button5.Text = "Log";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.log_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gold;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gold;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.ImageIndex = 11;
            this.button2.ImageList = this.imageList3;
            this.button2.Location = new System.Drawing.Point(0, 90);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(144, 45);
            this.button2.TabIndex = 104;
            this.button2.Text = "Settings";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gold;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gold;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.ImageIndex = 21;
            this.button1.ImageList = this.imageList3;
            this.button1.Location = new System.Drawing.Point(0, 45);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(144, 45);
            this.button1.TabIndex = 96;
            this.button1.Text = "Event List";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buTech
            // 
            this.buTech.BackColor = System.Drawing.Color.Transparent;
            this.buTech.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buTech.Dock = System.Windows.Forms.DockStyle.Top;
            this.buTech.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.buTech.FlatAppearance.BorderSize = 0;
            this.buTech.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gold;
            this.buTech.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gold;
            this.buTech.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buTech.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buTech.ForeColor = System.Drawing.Color.Black;
            this.buTech.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buTech.ImageKey = "star_32.png";
            this.buTech.ImageList = this.imageList3;
            this.buTech.Location = new System.Drawing.Point(0, 0);
            this.buTech.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buTech.Name = "buTech";
            this.buTech.Size = new System.Drawing.Size(144, 45);
            this.buTech.TabIndex = 95;
            this.buTech.Text = "New Event";
            this.buTech.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buTech.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buTech.UseVisualStyleBackColor = false;
            this.buTech.Click += new System.EventHandler(this.buTech_Click);
            // 
            // panStartEvent
            // 
            this.panStartEvent.AutoScroll = true;
            this.panStartEvent.BackColor = System.Drawing.Color.White;
            this.panStartEvent.Controls.Add(this.deletegare);
            this.panStartEvent.Controls.Add(this.gara);
            this.panStartEvent.Controls.Add(this.reload);
            this.panStartEvent.Controls.Add(this.tv);
            this.panStartEvent.Controls.Add(this.connect);
            this.panStartEvent.Controls.Add(this.reset);
            this.panStartEvent.Controls.Add(this.save);
            this.panStartEvent.Controls.Add(this.label5);
            this.panStartEvent.Controls.Add(this.pb);
            this.panStartEvent.Controls.Add(this.panReport);
            this.panStartEvent.Controls.Add(this.deleteall);
            this.panStartEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panStartEvent.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panStartEvent.Location = new System.Drawing.Point(0, 0);
            this.panStartEvent.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panStartEvent.Name = "panStartEvent";
            this.panStartEvent.Size = new System.Drawing.Size(872, 684);
            this.panStartEvent.TabIndex = 1;
            this.panStartEvent.Visible = false;
            // 
            // deletegare
            // 
            this.deletegare.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deletegare.BackColor = System.Drawing.Color.White;
            this.deletegare.Cursor = System.Windows.Forms.Cursors.Hand;
            this.deletegare.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.deletegare.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.deletegare.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deletegare.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deletegare.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.deletegare.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.deletegare.Location = new System.Drawing.Point(84, 555);
            this.deletegare.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.deletegare.Name = "deletegare";
            this.deletegare.Size = new System.Drawing.Size(58, 47);
            this.deletegare.TabIndex = 138;
            this.deletegare.Text = "Delete";
            this.deletegare.UseVisualStyleBackColor = false;
            this.deletegare.Click += new System.EventHandler(this.deletegare_Click);
            // 
            // gara
            // 
            this.gara.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gara.BackgroundImage = global::RollartSystemTech.Properties.Resources.backgara;
            this.gara.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.gara.Controls.Add(this.l4);
            this.gara.Controls.Add(this.l1);
            this.gara.Controls.Add(this.cbPattern);
            this.gara.Controls.Add(this.labPattern);
            this.gara.Controls.Add(this.panStyle);
            this.gara.Controls.Add(this.l20);
            this.gara.Controls.Add(this.label22);
            this.gara.Controls.Add(this.o3);
            this.gara.Controls.Add(this.lv3);
            this.gara.Controls.Add(this.b9);
            this.gara.Controls.Add(this.b7);
            this.gara.Controls.Add(this.b6);
            this.gara.Controls.Add(this.b5);
            this.gara.Controls.Add(this.b2);
            this.gara.Controls.Add(this.b1);
            this.gara.Controls.Add(this.l11);
            this.gara.Controls.Add(this.l9);
            this.gara.Controls.Add(this.label14);
            this.gara.Controls.Add(this.label20);
            this.gara.Controls.Add(this.l8);
            this.gara.Controls.Add(this.l7);
            this.gara.Controls.Add(this.o1);
            this.gara.Controls.Add(this.l6);
            this.gara.Controls.Add(this.label19);
            this.gara.Controls.Add(this.judges);
            this.gara.Controls.Add(this.l5);
            this.gara.Controls.Add(this.label18);
            this.gara.Controls.Add(this.label17);
            this.gara.Controls.Add(this.l3);
            this.gara.Controls.Add(this.l2);
            this.gara.Controls.Add(this.label13);
            this.gara.Controls.Add(this.label11);
            this.gara.Location = new System.Drawing.Point(306, 22);
            this.gara.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gara.Name = "gara";
            this.gara.Size = new System.Drawing.Size(554, 518);
            this.gara.TabIndex = 137;
            this.gara.Visible = false;
            // 
            // l4
            // 
            this.l4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.l4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l4.Location = new System.Drawing.Point(185, 76);
            this.l4.Name = "l4";
            this.l4.Size = new System.Drawing.Size(359, 22);
            this.l4.TabIndex = 169;
            this.l4.TextChanged += new System.EventHandler(this.l4_TextChanged);
            // 
            // l1
            // 
            this.l1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.l1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.l1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.l1.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l1.ForeColor = System.Drawing.Color.White;
            this.l1.Location = new System.Drawing.Point(3, 17);
            this.l1.Name = "l1";
            this.l1.Size = new System.Drawing.Size(549, 25);
            this.l1.TabIndex = 168;
            this.l1.Text = "sdfsdf";
            this.l1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.l1.TextChanged += new System.EventHandler(this.l1_TextChanged);
            // 
            // cbPattern
            // 
            this.cbPattern.BackColor = System.Drawing.Color.AntiqueWhite;
            this.cbPattern.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPattern.DropDownWidth = 170;
            this.cbPattern.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPattern.FormattingEnabled = true;
            this.cbPattern.Items.AddRange(new object[] {
            "Argentine Tango",
            "Argentine Tango (Solo)",
            "Blues",
            "Castel March",
            "Fourteen Step",
            "Harris Tango",
            "Italian Foxtrot",
            "Midnight Blues",
            "Paso Doble",
            "Quick Step",
            "Rocker Foxtrot",
            "Starlight",
            "Tango Delanco",
            "Terenzi",
            "Viennese Waltz",
            "Westminster Waltz"});
            this.cbPattern.Location = new System.Drawing.Point(392, 443);
            this.cbPattern.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbPattern.Name = "cbPattern";
            this.cbPattern.Size = new System.Drawing.Size(152, 25);
            this.cbPattern.TabIndex = 167;
            this.cbPattern.Visible = false;
            // 
            // labPattern
            // 
            this.labPattern.AutoSize = true;
            this.labPattern.BackColor = System.Drawing.Color.Transparent;
            this.labPattern.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPattern.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.labPattern.Location = new System.Drawing.Point(206, 444);
            this.labPattern.Name = "labPattern";
            this.labPattern.Size = new System.Drawing.Size(176, 21);
            this.labPattern.TabIndex = 166;
            this.labPattern.Tag = "";
            this.labPattern.Text = "Select Pattern Sequence";
            this.labPattern.Visible = false;
            // 
            // panStyle
            // 
            this.panStyle.Controls.Add(this.l10);
            this.panStyle.Controls.Add(this.b3);
            this.panStyle.Controls.Add(this.b4);
            this.panStyle.Controls.Add(this.b8);
            this.panStyle.Controls.Add(this.o2);
            this.panStyle.Location = new System.Drawing.Point(7, 436);
            this.panStyle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panStyle.Name = "panStyle";
            this.panStyle.Size = new System.Drawing.Size(542, 41);
            this.panStyle.TabIndex = 165;
            // 
            // l10
            // 
            this.l10.AutoSize = true;
            this.l10.BackColor = System.Drawing.Color.Transparent;
            this.l10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l10.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.l10.Location = new System.Drawing.Point(3, 6);
            this.l10.Name = "l10";
            this.l10.Size = new System.Drawing.Size(113, 21);
            this.l10.TabIndex = 150;
            this.l10.Tag = "";
            this.l10.Text = "Short Program";
            this.l10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // b3
            // 
            this.b3.BackColor = System.Drawing.Color.White;
            this.b3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b3.Enabled = false;
            this.b3.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.b3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.b3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b3.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.b3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b3.Location = new System.Drawing.Point(176, 4);
            this.b3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.b3.Name = "b3";
            this.b3.Size = new System.Drawing.Size(121, 32);
            this.b3.TabIndex = 154;
            this.b3.Text = "Start";
            this.b3.UseVisualStyleBackColor = false;
            this.b3.Visible = false;
            this.b3.Click += new System.EventHandler(this.b3_Click);
            // 
            // b4
            // 
            this.b4.BackColor = System.Drawing.Color.White;
            this.b4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b4.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.b4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.b4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b4.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.b4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b4.Location = new System.Drawing.Point(176, 4);
            this.b4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.b4.Name = "b4";
            this.b4.Size = new System.Drawing.Size(115, 32);
            this.b4.TabIndex = 155;
            this.b4.Text = "View Results";
            this.b4.UseVisualStyleBackColor = false;
            this.b4.Visible = false;
            this.b4.Click += new System.EventHandler(this.b4_Click);
            // 
            // b8
            // 
            this.b8.BackColor = System.Drawing.Color.White;
            this.b8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b8.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.b8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.b8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b8.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.b8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b8.Location = new System.Drawing.Point(304, 4);
            this.b8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.b8.Name = "b8";
            this.b8.Size = new System.Drawing.Size(73, 32);
            this.b8.TabIndex = 159;
            this.b8.Text = "Delete";
            this.b8.UseVisualStyleBackColor = false;
            this.b8.Visible = false;
            this.b8.Click += new System.EventHandler(this.b8_Click);
            // 
            // o2
            // 
            this.o2.BackColor = System.Drawing.Color.White;
            this.o2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.o2.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.o2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.o2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.o2.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.o2.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.o2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.o2.Location = new System.Drawing.Point(385, 4);
            this.o2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.o2.Name = "o2";
            this.o2.Size = new System.Drawing.Size(152, 32);
            this.o2.TabIndex = 161;
            this.o2.Text = "Skating Order";
            this.o2.UseVisualStyleBackColor = false;
            this.o2.Visible = false;
            this.o2.Click += new System.EventHandler(this.o2_Click);
            // 
            // l20
            // 
            this.l20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.l20.AutoSize = true;
            this.l20.BackColor = System.Drawing.Color.Transparent;
            this.l20.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l20.ForeColor = System.Drawing.Color.Black;
            this.l20.Location = new System.Drawing.Point(491, 2);
            this.l20.Name = "l20";
            this.l20.Size = new System.Drawing.Size(37, 15);
            this.l20.TabIndex = 164;
            this.l20.Tag = "";
            this.l20.Text = "idxxxx";
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.SlateGray;
            this.label22.Location = new System.Drawing.Point(442, 1);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 15);
            this.label22.TabIndex = 163;
            this.label22.Tag = "";
            this.label22.Text = "Event ID:";
            // 
            // o3
            // 
            this.o3.BackColor = System.Drawing.Color.White;
            this.o3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.o3.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.o3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.o3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.o3.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.o3.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.o3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.o3.Location = new System.Drawing.Point(392, 478);
            this.o3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.o3.Name = "o3";
            this.o3.Size = new System.Drawing.Size(152, 32);
            this.o3.TabIndex = 162;
            this.o3.Text = "Skating Order";
            this.o3.UseVisualStyleBackColor = false;
            this.o3.Visible = false;
            this.o3.Click += new System.EventHandler(this.o3_Click);
            // 
            // lv3
            // 
            this.lv3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lv3.AutoArrange = false;
            this.lv3.BackColor = System.Drawing.Color.AliceBlue;
            this.lv3.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9});
            this.lv3.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv3.FullRowSelect = true;
            this.lv3.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lv3.HideSelection = false;
            this.lv3.LabelWrap = false;
            this.lv3.Location = new System.Drawing.Point(183, 221);
            this.lv3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lv3.MultiSelect = false;
            this.lv3.Name = "lv3";
            this.lv3.ShowGroups = false;
            this.lv3.ShowItemToolTips = true;
            this.lv3.Size = new System.Drawing.Size(361, 164);
            this.lv3.SmallImageList = this.imageList1;
            this.lv3.TabIndex = 143;
            this.lv3.UseCompatibleStateImageBehavior = false;
            this.lv3.View = System.Windows.Forms.View.Details;
            this.lv3.Visible = false;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "";
            this.columnHeader7.Width = 132;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "";
            this.columnHeader8.Width = 90;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Width = 135;
            // 
            // b9
            // 
            this.b9.BackColor = System.Drawing.Color.White;
            this.b9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b9.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.b9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.b9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b9.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.b9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b9.Location = new System.Drawing.Point(311, 478);
            this.b9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.b9.Name = "b9";
            this.b9.Size = new System.Drawing.Size(73, 32);
            this.b9.TabIndex = 160;
            this.b9.Text = "Delete";
            this.b9.UseVisualStyleBackColor = false;
            this.b9.Visible = false;
            this.b9.Click += new System.EventHandler(this.b9_Click);
            // 
            // b7
            // 
            this.b7.BackColor = System.Drawing.Color.White;
            this.b7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b7.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.b7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.b7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b7.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.b7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b7.Location = new System.Drawing.Point(311, 396);
            this.b7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.b7.Name = "b7";
            this.b7.Size = new System.Drawing.Size(73, 32);
            this.b7.TabIndex = 158;
            this.b7.Text = "Delete";
            this.b7.UseVisualStyleBackColor = false;
            this.b7.Visible = false;
            this.b7.Click += new System.EventHandler(this.b7_Click);
            // 
            // b6
            // 
            this.b6.BackColor = System.Drawing.Color.White;
            this.b6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b6.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.b6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.b6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b6.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.b6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b6.Location = new System.Drawing.Point(183, 478);
            this.b6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.b6.Name = "b6";
            this.b6.Size = new System.Drawing.Size(115, 32);
            this.b6.TabIndex = 157;
            this.b6.Text = "View Results";
            this.b6.UseVisualStyleBackColor = false;
            this.b6.Visible = false;
            this.b6.Click += new System.EventHandler(this.b6_Click);
            // 
            // b5
            // 
            this.b5.BackColor = System.Drawing.Color.White;
            this.b5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b5.Enabled = false;
            this.b5.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.b5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.b5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b5.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.b5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b5.Location = new System.Drawing.Point(183, 478);
            this.b5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.b5.Name = "b5";
            this.b5.Size = new System.Drawing.Size(121, 32);
            this.b5.TabIndex = 156;
            this.b5.Text = "Start";
            this.b5.UseVisualStyleBackColor = false;
            this.b5.Visible = false;
            this.b5.Click += new System.EventHandler(this.b5_Click);
            // 
            // b2
            // 
            this.b2.BackColor = System.Drawing.Color.White;
            this.b2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b2.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.b2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.b2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b2.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.b2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b2.Location = new System.Drawing.Point(183, 396);
            this.b2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.b2.Name = "b2";
            this.b2.Size = new System.Drawing.Size(115, 32);
            this.b2.TabIndex = 153;
            this.b2.Text = "View Results";
            this.b2.UseVisualStyleBackColor = false;
            this.b2.Visible = false;
            this.b2.Click += new System.EventHandler(this.b2_Click);
            // 
            // b1
            // 
            this.b1.BackColor = System.Drawing.Color.White;
            this.b1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b1.Enabled = false;
            this.b1.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.b1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.b1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b1.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.b1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b1.Location = new System.Drawing.Point(183, 396);
            this.b1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.b1.Name = "b1";
            this.b1.Size = new System.Drawing.Size(121, 32);
            this.b1.TabIndex = 152;
            this.b1.Text = "Start";
            this.b1.UseVisualStyleBackColor = false;
            this.b1.Visible = false;
            this.b1.Click += new System.EventHandler(this.b1_Click);
            // 
            // l11
            // 
            this.l11.AutoSize = true;
            this.l11.BackColor = System.Drawing.Color.Transparent;
            this.l11.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l11.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.l11.Location = new System.Drawing.Point(10, 483);
            this.l11.Name = "l11";
            this.l11.Size = new System.Drawing.Size(113, 21);
            this.l11.TabIndex = 151;
            this.l11.Tag = "";
            this.l11.Text = "Short Program";
            this.l11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // l9
            // 
            this.l9.AutoSize = true;
            this.l9.BackColor = System.Drawing.Color.Transparent;
            this.l9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l9.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.l9.Location = new System.Drawing.Point(10, 402);
            this.l9.Name = "l9";
            this.l9.Size = new System.Drawing.Size(142, 21);
            this.l9.TabIndex = 149;
            this.l9.Tag = "";
            this.l9.Text = "Compulsory Dance";
            this.l9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.l9.Visible = false;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.SlateGray;
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(93, 189);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(79, 23);
            this.label14.TabIndex = 148;
            this.label14.Tag = "";
            this.label14.Text = "Categoria";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.SlateGray;
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label20.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(94, 159);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(78, 23);
            this.label20.TabIndex = 147;
            this.label20.Tag = "";
            this.label20.Text = "Specialità";
            // 
            // l8
            // 
            this.l8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.l8.AutoSize = true;
            this.l8.BackColor = System.Drawing.Color.Transparent;
            this.l8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.l8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l8.ForeColor = System.Drawing.Color.Black;
            this.l8.Location = new System.Drawing.Point(183, 189);
            this.l8.Name = "l8";
            this.l8.Size = new System.Drawing.Size(79, 23);
            this.l8.TabIndex = 146;
            this.l8.Tag = "";
            this.l8.Text = "Categoria";
            this.l8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // l7
            // 
            this.l7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.l7.AutoSize = true;
            this.l7.BackColor = System.Drawing.Color.Transparent;
            this.l7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.l7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l7.ForeColor = System.Drawing.Color.Black;
            this.l7.Location = new System.Drawing.Point(183, 159);
            this.l7.Name = "l7";
            this.l7.Size = new System.Drawing.Size(78, 23);
            this.l7.TabIndex = 145;
            this.l7.Tag = "";
            this.l7.Text = "Specialità";
            // 
            // o1
            // 
            this.o1.BackColor = System.Drawing.Color.White;
            this.o1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.o1.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.o1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.o1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.o1.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.o1.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.o1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.o1.Location = new System.Drawing.Point(392, 396);
            this.o1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.o1.Name = "o1";
            this.o1.Size = new System.Drawing.Size(152, 32);
            this.o1.TabIndex = 144;
            this.o1.Text = "Skating Order";
            this.o1.UseVisualStyleBackColor = false;
            this.o1.Visible = false;
            this.o1.Click += new System.EventHandler(this.o1_Click);
            // 
            // l6
            // 
            this.l6.AutoSize = true;
            this.l6.BackColor = System.Drawing.Color.Transparent;
            this.l6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l6.ForeColor = System.Drawing.Color.Black;
            this.l6.Location = new System.Drawing.Point(181, 131);
            this.l6.Name = "l6";
            this.l6.Size = new System.Drawing.Size(132, 21);
            this.l6.TabIndex = 143;
            this.l6.Tag = "";
            this.l6.Text = "Competitor name";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.SlateGray;
            this.label19.Location = new System.Drawing.Point(74, 131);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(99, 21);
            this.label19.TabIndex = 0;
            this.label19.Tag = "";
            this.label19.Text = "Competitors:";
            // 
            // judges
            // 
            this.judges.BackColor = System.Drawing.Color.White;
            this.judges.Cursor = System.Windows.Forms.Cursors.Hand;
            this.judges.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan;
            this.judges.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.judges.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.judges.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.judges.ForeColor = System.Drawing.Color.SlateGray;
            this.judges.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.judges.Location = new System.Drawing.Point(62, 221);
            this.judges.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.judges.Name = "judges";
            this.judges.Size = new System.Drawing.Size(111, 32);
            this.judges.TabIndex = 141;
            this.judges.Text = "Judges Panel";
            this.judges.UseVisualStyleBackColor = false;
            this.judges.Click += new System.EventHandler(this.judges_Click);
            // 
            // l5
            // 
            this.l5.AutoSize = true;
            this.l5.BackColor = System.Drawing.Color.Transparent;
            this.l5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l5.ForeColor = System.Drawing.Color.Black;
            this.l5.Location = new System.Drawing.Point(181, 103);
            this.l5.Name = "l5";
            this.l5.Size = new System.Drawing.Size(132, 21);
            this.l5.TabIndex = 140;
            this.l5.Tag = "";
            this.l5.Text = "Competitor name";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.SlateGray;
            this.label18.Location = new System.Drawing.Point(58, 103);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(116, 21);
            this.label18.TabIndex = 139;
            this.label18.Tag = "";
            this.label18.Text = "Judge Number:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.SlateGray;
            this.label17.Location = new System.Drawing.Point(126, 75);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(49, 21);
            this.label17.TabIndex = 137;
            this.label17.Tag = "";
            this.label17.Text = "Place:";
            // 
            // l3
            // 
            this.l3.AutoSize = true;
            this.l3.BackColor = System.Drawing.Color.Transparent;
            this.l3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l3.ForeColor = System.Drawing.Color.Black;
            this.l3.Location = new System.Drawing.Point(322, 47);
            this.l3.Name = "l3";
            this.l3.Size = new System.Drawing.Size(42, 21);
            this.l3.TabIndex = 136;
            this.l3.Tag = "";
            this.l3.Text = "Date";
            // 
            // l2
            // 
            this.l2.AutoSize = true;
            this.l2.BackColor = System.Drawing.Color.Transparent;
            this.l2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l2.ForeColor = System.Drawing.Color.Black;
            this.l2.Location = new System.Drawing.Point(181, 47);
            this.l2.Name = "l2";
            this.l2.Size = new System.Drawing.Size(42, 21);
            this.l2.TabIndex = 135;
            this.l2.Tag = "";
            this.l2.Text = "Date";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.DarkCyan;
            this.label13.Location = new System.Drawing.Point(286, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 21);
            this.label13.TabIndex = 134;
            this.label13.Tag = "";
            this.label13.Text = "To:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.SlateGray;
            this.label11.Location = new System.Drawing.Point(92, 47);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 21);
            this.label11.TabIndex = 133;
            this.label11.Tag = "";
            this.label11.Text = "Date from:";
            // 
            // reload
            // 
            this.reload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.reload.BackColor = System.Drawing.Color.White;
            this.reload.Cursor = System.Windows.Forms.Cursors.Hand;
            this.reload.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.reload.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.reload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.reload.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reload.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.reload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.reload.Location = new System.Drawing.Point(14, 555);
            this.reload.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.reload.Name = "reload";
            this.reload.Size = new System.Drawing.Size(69, 47);
            this.reload.TabIndex = 136;
            this.reload.Text = "Reload";
            this.reload.UseVisualStyleBackColor = false;
            this.reload.Click += new System.EventHandler(this.reload_Click);
            // 
            // tv
            // 
            this.tv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tv.BackColor = System.Drawing.Color.White;
            this.tv.ContextMenuStrip = this.menu;
            this.tv.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tv.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tv.FullRowSelect = true;
            this.tv.ImageIndex = 20;
            this.tv.ImageList = this.imageList3;
            this.tv.Indent = 27;
            this.tv.ItemHeight = 26;
            this.tv.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(206)))), ((int)(((byte)(56)))));
            this.tv.Location = new System.Drawing.Point(11, 22);
            this.tv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tv.Name = "tv";
            treeNode1.ImageIndex = 21;
            treeNode1.Name = "Nodo0";
            treeNode1.NodeFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            treeNode1.SelectedImageIndex = 21;
            treeNode1.Text = "Events";
            this.tv.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
            this.tv.SelectedImageIndex = 8;
            this.tv.ShowNodeToolTips = true;
            this.tv.ShowRootLines = false;
            this.tv.Size = new System.Drawing.Size(286, 518);
            this.tv.TabIndex = 1;
            this.tv.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tv_NodeMouseClick);
            this.tv.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tv_NodeMouseDoubleClick);
            // 
            // menu
            // 
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(61, 4);
            this.menu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menu_ItemClicked);
            // 
            // connect
            // 
            this.connect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.connect.BackColor = System.Drawing.Color.White;
            this.connect.Cursor = System.Windows.Forms.Cursors.Hand;
            this.connect.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan;
            this.connect.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.connect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.connect.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connect.ForeColor = System.Drawing.Color.DarkCyan;
            this.connect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.connect.Location = new System.Drawing.Point(544, 555);
            this.connect.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.connect.Name = "connect";
            this.connect.Size = new System.Drawing.Size(152, 47);
            this.connect.TabIndex = 140;
            this.connect.Tag = "1";
            this.connect.Text = "Connect Judges";
            this.connect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.connect.UseVisualStyleBackColor = false;
            this.connect.Visible = false;
            this.connect.Click += new System.EventHandler(this.connect_Click);
            // 
            // reset
            // 
            this.reset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.reset.BackColor = System.Drawing.Color.White;
            this.reset.Cursor = System.Windows.Forms.Cursors.Hand;
            this.reset.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.reset.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.reset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.reset.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reset.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.reset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.reset.Location = new System.Drawing.Point(143, 555);
            this.reset.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.reset.Name = "reset";
            this.reset.Size = new System.Drawing.Size(69, 47);
            this.reset.TabIndex = 139;
            this.reset.Text = "Reset";
            this.reset.UseVisualStyleBackColor = false;
            this.reset.Click += new System.EventHandler(this.reset_Click);
            // 
            // save
            // 
            this.save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.save.BackColor = System.Drawing.Color.White;
            this.save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.save.Enabled = false;
            this.save.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan;
            this.save.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.save.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.save.ForeColor = System.Drawing.Color.DarkCyan;
            this.save.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.save.Location = new System.Drawing.Point(698, 555);
            this.save.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(162, 47);
            this.save.TabIndex = 147;
            this.save.Tag = "1";
            this.save.Text = "Save Competition";
            this.save.UseVisualStyleBackColor = false;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(275, 566);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 25);
            this.label5.TabIndex = 151;
            this.label5.Tag = "";
            this.label5.Text = "0";
            this.label5.Visible = false;
            // 
            // pb
            // 
            this.pb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pb.Image = global::RollartSystemTech.Properties.Resources.waiting;
            this.pb.Location = new System.Drawing.Point(218, 555);
            this.pb.Name = "pb";
            this.pb.Size = new System.Drawing.Size(54, 47);
            this.pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb.TabIndex = 141;
            this.pb.TabStop = false;
            this.pb.Visible = false;
            this.pb.Click += new System.EventHandler(this.pb_Click);
            // 
            // panReport
            // 
            this.panReport.BackColor = System.Drawing.Color.White;
            this.panReport.Controls.Add(this.button3);
            this.panReport.Controls.Add(this.result);
            this.panReport.Controls.Add(this.classifica);
            this.panReport.Controls.Add(this.panel);
            this.panReport.Controls.Add(this.back);
            this.panReport.Controls.Add(this.cr);
            this.panReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panReport.Location = new System.Drawing.Point(0, 0);
            this.panReport.Name = "panReport";
            this.panReport.Size = new System.Drawing.Size(872, 684);
            this.panReport.TabIndex = 148;
            this.panReport.Visible = false;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.DarkCyan;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.Location = new System.Drawing.Point(332, 555);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(112, 47);
            this.button3.TabIndex = 150;
            this.button3.Tag = "1";
            this.button3.Text = "Export All";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // result
            // 
            this.result.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.result.BackColor = System.Drawing.Color.White;
            this.result.Cursor = System.Windows.Forms.Cursors.Hand;
            this.result.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan;
            this.result.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.result.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.result.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.result.ForeColor = System.Drawing.Color.DarkCyan;
            this.result.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.result.ImageIndex = 2;
            this.result.ImageList = this.imageList3;
            this.result.Location = new System.Drawing.Point(446, 555);
            this.result.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(89, 47);
            this.result.TabIndex = 149;
            this.result.Tag = "1";
            this.result.Text = "Results";
            this.result.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.result.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.result.UseVisualStyleBackColor = false;
            this.result.Visible = false;
            this.result.Click += new System.EventHandler(this.result_Click);
            // 
            // classifica
            // 
            this.classifica.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.classifica.BackColor = System.Drawing.Color.White;
            this.classifica.Cursor = System.Windows.Forms.Cursors.Hand;
            this.classifica.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan;
            this.classifica.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.classifica.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.classifica.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.classifica.ForeColor = System.Drawing.Color.DarkCyan;
            this.classifica.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.classifica.ImageIndex = 2;
            this.classifica.ImageList = this.imageList3;
            this.classifica.Location = new System.Drawing.Point(537, 555);
            this.classifica.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.classifica.Name = "classifica";
            this.classifica.Size = new System.Drawing.Size(124, 47);
            this.classifica.TabIndex = 148;
            this.classifica.Tag = "1";
            this.classifica.Text = "Classification";
            this.classifica.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.classifica.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.classifica.UseVisualStyleBackColor = false;
            this.classifica.Visible = false;
            this.classifica.Click += new System.EventHandler(this.classifica_Click);
            // 
            // panel
            // 
            this.panel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel.BackColor = System.Drawing.Color.White;
            this.panel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan;
            this.panel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.panel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.panel.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel.ForeColor = System.Drawing.Color.DarkCyan;
            this.panel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.panel.ImageIndex = 2;
            this.panel.ImageList = this.imageList3;
            this.panel.Location = new System.Drawing.Point(663, 555);
            this.panel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(135, 47);
            this.panel.TabIndex = 147;
            this.panel.Tag = "1";
            this.panel.Text = "Judges Scores";
            this.panel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.panel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.panel.UseVisualStyleBackColor = false;
            this.panel.Visible = false;
            this.panel.Click += new System.EventHandler(this.panel_Click);
            // 
            // back
            // 
            this.back.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.back.BackColor = System.Drawing.Color.White;
            this.back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.back.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan;
            this.back.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.back.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.back.ForeColor = System.Drawing.Color.DarkCyan;
            this.back.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.back.Location = new System.Drawing.Point(800, 555);
            this.back.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(60, 47);
            this.back.TabIndex = 146;
            this.back.Tag = "1";
            this.back.Text = "Back";
            this.back.UseVisualStyleBackColor = false;
            this.back.Visible = false;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // cr
            // 
            this.cr.ActiveViewIndex = -1;
            this.cr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cr.Cursor = System.Windows.Forms.Cursors.Default;
            this.cr.EnableDrillDown = false;
            this.cr.Location = new System.Drawing.Point(0, 0);
            this.cr.Name = "cr";
            this.cr.Size = new System.Drawing.Size(872, 553);
            this.cr.TabIndex = 0;
            this.cr.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // deleteall
            // 
            this.deleteall.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deleteall.BackColor = System.Drawing.Color.White;
            this.deleteall.Cursor = System.Windows.Forms.Cursors.Hand;
            this.deleteall.Enabled = false;
            this.deleteall.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.deleteall.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.deleteall.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteall.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteall.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.deleteall.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.deleteall.Location = new System.Drawing.Point(143, 555);
            this.deleteall.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.deleteall.Name = "deleteall";
            this.deleteall.Size = new System.Drawing.Size(84, 47);
            this.deleteall.TabIndex = 150;
            this.deleteall.Text = "Delete all";
            this.deleteall.UseVisualStyleBackColor = false;
            this.deleteall.Visible = false;
            // 
            // panNewEvent
            // 
            this.panNewEvent.AutoScroll = true;
            this.panNewEvent.BackColor = System.Drawing.Color.White;
            this.panNewEvent.Controls.Add(this.groupBox1);
            this.panNewEvent.Controls.Add(this.pb1);
            this.panNewEvent.Controls.Add(this.nJudges);
            this.panNewEvent.Controls.Add(this.confirmNewEvent);
            this.panNewEvent.Controls.Add(this.label9);
            this.panNewEvent.Controls.Add(this.lv1);
            this.panNewEvent.Controls.Add(this.label7);
            this.panNewEvent.Controls.Add(this.browse);
            this.panNewEvent.Controls.Add(this.cb6);
            this.panNewEvent.Controls.Add(this.cb5);
            this.panNewEvent.Controls.Add(this.cb4);
            this.panNewEvent.Controls.Add(this.cb3);
            this.panNewEvent.Controls.Add(this.cb2);
            this.panNewEvent.Controls.Add(this.cb1);
            this.panNewEvent.Controls.Add(this.cbCategory);
            this.panNewEvent.Controls.Add(this.label16);
            this.panNewEvent.Controls.Add(this.label15);
            this.panNewEvent.Controls.Add(this.cbEvent);
            this.panNewEvent.Controls.Add(this.label12);
            this.panNewEvent.Controls.Add(this.label3);
            this.panNewEvent.Controls.Add(this.label2);
            this.panNewEvent.Controls.Add(this.place);
            this.panNewEvent.Controls.Add(this.label4);
            this.panNewEvent.Controls.Add(this.name);
            this.panNewEvent.Controls.Add(this.dateFrom);
            this.panNewEvent.Controls.Add(this.dateTo);
            this.panNewEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panNewEvent.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panNewEvent.Location = new System.Drawing.Point(0, 0);
            this.panNewEvent.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panNewEvent.Name = "panNewEvent";
            this.panNewEvent.Size = new System.Drawing.Size(872, 684);
            this.panNewEvent.TabIndex = 2;
            this.panNewEvent.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.club);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.naz);
            this.groupBox1.Controls.Add(this.add);
            this.groupBox1.Controls.Add(this.lv2);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.namep);
            this.groupBox1.Controls.Add(this.delete);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.groupBox1.Location = new System.Drawing.Point(14, 234);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(582, 364);
            this.groupBox1.TabIndex = 150;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Competitors";
            // 
            // club
            // 
            this.club.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.club.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.club.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.club.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.club.ForeColor = System.Drawing.Color.Black;
            this.club.Location = new System.Drawing.Point(320, 64);
            this.club.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.club.Name = "club";
            this.club.Size = new System.Drawing.Size(176, 25);
            this.club.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DarkOrange;
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(17, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 21);
            this.label6.TabIndex = 144;
            this.label6.Tag = "";
            this.label6.Text = "Name";
            // 
            // naz
            // 
            this.naz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.naz.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.naz.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.naz.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.naz.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.naz.ForeColor = System.Drawing.Color.Black;
            this.naz.Location = new System.Drawing.Point(502, 64);
            this.naz.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.naz.Name = "naz";
            this.naz.Size = new System.Drawing.Size(62, 25);
            this.naz.TabIndex = 2;
            // 
            // add
            // 
            this.add.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.add.BackColor = System.Drawing.Color.Transparent;
            this.add.Cursor = System.Windows.Forms.Cursors.Hand;
            this.add.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.add.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.add.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.add.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.add.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.add.Location = new System.Drawing.Point(370, 93);
            this.add.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(92, 30);
            this.add.TabIndex = 3;
            this.add.Text = "Add";
            this.add.UseVisualStyleBackColor = false;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // lv2
            // 
            this.lv2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lv2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lv2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader4,
            this.columnHeader5});
            this.lv2.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv2.FullRowSelect = true;
            this.lv2.GridLines = true;
            this.lv2.Location = new System.Drawing.Point(10, 127);
            this.lv2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lv2.MultiSelect = false;
            this.lv2.Name = "lv2";
            this.lv2.ShowGroups = false;
            this.lv2.ShowItemToolTips = true;
            this.lv2.Size = new System.Drawing.Size(554, 225);
            this.lv2.TabIndex = 146;
            this.lv2.UseCompatibleStateImageBehavior = false;
            this.lv2.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Order";
            this.columnHeader1.Width = 61;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Skater ";
            this.columnHeader2.Width = 249;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Club";
            this.columnHeader4.Width = 182;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Nation";
            this.columnHeader5.Width = 55;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.DarkOrange;
            this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label8.Location = new System.Drawing.Point(498, 37);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 21);
            this.label8.TabIndex = 145;
            this.label8.Tag = "";
            this.label8.Text = "Country";
            // 
            // namep
            // 
            this.namep.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.namep.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.namep.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.namep.BackColor = System.Drawing.Color.White;
            this.namep.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.namep.Location = new System.Drawing.Point(10, 64);
            this.namep.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.namep.Name = "namep";
            this.namep.Size = new System.Drawing.Size(304, 25);
            this.namep.TabIndex = 0;
            // 
            // delete
            // 
            this.delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.delete.BackColor = System.Drawing.Color.Transparent;
            this.delete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.delete.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.delete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.delete.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.delete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.delete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.delete.Location = new System.Drawing.Point(468, 93);
            this.delete.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(96, 30);
            this.delete.TabIndex = 149;
            this.delete.Text = "Delete";
            this.delete.UseVisualStyleBackColor = false;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkOrange;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(319, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 21);
            this.label1.TabIndex = 65;
            this.label1.Tag = "";
            this.label1.Text = "Club";
            // 
            // pb1
            // 
            this.pb1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pb1.BackColor = System.Drawing.Color.White;
            this.pb1.Location = new System.Drawing.Point(605, 575);
            this.pb1.Maximum = 7;
            this.pb1.Name = "pb1";
            this.pb1.Size = new System.Drawing.Size(248, 20);
            this.pb1.Step = 1;
            this.pb1.TabIndex = 148;
            this.pb1.Visible = false;
            // 
            // nJudges
            // 
            this.nJudges.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nJudges.BackColor = System.Drawing.Color.White;
            this.nJudges.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nJudges.Location = new System.Drawing.Point(728, 256);
            this.nJudges.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.nJudges.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.nJudges.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nJudges.Name = "nJudges";
            this.nJudges.Size = new System.Drawing.Size(38, 32);
            this.nJudges.TabIndex = 15;
            this.nJudges.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nJudges.ValueChanged += new System.EventHandler(this.nJudges_ValueChanged);
            // 
            // confirmNewEvent
            // 
            this.confirmNewEvent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.confirmNewEvent.BackColor = System.Drawing.Color.Transparent;
            this.confirmNewEvent.Cursor = System.Windows.Forms.Cursors.Hand;
            this.confirmNewEvent.Enabled = false;
            this.confirmNewEvent.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.confirmNewEvent.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.confirmNewEvent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.confirmNewEvent.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirmNewEvent.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.confirmNewEvent.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.confirmNewEvent.Location = new System.Drawing.Point(602, 523);
            this.confirmNewEvent.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.confirmNewEvent.Name = "confirmNewEvent";
            this.confirmNewEvent.Size = new System.Drawing.Size(255, 75);
            this.confirmNewEvent.TabIndex = 17;
            this.confirmNewEvent.Text = "Insert Event";
            this.confirmNewEvent.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.confirmNewEvent.UseVisualStyleBackColor = false;
            this.confirmNewEvent.Click += new System.EventHandler(this.confirmNewEvent_Click);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.DarkOrange;
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(614, 269);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 21);
            this.label9.TabIndex = 147;
            this.label9.Tag = "";
            this.label9.Text = "Judge list";
            // 
            // lv1
            // 
            this.lv1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lv1.BackColor = System.Drawing.Color.OldLace;
            this.lv1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader6});
            this.lv1.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv1.FullRowSelect = true;
            this.lv1.GridLines = true;
            this.lv1.LabelEdit = true;
            this.lv1.LargeImageList = this.imageList1;
            this.lv1.Location = new System.Drawing.Point(602, 294);
            this.lv1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lv1.MultiSelect = false;
            this.lv1.Name = "lv1";
            this.lv1.ShowGroups = false;
            this.lv1.ShowItemToolTips = true;
            this.lv1.Size = new System.Drawing.Size(256, 225);
            this.lv1.SmallImageList = this.imageList1;
            this.lv1.TabIndex = 142;
            this.lv1.UseCompatibleStateImageBehavior = false;
            this.lv1.View = System.Windows.Forms.View.SmallIcon;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "";
            this.columnHeader3.Width = 105;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "";
            this.columnHeader6.Width = 154;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.DarkOrange;
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(598, 238);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 25);
            this.label7.TabIndex = 133;
            this.label7.Tag = "";
            this.label7.Text = "Judge Panel*";
            // 
            // browse
            // 
            this.browse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browse.BackColor = System.Drawing.Color.Transparent;
            this.browse.Cursor = System.Windows.Forms.Cursors.Hand;
            this.browse.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.browse.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.browse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.browse.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.browse.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.browse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.browse.Location = new System.Drawing.Point(773, 256);
            this.browse.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.browse.Name = "browse";
            this.browse.Size = new System.Drawing.Size(84, 32);
            this.browse.TabIndex = 12;
            this.browse.Text = "Select";
            this.browse.UseVisualStyleBackColor = false;
            this.browse.Click += new System.EventHandler(this.browse_Click);
            // 
            // cb6
            // 
            this.cb6.AutoSize = true;
            this.cb6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb6.FlatAppearance.BorderSize = 0;
            this.cb6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb6.ForeColor = System.Drawing.Color.Black;
            this.cb6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cb6.Location = new System.Drawing.Point(342, 177);
            this.cb6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb6.Name = "cb6";
            this.cb6.Size = new System.Drawing.Size(66, 25);
            this.cb6.TabIndex = 5;
            this.cb6.Tag = "6";
            this.cb6.Text = "show";
            this.cb6.UseVisualStyleBackColor = true;
            this.cb6.Visible = false;
            // 
            // cb5
            // 
            this.cb5.AutoSize = true;
            this.cb5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb5.FlatAppearance.BorderSize = 0;
            this.cb5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb5.ForeColor = System.Drawing.Color.Black;
            this.cb5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cb5.Location = new System.Drawing.Point(466, 177);
            this.cb5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb5.Name = "cb5";
            this.cb5.Size = new System.Drawing.Size(102, 25);
            this.cb5.TabIndex = 6;
            this.cb5.Tag = "5";
            this.cb5.Text = "freedance";
            this.cb5.UseVisualStyleBackColor = true;
            this.cb5.Visible = false;
            // 
            // cb4
            // 
            this.cb4.AutoSize = true;
            this.cb4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb4.FlatAppearance.BorderSize = 0;
            this.cb4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb4.ForeColor = System.Drawing.Color.Black;
            this.cb4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cb4.Location = new System.Drawing.Point(342, 177);
            this.cb4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb4.Name = "cb4";
            this.cb4.Size = new System.Drawing.Size(62, 25);
            this.cb4.TabIndex = 6;
            this.cb4.Tag = "4";
            this.cb4.Text = "style";
            this.cb4.UseVisualStyleBackColor = true;
            this.cb4.Visible = false;
            // 
            // cb3
            // 
            this.cb3.AutoSize = true;
            this.cb3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb3.FlatAppearance.BorderSize = 0;
            this.cb3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb3.ForeColor = System.Drawing.Color.Black;
            this.cb3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cb3.Location = new System.Drawing.Point(342, 202);
            this.cb3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb3.Name = "cb3";
            this.cb3.Size = new System.Drawing.Size(91, 25);
            this.cb3.TabIndex = 9;
            this.cb3.Tag = "3";
            this.cb3.Text = "compuls";
            this.cb3.UseVisualStyleBackColor = true;
            this.cb3.Visible = false;
            // 
            // cb2
            // 
            this.cb2.AutoSize = true;
            this.cb2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb2.FlatAppearance.BorderSize = 0;
            this.cb2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb2.ForeColor = System.Drawing.Color.Black;
            this.cb2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cb2.Location = new System.Drawing.Point(466, 177);
            this.cb2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb2.Name = "cb2";
            this.cb2.Size = new System.Drawing.Size(61, 25);
            this.cb2.TabIndex = 8;
            this.cb2.Tag = "2";
            this.cb2.Text = "long";
            this.cb2.UseVisualStyleBackColor = true;
            this.cb2.Visible = false;
            // 
            // cb1
            // 
            this.cb1.AutoSize = true;
            this.cb1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb1.FlatAppearance.BorderSize = 0;
            this.cb1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb1.ForeColor = System.Drawing.Color.Black;
            this.cb1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cb1.Location = new System.Drawing.Point(342, 177);
            this.cb1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb1.Name = "cb1";
            this.cb1.Size = new System.Drawing.Size(65, 25);
            this.cb1.TabIndex = 5;
            this.cb1.Tag = "1";
            this.cb1.Text = "short";
            this.cb1.UseVisualStyleBackColor = true;
            this.cb1.Visible = false;
            // 
            // cbCategory
            // 
            this.cbCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCategory.BackColor = System.Drawing.Color.White;
            this.cbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCategory.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCategory.FormattingEnabled = true;
            this.cbCategory.Location = new System.Drawing.Point(635, 143);
            this.cbCategory.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbCategory.Name = "cbCategory";
            this.cbCategory.Size = new System.Drawing.Size(218, 29);
            this.cbCategory.TabIndex = 5;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.DarkOrange;
            this.label16.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label16.Location = new System.Drawing.Point(181, 107);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(33, 25);
            this.label16.TabIndex = 69;
            this.label16.Tag = "";
            this.label16.Text = "To";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.DarkOrange;
            this.label15.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label15.Location = new System.Drawing.Point(629, 107);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 25);
            this.label15.TabIndex = 68;
            this.label15.Tag = "";
            this.label15.Text = "Category*";
            // 
            // cbEvent
            // 
            this.cbEvent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbEvent.BackColor = System.Drawing.Color.White;
            this.cbEvent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEvent.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEvent.FormattingEnabled = true;
            this.cbEvent.Location = new System.Drawing.Point(342, 142);
            this.cbEvent.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbEvent.Name = "cbEvent";
            this.cbEvent.Size = new System.Drawing.Size(252, 29);
            this.cbEvent.TabIndex = 4;
            this.cbEvent.SelectedIndexChanged += new System.EventHandler(this.cbEvent_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.DarkOrange;
            this.label12.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label12.Location = new System.Drawing.Point(337, 107);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(107, 25);
            this.label12.TabIndex = 66;
            this.label12.Tag = "";
            this.label12.Text = "Event type*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DarkOrange;
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(6, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 25);
            this.label3.TabIndex = 62;
            this.label3.Tag = "";
            this.label3.Text = "Date from";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DarkOrange;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(629, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 25);
            this.label2.TabIndex = 61;
            this.label2.Tag = "";
            this.label2.Text = "Place";
            // 
            // place
            // 
            this.place.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.place.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.place.Location = new System.Drawing.Point(634, 57);
            this.place.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.place.Name = "place";
            this.place.Size = new System.Drawing.Size(218, 29);
            this.place.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(6, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 25);
            this.label4.TabIndex = 60;
            this.label4.Tag = "";
            this.label4.Text = "Event name*";
            // 
            // name
            // 
            this.name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.name.BackColor = System.Drawing.Color.White;
            this.name.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name.Location = new System.Drawing.Point(26, 57);
            this.name.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(568, 29);
            this.name.TabIndex = 0;
            // 
            // dateFrom
            // 
            this.dateFrom.CalendarMonthBackground = System.Drawing.SystemColors.Info;
            this.dateFrom.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateFrom.Location = new System.Drawing.Point(26, 142);
            this.dateFrom.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.Size = new System.Drawing.Size(112, 29);
            this.dateFrom.TabIndex = 2;
            // 
            // dateTo
            // 
            this.dateTo.CalendarMonthBackground = System.Drawing.SystemColors.Info;
            this.dateTo.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTo.Location = new System.Drawing.Point(185, 142);
            this.dateTo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateTo.Name = "dateTo";
            this.dateTo.Size = new System.Drawing.Size(112, 29);
            this.dateTo.TabIndex = 3;
            // 
            // panLog
            // 
            this.panLog.Controls.Add(this.log);
            this.panLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panLog.Location = new System.Drawing.Point(0, 0);
            this.panLog.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panLog.Name = "panLog";
            this.panLog.Size = new System.Drawing.Size(872, 684);
            this.panLog.TabIndex = 94;
            this.panLog.Visible = false;
            // 
            // log
            // 
            this.log.BackColor = System.Drawing.Color.White;
            this.log.Font = new System.Drawing.Font("Courier New", 8F);
            this.log.ForeColor = System.Drawing.SystemColors.ControlText;
            this.log.Location = new System.Drawing.Point(0, 45);
            this.log.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.log.Multiline = true;
            this.log.Name = "log";
            this.log.ReadOnly = true;
            this.log.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.log.Size = new System.Drawing.Size(872, 665);
            this.log.TabIndex = 93;
            // 
            // of1
            // 
            this.of1.FileName = "rolljudge2.s3db";
            this.of1.Filter = "File SQLite|*.s3db";
            this.of1.InitialDirectory = "C:\\RollartSystem";
            this.of1.RestoreDirectory = true;
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.BackColor = System.Drawing.Color.Transparent;
            this.message.Dock = System.Windows.Forms.DockStyle.Fill;
            this.message.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.message.ForeColor = System.Drawing.Color.Yellow;
            this.message.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.message.Location = new System.Drawing.Point(0, 0);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(34, 21);
            this.message.TabIndex = 98;
            this.message.Tag = "";
            this.message.Text = "      ";
            // 
            // message2
            // 
            this.message2.BackColor = System.Drawing.Color.Transparent;
            this.message2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.message2.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.message2.ForeColor = System.Drawing.Color.White;
            this.message2.Location = new System.Drawing.Point(0, 57);
            this.message2.Name = "message2";
            this.message2.Size = new System.Drawing.Size(1017, 22);
            this.message2.TabIndex = 133;
            this.message2.Tag = "";
            this.message2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.SlateGray;
            this.panel2.Controls.Add(this.message2);
            this.panel2.Controls.Add(this.message);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 654);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1017, 79);
            this.panel2.TabIndex = 1;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // EventsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1017, 733);
            this.ControlBox = false;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "EventsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RollArt System";
            this.Load += new System.EventHandler(this.EventsForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panStartEvent.ResumeLayout(false);
            this.panStartEvent.PerformLayout();
            this.gara.ResumeLayout(false);
            this.gara.PerformLayout();
            this.panStyle.ResumeLayout(false);
            this.panStyle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb)).EndInit();
            this.panReport.ResumeLayout(false);
            this.panNewEvent.ResumeLayout(false);
            this.panNewEvent.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nJudges)).EndInit();
            this.panLog.ResumeLayout(false);
            this.panLog.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        //private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel panStartEvent;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buTech;
        private System.Windows.Forms.Button titolo;
        private System.Windows.Forms.Panel panNewEvent;
        private System.Windows.Forms.OpenFileDialog of1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.FolderBrowserDialog fd1;
        private System.Windows.Forms.CheckBox cb6;
        private System.Windows.Forms.CheckBox cb5;
        private System.Windows.Forms.CheckBox cb4;
        private System.Windows.Forms.CheckBox cb3;
        private System.Windows.Forms.CheckBox cb2;
        private System.Windows.Forms.CheckBox cb1;
        private System.Windows.Forms.ComboBox cbCategory;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cbEvent;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox place;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.DateTimePicker dateFrom;
        private System.Windows.Forms.DateTimePicker dateTo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button browse;
        private System.Windows.Forms.ListView lv1;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListView lv2;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox club;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button add;
        private System.Windows.Forms.TextBox namep;
        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button confirmNewEvent;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.TextBox naz;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.TreeView tv;
        private System.Windows.Forms.Button reload;
        private System.Windows.Forms.Panel gara;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label l3;
        private System.Windows.Forms.Label l2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label l5;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button judges;
        private System.Windows.Forms.Button o1;
        private System.Windows.Forms.Label l6;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label l7;
        private System.Windows.Forms.Label l8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label l9;
        private System.Windows.Forms.Label l11;
        private System.Windows.Forms.Label l10;
        private System.Windows.Forms.Button b1;
        private System.Windows.Forms.Button b2;
        private System.Windows.Forms.Button b6;
        private System.Windows.Forms.Button b5;
        private System.Windows.Forms.Button b4;
        private System.Windows.Forms.Button b3;
        private System.Windows.Forms.Button b9;
        private System.Windows.Forms.Button b8;
        private System.Windows.Forms.Button b7;
        private System.Windows.Forms.ListView lv3;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Button o3;
        private System.Windows.Forms.Button o2;
        private System.Windows.Forms.Label l20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button deletegare;
        private System.Windows.Forms.Button reset;
        private System.Windows.Forms.Button connect;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panLog;
        private System.Windows.Forms.TextBox log;
        private System.Windows.Forms.NumericUpDown nJudges;
        private System.Windows.Forms.Panel panStyle;
        private System.Windows.Forms.Label labPattern;
        private System.Windows.Forms.ComboBox cbPattern;
        private System.Windows.Forms.PictureBox pb;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private RollartSystemTech.Report.ResultReport ResultReport1;
        private RollartSystemTech.Report.PanelReport PanelReport1;
        private RollartSystemTech.Report.FinalReport FinalReport1;
        private System.Windows.Forms.ImageList imageList3;
        private System.Windows.Forms.ProgressBar pb1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox l1;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.TextBox l4;
        private System.Windows.Forms.Panel panReport;
        private System.Windows.Forms.Button classifica;
        private System.Windows.Forms.Button panel;
        private System.Windows.Forms.Button back;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer cr;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.Label message2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button result;
        private System.Windows.Forms.Button deleteall;
        private System.Windows.Forms.Button button3;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ContextMenuStrip menu;
        private System.Windows.Forms.Label label5;
    }
}
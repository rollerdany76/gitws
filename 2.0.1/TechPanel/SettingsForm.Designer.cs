﻿namespace RollartSystemTech
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.err4 = new System.Windows.Forms.Label();
            this.save = new System.Windows.Forms.Button();
            this.titolo = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.message = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.button5 = new System.Windows.Forms.Button();
            this.video = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.buTech = new System.Windows.Forms.Button();
            this.panCompetitors = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.region2 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.dg4 = new System.Windows.Forms.DataGridView();
            this.dg4ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dg4Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dg4Societa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dg4Region = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dg4Country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dg4Spec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.found2 = new System.Windows.Forms.Label();
            this.pb2 = new System.Windows.Forms.PictureBox();
            this.delete = new System.Windows.Forms.Button();
            this.err2 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.add = new System.Windows.Forms.Button();
            this.club = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.country = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.panTech = new System.Windows.Forms.Panel();
            this.port = new System.Windows.Forms.NumericUpDown();
            this.cbSec = new System.Windows.Forms.ComboBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.ts1 = new JCS.ToggleSwitch();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.rb12 = new System.Windows.Forms.RadioButton();
            this.pb1 = new System.Windows.Forms.PictureBox();
            this.cb4 = new System.Windows.Forms.CheckBox();
            this.button15 = new System.Windows.Forms.Button();
            this.rb11 = new System.Windows.Forms.RadioButton();
            this.label44 = new System.Windows.Forms.Label();
            this.button14 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ipT = new IPAddressControlLib.IPAddressControl();
            this.root = new System.Windows.Forms.TextBox();
            this.nJudges = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.panVideo = new System.Windows.Forms.Panel();
            this.portVideo = new System.Windows.Forms.NumericUpDown();
            this.pingVideo = new System.Windows.Forms.ComboBox();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.ipVideo = new IPAddressControlLib.IPAddressControl();
            this.label58 = new System.Windows.Forms.Label();
            this.panJudges = new System.Windows.Forms.Panel();
            this.label47 = new System.Windows.Forms.Label();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.found = new System.Windows.Forms.Label();
            this.pb = new System.Windows.Forms.PictureBox();
            this.button10 = new System.Windows.Forms.Button();
            this.err3 = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.region = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.country2 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.name2 = new System.Windows.Forms.TextBox();
            this.dg5 = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dg5Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dg5Region = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dg5Country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.panElements = new System.Windows.Forms.Panel();
            this.panEl = new System.Windows.Forms.Panel();
            this.dg3 = new System.Windows.Forms.DataGridView();
            this.label45 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cb3 = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.panParameters = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cbCategory = new System.Windows.Forms.ComboBox();
            this.dg1 = new System.Windows.Forms.DataGridView();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.dg2 = new System.Windows.Forms.DataGridView();
            this.cb2 = new System.Windows.Forms.ComboBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label41 = new System.Windows.Forms.Label();
            this.rb3 = new System.Windows.Forms.RadioButton();
            this.label40 = new System.Windows.Forms.Label();
            this.rb2 = new System.Windows.Forms.RadioButton();
            this.cbCategoryDance = new System.Windows.Forms.ComboBox();
            this.rb1 = new System.Windows.Forms.RadioButton();
            this.label39 = new System.Windows.Forms.Label();
            this.dgDance = new System.Windows.Forms.DataGridView();
            this.label34 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.dgPrecision = new System.Windows.Forms.DataGridView();
            this.cbPrecision = new System.Windows.Forms.ComboBox();
            this.panDB = new System.Windows.Forms.Panel();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.backupfolder = new System.Windows.Forms.TextBox();
            this.lv3 = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label26 = new System.Windows.Forms.Label();
            this.lv2 = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label22 = new System.Windows.Forms.Label();
            this.lv1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label21 = new System.Windows.Forms.Label();
            this.mess1 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.browse = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.dbName = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.of1 = new System.Windows.Forms.OpenFileDialog();
            this.fd1 = new System.Windows.Forms.FolderBrowserDialog();
            this.of2 = new System.Windows.Forms.OpenFileDialog();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openXML = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panCompetitors.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb2)).BeginInit();
            this.panTech.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.port)).BeginInit();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nJudges)).BeginInit();
            this.panVideo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.portVideo)).BeginInit();
            this.panJudges.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg5)).BeginInit();
            this.panElements.SuspendLayout();
            this.panEl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg3)).BeginInit();
            this.panParameters.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDance)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPrecision)).BeginInit();
            this.panDB.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.err4);
            this.panel1.Controls.Add(this.save);
            this.panel1.Controls.Add(this.titolo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1124, 46);
            this.panel1.TabIndex = 0;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // err4
            // 
            this.err4.AutoSize = true;
            this.err4.BackColor = System.Drawing.Color.Transparent;
            this.err4.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.err4.ForeColor = System.Drawing.Color.Black;
            this.err4.Location = new System.Drawing.Point(367, 15);
            this.err4.Name = "err4";
            this.err4.Size = new System.Drawing.Size(0, 26);
            this.err4.TabIndex = 123;
            this.err4.Tag = "";
            // 
            // save
            // 
            this.save.BackColor = System.Drawing.Color.Transparent;
            this.save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.save.Enabled = false;
            this.save.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.save.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumPurple;
            this.save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.save.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.save.ForeColor = System.Drawing.Color.White;
            this.save.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.save.Location = new System.Drawing.Point(236, 8);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(123, 31);
            this.save.TabIndex = 97;
            this.save.Text = "Save Settings";
            this.save.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.save.UseVisualStyleBackColor = false;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // titolo
            // 
            this.titolo.AutoSize = true;
            this.titolo.BackColor = System.Drawing.Color.Transparent;
            this.titolo.Cursor = System.Windows.Forms.Cursors.Default;
            this.titolo.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.titolo.FlatAppearance.BorderSize = 0;
            this.titolo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.titolo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.titolo.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titolo.ForeColor = System.Drawing.Color.White;
            this.titolo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.titolo.ImageKey = "gear_32.png";
            this.titolo.Location = new System.Drawing.Point(3, 3);
            this.titolo.Name = "titolo";
            this.titolo.Size = new System.Drawing.Size(239, 42);
            this.titolo.TabIndex = 96;
            this.titolo.Text = "Settings --> General";
            this.titolo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.titolo.UseVisualStyleBackColor = false;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "note.png");
            this.imageList1.Images.SetKeyName(1, "document.png");
            this.imageList1.Images.SetKeyName(2, "iconmonstr-user-8-icon-256.png");
            this.imageList1.Images.SetKeyName(3, "ws_Artistic.jpg");
            this.imageList1.Images.SetKeyName(4, "cup.png");
            this.imageList1.Images.SetKeyName(5, "info.png");
            this.imageList1.Images.SetKeyName(6, "male-user.png");
            this.imageList1.Images.SetKeyName(7, "settings.png");
            this.imageList1.Images.SetKeyName(8, "star.png");
            this.imageList1.Images.SetKeyName(9, "documents.png");
            this.imageList1.Images.SetKeyName(10, "data.png");
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkGray;
            this.panel2.Controls.Add(this.message);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 643);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1124, 71);
            this.panel2.TabIndex = 1;
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.BackColor = System.Drawing.Color.Transparent;
            this.message.Dock = System.Windows.Forms.DockStyle.Fill;
            this.message.Font = new System.Drawing.Font("Trebuchet MS", 12F);
            this.message.ForeColor = System.Drawing.Color.Yellow;
            this.message.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.message.Location = new System.Drawing.Point(0, 0);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(0, 26);
            this.message.TabIndex = 99;
            this.message.Tag = "";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 46);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.splitContainer1.Panel1.Controls.Add(this.button5);
            this.splitContainer1.Panel1.Controls.Add(this.video);
            this.splitContainer1.Panel1.Controls.Add(this.button13);
            this.splitContainer1.Panel1.Controls.Add(this.button1);
            this.splitContainer1.Panel1.Controls.Add(this.button6);
            this.splitContainer1.Panel1.Controls.Add(this.button2);
            this.splitContainer1.Panel1.Controls.Add(this.button4);
            this.splitContainer1.Panel1.Controls.Add(this.buTech);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel2.Controls.Add(this.panCompetitors);
            this.splitContainer1.Panel2.Controls.Add(this.panTech);
            this.splitContainer1.Panel2.Controls.Add(this.panVideo);
            this.splitContainer1.Panel2.Controls.Add(this.panJudges);
            this.splitContainer1.Panel2.Controls.Add(this.panElements);
            this.splitContainer1.Panel2.Controls.Add(this.panParameters);
            this.splitContainer1.Panel2.Controls.Add(this.panDB);
            this.splitContainer1.Size = new System.Drawing.Size(1124, 597);
            this.splitContainer1.SplitterDistance = 157;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 2;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Transparent;
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.Dock = System.Windows.Forms.DockStyle.Top;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SkyBlue;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.Black;
            this.button5.Image = global::RollartSystemTech.Properties.Resources.logout;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(0, 294);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(157, 42);
            this.button5.TabIndex = 103;
            this.button5.Text = "Close";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // video
            // 
            this.video.BackColor = System.Drawing.Color.Transparent;
            this.video.Cursor = System.Windows.Forms.Cursors.Hand;
            this.video.Dock = System.Windows.Forms.DockStyle.Top;
            this.video.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.video.FlatAppearance.BorderSize = 0;
            this.video.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.video.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.video.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.video.ForeColor = System.Drawing.Color.Black;
            this.video.Image = global::RollartSystemTech.Properties.Resources.display;
            this.video.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.video.Location = new System.Drawing.Point(0, 252);
            this.video.Name = "video";
            this.video.Size = new System.Drawing.Size(157, 42);
            this.video.TabIndex = 105;
            this.video.Text = "Video";
            this.video.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.video.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.video.UseVisualStyleBackColor = false;
            this.video.Click += new System.EventHandler(this.video_Click);
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Transparent;
            this.button13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button13.Dock = System.Windows.Forms.DockStyle.Top;
            this.button13.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.button13.FlatAppearance.BorderSize = 0;
            this.button13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Salmon;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.ForeColor = System.Drawing.Color.Black;
            this.button13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button13.ImageIndex = 6;
            this.button13.ImageList = this.imageList1;
            this.button13.Location = new System.Drawing.Point(0, 210);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(157, 42);
            this.button13.TabIndex = 102;
            this.button13.Text = "Officials";
            this.button13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button13.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gold;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.ImageIndex = 8;
            this.button1.ImageList = this.imageList1;
            this.button1.Location = new System.Drawing.Point(0, 168);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(157, 42);
            this.button1.TabIndex = 96;
            this.button1.Text = "Competitors";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Transparent;
            this.button6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button6.Dock = System.Windows.Forms.DockStyle.Top;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.Black;
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.ImageIndex = 9;
            this.button6.ImageList = this.imageList1;
            this.button6.Location = new System.Drawing.Point(0, 126);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(157, 42);
            this.button6.TabIndex = 100;
            this.button6.Text = "Parameters";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SkyBlue;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.ImageIndex = 5;
            this.button2.ImageList = this.imageList1;
            this.button2.Location = new System.Drawing.Point(0, 84);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(157, 42);
            this.button2.TabIndex = 97;
            this.button2.Text = "Elements";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Transparent;
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.Dock = System.Windows.Forms.DockStyle.Top;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.ImageIndex = 0;
            this.button4.ImageList = this.imageList1;
            this.button4.Location = new System.Drawing.Point(0, 42);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(157, 42);
            this.button4.TabIndex = 96;
            this.button4.Text = "Database";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // buTech
            // 
            this.buTech.BackColor = System.Drawing.Color.Transparent;
            this.buTech.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buTech.Dock = System.Windows.Forms.DockStyle.Top;
            this.buTech.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.buTech.FlatAppearance.BorderSize = 0;
            this.buTech.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumPurple;
            this.buTech.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buTech.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buTech.ForeColor = System.Drawing.Color.Black;
            this.buTech.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buTech.ImageIndex = 7;
            this.buTech.ImageList = this.imageList1;
            this.buTech.Location = new System.Drawing.Point(0, 0);
            this.buTech.Name = "buTech";
            this.buTech.Size = new System.Drawing.Size(157, 42);
            this.buTech.TabIndex = 95;
            this.buTech.Text = "General";
            this.buTech.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buTech.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buTech.UseVisualStyleBackColor = false;
            this.buTech.Click += new System.EventHandler(this.back_Click);
            // 
            // panCompetitors
            // 
            this.panCompetitors.AutoScroll = true;
            this.panCompetitors.Controls.Add(this.groupBox2);
            this.panCompetitors.Controls.Add(this.region2);
            this.panCompetitors.Controls.Add(this.label46);
            this.panCompetitors.Controls.Add(this.label48);
            this.panCompetitors.Controls.Add(this.dg4);
            this.panCompetitors.Controls.Add(this.button18);
            this.panCompetitors.Controls.Add(this.button19);
            this.panCompetitors.Controls.Add(this.found2);
            this.panCompetitors.Controls.Add(this.pb2);
            this.panCompetitors.Controls.Add(this.delete);
            this.panCompetitors.Controls.Add(this.err2);
            this.panCompetitors.Controls.Add(this.button9);
            this.panCompetitors.Controls.Add(this.add);
            this.panCompetitors.Controls.Add(this.club);
            this.panCompetitors.Controls.Add(this.label33);
            this.panCompetitors.Controls.Add(this.country);
            this.panCompetitors.Controls.Add(this.label30);
            this.panCompetitors.Controls.Add(this.name);
            this.panCompetitors.Controls.Add(this.label27);
            this.panCompetitors.Controls.Add(this.label29);
            this.panCompetitors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panCompetitors.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panCompetitors.Location = new System.Drawing.Point(0, 0);
            this.panCompetitors.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panCompetitors.Name = "panCompetitors";
            this.panCompetitors.Size = new System.Drawing.Size(966, 597);
            this.panCompetitors.TabIndex = 4;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.radioButton8);
            this.groupBox2.Controls.Add(this.radioButton7);
            this.groupBox2.Controls.Add(this.radioButton6);
            this.groupBox2.Controls.Add(this.radioButton5);
            this.groupBox2.Controls.Add(this.radioButton4);
            this.groupBox2.Controls.Add(this.radioButton3);
            this.groupBox2.Controls.Add(this.radioButton2);
            this.groupBox2.Controls.Add(this.radioButton1);
            this.groupBox2.ForeColor = System.Drawing.Color.Blue;
            this.groupBox2.Location = new System.Drawing.Point(431, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(513, 68);
            this.groupBox2.TabIndex = 155;
            this.groupBox2.TabStop = false;
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.Location = new System.Drawing.Point(273, 40);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(126, 27);
            this.radioButton8.TabIndex = 7;
            this.radioButton8.Text = "Solo Dance F";
            this.radioButton8.UseVisualStyleBackColor = true;
            this.radioButton8.CheckedChanged += new System.EventHandler(this.radioButton8_CheckedChanged);
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(13, 40);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(140, 27);
            this.radioButton7.TabIndex = 6;
            this.radioButton7.Text = "Free Skating F";
            this.radioButton7.UseVisualStyleBackColor = true;
            this.radioButton7.CheckedChanged += new System.EventHandler(this.radioButton7_CheckedChanged);
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton6.ForeColor = System.Drawing.Color.Blue;
            this.radioButton6.Location = new System.Drawing.Point(414, 39);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(60, 27);
            this.radioButton6.TabIndex = 0;
            this.radioButton6.Text = "ALL";
            this.radioButton6.UseVisualStyleBackColor = true;
            this.radioButton6.CheckedChanged += new System.EventHandler(this.radioButton6_CheckedChanged);
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(414, 14);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(99, 27);
            this.radioButton5.TabIndex = 5;
            this.radioButton5.Text = "Precision";
            this.radioButton5.UseVisualStyleBackColor = true;
            this.radioButton5.CheckedChanged += new System.EventHandler(this.radioButton5_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(273, 14);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(129, 27);
            this.radioButton4.TabIndex = 4;
            this.radioButton4.Text = "Solo Dance M";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(145, 14);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(132, 27);
            this.radioButton3.TabIndex = 3;
            this.radioButton3.Text = "Couple Dance";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(145, 40);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(68, 27);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.Text = "Pairs";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.ForeColor = System.Drawing.Color.Red;
            this.radioButton1.Location = new System.Drawing.Point(13, 14);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(143, 27);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Free Skating M";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // region2
            // 
            this.region2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.region2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.region2.Location = new System.Drawing.Point(745, 132);
            this.region2.MaxLength = 40;
            this.region2.Name = "region2";
            this.region2.Size = new System.Drawing.Size(199, 26);
            this.region2.TabIndex = 3;
            // 
            // label46
            // 
            this.label46.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(680, 133);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(72, 26);
            this.label46.TabIndex = 153;
            this.label46.Tag = "";
            this.label46.Text = "Region";
            // 
            // label48
            // 
            this.label48.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.Color.Transparent;
            this.label48.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(31, 568);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(135, 20);
            this.label48.TabIndex = 152;
            this.label48.Tag = "";
            this.label48.Text = "* One click to edit";
            // 
            // dg4
            // 
            this.dg4.AllowUserToAddRows = false;
            this.dg4.AllowUserToDeleteRows = false;
            this.dg4.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Honeydew;
            this.dg4.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dg4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dg4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg4.BackgroundColor = System.Drawing.Color.White;
            this.dg4.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg4.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dg4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dg4ID,
            this.dg4Name,
            this.dg4Societa,
            this.dg4Region,
            this.dg4Country,
            this.dg4Spec});
            this.dg4.GridColor = System.Drawing.Color.Silver;
            this.dg4.Location = new System.Drawing.Point(29, 242);
            this.dg4.Name = "dg4";
            this.dg4.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dg4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dg4.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg4.Size = new System.Drawing.Size(915, 323);
            this.dg4.TabIndex = 9;
            this.dg4.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg4_CellEndEdit);
            this.dg4.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg4_CellEnter);
            // 
            // dg4ID
            // 
            this.dg4ID.HeaderText = "ID";
            this.dg4ID.Name = "dg4ID";
            this.dg4ID.Visible = false;
            // 
            // dg4Name
            // 
            this.dg4Name.HeaderText = "Name";
            this.dg4Name.Name = "dg4Name";
            // 
            // dg4Societa
            // 
            this.dg4Societa.HeaderText = "Club";
            this.dg4Societa.Name = "dg4Societa";
            // 
            // dg4Region
            // 
            this.dg4Region.HeaderText = "Region";
            this.dg4Region.Name = "dg4Region";
            // 
            // dg4Country
            // 
            this.dg4Country.HeaderText = "Nation";
            this.dg4Country.MaxInputLength = 3;
            this.dg4Country.Name = "dg4Country";
            // 
            // dg4Spec
            // 
            this.dg4Spec.HeaderText = "Discipline";
            this.dg4Spec.Name = "dg4Spec";
            this.dg4Spec.ReadOnly = true;
            // 
            // button18
            // 
            this.button18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button18.BackColor = System.Drawing.Color.White;
            this.button18.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button18.BackgroundImage")));
            this.button18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button18.FlatAppearance.BorderColor = System.Drawing.Color.Blue;
            this.button18.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button18.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.ForeColor = System.Drawing.Color.Black;
            this.button18.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button18.Location = new System.Drawing.Point(823, 213);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(121, 27);
            this.button18.TabIndex = 8;
            this.button18.Text = "Export to xml";
            this.button18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button19
            // 
            this.button19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button19.BackColor = System.Drawing.Color.White;
            this.button19.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button19.BackgroundImage")));
            this.button19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button19.FlatAppearance.BorderColor = System.Drawing.Color.Blue;
            this.button19.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button19.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button19.ForeColor = System.Drawing.Color.Black;
            this.button19.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button19.Location = new System.Drawing.Point(682, 213);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(139, 27);
            this.button19.TabIndex = 7;
            this.button19.Text = "Import from xml";
            this.button19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button19.UseVisualStyleBackColor = false;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // found2
            // 
            this.found2.AutoSize = true;
            this.found2.BackColor = System.Drawing.Color.Transparent;
            this.found2.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.found2.ForeColor = System.Drawing.Color.Blue;
            this.found2.Location = new System.Drawing.Point(349, 168);
            this.found2.Name = "found2";
            this.found2.Size = new System.Drawing.Size(79, 26);
            this.found2.TabIndex = 147;
            this.found2.Tag = "";
            this.found2.Text = "0 found";
            this.found2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pb2
            // 
            this.pb2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pb2.Image = global::RollartSystemTech.Properties.Resources.loading24x24;
            this.pb2.Location = new System.Drawing.Point(345, 164);
            this.pb2.Name = "pb2";
            this.pb2.Size = new System.Drawing.Size(29, 29);
            this.pb2.TabIndex = 146;
            this.pb2.TabStop = false;
            this.pb2.Visible = false;
            // 
            // delete
            // 
            this.delete.BackColor = System.Drawing.Color.White;
            this.delete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.delete.FlatAppearance.BorderColor = System.Drawing.Color.DarkGreen;
            this.delete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.delete.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.delete.ForeColor = System.Drawing.Color.DarkGreen;
            this.delete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.delete.Location = new System.Drawing.Point(151, 163);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(93, 30);
            this.delete.TabIndex = 5;
            this.delete.Text = "Delete";
            this.delete.UseVisualStyleBackColor = false;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // err2
            // 
            this.err2.BackColor = System.Drawing.Color.Transparent;
            this.err2.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.err2.ForeColor = System.Drawing.Color.Red;
            this.err2.Location = new System.Drawing.Point(83, 196);
            this.err2.Name = "err2";
            this.err2.Size = new System.Drawing.Size(592, 42);
            this.err2.TabIndex = 136;
            this.err2.Tag = "";
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.White;
            this.button9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button9.FlatAppearance.BorderColor = System.Drawing.Color.DarkGreen;
            this.button9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.Color.DarkGreen;
            this.button9.Image = global::RollartSystemTech.Properties.Resources.search;
            this.button9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button9.Location = new System.Drawing.Point(250, 163);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(93, 30);
            this.button9.TabIndex = 6;
            this.button9.Text = "Find";
            this.button9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // add
            // 
            this.add.BackColor = System.Drawing.Color.White;
            this.add.Cursor = System.Windows.Forms.Cursors.Hand;
            this.add.FlatAppearance.BorderColor = System.Drawing.Color.DarkGreen;
            this.add.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.add.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.add.ForeColor = System.Drawing.Color.DarkGreen;
            this.add.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.add.Location = new System.Drawing.Point(83, 163);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(62, 30);
            this.add.TabIndex = 4;
            this.add.Text = "Add";
            this.add.UseVisualStyleBackColor = false;
            this.add.Click += new System.EventHandler(this.button8_Click);
            // 
            // club
            // 
            this.club.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.club.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.club.Location = new System.Drawing.Point(83, 132);
            this.club.MaxLength = 50;
            this.club.Name = "club";
            this.club.Size = new System.Drawing.Size(567, 26);
            this.club.TabIndex = 2;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(37, 132);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(52, 26);
            this.label33.TabIndex = 132;
            this.label33.Tag = "";
            this.label33.Text = "Club";
            // 
            // country
            // 
            this.country.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.country.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.country.Location = new System.Drawing.Point(891, 101);
            this.country.MaxLength = 3;
            this.country.Name = "country";
            this.country.Size = new System.Drawing.Size(53, 26);
            this.country.TabIndex = 1;
            // 
            // label30
            // 
            this.label30.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(831, 102);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(72, 26);
            this.label30.TabIndex = 130;
            this.label30.Tag = "";
            this.label30.Text = "Nation";
            // 
            // name
            // 
            this.name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.name.Location = new System.Drawing.Point(83, 101);
            this.name.MaxLength = 50;
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(710, 26);
            this.name.TabIndex = 0;
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(4, 98);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(80, 28);
            this.label27.TabIndex = 124;
            this.label27.Tag = "";
            this.label27.Text = "Name*";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.DarkGreen;
            this.label29.Location = new System.Drawing.Point(16, 39);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(145, 29);
            this.label29.TabIndex = 123;
            this.label29.Tag = "";
            this.label29.Text = "Competitors";
            // 
            // panTech
            // 
            this.panTech.AutoScroll = true;
            this.panTech.Controls.Add(this.port);
            this.panTech.Controls.Add(this.cbSec);
            this.panTech.Controls.Add(this.label50);
            this.panTech.Controls.Add(this.label49);
            this.panTech.Controls.Add(this.ts1);
            this.panTech.Controls.Add(this.panel3);
            this.panTech.Controls.Add(this.button14);
            this.panTech.Controls.Add(this.label6);
            this.panTech.Controls.Add(this.label7);
            this.panTech.Controls.Add(this.label1);
            this.panTech.Controls.Add(this.label5);
            this.panTech.Controls.Add(this.button3);
            this.panTech.Controls.Add(this.label3);
            this.panTech.Controls.Add(this.label2);
            this.panTech.Controls.Add(this.ipT);
            this.panTech.Controls.Add(this.root);
            this.panTech.Controls.Add(this.nJudges);
            this.panTech.Controls.Add(this.label9);
            this.panTech.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panTech.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panTech.Location = new System.Drawing.Point(0, 0);
            this.panTech.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panTech.Name = "panTech";
            this.panTech.Size = new System.Drawing.Size(966, 597);
            this.panTech.TabIndex = 0;
            // 
            // port
            // 
            this.port.Location = new System.Drawing.Point(173, 89);
            this.port.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.port.Name = "port";
            this.port.Size = new System.Drawing.Size(79, 31);
            this.port.TabIndex = 168;
            // 
            // cbSec
            // 
            this.cbSec.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSec.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSec.FormattingEnabled = true;
            this.cbSec.Items.AddRange(new object[] {
            "2000",
            "5000",
            "10000"});
            this.cbSec.Location = new System.Drawing.Point(390, 91);
            this.cbSec.Name = "cbSec";
            this.cbSec.Size = new System.Drawing.Size(87, 31);
            this.cbSec.TabIndex = 167;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(263, 93);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(158, 26);
            this.label50.TabIndex = 166;
            this.label50.Tag = "";
            this.label50.Text = "Ping time in ms:";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(37, 92);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(170, 26);
            this.label49.TabIndex = 164;
            this.label49.Tag = "";
            this.label49.Text = "Judge Panel Port:";
            // 
            // ts1
            // 
            this.ts1.BackColor = System.Drawing.Color.Transparent;
            this.ts1.Checked = true;
            this.ts1.Location = new System.Drawing.Point(273, 371);
            this.ts1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ts1.Name = "ts1";
            this.ts1.OffFont = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ts1.OffForeColor = System.Drawing.Color.Yellow;
            this.ts1.OffText = "OFF";
            this.ts1.OnFont = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ts1.OnForeColor = System.Drawing.Color.Yellow;
            this.ts1.OnText = "ON";
            this.ts1.Size = new System.Drawing.Size(88, 30);
            this.ts1.Style = JCS.ToggleSwitch.ToggleSwitchStyle.Fancy;
            this.ts1.TabIndex = 162;
            this.ts1.CheckedChanged += new JCS.ToggleSwitch.CheckedChangedDelegate(this.checkCombo2_CheckedChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Location = new System.Drawing.Point(29, 406);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(764, 187);
            this.panel3.TabIndex = 138;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label43);
            this.groupBox1.Controls.Add(this.label42);
            this.groupBox1.Controls.Add(this.rb12);
            this.groupBox1.Controls.Add(this.pb1);
            this.groupBox1.Controls.Add(this.cb4);
            this.groupBox1.Controls.Add(this.button15);
            this.groupBox1.Controls.Add(this.rb11);
            this.groupBox1.Controls.Add(this.label44);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(764, 187);
            this.groupBox1.TabIndex = 138;
            this.groupBox1.TabStop = false;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(385, 33);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(231, 26);
            this.label43.TabIndex = 134;
            this.label43.Tag = "";
            this.label43.Text = "Choose image to display";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label42.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(683, 33);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(84, 26);
            this.label42.TabIndex = 130;
            this.label42.Tag = "";
            this.label42.Text = "Preview";
            this.label42.Click += new System.EventHandler(this.label42_Click);
            // 
            // rb12
            // 
            this.rb12.BackgroundImage = global::RollartSystemTech.Properties.Resources.screen2;
            this.rb12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.rb12.Checked = true;
            this.rb12.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb12.ForeColor = System.Drawing.Color.Yellow;
            this.rb12.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rb12.Location = new System.Drawing.Point(192, 22);
            this.rb12.Name = "rb12";
            this.rb12.Size = new System.Drawing.Size(167, 115);
            this.rb12.TabIndex = 132;
            this.rb12.TabStop = true;
            this.rb12.Text = "Screen 2";
            this.rb12.UseVisualStyleBackColor = true;
            this.rb12.CheckedChanged += new System.EventHandler(this.rb12_CheckedChanged);
            // 
            // pb1
            // 
            this.pb1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb1.Location = new System.Drawing.Point(503, 58);
            this.pb1.Name = "pb1";
            this.pb1.Size = new System.Drawing.Size(62, 62);
            this.pb1.TabIndex = 136;
            this.pb1.TabStop = false;
            // 
            // cb4
            // 
            this.cb4.AutoSize = true;
            this.cb4.Checked = true;
            this.cb4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb4.Location = new System.Drawing.Point(12, 156);
            this.cb4.Name = "cb4";
            this.cb4.Size = new System.Drawing.Size(202, 30);
            this.cb4.TabIndex = 133;
            this.cb4.Text = "Use Display Switch";
            this.cb4.UseVisualStyleBackColor = true;
            this.cb4.CheckedChanged += new System.EventHandler(this.cb4_CheckedChanged);
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.Transparent;
            this.button15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button15.FlatAppearance.BorderColor = System.Drawing.Color.MediumPurple;
            this.button15.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Lavender;
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button15.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.ForeColor = System.Drawing.Color.MediumPurple;
            this.button15.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button15.Location = new System.Drawing.Point(389, 90);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(93, 30);
            this.button15.TabIndex = 135;
            this.button15.Text = "Browse >>";
            this.button15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // rb11
            // 
            this.rb11.BackgroundImage = global::RollartSystemTech.Properties.Resources.screen1;
            this.rb11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.rb11.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb11.ForeColor = System.Drawing.Color.Yellow;
            this.rb11.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rb11.Location = new System.Drawing.Point(12, 22);
            this.rb11.Name = "rb11";
            this.rb11.Size = new System.Drawing.Size(167, 115);
            this.rb11.TabIndex = 131;
            this.rb11.TabStop = true;
            this.rb11.Text = "Screen 1";
            this.rb11.UseVisualStyleBackColor = true;
            this.rb11.CheckedChanged += new System.EventHandler(this.rb11_CheckedChanged);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(500, 124);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(78, 18);
            this.label44.TabIndex = 137;
            this.label44.Tag = "";
            this.label44.Text = "H313xW313";
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.Transparent;
            this.button14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button14.FlatAppearance.BorderColor = System.Drawing.Color.MediumPurple;
            this.button14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Lavender;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.ForeColor = System.Drawing.Color.MediumPurple;
            this.button14.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button14.Location = new System.Drawing.Point(700, 269);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(93, 30);
            this.button14.TabIndex = 11;
            this.button14.Text = "Browse >>";
            this.button14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(37, 377);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(284, 26);
            this.label6.TabIndex = 129;
            this.label6.Tag = "";
            this.label6.Text = "Choose if enable static screen";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.MediumPurple;
            this.label7.Location = new System.Drawing.Point(25, 341);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(156, 29);
            this.label7.TabIndex = 128;
            this.label7.Tag = "";
            this.label7.Text = "Static Screen";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(33, 273);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 26);
            this.label1.TabIndex = 127;
            this.label1.Tag = "";
            this.label1.Text = "Choose events folder:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.MediumPurple;
            this.label5.Location = new System.Drawing.Point(24, 242);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(159, 29);
            this.label5.TabIndex = 126;
            this.label5.Tag = "";
            this.label5.Text = "Events Folder";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Transparent;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.Enabled = false;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.MediumPurple;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.ImageIndex = 27;
            this.button3.Location = new System.Drawing.Point(16, 166);
            this.button3.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(115, 44);
            this.button3.TabIndex = 125;
            this.button3.Text = "Tech Panel:";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.MediumPurple;
            this.label3.Location = new System.Drawing.Point(25, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 29);
            this.label3.TabIndex = 123;
            this.label3.Tag = "";
            this.label3.Text = "IP Address";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(34, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(366, 26);
            this.label2.TabIndex = 122;
            this.label2.Tag = "";
            this.label2.Text = "Select the number of judges (1 up to 9)";
            // 
            // ipT
            // 
            this.ipT.AllowInternalTab = false;
            this.ipT.AutoHeight = true;
            this.ipT.BackColor = System.Drawing.SystemColors.Window;
            this.ipT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ipT.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipT.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ipT.Location = new System.Drawing.Point(135, 175);
            this.ipT.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ipT.MinimumSize = new System.Drawing.Size(154, 29);
            this.ipT.Name = "ipT";
            this.ipT.ReadOnly = true;
            this.ipT.Size = new System.Drawing.Size(154, 29);
            this.ipT.TabIndex = 1;
            this.ipT.Text = "...";
            this.ipT.TextChanged += new System.EventHandler(this.ipT_TextChanged);
            // 
            // root
            // 
            this.root.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.root.Location = new System.Drawing.Point(205, 273);
            this.root.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.root.Name = "root";
            this.root.Size = new System.Drawing.Size(479, 26);
            this.root.TabIndex = 10;
            this.root.TextChanged += new System.EventHandler(this.root_TextChanged);
            // 
            // nJudges
            // 
            this.nJudges.BackColor = System.Drawing.Color.White;
            this.nJudges.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nJudges.Location = new System.Drawing.Point(329, 44);
            this.nJudges.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.nJudges.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.nJudges.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nJudges.Name = "nJudges";
            this.nJudges.Size = new System.Drawing.Size(70, 31);
            this.nJudges.TabIndex = 0;
            this.nJudges.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nJudges.ValueChanged += new System.EventHandler(this.nJudges_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.MediumPurple;
            this.label9.Location = new System.Drawing.Point(25, 15);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(204, 29);
            this.label9.TabIndex = 101;
            this.label9.Tag = "";
            this.label9.Text = "Number of Judges";
            // 
            // panVideo
            // 
            this.panVideo.Controls.Add(this.portVideo);
            this.panVideo.Controls.Add(this.pingVideo);
            this.panVideo.Controls.Add(this.label54);
            this.panVideo.Controls.Add(this.label55);
            this.panVideo.Controls.Add(this.label56);
            this.panVideo.Controls.Add(this.ipVideo);
            this.panVideo.Controls.Add(this.label58);
            this.panVideo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panVideo.Location = new System.Drawing.Point(0, 0);
            this.panVideo.Name = "panVideo";
            this.panVideo.Size = new System.Drawing.Size(966, 597);
            this.panVideo.TabIndex = 157;
            // 
            // portVideo
            // 
            this.portVideo.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.portVideo.Location = new System.Drawing.Point(363, 77);
            this.portVideo.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.portVideo.Name = "portVideo";
            this.portVideo.Size = new System.Drawing.Size(89, 30);
            this.portVideo.TabIndex = 178;
            // 
            // pingVideo
            // 
            this.pingVideo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pingVideo.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pingVideo.FormattingEnabled = true;
            this.pingVideo.Items.AddRange(new object[] {
            "2000",
            "5000",
            "10000"});
            this.pingVideo.Location = new System.Drawing.Point(144, 113);
            this.pingVideo.Name = "pingVideo";
            this.pingVideo.Size = new System.Drawing.Size(87, 31);
            this.pingVideo.TabIndex = 177;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(17, 115);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(158, 26);
            this.label54.TabIndex = 176;
            this.label54.Tag = "";
            this.label54.Text = "Ping time in ms:";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(316, 78);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(56, 26);
            this.label55.TabIndex = 175;
            this.label55.Tag = "";
            this.label55.Text = "Port:";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.BackColor = System.Drawing.Color.Transparent;
            this.label56.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(53, 78);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(110, 26);
            this.label56.TabIndex = 173;
            this.label56.Tag = "";
            this.label56.Text = "IP Address:";
            // 
            // ipVideo
            // 
            this.ipVideo.AllowInternalTab = false;
            this.ipVideo.AutoHeight = true;
            this.ipVideo.BackColor = System.Drawing.SystemColors.Window;
            this.ipVideo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ipVideo.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipVideo.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ipVideo.Location = new System.Drawing.Point(144, 77);
            this.ipVideo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ipVideo.MinimumSize = new System.Drawing.Size(154, 29);
            this.ipVideo.Name = "ipVideo";
            this.ipVideo.ReadOnly = false;
            this.ipVideo.Size = new System.Drawing.Size(154, 29);
            this.ipVideo.TabIndex = 170;
            this.ipVideo.Text = "...";
            // 
            // label58
            // 
            this.label58.BackColor = System.Drawing.Color.Transparent;
            this.label58.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Red;
            this.label58.Image = global::RollartSystemTech.Properties.Resources.videoscreen;
            this.label58.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label58.Location = new System.Drawing.Point(30, 20);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(180, 40);
            this.label58.TabIndex = 171;
            this.label58.Tag = "";
            this.label58.Text = "VideoScreen ";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panJudges
            // 
            this.panJudges.AutoScroll = true;
            this.panJudges.Controls.Add(this.label47);
            this.panJudges.Controls.Add(this.button17);
            this.panJudges.Controls.Add(this.button16);
            this.panJudges.Controls.Add(this.found);
            this.panJudges.Controls.Add(this.pb);
            this.panJudges.Controls.Add(this.button10);
            this.panJudges.Controls.Add(this.err3);
            this.panJudges.Controls.Add(this.button11);
            this.panJudges.Controls.Add(this.button12);
            this.panJudges.Controls.Add(this.region);
            this.panJudges.Controls.Add(this.label35);
            this.panJudges.Controls.Add(this.country2);
            this.panJudges.Controls.Add(this.label36);
            this.panJudges.Controls.Add(this.name2);
            this.panJudges.Controls.Add(this.dg5);
            this.panJudges.Controls.Add(this.label37);
            this.panJudges.Controls.Add(this.label38);
            this.panJudges.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panJudges.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panJudges.Location = new System.Drawing.Point(0, 0);
            this.panJudges.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panJudges.Name = "panJudges";
            this.panJudges.Size = new System.Drawing.Size(966, 597);
            this.panJudges.TabIndex = 5;
            // 
            // label47
            // 
            this.label47.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label47.AutoSize = true;
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(33, 568);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(135, 20);
            this.label47.TabIndex = 144;
            this.label47.Tag = "";
            this.label47.Text = "* One click to edit";
            // 
            // button17
            // 
            this.button17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button17.BackColor = System.Drawing.Color.White;
            this.button17.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button17.BackgroundImage")));
            this.button17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button17.FlatAppearance.BorderColor = System.Drawing.Color.Salmon;
            this.button17.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button17.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.ForeColor = System.Drawing.Color.Black;
            this.button17.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button17.Location = new System.Drawing.Point(826, 134);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(121, 27);
            this.button17.TabIndex = 7;
            this.button17.Text = "Export to xml";
            this.button17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button16
            // 
            this.button16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button16.BackColor = System.Drawing.Color.White;
            this.button16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button16.BackgroundImage")));
            this.button16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button16.FlatAppearance.BorderColor = System.Drawing.Color.Salmon;
            this.button16.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button16.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.ForeColor = System.Drawing.Color.Black;
            this.button16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button16.Location = new System.Drawing.Point(684, 134);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(139, 27);
            this.button16.TabIndex = 6;
            this.button16.Text = "Import from xml";
            this.button16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // found
            // 
            this.found.AutoSize = true;
            this.found.BackColor = System.Drawing.Color.Transparent;
            this.found.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.found.ForeColor = System.Drawing.Color.Salmon;
            this.found.Location = new System.Drawing.Point(326, 89);
            this.found.Name = "found";
            this.found.Size = new System.Drawing.Size(79, 26);
            this.found.TabIndex = 143;
            this.found.Tag = "";
            this.found.Text = "0 found";
            this.found.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.found.Visible = false;
            // 
            // pb
            // 
            this.pb.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pb.Image = global::RollartSystemTech.Properties.Resources.loading24x24;
            this.pb.Location = new System.Drawing.Point(326, 87);
            this.pb.Name = "pb";
            this.pb.Size = new System.Drawing.Size(29, 29);
            this.pb.TabIndex = 142;
            this.pb.TabStop = false;
            this.pb.Visible = false;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.White;
            this.button10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button10.FlatAppearance.BorderColor = System.Drawing.Color.Salmon;
            this.button10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.ForeColor = System.Drawing.Color.Salmon;
            this.button10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button10.Location = new System.Drawing.Point(148, 85);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(76, 30);
            this.button10.TabIndex = 4;
            this.button10.Text = "Delete";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // err3
            // 
            this.err3.BackColor = System.Drawing.Color.Transparent;
            this.err3.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.err3.ForeColor = System.Drawing.Color.Red;
            this.err3.Location = new System.Drawing.Point(83, 117);
            this.err3.Name = "err3";
            this.err3.Size = new System.Drawing.Size(592, 44);
            this.err3.TabIndex = 136;
            this.err3.Tag = "";
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.White;
            this.button11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button11.FlatAppearance.BorderColor = System.Drawing.Color.Salmon;
            this.button11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = System.Drawing.Color.Salmon;
            this.button11.Image = global::RollartSystemTech.Properties.Resources.search;
            this.button11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button11.Location = new System.Drawing.Point(227, 85);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(93, 30);
            this.button11.TabIndex = 5;
            this.button11.Text = "Find";
            this.button11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.White;
            this.button12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button12.FlatAppearance.BorderColor = System.Drawing.Color.Salmon;
            this.button12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = System.Drawing.Color.Salmon;
            this.button12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button12.Location = new System.Drawing.Point(83, 85);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(62, 30);
            this.button12.TabIndex = 3;
            this.button12.Text = "Add";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // region
            // 
            this.region.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.region.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.region.Location = new System.Drawing.Point(543, 51);
            this.region.Name = "region";
            this.region.Size = new System.Drawing.Size(271, 26);
            this.region.TabIndex = 1;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(482, 51);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(72, 26);
            this.label35.TabIndex = 132;
            this.label35.Tag = "";
            this.label35.Text = "Region";
            // 
            // country2
            // 
            this.country2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.country2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.country2.Location = new System.Drawing.Point(894, 50);
            this.country2.MaxLength = 3;
            this.country2.Name = "country2";
            this.country2.Size = new System.Drawing.Size(53, 26);
            this.country2.TabIndex = 2;
            // 
            // label36
            // 
            this.label36.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(835, 50);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(72, 26);
            this.label36.TabIndex = 130;
            this.label36.Tag = "";
            this.label36.Text = "Nation";
            // 
            // name2
            // 
            this.name2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.name2.Location = new System.Drawing.Point(83, 51);
            this.name2.Name = "name2";
            this.name2.Size = new System.Drawing.Size(384, 26);
            this.name2.TabIndex = 0;
            // 
            // dg5
            // 
            this.dg5.AllowUserToAddRows = false;
            this.dg5.AllowUserToDeleteRows = false;
            this.dg5.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.MistyRose;
            this.dg5.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dg5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dg5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg5.BackgroundColor = System.Drawing.Color.White;
            this.dg5.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg5.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dg5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.dg5Name,
            this.dg5Region,
            this.dg5Country});
            this.dg5.GridColor = System.Drawing.Color.Silver;
            this.dg5.Location = new System.Drawing.Point(30, 163);
            this.dg5.Name = "dg5";
            this.dg5.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dg5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dg5.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg5.Size = new System.Drawing.Size(917, 402);
            this.dg5.TabIndex = 128;
            this.dg5.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg5_CellEndEdit);
            this.dg5.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg5_CellEnter);
            this.dg5.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dg5_ColumnHeaderMouseClick);
            // 
            // id
            // 
            this.id.HeaderText = "ID";
            this.id.Name = "id";
            this.id.Visible = false;
            // 
            // dg5Name
            // 
            this.dg5Name.HeaderText = "Name";
            this.dg5Name.Name = "dg5Name";
            // 
            // dg5Region
            // 
            this.dg5Region.HeaderText = "Region";
            this.dg5Region.Name = "dg5Region";
            // 
            // dg5Country
            // 
            this.dg5Country.HeaderText = "Nation";
            this.dg5Country.Name = "dg5Country";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(25, 50);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(64, 26);
            this.label37.TabIndex = 124;
            this.label37.Tag = "";
            this.label37.Text = "Name";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Salmon;
            this.label38.Location = new System.Drawing.Point(16, 13);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(103, 29);
            this.label38.TabIndex = 123;
            this.label38.Tag = "";
            this.label38.Text = "Officials";
            // 
            // panElements
            // 
            this.panElements.AutoScroll = true;
            this.panElements.Controls.Add(this.panEl);
            this.panElements.Controls.Add(this.label4);
            this.panElements.Controls.Add(this.cb3);
            this.panElements.Controls.Add(this.label31);
            this.panElements.Controls.Add(this.label32);
            this.panElements.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panElements.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panElements.Location = new System.Drawing.Point(0, 0);
            this.panElements.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panElements.Name = "panElements";
            this.panElements.Size = new System.Drawing.Size(966, 597);
            this.panElements.TabIndex = 2;
            // 
            // panEl
            // 
            this.panEl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panEl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panEl.Controls.Add(this.dg3);
            this.panEl.Controls.Add(this.label45);
            this.panEl.Location = new System.Drawing.Point(21, 106);
            this.panEl.Name = "panEl";
            this.panEl.Size = new System.Drawing.Size(924, 469);
            this.panEl.TabIndex = 130;
            this.panEl.Visible = false;
            // 
            // dg3
            // 
            this.dg3.AllowUserToAddRows = false;
            this.dg3.AllowUserToDeleteRows = false;
            this.dg3.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.LightCyan;
            this.dg3.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dg3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dg3.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dg3.BackgroundColor = System.Drawing.Color.White;
            this.dg3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dg3.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dg3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg3.GridColor = System.Drawing.Color.Silver;
            this.dg3.Location = new System.Drawing.Point(28, 0);
            this.dg3.MultiSelect = false;
            this.dg3.Name = "dg3";
            this.dg3.ReadOnly = true;
            this.dg3.RowHeadersVisible = false;
            this.dg3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dg3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg3.Size = new System.Drawing.Size(894, 467);
            this.dg3.TabIndex = 130;
            // 
            // label45
            // 
            this.label45.BackColor = System.Drawing.Color.LemonChiffon;
            this.label45.Dock = System.Windows.Forms.DockStyle.Left;
            this.label45.Font = new System.Drawing.Font("Trebuchet MS", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(0, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(28, 467);
            this.label45.TabIndex = 129;
            this.label45.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label4.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label4.Location = new System.Drawing.Point(17, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 23);
            this.label4.TabIndex = 129;
            this.label4.Tag = "";
            this.label4.Text = "Print preview";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label4.Visible = false;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // cb3
            // 
            this.cb3.BackColor = System.Drawing.Color.White;
            this.cb3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb3.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold);
            this.cb3.FormattingEnabled = true;
            this.cb3.Location = new System.Drawing.Point(185, 48);
            this.cb3.Name = "cb3";
            this.cb3.Size = new System.Drawing.Size(191, 31);
            this.cb3.TabIndex = 125;
            this.cb3.SelectedIndexChanged += new System.EventHandler(this.cb3_SelectedIndexChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(25, 50);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(202, 26);
            this.label31.TabIndex = 124;
            this.label31.Tag = "";
            this.label31.Text = "Select element type:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.label32.Location = new System.Drawing.Point(16, 21);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(112, 29);
            this.label32.TabIndex = 123;
            this.label32.Tag = "";
            this.label32.Text = "Elements";
            // 
            // panParameters
            // 
            this.panParameters.AutoScroll = true;
            this.panParameters.Controls.Add(this.tabControl1);
            this.panParameters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panParameters.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panParameters.Location = new System.Drawing.Point(0, 0);
            this.panParameters.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panParameters.Name = "panParameters";
            this.panParameters.Size = new System.Drawing.Size(966, 597);
            this.panParameters.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(966, 597);
            this.tabControl1.TabIndex = 148;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.cbCategory);
            this.tabPage1.Controls.Add(this.dg1);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.ForeColor = System.Drawing.Color.Black;
            this.tabPage1.Location = new System.Drawing.Point(4, 38);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(958, 555);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Free Skating";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(18, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(147, 29);
            this.label10.TabIndex = 123;
            this.label10.Tag = "";
            this.label10.Text = "Free Skating";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(27, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(419, 26);
            this.label8.TabIndex = 124;
            this.label8.Tag = "";
            this.label8.Text = "Select category to view program parameters";
            // 
            // cbCategory
            // 
            this.cbCategory.BackColor = System.Drawing.Color.White;
            this.cbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCategory.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold);
            this.cbCategory.FormattingEnabled = true;
            this.cbCategory.Location = new System.Drawing.Point(360, 49);
            this.cbCategory.Name = "cbCategory";
            this.cbCategory.Size = new System.Drawing.Size(173, 31);
            this.cbCategory.TabIndex = 125;
            this.cbCategory.SelectedIndexChanged += new System.EventHandler(this.cbCategory_SelectedIndexChanged);
            // 
            // dg1
            // 
            this.dg1.AllowUserToAddRows = false;
            this.dg1.AllowUserToDeleteRows = false;
            this.dg1.AllowUserToResizeColumns = false;
            this.dg1.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            this.dg1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dg1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dg1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dg1.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dg1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dg1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg1.GridColor = System.Drawing.Color.Silver;
            this.dg1.Location = new System.Drawing.Point(208, 92);
            this.dg1.MultiSelect = false;
            this.dg1.Name = "dg1";
            this.dg1.ReadOnly = true;
            this.dg1.RowHeadersVisible = false;
            this.dg1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dg1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dg1.Size = new System.Drawing.Size(729, 169);
            this.dg1.TabIndex = 128;
            this.dg1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg1_CellEndEdit);
            this.dg1.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg1_CellEnter);
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Blue;
            this.label13.Location = new System.Drawing.Point(21, 136);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(185, 21);
            this.label13.TabIndex = 129;
            this.label13.Tag = "";
            this.label13.Text = "Short Program Men";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.DarkCyan;
            this.label14.Location = new System.Drawing.Point(24, 182);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(182, 21);
            this.label14.TabIndex = 130;
            this.label14.Tag = "";
            this.label14.Text = "Long Program Men";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Blue;
            this.label16.Location = new System.Drawing.Point(8, 159);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(198, 21);
            this.label16.TabIndex = 131;
            this.label16.Tag = "";
            this.label16.Text = "Short Program Ladies";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.DarkCyan;
            this.label15.Location = new System.Drawing.Point(11, 205);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(195, 21);
            this.label15.TabIndex = 132;
            this.label15.Tag = "";
            this.label15.Text = "Long Program Ladies";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.White;
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.dg2);
            this.tabPage2.Controls.Add(this.cb2);
            this.tabPage2.Location = new System.Drawing.Point(4, 38);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(958, 555);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Pairs";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label18.Location = new System.Drawing.Point(19, 20);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(64, 29);
            this.label18.TabIndex = 133;
            this.label18.Tag = "";
            this.label18.Text = "Pairs";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(27, 49);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(419, 26);
            this.label17.TabIndex = 134;
            this.label17.Tag = "";
            this.label17.Text = "Select category to view program parameters";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label20.Location = new System.Drawing.Point(92, 134);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(141, 26);
            this.label20.TabIndex = 136;
            this.label20.Tag = "";
            this.label20.Text = "Short Program";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label19.Location = new System.Drawing.Point(95, 158);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(135, 26);
            this.label19.TabIndex = 137;
            this.label19.Tag = "";
            this.label19.Text = "Long Program";
            // 
            // dg2
            // 
            this.dg2.AllowUserToAddRows = false;
            this.dg2.AllowUserToDeleteRows = false;
            this.dg2.AllowUserToResizeColumns = false;
            this.dg2.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            this.dg2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dg2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dg2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dg2.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dg2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dg2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg2.GridColor = System.Drawing.Color.Silver;
            this.dg2.Location = new System.Drawing.Point(208, 92);
            this.dg2.MultiSelect = false;
            this.dg2.Name = "dg2";
            this.dg2.ReadOnly = true;
            this.dg2.RowHeadersVisible = false;
            this.dg2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dg2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dg2.Size = new System.Drawing.Size(729, 127);
            this.dg2.TabIndex = 138;
            this.dg2.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg2_CellEndEdit);
            this.dg2.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg2_CellEnter);
            // 
            // cb2
            // 
            this.cb2.BackColor = System.Drawing.Color.White;
            this.cb2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb2.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold);
            this.cb2.FormattingEnabled = true;
            this.cb2.Location = new System.Drawing.Point(360, 49);
            this.cb2.Name = "cb2";
            this.cb2.Size = new System.Drawing.Size(173, 31);
            this.cb2.TabIndex = 135;
            this.cb2.SelectedIndexChanged += new System.EventHandler(this.cb2_SelectedIndexChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.White;
            this.tabPage3.Controls.Add(this.label41);
            this.tabPage3.Controls.Add(this.rb3);
            this.tabPage3.Controls.Add(this.label40);
            this.tabPage3.Controls.Add(this.rb2);
            this.tabPage3.Controls.Add(this.cbCategoryDance);
            this.tabPage3.Controls.Add(this.rb1);
            this.tabPage3.Controls.Add(this.label39);
            this.tabPage3.Controls.Add(this.dgDance);
            this.tabPage3.Controls.Add(this.label34);
            this.tabPage3.Location = new System.Drawing.Point(4, 38);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(958, 555);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Dance";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label41.Location = new System.Drawing.Point(18, 20);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(290, 29);
            this.label41.TabIndex = 139;
            this.label41.Tag = "";
            this.label41.Text = "Couple Dance/Solo Dance";
            // 
            // rb3
            // 
            this.rb3.AutoSize = true;
            this.rb3.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.rb3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rb3.Location = new System.Drawing.Point(812, 50);
            this.rb3.Name = "rb3";
            this.rb3.Size = new System.Drawing.Size(152, 30);
            this.rb3.TabIndex = 147;
            this.rb3.Tag = "5";
            this.rb3.Text = "Solo Dance M";
            this.rb3.UseVisualStyleBackColor = true;
            this.rb3.CheckedChanged += new System.EventHandler(this.rb3_CheckedChanged);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(27, 49);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(419, 26);
            this.label40.TabIndex = 140;
            this.label40.Tag = "";
            this.label40.Text = "Select category to view program parameters";
            // 
            // rb2
            // 
            this.rb2.AutoSize = true;
            this.rb2.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.rb2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rb2.Location = new System.Drawing.Point(672, 50);
            this.rb2.Name = "rb2";
            this.rb2.Size = new System.Drawing.Size(148, 30);
            this.rb2.TabIndex = 146;
            this.rb2.Tag = "6";
            this.rb2.Text = "Solo Dance F";
            this.rb2.UseVisualStyleBackColor = true;
            this.rb2.CheckedChanged += new System.EventHandler(this.rb2_CheckedChanged);
            // 
            // cbCategoryDance
            // 
            this.cbCategoryDance.BackColor = System.Drawing.Color.White;
            this.cbCategoryDance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCategoryDance.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold);
            this.cbCategoryDance.FormattingEnabled = true;
            this.cbCategoryDance.Location = new System.Drawing.Point(360, 49);
            this.cbCategoryDance.Name = "cbCategoryDance";
            this.cbCategoryDance.Size = new System.Drawing.Size(173, 31);
            this.cbCategoryDance.TabIndex = 141;
            this.cbCategoryDance.SelectedIndexChanged += new System.EventHandler(this.cbCategoryDance_SelectedIndexChanged);
            // 
            // rb1
            // 
            this.rb1.AutoSize = true;
            this.rb1.Checked = true;
            this.rb1.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.rb1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rb1.Location = new System.Drawing.Point(572, 50);
            this.rb1.Name = "rb1";
            this.rb1.Size = new System.Drawing.Size(87, 30);
            this.rb1.TabIndex = 145;
            this.rb1.TabStop = true;
            this.rb1.Tag = "4";
            this.rb1.Text = "Dance";
            this.rb1.UseVisualStyleBackColor = true;
            this.rb1.CheckedChanged += new System.EventHandler(this.rb1_CheckedChanged);
            // 
            // label39
            // 
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label39.Location = new System.Drawing.Point(17, 135);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(188, 21);
            this.label39.TabIndex = 142;
            this.label39.Tag = "";
            this.label39.Text = "Compulsory Dances";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dgDance
            // 
            this.dgDance.AllowUserToAddRows = false;
            this.dgDance.AllowUserToDeleteRows = false;
            this.dgDance.AllowUserToResizeRows = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            this.dgDance.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dgDance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgDance.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dgDance.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgDance.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDance.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgDance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDance.GridColor = System.Drawing.Color.Silver;
            this.dgDance.Location = new System.Drawing.Point(208, 92);
            this.dgDance.MultiSelect = false;
            this.dgDance.Name = "dgDance";
            this.dgDance.ReadOnly = true;
            this.dgDance.RowHeadersVisible = false;
            this.dgDance.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgDance.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgDance.Size = new System.Drawing.Size(729, 125);
            this.dgDance.TabIndex = 144;
            this.dgDance.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgDance_CellEndEdit);
            this.dgDance.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgDance_CellEnter);
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label34.Location = new System.Drawing.Point(74, 157);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(130, 21);
            this.label34.TabIndex = 143;
            this.label34.Tag = "";
            this.label34.Text = "Free Dance";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.White;
            this.tabPage4.Controls.Add(this.label11);
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Controls.Add(this.dgPrecision);
            this.tabPage4.Controls.Add(this.cbPrecision);
            this.tabPage4.Location = new System.Drawing.Point(4, 38);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(958, 555);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Precision";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Green;
            this.label11.Location = new System.Drawing.Point(19, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 29);
            this.label11.TabIndex = 139;
            this.label11.Tag = "";
            this.label11.Text = "Precision";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(27, 49);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(419, 26);
            this.label12.TabIndex = 140;
            this.label12.Tag = "";
            this.label12.Text = "Select category to view program parameters";
            // 
            // dgPrecision
            // 
            this.dgPrecision.AllowUserToAddRows = false;
            this.dgPrecision.AllowUserToDeleteRows = false;
            this.dgPrecision.AllowUserToResizeColumns = false;
            this.dgPrecision.AllowUserToResizeRows = false;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            this.dgPrecision.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.dgPrecision.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgPrecision.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dgPrecision.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgPrecision.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPrecision.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgPrecision.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPrecision.GridColor = System.Drawing.Color.Silver;
            this.dgPrecision.Location = new System.Drawing.Point(33, 92);
            this.dgPrecision.MultiSelect = false;
            this.dgPrecision.Name = "dgPrecision";
            this.dgPrecision.ReadOnly = true;
            this.dgPrecision.RowHeadersVisible = false;
            this.dgPrecision.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgPrecision.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgPrecision.Size = new System.Drawing.Size(904, 127);
            this.dgPrecision.TabIndex = 144;
            // 
            // cbPrecision
            // 
            this.cbPrecision.BackColor = System.Drawing.Color.White;
            this.cbPrecision.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPrecision.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold);
            this.cbPrecision.FormattingEnabled = true;
            this.cbPrecision.Location = new System.Drawing.Point(360, 49);
            this.cbPrecision.Name = "cbPrecision";
            this.cbPrecision.Size = new System.Drawing.Size(173, 31);
            this.cbPrecision.TabIndex = 141;
            this.cbPrecision.SelectedIndexChanged += new System.EventHandler(this.cbPrecision_SelectedIndexChanged);
            // 
            // panDB
            // 
            this.panDB.AutoScroll = true;
            this.panDB.Controls.Add(this.label53);
            this.panDB.Controls.Add(this.label52);
            this.panDB.Controls.Add(this.label51);
            this.panDB.Controls.Add(this.backupfolder);
            this.panDB.Controls.Add(this.lv3);
            this.panDB.Controls.Add(this.label26);
            this.panDB.Controls.Add(this.lv2);
            this.panDB.Controls.Add(this.label22);
            this.panDB.Controls.Add(this.lv1);
            this.panDB.Controls.Add(this.label21);
            this.panDB.Controls.Add(this.mess1);
            this.panDB.Controls.Add(this.button7);
            this.panDB.Controls.Add(this.label24);
            this.panDB.Controls.Add(this.label25);
            this.panDB.Controls.Add(this.browse);
            this.panDB.Controls.Add(this.label23);
            this.panDB.Controls.Add(this.dbName);
            this.panDB.Controls.Add(this.label28);
            this.panDB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panDB.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panDB.Location = new System.Drawing.Point(0, 0);
            this.panDB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panDB.Name = "panDB";
            this.panDB.Size = new System.Drawing.Size(966, 597);
            this.panDB.TabIndex = 3;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(603, 494);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(241, 20);
            this.label53.TabIndex = 144;
            this.label53.Tag = "";
            this.label53.Text = "* One Click to edit segment name";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(323, 494);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(242, 20);
            this.label52.TabIndex = 143;
            this.label52.Tag = "";
            this.label52.Text = "* One Click to edit category name";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(44, 494);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(250, 20);
            this.label51.TabIndex = 142;
            this.label51.Tag = "";
            this.label51.Text = "* One Click to edit discipline name";
            // 
            // backupfolder
            // 
            this.backupfolder.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backupfolder.Location = new System.Drawing.Point(42, 124);
            this.backupfolder.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.backupfolder.Name = "backupfolder";
            this.backupfolder.ReadOnly = true;
            this.backupfolder.Size = new System.Drawing.Size(469, 26);
            this.backupfolder.TabIndex = 141;
            // 
            // lv3
            // 
            this.lv3.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3});
            this.lv3.FullRowSelect = true;
            this.lv3.GridLines = true;
            this.lv3.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lv3.LabelEdit = true;
            this.lv3.Location = new System.Drawing.Point(600, 269);
            this.lv3.Name = "lv3";
            this.lv3.ShowGroups = false;
            this.lv3.ShowItemToolTips = true;
            this.lv3.Size = new System.Drawing.Size(218, 221);
            this.lv3.TabIndex = 0;
            this.lv3.UseCompatibleStateImageBehavior = false;
            this.lv3.View = System.Windows.Forms.View.List;
            this.lv3.AfterLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.lv3_AfterLabelEdit);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Width = 150;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label26.Location = new System.Drawing.Point(597, 235);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(116, 29);
            this.label26.TabIndex = 140;
            this.label26.Tag = "";
            this.label26.Text = "Segments";
            // 
            // lv2
            // 
            this.lv2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2});
            this.lv2.FullRowSelect = true;
            this.lv2.GridLines = true;
            this.lv2.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lv2.LabelEdit = true;
            this.lv2.Location = new System.Drawing.Point(319, 269);
            this.lv2.Name = "lv2";
            this.lv2.ShowGroups = false;
            this.lv2.ShowItemToolTips = true;
            this.lv2.Size = new System.Drawing.Size(218, 221);
            this.lv2.TabIndex = 139;
            this.lv2.UseCompatibleStateImageBehavior = false;
            this.lv2.View = System.Windows.Forms.View.List;
            this.lv2.AfterLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.lv2_AfterLabelEdit);
            // 
            // columnHeader2
            // 
            this.columnHeader2.Width = 150;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label22.Location = new System.Drawing.Point(312, 235);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(127, 29);
            this.label22.TabIndex = 138;
            this.label22.Tag = "";
            this.label22.Text = "Categories";
            // 
            // lv1
            // 
            this.lv1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.lv1.FullRowSelect = true;
            this.lv1.GridLines = true;
            this.lv1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lv1.LabelEdit = true;
            this.lv1.Location = new System.Drawing.Point(42, 270);
            this.lv1.Name = "lv1";
            this.lv1.ShowGroups = false;
            this.lv1.ShowItemToolTips = true;
            this.lv1.Size = new System.Drawing.Size(218, 221);
            this.lv1.TabIndex = 137;
            this.lv1.UseCompatibleStateImageBehavior = false;
            this.lv1.View = System.Windows.Forms.View.List;
            this.lv1.AfterLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.lv1_AfterLabelEdit);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 150;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label21.Location = new System.Drawing.Point(38, 237);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(127, 29);
            this.label21.TabIndex = 136;
            this.label21.Tag = "";
            this.label21.Text = "Disciplines";
            // 
            // mess1
            // 
            this.mess1.BackColor = System.Drawing.Color.Transparent;
            this.mess1.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mess1.ForeColor = System.Drawing.Color.Black;
            this.mess1.Location = new System.Drawing.Point(265, 159);
            this.mess1.Name = "mess1";
            this.mess1.Size = new System.Drawing.Size(549, 49);
            this.mess1.TabIndex = 135;
            this.mess1.Tag = "";
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Transparent;
            this.button7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button7.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Honeydew;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(199, 160);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(53, 30);
            this.button7.TabIndex = 134;
            this.button7.Text = "Ok";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(39, 164);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(191, 26);
            this.label24.TabIndex = 133;
            this.label24.Tag = "";
            this.label24.Text = "Save database copy";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label25.Location = new System.Drawing.Point(25, 87);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(90, 29);
            this.label25.TabIndex = 132;
            this.label25.Tag = "";
            this.label25.Text = "Backup";
            // 
            // browse
            // 
            this.browse.BackColor = System.Drawing.Color.Transparent;
            this.browse.Cursor = System.Windows.Forms.Cursors.Hand;
            this.browse.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.browse.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Honeydew;
            this.browse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.browse.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.browse.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.browse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.browse.Location = new System.Drawing.Point(528, 121);
            this.browse.Name = "browse";
            this.browse.Size = new System.Drawing.Size(93, 30);
            this.browse.TabIndex = 131;
            this.browse.Text = "Browse >>";
            this.browse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.browse.UseVisualStyleBackColor = false;
            this.browse.Click += new System.EventHandler(this.browse_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(209, 18);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(319, 26);
            this.label23.TabIndex = 127;
            this.label23.Tag = "";
            this.label23.Text = "Choose database path and name: ";
            this.label23.Visible = false;
            // 
            // dbName
            // 
            this.dbName.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dbName.Location = new System.Drawing.Point(42, 51);
            this.dbName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dbName.Name = "dbName";
            this.dbName.ReadOnly = true;
            this.dbName.Size = new System.Drawing.Size(469, 26);
            this.dbName.TabIndex = 104;
            this.dbName.TextChanged += new System.EventHandler(this.dbName_TextChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label28.Location = new System.Drawing.Point(25, 15);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(168, 29);
            this.label28.TabIndex = 101;
            this.label28.Tag = "";
            this.label28.Text = "Database Path";
            // 
            // of1
            // 
            this.of1.FileName = "rolljudge2.s3db";
            this.of1.Filter = "File SQLite|*.s3db";
            this.of1.InitialDirectory = "C:\\RollartSystem";
            this.of1.RestoreDirectory = true;
            // 
            // fd1
            // 
            this.fd1.Description = "Select folder";
            // 
            // of2
            // 
            this.of2.FileName = "image file";
            this.of2.Filter = "Image Files (*.bmp;*.jpg;*.jpeg;*.png)|*bmp;*.jpg;*.jpeg;*.png";
            this.of2.InitialDirectory = "C:\\RollartSystem";
            this.of2.Title = "Choose an image to display";
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // openXML
            // 
            this.openXML.FileName = "Rollart_officials";
            this.openXML.Filter = "xml file|*.xml";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1124, 714);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "SettingsForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsForm_FormClosing);
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panCompetitors.ResumeLayout(false);
            this.panCompetitors.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb2)).EndInit();
            this.panTech.ResumeLayout(false);
            this.panTech.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.port)).EndInit();
            this.panel3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nJudges)).EndInit();
            this.panVideo.ResumeLayout(false);
            this.panVideo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.portVideo)).EndInit();
            this.panJudges.ResumeLayout(false);
            this.panJudges.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg5)).EndInit();
            this.panElements.ResumeLayout(false);
            this.panElements.PerformLayout();
            this.panEl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg3)).EndInit();
            this.panParameters.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDance)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPrecision)).EndInit();
            this.panDB.ResumeLayout(false);
            this.panDB.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panTech;
        private System.Windows.Forms.NumericUpDown nJudges;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox root;
        private IPAddressControlLib.IPAddressControl ipT;
        private System.Windows.Forms.Panel panParameters;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buTech;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button titolo;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbCategory;
        private System.Windows.Forms.DataGridView dg1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cb2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DataGridView dg2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panElements;
        private System.Windows.Forms.ComboBox cb3;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Panel panDB;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox dbName;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.OpenFileDialog of1;
        private System.Windows.Forms.Button browse;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label mess1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ListView lv1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ListView lv2;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ListView lv3;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panCompetitors;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox club;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox country;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button add;
        private System.Windows.Forms.Label err2;
        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Panel panJudges;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label err3;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.TextBox region;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox country2;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox name2;
        private System.Windows.Forms.DataGridView dg5;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label err4;
        private System.Windows.Forms.FolderBrowserDialog fd1;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.DataGridView dgDance;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox cbCategoryDance;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.RadioButton rb1;
        private System.Windows.Forms.RadioButton rb3;
        private System.Windows.Forms.RadioButton rb2;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.RadioButton rb11;
        private System.Windows.Forms.RadioButton rb12;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.CheckBox cb4;
        private System.Windows.Forms.PictureBox pb1;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.OpenFileDialog of2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private JCS.ToggleSwitch ts1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.Panel panEl;
        private System.Windows.Forms.DataGridView dg3;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView dgPrecision;
        private System.Windows.Forms.ComboBox cbPrecision;
        private System.Windows.Forms.PictureBox pb;
        private System.Windows.Forms.Label found;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openXML;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Label found2;
        private System.Windows.Forms.PictureBox pb2;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.DataGridView dg4;
        private System.Windows.Forms.TextBox region2;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn dg5Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn dg5Region;
        private System.Windows.Forms.DataGridViewTextBoxColumn dg5Country;
        private System.Windows.Forms.TextBox backupfolder;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.ComboBox cbSec;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.NumericUpDown port;
        private System.Windows.Forms.Button video;
        private System.Windows.Forms.Panel panVideo;
        private System.Windows.Forms.NumericUpDown portVideo;
        private System.Windows.Forms.ComboBox pingVideo;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private IPAddressControlLib.IPAddressControl ipVideo;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.DataGridViewTextBoxColumn dg4ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dg4Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn dg4Societa;
        private System.Windows.Forms.DataGridViewTextBoxColumn dg4Region;
        private System.Windows.Forms.DataGridViewTextBoxColumn dg4Country;
        private System.Windows.Forms.DataGridViewTextBoxColumn dg4Spec;
    }
}
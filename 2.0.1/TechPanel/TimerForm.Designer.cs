﻿namespace RollartSystemTech
{
    partial class TimerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label56 = new System.Windows.Forms.Label();
            this.display = new System.Windows.Forms.Label();
            this.ts1 = new JCS.ToggleSwitch();
            this.gbTimer = new System.Windows.Forms.GroupBox();
            this.timer = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.play = new System.Windows.Forms.Button();
            this.error = new System.Windows.Forms.Label();
            this.upSeconds = new System.Windows.Forms.NumericUpDown();
            this.upMinutes = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.rbLarge = new System.Windows.Forms.RadioButton();
            this.rbSmall = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.gbTimer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.upSeconds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upMinutes)).BeginInit();
            this.SuspendLayout();
            // 
            // label56
            // 
            this.label56.BackColor = System.Drawing.Color.Transparent;
            this.label56.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(106, 11);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(179, 37);
            this.label56.TabIndex = 181;
            this.label56.Tag = "";
            this.label56.Text = "OFF removes the clock display, ON adds the clock to the display";
            // 
            // display
            // 
            this.display.BackColor = System.Drawing.Color.Transparent;
            this.display.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.display.ForeColor = System.Drawing.Color.Black;
            this.display.Image = global::RollartSystemTech.Properties.Resources.videoscreen;
            this.display.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.display.Location = new System.Drawing.Point(272, 2);
            this.display.Name = "display";
            this.display.Size = new System.Drawing.Size(74, 65);
            this.display.TabIndex = 180;
            this.display.Tag = "";
            this.display.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // ts1
            // 
            this.ts1.BackColor = System.Drawing.Color.Transparent;
            this.ts1.Location = new System.Drawing.Point(12, 11);
            this.ts1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ts1.Name = "ts1";
            this.ts1.OffFont = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ts1.OffForeColor = System.Drawing.Color.Yellow;
            this.ts1.OffText = "OFF";
            this.ts1.OnFont = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ts1.OnForeColor = System.Drawing.Color.Yellow;
            this.ts1.OnText = "ON";
            this.ts1.Size = new System.Drawing.Size(88, 30);
            this.ts1.Style = JCS.ToggleSwitch.ToggleSwitchStyle.Fancy;
            this.ts1.TabIndex = 182;
            this.ts1.CheckedChanged += new JCS.ToggleSwitch.CheckedChangedDelegate(this.ts1_CheckedChanged);
            // 
            // gbTimer
            // 
            this.gbTimer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.gbTimer.Controls.Add(this.timer);
            this.gbTimer.Controls.Add(this.button3);
            this.gbTimer.Controls.Add(this.button4);
            this.gbTimer.Controls.Add(this.button2);
            this.gbTimer.Controls.Add(this.play);
            this.gbTimer.Controls.Add(this.error);
            this.gbTimer.Controls.Add(this.upSeconds);
            this.gbTimer.Controls.Add(this.upMinutes);
            this.gbTimer.Controls.Add(this.label3);
            this.gbTimer.Controls.Add(this.rbLarge);
            this.gbTimer.Controls.Add(this.rbSmall);
            this.gbTimer.Controls.Add(this.label2);
            this.gbTimer.Controls.Add(this.label4);
            this.gbTimer.Enabled = false;
            this.gbTimer.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbTimer.Location = new System.Drawing.Point(12, 61);
            this.gbTimer.Name = "gbTimer";
            this.gbTimer.Size = new System.Drawing.Size(330, 151);
            this.gbTimer.TabIndex = 183;
            this.gbTimer.TabStop = false;
            this.gbTimer.Text = "Display Timer";
            this.gbTimer.Enter += new System.EventHandler(this.gbTimer_Enter);
            // 
            // timer
            // 
            this.timer.AutoSize = true;
            this.timer.BackColor = System.Drawing.Color.Transparent;
            this.timer.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timer.ForeColor = System.Drawing.Color.Green;
            this.timer.Location = new System.Drawing.Point(202, 44);
            this.timer.Name = "timer";
            this.timer.Size = new System.Drawing.Size(0, 24);
            this.timer.TabIndex = 194;
            this.timer.Tag = "";
            // 
            // button3
            // 
            this.button3.BackgroundImage = global::RollartSystemTech.Properties.Resources.playback_reload_icon_24;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(294, 91);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(20, 20);
            this.button3.TabIndex = 193;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackgroundImage = global::RollartSystemTech.Properties.Resources.playback_stop_icon_24;
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(260, 89);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(26, 26);
            this.button4.TabIndex = 192;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::RollartSystemTech.Properties.Resources.playback_pause_icon_24;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(224, 89);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(26, 26);
            this.button2.TabIndex = 191;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // play
            // 
            this.play.BackgroundImage = global::RollartSystemTech.Properties.Resources.playback_play_icon_241;
            this.play.Cursor = System.Windows.Forms.Cursors.Hand;
            this.play.FlatAppearance.BorderSize = 0;
            this.play.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.play.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.play.Location = new System.Drawing.Point(191, 89);
            this.play.Name = "play";
            this.play.Size = new System.Drawing.Size(26, 26);
            this.play.TabIndex = 190;
            this.play.UseVisualStyleBackColor = true;
            this.play.Click += new System.EventHandler(this.play_Click);
            // 
            // error
            // 
            this.error.BackColor = System.Drawing.Color.Transparent;
            this.error.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.error.ForeColor = System.Drawing.Color.Red;
            this.error.Location = new System.Drawing.Point(6, 118);
            this.error.Name = "error";
            this.error.Size = new System.Drawing.Size(324, 19);
            this.error.TabIndex = 189;
            this.error.Tag = "";
            // 
            // upSeconds
            // 
            this.upSeconds.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upSeconds.Location = new System.Drawing.Point(139, 42);
            this.upSeconds.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.upSeconds.Name = "upSeconds";
            this.upSeconds.Size = new System.Drawing.Size(57, 23);
            this.upSeconds.TabIndex = 188;
            this.upSeconds.ValueChanged += new System.EventHandler(this.upMinutes_ValueChanged);
            // 
            // upMinutes
            // 
            this.upMinutes.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upMinutes.Location = new System.Drawing.Point(72, 42);
            this.upMinutes.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.upMinutes.Name = "upMinutes";
            this.upMinutes.Size = new System.Drawing.Size(57, 23);
            this.upMinutes.TabIndex = 187;
            this.upMinutes.ValueChanged += new System.EventHandler(this.upMinutes_ValueChanged);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(85, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 19);
            this.label3.TabIndex = 185;
            this.label3.Tag = "";
            this.label3.Text = "(minutes:seconds)";
            // 
            // rbLarge
            // 
            this.rbLarge.AutoSize = true;
            this.rbLarge.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbLarge.Location = new System.Drawing.Point(88, 89);
            this.rbLarge.Name = "rbLarge";
            this.rbLarge.Size = new System.Drawing.Size(67, 22);
            this.rbLarge.TabIndex = 185;
            this.rbLarge.Text = "LARGE";
            this.rbLarge.UseVisualStyleBackColor = true;
            // 
            // rbSmall
            // 
            this.rbSmall.AutoSize = true;
            this.rbSmall.Checked = true;
            this.rbSmall.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSmall.Location = new System.Drawing.Point(19, 89);
            this.rbSmall.Name = "rbSmall";
            this.rbSmall.Size = new System.Drawing.Size(57, 22);
            this.rbSmall.TabIndex = 184;
            this.rbSmall.TabStop = true;
            this.rbSmall.Text = "Small";
            this.rbSmall.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(13, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 18);
            this.label2.TabIndex = 183;
            this.label2.Tag = "";
            this.label2.Text = "Period:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(127, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 22);
            this.label4.TabIndex = 185;
            this.label4.Tag = "";
            this.label4.Text = ":";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(267, 218);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 30);
            this.button1.TabIndex = 184;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // TimerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(353, 260);
            this.ControlBox = false;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.gbTimer);
            this.Controls.Add(this.ts1);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.display);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "TimerForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "RollArt Tech Panel - TIMER";
            this.Load += new System.EventHandler(this.TimerForm_Load);
            this.gbTimer.ResumeLayout(false);
            this.gbTimer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.upSeconds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upMinutes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label display;
        private JCS.ToggleSwitch ts1;
        private System.Windows.Forms.GroupBox gbTimer;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbLarge;
        private System.Windows.Forms.RadioButton rbSmall;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown upMinutes;
        private System.Windows.Forms.NumericUpDown upSeconds;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label error;
        private System.Windows.Forms.Button play;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label timer;
    }
}
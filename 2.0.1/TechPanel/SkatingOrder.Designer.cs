﻿namespace RollartSystemTech
{
    partial class SkatingOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SkatingOrder));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panSkater = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.namep = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.club = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.naz = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.info = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.add = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.updateskater = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.delete = new System.Windows.Forms.ToolStripButton();
            this.hidden = new System.Windows.Forms.ToolStripLabel();
            this.gara = new System.Windows.Forms.Label();
            this.status = new System.Windows.Forms.Label();
            this.l1 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dado = new System.Windows.Forms.Button();
            this.print = new System.Windows.Forms.Button();
            this.up = new System.Windows.Forms.Button();
            this.down = new System.Windows.Forms.Button();
            this.confirm = new System.Windows.Forms.Button();
            this.close = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pb = new System.Windows.Forms.PictureBox();
            this.lbCompetitors = new System.Windows.Forms.ListView();
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.id_specialita = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.cr = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panelSkater = new RollartSystemTech.Report.PanelReport();
            this.panel1.SuspendLayout();
            this.panSkater.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.panel1.Controls.Add(this.panSkater);
            this.panel1.Controls.Add(this.gara);
            this.panel1.Controls.Add(this.status);
            this.panel1.Controls.Add(this.l1);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(932, 114);
            this.panel1.TabIndex = 100;
            // 
            // panSkater
            // 
            this.panSkater.AutoSize = false;
            this.panSkater.BackgroundImage = global::RollartSystemTech.Properties.Resources.WS_Logo_OnDark_Horz;
            this.panSkater.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panSkater.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panSkater.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.panSkater.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.panSkater.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel4,
            this.namep,
            this.toolStripLabel3,
            this.club,
            this.toolStripLabel1,
            this.naz,
            this.toolStripLabel2,
            this.info,
            this.toolStripSeparator1,
            this.add,
            this.toolStripSeparator2,
            this.updateskater,
            this.toolStripSeparator3,
            this.delete,
            this.hidden});
            this.panSkater.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.panSkater.Location = new System.Drawing.Point(0, 49);
            this.panSkater.Name = "panSkater";
            this.panSkater.Padding = new System.Windows.Forms.Padding(5);
            this.panSkater.Size = new System.Drawing.Size(932, 65);
            this.panSkater.TabIndex = 138;
            this.panSkater.Text = "toolStrip1";
            this.panSkater.Visible = false;
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.ForeColor = System.Drawing.Color.White;
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(47, 16);
            this.toolStripLabel4.Text = "Name    ";
            // 
            // namep
            // 
            this.namep.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.namep.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.namep.MaxLength = 50;
            this.namep.Name = "namep";
            this.namep.Size = new System.Drawing.Size(330, 20);
            this.namep.Leave += new System.EventHandler(this.namep_Leave);
            this.namep.KeyDown += new System.Windows.Forms.KeyEventHandler(this.namep_KeyDown);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.ForeColor = System.Drawing.Color.White;
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(29, 16);
            this.toolStripLabel3.Text = "Club";
            // 
            // club
            // 
            this.club.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.club.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.club.MaxLength = 50;
            this.club.Name = "club";
            this.club.Size = new System.Drawing.Size(400, 20);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.ForeColor = System.Drawing.Color.White;
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(41, 16);
            this.toolStripLabel1.Text = "Nation";
            // 
            // naz
            // 
            this.naz.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.naz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.naz.MaxLength = 3;
            this.naz.Name = "naz";
            this.naz.Size = new System.Drawing.Size(50, 20);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.AutoSize = false;
            this.toolStripLabel2.ForeColor = System.Drawing.Color.White;
            this.toolStripLabel2.Margin = new System.Windows.Forms.Padding(0, 8, 0, 2);
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(200, 16);
            this.toolStripLabel2.Text = "Info (music, coach, coreographer...)";
            this.toolStripLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // info
            // 
            this.info.AutoToolTip = true;
            this.info.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.info.Margin = new System.Windows.Forms.Padding(1, 5, 1, 0);
            this.info.MaxLength = 40;
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(330, 20);
            this.info.ToolTipText = "max 40 characters";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 23);
            // 
            // add
            // 
            this.add.BackColor = System.Drawing.Color.Transparent;
            this.add.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.add.Font = new System.Drawing.Font("Trebuchet MS", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.add.ForeColor = System.Drawing.Color.Yellow;
            this.add.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.add.Margin = new System.Windows.Forms.Padding(0, 5, 0, 2);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(62, 22);
            this.add.Text = "Add new";
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 23);
            // 
            // updateskater
            // 
            this.updateskater.BackColor = System.Drawing.Color.Transparent;
            this.updateskater.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.updateskater.Font = new System.Drawing.Font("Trebuchet MS", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateskater.ForeColor = System.Drawing.Color.Yellow;
            this.updateskater.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.updateskater.Margin = new System.Windows.Forms.Padding(0, 5, 0, 2);
            this.updateskater.Name = "updateskater";
            this.updateskater.Size = new System.Drawing.Size(52, 22);
            this.updateskater.Text = "Update";
            this.updateskater.Click += new System.EventHandler(this.updateskater_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 23);
            // 
            // delete
            // 
            this.delete.BackColor = System.Drawing.Color.Transparent;
            this.delete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.delete.Font = new System.Drawing.Font("Trebuchet MS", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.delete.ForeColor = System.Drawing.Color.Yellow;
            this.delete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.delete.Margin = new System.Windows.Forms.Padding(0, 5, 0, 2);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(50, 22);
            this.delete.Text = "Delete";
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // hidden
            // 
            this.hidden.Name = "hidden";
            this.hidden.Size = new System.Drawing.Size(0, 0);
            this.hidden.Visible = false;
            // 
            // gara
            // 
            this.gara.AutoSize = true;
            this.gara.BackColor = System.Drawing.Color.Transparent;
            this.gara.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gara.ForeColor = System.Drawing.Color.Black;
            this.gara.Location = new System.Drawing.Point(8, 3);
            this.gara.Name = "gara";
            this.gara.Size = new System.Drawing.Size(73, 22);
            this.gara.TabIndex = 136;
            this.gara.Tag = "";
            this.gara.Text = "Segment";
            // 
            // status
            // 
            this.status.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.status.AutoSize = true;
            this.status.BackColor = System.Drawing.Color.Transparent;
            this.status.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.status.ForeColor = System.Drawing.Color.Black;
            this.status.Image = global::RollartSystemTech.Properties.Resources.bullet_green;
            this.status.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.status.Location = new System.Drawing.Point(781, 6);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(143, 24);
            this.status.TabIndex = 135;
            this.status.Tag = "";
            this.status.Text = "      Completed";
            this.status.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // l1
            // 
            this.l1.AutoSize = true;
            this.l1.BackColor = System.Drawing.Color.Transparent;
            this.l1.Font = new System.Drawing.Font("Trebuchet MS", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l1.ForeColor = System.Drawing.Color.White;
            this.l1.Location = new System.Drawing.Point(9, 22);
            this.l1.Name = "l1";
            this.l1.Size = new System.Drawing.Size(74, 22);
            this.l1.TabIndex = 133;
            this.l1.Tag = "";
            this.l1.Text = "Segment";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Trebuchet MS", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.SlateGray;
            this.label29.Location = new System.Drawing.Point(12, 9);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(0, 27);
            this.label29.TabIndex = 131;
            this.label29.Tag = "";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Controls.Add(this.dado);
            this.panel2.Controls.Add(this.print);
            this.panel2.Controls.Add(this.up);
            this.panel2.Controls.Add(this.down);
            this.panel2.Controls.Add(this.confirm);
            this.panel2.Controls.Add(this.close);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 584);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(932, 67);
            this.panel2.TabIndex = 101;
            // 
            // dado
            // 
            this.dado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dado.BackColor = System.Drawing.Color.White;
            this.dado.BackgroundImage = global::RollartSystemTech.Properties.Resources.dice;
            this.dado.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.dado.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dado.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.dado.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dado.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dado.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dado.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.dado.Location = new System.Drawing.Point(618, 9);
            this.dado.Name = "dado";
            this.dado.Size = new System.Drawing.Size(48, 49);
            this.dado.TabIndex = 138;
            this.dado.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.dado.UseVisualStyleBackColor = false;
            this.dado.Click += new System.EventHandler(this.button2_Click);
            // 
            // print
            // 
            this.print.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.print.BackColor = System.Drawing.Color.White;
            this.print.BackgroundImage = global::RollartSystemTech.Properties.Resources.print_icon_24;
            this.print.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.print.Cursor = System.Windows.Forms.Cursors.Hand;
            this.print.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.print.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.print.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.print.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.print.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.print.Location = new System.Drawing.Point(673, 9);
            this.print.Name = "print";
            this.print.Size = new System.Drawing.Size(48, 49);
            this.print.TabIndex = 137;
            this.print.UseVisualStyleBackColor = false;
            this.print.Visible = false;
            this.print.Click += new System.EventHandler(this.print_Click);
            // 
            // up
            // 
            this.up.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.up.BackColor = System.Drawing.Color.Transparent;
            this.up.Cursor = System.Windows.Forms.Cursors.Hand;
            this.up.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.up.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.up.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(253)))), ((int)(((byte)(208)))));
            this.up.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.up.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.up.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.up.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.up.Location = new System.Drawing.Point(462, 9);
            this.up.Name = "up";
            this.up.Size = new System.Drawing.Size(149, 49);
            this.up.TabIndex = 136;
            this.up.Text = "Move Up";
            this.up.UseVisualStyleBackColor = false;
            this.up.Click += new System.EventHandler(this.up_Click);
            // 
            // down
            // 
            this.down.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.down.BackColor = System.Drawing.Color.Transparent;
            this.down.Cursor = System.Windows.Forms.Cursors.Hand;
            this.down.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.down.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.down.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(253)))), ((int)(((byte)(208)))));
            this.down.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.down.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.down.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.down.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.down.Location = new System.Drawing.Point(292, 9);
            this.down.Name = "down";
            this.down.Size = new System.Drawing.Size(163, 49);
            this.down.TabIndex = 135;
            this.down.Text = "Move Down";
            this.down.UseVisualStyleBackColor = false;
            this.down.Click += new System.EventHandler(this.down_Click);
            // 
            // confirm
            // 
            this.confirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.confirm.BackColor = System.Drawing.Color.Transparent;
            this.confirm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.confirm.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.confirm.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.confirm.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(253)))), ((int)(((byte)(208)))));
            this.confirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.confirm.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.confirm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.confirm.Location = new System.Drawing.Point(727, 9);
            this.confirm.Name = "confirm";
            this.confirm.Size = new System.Drawing.Size(193, 49);
            this.confirm.TabIndex = 134;
            this.confirm.Text = "Confirm and Close";
            this.confirm.UseVisualStyleBackColor = false;
            this.confirm.Click += new System.EventHandler(this.confirmNewEvent_Click);
            // 
            // close
            // 
            this.close.BackColor = System.Drawing.Color.Transparent;
            this.close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.close.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.close.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.close.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(253)))), ((int)(((byte)(208)))));
            this.close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.close.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.close.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.close.Location = new System.Drawing.Point(12, 9);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(145, 49);
            this.close.TabIndex = 133;
            this.close.Text = "Close";
            this.close.UseVisualStyleBackColor = false;
            this.close.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.pb);
            this.panel3.Controls.Add(this.lbCompetitors);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.cr);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 114);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(932, 470);
            this.panel3.TabIndex = 102;
            // 
            // pb
            // 
            this.pb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pb.BackColor = System.Drawing.Color.White;
            this.pb.Image = global::RollartSystemTech.Properties.Resources.loading;
            this.pb.Location = new System.Drawing.Point(477, 238);
            this.pb.Name = "pb";
            this.pb.Size = new System.Drawing.Size(52, 51);
            this.pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb.TabIndex = 142;
            this.pb.TabStop = false;
            this.pb.Visible = false;
            // 
            // lbCompetitors
            // 
            this.lbCompetitors.Activation = System.Windows.Forms.ItemActivation.TwoClick;
            this.lbCompetitors.BackColor = System.Drawing.Color.White;
            this.lbCompetitors.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader3,
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11,
            this.id_specialita});
            this.lbCompetitors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbCompetitors.Font = new System.Drawing.Font("Trebuchet MS", 9F);
            this.lbCompetitors.FullRowSelect = true;
            this.lbCompetitors.GridLines = true;
            this.lbCompetitors.HideSelection = false;
            this.lbCompetitors.HoverSelection = true;
            this.lbCompetitors.Location = new System.Drawing.Point(0, 0);
            this.lbCompetitors.MultiSelect = false;
            this.lbCompetitors.Name = "lbCompetitors";
            this.lbCompetitors.ShowItemToolTips = true;
            this.lbCompetitors.Size = new System.Drawing.Size(932, 470);
            this.lbCompetitors.SmallImageList = this.imageList1;
            this.lbCompetitors.TabIndex = 85;
            this.lbCompetitors.UseCompatibleStateImageBehavior = false;
            this.lbCompetitors.View = System.Windows.Forms.View.Details;
            this.lbCompetitors.ItemActivate += new System.EventHandler(this.lbCompetitors_ItemActivate);
            this.lbCompetitors.SelectedIndexChanged += new System.EventHandler(this.lbCompetitors_SelectedIndexChanged);
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Order";
            this.columnHeader4.Width = 48;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Name";
            this.columnHeader5.Width = 257;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "";
            this.columnHeader3.Width = 0;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Club";
            this.columnHeader1.Width = 162;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Nation";
            this.columnHeader2.Width = 55;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "BaseTech";
            this.columnHeader6.Width = 70;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Final Tech";
            this.columnHeader7.Width = 72;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Components";
            this.columnHeader8.Width = 81;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Deductions";
            this.columnHeader9.Width = 76;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Total";
            this.columnHeader10.Width = 53;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Rank";
            this.columnHeader11.Width = 40;
            // 
            // id_specialita
            // 
            this.id_specialita.Text = "";
            this.id_specialita.Width = 0;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "medal_gold_2.png");
            this.imageList1.Images.SetKeyName(1, "medal_silver_3.png");
            this.imageList1.Images.SetKeyName(2, "medal_bronze_1.png");
            this.imageList1.Images.SetKeyName(3, "tick.png");
            this.imageList1.Images.SetKeyName(4, "file_extension_pdf.png");
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(879, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(53, 28);
            this.button1.TabIndex = 87;
            this.button1.Text = "Back";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // cr
            // 
            this.cr.ActiveViewIndex = -1;
            this.cr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cr.Cursor = System.Windows.Forms.Cursors.Default;
            this.cr.DisplayBackgroundEdge = false;
            this.cr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cr.Location = new System.Drawing.Point(0, 0);
            this.cr.Name = "cr";
            this.cr.ShowCloseButton = false;
            this.cr.ShowGotoPageButton = false;
            this.cr.ShowGroupTreeButton = false;
            this.cr.ShowLogo = false;
            this.cr.ShowPageNavigateButtons = false;
            this.cr.ShowParameterPanelButton = false;
            this.cr.Size = new System.Drawing.Size(932, 470);
            this.cr.TabIndex = 86;
            this.cr.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // SkatingOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(932, 651);
            this.ControlBox = false;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "SkatingOrder";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Skating Order/Results";
            this.Load += new System.EventHandler(this.SkatingOrder_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panSkater.ResumeLayout(false);
            this.panSkater.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button confirm;
        private System.Windows.Forms.Button close;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label l1;
        private System.Windows.Forms.ListView lbCompetitors;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button up;
        private System.Windows.Forms.Button down;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Label status;
        private System.Windows.Forms.Label gara;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private RollartSystemTech.Report.PanelReport panelSkater;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer cr;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pb;
        private System.Windows.Forms.Button print;
        private System.Windows.Forms.ColumnHeader id_specialita;
        private System.Windows.Forms.ToolStrip panSkater;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripTextBox namep;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripTextBox club;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripTextBox naz;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton delete;
        private System.Windows.Forms.ToolStripButton add;
        private System.Windows.Forms.ToolStripButton updateskater;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel hidden;
        private System.Windows.Forms.Button dado;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox info;
    }
}
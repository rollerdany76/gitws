﻿namespace RollartSystemTech
{
    partial class ScoreToScreen2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rank = new System.Windows.Forms.Label();
            this.total = new System.Windows.Forms.Label();
            this.techEl = new System.Windows.Forms.Label();
            this.comp = new System.Windows.Forms.Label();
            this.ded = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.final = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rank
            // 
            this.rank.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rank.BackColor = System.Drawing.Color.Black;
            this.rank.Font = new System.Drawing.Font("Trebuchet MS", 70F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rank.ForeColor = System.Drawing.Color.Yellow;
            this.rank.Location = new System.Drawing.Point(1103, 395);
            this.rank.Name = "rank";
            this.rank.Size = new System.Drawing.Size(177, 159);
            this.rank.TabIndex = 100;
            this.rank.Text = "8";
            this.rank.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // total
            // 
            this.total.BackColor = System.Drawing.Color.Black;
            this.total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.total.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.total.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.total.Font = new System.Drawing.Font("Trebuchet MS", 48F, System.Drawing.FontStyle.Bold);
            this.total.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(199)))), ((int)(((byte)(54)))));
            this.total.Location = new System.Drawing.Point(768, 128);
            this.total.Margin = new System.Windows.Forms.Padding(0);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(256, 90);
            this.total.TabIndex = 101;
            this.total.Text = "999,99";
            this.total.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // techEl
            // 
            this.techEl.BackColor = System.Drawing.Color.Black;
            this.techEl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.techEl.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.techEl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.techEl.Font = new System.Drawing.Font("Trebuchet MS", 48F, System.Drawing.FontStyle.Bold);
            this.techEl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(199)))), ((int)(((byte)(54)))));
            this.techEl.Location = new System.Drawing.Point(0, 128);
            this.techEl.Margin = new System.Windows.Forms.Padding(0);
            this.techEl.Name = "techEl";
            this.techEl.Size = new System.Drawing.Size(256, 90);
            this.techEl.TabIndex = 102;
            this.techEl.Text = "188,88";
            this.techEl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comp
            // 
            this.comp.BackColor = System.Drawing.Color.Black;
            this.comp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.comp.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.comp.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.comp.Font = new System.Drawing.Font("Trebuchet MS", 48F, System.Drawing.FontStyle.Bold);
            this.comp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(199)))), ((int)(((byte)(54)))));
            this.comp.Location = new System.Drawing.Point(256, 128);
            this.comp.Margin = new System.Windows.Forms.Padding(0);
            this.comp.Name = "comp";
            this.comp.Size = new System.Drawing.Size(256, 90);
            this.comp.TabIndex = 103;
            this.comp.Text = "188,88";
            this.comp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ded
            // 
            this.ded.BackColor = System.Drawing.Color.Black;
            this.ded.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ded.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.ded.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ded.Font = new System.Drawing.Font("Trebuchet MS", 48F, System.Drawing.FontStyle.Bold);
            this.ded.ForeColor = System.Drawing.Color.Red;
            this.ded.Location = new System.Drawing.Point(512, 128);
            this.ded.Margin = new System.Windows.Forms.Padding(0);
            this.ded.Name = "ded";
            this.ded.Size = new System.Drawing.Size(256, 90);
            this.ded.TabIndex = 104;
            this.ded.Text = "-2,00";
            this.ded.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(1238, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(42, 32);
            this.button1.TabIndex = 105;
            this.button1.Text = "-->";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // final
            // 
            this.final.BackColor = System.Drawing.Color.Black;
            this.final.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.final.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.final.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.final.Font = new System.Drawing.Font("Trebuchet MS", 48F, System.Drawing.FontStyle.Bold);
            this.final.ForeColor = System.Drawing.Color.Yellow;
            this.final.Location = new System.Drawing.Point(1024, 128);
            this.final.Margin = new System.Windows.Forms.Padding(0);
            this.final.Name = "final";
            this.final.Size = new System.Drawing.Size(256, 90);
            this.final.TabIndex = 113;
            this.final.Text = "999,99";
            this.final.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Font = new System.Drawing.Font("Trebuchet MS", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Yellow;
            this.label1.Location = new System.Drawing.Point(1136, 518);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 27);
            this.label1.TabIndex = 115;
            this.label1.Text = "Currently";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(934, 38);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(313, 313);
            this.pictureBox3.TabIndex = 117;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.DimGray;
            this.pictureBox2.BackgroundImage = global::RollartSystemTech.Properties.Resources.WS_Logo_OnDark_Horz;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(36, 38);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(658, 313);
            this.pictureBox2.TabIndex = 116;
            this.pictureBox2.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.Black;
            this.label2.ForeColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(-5, 687);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1285, 70);
            this.label2.TabIndex = 118;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Black;
            this.label3.ForeColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(-2, 411);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 46);
            this.label3.TabIndex = 119;
            // 
            // name
            // 
            this.name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.name.BackColor = System.Drawing.Color.Black;
            this.name.Font = new System.Drawing.Font("Trebuchet MS", 46F, System.Drawing.FontStyle.Bold);
            this.name.ForeColor = System.Drawing.Color.White;
            this.name.Location = new System.Drawing.Point(-1, 395);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(1141, 159);
            this.name.TabIndex = 120;
            this.name.Text = "COUPLE DANCE NAME 1 - COUPLE DANCE NAME 2";
            this.name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.BackgroundImage = global::RollartSystemTech.Properties.Resources.newback;
            this.tableLayoutPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.techEl, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.comp, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.ded, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.total, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.final, 4, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 554);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.72131F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 53.27869F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1280, 218);
            this.tableLayoutPanel1.TabIndex = 121;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.BackColor = System.Drawing.Color.Black;
            this.label4.Font = new System.Drawing.Font("Trebuchet MS", 60F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Yellow;
            this.label4.Location = new System.Drawing.Point(1133, 395);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(147, 159);
            this.label4.TabIndex = 122;
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ScoreToScreen2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(89)))), ((int)(((byte)(79)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1280, 772);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rank);
            this.Controls.Add(this.name);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label4);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.ForeColor = System.Drawing.Color.Blue;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ScoreToScreen2";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "ScoreToScreen";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ScoreToScreen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label rank;
        private System.Windows.Forms.Label total;
        private System.Windows.Forms.Label techEl;
        private System.Windows.Forms.Label comp;
        private System.Windows.Forms.Label ded;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label final;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label name;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label4;
    }
}
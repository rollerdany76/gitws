﻿namespace RollartSystemTech
{
    partial class CheckQoe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckQoe));
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] {
            "1",
            "Skating Skills"}, -1, System.Drawing.Color.Empty, System.Drawing.Color.Empty, new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0))));
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem(new string[] {
            "2",
            "Transitions"}, -1);
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem(new string[] {
            "3",
            "Performance "}, -1);
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem(new string[] {
            "4",
            "Choreography"}, -1);
            this.panel2 = new System.Windows.Forms.Panel();
            this.competitor = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.final = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.position = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.artistic = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.elements2 = new System.Windows.Forms.Label();
            this.tech2 = new System.Windows.Forms.Label();
            this.total = new System.Windows.Forms.Label();
            this.deductions = new System.Windows.Forms.Label();
            this.elements = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tech = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.next = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lvQOE = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.note = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.magg = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lvComp = new System.Windows.Forms.ListView();
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Controls.Add(this.competitor);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.next);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(875, 52);
            this.panel2.TabIndex = 1;
            // 
            // competitor
            // 
            this.competitor.BackColor = System.Drawing.Color.Black;
            this.competitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.competitor.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.competitor.ForeColor = System.Drawing.Color.Lime;
            this.competitor.Location = new System.Drawing.Point(0, 0);
            this.competitor.Name = "competitor";
            this.competitor.Size = new System.Drawing.Size(291, 52);
            this.competitor.TabIndex = 107;
            this.competitor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Black;
            this.panel5.Controls.Add(this.final);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.position);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.artistic);
            this.panel5.Controls.Add(this.label26);
            this.panel5.Controls.Add(this.elements2);
            this.panel5.Controls.Add(this.tech2);
            this.panel5.Controls.Add(this.total);
            this.panel5.Controls.Add(this.deductions);
            this.panel5.Controls.Add(this.elements);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.tech);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(291, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(510, 52);
            this.panel5.TabIndex = 106;
            // 
            // final
            // 
            this.final.AutoSize = true;
            this.final.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.final.ForeColor = System.Drawing.Color.Aqua;
            this.final.Location = new System.Drawing.Point(445, 6);
            this.final.Name = "final";
            this.final.Size = new System.Drawing.Size(59, 23);
            this.final.TabIndex = 104;
            this.final.Text = "152,00";
            this.final.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Aqua;
            this.label4.Location = new System.Drawing.Point(457, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 17);
            this.label4.TabIndex = 103;
            this.label4.Text = "Final";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // position
            // 
            this.position.AutoSize = true;
            this.position.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.position.ForeColor = System.Drawing.Color.Yellow;
            this.position.Location = new System.Drawing.Point(402, 6);
            this.position.Name = "position";
            this.position.Size = new System.Drawing.Size(19, 23);
            this.position.TabIndex = 102;
            this.position.Text = "0";
            this.position.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Yellow;
            this.label3.Location = new System.Drawing.Point(389, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 17);
            this.label3.TabIndex = 101;
            this.label3.Text = "Position";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // artistic
            // 
            this.artistic.AutoSize = true;
            this.artistic.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.artistic.ForeColor = System.Drawing.Color.White;
            this.artistic.Location = new System.Drawing.Point(243, 6);
            this.artistic.Name = "artistic";
            this.artistic.Size = new System.Drawing.Size(50, 23);
            this.artistic.TabIndex = 100;
            this.artistic.Text = "56,00";
            this.artistic.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.White;
            this.label26.Location = new System.Drawing.Point(233, 29);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(70, 17);
            this.label26.TabIndex = 99;
            this.label26.Text = "Components";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // elements2
            // 
            this.elements2.AutoSize = true;
            this.elements2.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.elements2.ForeColor = System.Drawing.Color.White;
            this.elements2.Location = new System.Drawing.Point(90, 6);
            this.elements2.Name = "elements2";
            this.elements2.Size = new System.Drawing.Size(50, 23);
            this.elements2.TabIndex = 98;
            this.elements2.Text = "99,00";
            this.elements2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tech2
            // 
            this.tech2.AutoSize = true;
            this.tech2.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tech2.ForeColor = System.Drawing.Color.White;
            this.tech2.Location = new System.Drawing.Point(89, 29);
            this.tech2.Name = "tech2";
            this.tech2.Size = new System.Drawing.Size(54, 17);
            this.tech2.TabIndex = 97;
            this.tech2.Text = "Final tech";
            this.tech2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // total
            // 
            this.total.AutoSize = true;
            this.total.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total.ForeColor = System.Drawing.Color.Lime;
            this.total.Location = new System.Drawing.Point(322, 6);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(59, 23);
            this.total.TabIndex = 96;
            this.total.Text = "159,00";
            this.total.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // deductions
            // 
            this.deductions.AutoSize = true;
            this.deductions.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deductions.ForeColor = System.Drawing.Color.Red;
            this.deductions.Location = new System.Drawing.Point(167, 6);
            this.deductions.Name = "deductions";
            this.deductions.Size = new System.Drawing.Size(46, 23);
            this.deductions.TabIndex = 95;
            this.deductions.Text = "-1,00";
            this.deductions.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // elements
            // 
            this.elements.AutoSize = true;
            this.elements.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.elements.ForeColor = System.Drawing.Color.White;
            this.elements.Location = new System.Drawing.Point(14, 6);
            this.elements.Name = "elements";
            this.elements.Size = new System.Drawing.Size(50, 23);
            this.elements.TabIndex = 94;
            this.elements.Text = "88,00";
            this.elements.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Lime;
            this.label11.Location = new System.Drawing.Point(334, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 17);
            this.label11.TabIndex = 91;
            this.label11.Text = "Total";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tech
            // 
            this.tech.AutoSize = true;
            this.tech.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tech.ForeColor = System.Drawing.Color.White;
            this.tech.Location = new System.Drawing.Point(9, 29);
            this.tech.Name = "tech";
            this.tech.Size = new System.Drawing.Size(57, 17);
            this.tech.TabIndex = 93;
            this.tech.Text = "Base tech";
            this.tech.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(161, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 17);
            this.label12.TabIndex = 92;
            this.label12.Text = "Deductions";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Coral;
            this.label1.Location = new System.Drawing.Point(10, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 98;
            // 
            // next
            // 
            this.next.BackColor = System.Drawing.Color.DimGray;
            this.next.Cursor = System.Windows.Forms.Cursors.Hand;
            this.next.Dock = System.Windows.Forms.DockStyle.Right;
            this.next.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.next.FlatAppearance.BorderSize = 2;
            this.next.FlatAppearance.MouseOverBackColor = System.Drawing.Color.ForestGreen;
            this.next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.next.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.next.ForeColor = System.Drawing.Color.White;
            this.next.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.next.Location = new System.Drawing.Point(801, 0);
            this.next.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.next.Name = "next";
            this.next.Size = new System.Drawing.Size(74, 52);
            this.next.TabIndex = 97;
            this.next.Tag = "1";
            this.next.Text = "Show Results";
            this.next.UseVisualStyleBackColor = false;
            this.next.Click += new System.EventHandler(this.next_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lvQOE);
            this.panel1.Controls.Add(this.lvComp);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(879, 721);
            this.panel1.TabIndex = 2;
            // 
            // lvQOE
            // 
            this.lvQOE.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.lvQOE.BackColor = System.Drawing.Color.Black;
            this.lvQOE.BackgroundImageTiled = true;
            this.lvQOE.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.note,
            this.magg});
            this.lvQOE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvQOE.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvQOE.ForeColor = System.Drawing.Color.Cyan;
            this.lvQOE.FullRowSelect = true;
            this.lvQOE.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvQOE.Location = new System.Drawing.Point(0, 52);
            this.lvQOE.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.lvQOE.MultiSelect = false;
            this.lvQOE.Name = "lvQOE";
            this.lvQOE.ShowItemToolTips = true;
            this.lvQOE.Size = new System.Drawing.Size(875, 501);
            this.lvQOE.SmallImageList = this.imageList1;
            this.lvQOE.TabIndex = 96;
            this.lvQOE.UseCompatibleStateImageBehavior = false;
            this.lvQOE.View = System.Windows.Forms.View.Details;
            this.lvQOE.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.lvQOE_ColumnWidthChanging);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "";
            this.columnHeader1.Width = 47;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Element";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 86;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Value";
            this.columnHeader3.Width = 65;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Type";
            this.columnHeader4.Width = 104;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "CAT";
            this.columnHeader5.Width = 0;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "NUMCOMBO";
            this.columnHeader6.Width = 0;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "D/U";
            this.columnHeader7.Width = 0;
            // 
            // note
            // 
            this.note.Text = "Note";
            // 
            // magg
            // 
            this.magg.Text = "Time";
            this.magg.Width = 52;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "bullet_green.png");
            this.imageList1.Images.SetKeyName(1, "bullet_red.png");
            // 
            // lvComp
            // 
            this.lvComp.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.lvComp.BackColor = System.Drawing.Color.Black;
            this.lvComp.BackgroundImageTiled = true;
            this.lvComp.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10});
            this.lvComp.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lvComp.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvComp.ForeColor = System.Drawing.Color.Yellow;
            this.lvComp.FullRowSelect = true;
            this.lvComp.GridLines = true;
            this.lvComp.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvComp.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem5,
            listViewItem6,
            listViewItem7,
            listViewItem8});
            this.lvComp.Location = new System.Drawing.Point(0, 553);
            this.lvComp.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.lvComp.MultiSelect = false;
            this.lvComp.Name = "lvComp";
            this.lvComp.Scrollable = false;
            this.lvComp.ShowItemToolTips = true;
            this.lvComp.Size = new System.Drawing.Size(875, 164);
            this.lvComp.SmallImageList = this.imageList1;
            this.lvComp.TabIndex = 95;
            this.lvComp.UseCompatibleStateImageBehavior = false;
            this.lvComp.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "";
            this.columnHeader8.Width = 47;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "COMPONENTS";
            this.columnHeader9.Width = 296;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Final Value";
            this.columnHeader10.Width = 100;
            // 
            // CheckQoe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 721);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CheckQoe";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RollArt - Verify QOEs";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CheckQoe_FormClosing);
            this.Load += new System.EventHandler(this.CheckQoe_Load);
            this.Shown += new System.EventHandler(this.CheckQoe_Shown);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button next;
        private System.Windows.Forms.ListView lvQOE;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader note;
        private System.Windows.Forms.ColumnHeader magg;
        private System.Windows.Forms.ListView lvComp;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label artistic;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label elements2;
        private System.Windows.Forms.Label tech2;
        private System.Windows.Forms.Label total;
        private System.Windows.Forms.Label deductions;
        private System.Windows.Forms.Label elements;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label tech;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label competitor;
        private System.Windows.Forms.Label position;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label final;
        private System.Windows.Forms.Label label4;
    }
}
﻿using System;

namespace RollartSystemTech
{
    partial class CoppieForm
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CoppieForm));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.j1 = new System.Windows.Forms.Button();
            this.j2 = new System.Windows.Forms.Button();
            this.j3 = new System.Windows.Forms.Button();
            this.j4 = new System.Windows.Forms.Button();
            this.j5 = new System.Windows.Forms.Button();
            this.j6 = new System.Windows.Forms.Button();
            this.j7 = new System.Windows.Forms.Button();
            this.j8 = new System.Windows.Forms.Button();
            this.j9 = new System.Windows.Forms.Button();
            this.tp = new System.Windows.Forms.Button();
            this.startstop = new System.Windows.Forms.Button();
            this.ltimer = new System.Windows.Forms.Button();
            this.confirm = new System.Windows.Forms.Button();
            this.average = new System.Windows.Forms.Button();
            this.reset = new System.Windows.Forms.Button();
            this.skip = new System.Windows.Forms.Button();
            this.prev = new System.Windows.Forms.Button();
            this.info = new System.Windows.Forms.Button();
            this.quit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gbElements = new System.Windows.Forms.GroupBox();
            this.lv = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.note = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.magg = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.error = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cbVerify = new System.Windows.Forms.CheckBox();
            this.review = new System.Windows.Forms.Button();
            this.asterisco = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonNext = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.total = new System.Windows.Forms.Label();
            this.deductions = new System.Windows.Forms.Label();
            this.elements = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.labelPartial = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabLifts = new System.Windows.Forms.TabControl();
            this.tabLift1 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.el147 = new System.Windows.Forms.Button();
            this.el166 = new System.Windows.Forms.Button();
            this.label66 = new System.Windows.Forms.Label();
            this.el146 = new System.Windows.Forms.Button();
            this.el165 = new System.Windows.Forms.Button();
            this.el239 = new System.Windows.Forms.Button();
            this.el164 = new System.Windows.Forms.Button();
            this.el145 = new System.Windows.Forms.Button();
            this.label57 = new System.Windows.Forms.Label();
            this.el238 = new System.Windows.Forms.Button();
            this.el162 = new System.Windows.Forms.Button();
            this.label55 = new System.Windows.Forms.Label();
            this.el173 = new System.Windows.Forms.Button();
            this.el158 = new System.Windows.Forms.Button();
            this.el157 = new System.Windows.Forms.Button();
            this.el156 = new System.Windows.Forms.Button();
            this.el151 = new System.Windows.Forms.Button();
            this.el150 = new System.Windows.Forms.Button();
            this.el149 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.el154 = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.el172 = new System.Windows.Forms.Button();
            this.el161 = new System.Windows.Forms.Button();
            this.el160 = new System.Windows.Forms.Button();
            this.cbComboLift1 = new System.Windows.Forms.CheckBox();
            this.el167 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.tabLift2 = new System.Windows.Forms.TabPage();
            this.el805 = new System.Windows.Forms.Button();
            this.el804 = new System.Windows.Forms.Button();
            this.el803 = new System.Windows.Forms.Button();
            this.el802 = new System.Windows.Forms.Button();
            this.el801 = new System.Windows.Forms.Button();
            this.cbComboLift2 = new System.Windows.Forms.CheckBox();
            this.el200 = new System.Windows.Forms.Button();
            this.el195 = new System.Windows.Forms.Button();
            this.el190 = new System.Windows.Forms.Button();
            this.el185 = new System.Windows.Forms.Button();
            this.el180 = new System.Windows.Forms.Button();
            this.el199 = new System.Windows.Forms.Button();
            this.el198 = new System.Windows.Forms.Button();
            this.el197 = new System.Windows.Forms.Button();
            this.el196 = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.el194 = new System.Windows.Forms.Button();
            this.el193 = new System.Windows.Forms.Button();
            this.el192 = new System.Windows.Forms.Button();
            this.el191 = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.el189 = new System.Windows.Forms.Button();
            this.el184 = new System.Windows.Forms.Button();
            this.el179 = new System.Windows.Forms.Button();
            this.el188 = new System.Windows.Forms.Button();
            this.el187 = new System.Windows.Forms.Button();
            this.el186 = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.el183 = new System.Windows.Forms.Button();
            this.el182 = new System.Windows.Forms.Button();
            this.el181 = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.el178 = new System.Windows.Forms.Button();
            this.el177 = new System.Windows.Forms.Button();
            this.el176 = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.tabLift3 = new System.Windows.Forms.TabPage();
            this.cbComboLift3 = new System.Windows.Forms.CheckBox();
            this.el810 = new System.Windows.Forms.Button();
            this.el809 = new System.Windows.Forms.Button();
            this.el808 = new System.Windows.Forms.Button();
            this.el807 = new System.Windows.Forms.Button();
            this.el806 = new System.Windows.Forms.Button();
            this.el225 = new System.Windows.Forms.Button();
            this.el220 = new System.Windows.Forms.Button();
            this.el215 = new System.Windows.Forms.Button();
            this.el210 = new System.Windows.Forms.Button();
            this.el205 = new System.Windows.Forms.Button();
            this.el224 = new System.Windows.Forms.Button();
            this.el201 = new System.Windows.Forms.Button();
            this.el223 = new System.Windows.Forms.Button();
            this.el202 = new System.Windows.Forms.Button();
            this.el222 = new System.Windows.Forms.Button();
            this.el203 = new System.Windows.Forms.Button();
            this.el221 = new System.Windows.Forms.Button();
            this.el206 = new System.Windows.Forms.Button();
            this.el219 = new System.Windows.Forms.Button();
            this.el207 = new System.Windows.Forms.Button();
            this.el218 = new System.Windows.Forms.Button();
            this.el208 = new System.Windows.Forms.Button();
            this.el217 = new System.Windows.Forms.Button();
            this.el211 = new System.Windows.Forms.Button();
            this.el216 = new System.Windows.Forms.Button();
            this.el212 = new System.Windows.Forms.Button();
            this.el214 = new System.Windows.Forms.Button();
            this.el213 = new System.Windows.Forms.Button();
            this.el209 = new System.Windows.Forms.Button();
            this.el204 = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.imList = new System.Windows.Forms.ImageList(this.components);
            this.tabJ = new System.Windows.Forms.TabControl();
            this.tabJumps = new System.Windows.Forms.TabPage();
            this.el0 = new System.Windows.Forms.Button();
            this.half = new System.Windows.Forms.Button();
            this.cbCombo = new System.Windows.Forms.CheckBox();
            this.label40 = new System.Windows.Forms.Label();
            this.newCombo = new System.Windows.Forms.Button();
            this.el64 = new System.Windows.Forms.Button();
            this.under = new System.Windows.Forms.Button();
            this.down = new System.Windows.Forms.Button();
            this.el23 = new System.Windows.Forms.Button();
            this.el22 = new System.Windows.Forms.Button();
            this.el21 = new System.Windows.Forms.Button();
            this.el20 = new System.Windows.Forms.Button();
            this.el19 = new System.Windows.Forms.Button();
            this.el17 = new System.Windows.Forms.Button();
            this.el11 = new System.Windows.Forms.Button();
            this.el5 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.el16 = new System.Windows.Forms.Button();
            this.el10 = new System.Windows.Forms.Button();
            this.el4 = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.el15 = new System.Windows.Forms.Button();
            this.el9 = new System.Windows.Forms.Button();
            this.el3 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.el13 = new System.Windows.Forms.Button();
            this.el8 = new System.Windows.Forms.Button();
            this.el2 = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.el14 = new System.Windows.Forms.Button();
            this.el7 = new System.Windows.Forms.Button();
            this.el1 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.el18 = new System.Windows.Forms.Button();
            this.el12 = new System.Windows.Forms.Button();
            this.el6 = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.tabLanciati = new System.Windows.Forms.TabPage();
            this.el75 = new System.Windows.Forms.Button();
            this.half2 = new System.Windows.Forms.Button();
            this.under2 = new System.Windows.Forms.Button();
            this.down2 = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.el226 = new System.Windows.Forms.Button();
            this.el94 = new System.Windows.Forms.Button();
            this.el95 = new System.Windows.Forms.Button();
            this.el93 = new System.Windows.Forms.Button();
            this.el92 = new System.Windows.Forms.Button();
            this.el89 = new System.Windows.Forms.Button();
            this.el84 = new System.Windows.Forms.Button();
            this.el79 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.el90 = new System.Windows.Forms.Button();
            this.el85 = new System.Windows.Forms.Button();
            this.el80 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.el88 = new System.Windows.Forms.Button();
            this.el83 = new System.Windows.Forms.Button();
            this.el78 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.el87 = new System.Windows.Forms.Button();
            this.el82 = new System.Windows.Forms.Button();
            this.el77 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.el91 = new System.Windows.Forms.Button();
            this.el86 = new System.Windows.Forms.Button();
            this.el81 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.tabTwist = new System.Windows.Forms.TabPage();
            this.half3 = new System.Windows.Forms.Button();
            this.under3 = new System.Windows.Forms.Button();
            this.down3 = new System.Windows.Forms.Button();
            this.el102 = new System.Windows.Forms.Button();
            this.label65 = new System.Windows.Forms.Label();
            this.el118 = new System.Windows.Forms.Button();
            this.el113 = new System.Windows.Forms.Button();
            this.el108 = new System.Windows.Forms.Button();
            this.label48 = new System.Windows.Forms.Label();
            this.el122 = new System.Windows.Forms.Button();
            this.el121 = new System.Windows.Forms.Button();
            this.el119 = new System.Windows.Forms.Button();
            this.el120 = new System.Windows.Forms.Button();
            this.el117 = new System.Windows.Forms.Button();
            this.el116 = new System.Windows.Forms.Button();
            this.el115 = new System.Windows.Forms.Button();
            this.el114 = new System.Windows.Forms.Button();
            this.label47 = new System.Windows.Forms.Label();
            this.el112 = new System.Windows.Forms.Button();
            this.el111 = new System.Windows.Forms.Button();
            this.el110 = new System.Windows.Forms.Button();
            this.el109 = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.el107 = new System.Windows.Forms.Button();
            this.el106 = new System.Windows.Forms.Button();
            this.el105 = new System.Windows.Forms.Button();
            this.el104 = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.el103 = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.tabSpins = new System.Windows.Forms.TabControl();
            this.tabSpin = new System.Windows.Forms.TabPage();
            this.el26 = new System.Windows.Forms.Button();
            this.el41 = new System.Windows.Forms.Button();
            this.el46 = new System.Windows.Forms.Button();
            this.el50 = new System.Windows.Forms.Button();
            this.el51 = new System.Windows.Forms.Button();
            this.el52 = new System.Windows.Forms.Button();
            this.checkCombo1 = new JCS.ToggleSwitch();
            this.cbBonusSpin = new System.Windows.Forms.CheckBox();
            this.newComboSpin = new System.Windows.Forms.Button();
            this.cb49 = new System.Windows.Forms.ComboBox();
            this.cb25 = new System.Windows.Forms.ComboBox();
            this.cb47 = new System.Windows.Forms.ComboBox();
            this.cb40 = new System.Windows.Forms.ComboBox();
            this.cb48 = new System.Windows.Forms.ComboBox();
            this.cb45 = new System.Windows.Forms.ComboBox();
            this.el65 = new System.Windows.Forms.Button();
            this.el25 = new System.Windows.Forms.Button();
            this.el40 = new System.Windows.Forms.Button();
            this.el45 = new System.Windows.Forms.Button();
            this.el48 = new System.Windows.Forms.Button();
            this.el47 = new System.Windows.Forms.Button();
            this.el49 = new System.Windows.Forms.Button();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.tabCspin1 = new System.Windows.Forms.TabPage();
            this.cbBonusSpin2 = new System.Windows.Forms.CheckBox();
            this.checkCombo3 = new JCS.ToggleSwitch();
            this.newComboSpin2 = new System.Windows.Forms.Button();
            this.panelContactCombo = new System.Windows.Forms.Panel();
            this.el127 = new System.Windows.Forms.Button();
            this.cb126 = new System.Windows.Forms.ComboBox();
            this.el126 = new System.Windows.Forms.Button();
            this.el139 = new System.Windows.Forms.Button();
            this.el141 = new System.Windows.Forms.Button();
            this.el140 = new System.Windows.Forms.Button();
            this.el153 = new System.Windows.Forms.Button();
            this.el125 = new System.Windows.Forms.Button();
            this.el129 = new System.Windows.Forms.Button();
            this.el137 = new System.Windows.Forms.Button();
            this.el143 = new System.Windows.Forms.Button();
            this.el142 = new System.Windows.Forms.Button();
            this.cb163 = new System.Windows.Forms.ComboBox();
            this.cb159 = new System.Windows.Forms.ComboBox();
            this.cb155 = new System.Windows.Forms.ComboBox();
            this.cb152 = new System.Windows.Forms.ComboBox();
            this.cb148 = new System.Windows.Forms.ComboBox();
            this.cb144 = new System.Windows.Forms.ComboBox();
            this.cb136 = new System.Windows.Forms.ComboBox();
            this.cb128 = new System.Windows.Forms.ComboBox();
            this.cb124 = new System.Windows.Forms.ComboBox();
            this.el144 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.el148 = new System.Windows.Forms.Button();
            this.el163 = new System.Windows.Forms.Button();
            this.label71 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.el152 = new System.Windows.Forms.Button();
            this.el159 = new System.Windows.Forms.Button();
            this.label70 = new System.Windows.Forms.Label();
            this.el155 = new System.Windows.Forms.Button();
            this.el124 = new System.Windows.Forms.Button();
            this.label58 = new System.Windows.Forms.Label();
            this.el136 = new System.Windows.Forms.Button();
            this.label64 = new System.Windows.Forms.Label();
            this.el128 = new System.Windows.Forms.Button();
            this.label62 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.el65_ = new System.Windows.Forms.Button();
            this.tabVarie = new System.Windows.Forms.TabControl();
            this.tabSpiral = new System.Windows.Forms.TabPage();
            this.el97 = new System.Windows.Forms.Button();
            this.label75 = new System.Windows.Forms.Label();
            this.el96 = new System.Windows.Forms.Button();
            this.label74 = new System.Windows.Forms.Label();
            this.el101 = new System.Windows.Forms.Button();
            this.el100 = new System.Windows.Forms.Button();
            this.el99 = new System.Windows.Forms.Button();
            this.el98 = new System.Windows.Forms.Button();
            this.label73 = new System.Windows.Forms.Label();
            this.tabSteps = new System.Windows.Forms.TabPage();
            this.el73 = new System.Windows.Forms.Button();
            this.el76 = new System.Windows.Forms.Button();
            this.el63 = new System.Windows.Forms.Button();
            this.el59 = new System.Windows.Forms.Button();
            this.el60 = new System.Windows.Forms.Button();
            this.el61 = new System.Windows.Forms.Button();
            this.el62 = new System.Windows.Forms.Button();
            this.label56 = new System.Windows.Forms.Label();
            this.log = new System.Windows.Forms.TextBox();
            this.tabDeductions = new System.Windows.Forms.TabPage();
            this.ded5 = new System.Windows.Forms.NumericUpDown();
            this.label42 = new System.Windows.Forms.Label();
            this.ded3 = new System.Windows.Forms.NumericUpDown();
            this.ded4 = new System.Windows.Forms.NumericUpDown();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.ded2 = new System.Windows.Forms.NumericUpDown();
            this.label46 = new System.Windows.Forms.Label();
            this.ded1 = new System.Windows.Forms.NumericUpDown();
            this.tt2 = new System.Windows.Forms.ToolTip(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.gbElements.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabLifts.SuspendLayout();
            this.tabLift1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabLift2.SuspendLayout();
            this.tabLift3.SuspendLayout();
            this.tabJ.SuspendLayout();
            this.tabJumps.SuspendLayout();
            this.tabLanciati.SuspendLayout();
            this.tabTwist.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tabSpins.SuspendLayout();
            this.tabSpin.SuspendLayout();
            this.tabCspin1.SuspendLayout();
            this.panelContactCombo.SuspendLayout();
            this.tabVarie.SuspendLayout();
            this.tabSpiral.SuspendLayout();
            this.tabSteps.SuspendLayout();
            this.tabDeductions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ded5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded1)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.65306F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.69388F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.65306F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 2, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1213, 693);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.flowLayoutPanel1, 3);
            this.flowLayoutPanel1.Controls.Add(this.j1);
            this.flowLayoutPanel1.Controls.Add(this.j2);
            this.flowLayoutPanel1.Controls.Add(this.j3);
            this.flowLayoutPanel1.Controls.Add(this.j4);
            this.flowLayoutPanel1.Controls.Add(this.j5);
            this.flowLayoutPanel1.Controls.Add(this.j6);
            this.flowLayoutPanel1.Controls.Add(this.j7);
            this.flowLayoutPanel1.Controls.Add(this.j8);
            this.flowLayoutPanel1.Controls.Add(this.j9);
            this.flowLayoutPanel1.Controls.Add(this.tp);
            this.flowLayoutPanel1.Controls.Add(this.startstop);
            this.flowLayoutPanel1.Controls.Add(this.ltimer);
            this.flowLayoutPanel1.Controls.Add(this.confirm);
            this.flowLayoutPanel1.Controls.Add(this.average);
            this.flowLayoutPanel1.Controls.Add(this.reset);
            this.flowLayoutPanel1.Controls.Add(this.skip);
            this.flowLayoutPanel1.Controls.Add(this.prev);
            this.flowLayoutPanel1.Controls.Add(this.info);
            this.flowLayoutPanel1.Controls.Add(this.quit);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1209, 45);
            this.flowLayoutPanel1.TabIndex = 9;
            // 
            // j1
            // 
            this.j1.AutoEllipsis = true;
            this.j1.BackColor = System.Drawing.Color.Transparent;
            this.j1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j1.Cursor = System.Windows.Forms.Cursors.Default;
            this.j1.Dock = System.Windows.Forms.DockStyle.Top;
            this.j1.Enabled = false;
            this.j1.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j1.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j1.ForeColor = System.Drawing.Color.Gray;
            this.j1.Location = new System.Drawing.Point(0, 0);
            this.j1.Margin = new System.Windows.Forms.Padding(0);
            this.j1.Name = "j1";
            this.j1.Size = new System.Drawing.Size(38, 40);
            this.j1.TabIndex = 76;
            this.j1.Tag = "1";
            this.j1.Text = "J1";
            this.j1.UseVisualStyleBackColor = false;
            this.j1.Click += new System.EventHandler(this.j1_Click);
            this.j1.MouseEnter += new System.EventHandler(this.j1_MouseEnter);
            // 
            // j2
            // 
            this.j2.BackColor = System.Drawing.Color.Transparent;
            this.j2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j2.Cursor = System.Windows.Forms.Cursors.Default;
            this.j2.Dock = System.Windows.Forms.DockStyle.Top;
            this.j2.Enabled = false;
            this.j2.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j2.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j2.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j2.ForeColor = System.Drawing.Color.Gray;
            this.j2.Location = new System.Drawing.Point(38, 0);
            this.j2.Margin = new System.Windows.Forms.Padding(0);
            this.j2.Name = "j2";
            this.j2.Size = new System.Drawing.Size(38, 40);
            this.j2.TabIndex = 77;
            this.j2.Tag = "2";
            this.j2.Text = "J2";
            this.j2.UseVisualStyleBackColor = false;
            this.j2.Click += new System.EventHandler(this.j1_Click);
            this.j2.MouseEnter += new System.EventHandler(this.j2_MouseEnter);
            // 
            // j3
            // 
            this.j3.BackColor = System.Drawing.Color.Transparent;
            this.j3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j3.Cursor = System.Windows.Forms.Cursors.Default;
            this.j3.Dock = System.Windows.Forms.DockStyle.Top;
            this.j3.Enabled = false;
            this.j3.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j3.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j3.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j3.ForeColor = System.Drawing.Color.Gray;
            this.j3.Location = new System.Drawing.Point(76, 0);
            this.j3.Margin = new System.Windows.Forms.Padding(0);
            this.j3.Name = "j3";
            this.j3.Size = new System.Drawing.Size(38, 40);
            this.j3.TabIndex = 78;
            this.j3.Tag = "3";
            this.j3.Text = "J3";
            this.j3.UseVisualStyleBackColor = false;
            this.j3.Click += new System.EventHandler(this.j1_Click);
            this.j3.MouseEnter += new System.EventHandler(this.j3_MouseEnter);
            // 
            // j4
            // 
            this.j4.BackColor = System.Drawing.Color.Transparent;
            this.j4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j4.Cursor = System.Windows.Forms.Cursors.Default;
            this.j4.Dock = System.Windows.Forms.DockStyle.Top;
            this.j4.Enabled = false;
            this.j4.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j4.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j4.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j4.ForeColor = System.Drawing.Color.Gray;
            this.j4.Location = new System.Drawing.Point(114, 0);
            this.j4.Margin = new System.Windows.Forms.Padding(0);
            this.j4.Name = "j4";
            this.j4.Size = new System.Drawing.Size(38, 40);
            this.j4.TabIndex = 79;
            this.j4.Tag = "1";
            this.j4.Text = "J4";
            this.j4.UseVisualStyleBackColor = false;
            this.j4.Click += new System.EventHandler(this.j1_Click);
            this.j4.MouseEnter += new System.EventHandler(this.j4_MouseEnter);
            // 
            // j5
            // 
            this.j5.BackColor = System.Drawing.Color.Transparent;
            this.j5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j5.Cursor = System.Windows.Forms.Cursors.Default;
            this.j5.Dock = System.Windows.Forms.DockStyle.Top;
            this.j5.Enabled = false;
            this.j5.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j5.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j5.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j5.ForeColor = System.Drawing.Color.Gray;
            this.j5.Location = new System.Drawing.Point(152, 0);
            this.j5.Margin = new System.Windows.Forms.Padding(0);
            this.j5.Name = "j5";
            this.j5.Size = new System.Drawing.Size(38, 40);
            this.j5.TabIndex = 80;
            this.j5.Tag = "2";
            this.j5.Text = "J5";
            this.j5.UseVisualStyleBackColor = false;
            this.j5.Click += new System.EventHandler(this.j1_Click);
            this.j5.MouseEnter += new System.EventHandler(this.j5_MouseEnter);
            // 
            // j6
            // 
            this.j6.BackColor = System.Drawing.Color.Transparent;
            this.j6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j6.Cursor = System.Windows.Forms.Cursors.Default;
            this.j6.Dock = System.Windows.Forms.DockStyle.Top;
            this.j6.Enabled = false;
            this.j6.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j6.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j6.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j6.ForeColor = System.Drawing.Color.Gray;
            this.j6.Location = new System.Drawing.Point(190, 0);
            this.j6.Margin = new System.Windows.Forms.Padding(0);
            this.j6.Name = "j6";
            this.j6.Size = new System.Drawing.Size(38, 40);
            this.j6.TabIndex = 81;
            this.j6.Tag = "3";
            this.j6.Text = "J6";
            this.j6.UseVisualStyleBackColor = false;
            this.j6.Click += new System.EventHandler(this.j1_Click);
            this.j6.MouseEnter += new System.EventHandler(this.j6_MouseEnter);
            // 
            // j7
            // 
            this.j7.BackColor = System.Drawing.Color.Transparent;
            this.j7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j7.Cursor = System.Windows.Forms.Cursors.Default;
            this.j7.Dock = System.Windows.Forms.DockStyle.Top;
            this.j7.Enabled = false;
            this.j7.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j7.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j7.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j7.ForeColor = System.Drawing.Color.Gray;
            this.j7.Location = new System.Drawing.Point(228, 0);
            this.j7.Margin = new System.Windows.Forms.Padding(0);
            this.j7.Name = "j7";
            this.j7.Size = new System.Drawing.Size(38, 40);
            this.j7.TabIndex = 82;
            this.j7.Tag = "1";
            this.j7.Text = "J7";
            this.j7.UseVisualStyleBackColor = false;
            this.j7.Click += new System.EventHandler(this.j1_Click);
            this.j7.MouseEnter += new System.EventHandler(this.j7_MouseEnter);
            // 
            // j8
            // 
            this.j8.BackColor = System.Drawing.Color.Transparent;
            this.j8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j8.Cursor = System.Windows.Forms.Cursors.Default;
            this.j8.Dock = System.Windows.Forms.DockStyle.Top;
            this.j8.Enabled = false;
            this.j8.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j8.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j8.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j8.ForeColor = System.Drawing.Color.Gray;
            this.j8.Location = new System.Drawing.Point(266, 0);
            this.j8.Margin = new System.Windows.Forms.Padding(0);
            this.j8.Name = "j8";
            this.j8.Size = new System.Drawing.Size(38, 40);
            this.j8.TabIndex = 83;
            this.j8.Tag = "2";
            this.j8.Text = "J8";
            this.j8.UseVisualStyleBackColor = false;
            this.j8.Click += new System.EventHandler(this.j1_Click);
            this.j8.MouseEnter += new System.EventHandler(this.j8_MouseEnter);
            // 
            // j9
            // 
            this.j9.BackColor = System.Drawing.Color.Transparent;
            this.j9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j9.Cursor = System.Windows.Forms.Cursors.Default;
            this.j9.Dock = System.Windows.Forms.DockStyle.Top;
            this.j9.Enabled = false;
            this.j9.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j9.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j9.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j9.ForeColor = System.Drawing.Color.Gray;
            this.j9.Location = new System.Drawing.Point(304, 0);
            this.j9.Margin = new System.Windows.Forms.Padding(0);
            this.j9.Name = "j9";
            this.j9.Size = new System.Drawing.Size(38, 40);
            this.j9.TabIndex = 84;
            this.j9.Tag = "3";
            this.j9.Text = "J9";
            this.j9.UseVisualStyleBackColor = false;
            this.j9.Click += new System.EventHandler(this.j1_Click);
            this.j9.MouseEnter += new System.EventHandler(this.j9_MouseEnter);
            // 
            // tp
            // 
            this.tp.AutoSize = true;
            this.tp.BackColor = System.Drawing.Color.Transparent;
            this.tp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tp.Cursor = System.Windows.Forms.Cursors.Default;
            this.tp.Dock = System.Windows.Forms.DockStyle.Top;
            this.tp.FlatAppearance.BorderColor = System.Drawing.Color.Aqua;
            this.tp.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.tp.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.tp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.tp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tp.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp.ForeColor = System.Drawing.Color.Aqua;
            this.tp.Location = new System.Drawing.Point(344, 0);
            this.tp.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.tp.Name = "tp";
            this.tp.Size = new System.Drawing.Size(45, 40);
            this.tp.TabIndex = 88;
            this.tp.Tag = "3";
            this.tp.Text = "R";
            this.tp.UseVisualStyleBackColor = false;
            this.tp.Visible = false;
            // 
            // startstop
            // 
            this.startstop.BackColor = System.Drawing.Color.DimGray;
            this.startstop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.startstop.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.startstop.FlatAppearance.BorderSize = 2;
            this.startstop.FlatAppearance.MouseDownBackColor = System.Drawing.Color.YellowGreen;
            this.startstop.FlatAppearance.MouseOverBackColor = System.Drawing.Color.ForestGreen;
            this.startstop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.startstop.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startstop.ForeColor = System.Drawing.Color.White;
            this.startstop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.startstop.Location = new System.Drawing.Point(391, 0);
            this.startstop.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.startstop.Name = "startstop";
            this.startstop.Size = new System.Drawing.Size(84, 40);
            this.startstop.TabIndex = 90;
            this.startstop.Text = "START";
            this.startstop.UseVisualStyleBackColor = false;
            this.startstop.Click += new System.EventHandler(this.startstop_Click);
            // 
            // ltimer
            // 
            this.ltimer.BackColor = System.Drawing.Color.Transparent;
            this.ltimer.Cursor = System.Windows.Forms.Cursors.Default;
            this.ltimer.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.ltimer.FlatAppearance.BorderSize = 2;
            this.ltimer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ltimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ltimer.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ltimer.ForeColor = System.Drawing.Color.Magenta;
            this.ltimer.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.ltimer.Location = new System.Drawing.Point(477, 0);
            this.ltimer.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.ltimer.Name = "ltimer";
            this.ltimer.Size = new System.Drawing.Size(80, 40);
            this.ltimer.TabIndex = 98;
            this.ltimer.Text = "00:00";
            this.ltimer.UseVisualStyleBackColor = false;
            // 
            // confirm
            // 
            this.confirm.BackColor = System.Drawing.Color.DimGray;
            this.confirm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.confirm.Enabled = false;
            this.confirm.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.confirm.FlatAppearance.BorderSize = 2;
            this.confirm.FlatAppearance.MouseOverBackColor = System.Drawing.Color.ForestGreen;
            this.confirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.confirm.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirm.ForeColor = System.Drawing.Color.White;
            this.confirm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.confirm.Location = new System.Drawing.Point(559, 0);
            this.confirm.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.confirm.Name = "confirm";
            this.confirm.Size = new System.Drawing.Size(127, 40);
            this.confirm.TabIndex = 95;
            this.confirm.Text = "CONFIRM";
            this.confirm.UseVisualStyleBackColor = false;
            this.confirm.Click += new System.EventHandler(this.button2_Click);
            // 
            // average
            // 
            this.average.BackColor = System.Drawing.Color.DimGray;
            this.average.Cursor = System.Windows.Forms.Cursors.Hand;
            this.average.Enabled = false;
            this.average.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.average.FlatAppearance.BorderSize = 2;
            this.average.FlatAppearance.MouseOverBackColor = System.Drawing.Color.ForestGreen;
            this.average.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.average.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.average.ForeColor = System.Drawing.Color.White;
            this.average.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.average.Location = new System.Drawing.Point(688, 0);
            this.average.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.average.Name = "average";
            this.average.Size = new System.Drawing.Size(142, 40);
            this.average.TabIndex = 110;
            this.average.Text = "AVERAGE";
            this.average.UseVisualStyleBackColor = false;
            this.average.Click += new System.EventHandler(this.average_Click);
            // 
            // reset
            // 
            this.reset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.reset.Cursor = System.Windows.Forms.Cursors.Hand;
            this.reset.Enabled = false;
            this.reset.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.reset.FlatAppearance.BorderSize = 2;
            this.reset.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.reset.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.reset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.reset.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reset.ForeColor = System.Drawing.Color.White;
            this.reset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.reset.Location = new System.Drawing.Point(832, 0);
            this.reset.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.reset.Name = "reset";
            this.reset.Size = new System.Drawing.Size(97, 40);
            this.reset.TabIndex = 96;
            this.reset.Text = "RESET";
            this.reset.UseVisualStyleBackColor = false;
            this.reset.Click += new System.EventHandler(this.next_Click);
            // 
            // skip
            // 
            this.skip.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.skip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.skip.Cursor = System.Windows.Forms.Cursors.Hand;
            this.skip.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.skip.FlatAppearance.BorderSize = 2;
            this.skip.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Navy;
            this.skip.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Navy;
            this.skip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.skip.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.skip.ForeColor = System.Drawing.Color.White;
            this.skip.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.skip.Location = new System.Drawing.Point(931, 0);
            this.skip.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.skip.Name = "skip";
            this.skip.Size = new System.Drawing.Size(64, 40);
            this.skip.TabIndex = 107;
            this.skip.Text = "SKIP";
            this.tt2.SetToolTip(this.skip, "Skip to the next skater");
            this.skip.UseVisualStyleBackColor = false;
            this.skip.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // prev
            // 
            this.prev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prev.BackColor = System.Drawing.Color.White;
            this.prev.Cursor = System.Windows.Forms.Cursors.Hand;
            this.prev.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.prev.FlatAppearance.BorderSize = 2;
            this.prev.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.prev.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.prev.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.prev.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prev.ForeColor = System.Drawing.Color.White;
            this.prev.Image = global::RollartSystemTech.Properties.Resources.display;
            this.prev.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.prev.Location = new System.Drawing.Point(997, 0);
            this.prev.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.prev.Name = "prev";
            this.prev.Size = new System.Drawing.Size(49, 40);
            this.prev.TabIndex = 111;
            this.tt2.SetToolTip(this.prev, "Display the previous skater score");
            this.prev.UseVisualStyleBackColor = false;
            this.prev.Click += new System.EventHandler(this.prev_Click);
            // 
            // info
            // 
            this.info.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.info.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.info.Cursor = System.Windows.Forms.Cursors.Hand;
            this.info.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.info.FlatAppearance.BorderSize = 2;
            this.info.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Purple;
            this.info.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Purple;
            this.info.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.info.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.info.ForeColor = System.Drawing.Color.White;
            this.info.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.info.Location = new System.Drawing.Point(1048, 0);
            this.info.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(62, 40);
            this.info.TabIndex = 106;
            this.info.Text = "INFO";
            this.tt2.SetToolTip(this.info, "Event Info");
            this.info.UseVisualStyleBackColor = false;
            this.info.Click += new System.EventHandler(this.button1_Click_3);
            // 
            // quit
            // 
            this.quit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.quit.BackColor = System.Drawing.Color.Red;
            this.quit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.quit.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.quit.FlatAppearance.BorderSize = 2;
            this.quit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.quit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Maroon;
            this.quit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.quit.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quit.ForeColor = System.Drawing.Color.White;
            this.quit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.quit.Location = new System.Drawing.Point(1112, 0);
            this.quit.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.quit.Name = "quit";
            this.quit.Size = new System.Drawing.Size(64, 40);
            this.quit.TabIndex = 108;
            this.quit.Text = "QUIT";
            this.quit.UseVisualStyleBackColor = false;
            this.quit.Click += new System.EventHandler(this.button2_Click_2);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.gbElements);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(398, 49);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(416, 594);
            this.panel1.TabIndex = 3;
            // 
            // gbElements
            // 
            this.gbElements.Controls.Add(this.lv);
            this.gbElements.Controls.Add(this.error);
            this.gbElements.Controls.Add(this.panel3);
            this.gbElements.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbElements.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbElements.Location = new System.Drawing.Point(0, 0);
            this.gbElements.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbElements.Name = "gbElements";
            this.gbElements.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbElements.Size = new System.Drawing.Size(416, 594);
            this.gbElements.TabIndex = 1;
            this.gbElements.TabStop = false;
            // 
            // lv
            // 
            this.lv.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.lv.BackColor = System.Drawing.Color.Black;
            this.lv.BackgroundImageTiled = true;
            this.lv.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.note,
            this.magg});
            this.lv.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lv.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv.ForeColor = System.Drawing.Color.White;
            this.lv.FullRowSelect = true;
            this.lv.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lv.Location = new System.Drawing.Point(3, 24);
            this.lv.Margin = new System.Windows.Forms.Padding(2);
            this.lv.MultiSelect = false;
            this.lv.Name = "lv";
            this.lv.ShowItemToolTips = true;
            this.lv.Size = new System.Drawing.Size(410, 475);
            this.lv.SmallImageList = this.imageList1;
            this.lv.TabIndex = 95;
            this.lv.UseCompatibleStateImageBehavior = false;
            this.lv.View = System.Windows.Forms.View.Details;
            this.lv.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.lv_ColumnWidthChanging);
            this.lv.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lv_MouseClick);
            this.lv.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lv_MouseDoubleClick);
            this.lv.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lv_MouseDown);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Element";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 81;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Value";
            this.columnHeader3.Width = 53;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Type";
            this.columnHeader4.Width = 120;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "CAT";
            this.columnHeader5.Width = 0;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "NUMCOMBO";
            this.columnHeader6.Width = 0;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "D/U";
            this.columnHeader7.Width = 0;
            // 
            // note
            // 
            this.note.Text = "Note";
            this.note.Width = 80;
            // 
            // magg
            // 
            this.magg.Text = "Time";
            this.magg.Width = 0;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "bullet_green.png");
            this.imageList1.Images.SetKeyName(1, "bullet_red.png");
            this.imageList1.Images.SetKeyName(2, "bullet_yellow.png");
            // 
            // error
            // 
            this.error.AutoEllipsis = true;
            this.error.BackColor = System.Drawing.Color.Yellow;
            this.error.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.error.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.error.Font = new System.Drawing.Font("Arial Narrow", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.error.ForeColor = System.Drawing.Color.Red;
            this.error.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.error.Location = new System.Drawing.Point(3, 499);
            this.error.Name = "error";
            this.error.Size = new System.Drawing.Size(410, 56);
            this.error.TabIndex = 94;
            this.error.Text = "error test";
            this.error.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.error.Visible = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.cbVerify);
            this.panel3.Controls.Add(this.review);
            this.panel3.Controls.Add(this.asterisco);
            this.panel3.Controls.Add(this.cancel);
            this.panel3.Controls.Add(this.back);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(3, 555);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(410, 37);
            this.panel3.TabIndex = 92;
            // 
            // cbVerify
            // 
            this.cbVerify.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbVerify.BackColor = System.Drawing.Color.Gainsboro;
            this.cbVerify.Checked = true;
            this.cbVerify.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbVerify.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbVerify.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbVerify.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.cbVerify.FlatAppearance.BorderSize = 2;
            this.cbVerify.FlatAppearance.CheckedBackColor = System.Drawing.Color.Lime;
            this.cbVerify.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Yellow;
            this.cbVerify.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Lime;
            this.cbVerify.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbVerify.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVerify.ForeColor = System.Drawing.Color.Black;
            this.cbVerify.Location = new System.Drawing.Point(210, 0);
            this.cbVerify.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbVerify.Name = "cbVerify";
            this.cbVerify.Size = new System.Drawing.Size(84, 37);
            this.cbVerify.TabIndex = 177;
            this.cbVerify.Text = "Check";
            this.cbVerify.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbVerify.UseVisualStyleBackColor = false;
            this.cbVerify.CheckedChanged += new System.EventHandler(this.cbVerify_CheckedChanged);
            // 
            // review
            // 
            this.review.BackColor = System.Drawing.Color.DarkRed;
            this.review.Cursor = System.Windows.Forms.Cursors.Hand;
            this.review.Dock = System.Windows.Forms.DockStyle.Right;
            this.review.Enabled = false;
            this.review.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.review.FlatAppearance.BorderSize = 2;
            this.review.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.review.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.review.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.review.ForeColor = System.Drawing.Color.Yellow;
            this.review.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.review.Location = new System.Drawing.Point(294, 0);
            this.review.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.review.Name = "review";
            this.review.Size = new System.Drawing.Size(82, 37);
            this.review.TabIndex = 178;
            this.review.Text = "Review";
            this.review.UseVisualStyleBackColor = false;
            this.review.Click += new System.EventHandler(this.edit_Click);
            // 
            // asterisco
            // 
            this.asterisco.BackColor = System.Drawing.Color.Blue;
            this.asterisco.Cursor = System.Windows.Forms.Cursors.Hand;
            this.asterisco.Dock = System.Windows.Forms.DockStyle.Right;
            this.asterisco.Enabled = false;
            this.asterisco.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.asterisco.FlatAppearance.BorderSize = 2;
            this.asterisco.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.asterisco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.asterisco.Font = new System.Drawing.Font("Arial Narrow", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.asterisco.ForeColor = System.Drawing.Color.Yellow;
            this.asterisco.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.asterisco.Location = new System.Drawing.Point(376, 0);
            this.asterisco.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.asterisco.Name = "asterisco";
            this.asterisco.Size = new System.Drawing.Size(34, 37);
            this.asterisco.TabIndex = 180;
            this.asterisco.Text = "*";
            this.tt2.SetToolTip(this.asterisco, "Add/Remove * to the selected element");
            this.asterisco.UseVisualStyleBackColor = false;
            this.asterisco.Click += new System.EventHandler(this.asterisco_Click);
            // 
            // cancel
            // 
            this.cancel.BackColor = System.Drawing.Color.DimGray;
            this.cancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cancel.Dock = System.Windows.Forms.DockStyle.Left;
            this.cancel.Enabled = false;
            this.cancel.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.cancel.FlatAppearance.BorderSize = 2;
            this.cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancel.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancel.ForeColor = System.Drawing.Color.White;
            this.cancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cancel.Location = new System.Drawing.Point(117, 0);
            this.cancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(93, 37);
            this.cancel.TabIndex = 96;
            this.cancel.Text = "Cancel";
            this.cancel.UseVisualStyleBackColor = false;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // back
            // 
            this.back.BackColor = System.Drawing.Color.DimGray;
            this.back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.back.Dock = System.Windows.Forms.DockStyle.Left;
            this.back.Enabled = false;
            this.back.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.back.FlatAppearance.BorderSize = 2;
            this.back.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.back.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.back.ForeColor = System.Drawing.Color.White;
            this.back.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.back.Location = new System.Drawing.Point(0, 0);
            this.back.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(117, 37);
            this.back.TabIndex = 94;
            this.back.Text = "Delete Last";
            this.back.UseVisualStyleBackColor = false;
            this.back.Click += new System.EventHandler(this.clear_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 3);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 397F));
            this.tableLayoutPanel2.Controls.Add(this.buttonNext, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel5, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelPartial, 4, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 647);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel1.SetRowSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1207, 44);
            this.tableLayoutPanel2.TabIndex = 10;
            // 
            // buttonNext
            // 
            this.buttonNext.AutoEllipsis = true;
            this.buttonNext.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonNext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.buttonNext.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonNext.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.buttonNext.FlatAppearance.BorderSize = 0;
            this.buttonNext.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonNext.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNext.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNext.ForeColor = System.Drawing.Color.Yellow;
            this.buttonNext.Location = new System.Drawing.Point(394, 0);
            this.buttonNext.Margin = new System.Windows.Forms.Padding(0);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(617, 42);
            this.buttonNext.TabIndex = 105;
            this.buttonNext.Text = "1/100 - Pair Name 1 - Pair Name 2";
            this.buttonNext.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonNext.UseVisualStyleBackColor = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.panel5.Controls.Add(this.total);
            this.panel5.Controls.Add(this.deductions);
            this.panel5.Controls.Add(this.elements);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Location = new System.Drawing.Point(3, 2);
            this.panel5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(388, 37);
            this.panel5.TabIndex = 101;
            // 
            // total
            // 
            this.total.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.total.AutoSize = true;
            this.total.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total.ForeColor = System.Drawing.Color.Lime;
            this.total.Location = new System.Drawing.Point(165, 2);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(41, 23);
            this.total.TabIndex = 96;
            this.total.Text = "0.00";
            this.total.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // deductions
            // 
            this.deductions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.deductions.AutoSize = true;
            this.deductions.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deductions.ForeColor = System.Drawing.Color.White;
            this.deductions.Location = new System.Drawing.Point(96, 2);
            this.deductions.Name = "deductions";
            this.deductions.Size = new System.Drawing.Size(41, 23);
            this.deductions.TabIndex = 95;
            this.deductions.Text = "0.00";
            this.deductions.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // elements
            // 
            this.elements.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.elements.AutoSize = true;
            this.elements.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.elements.ForeColor = System.Drawing.Color.White;
            this.elements.Location = new System.Drawing.Point(19, 2);
            this.elements.Name = "elements";
            this.elements.Size = new System.Drawing.Size(41, 23);
            this.elements.TabIndex = 94;
            this.elements.Text = "0.00";
            this.elements.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Lime;
            this.label11.Location = new System.Drawing.Point(168, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 17);
            this.label11.TabIndex = 91;
            this.label11.Text = "Total";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(4, 21);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(69, 16);
            this.label13.TabIndex = 93;
            this.label13.Text = "Base technical";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(89, 21);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 16);
            this.label12.TabIndex = 92;
            this.label12.Text = "Deductions";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPartial
            // 
            this.labelPartial.AutoSize = true;
            this.labelPartial.BackColor = System.Drawing.Color.Transparent;
            this.labelPartial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelPartial.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.labelPartial.ForeColor = System.Drawing.Color.Aqua;
            this.labelPartial.Location = new System.Drawing.Point(1013, 0);
            this.labelPartial.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPartial.Name = "labelPartial";
            this.labelPartial.Size = new System.Drawing.Size(103, 40);
            this.labelPartial.TabIndex = 231;
            this.labelPartial.Tag = "6";
            this.labelPartial.Text = "Partial RANK:\r\nPartial SCORE:";
            this.labelPartial.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabLifts);
            this.panel2.Controls.Add(this.tabJ);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 49);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(390, 594);
            this.panel2.TabIndex = 11;
            // 
            // tabLifts
            // 
            this.tabLifts.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabLifts.Controls.Add(this.tabLift1);
            this.tabLifts.Controls.Add(this.tabLift2);
            this.tabLifts.Controls.Add(this.tabLift3);
            this.tabLifts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabLifts.Enabled = false;
            this.tabLifts.Font = new System.Drawing.Font("Arial Narrow", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabLifts.ImageList = this.imList;
            this.tabLifts.ItemSize = new System.Drawing.Size(80, 40);
            this.tabLifts.Location = new System.Drawing.Point(0, 296);
            this.tabLifts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabLifts.Name = "tabLifts";
            this.tabLifts.SelectedIndex = 0;
            this.tabLifts.Size = new System.Drawing.Size(390, 298);
            this.tabLifts.TabIndex = 1;
            // 
            // tabLift1
            // 
            this.tabLift1.AutoScroll = true;
            this.tabLift1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.tabLift1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabLift1.Controls.Add(this.panel4);
            this.tabLift1.Controls.Add(this.cbComboLift1);
            this.tabLift1.Controls.Add(this.el167);
            this.tabLift1.Controls.Add(this.label8);
            this.tabLift1.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabLift1.Location = new System.Drawing.Point(4, 44);
            this.tabLift1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabLift1.Name = "tabLift1";
            this.tabLift1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabLift1.Size = new System.Drawing.Size(382, 250);
            this.tabLift1.TabIndex = 0;
            this.tabLift1.Text = "Lifts 1";
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.AutoScroll = true;
            this.panel4.Controls.Add(this.el147);
            this.panel4.Controls.Add(this.el166);
            this.panel4.Controls.Add(this.label66);
            this.panel4.Controls.Add(this.el146);
            this.panel4.Controls.Add(this.el165);
            this.panel4.Controls.Add(this.el239);
            this.panel4.Controls.Add(this.el164);
            this.panel4.Controls.Add(this.el145);
            this.panel4.Controls.Add(this.label57);
            this.panel4.Controls.Add(this.el238);
            this.panel4.Controls.Add(this.el162);
            this.panel4.Controls.Add(this.label55);
            this.panel4.Controls.Add(this.el173);
            this.panel4.Controls.Add(this.el158);
            this.panel4.Controls.Add(this.el157);
            this.panel4.Controls.Add(this.el156);
            this.panel4.Controls.Add(this.el151);
            this.panel4.Controls.Add(this.el150);
            this.panel4.Controls.Add(this.el149);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Controls.Add(this.el154);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.el172);
            this.panel4.Controls.Add(this.el161);
            this.panel4.Controls.Add(this.el160);
            this.panel4.Location = new System.Drawing.Point(16, 48);
            this.panel4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(358, 163);
            this.panel4.TabIndex = 178;
            // 
            // el147
            // 
            this.el147.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el147.BackColor = System.Drawing.Color.Transparent;
            this.el147.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el147.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el147.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el147.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el147.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el147.ForeColor = System.Drawing.Color.Yellow;
            this.el147.Location = new System.Drawing.Point(316, 130);
            this.el147.Margin = new System.Windows.Forms.Padding(0);
            this.el147.Name = "el147";
            this.el147.Size = new System.Drawing.Size(24, 28);
            this.el147.TabIndex = 173;
            this.el147.Text = "4";
            this.el147.UseVisualStyleBackColor = false;
            this.el147.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el166
            // 
            this.el166.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el166.BackColor = System.Drawing.Color.Transparent;
            this.el166.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el166.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el166.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el166.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el166.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el166.ForeColor = System.Drawing.Color.Yellow;
            this.el166.Location = new System.Drawing.Point(316, 98);
            this.el166.Margin = new System.Windows.Forms.Padding(0);
            this.el166.Name = "el166";
            this.el166.Size = new System.Drawing.Size(24, 28);
            this.el166.TabIndex = 168;
            this.el166.Text = "4";
            this.el166.UseVisualStyleBackColor = false;
            this.el166.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // label66
            // 
            this.label66.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label66.BackColor = System.Drawing.Color.Transparent;
            this.label66.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label66.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label66.Location = new System.Drawing.Point(14, 128);
            this.label66.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(206, 32);
            this.label66.TabIndex = 169;
            this.label66.Tag = "6";
            this.label66.Text = "LOW MILATANO";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el146
            // 
            this.el146.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el146.BackColor = System.Drawing.Color.Transparent;
            this.el146.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el146.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el146.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el146.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el146.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el146.ForeColor = System.Drawing.Color.Yellow;
            this.el146.Location = new System.Drawing.Point(287, 130);
            this.el146.Margin = new System.Windows.Forms.Padding(0);
            this.el146.Name = "el146";
            this.el146.Size = new System.Drawing.Size(24, 28);
            this.el146.TabIndex = 172;
            this.el146.Text = "3";
            this.el146.UseVisualStyleBackColor = false;
            this.el146.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el165
            // 
            this.el165.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el165.BackColor = System.Drawing.Color.Transparent;
            this.el165.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el165.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el165.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el165.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el165.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el165.ForeColor = System.Drawing.Color.Yellow;
            this.el165.Location = new System.Drawing.Point(287, 98);
            this.el165.Margin = new System.Windows.Forms.Padding(0);
            this.el165.Name = "el165";
            this.el165.Size = new System.Drawing.Size(24, 28);
            this.el165.TabIndex = 167;
            this.el165.Text = "3";
            this.el165.UseVisualStyleBackColor = false;
            this.el165.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el239
            // 
            this.el239.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el239.BackColor = System.Drawing.Color.Transparent;
            this.el239.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el239.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el239.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el239.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el239.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el239.ForeColor = System.Drawing.Color.Yellow;
            this.el239.Location = new System.Drawing.Point(231, 130);
            this.el239.Margin = new System.Windows.Forms.Padding(0);
            this.el239.Name = "el239";
            this.el239.Size = new System.Drawing.Size(24, 28);
            this.el239.TabIndex = 170;
            this.el239.Text = "1";
            this.el239.UseVisualStyleBackColor = false;
            this.el239.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el164
            // 
            this.el164.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el164.BackColor = System.Drawing.Color.Transparent;
            this.el164.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el164.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el164.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el164.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el164.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el164.ForeColor = System.Drawing.Color.Yellow;
            this.el164.Location = new System.Drawing.Point(258, 98);
            this.el164.Margin = new System.Windows.Forms.Padding(0);
            this.el164.Name = "el164";
            this.el164.Size = new System.Drawing.Size(24, 28);
            this.el164.TabIndex = 166;
            this.el164.Text = "2";
            this.el164.UseVisualStyleBackColor = false;
            this.el164.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el145
            // 
            this.el145.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el145.BackColor = System.Drawing.Color.Transparent;
            this.el145.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el145.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el145.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el145.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el145.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el145.ForeColor = System.Drawing.Color.Yellow;
            this.el145.Location = new System.Drawing.Point(258, 130);
            this.el145.Margin = new System.Windows.Forms.Padding(0);
            this.el145.Name = "el145";
            this.el145.Size = new System.Drawing.Size(24, 28);
            this.el145.TabIndex = 171;
            this.el145.Text = "2";
            this.el145.UseVisualStyleBackColor = false;
            this.el145.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // label57
            // 
            this.label57.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label57.BackColor = System.Drawing.Color.Transparent;
            this.label57.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label57.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label57.Location = new System.Drawing.Point(14, 96);
            this.label57.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(193, 32);
            this.label57.TabIndex = 164;
            this.label57.Tag = "6";
            this.label57.Text = "LOW KENNEDY";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el238
            // 
            this.el238.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el238.BackColor = System.Drawing.Color.Transparent;
            this.el238.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el238.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el238.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el238.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el238.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el238.ForeColor = System.Drawing.Color.Yellow;
            this.el238.Location = new System.Drawing.Point(231, 98);
            this.el238.Margin = new System.Windows.Forms.Padding(0);
            this.el238.Name = "el238";
            this.el238.Size = new System.Drawing.Size(24, 28);
            this.el238.TabIndex = 165;
            this.el238.Text = "1";
            this.el238.UseVisualStyleBackColor = false;
            this.el238.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el162
            // 
            this.el162.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el162.BackColor = System.Drawing.Color.Transparent;
            this.el162.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el162.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el162.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el162.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el162.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el162.ForeColor = System.Drawing.Color.Yellow;
            this.el162.Location = new System.Drawing.Point(316, 67);
            this.el162.Margin = new System.Windows.Forms.Padding(0);
            this.el162.Name = "el162";
            this.el162.Size = new System.Drawing.Size(24, 28);
            this.el162.TabIndex = 163;
            this.el162.Text = "4";
            this.el162.UseVisualStyleBackColor = false;
            this.el162.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // label55
            // 
            this.label55.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label55.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label55.Location = new System.Drawing.Point(14, 35);
            this.label55.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(198, 32);
            this.label55.TabIndex = 159;
            this.label55.Tag = "6";
            this.label55.Text = "Axel ar. the back";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el173
            // 
            this.el173.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el173.BackColor = System.Drawing.Color.Transparent;
            this.el173.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el173.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el173.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el173.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el173.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el173.ForeColor = System.Drawing.Color.Yellow;
            this.el173.Location = new System.Drawing.Point(231, 67);
            this.el173.Margin = new System.Windows.Forms.Padding(0);
            this.el173.Name = "el173";
            this.el173.Size = new System.Drawing.Size(24, 28);
            this.el173.TabIndex = 160;
            this.el173.Text = "1";
            this.el173.UseVisualStyleBackColor = false;
            this.el173.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el158
            // 
            this.el158.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el158.BackColor = System.Drawing.Color.Transparent;
            this.el158.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el158.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el158.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el158.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el158.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el158.ForeColor = System.Drawing.Color.Yellow;
            this.el158.Location = new System.Drawing.Point(316, 36);
            this.el158.Margin = new System.Windows.Forms.Padding(0);
            this.el158.Name = "el158";
            this.el158.Size = new System.Drawing.Size(24, 28);
            this.el158.TabIndex = 158;
            this.el158.Text = "4";
            this.el158.UseVisualStyleBackColor = false;
            this.el158.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el157
            // 
            this.el157.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el157.BackColor = System.Drawing.Color.Transparent;
            this.el157.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el157.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el157.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el157.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el157.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el157.ForeColor = System.Drawing.Color.Yellow;
            this.el157.Location = new System.Drawing.Point(287, 36);
            this.el157.Margin = new System.Windows.Forms.Padding(0);
            this.el157.Name = "el157";
            this.el157.Size = new System.Drawing.Size(24, 28);
            this.el157.TabIndex = 157;
            this.el157.Text = "3";
            this.el157.UseVisualStyleBackColor = false;
            this.el157.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el156
            // 
            this.el156.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el156.BackColor = System.Drawing.Color.Transparent;
            this.el156.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el156.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el156.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el156.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el156.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el156.ForeColor = System.Drawing.Color.Yellow;
            this.el156.Location = new System.Drawing.Point(258, 36);
            this.el156.Margin = new System.Windows.Forms.Padding(0);
            this.el156.Name = "el156";
            this.el156.Size = new System.Drawing.Size(24, 28);
            this.el156.TabIndex = 156;
            this.el156.Text = "2";
            this.el156.UseVisualStyleBackColor = false;
            this.el156.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el151
            // 
            this.el151.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el151.BackColor = System.Drawing.Color.Transparent;
            this.el151.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el151.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el151.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el151.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el151.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el151.ForeColor = System.Drawing.Color.Yellow;
            this.el151.Location = new System.Drawing.Point(316, 6);
            this.el151.Margin = new System.Windows.Forms.Padding(0);
            this.el151.Name = "el151";
            this.el151.Size = new System.Drawing.Size(24, 28);
            this.el151.TabIndex = 155;
            this.el151.Text = "4";
            this.el151.UseVisualStyleBackColor = false;
            this.el151.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el150
            // 
            this.el150.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el150.BackColor = System.Drawing.Color.Transparent;
            this.el150.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el150.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el150.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el150.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el150.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el150.ForeColor = System.Drawing.Color.Yellow;
            this.el150.Location = new System.Drawing.Point(287, 6);
            this.el150.Margin = new System.Windows.Forms.Padding(0);
            this.el150.Name = "el150";
            this.el150.Size = new System.Drawing.Size(24, 28);
            this.el150.TabIndex = 154;
            this.el150.Text = "3";
            this.el150.UseVisualStyleBackColor = false;
            this.el150.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el149
            // 
            this.el149.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el149.BackColor = System.Drawing.Color.Transparent;
            this.el149.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el149.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el149.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el149.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el149.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el149.ForeColor = System.Drawing.Color.Yellow;
            this.el149.Location = new System.Drawing.Point(258, 6);
            this.el149.Margin = new System.Windows.Forms.Padding(0);
            this.el149.Name = "el149";
            this.el149.Size = new System.Drawing.Size(24, 28);
            this.el149.TabIndex = 153;
            this.el149.Text = "2";
            this.el149.UseVisualStyleBackColor = false;
            this.el149.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label22.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label22.Location = new System.Drawing.Point(14, 67);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(174, 26);
            this.label22.TabIndex = 151;
            this.label22.Tag = "6";
            this.label22.Text = "FLIP";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el154
            // 
            this.el154.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el154.BackColor = System.Drawing.Color.Transparent;
            this.el154.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el154.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el154.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el154.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el154.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el154.ForeColor = System.Drawing.Color.Yellow;
            this.el154.Location = new System.Drawing.Point(231, 36);
            this.el154.Margin = new System.Windows.Forms.Padding(0);
            this.el154.Name = "el154";
            this.el154.Size = new System.Drawing.Size(24, 28);
            this.el154.TabIndex = 152;
            this.el154.Text = "1";
            this.el154.UseVisualStyleBackColor = false;
            this.el154.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label14.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label14.Location = new System.Drawing.Point(14, 6);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(175, 32);
            this.label14.TabIndex = 149;
            this.label14.Tag = "6";
            this.label14.Text = "A X E L";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el172
            // 
            this.el172.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el172.BackColor = System.Drawing.Color.Transparent;
            this.el172.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el172.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el172.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el172.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el172.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el172.ForeColor = System.Drawing.Color.Yellow;
            this.el172.Location = new System.Drawing.Point(231, 6);
            this.el172.Margin = new System.Windows.Forms.Padding(0);
            this.el172.Name = "el172";
            this.el172.Size = new System.Drawing.Size(24, 28);
            this.el172.TabIndex = 150;
            this.el172.Text = "1";
            this.el172.UseVisualStyleBackColor = false;
            this.el172.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el161
            // 
            this.el161.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el161.BackColor = System.Drawing.Color.Transparent;
            this.el161.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el161.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el161.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el161.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el161.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el161.ForeColor = System.Drawing.Color.Yellow;
            this.el161.Location = new System.Drawing.Point(287, 67);
            this.el161.Margin = new System.Windows.Forms.Padding(0);
            this.el161.Name = "el161";
            this.el161.Size = new System.Drawing.Size(24, 28);
            this.el161.TabIndex = 162;
            this.el161.Text = "3";
            this.el161.UseVisualStyleBackColor = false;
            this.el161.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el160
            // 
            this.el160.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el160.BackColor = System.Drawing.Color.Transparent;
            this.el160.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el160.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el160.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el160.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el160.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el160.ForeColor = System.Drawing.Color.Yellow;
            this.el160.Location = new System.Drawing.Point(258, 67);
            this.el160.Margin = new System.Windows.Forms.Padding(0);
            this.el160.Name = "el160";
            this.el160.Size = new System.Drawing.Size(24, 28);
            this.el160.TabIndex = 161;
            this.el160.Text = "2";
            this.el160.UseVisualStyleBackColor = false;
            this.el160.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // cbComboLift1
            // 
            this.cbComboLift1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbComboLift1.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbComboLift1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cbComboLift1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbComboLift1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Lime;
            this.cbComboLift1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Yellow;
            this.cbComboLift1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cbComboLift1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbComboLift1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbComboLift1.ForeColor = System.Drawing.Color.Black;
            this.cbComboLift1.Location = new System.Drawing.Point(247, 16);
            this.cbComboLift1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbComboLift1.Name = "cbComboLift1";
            this.cbComboLift1.Size = new System.Drawing.Size(109, 31);
            this.cbComboLift1.TabIndex = 177;
            this.cbComboLift1.Text = "COMBO LIFT";
            this.cbComboLift1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbComboLift1.UseVisualStyleBackColor = false;
            this.cbComboLift1.CheckedChanged += new System.EventHandler(this.cbComboLift1_CheckedChanged);
            // 
            // el167
            // 
            this.el167.BackColor = System.Drawing.Color.Transparent;
            this.el167.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el167.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el167.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el167.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el167.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el167.ForeColor = System.Drawing.Color.Yellow;
            this.el167.Location = new System.Drawing.Point(146, 12);
            this.el167.Margin = new System.Windows.Forms.Padding(0);
            this.el167.Name = "el167";
            this.el167.Size = new System.Drawing.Size(24, 28);
            this.el167.TabIndex = 143;
            this.el167.Text = "0";
            this.el167.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.el167.UseVisualStyleBackColor = false;
            this.el167.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(11, 12);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(149, 32);
            this.label8.TabIndex = 144;
            this.label8.Tag = "6";
            this.label8.Text = "NO LEVEL";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabLift2
            // 
            this.tabLift2.AutoScroll = true;
            this.tabLift2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.tabLift2.Controls.Add(this.el805);
            this.tabLift2.Controls.Add(this.el804);
            this.tabLift2.Controls.Add(this.el803);
            this.tabLift2.Controls.Add(this.el802);
            this.tabLift2.Controls.Add(this.el801);
            this.tabLift2.Controls.Add(this.cbComboLift2);
            this.tabLift2.Controls.Add(this.el200);
            this.tabLift2.Controls.Add(this.el195);
            this.tabLift2.Controls.Add(this.el190);
            this.tabLift2.Controls.Add(this.el185);
            this.tabLift2.Controls.Add(this.el180);
            this.tabLift2.Controls.Add(this.el199);
            this.tabLift2.Controls.Add(this.el198);
            this.tabLift2.Controls.Add(this.el197);
            this.tabLift2.Controls.Add(this.el196);
            this.tabLift2.Controls.Add(this.label32);
            this.tabLift2.Controls.Add(this.el194);
            this.tabLift2.Controls.Add(this.el193);
            this.tabLift2.Controls.Add(this.el192);
            this.tabLift2.Controls.Add(this.el191);
            this.tabLift2.Controls.Add(this.label31);
            this.tabLift2.Controls.Add(this.el189);
            this.tabLift2.Controls.Add(this.el184);
            this.tabLift2.Controls.Add(this.el179);
            this.tabLift2.Controls.Add(this.el188);
            this.tabLift2.Controls.Add(this.el187);
            this.tabLift2.Controls.Add(this.el186);
            this.tabLift2.Controls.Add(this.label27);
            this.tabLift2.Controls.Add(this.el183);
            this.tabLift2.Controls.Add(this.el182);
            this.tabLift2.Controls.Add(this.el181);
            this.tabLift2.Controls.Add(this.label28);
            this.tabLift2.Controls.Add(this.el178);
            this.tabLift2.Controls.Add(this.el177);
            this.tabLift2.Controls.Add(this.el176);
            this.tabLift2.Controls.Add(this.label29);
            this.tabLift2.Location = new System.Drawing.Point(4, 44);
            this.tabLift2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabLift2.Name = "tabLift2";
            this.tabLift2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabLift2.Size = new System.Drawing.Size(382, 250);
            this.tabLift2.TabIndex = 1;
            this.tabLift2.Text = "Lifts 2";
            // 
            // el805
            // 
            this.el805.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el805.BackColor = System.Drawing.Color.Transparent;
            this.el805.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el805.Enabled = false;
            this.el805.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el805.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el805.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el805.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el805.ForeColor = System.Drawing.Color.Yellow;
            this.el805.Location = new System.Drawing.Point(389, 181);
            this.el805.Margin = new System.Windows.Forms.Padding(0);
            this.el805.Name = "el805";
            this.el805.Size = new System.Drawing.Size(24, 28);
            this.el805.TabIndex = 251;
            this.el805.Text = "6";
            this.el805.UseVisualStyleBackColor = false;
            this.el805.Visible = false;
            this.el805.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el804
            // 
            this.el804.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el804.BackColor = System.Drawing.Color.Transparent;
            this.el804.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el804.Enabled = false;
            this.el804.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el804.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el804.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el804.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el804.ForeColor = System.Drawing.Color.Yellow;
            this.el804.Location = new System.Drawing.Point(389, 147);
            this.el804.Margin = new System.Windows.Forms.Padding(0);
            this.el804.Name = "el804";
            this.el804.Size = new System.Drawing.Size(24, 28);
            this.el804.TabIndex = 250;
            this.el804.Text = "6";
            this.el804.UseVisualStyleBackColor = false;
            this.el804.Visible = false;
            this.el804.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el803
            // 
            this.el803.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el803.BackColor = System.Drawing.Color.Transparent;
            this.el803.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el803.Enabled = false;
            this.el803.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el803.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el803.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el803.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el803.ForeColor = System.Drawing.Color.Yellow;
            this.el803.Location = new System.Drawing.Point(389, 114);
            this.el803.Margin = new System.Windows.Forms.Padding(0);
            this.el803.Name = "el803";
            this.el803.Size = new System.Drawing.Size(24, 28);
            this.el803.TabIndex = 249;
            this.el803.Text = "6";
            this.el803.UseVisualStyleBackColor = false;
            this.el803.Visible = false;
            this.el803.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el802
            // 
            this.el802.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el802.BackColor = System.Drawing.Color.Transparent;
            this.el802.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el802.Enabled = false;
            this.el802.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el802.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el802.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el802.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el802.ForeColor = System.Drawing.Color.Yellow;
            this.el802.Location = new System.Drawing.Point(389, 79);
            this.el802.Margin = new System.Windows.Forms.Padding(0);
            this.el802.Name = "el802";
            this.el802.Size = new System.Drawing.Size(24, 28);
            this.el802.TabIndex = 248;
            this.el802.Text = "6";
            this.el802.UseVisualStyleBackColor = false;
            this.el802.Visible = false;
            this.el802.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el801
            // 
            this.el801.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el801.BackColor = System.Drawing.Color.Transparent;
            this.el801.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el801.Enabled = false;
            this.el801.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el801.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el801.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el801.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el801.ForeColor = System.Drawing.Color.Yellow;
            this.el801.Location = new System.Drawing.Point(389, 46);
            this.el801.Margin = new System.Windows.Forms.Padding(0);
            this.el801.Name = "el801";
            this.el801.Size = new System.Drawing.Size(24, 28);
            this.el801.TabIndex = 247;
            this.el801.Text = "6";
            this.el801.UseVisualStyleBackColor = false;
            this.el801.Visible = false;
            this.el801.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // cbComboLift2
            // 
            this.cbComboLift2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbComboLift2.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbComboLift2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cbComboLift2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbComboLift2.FlatAppearance.CheckedBackColor = System.Drawing.Color.Lime;
            this.cbComboLift2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Yellow;
            this.cbComboLift2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cbComboLift2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbComboLift2.Font = new System.Drawing.Font("Arial Narrow", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbComboLift2.ForeColor = System.Drawing.Color.Black;
            this.cbComboLift2.Location = new System.Drawing.Point(238, 12);
            this.cbComboLift2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbComboLift2.Name = "cbComboLift2";
            this.cbComboLift2.Size = new System.Drawing.Size(129, 31);
            this.cbComboLift2.TabIndex = 246;
            this.cbComboLift2.Text = "COMBO LIFT";
            this.cbComboLift2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbComboLift2.UseVisualStyleBackColor = false;
            this.cbComboLift2.CheckedChanged += new System.EventHandler(this.cbComboLift2_CheckedChanged);
            // 
            // el200
            // 
            this.el200.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el200.BackColor = System.Drawing.Color.Transparent;
            this.el200.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el200.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el200.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el200.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el200.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el200.ForeColor = System.Drawing.Color.Yellow;
            this.el200.Location = new System.Drawing.Point(343, 181);
            this.el200.Margin = new System.Windows.Forms.Padding(0);
            this.el200.Name = "el200";
            this.el200.Size = new System.Drawing.Size(24, 28);
            this.el200.TabIndex = 165;
            this.el200.Text = "5";
            this.el200.UseVisualStyleBackColor = false;
            this.el200.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el195
            // 
            this.el195.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el195.BackColor = System.Drawing.Color.Transparent;
            this.el195.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el195.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el195.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el195.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el195.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el195.ForeColor = System.Drawing.Color.Yellow;
            this.el195.Location = new System.Drawing.Point(343, 147);
            this.el195.Margin = new System.Windows.Forms.Padding(0);
            this.el195.Name = "el195";
            this.el195.Size = new System.Drawing.Size(24, 28);
            this.el195.TabIndex = 169;
            this.el195.Text = "5";
            this.el195.UseVisualStyleBackColor = false;
            this.el195.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el190
            // 
            this.el190.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el190.BackColor = System.Drawing.Color.Transparent;
            this.el190.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el190.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el190.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el190.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el190.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el190.ForeColor = System.Drawing.Color.Yellow;
            this.el190.Location = new System.Drawing.Point(343, 114);
            this.el190.Margin = new System.Windows.Forms.Padding(0);
            this.el190.Name = "el190";
            this.el190.Size = new System.Drawing.Size(24, 28);
            this.el190.TabIndex = 168;
            this.el190.Text = "5";
            this.el190.UseVisualStyleBackColor = false;
            this.el190.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el185
            // 
            this.el185.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el185.BackColor = System.Drawing.Color.Transparent;
            this.el185.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el185.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el185.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el185.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el185.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el185.ForeColor = System.Drawing.Color.Yellow;
            this.el185.Location = new System.Drawing.Point(343, 79);
            this.el185.Margin = new System.Windows.Forms.Padding(0);
            this.el185.Name = "el185";
            this.el185.Size = new System.Drawing.Size(24, 28);
            this.el185.TabIndex = 167;
            this.el185.Text = "5";
            this.el185.UseVisualStyleBackColor = false;
            this.el185.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el180
            // 
            this.el180.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el180.BackColor = System.Drawing.Color.Transparent;
            this.el180.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el180.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el180.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el180.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el180.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el180.ForeColor = System.Drawing.Color.Yellow;
            this.el180.Location = new System.Drawing.Point(343, 46);
            this.el180.Margin = new System.Windows.Forms.Padding(0);
            this.el180.Name = "el180";
            this.el180.Size = new System.Drawing.Size(24, 28);
            this.el180.TabIndex = 166;
            this.el180.Text = "5";
            this.el180.UseVisualStyleBackColor = false;
            this.el180.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el199
            // 
            this.el199.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el199.BackColor = System.Drawing.Color.Transparent;
            this.el199.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el199.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el199.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el199.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el199.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el199.ForeColor = System.Drawing.Color.Yellow;
            this.el199.Location = new System.Drawing.Point(318, 181);
            this.el199.Margin = new System.Windows.Forms.Padding(0);
            this.el199.Name = "el199";
            this.el199.Size = new System.Drawing.Size(24, 28);
            this.el199.TabIndex = 164;
            this.el199.Text = "4";
            this.el199.UseVisualStyleBackColor = false;
            this.el199.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el198
            // 
            this.el198.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el198.BackColor = System.Drawing.Color.Transparent;
            this.el198.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el198.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el198.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el198.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el198.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el198.ForeColor = System.Drawing.Color.Yellow;
            this.el198.Location = new System.Drawing.Point(290, 181);
            this.el198.Margin = new System.Windows.Forms.Padding(0);
            this.el198.Name = "el198";
            this.el198.Size = new System.Drawing.Size(24, 28);
            this.el198.TabIndex = 163;
            this.el198.Text = "3";
            this.el198.UseVisualStyleBackColor = false;
            this.el198.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el197
            // 
            this.el197.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el197.BackColor = System.Drawing.Color.Transparent;
            this.el197.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el197.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el197.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el197.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el197.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el197.ForeColor = System.Drawing.Color.Yellow;
            this.el197.Location = new System.Drawing.Point(265, 181);
            this.el197.Margin = new System.Windows.Forms.Padding(0);
            this.el197.Name = "el197";
            this.el197.Size = new System.Drawing.Size(24, 28);
            this.el197.TabIndex = 162;
            this.el197.Text = "2";
            this.el197.UseVisualStyleBackColor = false;
            this.el197.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el196
            // 
            this.el196.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el196.BackColor = System.Drawing.Color.Transparent;
            this.el196.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el196.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el196.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el196.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el196.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el196.ForeColor = System.Drawing.Color.Yellow;
            this.el196.Location = new System.Drawing.Point(238, 181);
            this.el196.Margin = new System.Windows.Forms.Padding(0);
            this.el196.Name = "el196";
            this.el196.Size = new System.Drawing.Size(24, 28);
            this.el196.TabIndex = 161;
            this.el196.Text = "1";
            this.el196.UseVisualStyleBackColor = false;
            this.el196.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // label32
            // 
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label32.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label32.Location = new System.Drawing.Point(34, 179);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(317, 32);
            this.label32.TabIndex = 160;
            this.label32.Text = "REVERSED LOOP";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el194
            // 
            this.el194.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el194.BackColor = System.Drawing.Color.Transparent;
            this.el194.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el194.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el194.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el194.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el194.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el194.ForeColor = System.Drawing.Color.Yellow;
            this.el194.Location = new System.Drawing.Point(318, 147);
            this.el194.Margin = new System.Windows.Forms.Padding(0);
            this.el194.Name = "el194";
            this.el194.Size = new System.Drawing.Size(24, 28);
            this.el194.TabIndex = 159;
            this.el194.Text = "4";
            this.el194.UseVisualStyleBackColor = false;
            this.el194.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el193
            // 
            this.el193.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el193.BackColor = System.Drawing.Color.Transparent;
            this.el193.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el193.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el193.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el193.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el193.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el193.ForeColor = System.Drawing.Color.Yellow;
            this.el193.Location = new System.Drawing.Point(290, 147);
            this.el193.Margin = new System.Windows.Forms.Padding(0);
            this.el193.Name = "el193";
            this.el193.Size = new System.Drawing.Size(24, 28);
            this.el193.TabIndex = 158;
            this.el193.Text = "3";
            this.el193.UseVisualStyleBackColor = false;
            this.el193.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el192
            // 
            this.el192.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el192.BackColor = System.Drawing.Color.Transparent;
            this.el192.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el192.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el192.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el192.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el192.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el192.ForeColor = System.Drawing.Color.Yellow;
            this.el192.Location = new System.Drawing.Point(265, 147);
            this.el192.Margin = new System.Windows.Forms.Padding(0);
            this.el192.Name = "el192";
            this.el192.Size = new System.Drawing.Size(24, 28);
            this.el192.TabIndex = 157;
            this.el192.Text = "2";
            this.el192.UseVisualStyleBackColor = false;
            this.el192.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el191
            // 
            this.el191.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el191.BackColor = System.Drawing.Color.Transparent;
            this.el191.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el191.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el191.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el191.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el191.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el191.ForeColor = System.Drawing.Color.Yellow;
            this.el191.Location = new System.Drawing.Point(238, 147);
            this.el191.Margin = new System.Windows.Forms.Padding(0);
            this.el191.Name = "el191";
            this.el191.Size = new System.Drawing.Size(24, 28);
            this.el191.TabIndex = 156;
            this.el191.Text = "1";
            this.el191.UseVisualStyleBackColor = false;
            this.el191.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label31.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label31.Location = new System.Drawing.Point(34, 145);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(317, 32);
            this.label31.TabIndex = 155;
            this.label31.Text = "CARTWHEEL";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el189
            // 
            this.el189.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el189.BackColor = System.Drawing.Color.Transparent;
            this.el189.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el189.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el189.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el189.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el189.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el189.ForeColor = System.Drawing.Color.Yellow;
            this.el189.Location = new System.Drawing.Point(318, 114);
            this.el189.Margin = new System.Windows.Forms.Padding(0);
            this.el189.Name = "el189";
            this.el189.Size = new System.Drawing.Size(24, 28);
            this.el189.TabIndex = 152;
            this.el189.Text = "4";
            this.el189.UseVisualStyleBackColor = false;
            this.el189.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el184
            // 
            this.el184.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el184.BackColor = System.Drawing.Color.Transparent;
            this.el184.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el184.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el184.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el184.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el184.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el184.ForeColor = System.Drawing.Color.Yellow;
            this.el184.Location = new System.Drawing.Point(318, 79);
            this.el184.Margin = new System.Windows.Forms.Padding(0);
            this.el184.Name = "el184";
            this.el184.Size = new System.Drawing.Size(24, 28);
            this.el184.TabIndex = 151;
            this.el184.Text = "4";
            this.el184.UseVisualStyleBackColor = false;
            this.el184.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el179
            // 
            this.el179.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el179.BackColor = System.Drawing.Color.Transparent;
            this.el179.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el179.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el179.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el179.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el179.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el179.ForeColor = System.Drawing.Color.Yellow;
            this.el179.Location = new System.Drawing.Point(318, 46);
            this.el179.Margin = new System.Windows.Forms.Padding(0);
            this.el179.Name = "el179";
            this.el179.Size = new System.Drawing.Size(24, 28);
            this.el179.TabIndex = 150;
            this.el179.Text = "4";
            this.el179.UseVisualStyleBackColor = false;
            this.el179.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el188
            // 
            this.el188.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el188.BackColor = System.Drawing.Color.Transparent;
            this.el188.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el188.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el188.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el188.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el188.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el188.ForeColor = System.Drawing.Color.Yellow;
            this.el188.Location = new System.Drawing.Point(290, 114);
            this.el188.Margin = new System.Windows.Forms.Padding(0);
            this.el188.Name = "el188";
            this.el188.Size = new System.Drawing.Size(24, 28);
            this.el188.TabIndex = 149;
            this.el188.Text = "3";
            this.el188.UseVisualStyleBackColor = false;
            this.el188.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el187
            // 
            this.el187.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el187.BackColor = System.Drawing.Color.Transparent;
            this.el187.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el187.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el187.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el187.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el187.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el187.ForeColor = System.Drawing.Color.Yellow;
            this.el187.Location = new System.Drawing.Point(265, 114);
            this.el187.Margin = new System.Windows.Forms.Padding(0);
            this.el187.Name = "el187";
            this.el187.Size = new System.Drawing.Size(24, 28);
            this.el187.TabIndex = 148;
            this.el187.Text = "2";
            this.el187.UseVisualStyleBackColor = false;
            this.el187.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el186
            // 
            this.el186.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el186.BackColor = System.Drawing.Color.Transparent;
            this.el186.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el186.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el186.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el186.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el186.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el186.ForeColor = System.Drawing.Color.Yellow;
            this.el186.Location = new System.Drawing.Point(238, 114);
            this.el186.Margin = new System.Windows.Forms.Padding(0);
            this.el186.Name = "el186";
            this.el186.Size = new System.Drawing.Size(24, 28);
            this.el186.TabIndex = 147;
            this.el186.Text = "1";
            this.el186.UseVisualStyleBackColor = false;
            this.el186.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label27.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label27.Location = new System.Drawing.Point(34, 112);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(317, 32);
            this.label27.TabIndex = 146;
            this.label27.Text = "P R E S S";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el183
            // 
            this.el183.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el183.BackColor = System.Drawing.Color.Transparent;
            this.el183.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el183.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el183.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el183.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el183.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el183.ForeColor = System.Drawing.Color.Yellow;
            this.el183.Location = new System.Drawing.Point(290, 79);
            this.el183.Margin = new System.Windows.Forms.Padding(0);
            this.el183.Name = "el183";
            this.el183.Size = new System.Drawing.Size(24, 28);
            this.el183.TabIndex = 145;
            this.el183.Text = "3";
            this.el183.UseVisualStyleBackColor = false;
            this.el183.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el182
            // 
            this.el182.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el182.BackColor = System.Drawing.Color.Transparent;
            this.el182.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el182.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el182.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el182.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el182.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el182.ForeColor = System.Drawing.Color.Yellow;
            this.el182.Location = new System.Drawing.Point(265, 79);
            this.el182.Margin = new System.Windows.Forms.Padding(0);
            this.el182.Name = "el182";
            this.el182.Size = new System.Drawing.Size(24, 28);
            this.el182.TabIndex = 144;
            this.el182.Text = "2";
            this.el182.UseVisualStyleBackColor = false;
            this.el182.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el181
            // 
            this.el181.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el181.BackColor = System.Drawing.Color.Transparent;
            this.el181.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el181.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el181.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el181.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el181.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el181.ForeColor = System.Drawing.Color.Yellow;
            this.el181.Location = new System.Drawing.Point(238, 79);
            this.el181.Margin = new System.Windows.Forms.Padding(0);
            this.el181.Name = "el181";
            this.el181.Size = new System.Drawing.Size(24, 28);
            this.el181.TabIndex = 143;
            this.el181.Text = "1";
            this.el181.UseVisualStyleBackColor = false;
            this.el181.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label28.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label28.Location = new System.Drawing.Point(34, 77);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(317, 32);
            this.label28.TabIndex = 142;
            this.label28.Text = "P A N C A K E ";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el178
            // 
            this.el178.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el178.BackColor = System.Drawing.Color.Transparent;
            this.el178.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el178.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el178.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el178.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el178.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el178.ForeColor = System.Drawing.Color.Yellow;
            this.el178.Location = new System.Drawing.Point(290, 46);
            this.el178.Margin = new System.Windows.Forms.Padding(0);
            this.el178.Name = "el178";
            this.el178.Size = new System.Drawing.Size(24, 28);
            this.el178.TabIndex = 141;
            this.el178.Text = "3";
            this.el178.UseVisualStyleBackColor = false;
            this.el178.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el177
            // 
            this.el177.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el177.BackColor = System.Drawing.Color.Transparent;
            this.el177.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el177.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el177.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el177.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el177.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el177.ForeColor = System.Drawing.Color.Yellow;
            this.el177.Location = new System.Drawing.Point(265, 46);
            this.el177.Margin = new System.Windows.Forms.Padding(0);
            this.el177.Name = "el177";
            this.el177.Size = new System.Drawing.Size(24, 28);
            this.el177.TabIndex = 140;
            this.el177.Text = "2";
            this.el177.UseVisualStyleBackColor = false;
            this.el177.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el176
            // 
            this.el176.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el176.BackColor = System.Drawing.Color.Transparent;
            this.el176.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el176.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el176.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el176.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el176.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el176.ForeColor = System.Drawing.Color.Yellow;
            this.el176.Location = new System.Drawing.Point(238, 46);
            this.el176.Margin = new System.Windows.Forms.Padding(0);
            this.el176.Name = "el176";
            this.el176.Size = new System.Drawing.Size(24, 28);
            this.el176.TabIndex = 139;
            this.el176.Text = "1";
            this.el176.UseVisualStyleBackColor = false;
            this.el176.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label29.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(34, 44);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(317, 32);
            this.label29.TabIndex = 138;
            this.label29.Tag = "6";
            this.label29.Text = "A I R P L A N E";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabLift3
            // 
            this.tabLift3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.tabLift3.Controls.Add(this.cbComboLift3);
            this.tabLift3.Controls.Add(this.el810);
            this.tabLift3.Controls.Add(this.el809);
            this.tabLift3.Controls.Add(this.el808);
            this.tabLift3.Controls.Add(this.el807);
            this.tabLift3.Controls.Add(this.el806);
            this.tabLift3.Controls.Add(this.el225);
            this.tabLift3.Controls.Add(this.el220);
            this.tabLift3.Controls.Add(this.el215);
            this.tabLift3.Controls.Add(this.el210);
            this.tabLift3.Controls.Add(this.el205);
            this.tabLift3.Controls.Add(this.el224);
            this.tabLift3.Controls.Add(this.el201);
            this.tabLift3.Controls.Add(this.el223);
            this.tabLift3.Controls.Add(this.el202);
            this.tabLift3.Controls.Add(this.el222);
            this.tabLift3.Controls.Add(this.el203);
            this.tabLift3.Controls.Add(this.el221);
            this.tabLift3.Controls.Add(this.el206);
            this.tabLift3.Controls.Add(this.el219);
            this.tabLift3.Controls.Add(this.el207);
            this.tabLift3.Controls.Add(this.el218);
            this.tabLift3.Controls.Add(this.el208);
            this.tabLift3.Controls.Add(this.el217);
            this.tabLift3.Controls.Add(this.el211);
            this.tabLift3.Controls.Add(this.el216);
            this.tabLift3.Controls.Add(this.el212);
            this.tabLift3.Controls.Add(this.el214);
            this.tabLift3.Controls.Add(this.el213);
            this.tabLift3.Controls.Add(this.el209);
            this.tabLift3.Controls.Add(this.el204);
            this.tabLift3.Controls.Add(this.label38);
            this.tabLift3.Controls.Add(this.label37);
            this.tabLift3.Controls.Add(this.label36);
            this.tabLift3.Controls.Add(this.label35);
            this.tabLift3.Controls.Add(this.label34);
            this.tabLift3.Location = new System.Drawing.Point(4, 44);
            this.tabLift3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabLift3.Name = "tabLift3";
            this.tabLift3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabLift3.Size = new System.Drawing.Size(382, 250);
            this.tabLift3.TabIndex = 2;
            this.tabLift3.Text = "Lifts 3";
            // 
            // cbComboLift3
            // 
            this.cbComboLift3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbComboLift3.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbComboLift3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cbComboLift3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbComboLift3.FlatAppearance.CheckedBackColor = System.Drawing.Color.Lime;
            this.cbComboLift3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Yellow;
            this.cbComboLift3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Yellow;
            this.cbComboLift3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbComboLift3.Font = new System.Drawing.Font("Arial Narrow", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbComboLift3.ForeColor = System.Drawing.Color.Black;
            this.cbComboLift3.Location = new System.Drawing.Point(238, 12);
            this.cbComboLift3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbComboLift3.Name = "cbComboLift3";
            this.cbComboLift3.Size = new System.Drawing.Size(129, 31);
            this.cbComboLift3.TabIndex = 292;
            this.cbComboLift3.Text = "COMBO LIFT";
            this.cbComboLift3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbComboLift3.UseVisualStyleBackColor = false;
            this.cbComboLift3.CheckedChanged += new System.EventHandler(this.cbComboLift3_CheckedChanged);
            // 
            // el810
            // 
            this.el810.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el810.BackColor = System.Drawing.Color.Transparent;
            this.el810.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el810.Enabled = false;
            this.el810.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el810.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el810.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el810.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el810.ForeColor = System.Drawing.Color.Yellow;
            this.el810.Location = new System.Drawing.Point(389, 181);
            this.el810.Margin = new System.Windows.Forms.Padding(0);
            this.el810.Name = "el810";
            this.el810.Size = new System.Drawing.Size(24, 28);
            this.el810.TabIndex = 291;
            this.el810.Text = "6";
            this.el810.UseVisualStyleBackColor = false;
            this.el810.Visible = false;
            this.el810.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el809
            // 
            this.el809.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el809.BackColor = System.Drawing.Color.Transparent;
            this.el809.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el809.Enabled = false;
            this.el809.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el809.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el809.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el809.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el809.ForeColor = System.Drawing.Color.Yellow;
            this.el809.Location = new System.Drawing.Point(389, 147);
            this.el809.Margin = new System.Windows.Forms.Padding(0);
            this.el809.Name = "el809";
            this.el809.Size = new System.Drawing.Size(24, 28);
            this.el809.TabIndex = 290;
            this.el809.Text = "6";
            this.el809.UseVisualStyleBackColor = false;
            this.el809.Visible = false;
            this.el809.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el808
            // 
            this.el808.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el808.BackColor = System.Drawing.Color.Transparent;
            this.el808.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el808.Enabled = false;
            this.el808.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el808.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el808.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el808.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el808.ForeColor = System.Drawing.Color.Yellow;
            this.el808.Location = new System.Drawing.Point(389, 114);
            this.el808.Margin = new System.Windows.Forms.Padding(0);
            this.el808.Name = "el808";
            this.el808.Size = new System.Drawing.Size(24, 28);
            this.el808.TabIndex = 289;
            this.el808.Text = "6";
            this.el808.UseVisualStyleBackColor = false;
            this.el808.Visible = false;
            this.el808.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el807
            // 
            this.el807.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el807.BackColor = System.Drawing.Color.Transparent;
            this.el807.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el807.Enabled = false;
            this.el807.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el807.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el807.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el807.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el807.ForeColor = System.Drawing.Color.Yellow;
            this.el807.Location = new System.Drawing.Point(389, 79);
            this.el807.Margin = new System.Windows.Forms.Padding(0);
            this.el807.Name = "el807";
            this.el807.Size = new System.Drawing.Size(24, 28);
            this.el807.TabIndex = 288;
            this.el807.Text = "6";
            this.el807.UseVisualStyleBackColor = false;
            this.el807.Visible = false;
            this.el807.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el806
            // 
            this.el806.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el806.BackColor = System.Drawing.Color.Transparent;
            this.el806.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el806.Enabled = false;
            this.el806.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el806.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el806.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el806.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el806.ForeColor = System.Drawing.Color.Yellow;
            this.el806.Location = new System.Drawing.Point(389, 46);
            this.el806.Margin = new System.Windows.Forms.Padding(0);
            this.el806.Name = "el806";
            this.el806.Size = new System.Drawing.Size(24, 28);
            this.el806.TabIndex = 287;
            this.el806.Text = "6";
            this.el806.UseVisualStyleBackColor = false;
            this.el806.Visible = false;
            this.el806.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el225
            // 
            this.el225.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el225.BackColor = System.Drawing.Color.Transparent;
            this.el225.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el225.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el225.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el225.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el225.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el225.ForeColor = System.Drawing.Color.Yellow;
            this.el225.Location = new System.Drawing.Point(343, 181);
            this.el225.Margin = new System.Windows.Forms.Padding(0);
            this.el225.Name = "el225";
            this.el225.Size = new System.Drawing.Size(24, 28);
            this.el225.TabIndex = 282;
            this.el225.Text = "5";
            this.el225.UseVisualStyleBackColor = false;
            this.el225.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el220
            // 
            this.el220.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el220.BackColor = System.Drawing.Color.Transparent;
            this.el220.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el220.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el220.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el220.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el220.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el220.ForeColor = System.Drawing.Color.Yellow;
            this.el220.Location = new System.Drawing.Point(343, 147);
            this.el220.Margin = new System.Windows.Forms.Padding(0);
            this.el220.Name = "el220";
            this.el220.Size = new System.Drawing.Size(24, 28);
            this.el220.TabIndex = 286;
            this.el220.Text = "5";
            this.el220.UseVisualStyleBackColor = false;
            this.el220.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el215
            // 
            this.el215.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el215.BackColor = System.Drawing.Color.Transparent;
            this.el215.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el215.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el215.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el215.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el215.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el215.ForeColor = System.Drawing.Color.Yellow;
            this.el215.Location = new System.Drawing.Point(343, 114);
            this.el215.Margin = new System.Windows.Forms.Padding(0);
            this.el215.Name = "el215";
            this.el215.Size = new System.Drawing.Size(24, 28);
            this.el215.TabIndex = 285;
            this.el215.Text = "5";
            this.el215.UseVisualStyleBackColor = false;
            this.el215.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el210
            // 
            this.el210.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el210.BackColor = System.Drawing.Color.Transparent;
            this.el210.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el210.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el210.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el210.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el210.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el210.ForeColor = System.Drawing.Color.Yellow;
            this.el210.Location = new System.Drawing.Point(343, 79);
            this.el210.Margin = new System.Windows.Forms.Padding(0);
            this.el210.Name = "el210";
            this.el210.Size = new System.Drawing.Size(24, 28);
            this.el210.TabIndex = 284;
            this.el210.Text = "5";
            this.el210.UseVisualStyleBackColor = false;
            this.el210.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el205
            // 
            this.el205.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el205.BackColor = System.Drawing.Color.Transparent;
            this.el205.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el205.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el205.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el205.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el205.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el205.ForeColor = System.Drawing.Color.Yellow;
            this.el205.Location = new System.Drawing.Point(343, 46);
            this.el205.Margin = new System.Windows.Forms.Padding(0);
            this.el205.Name = "el205";
            this.el205.Size = new System.Drawing.Size(24, 28);
            this.el205.TabIndex = 283;
            this.el205.Text = "5";
            this.el205.UseVisualStyleBackColor = false;
            this.el205.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el224
            // 
            this.el224.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el224.BackColor = System.Drawing.Color.Transparent;
            this.el224.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el224.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el224.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el224.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el224.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el224.ForeColor = System.Drawing.Color.Yellow;
            this.el224.Location = new System.Drawing.Point(318, 181);
            this.el224.Margin = new System.Windows.Forms.Padding(0);
            this.el224.Name = "el224";
            this.el224.Size = new System.Drawing.Size(24, 28);
            this.el224.TabIndex = 281;
            this.el224.Text = "4";
            this.el224.UseVisualStyleBackColor = false;
            this.el224.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el201
            // 
            this.el201.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el201.BackColor = System.Drawing.Color.Transparent;
            this.el201.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el201.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el201.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el201.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el201.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el201.ForeColor = System.Drawing.Color.Yellow;
            this.el201.Location = new System.Drawing.Point(238, 46);
            this.el201.Margin = new System.Windows.Forms.Padding(0);
            this.el201.Name = "el201";
            this.el201.Size = new System.Drawing.Size(24, 28);
            this.el201.TabIndex = 262;
            this.el201.Text = "1";
            this.el201.UseVisualStyleBackColor = false;
            this.el201.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el223
            // 
            this.el223.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el223.BackColor = System.Drawing.Color.Transparent;
            this.el223.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el223.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el223.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el223.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el223.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el223.ForeColor = System.Drawing.Color.Yellow;
            this.el223.Location = new System.Drawing.Point(290, 181);
            this.el223.Margin = new System.Windows.Forms.Padding(0);
            this.el223.Name = "el223";
            this.el223.Size = new System.Drawing.Size(24, 28);
            this.el223.TabIndex = 280;
            this.el223.Text = "3";
            this.el223.UseVisualStyleBackColor = false;
            this.el223.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el202
            // 
            this.el202.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el202.BackColor = System.Drawing.Color.Transparent;
            this.el202.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el202.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el202.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el202.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el202.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el202.ForeColor = System.Drawing.Color.Yellow;
            this.el202.Location = new System.Drawing.Point(265, 46);
            this.el202.Margin = new System.Windows.Forms.Padding(0);
            this.el202.Name = "el202";
            this.el202.Size = new System.Drawing.Size(24, 28);
            this.el202.TabIndex = 263;
            this.el202.Text = "2";
            this.el202.UseVisualStyleBackColor = false;
            this.el202.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el222
            // 
            this.el222.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el222.BackColor = System.Drawing.Color.Transparent;
            this.el222.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el222.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el222.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el222.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el222.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el222.ForeColor = System.Drawing.Color.Yellow;
            this.el222.Location = new System.Drawing.Point(265, 181);
            this.el222.Margin = new System.Windows.Forms.Padding(0);
            this.el222.Name = "el222";
            this.el222.Size = new System.Drawing.Size(24, 28);
            this.el222.TabIndex = 279;
            this.el222.Text = "2";
            this.el222.UseVisualStyleBackColor = false;
            this.el222.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el203
            // 
            this.el203.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el203.BackColor = System.Drawing.Color.Transparent;
            this.el203.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el203.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el203.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el203.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el203.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el203.ForeColor = System.Drawing.Color.Yellow;
            this.el203.Location = new System.Drawing.Point(290, 46);
            this.el203.Margin = new System.Windows.Forms.Padding(0);
            this.el203.Name = "el203";
            this.el203.Size = new System.Drawing.Size(24, 28);
            this.el203.TabIndex = 264;
            this.el203.Text = "3";
            this.el203.UseVisualStyleBackColor = false;
            this.el203.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el221
            // 
            this.el221.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el221.BackColor = System.Drawing.Color.Transparent;
            this.el221.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el221.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el221.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el221.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el221.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el221.ForeColor = System.Drawing.Color.Yellow;
            this.el221.Location = new System.Drawing.Point(238, 181);
            this.el221.Margin = new System.Windows.Forms.Padding(0);
            this.el221.Name = "el221";
            this.el221.Size = new System.Drawing.Size(24, 28);
            this.el221.TabIndex = 278;
            this.el221.Text = "1";
            this.el221.UseVisualStyleBackColor = false;
            this.el221.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el206
            // 
            this.el206.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el206.BackColor = System.Drawing.Color.Transparent;
            this.el206.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el206.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el206.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el206.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el206.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el206.ForeColor = System.Drawing.Color.Yellow;
            this.el206.Location = new System.Drawing.Point(238, 79);
            this.el206.Margin = new System.Windows.Forms.Padding(0);
            this.el206.Name = "el206";
            this.el206.Size = new System.Drawing.Size(24, 28);
            this.el206.TabIndex = 265;
            this.el206.Text = "1";
            this.el206.UseVisualStyleBackColor = false;
            this.el206.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el219
            // 
            this.el219.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el219.BackColor = System.Drawing.Color.Transparent;
            this.el219.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el219.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el219.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el219.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el219.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el219.ForeColor = System.Drawing.Color.Yellow;
            this.el219.Location = new System.Drawing.Point(318, 147);
            this.el219.Margin = new System.Windows.Forms.Padding(0);
            this.el219.Name = "el219";
            this.el219.Size = new System.Drawing.Size(24, 28);
            this.el219.TabIndex = 277;
            this.el219.Text = "4";
            this.el219.UseVisualStyleBackColor = false;
            this.el219.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el207
            // 
            this.el207.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el207.BackColor = System.Drawing.Color.Transparent;
            this.el207.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el207.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el207.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el207.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el207.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el207.ForeColor = System.Drawing.Color.Yellow;
            this.el207.Location = new System.Drawing.Point(265, 79);
            this.el207.Margin = new System.Windows.Forms.Padding(0);
            this.el207.Name = "el207";
            this.el207.Size = new System.Drawing.Size(24, 28);
            this.el207.TabIndex = 266;
            this.el207.Text = "2";
            this.el207.UseVisualStyleBackColor = false;
            this.el207.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el218
            // 
            this.el218.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el218.BackColor = System.Drawing.Color.Transparent;
            this.el218.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el218.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el218.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el218.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el218.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el218.ForeColor = System.Drawing.Color.Yellow;
            this.el218.Location = new System.Drawing.Point(290, 147);
            this.el218.Margin = new System.Windows.Forms.Padding(0);
            this.el218.Name = "el218";
            this.el218.Size = new System.Drawing.Size(24, 28);
            this.el218.TabIndex = 276;
            this.el218.Text = "3";
            this.el218.UseVisualStyleBackColor = false;
            this.el218.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el208
            // 
            this.el208.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el208.BackColor = System.Drawing.Color.Transparent;
            this.el208.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el208.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el208.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el208.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el208.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el208.ForeColor = System.Drawing.Color.Yellow;
            this.el208.Location = new System.Drawing.Point(290, 79);
            this.el208.Margin = new System.Windows.Forms.Padding(0);
            this.el208.Name = "el208";
            this.el208.Size = new System.Drawing.Size(24, 28);
            this.el208.TabIndex = 267;
            this.el208.Text = "3";
            this.el208.UseVisualStyleBackColor = false;
            this.el208.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el217
            // 
            this.el217.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el217.BackColor = System.Drawing.Color.Transparent;
            this.el217.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el217.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el217.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el217.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el217.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el217.ForeColor = System.Drawing.Color.Yellow;
            this.el217.Location = new System.Drawing.Point(265, 147);
            this.el217.Margin = new System.Windows.Forms.Padding(0);
            this.el217.Name = "el217";
            this.el217.Size = new System.Drawing.Size(24, 28);
            this.el217.TabIndex = 275;
            this.el217.Text = "2";
            this.el217.UseVisualStyleBackColor = false;
            this.el217.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el211
            // 
            this.el211.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el211.BackColor = System.Drawing.Color.Transparent;
            this.el211.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el211.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el211.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el211.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el211.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el211.ForeColor = System.Drawing.Color.Yellow;
            this.el211.Location = new System.Drawing.Point(238, 114);
            this.el211.Margin = new System.Windows.Forms.Padding(0);
            this.el211.Name = "el211";
            this.el211.Size = new System.Drawing.Size(24, 28);
            this.el211.TabIndex = 268;
            this.el211.Text = "1";
            this.el211.UseVisualStyleBackColor = false;
            this.el211.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el216
            // 
            this.el216.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el216.BackColor = System.Drawing.Color.Transparent;
            this.el216.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el216.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el216.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el216.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el216.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el216.ForeColor = System.Drawing.Color.Yellow;
            this.el216.Location = new System.Drawing.Point(238, 147);
            this.el216.Margin = new System.Windows.Forms.Padding(0);
            this.el216.Name = "el216";
            this.el216.Size = new System.Drawing.Size(24, 28);
            this.el216.TabIndex = 274;
            this.el216.Text = "1";
            this.el216.UseVisualStyleBackColor = false;
            this.el216.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el212
            // 
            this.el212.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el212.BackColor = System.Drawing.Color.Transparent;
            this.el212.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el212.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el212.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el212.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el212.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el212.ForeColor = System.Drawing.Color.Yellow;
            this.el212.Location = new System.Drawing.Point(265, 114);
            this.el212.Margin = new System.Windows.Forms.Padding(0);
            this.el212.Name = "el212";
            this.el212.Size = new System.Drawing.Size(24, 28);
            this.el212.TabIndex = 269;
            this.el212.Text = "2";
            this.el212.UseVisualStyleBackColor = false;
            this.el212.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el214
            // 
            this.el214.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el214.BackColor = System.Drawing.Color.Transparent;
            this.el214.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el214.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el214.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el214.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el214.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el214.ForeColor = System.Drawing.Color.Yellow;
            this.el214.Location = new System.Drawing.Point(318, 114);
            this.el214.Margin = new System.Windows.Forms.Padding(0);
            this.el214.Name = "el214";
            this.el214.Size = new System.Drawing.Size(24, 28);
            this.el214.TabIndex = 273;
            this.el214.Text = "4";
            this.el214.UseVisualStyleBackColor = false;
            this.el214.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el213
            // 
            this.el213.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el213.BackColor = System.Drawing.Color.Transparent;
            this.el213.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el213.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el213.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el213.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el213.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el213.ForeColor = System.Drawing.Color.Yellow;
            this.el213.Location = new System.Drawing.Point(290, 114);
            this.el213.Margin = new System.Windows.Forms.Padding(0);
            this.el213.Name = "el213";
            this.el213.Size = new System.Drawing.Size(24, 28);
            this.el213.TabIndex = 270;
            this.el213.Text = "3";
            this.el213.UseVisualStyleBackColor = false;
            this.el213.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el209
            // 
            this.el209.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el209.BackColor = System.Drawing.Color.Transparent;
            this.el209.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el209.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el209.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el209.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el209.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el209.ForeColor = System.Drawing.Color.Yellow;
            this.el209.Location = new System.Drawing.Point(318, 79);
            this.el209.Margin = new System.Windows.Forms.Padding(0);
            this.el209.Name = "el209";
            this.el209.Size = new System.Drawing.Size(24, 28);
            this.el209.TabIndex = 272;
            this.el209.Text = "4";
            this.el209.UseVisualStyleBackColor = false;
            this.el209.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // el204
            // 
            this.el204.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el204.BackColor = System.Drawing.Color.Transparent;
            this.el204.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el204.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el204.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el204.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el204.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el204.ForeColor = System.Drawing.Color.Yellow;
            this.el204.Location = new System.Drawing.Point(318, 46);
            this.el204.Margin = new System.Windows.Forms.Padding(0);
            this.el204.Name = "el204";
            this.el204.Size = new System.Drawing.Size(24, 28);
            this.el204.TabIndex = 271;
            this.el204.Text = "4";
            this.el204.UseVisualStyleBackColor = false;
            this.el204.Click += new System.EventHandler(this.Insert_Lifts);
            // 
            // label38
            // 
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label38.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Lime;
            this.label38.Location = new System.Drawing.Point(29, 45);
            this.label38.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(247, 32);
            this.label38.TabIndex = 261;
            this.label38.Tag = "6";
            this.label38.Text = "PANCAKE TWIST";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label37
            // 
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label37.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Lime;
            this.label37.Location = new System.Drawing.Point(29, 77);
            this.label37.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(247, 32);
            this.label37.TabIndex = 257;
            this.label37.Tag = "6";
            this.label37.Text = "K E N N E D Y";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label36
            // 
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label36.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label36.Location = new System.Drawing.Point(29, 111);
            this.label36.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(247, 32);
            this.label36.TabIndex = 258;
            this.label36.Text = "M I L A T A N O ";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label35
            // 
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label35.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label35.Location = new System.Drawing.Point(29, 146);
            this.label35.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(247, 32);
            this.label35.TabIndex = 259;
            this.label35.Text = "REV C.WHEEL";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label34.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.GreenYellow;
            this.label34.Location = new System.Drawing.Point(29, 180);
            this.label34.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(247, 32);
            this.label34.TabIndex = 260;
            this.label34.Text = "SPIN PANCAKE";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // imList
            // 
            this.imList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imList.ImageStream")));
            this.imList.TransparentColor = System.Drawing.Color.Transparent;
            this.imList.Images.SetKeyName(0, "jumps.png");
            this.imList.Images.SetKeyName(1, "throwjumps.png");
            this.imList.Images.SetKeyName(2, "twilutz.png");
            this.imList.Images.SetKeyName(3, "lifts.png");
            this.imList.Images.SetKeyName(4, "lifts2.png");
            this.imList.Images.SetKeyName(5, "lifts3.png");
            this.imList.Images.SetKeyName(6, "spins.png");
            this.imList.Images.SetKeyName(7, "contactspins.png");
            this.imList.Images.SetKeyName(8, "spirals.png");
            this.imList.Images.SetKeyName(9, "steps.png");
            this.imList.Images.SetKeyName(10, "deductions.png");
            // 
            // tabJ
            // 
            this.tabJ.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabJ.Controls.Add(this.tabJumps);
            this.tabJ.Controls.Add(this.tabLanciati);
            this.tabJ.Controls.Add(this.tabTwist);
            this.tabJ.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabJ.Enabled = false;
            this.tabJ.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabJ.ImageList = this.imList;
            this.tabJ.ItemSize = new System.Drawing.Size(80, 40);
            this.tabJ.Location = new System.Drawing.Point(0, 0);
            this.tabJ.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabJ.Name = "tabJ";
            this.tabJ.SelectedIndex = 0;
            this.tabJ.Size = new System.Drawing.Size(390, 296);
            this.tabJ.TabIndex = 0;
            this.tabJ.SelectedIndexChanged += new System.EventHandler(this.tabJ_SelectedIndexChanged);
            // 
            // tabJumps
            // 
            this.tabJumps.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.tabJumps.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabJumps.Controls.Add(this.el0);
            this.tabJumps.Controls.Add(this.half);
            this.tabJumps.Controls.Add(this.cbCombo);
            this.tabJumps.Controls.Add(this.label40);
            this.tabJumps.Controls.Add(this.newCombo);
            this.tabJumps.Controls.Add(this.el64);
            this.tabJumps.Controls.Add(this.under);
            this.tabJumps.Controls.Add(this.down);
            this.tabJumps.Controls.Add(this.el23);
            this.tabJumps.Controls.Add(this.el22);
            this.tabJumps.Controls.Add(this.el21);
            this.tabJumps.Controls.Add(this.el20);
            this.tabJumps.Controls.Add(this.el19);
            this.tabJumps.Controls.Add(this.el17);
            this.tabJumps.Controls.Add(this.el11);
            this.tabJumps.Controls.Add(this.el5);
            this.tabJumps.Controls.Add(this.label21);
            this.tabJumps.Controls.Add(this.el16);
            this.tabJumps.Controls.Add(this.el10);
            this.tabJumps.Controls.Add(this.el4);
            this.tabJumps.Controls.Add(this.label20);
            this.tabJumps.Controls.Add(this.el15);
            this.tabJumps.Controls.Add(this.el9);
            this.tabJumps.Controls.Add(this.el3);
            this.tabJumps.Controls.Add(this.label19);
            this.tabJumps.Controls.Add(this.el13);
            this.tabJumps.Controls.Add(this.el8);
            this.tabJumps.Controls.Add(this.el2);
            this.tabJumps.Controls.Add(this.label18);
            this.tabJumps.Controls.Add(this.el14);
            this.tabJumps.Controls.Add(this.el7);
            this.tabJumps.Controls.Add(this.el1);
            this.tabJumps.Controls.Add(this.label17);
            this.tabJumps.Controls.Add(this.el18);
            this.tabJumps.Controls.Add(this.el12);
            this.tabJumps.Controls.Add(this.el6);
            this.tabJumps.Controls.Add(this.label16);
            this.tabJumps.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabJumps.Location = new System.Drawing.Point(4, 44);
            this.tabJumps.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabJumps.Name = "tabJumps";
            this.tabJumps.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabJumps.Size = new System.Drawing.Size(382, 248);
            this.tabJumps.TabIndex = 0;
            this.tabJumps.Text = "Jumps";
            // 
            // el0
            // 
            this.el0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el0.BackColor = System.Drawing.Color.Transparent;
            this.el0.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el0.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el0.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el0.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el0.ForeColor = System.Drawing.Color.Yellow;
            this.el0.Location = new System.Drawing.Point(211, 44);
            this.el0.Margin = new System.Windows.Forms.Padding(0);
            this.el0.Name = "el0";
            this.el0.Size = new System.Drawing.Size(24, 28);
            this.el0.TabIndex = 178;
            this.el0.Text = "W";
            this.el0.UseVisualStyleBackColor = false;
            this.el0.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // half
            // 
            this.half.BackColor = System.Drawing.Color.Silver;
            this.half.Cursor = System.Windows.Forms.Cursors.Hand;
            this.half.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.half.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.half.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.half.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.half.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.half.ForeColor = System.Drawing.Color.Black;
            this.half.Location = new System.Drawing.Point(9, 109);
            this.half.Margin = new System.Windows.Forms.Padding(2);
            this.half.Name = "half";
            this.half.Size = new System.Drawing.Size(74, 26);
            this.half.TabIndex = 177;
            this.half.Text = "HALF";
            this.half.UseVisualStyleBackColor = false;
            this.half.Click += new System.EventHandler(this.half_Click);
            // 
            // cbCombo
            // 
            this.cbCombo.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbCombo.BackColor = System.Drawing.Color.White;
            this.cbCombo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbCombo.FlatAppearance.CheckedBackColor = System.Drawing.Color.Lime;
            this.cbCombo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Yellow;
            this.cbCombo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cbCombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbCombo.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCombo.ForeColor = System.Drawing.Color.Black;
            this.cbCombo.Location = new System.Drawing.Point(9, 14);
            this.cbCombo.Margin = new System.Windows.Forms.Padding(0);
            this.cbCombo.Name = "cbCombo";
            this.cbCombo.Size = new System.Drawing.Size(74, 26);
            this.cbCombo.TabIndex = 176;
            this.cbCombo.Text = "Combo";
            this.cbCombo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbCombo.UseVisualStyleBackColor = false;
            this.cbCombo.CheckedChanged += new System.EventHandler(this.cbCombo_CheckedChanged);
            // 
            // label40
            // 
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label40.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.White;
            this.label40.Location = new System.Drawing.Point(86, 10);
            this.label40.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(137, 32);
            this.label40.TabIndex = 175;
            this.label40.Tag = "6";
            this.label40.Text = "NO LEVEL";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // newCombo
            // 
            this.newCombo.BackColor = System.Drawing.Color.Transparent;
            this.newCombo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.newCombo.Enabled = false;
            this.newCombo.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.newCombo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.newCombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.newCombo.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newCombo.ForeColor = System.Drawing.Color.Lime;
            this.newCombo.Location = new System.Drawing.Point(9, 45);
            this.newCombo.Margin = new System.Windows.Forms.Padding(0);
            this.newCombo.Name = "newCombo";
            this.newCombo.Size = new System.Drawing.Size(74, 28);
            this.newCombo.TabIndex = 143;
            this.newCombo.Text = "New";
            this.newCombo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.newCombo.UseVisualStyleBackColor = false;
            this.newCombo.Click += new System.EventHandler(this.newCombo_Click);
            // 
            // el64
            // 
            this.el64.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el64.BackColor = System.Drawing.Color.Transparent;
            this.el64.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el64.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el64.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el64.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el64.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el64.ForeColor = System.Drawing.Color.Yellow;
            this.el64.Location = new System.Drawing.Point(240, 10);
            this.el64.Margin = new System.Windows.Forms.Padding(0);
            this.el64.Name = "el64";
            this.el64.Size = new System.Drawing.Size(24, 28);
            this.el64.TabIndex = 142;
            this.el64.Tag = "NJ";
            this.el64.Text = "0";
            this.el64.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.el64.UseVisualStyleBackColor = false;
            this.el64.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // under
            // 
            this.under.BackColor = System.Drawing.Color.Silver;
            this.under.Cursor = System.Windows.Forms.Cursors.Hand;
            this.under.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.under.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.under.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.under.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.under.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.under.ForeColor = System.Drawing.Color.Black;
            this.under.Location = new System.Drawing.Point(9, 78);
            this.under.Margin = new System.Windows.Forms.Padding(2);
            this.under.Name = "under";
            this.under.Size = new System.Drawing.Size(74, 26);
            this.under.TabIndex = 114;
            this.under.Text = "UNDER";
            this.under.UseVisualStyleBackColor = false;
            this.under.Click += new System.EventHandler(this.under_Click);
            // 
            // down
            // 
            this.down.BackColor = System.Drawing.Color.Silver;
            this.down.Cursor = System.Windows.Forms.Cursors.Hand;
            this.down.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.down.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.down.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.down.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.down.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.down.ForeColor = System.Drawing.Color.Black;
            this.down.Location = new System.Drawing.Point(9, 140);
            this.down.Margin = new System.Windows.Forms.Padding(2);
            this.down.Name = "down";
            this.down.Size = new System.Drawing.Size(74, 26);
            this.down.TabIndex = 113;
            this.down.Text = "DOWN";
            this.down.UseVisualStyleBackColor = false;
            this.down.Click += new System.EventHandler(this.down_Click);
            // 
            // el23
            // 
            this.el23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el23.BackColor = System.Drawing.Color.Transparent;
            this.el23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el23.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el23.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el23.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el23.ForeColor = System.Drawing.Color.Yellow;
            this.el23.Location = new System.Drawing.Point(330, 213);
            this.el23.Margin = new System.Windows.Forms.Padding(0);
            this.el23.Name = "el23";
            this.el23.Size = new System.Drawing.Size(24, 28);
            this.el23.TabIndex = 140;
            this.el23.Text = "4";
            this.el23.UseVisualStyleBackColor = false;
            this.el23.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el22
            // 
            this.el22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el22.BackColor = System.Drawing.Color.Transparent;
            this.el22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el22.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el22.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el22.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el22.ForeColor = System.Drawing.Color.Yellow;
            this.el22.Location = new System.Drawing.Point(330, 179);
            this.el22.Margin = new System.Windows.Forms.Padding(0);
            this.el22.Name = "el22";
            this.el22.Size = new System.Drawing.Size(24, 28);
            this.el22.TabIndex = 139;
            this.el22.Text = "4";
            this.el22.UseVisualStyleBackColor = false;
            this.el22.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el21
            // 
            this.el21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el21.BackColor = System.Drawing.Color.Transparent;
            this.el21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el21.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el21.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el21.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el21.ForeColor = System.Drawing.Color.Yellow;
            this.el21.Location = new System.Drawing.Point(330, 145);
            this.el21.Margin = new System.Windows.Forms.Padding(0);
            this.el21.Name = "el21";
            this.el21.Size = new System.Drawing.Size(24, 28);
            this.el21.TabIndex = 138;
            this.el21.Text = "4";
            this.el21.UseVisualStyleBackColor = false;
            this.el21.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el20
            // 
            this.el20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el20.BackColor = System.Drawing.Color.Transparent;
            this.el20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el20.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el20.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el20.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el20.ForeColor = System.Drawing.Color.Yellow;
            this.el20.Location = new System.Drawing.Point(330, 112);
            this.el20.Margin = new System.Windows.Forms.Padding(0);
            this.el20.Name = "el20";
            this.el20.Size = new System.Drawing.Size(24, 28);
            this.el20.TabIndex = 137;
            this.el20.Text = "4";
            this.el20.UseVisualStyleBackColor = false;
            this.el20.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el19
            // 
            this.el19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el19.BackColor = System.Drawing.Color.Transparent;
            this.el19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el19.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el19.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el19.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el19.ForeColor = System.Drawing.Color.Yellow;
            this.el19.Location = new System.Drawing.Point(330, 78);
            this.el19.Margin = new System.Windows.Forms.Padding(0);
            this.el19.Name = "el19";
            this.el19.Size = new System.Drawing.Size(24, 28);
            this.el19.TabIndex = 136;
            this.el19.Text = "4";
            this.el19.UseVisualStyleBackColor = false;
            this.el19.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el17
            // 
            this.el17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el17.BackColor = System.Drawing.Color.Transparent;
            this.el17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el17.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el17.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el17.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el17.ForeColor = System.Drawing.Color.Yellow;
            this.el17.Location = new System.Drawing.Point(300, 213);
            this.el17.Margin = new System.Windows.Forms.Padding(0);
            this.el17.Name = "el17";
            this.el17.Size = new System.Drawing.Size(24, 28);
            this.el17.TabIndex = 134;
            this.el17.Text = "3";
            this.el17.UseVisualStyleBackColor = false;
            this.el17.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el11
            // 
            this.el11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el11.BackColor = System.Drawing.Color.Transparent;
            this.el11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el11.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el11.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el11.ForeColor = System.Drawing.Color.Yellow;
            this.el11.Location = new System.Drawing.Point(270, 213);
            this.el11.Margin = new System.Windows.Forms.Padding(0);
            this.el11.Name = "el11";
            this.el11.Size = new System.Drawing.Size(24, 28);
            this.el11.TabIndex = 133;
            this.el11.Text = "2";
            this.el11.UseVisualStyleBackColor = false;
            this.el11.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el5
            // 
            this.el5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el5.BackColor = System.Drawing.Color.Transparent;
            this.el5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el5.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el5.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el5.ForeColor = System.Drawing.Color.Yellow;
            this.el5.Location = new System.Drawing.Point(240, 213);
            this.el5.Margin = new System.Windows.Forms.Padding(0);
            this.el5.Name = "el5";
            this.el5.Size = new System.Drawing.Size(24, 28);
            this.el5.TabIndex = 132;
            this.el5.Text = "1";
            this.el5.UseVisualStyleBackColor = false;
            this.el5.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // label21
            // 
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label21.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(86, 209);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(163, 32);
            this.label21.TabIndex = 131;
            this.label21.Text = "L O O P";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label21.Click += new System.EventHandler(this.label21_Click);
            // 
            // el16
            // 
            this.el16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el16.BackColor = System.Drawing.Color.Transparent;
            this.el16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el16.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el16.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el16.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el16.ForeColor = System.Drawing.Color.Yellow;
            this.el16.Location = new System.Drawing.Point(300, 179);
            this.el16.Margin = new System.Windows.Forms.Padding(0);
            this.el16.Name = "el16";
            this.el16.Size = new System.Drawing.Size(24, 28);
            this.el16.TabIndex = 130;
            this.el16.Text = "3";
            this.el16.UseVisualStyleBackColor = false;
            this.el16.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el10
            // 
            this.el10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el10.BackColor = System.Drawing.Color.Transparent;
            this.el10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el10.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el10.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el10.ForeColor = System.Drawing.Color.Yellow;
            this.el10.Location = new System.Drawing.Point(270, 179);
            this.el10.Margin = new System.Windows.Forms.Padding(0);
            this.el10.Name = "el10";
            this.el10.Size = new System.Drawing.Size(24, 28);
            this.el10.TabIndex = 129;
            this.el10.Text = "2";
            this.el10.UseVisualStyleBackColor = false;
            this.el10.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el4
            // 
            this.el4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el4.BackColor = System.Drawing.Color.Transparent;
            this.el4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el4.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el4.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el4.ForeColor = System.Drawing.Color.Yellow;
            this.el4.Location = new System.Drawing.Point(240, 179);
            this.el4.Margin = new System.Windows.Forms.Padding(0);
            this.el4.Name = "el4";
            this.el4.Size = new System.Drawing.Size(24, 28);
            this.el4.TabIndex = 128;
            this.el4.Text = "1";
            this.el4.UseVisualStyleBackColor = false;
            this.el4.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label20.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Cyan;
            this.label20.Location = new System.Drawing.Point(86, 176);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(163, 32);
            this.label20.TabIndex = 127;
            this.label20.Text = "L U T Z";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label20.Click += new System.EventHandler(this.label20_Click);
            // 
            // el15
            // 
            this.el15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el15.BackColor = System.Drawing.Color.Transparent;
            this.el15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el15.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el15.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el15.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el15.ForeColor = System.Drawing.Color.Yellow;
            this.el15.Location = new System.Drawing.Point(300, 145);
            this.el15.Margin = new System.Windows.Forms.Padding(0);
            this.el15.Name = "el15";
            this.el15.Size = new System.Drawing.Size(24, 28);
            this.el15.TabIndex = 126;
            this.el15.Text = "3";
            this.el15.UseVisualStyleBackColor = false;
            this.el15.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el9
            // 
            this.el9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el9.BackColor = System.Drawing.Color.Transparent;
            this.el9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el9.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el9.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el9.ForeColor = System.Drawing.Color.Yellow;
            this.el9.Location = new System.Drawing.Point(270, 145);
            this.el9.Margin = new System.Windows.Forms.Padding(0);
            this.el9.Name = "el9";
            this.el9.Size = new System.Drawing.Size(24, 28);
            this.el9.TabIndex = 125;
            this.el9.Text = "2";
            this.el9.UseVisualStyleBackColor = false;
            this.el9.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el3
            // 
            this.el3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el3.BackColor = System.Drawing.Color.Transparent;
            this.el3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el3.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el3.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el3.ForeColor = System.Drawing.Color.Yellow;
            this.el3.Location = new System.Drawing.Point(240, 145);
            this.el3.Margin = new System.Windows.Forms.Padding(0);
            this.el3.Name = "el3";
            this.el3.Size = new System.Drawing.Size(24, 28);
            this.el3.TabIndex = 124;
            this.el3.Text = "1";
            this.el3.UseVisualStyleBackColor = false;
            this.el3.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label19.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label19.Location = new System.Drawing.Point(86, 142);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(163, 32);
            this.label19.TabIndex = 123;
            this.label19.Text = "F L I P";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label19.Click += new System.EventHandler(this.label19_Click);
            // 
            // el13
            // 
            this.el13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el13.BackColor = System.Drawing.Color.Transparent;
            this.el13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el13.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el13.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el13.ForeColor = System.Drawing.Color.Yellow;
            this.el13.Location = new System.Drawing.Point(300, 112);
            this.el13.Margin = new System.Windows.Forms.Padding(0);
            this.el13.Name = "el13";
            this.el13.Size = new System.Drawing.Size(24, 28);
            this.el13.TabIndex = 122;
            this.el13.Text = "3";
            this.el13.UseVisualStyleBackColor = false;
            this.el13.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el8
            // 
            this.el8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el8.BackColor = System.Drawing.Color.Transparent;
            this.el8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el8.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el8.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el8.ForeColor = System.Drawing.Color.Yellow;
            this.el8.Location = new System.Drawing.Point(270, 112);
            this.el8.Margin = new System.Windows.Forms.Padding(0);
            this.el8.Name = "el8";
            this.el8.Size = new System.Drawing.Size(24, 28);
            this.el8.TabIndex = 121;
            this.el8.Text = "2";
            this.el8.UseVisualStyleBackColor = false;
            this.el8.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el2
            // 
            this.el2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el2.BackColor = System.Drawing.Color.Transparent;
            this.el2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el2.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el2.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el2.ForeColor = System.Drawing.Color.Yellow;
            this.el2.Location = new System.Drawing.Point(240, 112);
            this.el2.Margin = new System.Windows.Forms.Padding(0);
            this.el2.Name = "el2";
            this.el2.Size = new System.Drawing.Size(24, 28);
            this.el2.TabIndex = 120;
            this.el2.Text = "1";
            this.el2.UseVisualStyleBackColor = false;
            this.el2.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label18.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Turquoise;
            this.label18.Location = new System.Drawing.Point(86, 110);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(163, 32);
            this.label18.TabIndex = 119;
            this.label18.Text = "SALCHOW";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el14
            // 
            this.el14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el14.BackColor = System.Drawing.Color.Transparent;
            this.el14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el14.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el14.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el14.ForeColor = System.Drawing.Color.Yellow;
            this.el14.Location = new System.Drawing.Point(300, 78);
            this.el14.Margin = new System.Windows.Forms.Padding(0);
            this.el14.Name = "el14";
            this.el14.Size = new System.Drawing.Size(24, 28);
            this.el14.TabIndex = 118;
            this.el14.Text = "3";
            this.el14.UseVisualStyleBackColor = false;
            this.el14.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el7
            // 
            this.el7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el7.BackColor = System.Drawing.Color.Transparent;
            this.el7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el7.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el7.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el7.ForeColor = System.Drawing.Color.Yellow;
            this.el7.Location = new System.Drawing.Point(270, 78);
            this.el7.Margin = new System.Windows.Forms.Padding(0);
            this.el7.Name = "el7";
            this.el7.Size = new System.Drawing.Size(24, 28);
            this.el7.TabIndex = 117;
            this.el7.Text = "2";
            this.el7.UseVisualStyleBackColor = false;
            this.el7.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el1
            // 
            this.el1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el1.BackColor = System.Drawing.Color.Transparent;
            this.el1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el1.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el1.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el1.ForeColor = System.Drawing.Color.Yellow;
            this.el1.Location = new System.Drawing.Point(240, 78);
            this.el1.Margin = new System.Windows.Forms.Padding(0);
            this.el1.Name = "el1";
            this.el1.Size = new System.Drawing.Size(24, 28);
            this.el1.TabIndex = 116;
            this.el1.Text = "1";
            this.el1.UseVisualStyleBackColor = false;
            this.el1.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label17.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label17.Location = new System.Drawing.Point(86, 77);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(163, 32);
            this.label17.TabIndex = 115;
            this.label17.Text = "TOE LOOP";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el18
            // 
            this.el18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el18.BackColor = System.Drawing.Color.Transparent;
            this.el18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el18.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el18.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el18.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el18.ForeColor = System.Drawing.Color.Yellow;
            this.el18.Location = new System.Drawing.Point(300, 44);
            this.el18.Margin = new System.Windows.Forms.Padding(0);
            this.el18.Name = "el18";
            this.el18.Size = new System.Drawing.Size(24, 28);
            this.el18.TabIndex = 112;
            this.el18.Text = "3";
            this.el18.UseVisualStyleBackColor = false;
            this.el18.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el12
            // 
            this.el12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el12.BackColor = System.Drawing.Color.Transparent;
            this.el12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el12.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el12.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el12.ForeColor = System.Drawing.Color.Yellow;
            this.el12.Location = new System.Drawing.Point(270, 44);
            this.el12.Margin = new System.Windows.Forms.Padding(0);
            this.el12.Name = "el12";
            this.el12.Size = new System.Drawing.Size(24, 28);
            this.el12.TabIndex = 111;
            this.el12.Text = "2";
            this.el12.UseVisualStyleBackColor = false;
            this.el12.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el6
            // 
            this.el6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el6.BackColor = System.Drawing.Color.Transparent;
            this.el6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el6.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el6.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el6.ForeColor = System.Drawing.Color.Yellow;
            this.el6.Location = new System.Drawing.Point(240, 44);
            this.el6.Margin = new System.Windows.Forms.Padding(0);
            this.el6.Name = "el6";
            this.el6.Size = new System.Drawing.Size(24, 28);
            this.el6.TabIndex = 110;
            this.el6.Text = "1";
            this.el6.UseVisualStyleBackColor = false;
            this.el6.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label16.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label16.Location = new System.Drawing.Point(86, 43);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(163, 32);
            this.label16.TabIndex = 109;
            this.label16.Tag = "6";
            this.label16.Text = "A X E L";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabLanciati
            // 
            this.tabLanciati.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.tabLanciati.Controls.Add(this.el75);
            this.tabLanciati.Controls.Add(this.half2);
            this.tabLanciati.Controls.Add(this.under2);
            this.tabLanciati.Controls.Add(this.down2);
            this.tabLanciati.Controls.Add(this.label39);
            this.tabLanciati.Controls.Add(this.el226);
            this.tabLanciati.Controls.Add(this.el94);
            this.tabLanciati.Controls.Add(this.el95);
            this.tabLanciati.Controls.Add(this.el93);
            this.tabLanciati.Controls.Add(this.el92);
            this.tabLanciati.Controls.Add(this.el89);
            this.tabLanciati.Controls.Add(this.el84);
            this.tabLanciati.Controls.Add(this.el79);
            this.tabLanciati.Controls.Add(this.label1);
            this.tabLanciati.Controls.Add(this.el90);
            this.tabLanciati.Controls.Add(this.el85);
            this.tabLanciati.Controls.Add(this.el80);
            this.tabLanciati.Controls.Add(this.label2);
            this.tabLanciati.Controls.Add(this.el88);
            this.tabLanciati.Controls.Add(this.el83);
            this.tabLanciati.Controls.Add(this.el78);
            this.tabLanciati.Controls.Add(this.label4);
            this.tabLanciati.Controls.Add(this.el87);
            this.tabLanciati.Controls.Add(this.el82);
            this.tabLanciati.Controls.Add(this.el77);
            this.tabLanciati.Controls.Add(this.label5);
            this.tabLanciati.Controls.Add(this.el91);
            this.tabLanciati.Controls.Add(this.el86);
            this.tabLanciati.Controls.Add(this.el81);
            this.tabLanciati.Controls.Add(this.label6);
            this.tabLanciati.Location = new System.Drawing.Point(4, 44);
            this.tabLanciati.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabLanciati.Name = "tabLanciati";
            this.tabLanciati.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabLanciati.Size = new System.Drawing.Size(382, 248);
            this.tabLanciati.TabIndex = 1;
            this.tabLanciati.Text = "Throw Jumps";
            // 
            // el75
            // 
            this.el75.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el75.BackColor = System.Drawing.Color.Transparent;
            this.el75.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el75.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el75.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el75.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el75.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el75.ForeColor = System.Drawing.Color.Yellow;
            this.el75.Location = new System.Drawing.Point(213, 45);
            this.el75.Margin = new System.Windows.Forms.Padding(0);
            this.el75.Name = "el75";
            this.el75.Size = new System.Drawing.Size(24, 28);
            this.el75.TabIndex = 181;
            this.el75.Text = "W";
            this.el75.UseVisualStyleBackColor = false;
            this.el75.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // half2
            // 
            this.half2.BackColor = System.Drawing.Color.Silver;
            this.half2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.half2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.half2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.half2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.half2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.half2.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.half2.ForeColor = System.Drawing.Color.Black;
            this.half2.Location = new System.Drawing.Point(10, 45);
            this.half2.Margin = new System.Windows.Forms.Padding(2);
            this.half2.Name = "half2";
            this.half2.Size = new System.Drawing.Size(74, 26);
            this.half2.TabIndex = 180;
            this.half2.Text = "HALF";
            this.half2.UseVisualStyleBackColor = false;
            this.half2.Click += new System.EventHandler(this.half2_Click);
            // 
            // under2
            // 
            this.under2.BackColor = System.Drawing.Color.Silver;
            this.under2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.under2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.under2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.under2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.under2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.under2.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.under2.ForeColor = System.Drawing.Color.Black;
            this.under2.Location = new System.Drawing.Point(9, 14);
            this.under2.Margin = new System.Windows.Forms.Padding(2);
            this.under2.Name = "under2";
            this.under2.Size = new System.Drawing.Size(74, 26);
            this.under2.TabIndex = 179;
            this.under2.Text = "UNDER";
            this.under2.UseVisualStyleBackColor = false;
            this.under2.Click += new System.EventHandler(this.under2_Click);
            // 
            // down2
            // 
            this.down2.BackColor = System.Drawing.Color.Silver;
            this.down2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.down2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.down2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.down2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.down2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.down2.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.down2.ForeColor = System.Drawing.Color.Black;
            this.down2.Location = new System.Drawing.Point(10, 76);
            this.down2.Margin = new System.Windows.Forms.Padding(2);
            this.down2.Name = "down2";
            this.down2.Size = new System.Drawing.Size(74, 26);
            this.down2.TabIndex = 178;
            this.down2.Text = "DOWN";
            this.down2.UseVisualStyleBackColor = false;
            this.down2.Click += new System.EventHandler(this.down2_Click);
            // 
            // label39
            // 
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label39.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.White;
            this.label39.Location = new System.Drawing.Point(87, 11);
            this.label39.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(142, 32);
            this.label39.TabIndex = 174;
            this.label39.Tag = "6";
            this.label39.Text = "NO LEVEL";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el226
            // 
            this.el226.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el226.BackColor = System.Drawing.Color.Transparent;
            this.el226.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el226.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el226.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el226.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el226.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el226.ForeColor = System.Drawing.Color.Yellow;
            this.el226.Location = new System.Drawing.Point(242, 11);
            this.el226.Margin = new System.Windows.Forms.Padding(0);
            this.el226.Name = "el226";
            this.el226.Size = new System.Drawing.Size(24, 28);
            this.el226.TabIndex = 173;
            this.el226.Text = "0";
            this.el226.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.el226.UseVisualStyleBackColor = false;
            this.el226.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el94
            // 
            this.el94.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el94.BackColor = System.Drawing.Color.Transparent;
            this.el94.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el94.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el94.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el94.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el94.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el94.ForeColor = System.Drawing.Color.Yellow;
            this.el94.Location = new System.Drawing.Point(332, 180);
            this.el94.Margin = new System.Windows.Forms.Padding(0);
            this.el94.Name = "el94";
            this.el94.Size = new System.Drawing.Size(24, 28);
            this.el94.TabIndex = 172;
            this.el94.Text = "4";
            this.el94.UseVisualStyleBackColor = false;
            this.el94.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el95
            // 
            this.el95.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el95.BackColor = System.Drawing.Color.Transparent;
            this.el95.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el95.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el95.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el95.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el95.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el95.ForeColor = System.Drawing.Color.Yellow;
            this.el95.Location = new System.Drawing.Point(332, 146);
            this.el95.Margin = new System.Windows.Forms.Padding(0);
            this.el95.Name = "el95";
            this.el95.Size = new System.Drawing.Size(24, 28);
            this.el95.TabIndex = 171;
            this.el95.Text = "4";
            this.el95.UseVisualStyleBackColor = false;
            this.el95.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el93
            // 
            this.el93.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el93.BackColor = System.Drawing.Color.Transparent;
            this.el93.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el93.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el93.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el93.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el93.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el93.ForeColor = System.Drawing.Color.Yellow;
            this.el93.Location = new System.Drawing.Point(332, 113);
            this.el93.Margin = new System.Windows.Forms.Padding(0);
            this.el93.Name = "el93";
            this.el93.Size = new System.Drawing.Size(24, 28);
            this.el93.TabIndex = 169;
            this.el93.Text = "4";
            this.el93.UseVisualStyleBackColor = false;
            this.el93.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el92
            // 
            this.el92.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el92.BackColor = System.Drawing.Color.Transparent;
            this.el92.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el92.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el92.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el92.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el92.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el92.ForeColor = System.Drawing.Color.Yellow;
            this.el92.Location = new System.Drawing.Point(332, 79);
            this.el92.Margin = new System.Windows.Forms.Padding(0);
            this.el92.Name = "el92";
            this.el92.Size = new System.Drawing.Size(24, 28);
            this.el92.TabIndex = 168;
            this.el92.Text = "4";
            this.el92.UseVisualStyleBackColor = false;
            this.el92.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el89
            // 
            this.el89.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el89.BackColor = System.Drawing.Color.Transparent;
            this.el89.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el89.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el89.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el89.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el89.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el89.ForeColor = System.Drawing.Color.Yellow;
            this.el89.Location = new System.Drawing.Point(302, 180);
            this.el89.Margin = new System.Windows.Forms.Padding(0);
            this.el89.Name = "el89";
            this.el89.Size = new System.Drawing.Size(24, 28);
            this.el89.TabIndex = 166;
            this.el89.Text = "3";
            this.el89.UseVisualStyleBackColor = false;
            this.el89.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el84
            // 
            this.el84.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el84.BackColor = System.Drawing.Color.Transparent;
            this.el84.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el84.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el84.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el84.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el84.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el84.ForeColor = System.Drawing.Color.Yellow;
            this.el84.Location = new System.Drawing.Point(272, 180);
            this.el84.Margin = new System.Windows.Forms.Padding(0);
            this.el84.Name = "el84";
            this.el84.Size = new System.Drawing.Size(24, 28);
            this.el84.TabIndex = 165;
            this.el84.Text = "2";
            this.el84.UseVisualStyleBackColor = false;
            this.el84.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el79
            // 
            this.el79.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el79.BackColor = System.Drawing.Color.Transparent;
            this.el79.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el79.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el79.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el79.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el79.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el79.ForeColor = System.Drawing.Color.Yellow;
            this.el79.Location = new System.Drawing.Point(242, 180);
            this.el79.Margin = new System.Windows.Forms.Padding(0);
            this.el79.Name = "el79";
            this.el79.Size = new System.Drawing.Size(24, 28);
            this.el79.TabIndex = 164;
            this.el79.Text = "1";
            this.el79.UseVisualStyleBackColor = false;
            this.el79.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(87, 177);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 32);
            this.label1.TabIndex = 163;
            this.label1.Text = "L O O P";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el90
            // 
            this.el90.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el90.BackColor = System.Drawing.Color.Transparent;
            this.el90.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el90.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el90.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el90.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el90.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el90.ForeColor = System.Drawing.Color.Yellow;
            this.el90.Location = new System.Drawing.Point(302, 146);
            this.el90.Margin = new System.Windows.Forms.Padding(0);
            this.el90.Name = "el90";
            this.el90.Size = new System.Drawing.Size(24, 28);
            this.el90.TabIndex = 162;
            this.el90.Text = "3";
            this.el90.UseVisualStyleBackColor = false;
            this.el90.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el85
            // 
            this.el85.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el85.BackColor = System.Drawing.Color.Transparent;
            this.el85.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el85.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el85.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el85.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el85.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el85.ForeColor = System.Drawing.Color.Yellow;
            this.el85.Location = new System.Drawing.Point(272, 146);
            this.el85.Margin = new System.Windows.Forms.Padding(0);
            this.el85.Name = "el85";
            this.el85.Size = new System.Drawing.Size(24, 28);
            this.el85.TabIndex = 161;
            this.el85.Text = "2";
            this.el85.UseVisualStyleBackColor = false;
            this.el85.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el80
            // 
            this.el80.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el80.BackColor = System.Drawing.Color.Transparent;
            this.el80.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el80.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el80.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el80.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el80.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el80.ForeColor = System.Drawing.Color.Yellow;
            this.el80.Location = new System.Drawing.Point(242, 146);
            this.el80.Margin = new System.Windows.Forms.Padding(0);
            this.el80.Name = "el80";
            this.el80.Size = new System.Drawing.Size(24, 28);
            this.el80.TabIndex = 160;
            this.el80.Text = "1";
            this.el80.UseVisualStyleBackColor = false;
            this.el80.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Cyan;
            this.label2.Location = new System.Drawing.Point(87, 143);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 32);
            this.label2.TabIndex = 159;
            this.label2.Text = "F L I P";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // el88
            // 
            this.el88.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el88.BackColor = System.Drawing.Color.Transparent;
            this.el88.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el88.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el88.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el88.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el88.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el88.ForeColor = System.Drawing.Color.Yellow;
            this.el88.Location = new System.Drawing.Point(302, 113);
            this.el88.Margin = new System.Windows.Forms.Padding(0);
            this.el88.Name = "el88";
            this.el88.Size = new System.Drawing.Size(24, 28);
            this.el88.TabIndex = 154;
            this.el88.Text = "3";
            this.el88.UseVisualStyleBackColor = false;
            this.el88.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el83
            // 
            this.el83.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el83.BackColor = System.Drawing.Color.Transparent;
            this.el83.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el83.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el83.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el83.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el83.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el83.ForeColor = System.Drawing.Color.Yellow;
            this.el83.Location = new System.Drawing.Point(272, 113);
            this.el83.Margin = new System.Windows.Forms.Padding(0);
            this.el83.Name = "el83";
            this.el83.Size = new System.Drawing.Size(24, 28);
            this.el83.TabIndex = 153;
            this.el83.Text = "2";
            this.el83.UseVisualStyleBackColor = false;
            this.el83.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el78
            // 
            this.el78.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el78.BackColor = System.Drawing.Color.Transparent;
            this.el78.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el78.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el78.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el78.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el78.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el78.ForeColor = System.Drawing.Color.Yellow;
            this.el78.Location = new System.Drawing.Point(242, 113);
            this.el78.Margin = new System.Windows.Forms.Padding(0);
            this.el78.Name = "el78";
            this.el78.Size = new System.Drawing.Size(24, 28);
            this.el78.TabIndex = 152;
            this.el78.Text = "1";
            this.el78.UseVisualStyleBackColor = false;
            this.el78.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Cyan;
            this.label4.Location = new System.Drawing.Point(87, 111);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 32);
            this.label4.TabIndex = 151;
            this.label4.Text = "SALCHOW";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // el87
            // 
            this.el87.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el87.BackColor = System.Drawing.Color.Transparent;
            this.el87.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el87.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el87.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el87.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el87.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el87.ForeColor = System.Drawing.Color.Yellow;
            this.el87.Location = new System.Drawing.Point(302, 79);
            this.el87.Margin = new System.Windows.Forms.Padding(0);
            this.el87.Name = "el87";
            this.el87.Size = new System.Drawing.Size(24, 28);
            this.el87.TabIndex = 150;
            this.el87.Text = "3";
            this.el87.UseVisualStyleBackColor = false;
            this.el87.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el82
            // 
            this.el82.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el82.BackColor = System.Drawing.Color.Transparent;
            this.el82.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el82.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el82.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el82.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el82.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el82.ForeColor = System.Drawing.Color.Yellow;
            this.el82.Location = new System.Drawing.Point(272, 79);
            this.el82.Margin = new System.Windows.Forms.Padding(0);
            this.el82.Name = "el82";
            this.el82.Size = new System.Drawing.Size(24, 28);
            this.el82.TabIndex = 149;
            this.el82.Text = "2";
            this.el82.UseVisualStyleBackColor = false;
            this.el82.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el77
            // 
            this.el77.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el77.BackColor = System.Drawing.Color.Transparent;
            this.el77.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el77.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el77.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el77.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el77.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el77.ForeColor = System.Drawing.Color.Yellow;
            this.el77.Location = new System.Drawing.Point(242, 79);
            this.el77.Margin = new System.Windows.Forms.Padding(0);
            this.el77.Name = "el77";
            this.el77.Size = new System.Drawing.Size(24, 28);
            this.el77.TabIndex = 148;
            this.el77.Text = "1";
            this.el77.UseVisualStyleBackColor = false;
            this.el77.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label5.Location = new System.Drawing.Point(87, 78);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 32);
            this.label5.TabIndex = 147;
            this.label5.Text = "TOE LOOP";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // el91
            // 
            this.el91.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el91.BackColor = System.Drawing.Color.Transparent;
            this.el91.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el91.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el91.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el91.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el91.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el91.ForeColor = System.Drawing.Color.Yellow;
            this.el91.Location = new System.Drawing.Point(302, 45);
            this.el91.Margin = new System.Windows.Forms.Padding(0);
            this.el91.Name = "el91";
            this.el91.Size = new System.Drawing.Size(24, 28);
            this.el91.TabIndex = 144;
            this.el91.Text = "3";
            this.el91.UseVisualStyleBackColor = false;
            this.el91.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el86
            // 
            this.el86.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el86.BackColor = System.Drawing.Color.Transparent;
            this.el86.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el86.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el86.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el86.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el86.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el86.ForeColor = System.Drawing.Color.Yellow;
            this.el86.Location = new System.Drawing.Point(272, 45);
            this.el86.Margin = new System.Windows.Forms.Padding(0);
            this.el86.Name = "el86";
            this.el86.Size = new System.Drawing.Size(24, 28);
            this.el86.TabIndex = 143;
            this.el86.Text = "2";
            this.el86.UseVisualStyleBackColor = false;
            this.el86.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el81
            // 
            this.el81.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el81.BackColor = System.Drawing.Color.Transparent;
            this.el81.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el81.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el81.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el81.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el81.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el81.ForeColor = System.Drawing.Color.Yellow;
            this.el81.Location = new System.Drawing.Point(242, 45);
            this.el81.Margin = new System.Windows.Forms.Padding(0);
            this.el81.Name = "el81";
            this.el81.Size = new System.Drawing.Size(24, 28);
            this.el81.TabIndex = 142;
            this.el81.Text = "1";
            this.el81.UseVisualStyleBackColor = false;
            this.el81.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label6.Location = new System.Drawing.Point(87, 44);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(152, 32);
            this.label6.TabIndex = 141;
            this.label6.Tag = "6";
            this.label6.Text = "A X E L";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // tabTwist
            // 
            this.tabTwist.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.tabTwist.Controls.Add(this.half3);
            this.tabTwist.Controls.Add(this.under3);
            this.tabTwist.Controls.Add(this.down3);
            this.tabTwist.Controls.Add(this.el102);
            this.tabTwist.Controls.Add(this.label65);
            this.tabTwist.Controls.Add(this.el118);
            this.tabTwist.Controls.Add(this.el113);
            this.tabTwist.Controls.Add(this.el108);
            this.tabTwist.Controls.Add(this.label48);
            this.tabTwist.Controls.Add(this.el122);
            this.tabTwist.Controls.Add(this.el121);
            this.tabTwist.Controls.Add(this.el119);
            this.tabTwist.Controls.Add(this.el120);
            this.tabTwist.Controls.Add(this.el117);
            this.tabTwist.Controls.Add(this.el116);
            this.tabTwist.Controls.Add(this.el115);
            this.tabTwist.Controls.Add(this.el114);
            this.tabTwist.Controls.Add(this.label47);
            this.tabTwist.Controls.Add(this.el112);
            this.tabTwist.Controls.Add(this.el111);
            this.tabTwist.Controls.Add(this.el110);
            this.tabTwist.Controls.Add(this.el109);
            this.tabTwist.Controls.Add(this.label33);
            this.tabTwist.Controls.Add(this.el107);
            this.tabTwist.Controls.Add(this.el106);
            this.tabTwist.Controls.Add(this.el105);
            this.tabTwist.Controls.Add(this.el104);
            this.tabTwist.Controls.Add(this.label30);
            this.tabTwist.Controls.Add(this.el103);
            this.tabTwist.Location = new System.Drawing.Point(4, 44);
            this.tabTwist.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabTwist.Name = "tabTwist";
            this.tabTwist.Size = new System.Drawing.Size(382, 248);
            this.tabTwist.TabIndex = 2;
            this.tabTwist.Text = "Twist";
            // 
            // half3
            // 
            this.half3.BackColor = System.Drawing.Color.Silver;
            this.half3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.half3.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.half3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.half3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.half3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.half3.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.half3.ForeColor = System.Drawing.Color.Black;
            this.half3.Location = new System.Drawing.Point(10, 45);
            this.half3.Margin = new System.Windows.Forms.Padding(2);
            this.half3.Name = "half3";
            this.half3.Size = new System.Drawing.Size(74, 26);
            this.half3.TabIndex = 183;
            this.half3.Text = "HALF";
            this.half3.UseVisualStyleBackColor = false;
            this.half3.Click += new System.EventHandler(this.half3_Click);
            // 
            // under3
            // 
            this.under3.BackColor = System.Drawing.Color.Silver;
            this.under3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.under3.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.under3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.under3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.under3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.under3.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.under3.ForeColor = System.Drawing.Color.Black;
            this.under3.Location = new System.Drawing.Point(9, 14);
            this.under3.Margin = new System.Windows.Forms.Padding(2);
            this.under3.Name = "under3";
            this.under3.Size = new System.Drawing.Size(74, 26);
            this.under3.TabIndex = 182;
            this.under3.Text = "UNDER";
            this.under3.UseVisualStyleBackColor = false;
            this.under3.Click += new System.EventHandler(this.under3_Click);
            // 
            // down3
            // 
            this.down3.BackColor = System.Drawing.Color.Silver;
            this.down3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.down3.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.down3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.down3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.down3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.down3.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.down3.ForeColor = System.Drawing.Color.Black;
            this.down3.Location = new System.Drawing.Point(10, 76);
            this.down3.Margin = new System.Windows.Forms.Padding(2);
            this.down3.Name = "down3";
            this.down3.Size = new System.Drawing.Size(74, 26);
            this.down3.TabIndex = 181;
            this.down3.Text = "DOWN";
            this.down3.UseVisualStyleBackColor = false;
            this.down3.Click += new System.EventHandler(this.down3_Click);
            // 
            // el102
            // 
            this.el102.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el102.BackColor = System.Drawing.Color.Transparent;
            this.el102.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el102.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el102.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el102.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el102.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el102.ForeColor = System.Drawing.Color.Yellow;
            this.el102.Location = new System.Drawing.Point(227, 12);
            this.el102.Margin = new System.Windows.Forms.Padding(0);
            this.el102.Name = "el102";
            this.el102.Size = new System.Drawing.Size(24, 28);
            this.el102.TabIndex = 197;
            this.el102.Text = "0";
            this.el102.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.el102.UseVisualStyleBackColor = false;
            this.el102.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // label65
            // 
            this.label65.BackColor = System.Drawing.Color.Transparent;
            this.label65.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label65.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.White;
            this.label65.Location = new System.Drawing.Point(87, 10);
            this.label65.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(138, 32);
            this.label65.TabIndex = 198;
            this.label65.Tag = "6";
            this.label65.Text = "NO LEVEL";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el118
            // 
            this.el118.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el118.BackColor = System.Drawing.Color.Transparent;
            this.el118.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el118.Enabled = false;
            this.el118.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el118.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el118.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el118.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el118.ForeColor = System.Drawing.Color.Yellow;
            this.el118.Location = new System.Drawing.Point(227, 146);
            this.el118.Margin = new System.Windows.Forms.Padding(0);
            this.el118.Name = "el118";
            this.el118.Size = new System.Drawing.Size(24, 28);
            this.el118.TabIndex = 196;
            this.el118.Text = "0";
            this.el118.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.el118.UseVisualStyleBackColor = false;
            this.el118.Visible = false;
            this.el118.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el113
            // 
            this.el113.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el113.BackColor = System.Drawing.Color.Transparent;
            this.el113.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el113.Enabled = false;
            this.el113.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el113.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el113.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el113.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el113.ForeColor = System.Drawing.Color.Yellow;
            this.el113.Location = new System.Drawing.Point(227, 113);
            this.el113.Margin = new System.Windows.Forms.Padding(0);
            this.el113.Name = "el113";
            this.el113.Size = new System.Drawing.Size(24, 28);
            this.el113.TabIndex = 195;
            this.el113.Text = "0";
            this.el113.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.el113.UseVisualStyleBackColor = false;
            this.el113.Visible = false;
            this.el113.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el108
            // 
            this.el108.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el108.BackColor = System.Drawing.Color.Transparent;
            this.el108.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el108.Enabled = false;
            this.el108.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el108.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el108.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el108.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el108.ForeColor = System.Drawing.Color.Yellow;
            this.el108.Location = new System.Drawing.Point(227, 79);
            this.el108.Margin = new System.Windows.Forms.Padding(0);
            this.el108.Name = "el108";
            this.el108.Size = new System.Drawing.Size(24, 28);
            this.el108.TabIndex = 194;
            this.el108.Text = "0";
            this.el108.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.el108.UseVisualStyleBackColor = false;
            this.el108.Visible = false;
            this.el108.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // label48
            // 
            this.label48.BackColor = System.Drawing.Color.Transparent;
            this.label48.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label48.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label48.Location = new System.Drawing.Point(88, 145);
            this.label48.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(134, 32);
            this.label48.TabIndex = 193;
            this.label48.Tag = "6";
            this.label48.Text = "TWIST 4R";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el122
            // 
            this.el122.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el122.BackColor = System.Drawing.Color.Transparent;
            this.el122.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el122.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el122.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el122.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el122.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el122.ForeColor = System.Drawing.Color.Yellow;
            this.el122.Location = new System.Drawing.Point(343, 146);
            this.el122.Margin = new System.Windows.Forms.Padding(0);
            this.el122.Name = "el122";
            this.el122.Size = new System.Drawing.Size(24, 28);
            this.el122.TabIndex = 187;
            this.el122.Text = "4";
            this.el122.UseVisualStyleBackColor = false;
            this.el122.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el121
            // 
            this.el121.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el121.BackColor = System.Drawing.Color.Transparent;
            this.el121.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el121.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el121.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el121.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el121.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el121.ForeColor = System.Drawing.Color.Yellow;
            this.el121.Location = new System.Drawing.Point(314, 146);
            this.el121.Margin = new System.Windows.Forms.Padding(0);
            this.el121.Name = "el121";
            this.el121.Size = new System.Drawing.Size(24, 28);
            this.el121.TabIndex = 186;
            this.el121.Text = "3";
            this.el121.UseVisualStyleBackColor = false;
            this.el121.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el119
            // 
            this.el119.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el119.BackColor = System.Drawing.Color.Transparent;
            this.el119.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el119.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el119.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el119.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el119.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el119.ForeColor = System.Drawing.Color.Yellow;
            this.el119.Location = new System.Drawing.Point(256, 146);
            this.el119.Margin = new System.Windows.Forms.Padding(0);
            this.el119.Name = "el119";
            this.el119.Size = new System.Drawing.Size(24, 28);
            this.el119.TabIndex = 184;
            this.el119.Text = "1";
            this.el119.UseVisualStyleBackColor = false;
            this.el119.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el120
            // 
            this.el120.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el120.BackColor = System.Drawing.Color.Transparent;
            this.el120.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el120.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el120.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el120.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el120.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el120.ForeColor = System.Drawing.Color.Yellow;
            this.el120.Location = new System.Drawing.Point(285, 146);
            this.el120.Margin = new System.Windows.Forms.Padding(0);
            this.el120.Name = "el120";
            this.el120.Size = new System.Drawing.Size(24, 28);
            this.el120.TabIndex = 185;
            this.el120.Text = "2";
            this.el120.UseVisualStyleBackColor = false;
            this.el120.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el117
            // 
            this.el117.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el117.BackColor = System.Drawing.Color.Transparent;
            this.el117.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el117.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el117.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el117.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el117.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el117.ForeColor = System.Drawing.Color.Yellow;
            this.el117.Location = new System.Drawing.Point(343, 113);
            this.el117.Margin = new System.Windows.Forms.Padding(0);
            this.el117.Name = "el117";
            this.el117.Size = new System.Drawing.Size(24, 28);
            this.el117.TabIndex = 192;
            this.el117.Text = "4";
            this.el117.UseVisualStyleBackColor = false;
            this.el117.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el116
            // 
            this.el116.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el116.BackColor = System.Drawing.Color.Transparent;
            this.el116.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el116.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el116.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el116.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el116.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el116.ForeColor = System.Drawing.Color.Yellow;
            this.el116.Location = new System.Drawing.Point(314, 113);
            this.el116.Margin = new System.Windows.Forms.Padding(0);
            this.el116.Name = "el116";
            this.el116.Size = new System.Drawing.Size(24, 28);
            this.el116.TabIndex = 191;
            this.el116.Text = "3";
            this.el116.UseVisualStyleBackColor = false;
            this.el116.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el115
            // 
            this.el115.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el115.BackColor = System.Drawing.Color.Transparent;
            this.el115.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el115.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el115.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el115.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el115.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el115.ForeColor = System.Drawing.Color.Yellow;
            this.el115.Location = new System.Drawing.Point(285, 113);
            this.el115.Margin = new System.Windows.Forms.Padding(0);
            this.el115.Name = "el115";
            this.el115.Size = new System.Drawing.Size(24, 28);
            this.el115.TabIndex = 190;
            this.el115.Text = "2";
            this.el115.UseVisualStyleBackColor = false;
            this.el115.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el114
            // 
            this.el114.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el114.BackColor = System.Drawing.Color.Transparent;
            this.el114.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el114.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el114.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el114.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el114.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el114.ForeColor = System.Drawing.Color.Yellow;
            this.el114.Location = new System.Drawing.Point(256, 113);
            this.el114.Margin = new System.Windows.Forms.Padding(0);
            this.el114.Name = "el114";
            this.el114.Size = new System.Drawing.Size(24, 28);
            this.el114.TabIndex = 189;
            this.el114.Text = "1";
            this.el114.UseVisualStyleBackColor = false;
            this.el114.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // label47
            // 
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label47.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Aqua;
            this.label47.Location = new System.Drawing.Point(88, 110);
            this.label47.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(131, 32);
            this.label47.TabIndex = 188;
            this.label47.Tag = "6";
            this.label47.Text = "TWIST 3R";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el112
            // 
            this.el112.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el112.BackColor = System.Drawing.Color.Transparent;
            this.el112.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el112.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el112.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el112.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el112.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el112.ForeColor = System.Drawing.Color.Yellow;
            this.el112.Location = new System.Drawing.Point(343, 79);
            this.el112.Margin = new System.Windows.Forms.Padding(0);
            this.el112.Name = "el112";
            this.el112.Size = new System.Drawing.Size(24, 28);
            this.el112.TabIndex = 187;
            this.el112.Text = "4";
            this.el112.UseVisualStyleBackColor = false;
            this.el112.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el111
            // 
            this.el111.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el111.BackColor = System.Drawing.Color.Transparent;
            this.el111.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el111.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el111.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el111.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el111.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el111.ForeColor = System.Drawing.Color.Yellow;
            this.el111.Location = new System.Drawing.Point(314, 79);
            this.el111.Margin = new System.Windows.Forms.Padding(0);
            this.el111.Name = "el111";
            this.el111.Size = new System.Drawing.Size(24, 28);
            this.el111.TabIndex = 186;
            this.el111.Text = "3";
            this.el111.UseVisualStyleBackColor = false;
            this.el111.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el110
            // 
            this.el110.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el110.BackColor = System.Drawing.Color.Transparent;
            this.el110.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el110.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el110.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el110.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el110.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el110.ForeColor = System.Drawing.Color.Yellow;
            this.el110.Location = new System.Drawing.Point(285, 79);
            this.el110.Margin = new System.Windows.Forms.Padding(0);
            this.el110.Name = "el110";
            this.el110.Size = new System.Drawing.Size(24, 28);
            this.el110.TabIndex = 185;
            this.el110.Text = "2";
            this.el110.UseVisualStyleBackColor = false;
            this.el110.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el109
            // 
            this.el109.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el109.BackColor = System.Drawing.Color.Transparent;
            this.el109.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el109.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el109.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el109.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el109.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el109.ForeColor = System.Drawing.Color.Yellow;
            this.el109.Location = new System.Drawing.Point(256, 79);
            this.el109.Margin = new System.Windows.Forms.Padding(0);
            this.el109.Name = "el109";
            this.el109.Size = new System.Drawing.Size(24, 28);
            this.el109.TabIndex = 184;
            this.el109.Text = "1";
            this.el109.UseVisualStyleBackColor = false;
            this.el109.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // label33
            // 
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label33.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label33.Location = new System.Drawing.Point(88, 76);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(137, 32);
            this.label33.TabIndex = 183;
            this.label33.Tag = "6";
            this.label33.Text = "TWIST 2R";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el107
            // 
            this.el107.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el107.BackColor = System.Drawing.Color.Transparent;
            this.el107.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el107.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el107.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el107.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el107.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el107.ForeColor = System.Drawing.Color.Yellow;
            this.el107.Location = new System.Drawing.Point(343, 46);
            this.el107.Margin = new System.Windows.Forms.Padding(0);
            this.el107.Name = "el107";
            this.el107.Size = new System.Drawing.Size(24, 28);
            this.el107.TabIndex = 182;
            this.el107.Text = "4";
            this.el107.UseVisualStyleBackColor = false;
            this.el107.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el106
            // 
            this.el106.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el106.BackColor = System.Drawing.Color.Transparent;
            this.el106.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el106.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el106.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el106.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el106.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el106.ForeColor = System.Drawing.Color.Yellow;
            this.el106.Location = new System.Drawing.Point(314, 46);
            this.el106.Margin = new System.Windows.Forms.Padding(0);
            this.el106.Name = "el106";
            this.el106.Size = new System.Drawing.Size(24, 28);
            this.el106.TabIndex = 181;
            this.el106.Text = "3";
            this.el106.UseVisualStyleBackColor = false;
            this.el106.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el105
            // 
            this.el105.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el105.BackColor = System.Drawing.Color.Transparent;
            this.el105.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el105.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el105.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el105.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el105.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el105.ForeColor = System.Drawing.Color.Yellow;
            this.el105.Location = new System.Drawing.Point(285, 46);
            this.el105.Margin = new System.Windows.Forms.Padding(0);
            this.el105.Name = "el105";
            this.el105.Size = new System.Drawing.Size(24, 28);
            this.el105.TabIndex = 180;
            this.el105.Text = "2";
            this.el105.UseVisualStyleBackColor = false;
            this.el105.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // el104
            // 
            this.el104.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el104.BackColor = System.Drawing.Color.Transparent;
            this.el104.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el104.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el104.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el104.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el104.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el104.ForeColor = System.Drawing.Color.Yellow;
            this.el104.Location = new System.Drawing.Point(256, 46);
            this.el104.Margin = new System.Windows.Forms.Padding(0);
            this.el104.Name = "el104";
            this.el104.Size = new System.Drawing.Size(24, 28);
            this.el104.TabIndex = 179;
            this.el104.Text = "1";
            this.el104.UseVisualStyleBackColor = false;
            this.el104.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label30.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label30.Location = new System.Drawing.Point(88, 42);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(137, 32);
            this.label30.TabIndex = 178;
            this.label30.Tag = "6";
            this.label30.Text = "TWIST 1R";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el103
            // 
            this.el103.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el103.BackColor = System.Drawing.Color.Transparent;
            this.el103.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el103.Enabled = false;
            this.el103.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el103.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el103.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el103.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el103.ForeColor = System.Drawing.Color.Yellow;
            this.el103.Location = new System.Drawing.Point(227, 46);
            this.el103.Margin = new System.Windows.Forms.Padding(0);
            this.el103.Name = "el103";
            this.el103.Size = new System.Drawing.Size(24, 28);
            this.el103.TabIndex = 176;
            this.el103.Text = "0";
            this.el103.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.el103.UseVisualStyleBackColor = false;
            this.el103.Visible = false;
            this.el103.Click += new System.EventHandler(this.InsertElement_Click);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.tabSpins);
            this.panel6.Controls.Add(this.tabVarie);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(819, 49);
            this.panel6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(391, 594);
            this.panel6.TabIndex = 12;
            // 
            // tabSpins
            // 
            this.tabSpins.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabSpins.Controls.Add(this.tabSpin);
            this.tabSpins.Controls.Add(this.tabCspin1);
            this.tabSpins.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabSpins.Enabled = false;
            this.tabSpins.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabSpins.ImageList = this.imList;
            this.tabSpins.ItemSize = new System.Drawing.Size(80, 40);
            this.tabSpins.Location = new System.Drawing.Point(0, 0);
            this.tabSpins.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabSpins.Name = "tabSpins";
            this.tabSpins.SelectedIndex = 0;
            this.tabSpins.Size = new System.Drawing.Size(391, 313);
            this.tabSpins.TabIndex = 4;
            // 
            // tabSpin
            // 
            this.tabSpin.AutoScroll = true;
            this.tabSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.tabSpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabSpin.Controls.Add(this.el26);
            this.tabSpin.Controls.Add(this.el41);
            this.tabSpin.Controls.Add(this.el46);
            this.tabSpin.Controls.Add(this.el50);
            this.tabSpin.Controls.Add(this.el51);
            this.tabSpin.Controls.Add(this.el52);
            this.tabSpin.Controls.Add(this.checkCombo1);
            this.tabSpin.Controls.Add(this.cbBonusSpin);
            this.tabSpin.Controls.Add(this.newComboSpin);
            this.tabSpin.Controls.Add(this.cb49);
            this.tabSpin.Controls.Add(this.cb25);
            this.tabSpin.Controls.Add(this.cb47);
            this.tabSpin.Controls.Add(this.cb40);
            this.tabSpin.Controls.Add(this.cb48);
            this.tabSpin.Controls.Add(this.cb45);
            this.tabSpin.Controls.Add(this.el65);
            this.tabSpin.Controls.Add(this.el25);
            this.tabSpin.Controls.Add(this.el40);
            this.tabSpin.Controls.Add(this.el45);
            this.tabSpin.Controls.Add(this.el48);
            this.tabSpin.Controls.Add(this.el47);
            this.tabSpin.Controls.Add(this.el49);
            this.tabSpin.Controls.Add(this.label49);
            this.tabSpin.Controls.Add(this.label50);
            this.tabSpin.Controls.Add(this.label51);
            this.tabSpin.Controls.Add(this.label52);
            this.tabSpin.Controls.Add(this.label53);
            this.tabSpin.Controls.Add(this.label54);
            this.tabSpin.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabSpin.Location = new System.Drawing.Point(4, 44);
            this.tabSpin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabSpin.Name = "tabSpin";
            this.tabSpin.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabSpin.Size = new System.Drawing.Size(383, 265);
            this.tabSpin.TabIndex = 0;
            this.tabSpin.Text = "Side by side Spins";
            // 
            // el26
            // 
            this.el26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el26.BackColor = System.Drawing.Color.Transparent;
            this.el26.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el26.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el26.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el26.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el26.ForeColor = System.Drawing.Color.Yellow;
            this.el26.Location = new System.Drawing.Point(216, 84);
            this.el26.Margin = new System.Windows.Forms.Padding(2);
            this.el26.Name = "el26";
            this.el26.Size = new System.Drawing.Size(24, 28);
            this.el26.TabIndex = 223;
            this.el26.Tag = "cbUp";
            this.el26.Text = "0";
            this.el26.UseVisualStyleBackColor = false;
            this.el26.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el41
            // 
            this.el41.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el41.BackColor = System.Drawing.Color.Transparent;
            this.el41.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el41.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el41.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el41.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el41.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el41.ForeColor = System.Drawing.Color.Yellow;
            this.el41.Location = new System.Drawing.Point(216, 117);
            this.el41.Margin = new System.Windows.Forms.Padding(2);
            this.el41.Name = "el41";
            this.el41.Size = new System.Drawing.Size(24, 28);
            this.el41.TabIndex = 222;
            this.el41.Tag = "cbSit";
            this.el41.Text = "0";
            this.el41.UseVisualStyleBackColor = false;
            this.el41.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el46
            // 
            this.el46.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el46.BackColor = System.Drawing.Color.Transparent;
            this.el46.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el46.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el46.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el46.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el46.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el46.ForeColor = System.Drawing.Color.Yellow;
            this.el46.Location = new System.Drawing.Point(216, 150);
            this.el46.Margin = new System.Windows.Forms.Padding(2);
            this.el46.Name = "el46";
            this.el46.Size = new System.Drawing.Size(24, 28);
            this.el46.TabIndex = 221;
            this.el46.Tag = "cbCamel";
            this.el46.Text = "0";
            this.el46.UseVisualStyleBackColor = false;
            this.el46.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el50
            // 
            this.el50.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el50.BackColor = System.Drawing.Color.Transparent;
            this.el50.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el50.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el50.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el50.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el50.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el50.ForeColor = System.Drawing.Color.Yellow;
            this.el50.Location = new System.Drawing.Point(216, 216);
            this.el50.Margin = new System.Windows.Forms.Padding(2);
            this.el50.Name = "el50";
            this.el50.Size = new System.Drawing.Size(24, 28);
            this.el50.TabIndex = 220;
            this.el50.Tag = "cbHeel";
            this.el50.Text = "0";
            this.el50.UseVisualStyleBackColor = false;
            this.el50.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el51
            // 
            this.el51.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el51.BackColor = System.Drawing.Color.Transparent;
            this.el51.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el51.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el51.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el51.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el51.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el51.ForeColor = System.Drawing.Color.Yellow;
            this.el51.Location = new System.Drawing.Point(216, 183);
            this.el51.Margin = new System.Windows.Forms.Padding(2);
            this.el51.Name = "el51";
            this.el51.Size = new System.Drawing.Size(24, 28);
            this.el51.TabIndex = 219;
            this.el51.Tag = "cbBroken";
            this.el51.Text = "0";
            this.el51.UseVisualStyleBackColor = false;
            this.el51.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el52
            // 
            this.el52.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el52.BackColor = System.Drawing.Color.Transparent;
            this.el52.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el52.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el52.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el52.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el52.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el52.ForeColor = System.Drawing.Color.Yellow;
            this.el52.Location = new System.Drawing.Point(216, 249);
            this.el52.Margin = new System.Windows.Forms.Padding(2);
            this.el52.Name = "el52";
            this.el52.Size = new System.Drawing.Size(24, 28);
            this.el52.TabIndex = 218;
            this.el52.Tag = "cbInverted";
            this.el52.Text = "0";
            this.el52.UseVisualStyleBackColor = false;
            this.el52.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // checkCombo1
            // 
            this.checkCombo1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkCombo1.BackColor = System.Drawing.Color.Black;
            this.checkCombo1.Location = new System.Drawing.Point(149, 14);
            this.checkCombo1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkCombo1.Name = "checkCombo1";
            this.checkCombo1.OffFont = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkCombo1.OffForeColor = System.Drawing.Color.Yellow;
            this.checkCombo1.OffText = "SoloSpin";
            this.checkCombo1.OnFont = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkCombo1.OnForeColor = System.Drawing.Color.Yellow;
            this.checkCombo1.OnText = "ComboSpin";
            this.checkCombo1.Size = new System.Drawing.Size(149, 29);
            this.checkCombo1.Style = JCS.ToggleSwitch.ToggleSwitchStyle.Fancy;
            this.checkCombo1.TabIndex = 162;
            // 
            // cbBonusSpin
            // 
            this.cbBonusSpin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbBonusSpin.AutoSize = true;
            this.cbBonusSpin.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBonusSpin.ForeColor = System.Drawing.Color.Yellow;
            this.cbBonusSpin.Location = new System.Drawing.Point(245, 52);
            this.cbBonusSpin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbBonusSpin.Name = "cbBonusSpin";
            this.cbBonusSpin.Size = new System.Drawing.Size(63, 24);
            this.cbBonusSpin.TabIndex = 217;
            this.cbBonusSpin.Text = "Bonus";
            this.cbBonusSpin.UseVisualStyleBackColor = true;
            // 
            // newComboSpin
            // 
            this.newComboSpin.BackColor = System.Drawing.Color.Transparent;
            this.newComboSpin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.newComboSpin.Enabled = false;
            this.newComboSpin.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.newComboSpin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.newComboSpin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.newComboSpin.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newComboSpin.ForeColor = System.Drawing.Color.Yellow;
            this.newComboSpin.Location = new System.Drawing.Point(22, 14);
            this.newComboSpin.Margin = new System.Windows.Forms.Padding(0);
            this.newComboSpin.Name = "newComboSpin";
            this.newComboSpin.Size = new System.Drawing.Size(62, 29);
            this.newComboSpin.TabIndex = 216;
            this.newComboSpin.Text = "New";
            this.newComboSpin.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.newComboSpin.UseVisualStyleBackColor = false;
            this.newComboSpin.Click += new System.EventHandler(this.newComboSpin_Click);
            // 
            // cb49
            // 
            this.cb49.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb49.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb49.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb49.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb49.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb49.ForeColor = System.Drawing.Color.Yellow;
            this.cb49.FormattingEnabled = true;
            this.cb49.Location = new System.Drawing.Point(245, 249);
            this.cb49.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb49.Name = "cb49";
            this.cb49.Size = new System.Drawing.Size(68, 28);
            this.cb49.TabIndex = 164;
            // 
            // cb25
            // 
            this.cb25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb25.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb25.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb25.ForeColor = System.Drawing.Color.Yellow;
            this.cb25.FormattingEnabled = true;
            this.cb25.Location = new System.Drawing.Point(245, 84);
            this.cb25.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb25.Name = "cb25";
            this.cb25.Size = new System.Drawing.Size(68, 28);
            this.cb25.TabIndex = 159;
            // 
            // cb47
            // 
            this.cb47.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb47.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb47.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb47.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb47.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb47.ForeColor = System.Drawing.Color.Yellow;
            this.cb47.FormattingEnabled = true;
            this.cb47.Location = new System.Drawing.Point(245, 216);
            this.cb47.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb47.Name = "cb47";
            this.cb47.Size = new System.Drawing.Size(68, 28);
            this.cb47.TabIndex = 163;
            // 
            // cb40
            // 
            this.cb40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb40.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb40.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb40.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb40.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb40.ForeColor = System.Drawing.Color.Yellow;
            this.cb40.FormattingEnabled = true;
            this.cb40.Location = new System.Drawing.Point(245, 117);
            this.cb40.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb40.Name = "cb40";
            this.cb40.Size = new System.Drawing.Size(68, 28);
            this.cb40.TabIndex = 160;
            // 
            // cb48
            // 
            this.cb48.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb48.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb48.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb48.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb48.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb48.ForeColor = System.Drawing.Color.Yellow;
            this.cb48.FormattingEnabled = true;
            this.cb48.Location = new System.Drawing.Point(245, 183);
            this.cb48.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb48.Name = "cb48";
            this.cb48.Size = new System.Drawing.Size(68, 28);
            this.cb48.TabIndex = 162;
            // 
            // cb45
            // 
            this.cb45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb45.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb45.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb45.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb45.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb45.ForeColor = System.Drawing.Color.Yellow;
            this.cb45.FormattingEnabled = true;
            this.cb45.Location = new System.Drawing.Point(245, 150);
            this.cb45.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb45.Name = "cb45";
            this.cb45.Size = new System.Drawing.Size(68, 28);
            this.cb45.TabIndex = 161;
            // 
            // el65
            // 
            this.el65.BackColor = System.Drawing.Color.Transparent;
            this.el65.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el65.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el65.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el65.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el65.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el65.ForeColor = System.Drawing.Color.Yellow;
            this.el65.Location = new System.Drawing.Point(22, 14);
            this.el65.Margin = new System.Windows.Forms.Padding(0);
            this.el65.Name = "el65";
            this.el65.Size = new System.Drawing.Size(24, 28);
            this.el65.TabIndex = 213;
            this.el65.Tag = "NJ";
            this.el65.Text = "0";
            this.el65.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.el65.UseVisualStyleBackColor = false;
            this.el65.Visible = false;
            this.el65.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el25
            // 
            this.el25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el25.BackColor = System.Drawing.Color.Transparent;
            this.el25.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el25.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el25.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el25.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el25.ForeColor = System.Drawing.Color.Yellow;
            this.el25.Location = new System.Drawing.Point(318, 84);
            this.el25.Margin = new System.Windows.Forms.Padding(2);
            this.el25.Name = "el25";
            this.el25.Size = new System.Drawing.Size(24, 28);
            this.el25.TabIndex = 194;
            this.el25.Text = "1";
            this.el25.UseVisualStyleBackColor = false;
            this.el25.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el40
            // 
            this.el40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el40.BackColor = System.Drawing.Color.Transparent;
            this.el40.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el40.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el40.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el40.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el40.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el40.ForeColor = System.Drawing.Color.Yellow;
            this.el40.Location = new System.Drawing.Point(318, 117);
            this.el40.Margin = new System.Windows.Forms.Padding(2);
            this.el40.Name = "el40";
            this.el40.Size = new System.Drawing.Size(24, 28);
            this.el40.TabIndex = 189;
            this.el40.Text = "1";
            this.el40.UseVisualStyleBackColor = false;
            this.el40.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el45
            // 
            this.el45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el45.BackColor = System.Drawing.Color.Transparent;
            this.el45.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el45.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el45.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el45.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el45.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el45.ForeColor = System.Drawing.Color.Yellow;
            this.el45.Location = new System.Drawing.Point(318, 150);
            this.el45.Margin = new System.Windows.Forms.Padding(2);
            this.el45.Name = "el45";
            this.el45.Size = new System.Drawing.Size(24, 28);
            this.el45.TabIndex = 186;
            this.el45.Text = "1";
            this.el45.UseVisualStyleBackColor = false;
            this.el45.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el48
            // 
            this.el48.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el48.BackColor = System.Drawing.Color.Transparent;
            this.el48.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el48.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el48.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el48.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el48.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el48.ForeColor = System.Drawing.Color.Yellow;
            this.el48.Location = new System.Drawing.Point(318, 183);
            this.el48.Margin = new System.Windows.Forms.Padding(2);
            this.el48.Name = "el48";
            this.el48.Size = new System.Drawing.Size(24, 28);
            this.el48.TabIndex = 183;
            this.el48.Tag = "cbHeel";
            this.el48.Text = "1";
            this.el48.UseVisualStyleBackColor = false;
            this.el48.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el47
            // 
            this.el47.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el47.BackColor = System.Drawing.Color.Transparent;
            this.el47.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el47.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el47.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el47.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el47.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el47.ForeColor = System.Drawing.Color.Yellow;
            this.el47.Location = new System.Drawing.Point(318, 216);
            this.el47.Margin = new System.Windows.Forms.Padding(2);
            this.el47.Name = "el47";
            this.el47.Size = new System.Drawing.Size(24, 28);
            this.el47.TabIndex = 180;
            this.el47.Text = "1";
            this.el47.UseVisualStyleBackColor = false;
            this.el47.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el49
            // 
            this.el49.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el49.BackColor = System.Drawing.Color.Transparent;
            this.el49.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el49.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el49.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el49.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el49.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el49.ForeColor = System.Drawing.Color.Yellow;
            this.el49.Location = new System.Drawing.Point(318, 249);
            this.el49.Margin = new System.Windows.Forms.Padding(2);
            this.el49.Name = "el49";
            this.el49.Size = new System.Drawing.Size(24, 28);
            this.el49.TabIndex = 177;
            this.el49.Text = "1";
            this.el49.UseVisualStyleBackColor = false;
            this.el49.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // label49
            // 
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label49.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label49.Location = new System.Drawing.Point(16, 82);
            this.label49.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(183, 32);
            this.label49.TabIndex = 188;
            this.label49.Tag = "6";
            this.label49.Text = "UPRIGHT SPIN";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label50
            // 
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label50.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label50.Location = new System.Drawing.Point(16, 115);
            this.label50.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(183, 32);
            this.label50.TabIndex = 199;
            this.label50.Tag = "6";
            this.label50.Text = "SIT SPIN";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label51
            // 
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label51.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label51.Location = new System.Drawing.Point(16, 148);
            this.label51.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(183, 32);
            this.label51.TabIndex = 200;
            this.label51.Text = "CAMEL SPIN";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label52
            // 
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label52.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.White;
            this.label52.Location = new System.Drawing.Point(16, 247);
            this.label52.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(183, 32);
            this.label52.TabIndex = 203;
            this.label52.Text = "I N V E R T E D";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label53
            // 
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label53.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Red;
            this.label53.Location = new System.Drawing.Point(16, 214);
            this.label53.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(183, 32);
            this.label53.TabIndex = 202;
            this.label53.Text = "B R O K E N";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label54
            // 
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label54.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label54.Location = new System.Drawing.Point(16, 181);
            this.label54.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(157, 32);
            this.label54.TabIndex = 201;
            this.label54.Text = "H E E L";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabCspin1
            // 
            this.tabCspin1.AutoScroll = true;
            this.tabCspin1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.tabCspin1.Controls.Add(this.cbBonusSpin2);
            this.tabCspin1.Controls.Add(this.checkCombo3);
            this.tabCspin1.Controls.Add(this.newComboSpin2);
            this.tabCspin1.Controls.Add(this.panelContactCombo);
            this.tabCspin1.Controls.Add(this.el65_);
            this.tabCspin1.Location = new System.Drawing.Point(4, 44);
            this.tabCspin1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabCspin1.Name = "tabCspin1";
            this.tabCspin1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabCspin1.Size = new System.Drawing.Size(383, 265);
            this.tabCspin1.TabIndex = 1;
            this.tabCspin1.Text = "Contact Spins";
            // 
            // cbBonusSpin2
            // 
            this.cbBonusSpin2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbBonusSpin2.AutoSize = true;
            this.cbBonusSpin2.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBonusSpin2.ForeColor = System.Drawing.Color.Yellow;
            this.cbBonusSpin2.Location = new System.Drawing.Point(254, 52);
            this.cbBonusSpin2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbBonusSpin2.Name = "cbBonusSpin2";
            this.cbBonusSpin2.Size = new System.Drawing.Size(63, 24);
            this.cbBonusSpin2.TabIndex = 218;
            this.cbBonusSpin2.Text = "Bonus";
            this.cbBonusSpin2.UseVisualStyleBackColor = true;
            // 
            // checkCombo3
            // 
            this.checkCombo3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkCombo3.BackColor = System.Drawing.Color.Black;
            this.checkCombo3.Location = new System.Drawing.Point(151, 15);
            this.checkCombo3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkCombo3.Name = "checkCombo3";
            this.checkCombo3.OffFont = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkCombo3.OffForeColor = System.Drawing.Color.Yellow;
            this.checkCombo3.OffText = "SoloSpin";
            this.checkCombo3.OnFont = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkCombo3.OnForeColor = System.Drawing.Color.Yellow;
            this.checkCombo3.OnText = "ComboSpin";
            this.checkCombo3.Size = new System.Drawing.Size(149, 29);
            this.checkCombo3.Style = JCS.ToggleSwitch.ToggleSwitchStyle.Fancy;
            this.checkCombo3.TabIndex = 163;
            // 
            // newComboSpin2
            // 
            this.newComboSpin2.BackColor = System.Drawing.Color.Transparent;
            this.newComboSpin2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.newComboSpin2.Enabled = false;
            this.newComboSpin2.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.newComboSpin2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.newComboSpin2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.newComboSpin2.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newComboSpin2.ForeColor = System.Drawing.Color.Yellow;
            this.newComboSpin2.Location = new System.Drawing.Point(23, 15);
            this.newComboSpin2.Margin = new System.Windows.Forms.Padding(0);
            this.newComboSpin2.Name = "newComboSpin2";
            this.newComboSpin2.Size = new System.Drawing.Size(62, 29);
            this.newComboSpin2.TabIndex = 244;
            this.newComboSpin2.Text = "New";
            this.newComboSpin2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.newComboSpin2.UseVisualStyleBackColor = false;
            this.newComboSpin2.Click += new System.EventHandler(this.newComboSpin2_Click);
            // 
            // panelContactCombo
            // 
            this.panelContactCombo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelContactCombo.AutoScroll = true;
            this.panelContactCombo.Controls.Add(this.el127);
            this.panelContactCombo.Controls.Add(this.cb126);
            this.panelContactCombo.Controls.Add(this.el126);
            this.panelContactCombo.Controls.Add(this.el139);
            this.panelContactCombo.Controls.Add(this.el141);
            this.panelContactCombo.Controls.Add(this.el140);
            this.panelContactCombo.Controls.Add(this.el153);
            this.panelContactCombo.Controls.Add(this.el125);
            this.panelContactCombo.Controls.Add(this.el129);
            this.panelContactCombo.Controls.Add(this.el137);
            this.panelContactCombo.Controls.Add(this.el143);
            this.panelContactCombo.Controls.Add(this.el142);
            this.panelContactCombo.Controls.Add(this.cb163);
            this.panelContactCombo.Controls.Add(this.cb159);
            this.panelContactCombo.Controls.Add(this.cb155);
            this.panelContactCombo.Controls.Add(this.cb152);
            this.panelContactCombo.Controls.Add(this.cb148);
            this.panelContactCombo.Controls.Add(this.cb144);
            this.panelContactCombo.Controls.Add(this.cb136);
            this.panelContactCombo.Controls.Add(this.cb128);
            this.panelContactCombo.Controls.Add(this.cb124);
            this.panelContactCombo.Controls.Add(this.el144);
            this.panelContactCombo.Controls.Add(this.label3);
            this.panelContactCombo.Controls.Add(this.label72);
            this.panelContactCombo.Controls.Add(this.label68);
            this.panelContactCombo.Controls.Add(this.el148);
            this.panelContactCombo.Controls.Add(this.el163);
            this.panelContactCombo.Controls.Add(this.label71);
            this.panelContactCombo.Controls.Add(this.label69);
            this.panelContactCombo.Controls.Add(this.el152);
            this.panelContactCombo.Controls.Add(this.el159);
            this.panelContactCombo.Controls.Add(this.label70);
            this.panelContactCombo.Controls.Add(this.el155);
            this.panelContactCombo.Controls.Add(this.el124);
            this.panelContactCombo.Controls.Add(this.label58);
            this.panelContactCombo.Controls.Add(this.el136);
            this.panelContactCombo.Controls.Add(this.label64);
            this.panelContactCombo.Controls.Add(this.el128);
            this.panelContactCombo.Controls.Add(this.label62);
            this.panelContactCombo.Controls.Add(this.label25);
            this.panelContactCombo.Location = new System.Drawing.Point(1, 73);
            this.panelContactCombo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelContactCombo.Name = "panelContactCombo";
            this.panelContactCombo.Size = new System.Drawing.Size(363, 430);
            this.panelContactCombo.TabIndex = 243;
            this.panelContactCombo.Paint += new System.Windows.Forms.PaintEventHandler(this.panelContactCombo_Paint);
            // 
            // el127
            // 
            this.el127.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el127.BackColor = System.Drawing.Color.Transparent;
            this.el127.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el127.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el127.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el127.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el127.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el127.ForeColor = System.Drawing.Color.Yellow;
            this.el127.Location = new System.Drawing.Point(230, 101);
            this.el127.Margin = new System.Windows.Forms.Padding(2);
            this.el127.Name = "el127";
            this.el127.Size = new System.Drawing.Size(24, 28);
            this.el127.TabIndex = 382;
            this.el127.Tag = "cbBroken";
            this.el127.Text = "0";
            this.el127.UseVisualStyleBackColor = false;
            this.el127.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // cb126
            // 
            this.cb126.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb126.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb126.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb126.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb126.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb126.ForeColor = System.Drawing.Color.Yellow;
            this.cb126.FormattingEnabled = true;
            this.cb126.Location = new System.Drawing.Point(259, 101);
            this.cb126.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb126.Name = "cb126";
            this.cb126.Size = new System.Drawing.Size(68, 28);
            this.cb126.TabIndex = 381;
            // 
            // el126
            // 
            this.el126.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el126.BackColor = System.Drawing.Color.Transparent;
            this.el126.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el126.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el126.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el126.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el126.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el126.ForeColor = System.Drawing.Color.Yellow;
            this.el126.Location = new System.Drawing.Point(333, 102);
            this.el126.Margin = new System.Windows.Forms.Padding(0);
            this.el126.Name = "el126";
            this.el126.Size = new System.Drawing.Size(24, 28);
            this.el126.TabIndex = 380;
            this.el126.Text = "1";
            this.el126.UseVisualStyleBackColor = false;
            this.el126.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el139
            // 
            this.el139.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el139.BackColor = System.Drawing.Color.Transparent;
            this.el139.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el139.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el139.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el139.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el139.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el139.ForeColor = System.Drawing.Color.Yellow;
            this.el139.Location = new System.Drawing.Point(230, 281);
            this.el139.Margin = new System.Windows.Forms.Padding(2);
            this.el139.Name = "el139";
            this.el139.Size = new System.Drawing.Size(24, 28);
            this.el139.TabIndex = 378;
            this.el139.Tag = "cbInverted";
            this.el139.Text = "0";
            this.el139.UseVisualStyleBackColor = false;
            this.el139.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el141
            // 
            this.el141.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el141.BackColor = System.Drawing.Color.Transparent;
            this.el141.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el141.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el141.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el141.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el141.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el141.ForeColor = System.Drawing.Color.Yellow;
            this.el141.Location = new System.Drawing.Point(230, 221);
            this.el141.Margin = new System.Windows.Forms.Padding(2);
            this.el141.Name = "el141";
            this.el141.Size = new System.Drawing.Size(24, 28);
            this.el141.TabIndex = 377;
            this.el141.Tag = "cbBroken";
            this.el141.Text = "0";
            this.el141.UseVisualStyleBackColor = false;
            this.el141.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el140
            // 
            this.el140.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el140.BackColor = System.Drawing.Color.Transparent;
            this.el140.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el140.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el140.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el140.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el140.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el140.ForeColor = System.Drawing.Color.Yellow;
            this.el140.Location = new System.Drawing.Point(230, 251);
            this.el140.Margin = new System.Windows.Forms.Padding(2);
            this.el140.Name = "el140";
            this.el140.Size = new System.Drawing.Size(24, 28);
            this.el140.TabIndex = 376;
            this.el140.Tag = "cbInverted";
            this.el140.Text = "0";
            this.el140.UseVisualStyleBackColor = false;
            this.el140.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el153
            // 
            this.el153.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el153.BackColor = System.Drawing.Color.Transparent;
            this.el153.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el153.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el153.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el153.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el153.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el153.ForeColor = System.Drawing.Color.Yellow;
            this.el153.Location = new System.Drawing.Point(230, 191);
            this.el153.Margin = new System.Windows.Forms.Padding(2);
            this.el153.Name = "el153";
            this.el153.Size = new System.Drawing.Size(24, 28);
            this.el153.TabIndex = 375;
            this.el153.Tag = "cbInverted";
            this.el153.Text = "0";
            this.el153.UseVisualStyleBackColor = false;
            this.el153.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el125
            // 
            this.el125.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el125.BackColor = System.Drawing.Color.Transparent;
            this.el125.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el125.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el125.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el125.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el125.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el125.ForeColor = System.Drawing.Color.Yellow;
            this.el125.Location = new System.Drawing.Point(230, 11);
            this.el125.Margin = new System.Windows.Forms.Padding(2);
            this.el125.Name = "el125";
            this.el125.Size = new System.Drawing.Size(24, 28);
            this.el125.TabIndex = 374;
            this.el125.Tag = "cbUp";
            this.el125.Text = "0";
            this.el125.UseVisualStyleBackColor = false;
            this.el125.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el129
            // 
            this.el129.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el129.BackColor = System.Drawing.Color.Transparent;
            this.el129.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el129.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el129.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el129.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el129.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el129.ForeColor = System.Drawing.Color.Yellow;
            this.el129.Location = new System.Drawing.Point(230, 41);
            this.el129.Margin = new System.Windows.Forms.Padding(2);
            this.el129.Name = "el129";
            this.el129.Size = new System.Drawing.Size(24, 28);
            this.el129.TabIndex = 373;
            this.el129.Tag = "cbSit";
            this.el129.Text = "0";
            this.el129.UseVisualStyleBackColor = false;
            this.el129.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el137
            // 
            this.el137.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el137.BackColor = System.Drawing.Color.Transparent;
            this.el137.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el137.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el137.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el137.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el137.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el137.ForeColor = System.Drawing.Color.Yellow;
            this.el137.Location = new System.Drawing.Point(230, 71);
            this.el137.Margin = new System.Windows.Forms.Padding(2);
            this.el137.Name = "el137";
            this.el137.Size = new System.Drawing.Size(24, 28);
            this.el137.TabIndex = 371;
            this.el137.Tag = "cbHeel";
            this.el137.Text = "0";
            this.el137.UseVisualStyleBackColor = false;
            this.el137.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el143
            // 
            this.el143.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el143.BackColor = System.Drawing.Color.Transparent;
            this.el143.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el143.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el143.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el143.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el143.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el143.ForeColor = System.Drawing.Color.Yellow;
            this.el143.Location = new System.Drawing.Point(230, 131);
            this.el143.Margin = new System.Windows.Forms.Padding(2);
            this.el143.Name = "el143";
            this.el143.Size = new System.Drawing.Size(24, 28);
            this.el143.TabIndex = 370;
            this.el143.Tag = "cbBroken";
            this.el143.Text = "0";
            this.el143.UseVisualStyleBackColor = false;
            this.el143.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el142
            // 
            this.el142.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el142.BackColor = System.Drawing.Color.Transparent;
            this.el142.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el142.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el142.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el142.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el142.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el142.ForeColor = System.Drawing.Color.Yellow;
            this.el142.Location = new System.Drawing.Point(230, 161);
            this.el142.Margin = new System.Windows.Forms.Padding(2);
            this.el142.Name = "el142";
            this.el142.Size = new System.Drawing.Size(24, 28);
            this.el142.TabIndex = 369;
            this.el142.Tag = "cbInverted";
            this.el142.Text = "0";
            this.el142.UseVisualStyleBackColor = false;
            this.el142.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // cb163
            // 
            this.cb163.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb163.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb163.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb163.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb163.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb163.ForeColor = System.Drawing.Color.Yellow;
            this.cb163.FormattingEnabled = true;
            this.cb163.Location = new System.Drawing.Point(259, 281);
            this.cb163.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb163.Name = "cb163";
            this.cb163.Size = new System.Drawing.Size(68, 28);
            this.cb163.TabIndex = 368;
            // 
            // cb159
            // 
            this.cb159.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb159.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb159.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb159.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb159.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb159.ForeColor = System.Drawing.Color.Yellow;
            this.cb159.FormattingEnabled = true;
            this.cb159.Location = new System.Drawing.Point(259, 251);
            this.cb159.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb159.Name = "cb159";
            this.cb159.Size = new System.Drawing.Size(68, 28);
            this.cb159.TabIndex = 367;
            // 
            // cb155
            // 
            this.cb155.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb155.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb155.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb155.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb155.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb155.ForeColor = System.Drawing.Color.Yellow;
            this.cb155.FormattingEnabled = true;
            this.cb155.Location = new System.Drawing.Point(259, 221);
            this.cb155.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb155.Name = "cb155";
            this.cb155.Size = new System.Drawing.Size(68, 28);
            this.cb155.TabIndex = 366;
            // 
            // cb152
            // 
            this.cb152.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb152.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb152.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb152.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb152.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb152.ForeColor = System.Drawing.Color.Yellow;
            this.cb152.FormattingEnabled = true;
            this.cb152.Location = new System.Drawing.Point(259, 191);
            this.cb152.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb152.Name = "cb152";
            this.cb152.Size = new System.Drawing.Size(68, 28);
            this.cb152.TabIndex = 365;
            // 
            // cb148
            // 
            this.cb148.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb148.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb148.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb148.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb148.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb148.ForeColor = System.Drawing.Color.Yellow;
            this.cb148.FormattingEnabled = true;
            this.cb148.Location = new System.Drawing.Point(259, 161);
            this.cb148.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb148.Name = "cb148";
            this.cb148.Size = new System.Drawing.Size(68, 28);
            this.cb148.TabIndex = 364;
            // 
            // cb144
            // 
            this.cb144.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb144.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb144.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb144.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb144.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb144.ForeColor = System.Drawing.Color.Yellow;
            this.cb144.FormattingEnabled = true;
            this.cb144.Location = new System.Drawing.Point(259, 131);
            this.cb144.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb144.Name = "cb144";
            this.cb144.Size = new System.Drawing.Size(68, 28);
            this.cb144.TabIndex = 363;
            // 
            // cb136
            // 
            this.cb136.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb136.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb136.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb136.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb136.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb136.ForeColor = System.Drawing.Color.Yellow;
            this.cb136.FormattingEnabled = true;
            this.cb136.Location = new System.Drawing.Point(259, 71);
            this.cb136.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb136.Name = "cb136";
            this.cb136.Size = new System.Drawing.Size(68, 28);
            this.cb136.TabIndex = 361;
            // 
            // cb128
            // 
            this.cb128.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb128.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb128.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb128.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb128.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb128.ForeColor = System.Drawing.Color.Yellow;
            this.cb128.FormattingEnabled = true;
            this.cb128.Location = new System.Drawing.Point(259, 41);
            this.cb128.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb128.Name = "cb128";
            this.cb128.Size = new System.Drawing.Size(68, 28);
            this.cb128.TabIndex = 359;
            // 
            // cb124
            // 
            this.cb124.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb124.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb124.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb124.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb124.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb124.ForeColor = System.Drawing.Color.Yellow;
            this.cb124.FormattingEnabled = true;
            this.cb124.Location = new System.Drawing.Point(259, 11);
            this.cb124.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb124.Name = "cb124";
            this.cb124.Size = new System.Drawing.Size(68, 28);
            this.cb124.TabIndex = 354;
            // 
            // el144
            // 
            this.el144.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el144.BackColor = System.Drawing.Color.Transparent;
            this.el144.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el144.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el144.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el144.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el144.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el144.ForeColor = System.Drawing.Color.Yellow;
            this.el144.Location = new System.Drawing.Point(333, 132);
            this.el144.Margin = new System.Windows.Forms.Padding(0);
            this.el144.Name = "el144";
            this.el144.Size = new System.Drawing.Size(24, 28);
            this.el144.TabIndex = 353;
            this.el144.Text = "1";
            this.el144.UseVisualStyleBackColor = false;
            this.el144.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label3.Location = new System.Drawing.Point(10, 129);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(226, 32);
            this.label3.TabIndex = 351;
            this.label3.Tag = "6";
            this.label3.Text = "CAMEL KILIAN";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label72
            // 
            this.label72.BackColor = System.Drawing.Color.Transparent;
            this.label72.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label72.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label72.Location = new System.Drawing.Point(10, 160);
            this.label72.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(224, 32);
            this.label72.TabIndex = 340;
            this.label72.Tag = "6";
            this.label72.Text = "CAMEL TANGO";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label68
            // 
            this.label68.BackColor = System.Drawing.Color.Transparent;
            this.label68.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label68.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Yellow;
            this.label68.Location = new System.Drawing.Point(10, 285);
            this.label68.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(205, 118);
            this.label68.TabIndex = 344;
            this.label68.Tag = "6";
            this.label68.Text = "IMPOSSIBILE SIT";
            // 
            // el148
            // 
            this.el148.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el148.BackColor = System.Drawing.Color.Transparent;
            this.el148.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el148.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el148.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el148.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el148.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el148.ForeColor = System.Drawing.Color.Yellow;
            this.el148.Location = new System.Drawing.Point(333, 162);
            this.el148.Margin = new System.Windows.Forms.Padding(0);
            this.el148.Name = "el148";
            this.el148.Size = new System.Drawing.Size(24, 28);
            this.el148.TabIndex = 341;
            this.el148.Text = "1";
            this.el148.UseVisualStyleBackColor = false;
            this.el148.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el163
            // 
            this.el163.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el163.BackColor = System.Drawing.Color.Transparent;
            this.el163.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el163.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el163.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el163.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el163.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el163.ForeColor = System.Drawing.Color.Yellow;
            this.el163.Location = new System.Drawing.Point(333, 282);
            this.el163.Margin = new System.Windows.Forms.Padding(0);
            this.el163.Name = "el163";
            this.el163.Size = new System.Drawing.Size(24, 28);
            this.el163.TabIndex = 346;
            this.el163.Text = "1";
            this.el163.UseVisualStyleBackColor = false;
            this.el163.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // label71
            // 
            this.label71.BackColor = System.Drawing.Color.Transparent;
            this.label71.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label71.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label71.Location = new System.Drawing.Point(10, 190);
            this.label71.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(225, 32);
            this.label71.TabIndex = 342;
            this.label71.Tag = "6";
            this.label71.Text = "LAY OVER CAMEL";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label69
            // 
            this.label69.BackColor = System.Drawing.Color.Transparent;
            this.label69.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label69.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Yellow;
            this.label69.Location = new System.Drawing.Point(10, 250);
            this.label69.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(229, 32);
            this.label69.TabIndex = 343;
            this.label69.Tag = "6";
            this.label69.Text = "REVERSE LAY OVER";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el152
            // 
            this.el152.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el152.BackColor = System.Drawing.Color.Transparent;
            this.el152.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el152.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el152.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el152.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el152.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el152.ForeColor = System.Drawing.Color.Yellow;
            this.el152.Location = new System.Drawing.Point(333, 192);
            this.el152.Margin = new System.Windows.Forms.Padding(0);
            this.el152.Name = "el152";
            this.el152.Size = new System.Drawing.Size(24, 28);
            this.el152.TabIndex = 345;
            this.el152.Text = "1";
            this.el152.UseVisualStyleBackColor = false;
            this.el152.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el159
            // 
            this.el159.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el159.BackColor = System.Drawing.Color.Transparent;
            this.el159.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el159.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el159.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el159.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el159.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el159.ForeColor = System.Drawing.Color.Yellow;
            this.el159.Location = new System.Drawing.Point(333, 252);
            this.el159.Margin = new System.Windows.Forms.Padding(0);
            this.el159.Name = "el159";
            this.el159.Size = new System.Drawing.Size(24, 28);
            this.el159.TabIndex = 347;
            this.el159.Text = "1";
            this.el159.UseVisualStyleBackColor = false;
            this.el159.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // label70
            // 
            this.label70.BackColor = System.Drawing.Color.Transparent;
            this.label70.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label70.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label70.Location = new System.Drawing.Point(10, 220);
            this.label70.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(229, 32);
            this.label70.TabIndex = 348;
            this.label70.Tag = "6";
            this.label70.Text = "IMPOSSIBLE";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el155
            // 
            this.el155.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el155.BackColor = System.Drawing.Color.Transparent;
            this.el155.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el155.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el155.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el155.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el155.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el155.ForeColor = System.Drawing.Color.Yellow;
            this.el155.Location = new System.Drawing.Point(333, 222);
            this.el155.Margin = new System.Windows.Forms.Padding(0);
            this.el155.Name = "el155";
            this.el155.Size = new System.Drawing.Size(24, 28);
            this.el155.TabIndex = 349;
            this.el155.Text = "1";
            this.el155.UseVisualStyleBackColor = false;
            this.el155.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // el124
            // 
            this.el124.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el124.BackColor = System.Drawing.Color.Transparent;
            this.el124.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el124.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el124.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el124.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el124.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el124.ForeColor = System.Drawing.Color.Yellow;
            this.el124.Location = new System.Drawing.Point(333, 12);
            this.el124.Margin = new System.Windows.Forms.Padding(0);
            this.el124.Name = "el124";
            this.el124.Size = new System.Drawing.Size(24, 28);
            this.el124.TabIndex = 217;
            this.el124.Tag = "NJ";
            this.el124.Text = "1";
            this.el124.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.el124.UseVisualStyleBackColor = false;
            this.el124.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // label58
            // 
            this.label58.BackColor = System.Drawing.Color.Transparent;
            this.label58.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label58.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.White;
            this.label58.Location = new System.Drawing.Point(10, 7);
            this.label58.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(222, 32);
            this.label58.TabIndex = 218;
            this.label58.Tag = "6";
            this.label58.Text = "UPRIGHT BACK-OUT";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el136
            // 
            this.el136.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el136.BackColor = System.Drawing.Color.Transparent;
            this.el136.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el136.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el136.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el136.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el136.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el136.ForeColor = System.Drawing.Color.Yellow;
            this.el136.Location = new System.Drawing.Point(333, 72);
            this.el136.Margin = new System.Windows.Forms.Padding(0);
            this.el136.Name = "el136";
            this.el136.Size = new System.Drawing.Size(24, 28);
            this.el136.TabIndex = 236;
            this.el136.Text = "1";
            this.el136.UseVisualStyleBackColor = false;
            this.el136.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // label64
            // 
            this.label64.BackColor = System.Drawing.Color.Transparent;
            this.label64.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label64.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label64.Location = new System.Drawing.Point(10, 68);
            this.label64.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(225, 32);
            this.label64.TabIndex = 235;
            this.label64.Tag = "6";
            this.label64.Text = "SIT HAZEL";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el128
            // 
            this.el128.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el128.BackColor = System.Drawing.Color.Transparent;
            this.el128.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el128.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el128.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el128.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el128.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el128.ForeColor = System.Drawing.Color.Yellow;
            this.el128.Location = new System.Drawing.Point(333, 42);
            this.el128.Margin = new System.Windows.Forms.Padding(0);
            this.el128.Name = "el128";
            this.el128.Size = new System.Drawing.Size(24, 28);
            this.el128.TabIndex = 226;
            this.el128.Text = "1";
            this.el128.UseVisualStyleBackColor = false;
            this.el128.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // label62
            // 
            this.label62.BackColor = System.Drawing.Color.Transparent;
            this.label62.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label62.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label62.Location = new System.Drawing.Point(10, 38);
            this.label62.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(224, 32);
            this.label62.TabIndex = 225;
            this.label62.Tag = "6";
            this.label62.Text = "FACE to FACE Sit";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label25.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label25.Location = new System.Drawing.Point(10, 99);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(226, 32);
            this.label25.TabIndex = 379;
            this.label25.Tag = "6";
            this.label25.Text = "CATCH AT WAIST";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el65_
            // 
            this.el65_.BackColor = System.Drawing.Color.Transparent;
            this.el65_.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el65_.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el65_.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el65_.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el65_.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el65_.ForeColor = System.Drawing.Color.Yellow;
            this.el65_.Location = new System.Drawing.Point(15, 10);
            this.el65_.Margin = new System.Windows.Forms.Padding(0);
            this.el65_.Name = "el65_";
            this.el65_.Size = new System.Drawing.Size(24, 28);
            this.el65_.TabIndex = 215;
            this.el65_.Tag = "NJ";
            this.el65_.Text = "0";
            this.el65_.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.el65_.UseVisualStyleBackColor = false;
            this.el65_.Visible = false;
            this.el65_.Click += new System.EventHandler(this.InsertTrottola_Click);
            // 
            // tabVarie
            // 
            this.tabVarie.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabVarie.Controls.Add(this.tabSpiral);
            this.tabVarie.Controls.Add(this.tabSteps);
            this.tabVarie.Controls.Add(this.tabDeductions);
            this.tabVarie.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabVarie.Enabled = false;
            this.tabVarie.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabVarie.ImageList = this.imList;
            this.tabVarie.ItemSize = new System.Drawing.Size(80, 40);
            this.tabVarie.Location = new System.Drawing.Point(0, 313);
            this.tabVarie.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabVarie.Name = "tabVarie";
            this.tabVarie.SelectedIndex = 0;
            this.tabVarie.Size = new System.Drawing.Size(391, 281);
            this.tabVarie.TabIndex = 3;
            // 
            // tabSpiral
            // 
            this.tabSpiral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.tabSpiral.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabSpiral.Controls.Add(this.el97);
            this.tabSpiral.Controls.Add(this.label75);
            this.tabSpiral.Controls.Add(this.el96);
            this.tabSpiral.Controls.Add(this.label74);
            this.tabSpiral.Controls.Add(this.el101);
            this.tabSpiral.Controls.Add(this.el100);
            this.tabSpiral.Controls.Add(this.el99);
            this.tabSpiral.Controls.Add(this.el98);
            this.tabSpiral.Controls.Add(this.label73);
            this.tabSpiral.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabSpiral.Location = new System.Drawing.Point(4, 44);
            this.tabSpiral.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabSpiral.Name = "tabSpiral";
            this.tabSpiral.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabSpiral.Size = new System.Drawing.Size(383, 233);
            this.tabSpiral.TabIndex = 0;
            this.tabSpiral.Text = "Spirals";
            // 
            // el97
            // 
            this.el97.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el97.BackColor = System.Drawing.Color.Transparent;
            this.el97.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el97.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el97.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el97.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el97.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el97.ForeColor = System.Drawing.Color.Yellow;
            this.el97.Location = new System.Drawing.Point(249, 19);
            this.el97.Margin = new System.Windows.Forms.Padding(0);
            this.el97.Name = "el97";
            this.el97.Size = new System.Drawing.Size(24, 28);
            this.el97.TabIndex = 238;
            this.el97.Text = "0";
            this.el97.UseVisualStyleBackColor = false;
            this.el97.Click += new System.EventHandler(this.InsertSpirals_Click);
            // 
            // label75
            // 
            this.label75.BackColor = System.Drawing.Color.Transparent;
            this.label75.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label75.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label75.Location = new System.Drawing.Point(16, 19);
            this.label75.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(229, 32);
            this.label75.TabIndex = 237;
            this.label75.Tag = "6";
            this.label75.Text = "NO LEVEL SPIRAL";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el96
            // 
            this.el96.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el96.BackColor = System.Drawing.Color.Transparent;
            this.el96.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el96.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el96.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el96.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el96.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el96.ForeColor = System.Drawing.Color.Yellow;
            this.el96.Location = new System.Drawing.Point(249, 54);
            this.el96.Margin = new System.Windows.Forms.Padding(0);
            this.el96.Name = "el96";
            this.el96.Size = new System.Drawing.Size(24, 28);
            this.el96.TabIndex = 236;
            this.el96.Text = "1";
            this.el96.UseVisualStyleBackColor = false;
            this.el96.Click += new System.EventHandler(this.InsertSpirals_Click);
            // 
            // label74
            // 
            this.label74.BackColor = System.Drawing.Color.Transparent;
            this.label74.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label74.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label74.Location = new System.Drawing.Point(16, 53);
            this.label74.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(229, 32);
            this.label74.TabIndex = 235;
            this.label74.Tag = "6";
            this.label74.Text = "CAMEL SPIRAL";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el101
            // 
            this.el101.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el101.BackColor = System.Drawing.Color.Transparent;
            this.el101.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el101.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el101.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el101.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el101.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el101.ForeColor = System.Drawing.Color.Yellow;
            this.el101.Location = new System.Drawing.Point(339, 88);
            this.el101.Margin = new System.Windows.Forms.Padding(0);
            this.el101.Name = "el101";
            this.el101.Size = new System.Drawing.Size(24, 28);
            this.el101.TabIndex = 234;
            this.el101.Text = "4";
            this.el101.UseVisualStyleBackColor = false;
            this.el101.Click += new System.EventHandler(this.InsertSpirals_Click);
            // 
            // el100
            // 
            this.el100.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el100.BackColor = System.Drawing.Color.Transparent;
            this.el100.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el100.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el100.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el100.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el100.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el100.ForeColor = System.Drawing.Color.Yellow;
            this.el100.Location = new System.Drawing.Point(309, 88);
            this.el100.Margin = new System.Windows.Forms.Padding(0);
            this.el100.Name = "el100";
            this.el100.Size = new System.Drawing.Size(24, 28);
            this.el100.TabIndex = 233;
            this.el100.Text = "3";
            this.el100.UseVisualStyleBackColor = false;
            this.el100.Click += new System.EventHandler(this.InsertSpirals_Click);
            // 
            // el99
            // 
            this.el99.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el99.BackColor = System.Drawing.Color.Transparent;
            this.el99.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el99.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el99.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el99.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el99.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el99.ForeColor = System.Drawing.Color.Yellow;
            this.el99.Location = new System.Drawing.Point(279, 88);
            this.el99.Margin = new System.Windows.Forms.Padding(0);
            this.el99.Name = "el99";
            this.el99.Size = new System.Drawing.Size(24, 28);
            this.el99.TabIndex = 232;
            this.el99.Text = "2";
            this.el99.UseVisualStyleBackColor = false;
            this.el99.Click += new System.EventHandler(this.InsertSpirals_Click);
            // 
            // el98
            // 
            this.el98.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el98.BackColor = System.Drawing.Color.Transparent;
            this.el98.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el98.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el98.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el98.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el98.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el98.ForeColor = System.Drawing.Color.Yellow;
            this.el98.Location = new System.Drawing.Point(249, 88);
            this.el98.Margin = new System.Windows.Forms.Padding(0);
            this.el98.Name = "el98";
            this.el98.Size = new System.Drawing.Size(24, 28);
            this.el98.TabIndex = 231;
            this.el98.Text = "1";
            this.el98.UseVisualStyleBackColor = false;
            this.el98.Click += new System.EventHandler(this.InsertSpirals_Click);
            // 
            // label73
            // 
            this.label73.BackColor = System.Drawing.Color.Transparent;
            this.label73.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label73.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Magenta;
            this.label73.Location = new System.Drawing.Point(16, 88);
            this.label73.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(229, 32);
            this.label73.TabIndex = 230;
            this.label73.Tag = "6";
            this.label73.Text = "DEATH SPIRAL";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabSteps
            // 
            this.tabSteps.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.tabSteps.Controls.Add(this.el73);
            this.tabSteps.Controls.Add(this.el76);
            this.tabSteps.Controls.Add(this.el63);
            this.tabSteps.Controls.Add(this.el59);
            this.tabSteps.Controls.Add(this.el60);
            this.tabSteps.Controls.Add(this.el61);
            this.tabSteps.Controls.Add(this.el62);
            this.tabSteps.Controls.Add(this.label56);
            this.tabSteps.Controls.Add(this.log);
            this.tabSteps.Location = new System.Drawing.Point(4, 44);
            this.tabSteps.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabSteps.Name = "tabSteps";
            this.tabSteps.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabSteps.Size = new System.Drawing.Size(383, 233);
            this.tabSteps.TabIndex = 1;
            this.tabSteps.Text = "Footworks";
            // 
            // el73
            // 
            this.el73.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el73.BackColor = System.Drawing.Color.Transparent;
            this.el73.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el73.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el73.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el73.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el73.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el73.ForeColor = System.Drawing.Color.Yellow;
            this.el73.Location = new System.Drawing.Point(182, 61);
            this.el73.Margin = new System.Windows.Forms.Padding(0);
            this.el73.Name = "el73";
            this.el73.Size = new System.Drawing.Size(174, 29);
            this.el73.TabIndex = 186;
            this.el73.Text = "CHOREO SEQUENCE";
            this.el73.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.el73.UseVisualStyleBackColor = false;
            this.el73.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el76
            // 
            this.el76.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el76.BackColor = System.Drawing.Color.Transparent;
            this.el76.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el76.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el76.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el76.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el76.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el76.ForeColor = System.Drawing.Color.Yellow;
            this.el76.Location = new System.Drawing.Point(182, 28);
            this.el76.Margin = new System.Windows.Forms.Padding(2);
            this.el76.Name = "el76";
            this.el76.Size = new System.Drawing.Size(24, 28);
            this.el76.TabIndex = 185;
            this.el76.Text = "0";
            this.el76.UseVisualStyleBackColor = false;
            this.el76.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el63
            // 
            this.el63.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el63.BackColor = System.Drawing.Color.Transparent;
            this.el63.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el63.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el63.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el63.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el63.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el63.ForeColor = System.Drawing.Color.Yellow;
            this.el63.Location = new System.Drawing.Point(332, 28);
            this.el63.Margin = new System.Windows.Forms.Padding(2);
            this.el63.Name = "el63";
            this.el63.Size = new System.Drawing.Size(24, 28);
            this.el63.TabIndex = 184;
            this.el63.Text = "5";
            this.el63.UseVisualStyleBackColor = false;
            this.el63.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el59
            // 
            this.el59.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el59.BackColor = System.Drawing.Color.Transparent;
            this.el59.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el59.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el59.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el59.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el59.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el59.ForeColor = System.Drawing.Color.Yellow;
            this.el59.Location = new System.Drawing.Point(212, 28);
            this.el59.Margin = new System.Windows.Forms.Padding(2);
            this.el59.Name = "el59";
            this.el59.Size = new System.Drawing.Size(24, 28);
            this.el59.TabIndex = 180;
            this.el59.Text = "1";
            this.el59.UseVisualStyleBackColor = false;
            this.el59.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el60
            // 
            this.el60.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el60.BackColor = System.Drawing.Color.Transparent;
            this.el60.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el60.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el60.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el60.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el60.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el60.ForeColor = System.Drawing.Color.Yellow;
            this.el60.Location = new System.Drawing.Point(242, 28);
            this.el60.Margin = new System.Windows.Forms.Padding(2);
            this.el60.Name = "el60";
            this.el60.Size = new System.Drawing.Size(24, 28);
            this.el60.TabIndex = 181;
            this.el60.Text = "2";
            this.el60.UseVisualStyleBackColor = false;
            this.el60.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el61
            // 
            this.el61.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el61.BackColor = System.Drawing.Color.Transparent;
            this.el61.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el61.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el61.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el61.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el61.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el61.ForeColor = System.Drawing.Color.Yellow;
            this.el61.Location = new System.Drawing.Point(272, 28);
            this.el61.Margin = new System.Windows.Forms.Padding(2);
            this.el61.Name = "el61";
            this.el61.Size = new System.Drawing.Size(24, 28);
            this.el61.TabIndex = 182;
            this.el61.Text = "3";
            this.el61.UseVisualStyleBackColor = false;
            this.el61.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el62
            // 
            this.el62.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el62.BackColor = System.Drawing.Color.Transparent;
            this.el62.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el62.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el62.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el62.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el62.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el62.ForeColor = System.Drawing.Color.Yellow;
            this.el62.Location = new System.Drawing.Point(302, 28);
            this.el62.Margin = new System.Windows.Forms.Padding(2);
            this.el62.Name = "el62";
            this.el62.Size = new System.Drawing.Size(24, 28);
            this.el62.TabIndex = 183;
            this.el62.Text = "4";
            this.el62.UseVisualStyleBackColor = false;
            this.el62.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label56
            // 
            this.label56.BackColor = System.Drawing.Color.Transparent;
            this.label56.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label56.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label56.Location = new System.Drawing.Point(10, 26);
            this.label56.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(192, 32);
            this.label56.TabIndex = 179;
            this.label56.Tag = "6";
            this.label56.Text = "STEPS LEVEL";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // log
            // 
            this.log.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.log.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.log.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.log.Font = new System.Drawing.Font("Courier New", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.log.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.log.Location = new System.Drawing.Point(3, 123);
            this.log.Margin = new System.Windows.Forms.Padding(2);
            this.log.Multiline = true;
            this.log.Name = "log";
            this.log.ReadOnly = true;
            this.log.Size = new System.Drawing.Size(377, 108);
            this.log.TabIndex = 72;
            // 
            // tabDeductions
            // 
            this.tabDeductions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.tabDeductions.Controls.Add(this.ded5);
            this.tabDeductions.Controls.Add(this.label42);
            this.tabDeductions.Controls.Add(this.ded3);
            this.tabDeductions.Controls.Add(this.ded4);
            this.tabDeductions.Controls.Add(this.label43);
            this.tabDeductions.Controls.Add(this.label44);
            this.tabDeductions.Controls.Add(this.label45);
            this.tabDeductions.Controls.Add(this.ded2);
            this.tabDeductions.Controls.Add(this.label46);
            this.tabDeductions.Controls.Add(this.ded1);
            this.tabDeductions.Location = new System.Drawing.Point(4, 44);
            this.tabDeductions.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabDeductions.Name = "tabDeductions";
            this.tabDeductions.Size = new System.Drawing.Size(383, 233);
            this.tabDeductions.TabIndex = 2;
            this.tabDeductions.Text = "Deductions";
            // 
            // ded5
            // 
            this.ded5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ded5.BackColor = System.Drawing.Color.White;
            this.ded5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ded5.Font = new System.Drawing.Font("Arial Narrow", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded5.ForeColor = System.Drawing.Color.Red;
            this.ded5.Location = new System.Drawing.Point(299, 142);
            this.ded5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ded5.Name = "ded5";
            this.ded5.Size = new System.Drawing.Size(45, 25);
            this.ded5.TabIndex = 181;
            this.ded5.ValueChanged += new System.EventHandler(this.ded5_ValueChanged);
            // 
            // label42
            // 
            this.label42.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label42.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.White;
            this.label42.Location = new System.Drawing.Point(14, 143);
            this.label42.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(282, 23);
            this.label42.TabIndex = 176;
            this.label42.Text = "COSTUME VIOLATION";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ded3
            // 
            this.ded3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ded3.BackColor = System.Drawing.Color.White;
            this.ded3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ded3.Font = new System.Drawing.Font("Arial Narrow", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded3.ForeColor = System.Drawing.Color.Red;
            this.ded3.Location = new System.Drawing.Point(299, 85);
            this.ded3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ded3.Name = "ded3";
            this.ded3.Size = new System.Drawing.Size(45, 25);
            this.ded3.TabIndex = 179;
            this.ded3.ValueChanged += new System.EventHandler(this.ded3_ValueChanged);
            // 
            // ded4
            // 
            this.ded4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ded4.BackColor = System.Drawing.Color.White;
            this.ded4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ded4.Font = new System.Drawing.Font("Arial Narrow", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded4.ForeColor = System.Drawing.Color.Red;
            this.ded4.Location = new System.Drawing.Point(299, 57);
            this.ded4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ded4.Name = "ded4";
            this.ded4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ded4.Size = new System.Drawing.Size(45, 25);
            this.ded4.TabIndex = 180;
            this.ded4.ValueChanged += new System.EventHandler(this.ded4_ValueChanged);
            // 
            // label43
            // 
            this.label43.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label43.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.White;
            this.label43.Location = new System.Drawing.Point(19, 9);
            this.label43.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(270, 42);
            this.label43.TabIndex = 172;
            this.label43.Tag = "6";
            this.label43.Text = "FALLS";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label44
            // 
            this.label44.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label44.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.White;
            this.label44.Location = new System.Drawing.Point(10, 58);
            this.label44.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label44.Name = "label44";
            this.label44.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label44.Size = new System.Drawing.Size(285, 23);
            this.label44.TabIndex = 175;
            this.label44.Text = "ILLEGAL/MISSING ELEMENT";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label45
            // 
            this.label45.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label45.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(10, 87);
            this.label45.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(284, 23);
            this.label45.TabIndex = 174;
            this.label45.Text = "TIME VIOLATION";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ded2
            // 
            this.ded2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ded2.BackColor = System.Drawing.Color.White;
            this.ded2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ded2.DecimalPlaces = 1;
            this.ded2.Font = new System.Drawing.Font("Arial Narrow", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded2.ForeColor = System.Drawing.Color.Red;
            this.ded2.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.ded2.Location = new System.Drawing.Point(299, 114);
            this.ded2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ded2.Name = "ded2";
            this.ded2.Size = new System.Drawing.Size(45, 25);
            this.ded2.TabIndex = 178;
            this.ded2.ValueChanged += new System.EventHandler(this.ded2_ValueChanged);
            // 
            // label46
            // 
            this.label46.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label46.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.White;
            this.label46.Location = new System.Drawing.Point(10, 115);
            this.label46.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(285, 23);
            this.label46.TabIndex = 173;
            this.label46.Text = "MUSIC VIOLATION";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ded1
            // 
            this.ded1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ded1.BackColor = System.Drawing.Color.White;
            this.ded1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ded1.Font = new System.Drawing.Font("Arial Narrow", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded1.ForeColor = System.Drawing.Color.Red;
            this.ded1.Location = new System.Drawing.Point(299, 18);
            this.ded1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ded1.Name = "ded1";
            this.ded1.Size = new System.Drawing.Size(45, 25);
            this.ded1.TabIndex = 177;
            this.ded1.ValueChanged += new System.EventHandler(this.ded1_ValueChanged);
            // 
            // tt2
            // 
            this.tt2.IsBalloon = true;
            this.tt2.ShowAlways = true;
            this.tt2.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.tt2.ToolTipTitle = "Description:";
            // 
            // CoppieForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1213, 693);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "CoppieForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Pairs - ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CoppieForm_FormClosing);
            this.Load += new System.EventHandler(this.SPSingoloForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.gbElements.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tabLifts.ResumeLayout(false);
            this.tabLift1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.tabLift2.ResumeLayout(false);
            this.tabLift3.ResumeLayout(false);
            this.tabJ.ResumeLayout(false);
            this.tabJumps.ResumeLayout(false);
            this.tabLanciati.ResumeLayout(false);
            this.tabTwist.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.tabSpins.ResumeLayout(false);
            this.tabSpin.ResumeLayout(false);
            this.tabSpin.PerformLayout();
            this.tabCspin1.ResumeLayout(false);
            this.tabCspin1.PerformLayout();
            this.panelContactCombo.ResumeLayout(false);
            this.tabVarie.ResumeLayout(false);
            this.tabSpiral.ResumeLayout(false);
            this.tabSteps.ResumeLayout(false);
            this.tabSteps.PerformLayout();
            this.tabDeductions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ded5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.GroupBox gbElements;
        private System.Windows.Forms.Button j9;
        private System.Windows.Forms.Button j8;
        private System.Windows.Forms.Button j7;
        private System.Windows.Forms.Button j6;
        private System.Windows.Forms.Button j5;
        private System.Windows.Forms.Button j4;
        private System.Windows.Forms.Button j3;
        private System.Windows.Forms.Button j2;
        private System.Windows.Forms.Button j1;
        private System.Windows.Forms.Button tp;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ImageList imList;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ToolTip tt2;
        private System.Windows.Forms.Button startstop;
        private System.Windows.Forms.Button confirm;
        private System.Windows.Forms.Button reset;
        private System.Windows.Forms.Button ltimer;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label total;
        private System.Windows.Forms.Label deductions;
        private System.Windows.Forms.Label elements;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ListView lv;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader note;
        private System.Windows.Forms.Label error;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.ColumnHeader magg;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.TabControl tabLifts;
        private System.Windows.Forms.TabPage tabLift1;
        private System.Windows.Forms.TabPage tabLift2;
        private System.Windows.Forms.TabControl tabJ;
        private System.Windows.Forms.TabPage tabJumps;
        private System.Windows.Forms.TabPage tabLanciati;
        private System.Windows.Forms.TabControl tabSpins;
        private System.Windows.Forms.TabPage tabSpin;
        private System.Windows.Forms.TabPage tabCspin1;
        private System.Windows.Forms.TabControl tabVarie;
        private System.Windows.Forms.TabPage tabSpiral;
        private System.Windows.Forms.TabPage tabSteps;
        private System.Windows.Forms.Button under;
        private System.Windows.Forms.Button down;
        private System.Windows.Forms.Button el23;
        private System.Windows.Forms.Button el22;
        private System.Windows.Forms.Button el21;
        private System.Windows.Forms.Button el20;
        private System.Windows.Forms.Button el19;
        private System.Windows.Forms.Button el17;
        private System.Windows.Forms.Button el11;
        private System.Windows.Forms.Button el5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button el16;
        private System.Windows.Forms.Button el10;
        private System.Windows.Forms.Button el4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button el15;
        private System.Windows.Forms.Button el9;
        private System.Windows.Forms.Button el3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button el13;
        private System.Windows.Forms.Button el8;
        private System.Windows.Forms.Button el2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button el14;
        private System.Windows.Forms.Button el7;
        private System.Windows.Forms.Button el1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button el18;
        private System.Windows.Forms.Button el12;
        private System.Windows.Forms.Button el6;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button el94;
        private System.Windows.Forms.Button el95;
        private System.Windows.Forms.Button el93;
        private System.Windows.Forms.Button el92;
        private System.Windows.Forms.Button el89;
        private System.Windows.Forms.Button el84;
        private System.Windows.Forms.Button el79;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button el90;
        private System.Windows.Forms.Button el85;
        private System.Windows.Forms.Button el80;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button el88;
        private System.Windows.Forms.Button el83;
        private System.Windows.Forms.Button el78;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button el87;
        private System.Windows.Forms.Button el82;
        private System.Windows.Forms.Button el77;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button el91;
        private System.Windows.Forms.Button el86;
        private System.Windows.Forms.Button el81;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button newCombo;
        private System.Windows.Forms.Button el64;
        private System.Windows.Forms.Button skip;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button el172;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button el167;
        private System.Windows.Forms.Button el154;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button info;
        private System.Windows.Forms.Button el199;
        private System.Windows.Forms.Button el198;
        private System.Windows.Forms.Button el197;
        private System.Windows.Forms.Button el196;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Button el194;
        private System.Windows.Forms.Button el193;
        private System.Windows.Forms.Button el192;
        private System.Windows.Forms.Button el191;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button el189;
        private System.Windows.Forms.Button el184;
        private System.Windows.Forms.Button el179;
        private System.Windows.Forms.Button el188;
        private System.Windows.Forms.Button el187;
        private System.Windows.Forms.Button el186;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button el183;
        private System.Windows.Forms.Button el182;
        private System.Windows.Forms.Button el181;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button el178;
        private System.Windows.Forms.Button el177;
        private System.Windows.Forms.Button el176;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button el226;
        private System.Windows.Forms.TabPage tabTwist;
        private System.Windows.Forms.Button el103;
        private System.Windows.Forms.TabPage tabDeductions;
        private System.Windows.Forms.TextBox log;
        private System.Windows.Forms.NumericUpDown ded5;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.NumericUpDown ded3;
        private System.Windows.Forms.NumericUpDown ded4;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.NumericUpDown ded2;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.NumericUpDown ded1;
        private System.Windows.Forms.Button el185;
        private System.Windows.Forms.Button el180;
        private System.Windows.Forms.Button el190;
        private System.Windows.Forms.Button el195;
        private System.Windows.Forms.Button el200;
        private System.Windows.Forms.Button el107;
        private System.Windows.Forms.Button el106;
        private System.Windows.Forms.Button el105;
        private System.Windows.Forms.Button el104;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Button el122;
        private System.Windows.Forms.Button el121;
        private System.Windows.Forms.Button el119;
        private System.Windows.Forms.Button el120;
        private System.Windows.Forms.Button el117;
        private System.Windows.Forms.Button el116;
        private System.Windows.Forms.Button el115;
        private System.Windows.Forms.Button el114;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Button el112;
        private System.Windows.Forms.Button el111;
        private System.Windows.Forms.Button el110;
        private System.Windows.Forms.Button el109;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button el118;
        private System.Windows.Forms.Button el113;
        private System.Windows.Forms.Button el108;
        private System.Windows.Forms.Button el25;
        private System.Windows.Forms.Button el40;
        private System.Windows.Forms.Button el45;
        private System.Windows.Forms.Button el48;
        private System.Windows.Forms.Button el47;
        private System.Windows.Forms.Button el49;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Button el65;
        private System.Windows.Forms.Button el76;
        private System.Windows.Forms.Button el63;
        private System.Windows.Forms.Button el59;
        private System.Windows.Forms.Button el60;
        private System.Windows.Forms.Button el61;
        private System.Windows.Forms.Button el62;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Button el124;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Button el65_;
        private System.Windows.Forms.Button el136;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Button el128;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Button el102;
        private System.Windows.Forms.CheckBox cbCombo;
        private System.Windows.Forms.Button el97;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Button el96;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Button el101;
        private System.Windows.Forms.Button el100;
        private System.Windows.Forms.Button el99;
        private System.Windows.Forms.Button el98;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.CheckBox cbVerify;
        private System.Windows.Forms.CheckBox cbComboLift1;
        private System.Windows.Forms.Button el804;
        private System.Windows.Forms.Button el803;
        private System.Windows.Forms.Button el802;
        private System.Windows.Forms.Button el801;
        private System.Windows.Forms.CheckBox cbComboLift2;
        private System.Windows.Forms.Button el805;
        private System.Windows.Forms.Button quit;
        private System.Windows.Forms.TabPage tabLift3;
        private System.Windows.Forms.CheckBox cbComboLift3;
        private System.Windows.Forms.Button el810;
        private System.Windows.Forms.Button el809;
        private System.Windows.Forms.Button el808;
        private System.Windows.Forms.Button el807;
        private System.Windows.Forms.Button el806;
        private System.Windows.Forms.Button el225;
        private System.Windows.Forms.Button el220;
        private System.Windows.Forms.Button el215;
        private System.Windows.Forms.Button el210;
        private System.Windows.Forms.Button el205;
        private System.Windows.Forms.Button el224;
        private System.Windows.Forms.Button el201;
        private System.Windows.Forms.Button el223;
        private System.Windows.Forms.Button el202;
        private System.Windows.Forms.Button el222;
        private System.Windows.Forms.Button el203;
        private System.Windows.Forms.Button el221;
        private System.Windows.Forms.Button el206;
        private System.Windows.Forms.Button el219;
        private System.Windows.Forms.Button el207;
        private System.Windows.Forms.Button el218;
        private System.Windows.Forms.Button el208;
        private System.Windows.Forms.Button el217;
        private System.Windows.Forms.Button el211;
        private System.Windows.Forms.Button el216;
        private System.Windows.Forms.Button el212;
        private System.Windows.Forms.Button el214;
        private System.Windows.Forms.Button el213;
        private System.Windows.Forms.Button el209;
        private System.Windows.Forms.Button el204;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label labelPartial;
        private System.Windows.Forms.ComboBox cb49;
        private System.Windows.Forms.ComboBox cb25;
        private System.Windows.Forms.ComboBox cb47;
        private System.Windows.Forms.ComboBox cb40;
        private System.Windows.Forms.ComboBox cb48;
        private System.Windows.Forms.ComboBox cb45;
        private System.Windows.Forms.Button newComboSpin;
        private System.Windows.Forms.CheckBox cbBonusSpin;
        private System.Windows.Forms.Panel panelContactCombo;
        private System.Windows.Forms.Button newComboSpin2;
        private System.Windows.Forms.Button el144;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Button el148;
        private System.Windows.Forms.Button el163;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Button el152;
        private System.Windows.Forms.Button el159;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Button el155;
        private System.Windows.Forms.ComboBox cb163;
        private System.Windows.Forms.ComboBox cb159;
        private System.Windows.Forms.ComboBox cb155;
        private System.Windows.Forms.ComboBox cb152;
        private System.Windows.Forms.ComboBox cb148;
        private System.Windows.Forms.ComboBox cb144;
        private System.Windows.Forms.ComboBox cb136;
        private System.Windows.Forms.ComboBox cb128;
        private System.Windows.Forms.ComboBox cb124;
        private System.Windows.Forms.Button half;
        private System.Windows.Forms.Button half2;
        private System.Windows.Forms.Button under2;
        private System.Windows.Forms.Button down2;
        private System.Windows.Forms.Button half3;
        private System.Windows.Forms.Button under3;
        private System.Windows.Forms.Button down3;
        private JCS.ToggleSwitch checkCombo1;
        private JCS.ToggleSwitch checkCombo3;
        private System.Windows.Forms.CheckBox cbBonusSpin2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button el147;
        private System.Windows.Forms.Button el166;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Button el146;
        private System.Windows.Forms.Button el165;
        private System.Windows.Forms.Button el239;
        private System.Windows.Forms.Button el164;
        private System.Windows.Forms.Button el145;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Button el238;
        private System.Windows.Forms.Button el162;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Button el173;
        private System.Windows.Forms.Button el158;
        private System.Windows.Forms.Button el157;
        private System.Windows.Forms.Button el156;
        private System.Windows.Forms.Button el151;
        private System.Windows.Forms.Button el150;
        private System.Windows.Forms.Button el149;
        private System.Windows.Forms.Button el161;
        private System.Windows.Forms.Button el160;
        private System.Windows.Forms.Button average;
        private System.Windows.Forms.Button el26;
        private System.Windows.Forms.Button el41;
        private System.Windows.Forms.Button el46;
        private System.Windows.Forms.Button el50;
        private System.Windows.Forms.Button el51;
        private System.Windows.Forms.Button el52;
        private System.Windows.Forms.Button el125;
        private System.Windows.Forms.Button el129;
        private System.Windows.Forms.Button el137;
        private System.Windows.Forms.Button el143;
        private System.Windows.Forms.Button el142;
        private System.Windows.Forms.Button el139;
        private System.Windows.Forms.Button el141;
        private System.Windows.Forms.Button el140;
        private System.Windows.Forms.Button el153;
        private System.Windows.Forms.Button el0;
        private System.Windows.Forms.Button el75;
        private System.Windows.Forms.Button el127;
        private System.Windows.Forms.ComboBox cb126;
        private System.Windows.Forms.Button el126;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button review;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button asterisco;
        private System.Windows.Forms.Button el73;
        private System.Windows.Forms.Button prev;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Threading;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Collections;
using System.Xml.Linq;
using System.Xml;
using RollartSystemTech;
using System.Net.NetworkInformation;

namespace RollartSystemTech
{
    public partial class CoppieForm : Form
    {
        //int currentElem = 0; // da 0 a elemShort -1
        /********************************************************************************
         * [0] = durata            [1] = factor         [2] = numElems       [3] = numPosLift
         * [4] = NumComboLift      [5] = numTwistJumps  [6] = numThrowJumps  [7] = numSideJumps 
         * [8] = numSideComboJumps [9] = numContactSpins[10] = numSideSpins  [11] = numSpirals
         * [12] = numSteps         [13] = ComboLiftL1   [14] = ComboLiftL2
         * [15] = ComboLiftL3      [16] = ComboLiftL4   [17] = ComboLiftL5   [18] = PercentageCombo
         * [19] = ComboJumpDD       [20] = ComboJumpTD   [21] = ComboJumpTT 
         * ******************************************************************************/
		/******************************************************
         * 
         *  Modifica per la versione 1.0.4 di tutti i 
         *  InvioComandoAlGiudice con SendBroadcast
         *  
         * ***************************************************/
        TimeSpan time = new TimeSpan();

        // Colori
        Color rigaSoloJump = Color.Aquamarine;
        Color rigaComboJump = Color.Aqua;
        Color rigaCombo = Color.GreenYellow;
        Color rigaComboX = Color.OrangeRed;
        Color rigaLanciati = Color.Cyan;
        Color rigaTwist = Color.DeepSkyBlue;
        Color rigaSpin = Color.LightSalmon;
        Color rigaComboSpin = Color.Gold;
        Color rigaContactSpin = Color.Orange;
        Color rigaSteps = Color.White;
        Color rigaSpirals = Color.Violet;
        Color rigaPosLift = Color.PaleGreen;
        Color rigaComboLift = Color.Lime;
        Color rigaDed = Color.Red;
        Color oldForeColor;
        ToolTip tt = null;

        Utility ut = null;
        string saltoCorr = "", saltoPrec = "";
        int currentElem = 0, progEl = 0, tolleranza = 5;
        decimal secMetaDisco = 0;
        int numComboSpin = 1, numComboSpinContact = 1, numComboJump = 1;
        bool firstTrottolaSoloCombo = true, firstTrottolaContactCombo = true;
        //double secondsAfterFinish = 0;
        bool addAsterisco = false, exitCheck = false;
        Thread thrCheckGiudici = null;
        bool flagClose = false;

        public CoppieForm()
        {
            InitializeComponent();
            System.Windows.Forms.Form.CheckForIllegalCrossThreadCalls = false;
        }

        private void SPSingoloForm_Load(object sender, EventArgs e)
        {
			try
            {
                //2.0.1 - 05/02/2019
                Utility.skResultSingle = null;
                Utility.skResultComb = null;
                if (Definizioni.stateVideo == 2)
                {
                    Utility.InizializzoVideoScreen();
                }

                Definizioni.currentForm = this;
	            ut = new Utility(null);
	            this.Text = Definizioni.resources["panel1"].ToString() +
	                    Definizioni.idGaraParams + "/" + Definizioni.idSegment + " - " +
	                    Definizioni.dettaglioGara.Split('@')[3] + " " + Definizioni.dettaglioGara.Split('@')[4];
	            ut.LoadValuesFromDBForAll(this.Controls, log, tt2, "SegmentParamsPairs");
	            NewSegment();
	            tt = new ToolTip();
	            secMetaDisco = Definizioni.paramSegment[0] / 2;

				// nuovo thread per la verifica dei giudici
                thrCheckGiudici = new Thread(CheckGiudici);
                thrCheckGiudici.Start();
	            el65_.Tag = el65.Tag;
                this.MinimumSize = new System.Drawing.Size(1225, 853);
                if (Definizioni.idSegment == 2) tolleranza = 10;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Load: " + ex.Message, "ERROR");
            }
        }
        // Controllo ogni secondo se il giudice è connesso o meno

        private void CheckGiudici()
        {
            //bool judgeDisconnected = false;
            while (true)
            {
                if (exitCheck)
                    return;

                //judgeDisconnected = false;
                for (int i = 0; i < Definizioni.numJudges; i++)
                {
                    if (this == null)
                        return;

                    try
                    {
                        if (Main.jclient.list[i].state.Equals("1"))
                        {
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).Enabled = true;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).ForeColor = Color.Lime;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).FlatAppearance.BorderColor = Color.Lime;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).BackColor = Color.Transparent;
                        }
                        else // interfaccia giudice non avviata o chiusa
                        {
                            //judgeDisconnected = true;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).Enabled = true;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).ForeColor = Color.Red;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).FlatAppearance.BorderColor = Color.Red;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).BackColor = Color.Transparent;
                        }

                        // ping
                        if (!Utility.PingGiudice(i))
                        {
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).Font =
                                 new Font(((Button)this.Controls.Find("j" + (i + 1), true)[0]).Font, FontStyle.Regular | FontStyle.Underline);

                        }
                        else
                        {
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).Font =
                                 new Font(((Button)this.Controls.Find("j" + (i + 1), true)[0]).Font, FontStyle.Regular);
                        }

                    }
                    catch (Exception)
                    {
                    }

                }

                if (RollartSystemTech.Properties.Settings.Default.PingTime > 0)
                    Thread.Sleep(RollartSystemTech.Properties.Settings.Default.PingTime);
            }
        }
        
        #region GESTIONE SEGMENTO
        // INIZIO SEGMENTO
        private void startstop_Click(object sender, EventArgs e)
        {
            try
            {
                if (startstop.Text == "START")
                {
                    gbElements.Enabled = true;
                    confirm.Enabled = false;
                    average.Enabled = false;
                    skip.Enabled = false;
                    prev.Enabled = false;
                    reset.Enabled = false;
                    EnableGroupBox();
                    startstop.Text = "STOP";
                    timer1.Start();
                    ResetPercentualiTrottole();
                }
                else
                {
                    DisableGroupBox();
                    confirm.Enabled = true;
                    average.Enabled = true;
                    skip.Enabled = true;
                    prev.Enabled = true;
                    reset.Enabled = true;
                    startstop.Text = "START";
                    timer1.Stop();
                    // 2.0.1 - invio al Video Display il comando BLANK
                    Utility.Video_BlankCommand();
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("StartStop: " + ex.Message, "ERROR");
            }
        }
        
        // NUOVO SEGMENTO
        public void NewSegment()
        {
            try
            {
                progEl = 0;
                lv.Visible = true;
                // fine gara
                if (EndCompetition()) return;

                Definizioni.elemSegment = new decimal[30];
                ded1.Value = 0; ded2.Value = 0; ded3.Value = 0; ded4.Value = 0; ded5.Value = 0;
				//ded6.Value = 0; // *** Modifica del 17/08/2018 *** //
                total.Text = String.Format("{0:0.00}", 0); //"0,00";
                deductions.Text = String.Format("{0:0.00}", 0); //"0,00";
                elements.Text = String.Format("{0:0.00}", 0); //"0,00";
                Definizioni.currentPart = Definizioni.lastPart + 1;
				Definizioni.numComboElement = 0;
                Definizioni.totale = 0; Definizioni.deductionsDec = 0; Definizioni.elementsDec = 0;
                Definizioni.compAlreadyInserted = false;
                currentElem = 0;
                saltoPrec = "";
                saltoCorr = "";
                error.Text = "";
                ltimer.Text = "00:00";
                time = TimeSpan.Zero;
                ltimer.ForeColor = Color.Magenta;

                Utility.SendBroadcast("<CONNECT/>");
                Thread.Sleep(500);
                //Utility.CheckGiudici(this.Controls);
                Utility.SendBroadcast(
                    "<START Gara ='" + Definizioni.idGaraParams + "'" +
                          " Segment='" + Definizioni.idSegment + "'" +
                          " Partecipante='" + Definizioni.currentPart + "'" +
                          // **** 1.0.4 Modifica del 27/06/2018 **** //
                          // Invio al giudice anche la categoria e la disciplina
                          " Category='" + Definizioni.idCategoria + "'" +
                          " Discipline='" + Definizioni.idSpecialita + "'" +
                    "/>");

                // 2.0.1 - invio al Video Display il comando SKATER
                Utility.LoadSkater();
                Utility.Video_SkaterCommand();

                ut.LoadParticipant(log, buttonNext, labelPartial);
                lv.Items.Clear();
                DisableGroupBox();
                startstop.Text = "START";
                startstop.Enabled = true;
                confirm.Enabled = false;
                average.Enabled = false;

                // 2.0.0.10
                Definizioni.arDeductions = new decimal[6];
                Definizioni.skipped = false;

                if (Definizioni.currentPart == 1) prev.Enabled = false;

                numComboSpin = 1; numComboSpinContact = 1; numComboJump = 1;
                firstTrottolaContactCombo = true; firstTrottolaSoloCombo = true;

                Utility.WriteLog("___________________________________________________________________", "OK");
                Utility.WriteLog(" " + buttonNext.Text.ToUpper(), "OK");
                Utility.WriteLog("___________________________________________________________________", "OK");

                // 2.0.1
                ut.RefreshStaticScreen();
            }
            catch (Exception ex)
            {
                Utility.WriteLog("NewSegment: " + ex.Message, "ERROR");
            }
        }

        // FINE GARA
        public bool EndCompetition()
        {
            try
            {
                // fine gara
                if (Definizioni.currentPart == Definizioni.numPart)
                {
                    // invio classifica al sistema video
                    //ut.SendToVideoFinale();
                    Utility.SendBroadcast("<DISCONNECT/>");

                    //aggiorno a Y il campo COMPLETED di GaraParams
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "UPDATE GaraParams SET Completed = 'Y' " +
                            " WHERE ID_GaraParams = " + Definizioni.idGaraParams
                            + " AND ID_Segment = " + Definizioni.idSegment;
                        command.ExecuteNonQuery();
                    }

                    // 2.0.1 Gestione Video
                    Utility.Video_BlankCommand();

                    if (ut.ds != null) ut.ds.Dispose();

                    Definizioni.currentPart = 0;
                    Dispose();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("EndCompetition: " + ex.Message, "ERROR");
                return false;
            }
        }

        // CARICA IMPOSTAZIONI
        public void LoadDescriptions()
        {
            try
            {
                XmlDocument xml = new XmlDocument();
                xml.Load("settings.xml");
                XmlNode root = xml.DocumentElement;

                // steps
                int numSteps = xml.SelectNodes("/Settings/Steps/*").Count;
                XmlNodeList nodiSteps = xml.SelectNodes("/Settings/Steps/*");
                for (int i = 0; i < numSteps; i++)
                {

                    tt2.SetToolTip(((Button)this.Controls.Find("s" + (i + 1), true)[0])
                        , nodiSteps.Item(i).SelectNodes("@Desc").Item(0).Value);
                }   
            }
            catch (Exception ex)
            {
                Utility.WriteLog("LoadDescriptions: " + ex.Message, "ERROR");
            }
        }

        #endregion

        #region STARTSTOP, AVANTI, CONFERMA, Average, BACK, AGGIORNA, ESCI

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                // reset elementi
                Definizioni.elemSegment = new decimal[30];
                foreach (ListViewItem item in lv.Items)
                {
                    item.SubItems[0].Text = "";
                    item.SubItems[1].Text = "";
                    item.SubItems[2].Text = "";
                    item.SubItems[3].Text = "";

                }
                ded1.Value = 0; ded2.Value = 0; ded3.Value = 0; ded4.Value = 0; ded5.Value = 0; //ded6.Value = 0;
                elements.Text = String.Format("{0:0.00}", 0); //"0,00";
                deductions.Text = String.Format("{0:0.00}", 0); //"0,00";
                total.Text = String.Format("{0:0.00}", 0); //"0,00";

                Definizioni.totale = 0;
                currentElem = 0;
                lv.Items[currentElem].Selected = true;
                saltoPrec = "";
                saltoCorr = "";
            }
            catch (Exception ex)
            {
                Utility.WriteLog("button1: " + ex.Message, "ERROR");
            }
        }

        //CONFIRM ENTRIES
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                // Verifico gli elementi 
                if (!CheckBeforeConfirm()) return;

                // salvo sul log ciò che mi aspetto dai giudici
                SaveOnLog();

                DisableGroupBox();
                confirm.Enabled = false;
                back.Enabled = false;
				review.Enabled = false;
                startstop.Enabled = false;
                asterisco.Enabled = false;

                // Confermo il segmento
                exitCheck = true;
                Thread.Sleep(500);

                // 2.0.0.10 - modifica 26/01/2019
                Utility.SendBroadcast("<DEDUCTIONS ded1='" + ded1.Value + "' ded2='" + ded2.Value +
                "' ded3='" + ded3.Value + "' ded4='" + ded4.Value + "' ded5='" + ded5.Value + "' />");

                if (ut.ConfirmSegment())
                {
                    NewSegment();
                    error.Visible = false;
                } else
				{
				    startstop.Enabled = true;
                    confirm.Enabled = true;
                    Definizioni.connectedJudges = new Boolean[Definizioni.numJudges];
                    Definizioni.stopFromWaiting = false;
                    Definizioni.exitFromWaiting = false;
                    //for (int i = 0; i < Definizioni.connectedJudges.Length; i++)
                    //{
                    //    ((Button)this.Controls.Find("j" + (i + 1), true)[0]).Enabled = true;
                    //    ((Button)this.Controls.Find("j" + (i + 1), true)[0]).ForeColor = Color.Lime;
                    //    ((Button)this.Controls.Find("j" + (i + 1), true)[0]).FlatAppearance.BorderColor = Color.Lime;
                    //    ((Button)this.Controls.Find("j" + (i + 1), true)[0]).BackColor = Color.Transparent;
                    //}
                }
				thrCheckGiudici = new Thread(CheckGiudici);
                thrCheckGiudici.Start();
            }
            catch (Exception ex)
            {
                Utility.WriteLog("ConfirmEntries: " + ex.StackTrace, "ERROR");
                error.Visible = false;
            }
        }
   
        private void clear_Click(object sender, EventArgs e)
        {
            back.Enabled = false;
            ClearElement();
            back.Enabled = true;
        }

        // RESTART
        private void next_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = MessageBox.Show(
                Definizioni.resources["panel70"].ToString(), "Warning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    ut.RollbackCompetitor(log);
                    NewSegment();
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("RESET: " + ex.Message, "ERROR");
            }
        }

        public void SaveOnLog()
        {
            try
            {
                string elementsXML = "[ELEMENTS ";
                for (int i = 0; i < lv.Items.Count; i++)
                {
                    elementsXML += lv.Items[i].SubItems[1].Text + ";";
                }
                elementsXML += "][DEDUCTIONS: " + deductions.Text + "]";
                Utility.WriteLog(elementsXML, "");
            }
            catch (Exception ex)
            {
                Utility.WriteLog("SaveOnLog: " + ex.Message, "ERROR");
            }
        }

        // ASK COMPONENTS For Average
        private void average_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(
            Definizioni.resources["panel90"].ToString(), "Warning",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                DisableGroupBox();
                exitCheck = true;
                Thread.Sleep(500);
                ut.AskAverage();
               // Definizioni.connectedJudges = new Boolean[Definizioni.numJudges];
            }
            exitCheck = false;
            thrCheckGiudici = new Thread(CheckGiudici);
            thrCheckGiudici.Start();
        }

        // AGGIORNA ENTRY
        private void lv_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                foreach (ListViewItem lvi in lv.Items)
                {
                    lvi.BackColor = Color.Black;
                }
                cancel.Enabled = true;
                if (lv.SelectedItems[0] == null) return;
                Definizioni.updating = true; //modalità updating
                
                ListViewItem selectedItem = lv.SelectedItems[0];
                string numEl = selectedItem.SubItems[0].Text;
                string codeEl = selectedItem.SubItems[1].Text;
                string valEl = selectedItem.SubItems[2].Text;
                string typeEl = selectedItem.SubItems[3].Text;
                string cat = selectedItem.SubItems[4].Text;
                string numcombo = selectedItem.SubItems[5].Text;
                string du = selectedItem.SubItems[6].Text;
                int indexElement = selectedItem.Index;
                selectedItem.BackColor = Color.Yellow;
                oldForeColor = selectedItem.ForeColor;
                selectedItem.ForeColor = Color.Red;
                //oldForeColor = selectedItem.ForeColor;

                DisableGroupBox();
                // *** Modifica del 06/07/2018 - 1.0.4 *** //
                deductions.Enabled = false;
                flowLayoutPanel1.Enabled = false;
                review.Enabled = false;
                asterisco.Enabled = false;
                //startstop.Enabled = false;
                //confirm.Enabled = false;
                //deductions.Enabled = false;
                //average.Enabled = false;
                //skip.Enabled = false;
                back.Enabled = false;
                // *** //

                selectedItem.Selected = false;
                currentElem = indexElement;

                if (typeEl.Contains("SideJump")) // side singolo
                {
                    tabJ.Enabled = true;
                    ((Control)this.tabJumps).Enabled = true;
                }
                if (typeEl.Equals("ThrowJump")) // throw jump
                {
                    tabJ.Enabled = true;
                    ((Control)this.tabLanciati).Enabled = true;
                    ((Control)this.tabTwist).Enabled = true; // abilito anche twist
                }
                if (typeEl.Equals("TwistJump")) // twist jump
                {
                    tabJ.Enabled = true;
                    ((Control)this.tabTwist).Enabled = true;
                    ((Control)this.tabLanciati).Enabled = true; // abilito anche thorw
                }
                if (typeEl.StartsWith("SideSpin")) // side spin
                {
                    tabSpins.Enabled = true;
                    ((Control)this.tabSpin).Enabled = true;
                }
                if (typeEl.StartsWith("ContactSpin")) // contact spin
                {
                    tabSpins.Enabled = true;
                    ((Control)this.tabCspin1).Enabled = true;
                }
                if (typeEl.Equals("PosLift")) // pos lift
                {
                    tabLifts.Enabled = true;
                    //((Control)this.tabLift1).Enabled = true;
                    ((Control)this.tabLift1).Enabled = true;
                    ((Control)this.tabLift2).Enabled = true;
                    ((Control)this.tabLift3).Enabled = true;
                }
                if (typeEl.Equals("CombLift")) // combo lift
                {
                    tabLifts.Enabled = true;
                    //((Control)this.tabLift2).Enabled = true;
                    ((Control)this.tabLift1).Enabled = true;
                    ((Control)this.tabLift2).Enabled = true;
                    ((Control)this.tabLift3).Enabled = true;
                }
                if (typeEl.Equals("DeathSpiral")) // spirale
                {
                    tabVarie.Enabled = true;
                    ((Control)this.tabSpiral).Enabled = true;
                }
                if (typeEl.Equals("Steps")) // passi
                {
                    tabVarie.Enabled = true;
                    ((Control)this.tabSteps).Enabled = true;
                }
            }

            catch (Exception ex)
            {
                Utility.WriteLog("MouseDoubleClick: " + ex.Message, "ERROR");
            }
        }

        // ANNULLA AGGIORNAMENTO
        private void cancel_Click(object sender, EventArgs e)
        {
            ResetAfterInsertion();
        }

        // QUIT
        private void button2_Click_2(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(
                Definizioni.resources["panel8"].ToString(), "Warning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                Utility.SendBroadcast("<DISCONNECT/>");
                // faccio rollback ed elimino i dati dell'ultimo competitor
                ut.RollbackCompetitor(log);
				exitCheck = true;
                flagClose = true;
                // 2.0.1 Gestione Video
                Utility.Video_BlankCommand();
                if (ut.ds != null) ut.ds.Dispose();
                Dispose();
            }
        }

        // SKIP
        private void button1_Click_1(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(Definizioni.resources["panel9"].ToString(), "Warning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                ut.UpdateLastPartecipant();

                // 2.0.0.10 - 27/01/2019 modifica per non considerare l'atleta mancante nella classifica finale
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    cmd.CommandText = "UPDATE GaraTotal SET Position = '-'" +
                        " WHERE ID_GARAPARAMS = " + Definizioni.idGaraParams +
                        " AND ID_Atleta = " + Definizioni.idAtleta;
                    cmd.ExecuteReader();
                }

                NewSegment();

                Definizioni.skipped = true;
            }
        }
        #endregion

        #region FUNZIONI DI INTERFACCIA
        /***********************************************************************************
        * Controllo gli elementi inseriti
         * tipoEl --> 1=salto singolo in parallelo, 
         *            2=combinazione in parallelo,
         *            3=salto lanciato, 
         *            4=twist jump,
         *            5=trottola singola in parallelo, 
         *            6=trottola combo in parallelo,
         *            7=trottola d'incontro
         *            8=passi
         *            9=spirali
         *            10=sollevamenti singoli
         *            11=sollevamenti combo         
        ***********************************************************************************/
        public bool CheckShortAndLongProgram(int tipoEl, bool saltoSemplice, int numCombo)
        {
            error.Text = "";
            error.Visible = false;
            if (!cbVerify.Checked) return true;
            if (Definizioni.updating) return true;
            switch (tipoEl)
            {
                case 1: // salto in parallelo
                    if (int.Parse(Definizioni.elemSegment[7].ToString()) >= int.Parse(Definizioni.paramSegment[7].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel35"].ToString();
                        return false;
                    }
                    break;
                case 2: // salto combo in parallelo
                    //if (Definizioni.idSegment == 2) // solo long program
                    {
                        if (numCombo == 1) // controllo solo il primo salto di una combo
                        {
                            if (int.Parse(Definizioni.elemSegment[8].ToString()) >= int.Parse(Definizioni.paramSegment[8].ToString()))
                            {
                                error.Visible = true;
                                error.Text = Definizioni.resources["panel36"].ToString();
                                return false;
                            }
                        }
                        else
                        {
                            if (int.Parse(Definizioni.elemSegment[8].ToString()) > int.Parse(Definizioni.paramSegment[8].ToString()))
                            {
                                error.Visible = true;
                                error.Text = Definizioni.resources["panel36"].ToString();
                                return false;
                            }
                        }
                    }
                    break;
                case 3: // salti lanciati
                    if (int.Parse(Definizioni.elemSegment[6].ToString()) >= int.Parse(Definizioni.paramSegment[6].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel37"].ToString();
                        return false;
                    }
                    break;
                case 4: // twist jump
                    if (int.Parse(Definizioni.elemSegment[5].ToString()) >= int.Parse(Definizioni.paramSegment[5].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel38"].ToString();
                        return false;
                    }
                    break;
                case 5: // trottole singole in parallelo
                case 6: // trottole combo in parallelo
                    if (numCombo <= 1) // verifico solo per la prima trottola singola o combinata
                    {
                        if (int.Parse(Definizioni.elemSegment[10].ToString()) >= int.Parse(Definizioni.paramSegment[10].ToString()))
                        {
                            error.Visible = true;
                            error.Text = Definizioni.resources["panel39"].ToString();
                            return false;
                        }
                    } else // modifica 2.0 18/10/2018
                    {
                        if (int.Parse(Definizioni.elemSegment[10].ToString()) > int.Parse(Definizioni.paramSegment[10].ToString()))
                        {
                            error.Visible = true;
                            error.Text = Definizioni.resources["panel39"].ToString();
                            return false;
                        }
                    }
                    break;
                case 7: // trottola d'incontro 
                    if (numCombo <= 1) // verifico solo per la prima trottola singola o combinata
                    {
                        if (int.Parse(Definizioni.elemSegment[9].ToString()) >= int.Parse(Definizioni.paramSegment[9].ToString()))
                        {
                            error.Visible = true;
                            error.Text = Definizioni.resources["panel40"].ToString();
                            return false;
                        }
                    }
                    else // modifica 2.0 18/10/2018
                    {
                        if (int.Parse(Definizioni.elemSegment[9].ToString()) > int.Parse(Definizioni.paramSegment[9].ToString()))
                        {
                            error.Visible = true;
                            error.Text = Definizioni.resources["panel40"].ToString();
                            return false;
                        }
                    }
                    break;
                case 8: // passi
                    if (int.Parse(Definizioni.elemSegment[12].ToString()) >= int.Parse(Definizioni.paramSegment[12].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel29"].ToString(); 
                        return false;
                    }
                    break;
                case 9: // spirali
                    if (Definizioni.idSegment == 1) // short
                    {
                        if (int.Parse(Definizioni.elemSegment[11].ToString()) >= int.Parse(Definizioni.paramSegment[11].ToString()))
                        {
                            error.Visible = true;
                            error.Text = Definizioni.resources["panel41"].ToString();
                            return false;
                        }
                    }
                    else if (Definizioni.idSegment == 2) // long
                    {
                        if (int.Parse(Definizioni.elemSegment[11].ToString()) >= int.Parse(Definizioni.paramSegment[11].ToString()))
                        {
                            error.Visible = true;
                            error.Text = Definizioni.resources["panel42"].ToString();
                            return false;
                        }
                    }
                    break;
                case 10: // sollevamenti singoli
                    if (Definizioni.idSegment == 1) // short
                    {
                        if (int.Parse(Definizioni.elemSegment[3].ToString()) >= int.Parse(Definizioni.paramSegment[3].ToString()))
                        {
                            error.Visible = true;
                            error.Text = Definizioni.resources["panel43"].ToString();
                            return false;
                        }
                    }
                    else if (Definizioni.idSegment == 2) // long
                    {
                        if (int.Parse(Definizioni.elemSegment[3].ToString()) >= int.Parse(Definizioni.paramSegment[3].ToString()))
                        {
                            error.Visible = true;
                            error.Text = Definizioni.resources["panel44"].ToString();
                            return false;
                        }
                    }
                    break;
                case 11: // sollevamenti combo
                    if (int.Parse(Definizioni.elemSegment[4].ToString()) >= int.Parse(Definizioni.paramSegment[4].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel45"].ToString(); 
                        return false;
                    }
                    break;
                case 12:
                    break;
                default:
                    break;
            }

            return true;
        }

        public bool CheckBeforeConfirm()
        {
            try
            {
			if (!cbVerify.Checked) return true;
            string message = "";
            int numElemViol = 0;
            int numJumps = 0, numLanciati = 0, numTwist = 0;
            int numSpin = 0, numContactSpin = 0;
            int numSteps = 0, numSpirals = 0;
            int numPosLifts = 0, numComboLift = 0;

            foreach (ListViewItem lvi in lv.Items)
            {
                // Salti
                if (lvi.SubItems[3].Text.Contains("SideJump")) numJumps++;
                if (lvi.SubItems[3].Text.Equals("ThrowJump")) numLanciati++;
                if (lvi.SubItems[3].Text.Equals("TwistJump")) numTwist++;
                // trottole
                if (lvi.SubItems[3].Text.Contains("SideSpin")) numSpin++;
                if (lvi.SubItems[3].Text.Contains("ContactSpin")) numContactSpin++;
                //if (lvi.SubItems[3].Text.StartsWith("CombSpin")) numComboSpin++;
                // spirali
                if (lvi.SubItems[3].Text.Contains("DeathSpiral")) numSpirals++;
                // passi
                if (lvi.SubItems[3].Text.Contains("Steps")) numSteps++;
                // sollevamenti
                if (lvi.SubItems[3].Text.Equals("PosLift")) numPosLifts++;
                if (lvi.SubItems[3].Text.Equals("CombLift")) numComboLift++;
            }

            /***********************************************************************************
            * LUNGO: 
                • Maximum two (2) throw jumps.
             * 
                • One twist jump.
             * 
                • Maximum one (1) contact spin. If the pair chooses to perform a combination spin 
             *      the combination should contain maximum four (4) positions.
             * 
                • Two (2) side by side jumps elements. One must be a single jump; the other can be
             *      a combination jump (not more than four (4) jumps). The technical value of the 
             *      combination is the sum of the jumps of the combination. The connection jumps 
             *      will not be calculated (only doubles and triples in the combination will have a value).
             *      
                • Maximum one (1) side by side spins, combination or one position.
             * 
                • One (1) death spirals chosen each.
             * 
                • One (1) step sequence to be chosen from serpentine, circle, diagonal, straight line. 
             *      Please note that if a step sequence takes up more than half of the pattern this 
             *      sequence will be called so that the next will not have a value.
             *      
                Senior:
                • Maximum three (3) lifts: one (1) one position lift (no more than four (4) rotations of the man) 
             *      and two (2) combination lifts with no more than ten (10) revolutions of the man and no more than three (3) change of positions of the lady (4 positions).
                
             * Junior:
                • Two (2) lifts: one (1) one position lift (no more than four (4) rotations of the man) and one 
             *      (1) combination lift with no more than ten (10) revolutions of the man and no more than three 
             *      (3) change of positions of the lady (4 positions).
            **********************************************************************************/
            //int.Parse(Definizioni.paramSegment[3].ToString()) Solo lift
            //int.Parse(Definizioni.paramSegment[4].ToString()) Combo Lift
            //int.Parse(Definizioni.paramSegment[5].ToString()) twist
            //int.Parse(Definizioni.paramSegment[6].ToString()) throw
            //int.Parse(Definizioni.paramSegment[7].ToString()) side by side jumps
            //int.Parse(Definizioni.paramSegment[8.ToString()) side by side combo jumps
            //int.Parse(Definizioni.paramSegment[9].ToString()) contact spin
            //int.Parse(Definizioni.paramSegment[10].ToString()) side by side spin
            //int.Parse(Definizioni.paramSegment[11].ToString()) spirali
            //int.Parse(Definizioni.paramSegment[12].ToString()) passi

            if (Definizioni.idSegment == 2)
            {
                //if (numJumps > 2) { numElemViol++; message += Definizioni.resources["panel71"].ToString() + "\r\n"; }
                //if (numJumps == 0) { numElemViol++; message += Definizioni.resources["panel48"].ToString() + "\r\n"; }
                //if (numTwist > 1) { numElemViol++; message += Definizioni.resources["panel72"].ToString() + "\r\n"; }
                //if (numTwist == 0) { numElemViol++; message += Definizioni.resources["panel49"].ToString() + "\r\n"; }
                //if (numLanciati > 2) { numElemViol++; message += Definizioni.resources["panel73"].ToString() + "\r\n"; }

                //if (numSpin > 1) { numElemViol++; message += Definizioni.resources["panel74"].ToString() + "\r\n"; }
                //if (numContactSpin > 1) { numElemViol++; message += Definizioni.resources["panel75"].ToString() + "\r\n"; }

                //if (numSpirals > 1) { numElemViol++; message += Definizioni.resources["panel41"].ToString() + "\r\n"; }
                //if (numSpirals == 0) { numElemViol++; message += Definizioni.resources["panel69"].ToString() + "\r\n"; }

                //if (numPosLifts > 0) { numElemViol++; message += Definizioni.resources["panel37"].ToString() + "\r\n"; }
                //if (numComboLift > 2) { numElemViol++; message += Definizioni.resources["panel75"].ToString() + "\r\n"; }

                //if (numSteps > 1) { numElemViol++; message += Definizioni.resources["panel19"].ToString() + "\r\n"; }
                //if (numSteps == 0) { numElemViol++; message += Definizioni.resources["panel29"].ToString() + "\r\n"; }

            }
            
            /***********************************************************************************
            * SHORT: 
             One position lift of no more than four (4) rotations for the man.
             One combination lift of no more than eight (8) rotations of the man 
             * and no more than two (2) changes of position for the lady 
             * (3 positions).
             A twist jump.
             One combined contact spin (no more than three (3) positions).
             One side by side jump.
             One side by side spin.
             One death spiral.
             One step sequence.
            * **********************************************************************************/
            else if (Definizioni.idSegment == 1)
            {
                if (numJumps == 0) { numElemViol++; message += Definizioni.resources["panel48"].ToString() + "\r\n"; }
                if (numSpin == 0) { numElemViol++; message += Definizioni.resources["panel50"].ToString() + "\r\n"; }
                if (numContactSpin == 0) { numElemViol++; message += Definizioni.resources["panel51"].ToString() + "\r\n"; }
                if (numPosLifts == 0) { numElemViol++; message += Definizioni.resources["panel47"].ToString() + "\r\n"; }
                if (numSpirals == 0) { numElemViol++; message += Definizioni.resources["panel69"].ToString() + "\r\n"; }
                if (numSteps == 0) { numElemViol++; message += Definizioni.resources["panel29"].ToString() + "\r\n"; }

                if (numTwist < int.Parse(Definizioni.paramSegment[5].ToString()))
                    { numElemViol++; message += Definizioni.resources["panel49"].ToString() + "\r\n"; }

                if (numComboLift < int.Parse(Definizioni.paramSegment[4].ToString()))
                    { numElemViol++; message += Definizioni.resources["panel46"].ToString() + "\r\n"; }

            }

            CheckComboSideSpin();

            CheckComboContactSpin();

            if (numElemViol > 0)
            {
                if (MessageBox.Show(numElemViol + " " + Definizioni.resources["panel33"].ToString() + "\r\n\r\n" +
                    message + "\r\n\r\n" + Definizioni.resources["panel34"].ToString(), "CONFIRM",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    return true;
                else return false;
            }
            return true;
            } catch (Exception)
            {
                return true;
            }
        }

        // verifico se in una combo spin siano valide almeno 2 trottole
        public void CheckComboSideSpin()
        {
            int numSpinsValide = 0;
            int numComboSpins = 0;
            string listaIndici = "";
            List<string[]> comboSpins = new List<string[]>(10);
            bool newSpin = false;

            foreach (ListViewItem lvi in lv.Items)
            {
                // azzero se prima trottola
                if (lvi.SubItems[3].Text.Equals("SideSpin 1"))
                {
                    numComboSpins++;
                    newSpin = true;
                }

                // se trottola combo e Confirmed
                if (lvi.SubItems[3].Text.StartsWith("SideSpin ") &&
                  (!lvi.SubItems[2].Text.Equals("0,00") &&
                   !lvi.SubItems[2].Text.Equals("0.00")))
                {
                    numSpinsValide++;
                    listaIndici += lvi.Index + ";";
                }
                else // se trottola combo e non confermata
                if (lvi.SubItems[3].Text.StartsWith("SideSpin ") &&
                  (lvi.SubItems[2].Text.Equals("0,00") ||
                   lvi.SubItems[2].Text.Equals("0.00")))
                {
                    listaIndici += lvi.Index + ";";
                }
                else if (!(lvi.SubItems[3].Text.StartsWith("SideSpin "))) // trottola terminata
                {
                    if (numSpinsValide < 2 && newSpin)
                    {
                        listaIndici = listaIndici.TrimEnd(';');
                        // inserisco asterisco
                        foreach (string s in listaIndici.Split(';'))
                        {
                            lv.Items[int.Parse(s)].SubItems[7].Text = "(*)";
                            Utility.UpdateElementConAsterisco(int.Parse(s) + 1, true);
                        }

                    }

                    numSpinsValide = 0;
                    listaIndici = "";
                    newSpin = false;
                }
            }

            // se la trottola ultimo elemento
            if (numSpinsValide < 2 && newSpin)
            {
                listaIndici = listaIndici.TrimEnd(';');
                // inserisco asterisco
                foreach (string s in listaIndici.Split(';'))
                {
                    lv.Items[int.Parse(s)].SubItems[7].Text = "(*)";
                    Utility.UpdateElementConAsterisco(int.Parse(s) + 1, true);
                }

            }
        }

        public void CheckComboContactSpin()
        {
            int numSpinsValide = 0;
            int numComboSpins = 0;
            string listaIndici = "";
            List<string[]> comboSpins = new List<string[]>(10);
            bool newSpin = false;

            foreach (ListViewItem lvi in lv.Items)
            {
                // azzero se prima trottola
                if (lvi.SubItems[3].Text.Equals("ContactSpin 1"))
                {
                    numComboSpins++;
                    newSpin = true;
                }

                // se trottola combo e Confirmed
                if (lvi.SubItems[3].Text.StartsWith("ContactSpin ") &&
                  (!lvi.SubItems[2].Text.Equals("0,00") &&
                   !lvi.SubItems[2].Text.Equals("0.00")))
                {
                    numSpinsValide++;
                    listaIndici += lvi.Index + ";";
                }
                else // se trottola combo e non confermata
                if (lvi.SubItems[3].Text.StartsWith("ContactSpin ") &&
                  (lvi.SubItems[2].Text.Equals("0,00") ||
                   lvi.SubItems[2].Text.Equals("0.00")))
                {
                    listaIndici += lvi.Index + ";";
                }
                else if (!(lvi.SubItems[3].Text.StartsWith("ContactSpin "))) // trottola terminata
                {
                    if (numSpinsValide < 2 && newSpin)
                    {
                        listaIndici = listaIndici.TrimEnd(';');
                        // inserisco asterisco
                        foreach (string s in listaIndici.Split(';'))
                        {
                            lv.Items[int.Parse(s)].SubItems[7].Text = "(*)";
                            Utility.UpdateElementConAsterisco(int.Parse(s) + 1, true);
                        }

                    }

                    numSpinsValide = 0;
                    listaIndici = "";
                    newSpin = false;
                }
            }

            // se la trottola ultimo elemento
            if (numSpinsValide < 2 && newSpin)
            {
                listaIndici = listaIndici.TrimEnd(';');
                // inserisco asterisco
                foreach (string s in listaIndici.Split(';'))
                {
                    lv.Items[int.Parse(s)].SubItems[7].Text = "(*)";
                    Utility.UpdateElementConAsterisco(int.Parse(s) + 1, true);
                }

            }
        }

        public void ResetAfterInsertion()
        {
			try
            {
                if (Definizioni.updating)
            		lv.Items[currentElem].ForeColor = oldForeColor;

                Definizioni.updating = false;
                lv.Items[currentElem].BackColor = Color.Black;
                //if (Definizioni.updating)
                //    lv.Items[currentElem].ForeColor = oldForeColor;
                lv.Items[currentElem].Selected = false;
                currentElem = lv.Items.Count;

                EnableGroupBox();

            	if (lv.Items.Count > 0) back.Enabled = true;
            	else back.Enabled = false;
				// *** Modifica del 06/07/2018 - 1.0.4 *** //
                flowLayoutPanel1.Enabled = true;
                cancel.Enabled = false;
                startstop.Enabled = true;
                deductions.Enabled = true;
                //checkCombo2.Enabled = true;
                cbBonusSpin.Checked = false;
                cbBonusSpin2.Checked = false;
                cbComboLift1.Checked = false;
            	cbComboLift2.Checked = false;
            	cbComboLift3.Checked = false;
            	checkCombo1.Enabled = true;
            	checkCombo3.Enabled = true;
                // *** //

                //cancel.Enabled = false;
                //startstop.Enabled = true; 
                //confirm.Enabled = false; 
                //deductions.Enabled = true;
                //checkCombo2.Enabled = true;
                //cbBonusSpin.Checked = false;
                //average.Enabled = false;

                WriteLog();

                Thread.Sleep(100);
                //Thread.Sleep(500);
                Cursor.Show();
            }
            catch (Exception ex)
            {
                Utility.WriteLog("ResetAfterInsertion: " + ex.Message, "ERROR");
            }
        }

        public void ResetPercentualiTrottole()
        {
            try
            {
                cb25.SelectedItem = "0 %"; cb40.SelectedItem = "0 %";
                cb45.SelectedItem = "0 %"; cb48.SelectedItem = "0 %";
                cb47.SelectedItem = "0 %"; cb49.SelectedItem = "0 %";
                cb124.SelectedItem = "0 %"; 
                cb128.SelectedItem = "0 %";
                
                cb136.SelectedItem = "0 %"; cb144.SelectedItem = "0 %";
                cb148.SelectedItem = "0 %"; cb152.SelectedItem = "0 %";
                cb155.SelectedItem = "0 %"; cb159.SelectedItem = "0 %";
                cb163.SelectedItem = "0 %";
                cb126.SelectedItem = "0 %";
            }
            catch (Exception ex)
            {
                Utility.WriteLog("ResetPercentualiTrottole: " + ex.Message, "ERROR");
            }
        }

        public void WriteLog()
        {
            /********************************************************************************
         * [0] = durata            [1] = factor         [2] = numElems       [3] = numPosLift
         * [4] = NumComboLift      [5] = numTwistJumps  [6] = numThrowJumps  [7] = numSideJumps 
         * [8] = numSideComboJumps [9] = numContactSpins[10] = numSideSpins  [11] = numSpirals
         * [12] = numSteps
         * ******************************************************************************/
            log.Text = "Total=" + Definizioni.elemSegment[2] + "|" +
                       "PosLifts=" + Definizioni.elemSegment[3] + "|" +
                       "CombLifts=" + Definizioni.elemSegment[4] + "|" +
                       "TwistJumps=" + Definizioni.elemSegment[5] + "|" +
                       "ThrowJumps=" + Definizioni.elemSegment[6] + "|" +
                       "SideJumps=" + Definizioni.elemSegment[7] + "|" +
                       "SideComboJumps=" + Definizioni.elemSegment[8] + "|" +
                       "ContactSpins=" + Definizioni.elemSegment[9] + "|" +
                       "SideSpins=" + Definizioni.elemSegment[10] + "|" +
                       "DeathSpirals=" + Definizioni.elemSegment[11] + "|" +
                       "Steps=" + Definizioni.elemSegment[12] + "|\r\n\r\n";

                       //"ProgrEl      = " + progEl + "\r\n" +
                       //"COMBOJUMPS   = " + numComboJump+ "\r\n" +
                       //"COMOBSPINS   = " + numComboSpin + " " + firstTrottolaSoloCombo + "\r\n"+ 
                       //"COMBOCONTACT = " + numComboSpinContact + " " + firstTrottolaContactCombo + "\r\n";
            log.SelectionStart = log.Text.Length;
            log.ScrollToCaret();
        }
 
        public void ClearElement()
        {
            try
            {
                Cursor.Hide();
                int count = lv.Items.Count;
                if (count == 0)
                {
                    Cursor.Show();
                    return;
                }
                ListViewItem lastItem = lv.Items[count - 1];
                string numEl = lastItem.SubItems[0].Text;
                string codeEl = lastItem.SubItems[1].Text;
                string valEl = lastItem.SubItems[2].Text;
                string typeEl = lastItem.SubItems[3].Text;
                string cat = lastItem.SubItems[4].Text;
                string numcombo = lastItem.SubItems[5].Text;
                string du = lastItem.SubItems[6].Text;
                string note = lastItem.SubItems[7].Text;

                if (count > 1)
                {
                    if (lv.Items[count - 2].SubItems[3].Text.Contains("SideJump "))
                        saltoPrec = lv.Items[count - 2].SubItems[1].Text;
                }

                if (typeEl.Equals("SideJump")) // side singolo
                {
                    // tolgo un elemento salto singolo side
                    Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) - 1;
                    Definizioni.elemSegment[7] = int.Parse(Definizioni.elemSegment[7].ToString()) - 1;
                    progEl--;
                }
                if (typeEl.Contains("SideJump ")) // combo side singolo
                {
                    // tolgo un elemento dalla combinazione
                    if (typeEl.Equals("SideJump 1"))
                    {
                        progEl--;
                        //Definizioni.elemSegment[7] = int.Parse(Definizioni.elemSegment[7].ToString()) - 1;
                        Definizioni.elemSegment[8] = int.Parse(Definizioni.elemSegment[8].ToString()) - 1;
                        Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) - 1;
                    }
                    if (numcombo.Equals("1"))
                        saltoPrec = "";
                    //progEl--;
                    //numComboJump--;
                    numComboJump = int.Parse(numcombo);
                }
                if (typeEl.Equals("ThrowJump")) // throw jump
                {
                    progEl--;
                    Definizioni.elemSegment[6] = int.Parse(Definizioni.elemSegment[6].ToString()) - 1;
                    Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) - 1;
                }
                if (typeEl.Equals("TwistJump")) // twist jump
                {
                    progEl--;
                    Definizioni.elemSegment[5] = int.Parse(Definizioni.elemSegment[5].ToString()) - 1;
                    Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) - 1;
                }
                if (typeEl.Equals("SideSpin") || typeEl.Equals("SideSpin 1")) // side spin o combo side spin
                {
                    progEl--;
                    Definizioni.elemSegment[10] = int.Parse(Definizioni.elemSegment[10].ToString()) - 1;
                    Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) - 1;
                    numComboSpin = 1;
                    firstTrottolaSoloCombo = true;
                } else if (typeEl.StartsWith("SideSpin") && int.Parse(numcombo) > 1)
                {
                    firstTrottolaSoloCombo = false;
                }
                if (typeEl.Equals("ContactSpin") || typeEl.Equals("ContactSpin 1")) // contact spin o combo contact spin
                {
                    progEl--;
                    Definizioni.elemSegment[9] = int.Parse(Definizioni.elemSegment[9].ToString()) - 1;
                    Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) - 1;
                    numComboSpinContact = 1;
                    firstTrottolaContactCombo = true;
                }
                else if (typeEl.StartsWith("ContactSpin") && int.Parse(numcombo) > 1)
                {
                    firstTrottolaContactCombo = false;
                }
                if (typeEl.Equals("PosLift")) // pos lift
                {
                    progEl--;
                    Definizioni.elemSegment[3] = int.Parse(Definizioni.elemSegment[3].ToString()) - 1;
                    Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) - 1;
                }
                if (typeEl.Equals("CombLift")) // combo lift
                {
                    progEl--;
                    Definizioni.elemSegment[4] = int.Parse(Definizioni.elemSegment[4].ToString()) - 1;
                    Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) - 1;
                }  
                if (typeEl.Equals("DeathSpiral")) // spirale
                {
                    progEl--;
                    Definizioni.elemSegment[11] = int.Parse(Definizioni.elemSegment[11].ToString()) - 1;
                    Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) - 1;
                }
                if (typeEl.Equals("Steps")) // passi
                {
                    progEl--;
                    Definizioni.elemSegment[12] = int.Parse(Definizioni.elemSegment[12].ToString()) - 1;
                    Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) - 1;
                }
                currentElem--;
                lv.Items[count - 1].Remove();

                if (int.Parse(numcombo) <= 1 || typeEl.Contains("SideJump ")) // %%% modifica del 31/03/2018
                    Utility.SendBroadcast("<ANNULLA/>"); // invio il comando di annulla ai giudici 
                else
                {
                    //if (typeEl.StartsWith("ContactSpin")) numComboSpinContact--;
                    //if (typeEl.StartsWith("SideSpin")) numComboSpin--;
                    if (typeEl.StartsWith("ContactSpin")) numComboSpinContact = int.Parse(numcombo);
                    if (typeEl.StartsWith("SideSpin")) numComboSpin = int.Parse(numcombo);
                }
                Utility.ScrivoElementoOnDB(lv, log, true);
                
                // Aggiorno i totali
                if (!note.Contains("*"))
                    Utility.AggiornoTotali(log, lv, deductions, elements, total);

                if (count > 0) back.Enabled = true;
                else back.Enabled = false;

                WriteLog();
                Thread.Sleep(200);
                Cursor.Show();
                error.ResetText(); error.Visible = false;
            }

            catch (Exception ex)
            {
                Cursor.Show();
                Utility.WriteLog("ClearElement: " + ex.Message, "ERROR");
            }
        }
        #endregion

        #region SALTI, TROTTOLE, PASSI, LIFTS, SPIRALI, ...
        #region Jumps
        /**********************************************************
         *  Salti
         *  [5] = numTwistJumps  [6] = numThrowJumps  [7] = numSideJumps 
         *  [8] = numSideComboJumps 
         * ********************************************************/
        private void InsertElement_Click(object sender, EventArgs e)
        {
            try
            {
                firstTrottolaContactCombo = true;
                firstTrottolaSoloCombo = true;
                numComboSpin = 1; numComboSpinContact = 1;
                var element = (Button)sender;
                int numelement = int.Parse(element.Name.Remove(0,2));
                bool saltosemplice = false;
                if (element.Name == "el1" || element.Name == "el2" || element.Name == "el3" ||
                    element.Name == "el4" || element.Name == "el5" || element.Name == "el6" ||
                    element.Name == "el77" || element.Name == "el78" || element.Name == "el79" ||
                    element.Name == "el80" || element.Name == "el81" || element.Name == "el104" ||
                    element.Name == "el105" || element.Name == "el106" || element.Name == "el107" ||
                    element.Name == "el0" || element.Name == "el75") // aggiungo Walt Jump
                    saltosemplice = true;
                Button saltoungiro = null;
                if (!saltosemplice && !element.Text.Equals("0") && element.Name != "el81")
                    saltoungiro = ((Button)this.Controls.Find("el" + (numelement - 6), true)[0]);
                int tipoSalto = 0;
                if (!cbCombo.Checked) tipoSalto = 1; // salto singolo
                else tipoSalto = 2;// salto in combinazione 

                // verifico se lanciato o twist
                if (element.Parent.Name == "tabLanciati")
                {
                    tipoSalto = 3;
                    numComboJump = 1;
                }
                if (element.Parent.Name == "tabTwist")
                {
                    tipoSalto = 4;
                    numComboJump = 1;
                }
                InsertElement(saltosemplice, element, saltoungiro, tipoSalto);
            } 
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void InsertElement(bool saltoSemplice, Button salto, Button ungiro, int tipoSalto)
        {
            try
            {
                string nome = ((DataRow)salto.Tag)[1].ToString(); // nome salto
                string code = ((DataRow)salto.Tag)[3].ToString(); // codice del salto
                string valueBase = ((DataRow)salto.Tag)[7].ToString(); // valore base del salto
                string valueUnder = ((DataRow)salto.Tag)[8].ToString(); // under
                string valueHalf = ((DataRow)salto.Tag)[9].ToString(); // half
                string valueCombo = ((DataRow)salto.Tag)[13].ToString();// valore salto combo
                string valueComboU = ((DataRow)salto.Tag)[14].ToString();// valore salto combo under
                string valueComboH = ((DataRow)salto.Tag)[15].ToString();// valore salto combo half

                Cursor.Hide();
                //int numCombo = 1;
                addAsterisco = false;
                decimal valoreSalto = 0.00m;
                if (tipoSalto==2) // combinazioni
                {
                    if (!Definizioni.updating)
                    {
                        //numCombo = int.Parse(Definizioni.elemSegment[8].ToString()) + 1;
                        //if (!CheckShortAndLongProgram(1, saltoSemplice, numCombo)) // %%% modifica del 31/03/2018
                        if (!CheckShortAndLongProgram(2, saltoSemplice, numComboJump))
                        {
                            addAsterisco = true;
                        }
                        newCombo.Enabled = true;
                        lv.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "", "", "", "" }, -1));
                    }
                    else // aggiornamento
                    {
                        //if (!CheckShortAndLongProgram(1, saltoSemplice, numCombo)) // %%% modifica del 31/03/2018
                        if (!CheckShortAndLongProgram(2, saltoSemplice, numComboJump))
                        {
                            addAsterisco = true;
                        }
                        newCombo.Enabled = true;
                        numComboJump = int.Parse(lv.Items[currentElem].SubItems[3].Text.Substring(9, 1));
                        // %%% modifica del 18/10/2018 - 2.0
                        if (lv.Items[currentElem].SubItems[7].Text.Contains("*"))
                            addAsterisco = true;
                        lv.Items[currentElem].SubItems[7].Text = "";
                    }
                    saltoCorr = code; // codice salto
                    valoreSalto = decimal.Parse(valueCombo); // valore salto combo
                    lv.Items[currentElem].SubItems[1].Text = "";
                    lv.Items[currentElem].SubItems[1].Text = code; // codice salto
                    lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", valoreSalto);
                    lv.Items[currentElem].Tag = valoreSalto + ""; // imposto il valore del salto anche nel tag
                    lv.Items[currentElem].SubItems[3].Text = "SideJump " + numComboJump;
                    lv.Items[currentElem].SubItems[4].Text = "J";
                    //lv.Items[currentElem].SubItems[5].Text = int.Parse(Definizioni.elemSegment[13].ToString()) + 1 + "";
                    lv.Items[currentElem].SubItems[5].Text = numComboJump + "";
                    lv.Items[currentElem].SubItems[6].Text = "";
                    lv.Items[currentElem].ForeColor = rigaComboJump;
                    lv.Items[currentElem].ToolTipText = nome; // nome salti

                    // aggiungo un elemento salto alla combinazione se non sto in modifica elemento
                    if (!Definizioni.updating)
                    {
                        //Definizioni.elemSegment[8] = int.Parse(Definizioni.elemSegment[8].ToString()) + 1;
                        if (numComboJump == 1)
                        {
                            progEl++;
                            //Definizioni.elemSegment[7] = int.Parse(Definizioni.elemSegment[7].ToString()) + 1;
                            Definizioni.elemSegment[8] = int.Parse(Definizioni.elemSegment[8].ToString()) + 1;
                            Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) + 1;
                        }
                        numComboJump++;
                    }

                }
                else //salti singoli, twist e lanciati
                {
                    // 2.0.1-BUG FIX 
                    numComboJump = 1;

                    newCombo.Enabled = false;
                    //Definizioni.elemSegment[8] = 0; // azzero gli elementi di una combinazione
                    if (!CheckShortAndLongProgram(tipoSalto, false, 0))
                    {
                        addAsterisco = true;
                    }

                    //if (!Definizioni.updating)
                    //    lv.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "", "", "", "" }, -1));
                    // %%% modifica del 18/10/2018 - 2.0
                    if (!Definizioni.updating)
                    {
                        lv.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "", "", "", "" }, -1));
                        lv.Items[currentElem].SubItems[7].Text = "";
                    }
                    else
                    {
                        if (lv.Items[currentElem].SubItems[7].Text.Contains("*"))
                            addAsterisco = true;
                        lv.Items[currentElem].SubItems[7].Text = "";
                    }

                    lv.Items[currentElem].SubItems[1].Text = code; // codice salto
                    valoreSalto = decimal.Parse(valueBase);
                    lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", valoreSalto);
                    lv.Items[currentElem].SubItems[3].Text = "SideJump";
                    if (tipoSalto == 3) lv.Items[currentElem].SubItems[3].Text = "ThrowJump";
                    if (tipoSalto == 4) lv.Items[currentElem].SubItems[3].Text = "TwistJump";
                    lv.Items[currentElem].SubItems[4].Text = "J";
                    lv.Items[currentElem].SubItems[5].Text = "0";
                    lv.Items[currentElem].SubItems[6].Text = "";
                    lv.Items[currentElem].ToolTipText = nome; // nome salti

                    lv.Items[currentElem].ForeColor = rigaSoloJump;
                    if (tipoSalto == 3) lv.Items[currentElem].ForeColor = rigaLanciati;
                    if (tipoSalto == 4) lv.Items[currentElem].ForeColor = rigaTwist;
                    // aggiungo un elemento salto singolo
                    if (!Definizioni.updating)
                    {
                        progEl++;
                        if (tipoSalto == 1) Definizioni.elemSegment[7] = int.Parse(Definizioni.elemSegment[7].ToString()) + 1;
                        if (tipoSalto == 3) Definizioni.elemSegment[6] = int.Parse(Definizioni.elemSegment[6].ToString()) + 1;
                        if (tipoSalto == 4) Definizioni.elemSegment[5] = int.Parse(Definizioni.elemSegment[5].ToString()) + 1;
                        Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) + 1;
                    }
                    saltoPrec = "";
                }

                // se il salto è nojump o underotated o downgraded
                if (salto.Name.Equals("el64") || salto.Name.Equals("el102") || salto.Name.Equals("el226"))
                {
                    lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", 0); //"0,00";
                    lv.Items[currentElem].SubItems[6].Text = "";
                }
                else if (Definizioni.underJump)
                {
                    lv.Items[currentElem].SubItems[1].Text += " <";
                    if (tipoSalto == 2) lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", decimal.Parse(valueComboU));
                    else lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", decimal.Parse(valueUnder));
                    lv.Items[currentElem].SubItems[6].Text = "U";
                }
                else if (Definizioni.halfJump)
                {
                    lv.Items[currentElem].SubItems[1].Text += " <<";
                    if (tipoSalto == 2) lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", decimal.Parse(valueComboH));
                    else lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", decimal.Parse(valueHalf));
                    lv.Items[currentElem].SubItems[6].Text = "H";
                }
                else if (Definizioni.downJump)
                {
                    lv.Items[currentElem].SubItems[1].Text += " <<<";
                    //if (saltoSemplice) lv.Items[currentElem].SubItems[2].Text = "0,00";
                    if (saltoSemplice) lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", 0); //"0,00";
                    else if (tipoSalto == 2) lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", decimal.Parse(((DataRow)ungiro.Tag)[13].ToString()));
                    else lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", decimal.Parse(((DataRow)ungiro.Tag)[7].ToString()));
                    lv.Items[currentElem].SubItems[6].Text = "D";
                }

                // *** modifica del 10/07/2018 - verifico i salti duplicati *** //
                //if (RollartSystemTech.Properties.Settings.Default.CheckDuplicates)
                //{
                //    // verifico i salti duplicati
                //    CheckDuplicati(lv.Items[currentElem]);
                //}
                // *** //

                if (addAsterisco) lv.Items[currentElem].SubItems[7].Text = "(*)";
                else lv.Items[currentElem].SubItems[7].Text = "";

                //if (numComboJump == 1 && !Definizioni.updating)
                if (numComboJump <= 2 && !Definizioni.updating)
                    lv.Items[currentElem].Text = progEl + "";

                if (!addAsterisco) CalcoloMaggiorazioni();

                down.BackColor = Color.Silver;
                under.BackColor = Color.Silver;
                down2.BackColor = Color.Silver;
                under2.BackColor = Color.Silver;
                down3.BackColor = Color.Silver;
                under3.BackColor = Color.Silver;
                half.BackColor = Color.Silver;
                half2.BackColor = Color.Silver;
                half3.BackColor = Color.Silver;
                Definizioni.downJump = false; Definizioni.underJump = false; Definizioni.halfJump = false; 

                // se l'invio ai giudice va a buon fine aggiorno il db
                lv.Items[currentElem].Selected = true;
                if (Utility.InvioDati(lv, log))
                    Utility.ScrivoElementoOnDB(lv, log, false);

                // Aggiorno i totali
                if (!addAsterisco) Utility.AggiornoTotali(log, lv, deductions, elements, total);

                ResetAfterInsertion();
                Cursor.Current = Cursors.Hand;
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Hand;
                Utility.WriteLog("InsertElement: " + ex.Message, "ERROR");
            }
        }

        private void newCombo_Click(object sender, EventArgs e)
        {
            try
            {
                //Definizioni.elemSegment[8] = 0;
                numComboJump = 1;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("NewCombo: " + ex.Message, "ERROR");
            }
        }

        public void CalcoloMaggiorazioni()
        {
            try
            {
                string tipo = "", salto = "", saltoPrec = "";
                decimal valore;
                bool comboPrec = false;
                ListViewItem itemPrec = null;
                foreach (ListViewItem lvi in lv.Items)
                {
                    tipo = lvi.SubItems[3].Text;
                    if (!tipo.Contains("SideJump "))
                    {
                        comboPrec = false;
                        itemPrec = null;
                    }
                    else
                        if (tipo.Contains("SideJump ") && comboPrec && (!tipo.Equals("SideJump 1")))
                        {
                            if (lvi.Index == currentElem)
                            {
                                //salto = lvi.SubItems[1].Text;
                                //valore = decimal.Parse(lvi.Tag.ToString());
                                //saltoPrec = itemPrec.SubItems[1].Text;
                                salto = lvi.SubItems[1].Text;
                                // BUG FIX: Recupero il valore del salto under-half-down
                                valore = decimal.Parse(lvi.Tag.ToString());

                                if (Definizioni.underJump || Definizioni.downJump || Definizioni.halfJump)
                                    valore = decimal.Parse(lvi.SubItems[2].Text); // salto falloso

                                saltoPrec = itemPrec.SubItems[1].Text;

                                if (salto.StartsWith("2") && saltoPrec.StartsWith("2")) // D/D
                                {
                                    if (Definizioni.paramSegment[19] != 0) // solo per categorie piccole
                                    {
                                        lvi.SubItems[2].Text = valore + Math.Round(((valore * Definizioni.paramSegment[19]) / 100), 2) + ""; // 10%
                                        lvi.SubItems[7].Text = Definizioni.paramSegment[19] + "%"; // 10%
                                    }
                                }
                                if (salto.StartsWith("2") && saltoPrec.StartsWith("3")) // D/T
                                {
                                    lvi.SubItems[2].Text = valore + Math.Round(((valore * Definizioni.paramSegment[20]) / 100), 2) + ""; // 20%
                                    lvi.SubItems[7].Text = Definizioni.paramSegment[20] + "%"; // 20%
                                }
                                if (salto.StartsWith("3") && saltoPrec.StartsWith("2")) // T/D
                                {
                                    lvi.SubItems[2].Text = valore + Math.Round(((valore * Definizioni.paramSegment[20]) / 100), 2) + ""; // 20%
                                    lvi.SubItems[7].Text = Definizioni.paramSegment[20] + "%"; // 20%
                                }
                                if (salto.StartsWith("3") && saltoPrec.StartsWith("3")) // T/T
                                {
                                    lvi.SubItems[2].Text = valore + Math.Round(((valore * Definizioni.paramSegment[21]) / 100), 2) + ""; // 30%
                                    lvi.SubItems[7].Text = Definizioni.paramSegment[21] + "%"; // 30%
                                } 
                            }
                            itemPrec = lvi;
                            comboPrec = true;
                        }
                        else
                        if (tipo.Contains("SideJump "))
                        {
                            itemPrec = lvi;
                            comboPrec = true;
                        }
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("CalcoloMaggiorazioni: " + ex.Message, "ERROR");
            }
        }

        private void down_Click(object sender, EventArgs e)
        {
            if (Definizioni.downJump)
            {
                Definizioni.downJump = false; down.BackColor = Color.Silver;
                Definizioni.underJump = false; under.BackColor = Color.Silver;
                Definizioni.halfJump = false; half.BackColor = Color.Silver;
            }
            else
            {
                Definizioni.downJump = true; down.BackColor = Color.Orange;
                Definizioni.underJump = false; under.BackColor = Color.Silver;
                Definizioni.halfJump = false; half.BackColor = Color.Silver;
            }
        }

        private void under_Click(object sender, EventArgs e)
        {
            if (Definizioni.underJump)
            {
                Definizioni.downJump = false; down.BackColor = Color.Silver;
                Definizioni.underJump = false; under.BackColor = Color.Silver;
                Definizioni.halfJump = false; half.BackColor = Color.Silver;
            }
            else
            {
                Definizioni.downJump = false; down.BackColor = Color.Silver;
                Definizioni.underJump = true; under.BackColor = Color.Orange;
                Definizioni.halfJump = false; half.BackColor = Color.Silver;
            }
        }

        private void half_Click(object sender, EventArgs e)
        {
            if (Definizioni.halfJump)
            {
                Definizioni.downJump = false; down.BackColor = Color.Silver;
                Definizioni.underJump = false; under.BackColor = Color.Silver;
                Definizioni.halfJump = false; half.BackColor = Color.Silver;
            }
            else
            {
                Definizioni.downJump = false; down.BackColor = Color.Silver;
                Definizioni.underJump = false; under.BackColor = Color.Silver;
                Definizioni.halfJump = true; half.BackColor = Color.Orange;
            }
        }

        // Lanciati
        private void under2_Click(object sender, EventArgs e)
        {
            if (Definizioni.underJump)
            {
                Definizioni.downJump = false; down2.BackColor = Color.Silver;
                Definizioni.underJump = false; under2.BackColor = Color.Silver;
                Definizioni.halfJump = false; half2.BackColor = Color.Silver;
            }
            else
            {
                Definizioni.downJump = false; down2.BackColor = Color.Silver;
                Definizioni.underJump = true; under2.BackColor = Color.Orange;
                Definizioni.halfJump = false; half2.BackColor = Color.Silver;
            }
        }

        private void down2_Click(object sender, EventArgs e)
        {
            if (Definizioni.downJump)
            {
                Definizioni.downJump = false; down2.BackColor = Color.Silver;
                Definizioni.underJump = false; under2.BackColor = Color.Silver;
                Definizioni.halfJump = false; half2.BackColor = Color.Silver;
            }
            else
            {
                Definizioni.downJump = true; down2.BackColor = Color.Orange;
                Definizioni.underJump = false; under2.BackColor = Color.Silver;
                Definizioni.halfJump = false; half2.BackColor = Color.Silver;
            }
        }

        private void half2_Click(object sender, EventArgs e)
        {
            if (Definizioni.halfJump)
            {
                Definizioni.downJump = false; down2.BackColor = Color.Silver;
                Definizioni.underJump = false; under2.BackColor = Color.Silver;
                Definizioni.halfJump = false; half2.BackColor = Color.Silver;
            }
            else
            {
                Definizioni.downJump = false; down2.BackColor = Color.Silver;
                Definizioni.underJump = false; under2.BackColor = Color.Silver;
                Definizioni.halfJump = true; half2.BackColor = Color.Orange;
            }
        }

        // Twist
        private void under3_Click(object sender, EventArgs e)
        {
            if (Definizioni.underJump)
            {
                Definizioni.downJump = false; down3.BackColor = Color.Silver;
                Definizioni.underJump = false; under3.BackColor = Color.Silver;
                Definizioni.halfJump = false; half3.BackColor = Color.Silver;
            }
            else
            {
                Definizioni.downJump = false; down3.BackColor = Color.Silver;
                Definizioni.underJump = true; under3.BackColor = Color.Orange;
                Definizioni.halfJump = false; half3.BackColor = Color.Silver;
            }
            
        }

        private void down3_Click(object sender, EventArgs e)
        {
            if (Definizioni.downJump)
            {
                Definizioni.downJump = false; down3.BackColor = Color.Silver;
                Definizioni.underJump = false; under3.BackColor = Color.Silver;
                Definizioni.halfJump = false; half3.BackColor = Color.Silver;
            }
            else
            {
                Definizioni.downJump = true; down3.BackColor = Color.Orange;
                Definizioni.underJump = false; under3.BackColor = Color.Silver;
                Definizioni.halfJump = false; half3.BackColor = Color.Silver;
            }
        }

        private void half3_Click(object sender, EventArgs e)
        {
            if (Definizioni.halfJump)
            {
                Definizioni.downJump = false; down3.BackColor = Color.Silver;
                Definizioni.underJump = false; under3.BackColor = Color.Silver;
                Definizioni.halfJump = false; half3.BackColor = Color.Silver;
            }
            else
            {
                Definizioni.downJump = false; down3.BackColor = Color.Silver;
                Definizioni.underJump = false; under3.BackColor = Color.Silver;
                Definizioni.halfJump = true; half3.BackColor = Color.Orange;
            }
        }

        #endregion

        #region Spins
        /**********************************************************
        *  Trottole
         *  tipo: 1=singole 2=combo 3=contact
        * ********************************************************/
        private void InsertTrottola_Click(object sender, EventArgs e)
         {
            try
            {
                var element = (Button)sender;
                int tipoTrottola = 0;
                if ((element.Parent.Name == "tabSpin" || element.Name == "el65") && !checkCombo1.Checked) // trottole singole 
                    tipoTrottola = 1;
                if ((element.Parent.Name == "tabSpin" || element.Name == "el65") && checkCombo1.Checked) // trottole singole combinate
                    tipoTrottola = 2;
                if ((element.Parent.Name.StartsWith("panelContactCombo") || element.Name == "el65_") && !checkCombo3.Checked) // trottole contact
                    tipoTrottola = 3;
                if ((element.Parent.Name.StartsWith("panelContactCombo") || element.Name == "el65_") && checkCombo3.Checked) // trottole contact combinate
                    tipoTrottola = 4;
                InsertTrottola(element, tipoTrottola);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void InsertTrottola(Button tr, int tipoTrottola)
        {
            try
            {
                Cursor.Hide();
                decimal valoreTrottola = 0.00m;
                int percentuale = 0;
                newComboSpin.Enabled = false;
                newComboSpin2.Enabled = false;
                string typeTrottola = "";
                ComboBox comboPercentuale = null;
                addAsterisco = false;
                //Definizioni.elemSegment[8] = 0;
                numComboJump = 1;

                if (Definizioni.updating)
                    typeTrottola = lv.Items[currentElem].SubItems[3].Text;

                // calcolo percentuale su ogni trottola
                string numElement = tr.Name.ToString().Substring(2); // recupero il numero dell'elemento
                if (this.Controls.Find("cb" + numElement, true).Length > 0)
                    comboPercentuale = ((ComboBox)this.Controls.Find("cb" + numElement, true)[0]);

                // calcolo percentuale su ogni trottola
                if (comboPercentuale != null)
                {
                    percentuale = int.Parse(comboPercentuale.SelectedItem.ToString().Split(' ')[0]);
                }
                
                switch (tipoTrottola)
                {
                    case 1: // solo spin singola
                    case 3: // contact spin singola
                        if (tipoTrottola == 1)
                        {
                            if (!CheckShortAndLongProgram(5, false, 0))
                            {
                                addAsterisco = true;
                            }
                        }
                        else
                        {
                            if (!CheckShortAndLongProgram(7, false, 0))
                            {
                                addAsterisco = true;
                            }
                            
                        }
                        numComboSpin = 1; numComboSpinContact = 1;
                        if (!Definizioni.updating)
                        {
                            lv.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "", "", "", "" }, -1));
                        } else
                        {
                            // %%% modifica del 18/10/2018 - 2.0
                            if (lv.Items[currentElem].SubItems[7].Text.Contains("*"))
                                addAsterisco = true;
                            lv.Items[currentElem].SubItems[7].Text = "";
                        }
                        lv.Items[currentElem].SubItems[1].Text = ((DataRow)tr.Tag)[3].ToString();
                        valoreTrottola = decimal.Parse(((DataRow)tr.Tag)[7].ToString())
                            + Math.Round(((decimal.Parse(((DataRow)tr.Tag)[7].ToString()) * percentuale) / 100), 2);
                        if (comboPercentuale != null)
                        {
                            // *** modifica del 21/06/2017 - 1.0.1 - bonus di più 2 a tutte le trottole
                            // *** modifica del 26/06/2018 - 1.0.4 - bonus parametrico 
                            //if (comboPercentuale.Name.Equals("cb25") && cbBonusSpin.Checked)
                            if (cbBonusSpin.Checked)
                            {
                                //valoreTrottola = valoreTrottola + 2.00m;
                                if (Definizioni.paramSegment[22] > 0)
                                    valoreTrottola = valoreTrottola + Definizioni.paramSegment[22];
                                else valoreTrottola = valoreTrottola + 2.00m;
                            }
                            // *** //
                        }
                        lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", valoreTrottola);
                        if (tipoTrottola == 1) lv.Items[currentElem].SubItems[3].Text = "SideSpin";
                        else lv.Items[currentElem].SubItems[3].Text = "ContactSpin";
                        if (tipoTrottola == 1) lv.Items[currentElem].SubItems[4].Text = "S";
                        else lv.Items[currentElem].SubItems[4].Text = "CS";
                        lv.Items[currentElem].SubItems[5].Text = "0";

                        lv.Items[currentElem].SubItems[7].Text = "";
                        if (cbBonusSpin.Checked)
                            lv.Items[currentElem].SubItems[7].Text += "B ";
                        if (percentuale > 0)
                            lv.Items[currentElem].SubItems[7].Text += "+ " + percentuale + " %";

                        //else lv.Items[currentElem].SubItems[7].Text += "";
                        lv.Items[currentElem].ToolTipText = ((DataRow)tr.Tag)[1].ToString();
                        if (tipoTrottola == 1) lv.Items[currentElem].ForeColor = rigaSpin;
                        else lv.Items[currentElem].ForeColor = rigaContactSpin;
                        if (!Definizioni.updating)
                        {
                            if (tipoTrottola == 1) Definizioni.elemSegment[10] = int.Parse(Definizioni.elemSegment[10].ToString()) + 1;
                            else Definizioni.elemSegment[9] = int.Parse(Definizioni.elemSegment[9].ToString()) + 1;
                            Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) + 1;
                            progEl++;
                            lv.Items[currentElem].Text = progEl + "";
                        }

                        break;
                    case 2: // solo spin combinata
                    case 4: // contact spin combinata
                        if (tipoTrottola == 2)
                        {
                            if (!CheckShortAndLongProgram(6, false, numComboSpin))
                            {
                                addAsterisco = true;
                            }
                            newComboSpin.Enabled = true;
                            numComboSpinContact = 1;
                        }
                        else
                        {
                            if (!CheckShortAndLongProgram(7, false, numComboSpinContact))
                            {
                                addAsterisco = true;
                            }
                            newComboSpin2.Enabled = true;
                            numComboSpin = 1;
                        }

                        if (!Definizioni.updating)
                            lv.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "", "", "", "" }, -1));
                        else // aggiornamento
                        {
                            //if (tipoTrottola == 2) numComboSpin = int.Parse(lv.Items[currentElem].SubItems[3].Text.Substring(9, 1));
                            // **** risolta anomalia il 24/04/2018: .Substring(9, 1) --> .Substring(12, 1)
                            //if (tipoTrottola == 4) numComboSpinContact = int.Parse(lv.Items[currentElem].SubItems[3].Text.Substring(12, 1));

                            // %%% modifica del 18/10/2018 - 2.0
                            if (lv.Items[currentElem].SubItems[7].Text.Contains("*"))
                                addAsterisco = true;
                            lv.Items[currentElem].SubItems[7].Text = "";
                        }

                        lv.Items[currentElem].SubItems[1].Text = ((DataRow)tr.Tag)[3].ToString();
                        valoreTrottola = decimal.Parse(((DataRow)tr.Tag)[7].ToString())
                            + Math.Round(((decimal.Parse(((DataRow)tr.Tag)[7].ToString()) * percentuale) / 100), 2);
                        if (comboPercentuale != null)
                        {
                            // *** modifica del 21/06/2017 - 1.0.1 - bonus di più 2 a tutte le trottole
                            // *** modifica del 26/06/2018 - 1.0.4 - bonus parametrico 
                            //if (comboPercentuale.Name.Equals("cb25") && cbBonusSpin.Checked)
                            if ((tipoTrottola == 2 && cbBonusSpin.Checked) || (tipoTrottola == 4 && cbBonusSpin2.Checked))
                            {
                                //valoreTrottola = valoreTrottola + 2.00m;
                                if (Definizioni.paramSegment[22] > 0)
                                    valoreTrottola = valoreTrottola + Definizioni.paramSegment[22];
                                else valoreTrottola = valoreTrottola + 2.00m;
                            }
                            // *** //
                        }
                        lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", valoreTrottola);
                        if (!Definizioni.updating)
                        {
                            if (tipoTrottola == 2)
                            {
                                lv.Items[currentElem].SubItems[3].Text = "SideSpin " + numComboSpin;
                                lv.Items[currentElem].SubItems[4].Text = "S";
                                lv.Items[currentElem].SubItems[5].Text = numComboSpin + "";
                                lv.Items[currentElem].ForeColor = rigaComboSpin;
                            }
                            else
                            {
                                lv.Items[currentElem].SubItems[3].Text = "ContactSpin " + numComboSpinContact;
                                lv.Items[currentElem].SubItems[4].Text = "CS";
                                lv.Items[currentElem].SubItems[5].Text = numComboSpinContact + "";
                                lv.Items[currentElem].ForeColor = rigaContactSpin;
                            }
                        }

                        lv.Items[currentElem].SubItems[7].Text = "";
                        if (cbBonusSpin.Checked || cbBonusSpin2.Checked)
                            lv.Items[currentElem].SubItems[7].Text += "B ";
                        if (percentuale > 0)
                            lv.Items[currentElem].SubItems[7].Text += "+ " + percentuale + " %";
                        //else lv.Items[currentElem].SubItems[7].Text += "";

                        lv.Items[currentElem].ToolTipText = ((DataRow)tr.Tag)[1].ToString();

                        // aggiungo un elemento trottola alla combinazione se non sto in modifica elemento
                        if (!Definizioni.updating)
                        {
                            if (tipoTrottola == 2)
                            {
                                firstTrottolaContactCombo = true;
                                if (firstTrottolaSoloCombo)
                                {
                                    Definizioni.elemSegment[10] = int.Parse(Definizioni.elemSegment[10].ToString()) + 1;
                                    Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) + 1;
                                    firstTrottolaSoloCombo = false;
                                    progEl++;
                                    lv.Items[currentElem].Text = progEl + "";
                                }
                                numComboSpin++;
                            }
                            else
                            {
                                firstTrottolaSoloCombo = true;
                                if (firstTrottolaContactCombo)
                                {
                                    Definizioni.elemSegment[9] = int.Parse(Definizioni.elemSegment[9].ToString()) + 1;
                                    Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) + 1;
                                    firstTrottolaContactCombo = false;
                                    progEl++;
                                    lv.Items[currentElem].Text = progEl + "";
                                }
                                numComboSpinContact++;
                            }
                        }
                        
                        break;
                    default:
                        break;

                }

                if (addAsterisco) lv.Items[currentElem].SubItems[7].Text = "(*)";

                if (tr == el65 || tr == el65_)
                {
                    //lv.Items[currentElem].SubItems[2].Text = "0,00";
                    lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", 0); //"0,00";
                    lv.Items[currentElem].SubItems[6].Text = "";
                }

                lv.Items[currentElem].Selected = true;
                if (numComboSpin <= 2 && numComboSpinContact <= 2 && !Definizioni.updating)
                {
                    lv.Items[currentElem].Text = progEl + "";
                    // invio al giudice solo la prima trottola per trottole combo
                    if (Utility.InvioDati(lv, log))
                        Utility.ScrivoElementoOnDB(lv, log, false);
                }
                else // altrimenti scrivo solo sul db
                {
                    Utility.ScrivoElementoOnDB(lv, log, false);
                }

                // Aggiorno i totali
                if (!addAsterisco) Utility.AggiornoTotali(log, lv, deductions, elements, total);

                if (comboPercentuale != null) comboPercentuale.SelectedIndex = 0;
                ResetAfterInsertion();
            }
            catch (Exception ex)
            {
                Cursor.Show();
                Utility.WriteLog("InsertTrottola: " + ex.Message, "ERROR");
            }
        }

        private void checkCombo2_CheckedChanged(object sender, EventArgs e)
        {

        }


#endregion

        #region Spirals
        /**********************************************************
        *  Spirali
        * ********************************************************/
        public void InsertSpirali(Button spirali)
        {
            try
            {
                Cursor.Hide();
                newCombo.Enabled = false;
                addAsterisco = false;
                //Definizioni.elemSegment[8] = 0; // azzero gli elementi di una combinazione
                

                if (!CheckShortAndLongProgram(9, false, 0))
                {
                    addAsterisco = true;
                } 
                if (!Definizioni.updating)
                    lv.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "", "", "", "" }, -1));
                else
                {
                    // %%% modifica del 18/10/2018 - 2.0
                    if (lv.Items[currentElem].SubItems[7].Text.Contains("*"))
                        addAsterisco = true;
                    lv.Items[currentElem].SubItems[7].Text = "";
                }

                lv.Items[currentElem].SubItems[1].Text = ((DataRow)spirali.Tag)[3].ToString();
                decimal valoreSpirale = decimal.Parse(((DataRow)spirali.Tag)[7].ToString());
                lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", valoreSpirale); 
                lv.Items[currentElem].SubItems[3].Text = "DeathSpiral";
                lv.Items[currentElem].SubItems[4].Text = "SP";
                lv.Items[currentElem].SubItems[5].Text = "0";
                lv.Items[currentElem].SubItems[6].Text = "";
                lv.Items[currentElem].ToolTipText = ((DataRow)spirali.Tag)[1].ToString();
                if (!Definizioni.updating)
                {
                    Definizioni.elemSegment[11] = int.Parse(Definizioni.elemSegment[11].ToString()) + 1;
                    Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) + 1;
                }

                lv.Items[currentElem].ForeColor = rigaSpirals;
                if (!Definizioni.updating) progEl++;
                saltoPrec = "";
                if (!Definizioni.updating) lv.Items[currentElem].Text = progEl + "";
                //if (addAsterisco) lv.Items[currentElem].SubItems[7].Text += " (*)";
                if (addAsterisco) lv.Items[currentElem].SubItems[7].Text = "(*)";

                lv.Items[currentElem].Selected = true;
                if (Utility.InvioDati(lv, log))
                    Utility.ScrivoElementoOnDB(lv, log, false);
                // aggiorno totali
                if (!addAsterisco) Utility.AggiornoTotali(log, lv, deductions, elements, total);

                ResetAfterInsertion();
            }
            catch (Exception ex)
            {
                Cursor.Show();
                Utility.WriteLog("InsertSpirali: " + ex.Message, "ERROR");
            }
        }

        private void InsertSpirals_Click(object sender, EventArgs e)
        {
            try
            {
                numComboSpin = 1; numComboSpinContact = 1; numComboJump = 1;
                firstTrottolaContactCombo = true;
                firstTrottolaSoloCombo = true;
                var element = (Button)sender;
                InsertSpirali(element);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Steps
        /**********************************************************
        *  Steps
        * ********************************************************/
        private void InsertPassi_Click(object sender, EventArgs e)
        {
            try
            {
                numComboSpin = 1; numComboSpinContact = 1; numComboJump = 1;
                firstTrottolaContactCombo = true;
                firstTrottolaSoloCombo = true;
                var element = (Button)sender;
                InsertPassi(element);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void InsertPassi(Button steps)
        {
            try
            {
                Cursor.Hide();
                newCombo.Enabled = false;
                addAsterisco = false;
                //Definizioni.elemSegment[8] = 0; // azzero gli elementi di una combinazione

                if (!CheckShortAndLongProgram(8, false, 0))
                {
                    addAsterisco = true;
                }
                if (!Definizioni.updating)
                    lv.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "", "", "", "" }, -1));
                else
                {
                    // %%% modifica del 18/10/2018 - 2.0
                    if (lv.Items[currentElem].SubItems[7].Text.Contains("*"))
                        addAsterisco = true;
                    lv.Items[currentElem].SubItems[7].Text = "";
                }
                lv.Items[currentElem].SubItems[1].Text = ((DataRow)steps.Tag)[3].ToString();
                decimal valorePassi = decimal.Parse(((DataRow)steps.Tag)[7].ToString());
                lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", valorePassi); 
                lv.Items[currentElem].SubItems[3].Text = "Steps";
                lv.Items[currentElem].SubItems[4].Text = "ST";
                lv.Items[currentElem].SubItems[5].Text = "0";
                lv.Items[currentElem].SubItems[6].Text = "";
                lv.Items[currentElem].ToolTipText = ((DataRow)steps.Tag)[1].ToString();
                if (!Definizioni.updating)
                {
                    Definizioni.elemSegment[12] = int.Parse(Definizioni.elemSegment[12].ToString()) + 1;
                    Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) + 1;
                }
                lv.Items[currentElem].ForeColor = rigaSteps;
                if (!Definizioni.updating) progEl++;
                saltoPrec = "";
                if (!Definizioni.updating)
                    lv.Items[currentElem].Text = progEl + "";
                //if (addAsterisco) lv.Items[currentElem].SubItems[7].Text += " (*)";
                if (addAsterisco) lv.Items[currentElem].SubItems[7].Text = "(*)";

                lv.Items[currentElem].Selected = true;
                if (Utility.InvioDati(lv, log))
                    Utility.ScrivoElementoOnDB(lv, log, false);
                // aggiorno totali
                if (!addAsterisco) Utility.AggiornoTotali(log, lv, deductions, elements, total);

                ResetAfterInsertion();
            }
            catch (Exception ex)
            {
                Cursor.Show();
                Utility.WriteLog("InsertPassi: " + ex.Message, "ERROR");
            }
        }
        #endregion

        #region Sollevamenti
        /**********************************************************
        *  Sollevamenti
        * ********************************************************/
        private void Insert_Lifts(object sender, EventArgs e)
        {
            try
            {
                numComboSpin = 1; numComboSpinContact = 1; numComboJump = 1;
                firstTrottolaContactCombo = true;
                firstTrottolaSoloCombo = true;
                var element = (Button)sender;
                int tipoLift = 1;
                if (cbComboLift1.Checked) tipoLift = 2;
                if (cbComboLift2.Checked) tipoLift = 2;
                if (cbComboLift3.Checked) tipoLift = 2;
                InsertSollevamenti(element, tipoLift);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void InsertSollevamenti(Button lift, int tipoLift)
        {
            try
            {
                Cursor.Hide();
                newCombo.Enabled = false;
                addAsterisco = false;
                //Definizioni.elemSegment[8] = 0; // azzero gli elementi di una combinazione

                int numCombo = 1;
                switch (tipoLift)
                {
                    case 1: // lift singolo
                        if (!CheckShortAndLongProgram(10, false, 0))
                        {
                            addAsterisco = true;
                        }
                        if (!Definizioni.updating)
                        {
                            lv.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "", "", "", "" }, -1));
                        }
                        else
                        {
                            // %%% modifica del 18/10/2018 - 2.0
                            if (lv.Items[currentElem].SubItems[7].Text.Contains("*"))
                                addAsterisco = true;
                            lv.Items[currentElem].SubItems[7].Text = "";
                        }
                        lv.Items[currentElem].SubItems[1].Text = ((DataRow)lift.Tag)[3].ToString();
                        decimal valoreLift = decimal.Parse(((DataRow)lift.Tag)[7].ToString());
                        lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", valoreLift); 
                        lv.Items[currentElem].SubItems[3].Text = "PosLift";
                        lv.Items[currentElem].SubItems[4].Text = "L";
                        lv.Items[currentElem].SubItems[5].Text = "0";
                        lv.Items[currentElem].SubItems[6].Text = "";
                        lv.Items[currentElem].ForeColor = rigaPosLift;
                        lv.Items[currentElem].ToolTipText = ((DataRow)lift.Tag)[1].ToString();
                        // aggiungo un elemento position lift
                        if (!Definizioni.updating)
                        {
                            Definizioni.elemSegment[3] = int.Parse(Definizioni.elemSegment[3].ToString()) + 1;
                            Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) + 1;
                        }
                        break;
                    case 2: // lift combo
                        if (!CheckShortAndLongProgram(11, false, 0))
                        {
                            addAsterisco = true;
                        }
                        if (!Definizioni.updating)
                        {
                            lv.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "", "", "", "" }, -1));
                        }
                        else
                        {
                            // %%% modifica del 18/10/2018 - 2.0
                            if (lv.Items[currentElem].SubItems[7].Text.Contains("*"))
                                addAsterisco = true;
                            lv.Items[currentElem].SubItems[7].Text = "";
                        }
                        decimal valore = 0.00m;
                        string oldElem = lift.Text;
                        if (!oldElem.Equals("0"))
                        {

                            if (lift.Name == "el177" || lift.Name == "el178" || lift.Name == "el179" || lift.Name == "el180" || lift.Name == "el801") lift = el176;
                            if (lift.Name == "el182" || lift.Name == "el183" || lift.Name == "el184" || lift.Name == "el185" || lift.Name == "el802") lift = el181;
                            if (lift.Name == "el187" || lift.Name == "el188" || lift.Name == "el189" || lift.Name == "el190" || lift.Name == "el803") lift = el186;
                            if (lift.Name == "el192" || lift.Name == "el193" || lift.Name == "el194" || lift.Name == "el195" || lift.Name == "el804") lift = el191;
                            if (lift.Name == "el197" || lift.Name == "el198" || lift.Name == "el199" || lift.Name == "el200" || lift.Name == "el805") lift = el196;
                            if (lift.Name == "el202" || lift.Name == "el203" || lift.Name == "el204" || lift.Name == "el205" || lift.Name == "el806") lift = el201;
                            if (lift.Name == "el207" || lift.Name == "el208" || lift.Name == "el209" || lift.Name == "el210" || lift.Name == "el807") lift = el206;
                            if (lift.Name == "el212" || lift.Name == "el213" || lift.Name == "el214" || lift.Name == "el215" || lift.Name == "el808") lift = el211;
                            if (lift.Name == "el217" || lift.Name == "el218" || lift.Name == "el219" || lift.Name == "el220" || lift.Name == "el809") lift = el216;
                            if (lift.Name == "el222" || lift.Name == "el223" || lift.Name == "el224" || lift.Name == "el225" || lift.Name == "el810") lift = el221;

                            // modifica del 27/02/2018
                            if (lift.Name == "el149" || lift.Name == "el150" || lift.Name == "el151") lift = el172; // Axel
                            if (lift.Name == "el156" || lift.Name == "el157" || lift.Name == "el158") lift = el154; // Axel around the back
                            if (lift.Name == "el160" || lift.Name == "el161" || lift.Name == "el162") lift = el173; // Flip
                            if (lift.Name == "el164" || lift.Name == "el165" || lift.Name == "el166") lift = el238; // Low Kennedy
                            if (lift.Name == "el145" || lift.Name == "el146" || lift.Name == "el147") lift = el239; // Low Milatano

                            valore = CalcolaValoreLift(decimal.Parse(((DataRow)lift.Tag)[7].ToString()), int.Parse(oldElem));
                        }
                        else valore = decimal.Parse(((DataRow)lift.Tag)[7].ToString());

                        lv.Items[currentElem].SubItems[1].Text = ((DataRow)lift.Tag)[3].ToString().Replace("1","") + oldElem;
                        lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", valore); 
                        lv.Items[currentElem].SubItems[3].Text = "CombLift";
                        lv.Items[currentElem].SubItems[4].Text = "L";
                        lv.Items[currentElem].SubItems[5].Text = "1"; //%%% modifica del 31/03/2018
                        lv.Items[currentElem].SubItems[6].Text = "";
                        
                        lv.Items[currentElem].ForeColor = rigaComboLift;
                        lv.Items[currentElem].ToolTipText = ((DataRow)lift.Tag)[1].ToString();
                        if (!Definizioni.updating)
                        {
                            Definizioni.elemSegment[4] = int.Parse(Definizioni.elemSegment[4].ToString()) + 1;
                            Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) + 1;
                        }
                        break;
                    default:
                        break;
                }

                if (addAsterisco) lv.Items[currentElem].SubItems[7].Text = "(*)";

                if (lift == el167)
                {
                    lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", 0); //"0,00";
                    lv.Items[currentElem].SubItems[6].Text = "";
                }
                if (!Definizioni.updating) progEl++;
                if (numCombo == 1 && !Definizioni.updating)
                    lv.Items[currentElem].Text = progEl + "";

                lv.Items[currentElem].Selected = true;
                if (Utility.InvioDati(lv, log))
                    Utility.ScrivoElementoOnDB(lv, log, false);

                // Aggiorno i totali
                if (!addAsterisco) Utility.AggiornoTotali(log, lv, deductions, elements, total);

                ResetAfterInsertion();
            }
            catch (Exception ex)
            {
                Cursor.Show();
                Utility.WriteLog("InsertSollevamenti: " + ex.Message, "ERROR");
            }
        }

        private void cbComboLift1_CheckedChanged(object sender, EventArgs e)
        {
            if (cbComboLift1.Checked)
            {
                cbComboLift2.Checked = false;
                cbComboLift3.Checked = false;
            } 
        }

        private void cbComboLift2_CheckedChanged(object sender, EventArgs e)
        {
            if (cbComboLift2.Checked)
            {
                cbComboLift1.Checked = false;
                cbComboLift3.Checked = false;
            }
            else
            {

            }
        }

        private void cbComboLift3_CheckedChanged(object sender, EventArgs e)
        {
            if (cbComboLift3.Checked)
            {
                cbComboLift2.Checked = false;
                cbComboLift1.Checked = false;
            }
            else
            {

            }
        }

        public decimal CalcolaValoreLift(decimal valoreBase, int livello)
        {
            decimal valoreCombo = valoreBase;
            decimal percentuale = Definizioni.paramSegment[18];

            for (int i = 0; i < livello; i++)
            {
                valoreCombo = valoreCombo + Math.Round(((valoreBase * percentuale) / 100), 2);
            }

            return valoreCombo;
        }

        /**********************************************************
        *  Fine sollevamenti
        * ********************************************************/
        #endregion
        
        #endregion

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                double durata = Convert.ToDouble(Definizioni.paramSegment[0]);

                time = time.Add(TimeSpan.FromMilliseconds(1000));
                ltimer.Text = string.Format("{0:00}:{1:00}", time.Minutes,
                    time.Seconds);

                if (time.TotalSeconds < (durata - tolleranza))
                {
                    // lascio lo stesso colore
                }
                else if (time.TotalSeconds >= (durata - tolleranza) &&
                         time.TotalSeconds <= (durata + tolleranza))
                {
                    ltimer.ForeColor = Color.Lime;
                }
                else if (time.TotalSeconds > (durata + tolleranza))// oltre la tolleranza
                {
                    ltimer.ForeColor = Color.Red;
                }

                try
                {
                    // 2.0.1 Dopo 5 secondi invio BLANK al Video
                    if (time.TotalSeconds == 5)
                    {
                        Utility.Video_BlankCommand();
                        ((Label)ut.ds.Controls.Find("order", true)[0]).Text = "";
                    }
                }
                catch (Exception) { }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("Timer1_Tick: " + ex.Message, "ERROR");
            }
        }

        #region DEDUCTIONS
        /**********************************************************
         *  DEDUCTIONS
         * ********************************************************/
        private void ManageDeductions(NumericUpDown control)
        {
            try
            {
                deductions.Text = "-" + (ded1.Value + ded2.Value + ded3.Value + ded4.Value + ded5.Value).ToString("F");
                Definizioni.deductionsDec = decimal.Parse(deductions.Text);
                total.Text = String.Format("{0:0.00}", Definizioni.elementsDec + Definizioni.deductionsDec);

                // 2.0.0.10 - gestisco le tipologie di deductions per i report finali
                string numeroDed = control.Name.Substring(control.Name.Length - 1);
                Definizioni.arDeductions[int.Parse(numeroDed) - 1] = decimal.Parse(control.Value.ToString("F"));
            }
            catch (Exception ex)
            {
                Utility.WriteLog("ManageDeductions: " + ex.Message, "ERROR");
            }
        }

        private void ded1_ValueChanged(object sender, EventArgs e)
        {
            ManageDeductions(ded1);
        }
        private void ded4_ValueChanged(object sender, EventArgs e)
        {
            ManageDeductions(ded4);
        }
        private void ded2_ValueChanged(object sender, EventArgs e)
        {
            ManageDeductions(ded2);
        }
        private void ded5_ValueChanged(object sender, EventArgs e)
        {
            ManageDeductions(ded5);
        }
        private void ded3_ValueChanged(object sender, EventArgs e)
        {
            ManageDeductions(ded3);
        }

        private void next_Click_1(object sender, EventArgs e)
        {
            NewSegment();
        }
        #endregion
        
        #region CONTROLLI
        public void DisableGroupBox()
        {
            tabJ.Enabled = false; tabLifts.Enabled = false;
            tabSpins.Enabled = false; //tabVarie.Enabled = false;
            ((Control)this.tabJumps).Enabled = false;
            ((Control)this.tabLanciati).Enabled = false;
            ((Control)this.tabTwist).Enabled = false;
            ((Control)this.tabLift1).Enabled = false;
            ((Control)this.tabLift2).Enabled = false;
            ((Control)this.tabLift3).Enabled = false;
            ((Control)this.tabSpiral).Enabled = false;
            ((Control)this.tabSteps).Enabled = false;
            ((Control)this.tabDeductions).Enabled = false;
            ((Control)this.tabSpin).Enabled = false;
            ((Control)this.tabCspin1).Enabled = false;
        }

        public void EnableGroupBox()
        {
            tabJ.Enabled = true; tabLifts.Enabled = true;
            tabSpins.Enabled = true; tabVarie.Enabled = true;
            ((Control)this.tabJumps).Enabled = true;
            ((Control)this.tabLanciati).Enabled = true;
            ((Control)this.tabTwist).Enabled = true;
            ((Control)this.tabLift1).Enabled = true;
            ((Control)this.tabLift2).Enabled = true;
            ((Control)this.tabLift3).Enabled = true;
            ((Control)this.tabSpiral).Enabled = true;
            ((Control)this.tabSteps).Enabled = true;
            ((Control)this.tabDeductions).Enabled = true;
            ((Control)this.tabSpin).Enabled = true;
            ((Control)this.tabCspin1).Enabled = true;
        }

        private void lv_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = lv.Columns[e.ColumnIndex].Width;
        }

        // Blocco la larghezza delle colonne
        private void lv_ColumnWidthChanging_1(object sender, ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = lv.Columns[e.ColumnIndex].Width;
        }

        private void lv_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
                e.Item.Selected = false;
        }

        private void Tooltips(Button j)
        {
            tt.ToolTipTitle = j.Name;
            if (j.BackColor == Color.Yellow)
                tt.SetToolTip(j, Definizioni.resources["waiting"].ToString());
            else if (j.BackColor == Color.Transparent && j.ForeColor == Color.Lime)
            {
                tt.SetToolTip(j, Definizioni.resources["connected"].ToString() + "\r\n");
                tt.ForeColor = Color.Black;
                tt.ToolTipIcon = ToolTipIcon.Info;
            }
            else if (j.BackColor == Color.Transparent && j.ForeColor == Color.Red)
            {
                tt.SetToolTip(j, Definizioni.resources["notconnected"].ToString() + "\r\n");
                tt.ForeColor = Color.Black;
                tt.ToolTipIcon = ToolTipIcon.Warning;
            }
            if (j.Font.Underline)
            {
                tt.SetToolTip(j, "Ping timeout");
                tt.ForeColor = Color.Black;
                tt.ToolTipIcon = ToolTipIcon.Warning;
            }
        }
        
        private void j1_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j1);
        }

        private void j2_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j2);
        }

        private void j3_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j3);
        }

        private void j4_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j4);
        }

        private void j5_Click(object sender, EventArgs e)
        {
            Tooltips(j5);
            //ut.PingGiudici();
            //ut.HandleClientComm(Definizioni.clientsJudge[4]);
        }

        private void j6_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j6);
        }

        private void j7_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j7);
        }

        private void j8_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j8);
        }

        private void j9_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j9);
        }

        private void lv_MouseClick(object sender, MouseEventArgs e)
        {
            if (lv.SelectedItems.Count != 0)
            {
                review.Enabled = true;
                asterisco.Enabled = true;
            }
            else
            {
                review.Enabled = false;
                asterisco.Enabled = false;
            }
        }

        private void lv_MouseDown(object sender, MouseEventArgs e)
        {
            ListViewItem selection = lv.GetItemAt(e.X, e.Y);

     	    if (selection != null)
		    {
                //edit.Enabled = true;
            }
            else
            {
                //edit.Enabled = false;
            }
        }

        private void button1_Click_3(object sender, EventArgs e)
        {
            Info inf = new Info();
            inf.ShowDialog();
        }

        private void j1_Click(object sender, EventArgs e)
        {
            Utility.SendToSingleJudge("<HURRYUP/>", int.Parse(((Button)sender).Tag.ToString()));
        }

        private void j5_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j5);
        }

        private void asterisco_Click(object sender, EventArgs e)
        {
            try
            {
                if (lv.SelectedItems[0] == null) return;
                int indexSelectedItem = lv.SelectedItems[0].Index;

                if (lv.Items[indexSelectedItem].SubItems[7].Text.Contains("(*)")) // già asteriscato
                {
                    lv.Items[indexSelectedItem].SubItems[7].Text =
                        lv.Items[indexSelectedItem].SubItems[7].Text.Replace("(*)", ""); // elimino asterisco
                    Utility.UpdateElementConAsterisco(indexSelectedItem + 1, false);
                }
                else
                {
                    lv.Items[indexSelectedItem].SubItems[7].Text = "(*)";
                    Utility.UpdateElementConAsterisco(indexSelectedItem + 1, true);
                }
                asterisco.Enabled = false;

                Utility.AggiornoTotali(log, lv, deductions, elements, total);
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Add *: " + ex.Message, "ERROR");
            }
        }

        private void panelContactCombo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void tabJ_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void CoppieForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // *** Modifica del 05/12/2018 - 2.0.0.9 *** //
            if (!flagClose)
                e.Cancel = true;
        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        // preview
        private void prev_Click(object sender, EventArgs e)
        {
            if (Definizioni.currentPart == 1 || Definizioni.skipped) return;
            DialogResult result = MessageBox.Show("Display the previous skater scores?", "Warning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                Definizioni.currentPart = Definizioni.currentPart - 1;
                ut.DisplayScore(true);
                Definizioni.currentPart = Definizioni.currentPart + 1;
            }
        }

        // REVIEW
        private void edit_Click(object sender, EventArgs e)
        {
            try
            {
                if (lv.SelectedItems[0] == null) return;
                int indexSelectedItem = lv.SelectedItems[0].Index;

                if (lv.Items[indexSelectedItem].ImageIndex == 2) // già in Review
                {
                    lv.Items[indexSelectedItem].ImageIndex = 0;
                    lv.Items[indexSelectedItem].ToolTipText =
                        lv.Items[indexSelectedItem].ToolTipText.Split('-')[0];
                }
                else
                {
                    lv.Items[indexSelectedItem].ImageIndex = 2;
                    lv.Items[indexSelectedItem].ToolTipText =
                        lv.Items[indexSelectedItem].ToolTipText + " - REVIEW";
                }
                review.Enabled = false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Review: " + ex.Message, "ERROR");
            }
        }

        private void cbCombo_CheckedChanged(object sender, EventArgs e)
        {
            if (cbCombo.Checked) cbCombo.FlatAppearance.MouseOverBackColor = Color.Lime;
            else cbCombo.FlatAppearance.MouseOverBackColor = Color.Gainsboro;
        }

        private void cbVerify_CheckedChanged(object sender, EventArgs e)
        {
            if (cbVerify.Checked) cbVerify.FlatAppearance.MouseOverBackColor = Color.Lime;
            else cbVerify.FlatAppearance.MouseOverBackColor = Color.Gainsboro;
        }

        private void newComboSpin_Click(object sender, EventArgs e)
        {
            numComboSpin = 1;
            numComboSpinContact = 1;
            firstTrottolaSoloCombo = true;
            firstTrottolaContactCombo = true;
        }

        private void newComboSpin2_Click(object sender, EventArgs e)
        {
            numComboSpin = 1;
            numComboSpinContact = 1;
            firstTrottolaSoloCombo = true;
            firstTrottolaContactCombo = true;
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SQLite;

namespace RollartSystemTech
{
    public partial class StartSegment : Form
    {
    
        public StartSegment()
        {
            InitializeComponent();
        }

        private void SkatingOrder_Load(object sender, EventArgs e)
        {
            try
            {
                SQLiteDataReader dr2 = null, dr = null;
                SQLiteCommand cmd = new SQLiteCommand(
                            "select S.Name, Completed, LastPartecipant, Partecipants, NumJudges, A.Name, C.Name, P.Name" +
                            " from GaraParams as A, Segments as S, Category as C, Specialita as P " +
                            " where ID_GaraParams = " + Definizioni.idGaraParams +
                            " AND A.ID_SEGMENT = S.ID_SEGMENTS" +
                            " AND A.ID_CATEGORY = C.ID_CATEGORY" +
                            " AND A.ID_SPECIALITA = P.ID_SPECIALITA" +
                            " AND A.ID_SEGMENT = " + Definizioni.idSegment, Definizioni.conn);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    

                    string query = "select NumStartingList, B.Name, A.ID_Atleta, Country" +
                            " from Participants as A, Athletes as B" +
                            " where ID_GaraParams = " + Definizioni.idGaraParams +
                            " AND A.ID_SEGMENT = " + Definizioni.idSegment +
                            " AND A.ID_Atleta = B.ID_Atleta";

                    if (dr[1].ToString().Equals("Y"))
                    {
                        query = "select NumStartingList, B.Name, A.ID_Atleta, Country" +
                                " from Participants as A, Athletes as B, GaraFinal as G" +
                                " where A.ID_GaraParams = " + Definizioni.idGaraParams +
                                " AND A.ID_SEGMENT = G.ID_SEGMENT" +
                                " AND A.ID_GaraParams = G.ID_GaraParams" +
                                " AND A.ID_SEGMENT = " + Definizioni.idSegment +
                                " AND B.ID_Atleta = G.ID_Atleta" +
                                " AND A.ID_Atleta = B.ID_Atleta ORDER BY Position";
                        lbCompetitors.HotTracking = true;

                    }
                    else // non iniziato
                    {
                        lbCompetitors.Columns[0].Text = "Order";
                        lbCompetitors.HotTracking = false;
                    }

                    cmd = new SQLiteCommand(query, Definizioni.conn);
                    dr = cmd.ExecuteReader();
                    int numAtleti = 0;
                    while (dr.Read())
                    {
                        lbCompetitors.Items.Add(new ListViewItem(new string[] { "", "", "", "", "" }, -1));
                        lbCompetitors.Items[numAtleti].SubItems[0].Text = dr[0].ToString(); // numStartingList
                        lbCompetitors.Items[numAtleti].SubItems[1].Text = dr[1].ToString().ToUpper(); // name
                        lbCompetitors.Items[numAtleti].SubItems[2].Text = dr[2].ToString(); // ID_Atleta
                        lbCompetitors.Items[numAtleti].SubItems[3].Text = dr[3].ToString().ToUpper(); // country

                        /*************************************************************************
                         * Modifica del 18/10/2018
                         * recupero anche le info sul segmento dell'atleta
                         * ***********************************************************************/
                        cmd = new SQLiteCommand(
                                        "select Total, Position from Participants as A, GaraFinal as B" +
                                        " where A.ID_GaraParams = " + Definizioni.idGaraParams +
                                        "   AND A.ID_GaraParams = B.ID_GaraParams" +
                                        "   AND A.ID_SEGMENT = " + Definizioni.idSegment +
                                        "   AND A.ID_Atleta = " + dr[2].ToString() +
                                        "   AND A.ID_Segment = B.ID_Segment" +
                                        "   AND A.ID_Atleta = B.ID_Atleta", Definizioni.conn);
                        dr2 = cmd.ExecuteReader();
                        while (dr2.Read())
                        {
                            lbCompetitors.Items[numAtleti].SubItems[4].Text = String.Format("{0:0.00}", decimal.Parse(dr2[0].ToString())); // Total
                            lbCompetitors.Items[numAtleti].ForeColor = Color.Blue;
                            lbCompetitors.Items[numAtleti].ImageIndex = 0;
                            lbCompetitors.Items[numAtleti].BackColor = Color.LightGray;
                        }
                        //if (dr2.HasRows) // se segmento completato
                        {
                         //   lbCompetitors.Items[numAtleti].ImageIndex = 0;
                         //   lbCompetitors.Items[numAtleti].BackColor = Color.LightGray;
                        }
                        /*************************************************************************/
                        
                        numAtleti++;
                    }
                }

                // seleziono di default il primo atleta che non ha eseguito
                int i = 0;
                foreach (ListViewItem lvi in lbCompetitors.Items)
                {
                    if (lvi.BackColor == Color.White)
                    {
                        
                        lbCompetitors.Items[i].Selected = true;
                        lbCompetitors.Items[i].Focused = true;
                        lbCompetitors.Items[i].EnsureVisible();
                        break;
                    }
                    i++;
                }
                lbCompetitors.Select();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            Definizioni.dispose = true;
            Dispose();
        }
        
        private void lbCompetitors_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (lbCompetitors.SelectedItems.Count > 0)
                {
                    int indice = lbCompetitors.SelectedItems[0].Index;
                    skater.Text = (indice + 1) + "";
                    if (lbCompetitors.SelectedItems[0].BackColor == Color.White)
                    {
                        //lbCompetitors.SelectedItems[0].Selected = false;
                        lbCompetitors.SelectedItems[0].Focused = true;
                        lbCompetitors.SelectedItems[0].EnsureVisible();
                    } else
                    {
                        lbCompetitors.Items[indice + 1].Selected = true;
                        lbCompetitors.Items[indice + 1].Focused = true;
                        lbCompetitors.Items[indice + 1].EnsureVisible();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("lbCompetitors_SelectedIndexChanged: Error..... " + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void lbCompetitors_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (lbCompetitors.SelectedItems.Count > 0)
                {
                    if (lbCompetitors.SelectedItems[0].SubItems[4].Text.Equals(""))
                    {
                        Definizioni.lastPart =
                            int.Parse(lbCompetitors.SelectedItems[0].SubItems[0].Text) - 1;
                        Definizioni.currentPart = Definizioni.lastPart + 1;
                        Definizioni.dispose = false;
                        //Definizioni.currentPart = ((Skater)lbCompetitors.SelectedItems[0].Tag);
                        Dispose();
                    }
                }
                else
                {
                    Definizioni.lastPart = 0;
                    Definizioni.currentPart = 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("lbCompetitors_MouseDoubleClick: Error..... " + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void start_Click(object sender, EventArgs e)
        {
            lbCompetitors_MouseDoubleClick(sender, (MouseEventArgs)e);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Threading;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Collections;
using System.Xml.Linq;
using System.Xml;
using RollartSystemTech;
using RollartSystemTech.Segments;
using System.Net.NetworkInformation;
using Newtonsoft.Json;

namespace RollartSystemTech
{
    public partial class SingoloForm : Form
    {
        //int currentElem = 0; // da 0 a elemShort -1
        /********************************************************************************
         * [0] = numElems     [1] = numJumps     [2] = numSpins      [3] = numSteps
         * [4] = Durata       [5] = numSoloJumps [6] = numComboJumps [7] = maxElemsCombo 
         * [8] = minElemsCombo[9] = numSoloSpins [10] = numComboSpins[11] = factor
         * [12] = id_category [13] = ComboJumpDD [14] = ComboJumpTD  [15] = ComboJumpTT
         * [16] = maxElemsComboSpin
         * ******************************************************************************/

        /******************************************************
         * 
         *  Modifica per la versione 1.0.4 di tutti i 
         *  InvioComandoAlGiudice con SendBroadcast
         *  
         * ***************************************************/
        TimeSpan time = new TimeSpan();

        // Colori
        Color rigaSoloJump = Color.Aqua;
        Color rigaCombo = Color.GreenYellow;
        Color rigaComboX = Color.OrangeRed;
        Color rigaSpin = Color.Gold;
        Color rigaComboSpin = Color.LightSalmon;
        Color rigaSteps = Color.White;
        Color rigaDed = Color.Red;
        Color oldForeColor;
        ToolTip tt = null;

        Utility ut = null;
        string saltoCorr = "", saltoPrec = "";
        int currentElem = 0, progEl = 0, numComboSpins = 0, tolleranza = 5;
        decimal secMetaDisco = 0;
        bool addAsterisco = false, exitCheck = false, flagClose = false;
        Thread thrCheckGiudici = null;

        public SingoloForm()
        {
            InitializeComponent();
            System.Windows.Forms.Form.CheckForIllegalCrossThreadCalls = false;
        }

        private void SPSingoloForm_Load(object sender, EventArgs e)
        {
            try
            {
                //2.0.1 - 05/02/2019
                Utility.skResultComb = new List<Video.Result_Combined>();
                Utility.skResultSingle = new List<Video.Result_Single>();

                Definizioni.currentForm = this;
                ut = new Utility(null);
                this.Text = Definizioni.resources["panel1"].ToString() +
                    Definizioni.idGaraParams + "/" + Definizioni.idSegment + " - " +
                    Definizioni.dettaglioGara.Split('@')[3] + " " + Definizioni.dettaglioGara.Split('@')[4];
                ut.LoadValuesFromDBForAll(this.Controls, log, tt2, "SegmentParams");
                NewSegment();
                tt = new ToolTip();
                secMetaDisco = Definizioni.paramSegment[4] / 2;

                // nuovo thread per la verifica dei giudici
                thrCheckGiudici = new Thread(CheckGiudici);
                thrCheckGiudici.Start();

                this.MinimumSize = new System.Drawing.Size(1225, 853);

                if (Definizioni.idSegment == 2) tolleranza = 10;

            }
            catch (Exception ex)
            {
                Utility.WriteLog("Load: " + ex.Message, "ERROR");
            }
        }

        // Controllo ogni secondo se il giudice è connesso o meno
        private void CheckGiudici()
        {
            //bool judgeDisconnected = false;
            while (true)
            {
                if (exitCheck)
                    return;

                //judgeDisconnected = false;
                for (int i = 0; i < Definizioni.numJudges; i++)
                {
                    if (this == null)
                        return;

                    try
                    {
                        
                        if (Main.jclient.list[i].state.Equals("1"))
                        {
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).Enabled = true;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).ForeColor = Color.Lime;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).FlatAppearance.BorderColor = Color.Lime;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).BackColor = Color.Transparent;
                        }
                        else // interfaccia giudice non avviata o chiusa
                        {
                            //judgeDisconnected = true;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).Enabled = true;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).ForeColor = Color.Red;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).FlatAppearance.BorderColor = Color.Red;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).BackColor = Color.Transparent;
                        }

                        // ping
                        if (!Utility.PingGiudice(i))
                        {
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).Font =
                                 new Font(((Button)this.Controls.Find("j" + (i + 1), true)[0]).Font, FontStyle.Regular | FontStyle.Underline);
                            
                        } else
                        {
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).Font =
                                 new Font(((Button)this.Controls.Find("j" + (i + 1), true)[0]).Font, FontStyle.Regular);
                        }

                    }
                    catch (Exception)
                    {
                    }

                }
                if (RollartSystemTech.Properties.Settings.Default.PingTime > 0)
                    Thread.Sleep(RollartSystemTech.Properties.Settings.Default.PingTime);
            }
        }

        #region GESTIONE SEGMENTO
        // INIZIO SEGMENTO
        private void startstop_Click(object sender, EventArgs e)
        {
            try
            {
                if (startstop.Text == "START")
                {
                    gbElements.Enabled = true;
                    confirm.Enabled = false;
                    average.Enabled = false;
                    skip.Enabled = false;
                    prev.Enabled = false;
                    reset.Enabled = false;
                    EnableGroupBox();
                    startstop.Text = "STOP";
                    timer1.Start();
                    ResetPercentualiTrottole();
                }
                else
                {
                    DisableGroupBox();
                    confirm.Enabled = true;
                    average.Enabled = true;
                    skip.Enabled = true;
                    prev.Enabled = true;
                    reset.Enabled = true;
                    startstop.Text = "START";
                    timer1.Stop();
                    // 2.0.1 - invio al Video Display il comando BLANK
                    Utility.Video_BlankCommand();
                }
                
            }
            catch (Exception ex)
            {
                Utility.WriteLog("StartStop: " + ex.Message, "ERROR");
            }
        }

        // NUOVO SEGMENTO
        public void NewSegment()
        {
            try
            {
                progEl = 0;
                lv.Visible = true;
                // fine gara
                if (EndCompetition()) return;

                Definizioni.elemSegment = new decimal[30];
                ded1.Value = 0; ded2.Value = 0; ded3.Value = 0; ded4.Value = 0; ded5.Value = 0;
                ded6.Value = 0; // *** Modifica del 17/08/2018 *** //
                total.Text = String.Format("{0:0.00}", 0); //"0,00";
                deductions.Text = String.Format("{0:0.00}", 0); //"0,00";
                elements.Text = String.Format("{0:0.00}", 0); //"0,00";
                Definizioni.currentPart = Definizioni.lastPart + 1;
                Definizioni.numComboElement = 0;
                Definizioni.totale = 0; Definizioni.deductionsDec = 0; Definizioni.elementsDec = 0;
                Definizioni.compAlreadyInserted = false;
                currentElem = 0;
                saltoPrec = "";
                saltoCorr = "";
                error.Text = "";
                ltimer.Text = "00:00";
                time = TimeSpan.Zero;
                ltimer.ForeColor = Color.Magenta;
                rectangleShape7.BorderColor = Color.Magenta;

                Utility.SendBroadcast("<CONNECT/>");
                Thread.Sleep(500);
                //Utility.CheckGiudici(this.Controls);
                Utility.SendBroadcast(
                    "<START Gara ='" + Definizioni.idGaraParams + "'" +
                          " Segment='" + Definizioni.idSegment + "'" +
                          " Partecipante='" + Definizioni.currentPart + "'" +
                          // **** 1.0.4 Modifica del 27/06/2018 **** //
                          // Invio al giudice anche la categoria e la disciplina
                          " Category='" + Definizioni.idCategoria + "'" +
                          " Discipline='" + Definizioni.idSpecialita + "'" +
                    "/>");

                // 2.0.1 - invio al Video Display il comando SKATER
                Utility.LoadSkater();
                Utility.Video_SkaterCommand();

                ut.LoadParticipant(log, buttonNext, labelPartial);
                lv.Items.Clear();
                DisableGroupBox();
                startstop.Text = "START";
                startstop.Enabled = true;
                confirm.Enabled = false;
                average.Enabled = false;
                numComboSpins = 0;

                if (Definizioni.currentPart == 1) prev.Enabled = false;

                Utility.WriteLog("___________________________________________________________________", "OK");
                Utility.WriteLog(" " + buttonNext.Text.ToUpper() , "OK");
                Utility.WriteLog("___________________________________________________________________", "OK");

                
            }
            catch (Exception ex)
            {
                Utility.WriteLog("NewSegment: " + ex.Message, "ERROR");
            }
        }

        // FINE GARA
        public bool EndCompetition()
        {
            try
            {
                // fine gara
                if (Definizioni.currentPart == Definizioni.numPart)
                {
                    Utility.SendBroadcast("<DISCONNECT/>");

                    //aggiorno a Y il campo COMPLETED di GaraParams
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "UPDATE GaraParams SET Completed = 'Y' " +
                            " WHERE ID_GaraParams = " + Definizioni.idGaraParams
                            + " AND ID_Segment = " + Definizioni.idSegment;
                        command.ExecuteNonQuery();
                    }

                    Definizioni.currentPart = 0;

                    // 2.0.1 Gestione Video
                    Utility.Video_BlankCommand();

                    Dispose();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("EndCompetition: " + ex.Message, "ERROR");
                return false;
            }
        }

        // CARICA IMPOSTAZIONI
        public void LoadDescriptions()
        {
            try
            {
                XmlDocument xml = new XmlDocument();
                xml.Load("settings.xml");
                XmlNode root = xml.DocumentElement;

                // steps
                int numSteps = xml.SelectNodes("/Settings/Steps/*").Count;
                XmlNodeList nodiSteps = xml.SelectNodes("/Settings/Steps/*");
                for (int i = 0; i < numSteps; i++)
                {

                    tt2.SetToolTip(((Button)this.Controls.Find("s" + (i + 1), true)[0])
                        , nodiSteps.Item(i).SelectNodes("@Desc").Item(0).Value);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("LoadDescriptions: " + ex.Message, "ERROR");
            }
        }

        #endregion

        #region STARTSTOP, AVANTI, CONFERMA, Average, BACK, AGGIORNA, ESCI

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                // reset elementi
                Definizioni.elemSegment = new decimal[30];
                foreach (ListViewItem item in lv.Items)
                {
                    item.SubItems[0].Text = "";
                    item.SubItems[1].Text = "";
                    item.SubItems[2].Text = "";
                    item.SubItems[3].Text = "";

                }
                ded1.Value = 0; ded2.Value = 0; ded3.Value = 0; ded4.Value = 0; ded5.Value = 0; ded6.Value = 0;
                elements.Text = String.Format("{0:0.00}", 0); //"0,00";
                deductions.Text = String.Format("{0:0.00}", 0); //"0,00";
                total.Text = String.Format("{0:0.00}", 0); //"0,00";

                Definizioni.totale = 0;
                currentElem = 0;
                lv.Items[currentElem].Selected = true;
                saltoPrec = "";
                saltoCorr = "";
            }
            catch (Exception ex)
            {
                Utility.WriteLog("button1: " + ex.Message, "ERROR");
            }
        }

        //CONFIRM ENTRIES
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                // Verifico gli elementi 
                if (!CheckBeforeConfirm()) return;

                // salvo sul log ciò che mi aspetto dai giudici
                SaveOnLog();

                DisableGroupBox();
                confirm.Enabled = false;
                back.Enabled = false;
                review.Enabled = false;
                startstop.Enabled = false;
                asterisco.Enabled = false;
                buttonT.Enabled = false;

                // Confermo il segmento
                exitCheck = true;
                Thread.Sleep(500);

                Utility.SendBroadcast("<DEDUCTIONS ded1='" + ded1.Value + "' ded2='" + ded2.Value +
                "' ded3='" + ded3.Value + "' ded4='" + ded4.Value + "' ded5='" + ded5.Value + "' />");

                if (ut.ConfirmSegment())
                {
                    NewSegment();
                    error.Visible = false;
                } else
                {
                    startstop.Enabled = true;
                    confirm.Enabled = true;
                    back.Enabled = true;
                    review.Enabled = true;
                    asterisco.Enabled = false;
                    buttonT.Enabled = false;
                    Definizioni.stopFromWaiting = false;
                    Definizioni.exitFromWaiting = false;
                }
                exitCheck = false;
                thrCheckGiudici = new Thread(CheckGiudici);
                thrCheckGiudici.Start();

                // 2.0.1 - invio al Video Display il comando BLANK
                Utility.Video_BlankCommand();
            }
            catch (Exception ex)
            {
                Utility.WriteLog("ConfirmEntries: " + ex.StackTrace, "ERROR");
                error.Visible = false;
            }
        }

        private void clear_Click(object sender, EventArgs e)
        {
            back.Enabled = false;
            ClearElement();
            back.Enabled = true;
        }

        // RESTART
        private void next_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = MessageBox.Show(
                Definizioni.resources["panel70"].ToString(), "Warning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    ut.RollbackCompetitor(log);
                    NewSegment();
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("RESET: " + ex.Message, "ERROR");
            }
        }

        public void SaveOnLog()
        {
            try
            {
                string elementsXML = "[ELEMENTS ";
                for (int i = 0; i < lv.Items.Count; i++)
                {
                    elementsXML += lv.Items[i].SubItems[1].Text + ";";
                }
                elementsXML += "][DEDUCTIONS: " + deductions.Text + "]";
                Utility.WriteLog(elementsXML, "");
            }
            catch (Exception ex)
            {
                Utility.WriteLog("SaveOnLog: " + ex.Message, "ERROR");
            }
        }

        // ASK COMPONENTS For Average
        private void average_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(
            Definizioni.resources["panel90"].ToString(), "Warning",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                DisableGroupBox();
                exitCheck = true;
                Thread.Sleep(500);
                ut.AskAverage();
                //Definizioni.connectedJudges = new Boolean[Definizioni.numJudges];
            }
            exitCheck = false;
            thrCheckGiudici = new Thread(CheckGiudici);
            thrCheckGiudici.Start();

        }

        // AGGIORNA ENTRY
        private void lv_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                foreach (ListViewItem lvi in lv.Items)
                {
                    lvi.BackColor = Color.Black;
                }
                cancel.Enabled = true;
                if (lv.SelectedItems[0] == null) return;
                Definizioni.updating = true; //modalità updating

                ListViewItem selectedItem = lv.SelectedItems[0];
                string numEl = selectedItem.SubItems[0].Text;
                string codeEl = selectedItem.SubItems[1].Text;
                string valEl = selectedItem.SubItems[2].Text;
                string typeEl = selectedItem.SubItems[3].Text;
                string cat = selectedItem.SubItems[4].Text;
                string numcombo = selectedItem.SubItems[5].Text;
                string du = selectedItem.SubItems[6].Text;
                int indexElement = selectedItem.Index;
                selectedItem.BackColor = Color.Yellow;
                oldForeColor = selectedItem.ForeColor;
                selectedItem.ForeColor = Color.Red;
                //oldForeColor = selectedItem.ForeColor;

                DisableGroupBox();

                // *** Modifica del 06/07/2018 - 1.0.4 *** //
                deductions.Enabled = false;
                flowLayoutPanel1.Enabled = false;
                review.Enabled = false;
                asterisco.Enabled = false;
                buttonT.Enabled = false;
                //startstop.Enabled = false;
                //confirm.Enabled = false;
                //deductions.Enabled = false;
                //average.Enabled = false;
                //skip.Enabled = false;
                back.Enabled = false;
                // *** //

                selectedItem.Selected = false;
                currentElem = indexElement;

                if (typeEl.StartsWith("Jump")) gbJumps.Enabled = true;
                if (typeEl.StartsWith("CombJump")) gbComboJumps.Enabled = true;
                if (typeEl.StartsWith("Spin"))
                {
                    gbSpins.Enabled = true;
                    checkCombo2.Checked = false;
                    checkCombo2.Enabled = false;
                }
                if (typeEl.StartsWith("CombSpin"))
                {
                    gbSpins.Enabled = true;
                    checkCombo2.Checked = true;
                    checkCombo2.Enabled = false;
                }
                if (typeEl.StartsWith("Step")) gbSteps.Enabled = true;
            }

            catch (Exception ex)
            {
                Utility.WriteLog("MouseDoubleClick: " + ex.Message, "ERROR");
            }
        }

        // ANNULLA AGGIORNAMENTO
        private void cancel_Click(object sender, EventArgs e)
        {
            ResetAfterInsertion();
        }

        // QUIT
        private void button2_Click_2(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(
                Definizioni.resources["panel8"].ToString(), "Warning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                Utility.SendBroadcast("<DISCONNECT/>");
                // faccio rollback ed elimino i dati dell'ultimo competitor
                ut.RollbackCompetitor(log);
                exitCheck = true;
                flagClose = true;
                Dispose();
            }
        }

        // SKIP
        private void button1_Click_1(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(Definizioni.resources["panel9"].ToString(), "Warning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                ut.UpdateLastPartecipant();
                NewSegment();
            }
        }
        #endregion

        #region FUNZIONI DI INTERFACCIA

        public bool CheckShortAndLongProgram(int flag, bool saltoSemplice, int numCombo)
        {
            error.Text = "";
            error.Visible = false;
            if (!cbVerify.Checked) return true;
            if (Definizioni.updating) return true;

            /***********************************************************************************
             * SHORT: verifico il numero di salti
             * **********************************************************************************/
            if (Definizioni.idSegment == 1)
            {
                if (flag == 1 || flag == 2)
                {
                    if (int.Parse(Definizioni.elemSegment[1].ToString()) >= int.Parse(Definizioni.paramSegment[1].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel10"].ToString().
                            Replace("@", Definizioni.elemSegment[5].ToString()).
                            Replace("#", Definizioni.elemSegment[6].ToString());
                        return false;
                    }
                }
            }
            else
            {
                /***********************************************************************************
                 * LUNGO: verifico il numero di salti
                 * **********************************************************************************/
                if (flag == 1 || flag == 2)
                {
                    if (!saltoSemplice)
                    {
                        if (int.Parse(Definizioni.elemSegment[1].ToString()) >= int.Parse(Definizioni.paramSegment[1].ToString()))
                        {
                            error.Visible = true;
                            error.Text = Definizioni.resources["panel11"].ToString().Replace("@", Definizioni.elemSegment[1].ToString());
                            return false;
                        }
                    }
                }
            }
            /***********************************************************************************
             * SHORT e LUNGO: verifico i salti singoli
             * **********************************************************************************/
            if (flag == 1)
            {
                if (int.Parse(Definizioni.elemSegment[5].ToString()) >= int.Parse(Definizioni.paramSegment[5].ToString()))
                {
                    error.Visible = true;
                    error.Text = Definizioni.resources["panel12"].ToString().Replace("@", Definizioni.elemSegment[5].ToString());
                    return false;
                }
            }
            /***********************************************************************************
             * SHORT: verifico la combinazione
             * **********************************************************************************/
            if (Definizioni.idSegment == 1)
            {
                if (flag == 2)
                {

                    if (numCombo == 1 )
                    {
                        if (int.Parse(Definizioni.elemSegment[6].ToString()) + 1 > int.Parse(Definizioni.paramSegment[6].ToString()))
                        {
                            error.Visible = true;
                            error.Text = Definizioni.resources["panel13"].ToString();
                            return false;
                        }
                    } else
                    {
                        if (int.Parse(Definizioni.elemSegment[6].ToString()) > int.Parse(Definizioni.paramSegment[6].ToString()))
                        {
                            error.Visible = true;
                            error.Text = Definizioni.resources["panel13"].ToString();
                            return false;
                        }
                    }


                    if (int.Parse(Definizioni.elemSegment[7].ToString()) >= int.Parse(Definizioni.paramSegment[7].ToString()))
                    {
                        error.Visible = true;
                        // *** Modifica del 17/05/2018 *** //
                        error.Text = Definizioni.resources["panel16"].ToString().Replace("@", Definizioni.paramSegment[7].ToString());
                        //Replace("@", Definizioni.elemSegment[7].ToString()); 
                        return false;
                    }
                }
            }
            else
            {
                /***********************************************************************************
                 * LUNGO: verifico la combinazione
                 * **********************************************************************************/
                if (flag == 2)
                {
                    if (numCombo == 1)
                    {
                        if (int.Parse(Definizioni.elemSegment[6].ToString()) + 1 > int.Parse(Definizioni.paramSegment[6].ToString()))
                        {
                            error.Visible = true;
                            error.Text = Definizioni.resources["panel15"].ToString().Replace("@", Definizioni.elemSegment[6].ToString());
                            return false;
                        }
                    }
                    else
                    {
                        if (int.Parse(Definizioni.elemSegment[6].ToString()) > int.Parse(Definizioni.paramSegment[6].ToString()))
                        {
                            error.Visible = true;
                            error.Text = Definizioni.resources["panel15"].ToString().Replace("@", Definizioni.elemSegment[6].ToString());
                            return false;
                        }
                    }

                    if (int.Parse(Definizioni.elemSegment[7].ToString()) >= int.Parse(Definizioni.paramSegment[7].ToString()))
                    {
                        error.Visible = true;
                        // *** Modifica del 17/05/2018 *** //
                        error.Text = Definizioni.resources["panel16"].ToString().Replace("@", Definizioni.paramSegment[7].ToString());
                        //Replace("@", Definizioni.elemSegment[7].ToString());

                        return false;
                    }
                }
            }
            /***********************************************************************************
             * SHORT e LUNGO: verifico le trottole singole
             * **********************************************************************************/
            if (flag == 3)
            {
                if (int.Parse(Definizioni.elemSegment[9].ToString()) >= int.Parse(Definizioni.paramSegment[9].ToString()))
                {
                    error.Visible = true;
                    error.Text = Definizioni.resources["panel17"].ToString().Replace("@", Definizioni.elemSegment[9].ToString());
                    return false;
                }
            }

            /***********************************************************************************
             * SHORT e LUNGO: verifico le trottole combinate
             * **********************************************************************************/
            if (flag == 4)
            {
                if (numCombo == 1)
                {
                    if (int.Parse(Definizioni.elemSegment[10].ToString()) + 1 > int.Parse(Definizioni.paramSegment[10].ToString()))
                    {
                        error.Visible = true;
                        // *** Modifica del 17/05/2018 *** //
                        error.Text = Definizioni.resources["panel18"].ToString().Replace("@", Definizioni.paramSegment[10].ToString());
                        return false;
                    }
                }
                else
                {
                    if (int.Parse(Definizioni.elemSegment[10].ToString()) > int.Parse(Definizioni.paramSegment[10].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel18"].ToString().Replace("@", Definizioni.paramSegment[10].ToString());
                        return false;
                    }
                }

                if (int.Parse(Definizioni.elemSegment[16].ToString()) >= int.Parse(Definizioni.paramSegment[16].ToString()))
                {
                    error.Visible = true;
                    // *** Modifica del 17/05/2018 *** //
                    error.Text = Definizioni.resources["panel14"].ToString().Replace("@", Definizioni.paramSegment[16].ToString());
                    //Replace("@", Definizioni.elemSegment[16].ToString());
                    return false;
                }
            }

            /***********************************************************************************
             * SHORT e LUNGO: verifico i passi
             * **********************************************************************************/
            if (flag == 5)
            {
                if (int.Parse(Definizioni.elemSegment[3].ToString()) >= int.Parse(Definizioni.paramSegment[3].ToString()))
                {
                    error.Visible = true;
                    error.Text = Definizioni.resources["panel19"].ToString();
                    return false;
                }
            }

            return true;
        }

        public void CheckDuplicati(ListViewItem currentItem)
        {
            try
            {
                if (!cbVerify.Checked) return;

                int countSolo = 0, countCombo = 0, indexCurrent = 0, countSpin = 0, countHeelSpin = 0;
                int maxSameSpin = 0, maxSameJump = 0, maxHeelSpin = 0,countSingleJump = 1;
                string code = "", currentCode = "";
                string type = "", currentType = "";
                string currentName = "";

                // modifica del 25/11/2018 - segnalazione di Bruno Acena
                //if (Definizioni.idCategoria == 4 || Definizioni.idCategoria == 3) countSingleJump = 1;
                //else countSingleJump = 2;
                countSingleJump = int.Parse(Definizioni.paramSegment[19].ToString()) - 1;

                currentCode = currentItem.SubItems[1].Text.TrimEnd('<').TrimEnd(' ');
                currentType = currentItem.SubItems[3].Text;
                currentName = currentItem.ToolTipText;
                indexCurrent = currentItem.Index;

                // se axel singolo gestisco a parte
                // *** Modifica del 24/09/2018 *** //
                if (Definizioni.idSegment == 1 && currentName.Contains("Axel"))
                {
                    countSingleJump = 1;
                }

                if (currentCode.Equals("NJ") || currentCode.Equals("NS")) return;

                for (int i = 0; i < lv.Items.Count; i++) // ciclo su tutti gli elementi 
                {
                    code = lv.Items[i].SubItems[1].Text.TrimEnd('<').TrimEnd(' ');
                    type = lv.Items[i].SubItems[3].Text; // SoloJump, ComboJump 1, ComboJump 2,..., solo spin,...
                    //name = lv.Items[i].ToolTipText;

                    
                    if (( (code.Equals(currentCode) ||
                          (code.Equals("U") && currentCode.Equals("NLUpr")) || (currentCode.Equals("U") && code.Equals("NLUpr")) ||
                          (code.Equals("S") && currentCode.Equals("NLSit")) || (currentCode.Equals("S") && code.Equals("NLSit")) ||
                          (code.Equals("C") && currentCode.Equals("NLCam"))  || (currentCode.Equals("C") && code.Equals("NLCam")) ||
                          (code.Equals("H") && currentCode.Equals("NLHee"))  || (currentCode.Equals("H") && code.Equals("NLHee")) ||
                          (code.Equals("Br") && currentCode.Equals("NLBro")) || (currentCode.Equals("Br") && code.Equals("NLBro")) ||
                          (code.Equals("In") && currentCode.Equals("NLInv")) || (currentCode.Equals("In") && code.Equals("NLInv")))
                          && !lv.Items[i].SubItems[7].Text.Contains("*")))
                    {
                        if (type.Equals("Jump") && !code.Equals("NJ")) countSolo++;
                        else if (type.StartsWith("CombJump") && !code.Equals("NJ")) countCombo++;
                        else 
                            if (type.Contains("Spin") && !code.Equals("NS")) countSpin++;
                        if (code.Equals("H") || code.Equals("NLHee")) countHeelSpin++;
                    }
                }

                maxSameJump = int.Parse(Definizioni.paramSegment[19].ToString());
                if (currentCode.Equals("H") || currentCode.Equals("NLHee") || currentCode.Equals("H") || currentCode.Equals("NLHee"))
                    maxHeelSpin = int.Parse(Definizioni.paramSegment[17].ToString()); // heel
                else maxSameSpin = int.Parse(Definizioni.paramSegment[18].ToString());

                if (currentType.Equals("Jump")) // jump singolo
                {
                    if (countSolo + countCombo > maxSameJump) addAsterisco = true;
                    else if (countSolo > countSingleJump) addAsterisco = true;
                    else if (countCombo > maxSameJump) addAsterisco = true;

                } else if (currentType.StartsWith("CombJump")) // jump combo
                {
                    if (countCombo > maxSameJump) addAsterisco = true;
                    else if (countSolo + countCombo > maxSameJump) addAsterisco = true;
                    else if (countSolo == maxSameJump - countSingleJump && countCombo >= maxSameJump) addAsterisco = true;
                } else if (currentCode.Equals("H") || currentCode.Equals("NLHee"))
                {
                    if (countHeelSpin > maxHeelSpin) addAsterisco = true;
                }
                else if (currentType.Contains("Spin")) // spin
                {
                    if (countSpin > maxSameSpin) addAsterisco = true;
                }

                if (addAsterisco)
                {
                    error.Visible = true;
                    error.Text += "\r\n" + Definizioni.resources["panel37"].ToString();
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("CheckSaltiDuplicati: " + ex.Message, "ERROR");
            }
        }

        // Non utilizzata
        public bool CheckNumElements(int flag, bool saltoSemplice, int numCombo)
        {
            try
            {
                error.Text = "";
                error.Visible = false;
                if (!cbVerify.Checked) return true;

                switch (flag)
                {
                    case 1: //Solo Jump
                        if (int.Parse(Definizioni.elemSegment[5].ToString()) > int.Parse(Definizioni.paramSegment[5].ToString()))
                        {
                            error.Visible = true;
                            error.Text = Definizioni.resources["panel12"].ToString().Replace("@", Definizioni.elemSegment[5].ToString());
                            return false;
                        }
                        // controllo salti totali
                        if (Definizioni.idSegment == 1)
                        {
                            if (int.Parse(Definizioni.elemSegment[1].ToString()) >= int.Parse(Definizioni.paramSegment[1].ToString()))
                            {
                                error.Visible = true;
                                error.Text = Definizioni.resources["panel10"].ToString().
                                    Replace("@", Definizioni.elemSegment[5].ToString()).
                                    Replace("#", Definizioni.elemSegment[6].ToString());
                                return false;
                            }
                        } else if (Definizioni.idSegment == 2) // lungo
                        {
                            if (!saltoSemplice)
                            {
                                if (int.Parse(Definizioni.elemSegment[1].ToString()) >= int.Parse(Definizioni.paramSegment[1].ToString()))
                                {
                                    error.Visible = true;
                                    error.Text = Definizioni.resources["panel11"].ToString().Replace("@", Definizioni.elemSegment[1].ToString());
                                    return false;
                                }
                            }
                        }
                            break;
                    case 2: //Combo Jump
                        if (Definizioni.idSegment == 1) // short
                        {
                            if (int.Parse(Definizioni.elemSegment[7].ToString()) == 0)
                            {
                                if (int.Parse(Definizioni.elemSegment[6].ToString()) >= int.Parse(Definizioni.paramSegment[6].ToString()))
                                {
                                    error.Visible = true;
                                    error.Text = Definizioni.resources["panel13"].ToString();
                                    return false;
                                }
                            }
                        }
                        else // long program
                        {
                            if (numCombo == 1)
                            {
                                if (int.Parse(Definizioni.elemSegment[6].ToString()) >= int.Parse(Definizioni.paramSegment[6].ToString()))
                                {
                                    error.Visible = true;
                                    error.Text = Definizioni.resources["panel15"].ToString().Replace("@", Definizioni.elemSegment[6].ToString());
                                    return false;
                                }
                            }
                        }

                        if (int.Parse(Definizioni.elemSegment[7].ToString()) >= int.Parse(Definizioni.paramSegment[7].ToString()))
                        {
                            error.Visible = true;
                            // *** Modifica del 17/05/2018 *** //
                            error.Text = Definizioni.resources["panel16"].ToString().Replace("@", Definizioni.paramSegment[7].ToString());
                            return false;
                        }
                        // controllo salti totali
                        if (Definizioni.idSegment == 1)
                        {
                            if (int.Parse(Definizioni.elemSegment[1].ToString()) >= int.Parse(Definizioni.paramSegment[1].ToString()))
                            {
                                error.Visible = true;
                                error.Text = Definizioni.resources["panel10"].ToString().
                                    Replace("@", Definizioni.elemSegment[5].ToString()).
                                    Replace("#", Definizioni.elemSegment[6].ToString());
                                return false;
                            }
                        }
                        else if (Definizioni.idSegment == 2) // lungo
                        {
                            if (!saltoSemplice)
                            {
                                if (int.Parse(Definizioni.elemSegment[1].ToString()) >= int.Parse(Definizioni.paramSegment[1].ToString()))
                                {
                                    error.Visible = true;
                                    error.Text = Definizioni.resources["panel11"].ToString().Replace("@", Definizioni.elemSegment[1].ToString());
                                    return false;
                                }
                            }
                        }
                        break;
                    case 3: //Solo Spin
                        // if (int.Parse(Definizioni.elemSegment[9].ToString()) >= int.Parse(Definizioni.paramSegment[9].ToString()))
                        if (int.Parse(Definizioni.elemSegment[9].ToString()) > int.Parse(Definizioni.paramSegment[9].ToString()))
                        {
                            error.Visible = true;
                            error.Text = Definizioni.resources["panel17"].ToString().Replace("@", Definizioni.elemSegment[9].ToString());
                            return false;
                        }
                        break;
                    case 4: //Combo Spins
                        if (numCombo == 1)
                        {
                            //if (int.Parse(Definizioni.elemSegment[10].ToString()) >= int.Parse(Definizioni.paramSegment[10].ToString()))
                            if (int.Parse(Definizioni.elemSegment[10].ToString()) > int.Parse(Definizioni.paramSegment[10].ToString()))
                            {
                                error.Visible = true;
                                // *** Modifica del 17/05/2018 *** //
                                error.Text = Definizioni.resources["panel18"].ToString().Replace("@", Definizioni.paramSegment[10].ToString());
                                return false;
                            }
                        }
                        if (int.Parse(Definizioni.elemSegment[16].ToString()) >= int.Parse(Definizioni.paramSegment[16].ToString()))
                        {
                            error.Visible = true;
                            // *** Modifica del 17/05/2018 *** //
                            error.Text = Definizioni.resources["panel14"].ToString().Replace("@", Definizioni.paramSegment[16].ToString());
                            return false;
                        }
                        break;
                    case 5: //Steps
                        if (int.Parse(Definizioni.elemSegment[3].ToString()) >= int.Parse(Definizioni.paramSegment[3].ToString()))
                        {
                            error.Visible = true;
                            error.Text = Definizioni.resources["panel19"].ToString();
                            return false;
                        }
                        break;
                    default:
                        break;

                }

                return true;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("CheckNumElements: " + ex.Message, "ERROR");
                return true;
            }


        }

        public bool CheckBeforeConfirm()
        {
            if (!cbVerify.Checked) return true;

            string message = "";
            int numElemViol = 0;
            int numAxel = 0, numToe = 0, numSal = 0, numFlip = 0, numLutz = 0, numLoop = 0, numThoren = 0, numComboj = 0;
            int numSpin = 0, numComboSpin = 0;
            int numSteps = 0;
            int numSit = 0;
            /***********************************************************************************
            * LUNGO: 
            * Salti:
            *  o E’ obbligatoria l’esecuzione di un salto Axel (singolo, 
            *      doppio o triplo), che potrà essere presentato anche in combinazione.
            *  o L’Axel potrà essere presentato non più di 2 volte e i salti doppi e tripli non potranno essere 
            *    presentati più di tre volte. Se presentati tre volte, una delle tre deve essere in combinazione. 
            *
            * Trottole:
            * Devono essere presentati tre elementi trottola:
            *   o Una trottola singola
            *   o Una trottola combinata
            *   o Una trottola combinata con un’abbassata obbligatoria.
            *   o Tutte le trottole devono essere diverse. Se fosse eseguita una trottola uguale alla precedente,
            *     sarà considerata come una delle tre possibili ma non sarà attribuito alcun valore.
            *   o Le trottole combinate non possono contenere più di 6 cambi di posizione e/o di piede e/o di 
            *     filo.
            *     
            * Passi:
            * Deve essere presentata una sequenza di passi a scelta tra Serpentina, Cerchio, Diagonale,
            *    Straight Line. Nel caso l’atleta presenti più serie di passi, la serie di passi che verrà valutata nel 
            *    punteggio sarà la prima.
            * **********************************************************************************/
            if (Definizioni.idSegment == 2)
            {
                foreach (ListViewItem lvi in lv.Items)
                {
                    // Salti
                    if (lvi.ToolTipText.Contains("Axel")) numAxel++;
                    else if (lvi.ToolTipText.Contains("Toeloop")) numToe++;
                    else if (lvi.ToolTipText.Contains("Salchow")) numSal++;
                    else if (lvi.ToolTipText.Contains("Flip")) numFlip++;
                    else if (lvi.ToolTipText.Contains("Lutz")) numLutz++;
                    else if (lvi.ToolTipText.Contains("Loop")) numLoop++;
                    else if (lvi.ToolTipText.Contains("Thoren")) numThoren++;
                    // trottole
                    else if (lvi.SubItems[3].Text.Equals("Spin")) numSpin++;
                    else if (lvi.SubItems[3].Text.StartsWith("CombSpin")) numComboSpin++;
                    //if (lvi.ToolTipText.Contains("Sit")) numSit++;
                    // passi
                    else if (lvi.ToolTipText.Contains("Step")) numSteps++;
                }

                //message += "\r\n*******JUMPS********\r\n";
                if (numAxel == 0) { numElemViol++; message += Definizioni.resources["panel20"].ToString() + "\r\n"; }
                //if (numAxel > 2) { numElemViol++; message += Definizioni.resources["panel21"].ToString() + "\r\n"; }
                //if (numToe > 3) { numElemViol++; message += Definizioni.resources["panel22"].ToString() + "\r\n"; }
                //if (numSal > 3) { numElemViol++; message += Definizioni.resources["panel23"].ToString() + "\r\n"; }
                //if (numFlip > 3) { numElemViol++; message += Definizioni.resources["panel24"].ToString() + "\r\n"; }
                //if (numLutz > 3) { numElemViol++; message += Definizioni.resources["panel25"].ToString() + "\r\n"; }
                //if (numLoop > 3) { numElemViol++; message += Definizioni.resources["panel26"].ToString() + "\r\n"; }
                //if (numThoren > 3) { numElemViol++; message += Definizioni.resources["panel37"].ToString() + "\r\n"; }

                //message += "\r\n\r\n******SPINS******\r\n";
                //if (numSpin == 0) { numElemViol++; message += Definizioni.resources["panel27"].ToString() + "\r\n";}
                //if (numComboSpin < 2) { numElemViol++; message += Definizioni.resources["panel28"].ToString().Replace("@", "" + numComboSpin) + "\r\n"; }

                //message += "\r\n\r\n*******STEPS********\r\n";
                if (numSteps == 0) { numElemViol++; message += Definizioni.resources["panel29"].ToString() + "\r\n"; }

            }

            /***********************************************************************************
            * SHORT: 
            * * richiesti 3 elementi Salti:
            *      Axel - Semplice, Doppio, Triplo
            *      Combinazioni di Salti - 3-5 Salti
            *      Salto Puntato
            * I salti in eccesso non saranno conteggiati e non porteranno ad alcuna penalizzazione. 
            * Tutti i tentativi occuperanno una posizione tra gli elementi Salto nel sistema.
            * Sono richieste 2 trottole:
            *      Trottola ad una posizione
            *      Trottola combinata di Massimo tre posizioni con inclusa una trottola abbassata.
            * Per il Programma Corto, la serie di passi obbligatoria puÃ² includere un solo salto 
            * (anche non riconosciuto). Brevi fermate sono
            * permesse se in armonia con la musica ed Ã¨ permesso retrocedere.
            * **********************************************************************************/
            else if (Definizioni.idSegment == 1)
            {
                foreach (ListViewItem lvi in lv.Items)
                {
                    // Axel
                    if (lvi.ToolTipText.Contains("Axel") && lvi.SubItems[3].Text.Equals("Jump")) numAxel++;
                    //if (lvi.ToolTipText.Contains("Toeloop") && lvi.SubItems[3].Text.Equals("Jump")) numToe++;
                    //if (lvi.ToolTipText.Contains("Flip") && lvi.SubItems[3].Text.Equals("Jump")) numToe++;
                    //if (lvi.ToolTipText.Contains("Lutz") && lvi.SubItems[3].Text.Equals("Jump")) numToe++;
                    // combinazioni 
                    if (lvi.SubItems[3].Text.StartsWith("CombJump")) numComboj++;
                    // trottole
                    if (lvi.SubItems[3].Text.Equals("Spin")) numSpin++;
                    if (lvi.SubItems[3].Text.StartsWith("CombSpin")) numComboSpin++;
                    //if (lvi.ToolTipText.Contains("Sit")) numSit++;
                    // passi
                    if (lvi.ToolTipText.Contains("Step")) numSteps++;
                }

                //message += "\r\n*******JUMPS********\r\n";
                if (numAxel == 0) { numElemViol++; message += Definizioni.resources["panel20"].ToString() + "\r\n"; }
                // *** 2.0 - eliminato il controllo del salto puntato
                //if (numToe == 0) { numElemViol++; message += Definizioni.resources["panel30"].ToString() + "\r\n"; }
                if (numComboj == 0) { numElemViol++; message += Definizioni.resources["panel31"].ToString() + "\r\n"; }

                //message += "\r\n\r\n******SPINS******\r\n";
                if (numSpin == 0) { numElemViol++; message += Definizioni.resources["panel27"].ToString() + "\r\n"; }
                if (numComboSpin == 0) { numElemViol++; message += Definizioni.resources["panel32"].ToString() + "\r\n"; }
                
                //message += "\r\n\r\n*******STEPS********\r\n";
                if (numSteps == 0) { numElemViol++; message += Definizioni.resources["panel29"].ToString() + "\r\n"; }

            }

            CheckComboSpin();

            // Verifico sit nel lungo e nello short
            int countComboSpin = 0, countSpin = 0;
            int numSpinLowValue = 0;
            //bool sitPresenteNellaCombo = false;
            if (int.Parse(Definizioni.paramSegment[21].ToString()) == 1)// && !sitAlreadyChecked) // Sit obbligatoria, controllo nel lungo e nello short
            {

                List<decimal[]> temp = new List<decimal[]>(5);
                foreach (ListViewItem lvi in lv.Items)
                {
                    if (lvi.SubItems[1].Text.Equals("S"))
                    {
                        numSit++;
                        break;
                    }

                    // *** Modifica del 6/8/2018 - Cerco le trottole in cui è stata eseguita la sit, anche no level
                    if (lvi.SubItems[3].Text.Contains("Spin")) // trottola 
                    {
                        //if (decimal.Parse(lvi.SubItems[5].Text) <= 0) modifica del 9/9/2018
                        if (decimal.Parse(lvi.SubItems[5].Text) <= 1) // prima trottola della combo/trottola singola
                        {
                            countSpin++;
                            temp.Add(new decimal[]{ 0, decimal.Parse(lvi.SubItems[2].Text)}); //0 = non sit, poi valore trottola

                        } else
                        {
                            temp[countSpin - 1][1] += decimal.Parse(lvi.SubItems[2].Text);
                            //temp[1][countSpin - 1] += decimal.Parse(lvi.SubItems[2].Text); modifica del 9 / 9 / 2018
                        }

                        if (lvi.SubItems[1].Text.Equals("NLSit"))
                        {
                            //sitPresenteNellaCombo = true;
                            temp[countSpin - 1][0] = 1;
                            // temp[0][countSpin - 1] = 1; modifica del 9 / 9 / 2018
                        }
                    }
                }

                if (numSit == 0)
                {
                    // -1: sit non eseguita, neanche No Level
                    numSpinLowValue = WhatSpinRemve(temp);
                    if (numSpinLowValue > 0) // cerco solo se ho tentato una sit
                    {
                        countComboSpin = 0;
                        int countEl = 1;
                        foreach (ListViewItem lvi in lv.Items)
                        {
                            if (decimal.Parse(lvi.SubItems[5].Text) == 1 && lvi.SubItems[3].Text.Contains("Spin")) // prima trottola della combo
                                countComboSpin++;

                            if (countComboSpin == numSpinLowValue)
                            {
                                if (decimal.Parse(lvi.SubItems[5].Text) > 0 && lvi.SubItems[3].Text.Contains("Spin"))
                                {
                                    lvi.SubItems[7].Text = "(*)";
                                    // aggiorno anche il campo nel database
                                    Utility.UpdateElementConAsterisco(countEl, true);
                                }
                            }
                            countEl++;
                        }
                    }
                }

                //sitAlreadyChecked = true;
            }

            if (numElemViol > 0 || 
                (int.Parse(Definizioni.paramSegment[21].ToString()) == 1 && numSit == 0))
            {
                if (numElemViol > 0)
                    Definizioni.warningMessage = numElemViol + " " + Definizioni.resources["panel33"].ToString() + "\r\n" +
                        message + "\r\n" + Definizioni.resources["panel34"].ToString();
                else if (numSit == 0)
                    Definizioni.warningMessage = "Sit spin not executed.\r\n" +
                        message + "\r\n" + Definizioni.resources["panel34"].ToString();
                else
                    Definizioni.warningMessage = "No illegal elements.\r\n" +
                        message + "\r\n" + Definizioni.resources["panel34"].ToString();
                WarningForm wf = new WarningForm(countComboSpin);
                wf.Opacity = .75;
                wf.ShowDialog();
                if (Definizioni.confirm) return true;
                else return false;
            }
            return true;
        }

        public int WhatSpinRemve(List<decimal[]> spins)
        {
            int numSpinToRemove = 1;
            decimal prevValue = 0;
            bool sitPresent = false;
            foreach (decimal[] t in spins)
            {
                if (t[0] == 1) // confronto le sole trottole che hanno all'interno una sit
                {
                    sitPresent = true;
                    if (t[1] < prevValue)
                        numSpinToRemove++;
                    prevValue = t[1];
                }

            }

            if (!sitPresent)
                numSpinToRemove = 0;

            return numSpinToRemove;
        }

        // verifico se in una combo spin siano valide almeno 2 trottole
        public void CheckComboSpin()
        {
            int numSpinsValide = 0;
            int numComboSpins = 0;
            string listaIndici = "";
            List<string[]> comboSpins = new List<string[]>(10);
            bool newSpin = false;

            foreach (ListViewItem lvi in lv.Items)
            {
                // azzero se prima trottola
                if (lvi.SubItems[3].Text.Equals("CombSpin 1"))
                {
                    //numSpinsValide = 0;
                    numComboSpins++;
                    newSpin = true;
                }

                // se trottola combo e Confirmed
                if ( lvi.SubItems[3].Text.StartsWith("CombSpin") &&
                  (!lvi.SubItems[2].Text.Equals("0,00") && 
                   !lvi.SubItems[2].Text.Equals("0.00")))
                {
                    numSpinsValide++;
                    listaIndici += lvi.Index + ";";
                }
                else // se trottola combo e non confermata
                if (lvi.SubItems[3].Text.StartsWith("CombSpin") &&
                  (lvi.SubItems[2].Text.Equals("0,00") ||
                   lvi.SubItems[2].Text.Equals("0.00")))
                {
                    listaIndici += lvi.Index + ";";
                }
                else if(!lvi.SubItems[3].Text.StartsWith("CombSpin")) // trottola terminata
                {
                    if (numSpinsValide < 2 && newSpin)
                    {
                        listaIndici = listaIndici.TrimEnd(';');
                        // inserisco asterisco
                        foreach (string s in listaIndici.Split(';'))
                        {
                            lv.Items[int.Parse(s)].SubItems[7].Text = "(*)";
                            Utility.UpdateElementConAsterisco(int.Parse(s) + 1, true);
                        }

                    }
                    
                    numSpinsValide = 0;
                    listaIndici = "";
                    newSpin = false;
                }
            }

            // se la trottola ultimo elemento
            if (numSpinsValide < 2 && newSpin)
            {
                listaIndici = listaIndici.TrimEnd(';');
                // inserisco asterisco
                foreach (string s in listaIndici.Split(';'))
                {
                    lv.Items[int.Parse(s)].SubItems[7].Text = "(*)";
                    Utility.UpdateElementConAsterisco(int.Parse(s) + 1, true);
                }

            }
        }

        public void ResetAfterInsertion()
        {
            try
            {
                if (Definizioni.updating)
                    lv.Items[currentElem].ForeColor = oldForeColor;

                Definizioni.updating = false;
                lv.Items[currentElem].BackColor = Color.Black;
                //if (Definizioni.updating)
                //    lv.Items[currentElem].ForeColor = oldForeColor;
                lv.Items[currentElem].Selected = false;
                currentElem = lv.Items.Count;

                EnableGroupBox();

                if (lv.Items.Count > 0) back.Enabled = true;
                else back.Enabled = false;

                // *** Modifica del 06/07/2018 - 1.0.4 *** //
                flowLayoutPanel1.Enabled = true;
                cancel.Enabled = false;
                startstop.Enabled = true;
                deductions.Enabled = true;
                checkCombo2.Enabled = true;
                cbBonusSpin2.Checked = false;
                // *** //
                // *** Modifica del 05/12/2018 - 2.0.0.9 *** //
                cbBonusSpin1.Checked = false;

                WriteLog();

                Thread.Sleep(100);
                //Thread.Sleep(500);
                Cursor.Show();
            }
            catch (Exception ex)
            {
                Utility.WriteLog("ResetAfterInsertion: " + ex.Message, "ERROR");
            }
        }

        public void ResetPercentualiTrottole()
        {
            try
            {
                cb25.SelectedItem = "0 %";
                cb40.SelectedItem = "0 %";
                cb45.SelectedItem = "0 %";
                cb48.SelectedItem = "0 %";
                cb47.SelectedItem = "0 %";
                cb49.SelectedItem = "0 %";
                cbCombo.SelectedItem = "0 %";
            }
            catch (Exception ex)
            {
                Utility.WriteLog("ResetPercentualiTrottole: " + ex.Message, "ERROR");
            }
        }

        public void WriteLog()
        {

            log.Text = //"Total=" + Definizioni.elemSegment[0] + "|" +
                       "Solo Jumps   = " + Definizioni.elemSegment[5] + "   Combo Jumps = " + Definizioni.elemSegment[6] + "\r\n" +
                       "MaxCombJumps = " + Definizioni.elemSegment[7] + "   Total Jumps = " + Definizioni.elemSegment[1] + "\r\n" +
                       "Solo Spins   = " + Definizioni.elemSegment[9] + "   Combo Spins = " + Definizioni.elemSegment[10] + "\r\n" +
                       "MaxCombSpins = " + Definizioni.elemSegment[16] +"   Steps       = " + Definizioni.elemSegment[3];
            log.SelectionStart = log.Text.Length;
            log.ScrollToCaret();
        }

        public void ClearElement()
        {
            try
            {
                Cursor.Hide();
                int count = lv.Items.Count;
                if (count == 0)
                {
                    Cursor.Show();
                    return;
                }
                ListViewItem lastItem = lv.Items[count - 1];
                string numEl = lastItem.SubItems[0].Text;
                string codeEl = lastItem.SubItems[1].Text;
                string valEl = lastItem.SubItems[2].Text;
                string typeEl = lastItem.SubItems[3].Text;
                string cat = lastItem.SubItems[4].Text;
                string numcombo = lastItem.SubItems[5].Text;
                string du = lastItem.SubItems[6].Text;
                string note = lastItem.SubItems[7].Text;

                if (lv.Items.Count > 1)
                {
                    if (lv.Items[lv.Items.Count - 2].SubItems[3].Text.Contains("CombJump"))
                        saltoPrec = lv.Items[lv.Items.Count - 2].SubItems[1].Text;
                }

                if (typeEl.Equals("Jump")) // salto singolo
                {
                    // tolgo un elemento salto singolo
                    if (!codeEl.Contains("A")) // se axel non  lo tolgo
                        Definizioni.elemSegment[5] = int.Parse(Definizioni.elemSegment[5].ToString()) - 1;
                    Definizioni.elemSegment[0] = int.Parse(Definizioni.elemSegment[0].ToString()) - 1;
                    Definizioni.elemSegment[1] = int.Parse(Definizioni.elemSegment[1].ToString()) - 1;
                    progEl--;
                }
                else
                    if (typeEl.Contains("CombJump")) // combo
                {
                    // tolgo un elemento salto alla combinazione
                    Definizioni.elemSegment[7] = int.Parse(typeEl.Substring(9, 1));
                    Definizioni.elemSegment[7] = int.Parse(Definizioni.elemSegment[7].ToString()) - 1;
                    if (Definizioni.idSegment == 1) // SHORT
                    {
                        if (typeEl.Equals("CombJump 1"))
                        {
                            progEl--;
                            Definizioni.elemSegment[6] = int.Parse(Definizioni.elemSegment[6].ToString()) - 1;
                            Definizioni.elemSegment[0] = int.Parse(Definizioni.elemSegment[0].ToString()) - 1;
                            Definizioni.elemSegment[1] = int.Parse(Definizioni.elemSegment[1].ToString()) - 1;
                            Definizioni.numComboElement--;
                        }
                    }
                    else // LUNGO
                    {
                        if (typeEl.Equals("CombJump 1"))
                        {
                            progEl--;
                            Definizioni.elemSegment[6] = int.Parse(Definizioni.elemSegment[6].ToString()) - 1;
                            Definizioni.numComboElement--;
                        }
                        if (!codeEl.Equals("NJ") || note.Equals("X"))
                        {
                            Definizioni.elemSegment[0] = int.Parse(Definizioni.elemSegment[0].ToString()) - 1;
                            Definizioni.elemSegment[1] = int.Parse(Definizioni.elemSegment[1].ToString()) - 1;
                        }
                    }
                    if (numcombo.Equals("1"))
                        saltoPrec = "";
                }
                else
                        if (typeEl.Equals("Spin")) // trottola singola
                {
                    progEl--;
                    // tolgo un elemento trottola
                    Definizioni.elemSegment[9] = int.Parse(Definizioni.elemSegment[9].ToString()) - 1;
                    Definizioni.elemSegment[0] = int.Parse(Definizioni.elemSegment[0].ToString()) - 1;
                    Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) - 1;
                }
                else
                            if (typeEl.Contains("CombSpin")) // combo trottole
                {
                    Definizioni.elemSegment[16] = int.Parse(typeEl.Substring(9, 1));
                    Definizioni.elemSegment[16] = int.Parse(Definizioni.elemSegment[16].ToString()) - 1;

                    if (typeEl.Equals("CombSpin 1"))
                    {
                        progEl--;
                        Definizioni.elemSegment[10] = int.Parse(Definizioni.elemSegment[10].ToString()) - 1;
                        Definizioni.elemSegment[0] = int.Parse(Definizioni.elemSegment[0].ToString()) - 1;
                        Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) - 1;
                        Definizioni.numComboElement--;
                    }
                }
                else if (typeEl.Equals("Steps")) // passi
                {
                    progEl--;
                    // tolgo l'elemento passi
                    Definizioni.elemSegment[3] = int.Parse(Definizioni.elemSegment[3].ToString()) - 1;
                    Definizioni.elemSegment[0] = int.Parse(Definizioni.elemSegment[0].ToString()) - 1;
                }
                currentElem--;
                lv.Items[lv.Items.Count - 1].Remove();

                if (int.Parse(numcombo) <= 1 || typeEl.Contains("CombJump"))
                    Utility.SendBroadcast("<ANNULLA/>"); // invio il comando di annulla ai giudici
                Utility.ScrivoElementoOnDB(lv, log, true);

                // Aggiorno i totali
                if (!note.Contains("*"))
                    Utility.AggiornoTotali(log, lv, deductions, elements, total);

                if (lv.Items.Count > 0) back.Enabled = true;
                else back.Enabled = false;

                WriteLog();
                Thread.Sleep(200);
                Cursor.Show();
                error.ResetText(); error.Visible = false;
            }

            catch (Exception ex)
            {
                Cursor.Show();
                Utility.WriteLog("ClearElement: " + ex.Message, "ERROR");
            }
        }

        // MAGGIORAZIONI
        public void CalcoloMaggiorazioni()
        {
            try
            {
                string tipo = "", salto = "", saltoPrec = "";
                decimal valore;
                bool comboPrec = false;
                ListViewItem itemPrec = null;
                foreach (ListViewItem lvi in lv.Items)
                {
                    tipo = lvi.SubItems[3].Text;
                    if (!tipo.Contains("CombJump"))
                    {
                        comboPrec = false;
                        itemPrec = null;
                    } else
                    if (tipo.Contains("CombJump") && comboPrec && (!tipo.Equals("CombJump 1")))
                    {
                        //if (lvi.Index == (lv.Items.Count - 1))
                        if (lvi.Index == currentElem)
                        {
                            salto = lvi.SubItems[1].Text;
                            // BUG FIX: Recupero il valore del salto under-half-down
                            valore = decimal.Parse(lvi.Tag.ToString());

                            if (Definizioni.underJump || Definizioni.downJump || Definizioni.halfJump)
                                valore = decimal.Parse(lvi.SubItems[2].Text); // salto falloso

                            string a = lvi.SubItems[2].Text;
                            string b = lvi.SubItems[3].Text;
                            string c = lvi.SubItems[4].Text;
                            string d = lvi.SubItems[5].Text;

                            saltoPrec = itemPrec.SubItems[1].Text;

                            if (salto.StartsWith("2") && saltoPrec.StartsWith("2")) // D/D
                            {
                                if (Definizioni.paramSegment[13] != 0) // solo per categorie piccole
                                {
                                    lvi.SubItems[2].Text = valore + Math.Round(((valore * Definizioni.paramSegment[13]) / 100), 2) + ""; // 10%
                                    lvi.SubItems[7].Text = Definizioni.paramSegment[13] + "%";// 10%
                                }
                            }
                            else if (salto.StartsWith("2") && saltoPrec.StartsWith("3")) // D/T
                            {
                                lvi.SubItems[2].Text = valore + Math.Round(((valore * Definizioni.paramSegment[14]) / 100), 2) + ""; // 20%
                                lvi.SubItems[7].Text = Definizioni.paramSegment[14] + "%"; // 20%
                            }
                            else if (salto.StartsWith("3") && saltoPrec.StartsWith("2")) // T/D
                            {
                                lvi.SubItems[2].Text = valore + Math.Round(((valore * Definizioni.paramSegment[14]) / 100), 2) + ""; // 20%
                                lvi.SubItems[7].Text = Definizioni.paramSegment[14] + "%"; // 20%
                            }
                            else if (salto.StartsWith("3") && saltoPrec.StartsWith("3")) // T/T
                            {
                                lvi.SubItems[2].Text = valore + Math.Round(((valore * Definizioni.paramSegment[15]) / 100), 2) + ""; // 30%
                                lvi.SubItems[7].Text = Definizioni.paramSegment[15] + "%"; // 30%
                            }
                            //itemPrec = lvi;
                            //comboPrec = true;
                        }
                        itemPrec = lvi;
                        comboPrec = true;
                    } else
                    if (tipo.Contains("CombJump"))
                    {
                        itemPrec = lvi;
                        comboPrec = true;
                    }

                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("CalcoloMaggiorazioni: " + ex.Message, "ERROR");
            }
        }

        public void InsertElement(bool saltoSemplice, Button salto, Button ungiro, object ifCombo, bool failed)
        {
            try
            {
                string nome = ((DataRow)salto.Tag)[1].ToString(); // nome salto
                string code = ((DataRow)salto.Tag)[3].ToString(); // codice del salto
                string valueBase = ((DataRow)salto.Tag)[7].ToString(); // valore base del salto
                string valueUnder = ((DataRow)salto.Tag)[8].ToString(); // under
                string valueHalf = ((DataRow)salto.Tag)[9].ToString(); // half
                string valueCombo = ((DataRow)salto.Tag)[13].ToString();// valore salto combo
                string valueComboU = ((DataRow)salto.Tag)[14].ToString();// valore salto combo under
                string valueComboH = ((DataRow)salto.Tag)[15].ToString();// valore salto combo half

                addAsterisco = false;
                Cursor.Current = Cursors.WaitCursor;
                int numCombo = 1;
                checkCombo2.Checked = false;
                decimal valoreSalto = 0.00m;
                if (ifCombo.Equals("2")) // combinazioni
                {
                    Definizioni.elemSegment[16] = 0;

                    if (!Definizioni.updating)
                    {
                        numCombo = int.Parse(Definizioni.elemSegment[7].ToString()) + 1;
                        //if (!CheckNumElements(2, saltoSemplice, numCombo))
                        if (!CheckShortAndLongProgram(2, saltoSemplice, numCombo))
                        {
                            addAsterisco = true;
                        }
                        newCombo.Enabled = true;
                        lv.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "", "", "", "" }, -1));
                        lv.Items[currentElem].SubItems[7].Text = "";
                    }
                    else // aggiornamento
                    {
                        //if (!CheckNumElements(2, saltoSemplice, numCombo))
                        if (!CheckShortAndLongProgram(2, saltoSemplice, numCombo))
                        {
                            addAsterisco = true;
                        }
                        newCombo.Enabled = true;
                        numCombo = int.Parse(lv.Items[currentElem].SubItems[3].Text.Substring(9, 1));

                        if (lv.Items[currentElem].SubItems[7].Text.Contains("*"))
                            addAsterisco = true;
                        lv.Items[currentElem].SubItems[7].Text = "";
                    }
                    saltoCorr = code;
                    valoreSalto = decimal.Parse(valueCombo);
                    lv.Items[currentElem].SubItems[1].Text = "";
                    lv.Items[currentElem].SubItems[1].Text = code;
                    lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", valoreSalto);
                    lv.Items[currentElem].Tag = valoreSalto + ""; // imposto il valore del salto anche nel tag
                    lv.Items[currentElem].SubItems[3].Text = "CombJump " + numCombo;
                    lv.Items[currentElem].SubItems[4].Text = "J";
                    lv.Items[currentElem].SubItems[5].Text = int.Parse(Definizioni.elemSegment[7].ToString()) + 1 + "";
                    lv.Items[currentElem].SubItems[6].Text = "";
                    if (failed) lv.Items[currentElem].ForeColor = rigaComboX;
                    else lv.Items[currentElem].ForeColor = rigaCombo;
                    lv.Items[currentElem].ToolTipText = nome;
                    // aggiungo un elemento salto alla combinazione se non sto in modifica elemento
                    if (!Definizioni.updating)
                    {
                        Definizioni.elemSegment[7] = int.Parse(Definizioni.elemSegment[7].ToString()) + 1;
                        if (!saltoSemplice)
                        {
                            if (Definizioni.idSegment == 1) // SHORT
                            {
                                if (numCombo == 1)
                                {
                                    progEl++;
                                    Definizioni.elemSegment[6] = int.Parse(Definizioni.elemSegment[6].ToString()) + 1;
                                    Definizioni.elemSegment[0] = int.Parse(Definizioni.elemSegment[0].ToString()) + 1;
                                    Definizioni.elemSegment[1] = int.Parse(Definizioni.elemSegment[1].ToString()) + 1;
                                    Definizioni.numComboElement++; // aumento il progressivo solo quando trovo 1
                                }
                            }
                            else
                            {
                                if (numCombo == 1)
                                {
                                    progEl++;
                                    Definizioni.elemSegment[6] = int.Parse(Definizioni.elemSegment[6].ToString()) + 1;
                                    Definizioni.numComboElement++; // aumento il progressivo solo quando trovo 1
                                }
                                Definizioni.elemSegment[0] = int.Parse(Definizioni.elemSegment[0].ToString()) + 1;
                                Definizioni.elemSegment[1] = int.Parse(Definizioni.elemSegment[1].ToString()) + 1;
                            }
                        }
                        else
                        {
                            if (numCombo == 1) progEl++;
                        }

                    }

                }
                else //salti
                {
                    newCombo.Enabled = false;
                    Definizioni.elemSegment[7] = 0; // azzero gli elementi di una combinazione
                    Definizioni.elemSegment[16] = 0; // azzero gli elementi di una combinazione di trottole
                    if (nome.Contains("Axel") && Definizioni.idSegment == 1)
                    {
                        // se Axel e Short program non lo aggiungo ai salti singoli 
                    }
                    else if (!CheckShortAndLongProgram(1, false, 0))
                    {
                        addAsterisco = true;
                    }
                    if (!Definizioni.updating)
                    {
                        lv.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "", "", "", "" }, -1));
                        lv.Items[currentElem].SubItems[7].Text = "";
                    }
                    else
                    {
                        if (lv.Items[currentElem].SubItems[7].Text.Contains("*"))
                            addAsterisco = true;
                        lv.Items[currentElem].SubItems[7].Text = "";
                    }
                    lv.Items[currentElem].SubItems[1].Text = code;
                    valoreSalto = decimal.Parse(valueBase);
                    lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", valoreSalto);
                    lv.Items[currentElem].SubItems[3].Text = "Jump";
                    lv.Items[currentElem].SubItems[4].Text = "J";
                    lv.Items[currentElem].SubItems[5].Text = "0";
                    lv.Items[currentElem].SubItems[6].Text = "";
                    lv.Items[currentElem].ToolTipText = nome;
                    lv.Items[currentElem].ForeColor = rigaSoloJump;
                    // aggiungo un elemento salto singolo
                    if (!Definizioni.updating)
                    {
                        progEl++;
                        if (!nome.Contains("Axel"))
                            Definizioni.elemSegment[5] = int.Parse(Definizioni.elemSegment[5].ToString()) + 1;
                        Definizioni.elemSegment[0] = int.Parse(Definizioni.elemSegment[0].ToString()) + 1;
                        Definizioni.elemSegment[1] = int.Parse(Definizioni.elemSegment[1].ToString()) + 1;
                    }
                    saltoPrec = "";
                }

                // se il salto è nojump o underotated o downgraded
                if (salto == el64)
                {
                    //lv.Items[currentElem].SubItems[2].Text = "0,00";
                    lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", valoreSalto);
                    lv.Items[currentElem].SubItems[6].Text = "";
                }
                else if (failed)
                {
                    //lv.Items[currentElem].SubItems[2].Text = "0,00";
                    lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", valoreSalto);
                    lv.Items[currentElem].SubItems[7].Text = "X";
                }
                else if (Definizioni.underJump)
                {
                    lv.Items[currentElem].SubItems[1].Text += " <";
                    if (ifCombo.Equals("2")) lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", decimal.Parse(valueComboU));
                    else lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", decimal.Parse(valueUnder));
                    lv.Items[currentElem].SubItems[6].Text = "U";
                }
                else if (Definizioni.halfJump)
                {
                    lv.Items[currentElem].SubItems[1].Text += " <<";
                    if (ifCombo.Equals("2")) lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", decimal.Parse(valueComboH)); 
                    else lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", decimal.Parse(valueHalf));
                    lv.Items[currentElem].SubItems[6].Text = "H";
                }
                else if (Definizioni.downJump)
                {
                    lv.Items[currentElem].SubItems[1].Text += " <<<";
                    //if (saltoSemplice) lv.Items[currentElem].SubItems[2].Text = "0,00";
                    if (saltoSemplice) lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", 0);
                    else if (ifCombo.Equals("2")) lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", decimal.Parse(((DataRow)ungiro.Tag)[13].ToString()));
                    else lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", decimal.Parse(((DataRow)ungiro.Tag)[7].ToString()));
                    lv.Items[currentElem].SubItems[6].Text = "D";
                }

                // *** modifica del 10/07/2018 - verifico i salti duplicati *** //
                if (RollartSystemTech.Properties.Settings.Default.CheckDuplicates)
                {
                    // verifico i salti duplicati
                    CheckDuplicati(lv.Items[currentElem]);
                }

                if (addAsterisco) lv.Items[currentElem].SubItems[7].Text = "(*)";

                if (numCombo == 1 && !Definizioni.updating)
                    lv.Items[currentElem].Text = progEl + "";

                if (!addAsterisco) CalcoloMaggiorazioni();

                // maggiorazioni metà disco
                // *** modifica del 21/06/2017 - non calcolo le maggiorazioni per lo short *** //
                if (!Definizioni.updating && Definizioni.idSegment != 1 && !addAsterisco)
                {
                    if (salto != el64 && salto != el64_ && salto != el64__)
                    {
                        if (time.TotalSeconds >= Convert.ToDouble(secMetaDisco))
                        {
                            string valBefore = lv.Items[currentElem].SubItems[2].Text;
                            decimal dec = decimal.Parse(valBefore);
                            lv.Items[currentElem].SubItems[2].Text =
                                dec + Math.Round(((dec * 10) / 100), 2) + ""; // 10%
                            lv.Items[currentElem].SubItems[8].Text = "T";
                            //lv.Items[currentElem].SubItems[8].Text += " " + Utility.GetSymbol("00B9");
                        }
                    }
                }

                if (Definizioni.updating && !lv.Items[currentElem].SubItems[8].Text.Equals(""))
                {
                    string valBefore = lv.Items[currentElem].SubItems[2].Text;
                    decimal dec = decimal.Parse(valBefore);
                    lv.Items[currentElem].SubItems[2].Text =
                        dec + Math.Round(((dec * 10) / 100), 2) + ""; // 10%
                    //lv.Items[currentElem].SubItems[8].Text += "10%";
                }

                down.BackColor = Color.Silver;
                under.BackColor = Color.Silver;
                down2.BackColor = Color.Silver;
                under2.BackColor = Color.Silver;
                half.BackColor = Color.Silver;
                half2.BackColor = Color.Silver;
                Definizioni.downJump = false; Definizioni.underJump = false; Definizioni.halfJump = false;

                // se l'invio ai giudice va a buon fine aggiorno il db
                lv.Items[currentElem].Selected = true;
                if (Utility.InvioDati(lv, log))
                    Utility.ScrivoElementoOnDB(lv, log, false);

                // Aggiorno i totali
                if (!addAsterisco) Utility.AggiornoTotali(log, lv, deductions, elements, total);

                ResetAfterInsertion();
                Cursor.Current = Cursors.Hand;
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Hand;
                Utility.WriteLog("InsertElement: " + ex.Message, "ERROR");
            }
        }

        private void newCombo_Click(object sender, EventArgs e)
        {
            try
            {
                Definizioni.elemSegment[7] = 0;
                Definizioni.elemSegment[16] = 0;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("NewCombo: " + ex.Message, "ERROR");
            }
        }

        public void InsertPassi(Button steps, ListView lv, TextBox log,
            Label deductions, Label elements, Label total,
            Button back, Label error)
        {
            try
            {
                addAsterisco = false;
                Cursor.Hide();
                newCombo.Enabled = false;
                newComboSpin.Enabled = false;
                checkCombo2.Checked = false;
                Definizioni.elemSegment[7] = 0; // azzero gli elementi di una combinazione
                Definizioni.elemSegment[16] = 0; // azzero gli elementi di una combinazione di trottole
                //if (!CheckNumElements(5, false, 0))
                if (!CheckShortAndLongProgram(5, false, 0))
                {
                    addAsterisco = true;
                }
                if (!Definizioni.updating)
                {
                    lv.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "", "", "", "" }, -1));
                    lv.Items[currentElem].SubItems[7].Text = "";
                } else
                {
                    if (lv.Items[currentElem].SubItems[7].Text.Contains("*"))
                        addAsterisco = true;
                    lv.Items[currentElem].SubItems[7].Text = "";
                }
                lv.Items[currentElem].SubItems[1].Text = ((DataRow)steps.Tag)[3].ToString();
                lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", decimal.Parse(((DataRow)steps.Tag)[7].ToString())); ;
                lv.Items[currentElem].SubItems[3].Text = "Steps";
                lv.Items[currentElem].SubItems[4].Text = "ST";
                lv.Items[currentElem].SubItems[5].Text = "0";
                lv.Items[currentElem].SubItems[6].Text = "";
                lv.Items[currentElem].ToolTipText = ((DataRow)steps.Tag)[1].ToString();
                if (!Definizioni.updating)
                {
                    Definizioni.elemSegment[3] = int.Parse(Definizioni.elemSegment[3].ToString()) + 1;
                    Definizioni.elemSegment[0] = int.Parse(Definizioni.elemSegment[0].ToString()) + 1;
                }

                lv.Items[currentElem].ForeColor = rigaSteps;
                if (addAsterisco) lv.Items[currentElem].SubItems[7].Text = "(*)";
                if (!Definizioni.updating) progEl++;
                saltoPrec = "";
                if (!Definizioni.updating)
                    lv.Items[currentElem].Text = progEl + "";

                lv.Items[currentElem].Selected = true;
                if (Utility.InvioDati(lv, log))
                    Utility.ScrivoElementoOnDB(lv, log, false);
                // aggiorno totali
                if (!addAsterisco) Utility.AggiornoTotali(log, lv, deductions, elements, total);

                ResetAfterInsertion();
            }
            catch (Exception ex)
            {
                Cursor.Show();
                Utility.WriteLog("InsertPassi: " + ex.Message, "ERROR");
            }
        }

        public void InsertTrottola(Button tr)
        {
            try
            {
                addAsterisco = false;
                Cursor.Hide();
                decimal valoreTrottola = 0.00m;
                int percentuale = 0;
                newComboSpin.Enabled = false;
                string typeTrottola = "";
                ComboBox comboPercentuale = null;
                Definizioni.elemSegment[7] = 0;

                if (Definizioni.updating)
                    typeTrottola = lv.Items[currentElem].SubItems[3].Text;

                string numElement = tr.Name.ToString().Substring(2); // recupero il numero dell'elemento
                if (this.Controls.Find("cb" + numElement, true).Length > 0)
                    comboPercentuale = ((ComboBox)this.Controls.Find("cb" + numElement, true)[0]);

                // *** Modifica del 16/05/2018
                // se percentuale valorizzata per trottola combo utilizzo questa
                //comboPercentuale = ((ComboBox)this.Controls.Find("cbCombo", true)[0]);
                // *** //

                // calcolo percentuale su ogni trottola
                if (comboPercentuale != null)
                {
                    percentuale = int.Parse(comboPercentuale.SelectedItem.ToString().Split(' ')[0]);
                }

                int numComboSpin = 1;
                if (checkCombo2.Checked) // combinazioni di trottole
                {
                    if (!Definizioni.updating)
                    {
                        numComboSpin = int.Parse(Definizioni.elemSegment[16].ToString()) + 1;
                        if (!CheckShortAndLongProgram(4, false, numComboSpin))
                        {
                            addAsterisco = true;
                        }
                        newComboSpin.Enabled = true;
                        lv.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "", "", "", "" }, -1));
                        lv.Items[currentElem].SubItems[7].Text = "";

                    } else // aggiornamento
                    {
                        if (!CheckShortAndLongProgram(4, false, numComboSpin))
                        {
                            addAsterisco = true;
                        }
                        newComboSpin.Enabled = true;
                        numComboSpin = int.Parse(lv.Items[currentElem].SubItems[3].Text.Substring(9, 1));

                        if (lv.Items[currentElem].SubItems[7].Text.Contains("*"))
                            addAsterisco = true;
                        lv.Items[currentElem].SubItems[7].Text = "";
                    }
                    lv.Items[currentElem].SubItems[1].Text = ((DataRow)tr.Tag)[3].ToString();
                    valoreTrottola = decimal.Parse(((DataRow)tr.Tag)[7].ToString())
                        + Math.Round(((decimal.Parse(((DataRow)tr.Tag)[7].ToString()) * percentuale) / 100), 2);
                    if (comboPercentuale != null)
                    {
                        // *** modifica del 21/06/2017 - 1.0.1 - bonus di più 2 a tutte le trottole
                        // *** modifica del 26/06/2018 - 1.0.4 - bonus parametrico 
                        if (cbBonusSpin2.Checked)
                        {
                            //valoreTrottola = valoreTrottola + 2.00m;
                            if (Definizioni.paramSegment[23] > 0)
                                valoreTrottola = valoreTrottola + Definizioni.paramSegment[23];
                            else valoreTrottola = valoreTrottola + 2.00m;
                        }
                        // *** Modifica del 05/12/2018 - 2.0.0.9 *** // 
                        else if (cbBonusSpin1.Checked) 
                        {
                            valoreTrottola = valoreTrottola + 1.00m;
                        }
                    }
                    lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", valoreTrottola);
                    lv.Items[currentElem].SubItems[3].Text = "CombSpin " + numComboSpin;
                    lv.Items[currentElem].SubItems[4].Text = "S";
                    lv.Items[currentElem].SubItems[5].Text = numComboSpin + "";

                    lv.Items[currentElem].SubItems[7].Text = "";
                    if (cbBonusSpin2.Checked || cbBonusSpin1.Checked)
                        lv.Items[currentElem].SubItems[7].Text += "B ";
                    if (percentuale > 0)
                        lv.Items[currentElem].SubItems[7].Text += "+ " + percentuale + " %";
                    //else lv.Items[currentElem].SubItems[7].Text += "";

                    lv.Items[currentElem].ToolTipText = ((DataRow)tr.Tag)[1].ToString();
                    lv.Items[currentElem].ForeColor = rigaComboSpin;

                    // aggiungo un elemento trottola alla combinazione se non sto in modifica elemento
                    if (!Definizioni.updating)
                    {
                        Definizioni.elemSegment[16] = int.Parse(Definizioni.elemSegment[16].ToString()) + 1;
                        if (Definizioni.idSegment == 1) // SHORT
                        {
                            if (numComboSpin == 1)
                            {
                                progEl++;
                                Definizioni.elemSegment[10] = int.Parse(Definizioni.elemSegment[10].ToString()) + 1;
                                Definizioni.elemSegment[0] = int.Parse(Definizioni.elemSegment[0].ToString()) + 1;
                                Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) + 1;
                                Definizioni.numComboElement++; // aumento il progressivo solo quando trovo 1
                                numComboSpins++;
                            }
                        }
                        else
                        {
                            if (numComboSpin == 1)
                            {
                                progEl++;
                                Definizioni.elemSegment[10] = int.Parse(Definizioni.elemSegment[10].ToString()) + 1;
                                Definizioni.numComboElement++; // aumento il progressivo solo quando trovo 1
                            }
                            Definizioni.elemSegment[0] = int.Parse(Definizioni.elemSegment[0].ToString()) + 1;
                            Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) + 1;
                        }
                    }
                    else // sto in modifica elemento
                    {
                        if (typeTrottola.StartsWith("Spin")) // se la trottola che aggiorno è singola
                        {
                            // tolgo una trottola combinata...
                            Definizioni.elemSegment[10] = int.Parse(Definizioni.elemSegment[10].ToString()) - 1;
                            // ...e aggiungo una trottola singola
                            Definizioni.elemSegment[9] = int.Parse(Definizioni.elemSegment[9].ToString()) + 1;
                        }
                    }

                }
                else //trottole singole
                {
                    newComboSpin.Enabled = false;
                    Definizioni.elemSegment[16] = 0; // azzero gli elementi di una combinazione di trottole
                    //if (!CheckNumElements(3, false, 0))
                    if (!CheckShortAndLongProgram(3, false, 0))
                    {
                        addAsterisco = true;
                    }
                    if (!Definizioni.updating)
                    {
                        lv.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "", "", "", "" }, -1));
                    // *** Modifica del 10/9/2018 ***//
                        lv.Items[currentElem].SubItems[7].Text = "";
                    } else
                    {
                        if (lv.Items[currentElem].SubItems[7].Text.Contains("*"))
                            addAsterisco = true;
                        lv.Items[currentElem].SubItems[7].Text = "";
                    }
                    // *** Fine Modifica del 10/9/2018 ***//
                    lv.Items[currentElem].SubItems[1].Text = ((DataRow)tr.Tag)[3].ToString();
                    valoreTrottola = decimal.Parse(((DataRow)tr.Tag)[7].ToString())
                        + Math.Round(((decimal.Parse(((DataRow)tr.Tag)[7].ToString()) * percentuale) / 100), 2);
                    if (comboPercentuale != null)
                    {
                        // *** modifica del 21/06/2017 - 1.0.1 - bonus di più 2 a tutte le trottole
                        // *** modifica del 26/06/2018 - 1.0.4 - bonus parametrico 
                        //if (comboPercentuale.Name.Equals("cb25") && cbBonusSpin.Checked)
                        if (cbBonusSpin2.Checked)
                        {
                            //valoreTrottola = valoreTrottola + 2.00m;
                            if (Definizioni.paramSegment[23] > 0)
                                valoreTrottola = valoreTrottola + Definizioni.paramSegment[23];
                            else valoreTrottola = valoreTrottola + 2.00m;
                        }
                        // *** Modifica del 05/12/2018 - 2.0.0.9 *** // 
                        else if (cbBonusSpin1.Checked)
                        {
                            valoreTrottola = valoreTrottola + 1.00m;
                        }
                    }

                    lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", valoreTrottola);
                    lv.Items[currentElem].SubItems[3].Text = "Spin";
                    lv.Items[currentElem].SubItems[4].Text = "S";
                    lv.Items[currentElem].SubItems[5].Text = "0";

                    //lv.Items[currentElem].SubItems[7].Text = "";// modifica del 9/9/2018
                    if (cbBonusSpin2.Checked || cbBonusSpin1.Checked)
                        lv.Items[currentElem].SubItems[7].Text += "B ";
                    if (percentuale > 0)
                        lv.Items[currentElem].SubItems[7].Text += "+ " + percentuale + " %";
                    //else lv.Items[currentElem].SubItems[7].Text += "";

                    lv.Items[currentElem].ToolTipText = ((DataRow)tr.Tag)[1].ToString();
                    lv.Items[currentElem].ForeColor = rigaSpin;
                    // aggiungo un elemento trottola singola
                    if (!Definizioni.updating)
                    {
                        progEl++;
                        Definizioni.elemSegment[9] = int.Parse(Definizioni.elemSegment[9].ToString()) + 1;
                        Definizioni.elemSegment[0] = int.Parse(Definizioni.elemSegment[0].ToString()) + 1;
                        Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) + 1;
                    }
                    else
                    {
                        if (typeTrottola.StartsWith("CombSpin")) // se la trottola che aggiorno è combo
                        {
                            // aggiungo una trottola combinata...
                            Definizioni.elemSegment[10] = int.Parse(Definizioni.elemSegment[10].ToString()) + 1;
                            // ...e tolgo una trottola singola
                            Definizioni.elemSegment[9] = int.Parse(Definizioni.elemSegment[9].ToString()) - 1;
                        }

                    }
                }

                // *** modifica del 10/07/2018 - verifico i salti duplicati *** //
                if (RollartSystemTech.Properties.Settings.Default.CheckDuplicates)
                {
                    // verifico i salti duplicati
                    CheckDuplicati(lv.Items[currentElem]);
                }
                // *** Modifica del 10/9/2018 ***//
                //if (!Definizioni.updating) 
                //{
                //    if (addAsterisco) lv.Items[currentElem].SubItems[7].Text = " (*)";
                //    else lv.Items[currentElem].SubItems[7].Text = "";
                //}
                if (addAsterisco) lv.Items[currentElem].SubItems[7].Text = "(*)";
                // *** Fine Modifica del 10/9/2018 ***//

                if (tr == el65)
                {
                    //lv.Items[currentElem].SubItems[2].Text = "0,00";
                    lv.Items[currentElem].SubItems[2].Text = String.Format("{0:0.00}", valoreTrottola);
                    lv.Items[currentElem].SubItems[6].Text = "";
                }

                lv.Items[currentElem].Selected = true;

                if (numComboSpin == 1)
                {
                    if (!Definizioni.updating)
                        lv.Items[currentElem].Text = progEl + "";
                    // invio al giudice solo la ptrima trottola per trottole combo
                    if (Utility.InvioDati(lv, log))
                        Utility.ScrivoElementoOnDB(lv, log, false);
                }
                else
                {
                    Utility.ScrivoElementoOnDB(lv, log, false);
                }

                // Aggiorno i totali
                if (!addAsterisco) Utility.AggiornoTotali(log, lv, deductions, elements, total);

                if (comboPercentuale != null)
                    comboPercentuale.SelectedIndex = 0;
                ResetAfterInsertion();
            }
            catch (Exception ex)
            {
                Cursor.Show();
                Utility.WriteLog("InsertTrottola: " + ex.Message, "ERROR");
            }
        }

        #endregion

        #region SALTI, TROTTOLE, PASSI
        /**********************************************************
         *  No Jump - Failed Jump
         * ********************************************************/
        private void noj_Click(object sender, EventArgs e)
        {
            InsertElement(true, el64, null, ((Control)sender).Parent.Tag, false);
        }

        private void fail_Click(object sender, EventArgs e)
        {
            InsertElement(false, el64, null, ((Control)sender).Parent.Tag, true);
        }

        /**********************************************************
         *  Axel
         * ********************************************************/
        private void axel1_Click(object sender, EventArgs e)
        {
            //InsertElement(false, el6, null, ((Control)sender).Parent.Tag, false);
            InsertElement(false, el6, el0, ((Control)sender).Parent.Tag, false);
        }
        private void axel2_Click(object sender, EventArgs e)
        {
            InsertElement(false, el12, el6, ((Control)sender).Parent.Tag, false);
        }
        private void axel3_Click(object sender, EventArgs e)
        {
            InsertElement(false, el18, el12,
                ((Control)sender).Parent.Tag, false);
        }
        private void axel4_Click(object sender, EventArgs e)
        {
            InsertElement(false, el24, el18,
                ((Control)sender).Parent.Tag, false);
        }

        /**********************************************************
         *  Toeloop
         * ********************************************************/
        private void toeloop1_Click(object sender, EventArgs e)
        {
            InsertElement(true, el1, null,
                ((Control)sender).Parent.Tag, false);
        }
        private void toeloop2_Click(object sender, EventArgs e)
        {
            InsertElement(false, el7, el1,
                ((Control)sender).Parent.Tag, false);
        }
        private void toeloop3_Click(object sender, EventArgs e)
        {
            InsertElement(false, el14, el7,
                ((Control)sender).Parent.Tag, false);
        }
        private void toeloop4_Click(object sender, EventArgs e)
        {
            InsertElement(false, el20, el14,
                ((Control)sender).Parent.Tag, false);
        }

        /**********************************************************
        *  Salchow
        * ********************************************************/
        private void sal1_Click(object sender, EventArgs e)
        {
            InsertElement(true, el2, null,
                ((Control)sender).Parent.Tag, false);
        }
        private void sal2_Click(object sender, EventArgs e)
        {
            InsertElement(false, el8, el2,
                ((Control)sender).Parent.Tag, false);
        }
        private void sal3_Click(object sender, EventArgs e)
        {
            InsertElement(false, el13, el8,
                ((Control)sender).Parent.Tag, false);
        }
        private void sal4_Click(object sender, EventArgs e)
        {
            InsertElement(false, el19, el13,
                ((Control)sender).Parent.Tag, false);
        }

        /**********************************************************
        *  Flip
        * ********************************************************/
        private void flip1_Click(object sender, EventArgs e)
        {
            InsertElement(true, el3, null,
                ((Control)sender).Parent.Tag, false);
        }
        private void flip2_Click(object sender, EventArgs e)
        {
            InsertElement(false, el9, el3,
                ((Control)sender).Parent.Tag, false);
        }
        private void flip3_Click(object sender, EventArgs e)
        {
            InsertElement(false, el15, el9,
                   ((Control)sender).Parent.Tag, false);
        }
        private void flip4_Click(object sender, EventArgs e)
        {
            InsertElement(false, el22, el15,
                ((Control)sender).Parent.Tag, false);
        }

        /**********************************************************
        *  Lutz
        * ********************************************************/
        private void lutz1_Click(object sender, EventArgs e)
        {
            InsertElement(true, el4, null,
                ((Control)sender).Parent.Tag, false);
        }
        private void lutz2_Click(object sender, EventArgs e)
        {
            InsertElement(false, el10, el4,
                ((Control)sender).Parent.Tag, false);
        }
        private void lutz3_Click(object sender, EventArgs e)
        {
            InsertElement(false, el16, el10,
                ((Control)sender).Parent.Tag, false);
        }
        private void lutz4_Click(object sender, EventArgs e)
        {
            InsertElement(false, el23, el16,
                ((Control)sender).Parent.Tag, false);
        }

        /**********************************************************
        *  Ritt
        * ********************************************************/
        private void ritt1_Click(object sender, EventArgs e)
        {
            InsertElement(true, el5, null,
                ((Control)sender).Parent.Tag, false);
        }
        private void ritt2_Click(object sender, EventArgs e)
        {
            InsertElement(false, el11, el5,
                ((Control)sender).Parent.Tag, false);
        }
        private void ritt3_Click(object sender, EventArgs e)
        {
            InsertElement(false, el17, el11,
                ((Control)sender).Parent.Tag, false);
        }
        private void ritt4_Click(object sender, EventArgs e)
        {
            InsertElement(false, el21, el17,
                ((Control)sender).Parent.Tag, false);
        }

        // Waltz Jump
        private void el0__Click(object sender, EventArgs e)
        {
            //Button waltzJump = (Button)sender;
            //bool comboWalz = false;
            //if (waltzJump.Name.Equals("el0_")) comboWalz = true;
            InsertElement(true, el0, null, ((Control)sender).Parent.Tag, false);
        }

        // downgraded-underotated-halfrotated
        private void axelD_Click(object sender, EventArgs e)
        {
            if (Definizioni.downJump)
            {
                Definizioni.downJump = false; down.BackColor = Color.Silver;
                Definizioni.underJump = false; under.BackColor = Color.Silver;
                Definizioni.halfJump = false; half.BackColor = Color.Silver;
            }
            else
            {
                Definizioni.downJump = true; down.BackColor = Color.Orange;
                Definizioni.underJump = false; under.BackColor = Color.Silver;
                Definizioni.halfJump = false; half.BackColor = Color.Silver;
            }
        }
        private void axelT_Click(object sender, EventArgs e)
        {
            if (Definizioni.underJump)
            {
                Definizioni.downJump = false; down.BackColor = Color.Silver;
                Definizioni.underJump = false; under.BackColor = Color.Silver;
                Definizioni.halfJump = false; half.BackColor = Color.Silver;
            }
            else
            {
                Definizioni.downJump = false; down.BackColor = Color.Silver;
                Definizioni.underJump = true; under.BackColor = Color.Orange;
                Definizioni.halfJump = false; half.BackColor = Color.Silver;
            }

        }

        private void half_Click(object sender, EventArgs e)
        {
            if (Definizioni.halfJump)
            {
                Definizioni.downJump = false; down.BackColor = Color.Silver;
                Definizioni.underJump = false; under.BackColor = Color.Silver;
                Definizioni.halfJump = false; half.BackColor = Color.Silver;
            }
            else
            {
                Definizioni.downJump = false; down.BackColor = Color.Silver;
                Definizioni.underJump = false; under.BackColor = Color.Silver;
                Definizioni.halfJump = true; half.BackColor = Color.Orange;
            }
        }

        private void under2_Click(object sender, EventArgs e)
        {
            if (Definizioni.underJump)
            {
                Definizioni.downJump = false; down2.BackColor = Color.Silver;
                Definizioni.underJump = false; under2.BackColor = Color.Silver;
                Definizioni.halfJump = false; half2.BackColor = Color.Silver;
            }
            else
            {
                Definizioni.downJump = false; down2.BackColor = Color.Silver;
                Definizioni.underJump = true; under2.BackColor = Color.Orange;
                Definizioni.halfJump = false; half2.BackColor = Color.Silver;
            }
        }

        private void down2_Click(object sender, EventArgs e)
        {
            if (Definizioni.downJump)
            {
                Definizioni.downJump = false; down2.BackColor = Color.Silver;
                Definizioni.underJump = false; under2.BackColor = Color.Silver;
                Definizioni.halfJump = false; half2.BackColor = Color.Silver;
            }
            else
            {
                Definizioni.downJump = true; down2.BackColor = Color.Orange;
                Definizioni.underJump = false; under2.BackColor = Color.Silver;
                Definizioni.halfJump = false; half2.BackColor = Color.Silver;
            }
        }

        private void half2_Click(object sender, EventArgs e)
        {
            if (Definizioni.halfJump)
            {
                Definizioni.downJump = false; down2.BackColor = Color.Silver;
                Definizioni.underJump = false; under2.BackColor = Color.Silver;
                Definizioni.halfJump = false; half2.BackColor = Color.Silver;
            }
            else
            {
                Definizioni.downJump = false; down2.BackColor = Color.Silver;
                Definizioni.underJump = false; under2.BackColor = Color.Silver;
                Definizioni.halfJump = true; half2.BackColor = Color.Orange;
            }
        }

        /**********************************************************
        *  No Spin
        * ********************************************************/
        private void ns_Click(object sender, EventArgs e)
        {
            InsertTrottola(el65);
        }
        /**********************************************************
        *  Upright Spin
        * ********************************************************/
        private void us1_Click(object sender, EventArgs e)
        {
            InsertTrottola(el25);
        }

        /**********************************************************
        *  Sit Spin
        * ********************************************************/
        private void ss1_Click(object sender, EventArgs e)
        {
            InsertTrottola(el40);
        }

        /**********************************************************
        *  Camel Spin
        * ********************************************************/
        private void cs1_Click(object sender, EventArgs e)
        {
            InsertTrottola(el45);
        }
        private void he3_Click(object sender, EventArgs e)
        {
            InsertTrottola(el48);
        }
        private void br3_Click(object sender, EventArgs e)
        {
            InsertTrottola(el47);
        }
        private void in3_Click(object sender, EventArgs e)
        {
            InsertTrottola(el49);
        }

        private void el26_Click(object sender, EventArgs e)
        {
            Button spinNoLevel = (Button)sender;
            InsertTrottola(spinNoLevel);
        }


        /**********************************************************
        *  Steps
        * ********************************************************/
        private void s0_Click(object sender, EventArgs e)
        {
            InsertPassi(el76, lv, log, deductions, elements, total, back, error);
        }

        private void s1_Click(object sender, EventArgs e)
        {
            InsertPassi(el59, lv, log, deductions, elements, total, back, error);
        }
        private void s2_Click(object sender, EventArgs e)
        {
            InsertPassi(el60, lv, log, deductions, elements, total, back, error);
        }
        private void s3_Click(object sender, EventArgs e)
        {
            InsertPassi(el61, lv, log, deductions, elements, total, back, error);
        }
        private void s4_Click(object sender, EventArgs e)
        {
            InsertPassi(el62, lv, log, deductions, elements, total, back, error);
        }
        private void s5_Click(object sender, EventArgs e)
        {
            InsertPassi(el63, lv, log, deductions, elements, total, back, error);
        }

        private void el73_Click(object sender, EventArgs e)
        {
            InsertPassi(el73, lv, log, deductions, elements, total, back, error);
        }

        #endregion

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                double durata = Convert.ToDouble(Definizioni.paramSegment[4]);

                time = time.Add(TimeSpan.FromMilliseconds(1000));
                ltimer.Text = string.Format("{0:00}:{1:00}", time.Minutes,
                    time.Seconds);

                if (time.TotalSeconds < (durata - tolleranza))
                {
                    // lascio lo stesso colore
                } else if (time.TotalSeconds >= (durata - tolleranza) &&
                           time.TotalSeconds <= (durata + tolleranza))
                {
                    ltimer.ForeColor = Color.Lime;
                } else if (time.TotalSeconds > (durata + tolleranza))// oltre la tolleranza
                {
                    ltimer.ForeColor = Color.Red;
                }


                // 2.0.1 Dopo 5 secondi invio BLANK al Video
                if (time.TotalSeconds == 5)
                    Utility.Video_BlankCommand();

            }
            catch (Exception ex)
            {
                Utility.WriteLog("Timer1_Tick: " + ex.Message, "ERROR");
            }
        }

        #region DEDUCTIONS
        /**********************************************************
         *  DEDUCTIONS
         * ********************************************************/
        private void ManageDeductions(NumericUpDown control)
        {
            try
            {
                deductions.Text = "-" + (ded1.Value + ded2.Value + ded3.Value + ded4.Value + ded5.Value + ded6.Value).ToString("F");
                Definizioni.deductionsDec = decimal.Parse(deductions.Text);
                total.Text = String.Format("{0:0.00}", Definizioni.elementsDec + Definizioni.deductionsDec);
            }
            catch (Exception ex)
            {
                Utility.WriteLog("ManageDeductions: " + ex.Message, "ERROR");
            }
        }

        private void ded1_ValueChanged(object sender, EventArgs e)
        {
            ManageDeductions(ded1);
        }
        private void ded4_ValueChanged(object sender, EventArgs e)
        {
            ManageDeductions(ded4);
        }
        private void ded2_ValueChanged(object sender, EventArgs e)
        {
            ManageDeductions(ded2);
        }
        private void ded5_ValueChanged(object sender, EventArgs e)
        {
            ManageDeductions(ded5);
        }
        private void ded3_ValueChanged(object sender, EventArgs e)
        {
            ManageDeductions(ded3);
        }

        private void ded6_ValueChanged(object sender, EventArgs e)
        {
            ManageDeductions(ded6);
        }

        private void next_Click_1(object sender, EventArgs e)
        {
            NewSegment();
        }
        #endregion

        #region CONTROLLI
        public void DisableGroupBox()
        {
            gbJumps.Enabled = false; gbSpins.Enabled = false;
            gbComboJumps.Enabled = false; gbDed.Enabled = false;
            gbSteps.Enabled = false;
            //gbElements.Enabled = false;
        }

        public void EnableGroupBox()
        {
            gbJumps.Enabled = true; gbSpins.Enabled = true;
            gbComboJumps.Enabled = true; gbDed.Enabled = true;
            gbSteps.Enabled = true;
            //gbElements.Enabled = true;
        }

        private void lv_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = lv.Columns[e.ColumnIndex].Width;
        }

        // Blocco la larghezza delle colonne
        private void lv_ColumnWidthChanging_1(object sender, ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = lv.Columns[e.ColumnIndex].Width;
        }

        private void lv_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
                e.Item.Selected = false;
        }

        private void Tooltips(Button j)
        {
            tt.ToolTipTitle = j.Name;
            if (j.BackColor == Color.Yellow)
                tt.SetToolTip(j, Definizioni.resources["waiting"].ToString());
            else if (j.BackColor == Color.Transparent && j.ForeColor == Color.Lime)
            {
                tt.SetToolTip(j, Definizioni.resources["connected"].ToString() + "\r\n");
                tt.ForeColor = Color.Black;
                tt.ToolTipIcon = ToolTipIcon.Info;
            }
            else if (j.BackColor == Color.Transparent && j.ForeColor == Color.Red)
            {
                tt.SetToolTip(j, Definizioni.resources["notconnected"].ToString() + "\r\n");
                tt.ForeColor = Color.Black;
                tt.ToolTipIcon = ToolTipIcon.Warning;
            }
            if (j.Font.Underline)
            {
                tt.SetToolTip(j, "Ping timeout");
                tt.ForeColor = Color.Black;
                tt.ToolTipIcon = ToolTipIcon.Warning;
            }
        }

        private void j1_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j1);
        }

        private void j2_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j2);
        }

        private void j3_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j3);
        }

        private void j4_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j4);
        }

        private void j5_Click(object sender, EventArgs e)
        {
            Tooltips(j5);
        }

        private void j6_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j6);
        }

        private void j7_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j7);
        }

        private void j8_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j8);
        }

        private void j9_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j9);
        }

        private void lv_MouseClick(object sender, MouseEventArgs e)
        {
            if (lv.SelectedItems.Count != 0)
            {
                review.Enabled = true;
                asterisco.Enabled = true;
                buttonT.Enabled = true;
            }
            else
            {
                review.Enabled = false;
                asterisco.Enabled = false;
                buttonT.Enabled = false;
            }
        }

        private void openDed_Click(object sender, EventArgs e)
        {
            if (gbDed.Visible)
            {
                gbDed.Visible = false;
            }
            else gbDed.Visible = true;
        }

        private void lv_MouseDown(object sender, MouseEventArgs e)
        {
            ListViewItem selection = lv.GetItemAt(e.X, e.Y);

            if (selection != null)
            {
                //edit.Enabled = true;
            }
            else
            {
                //edit.Enabled = false;
            }
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            Info inf = new Info();
            inf.ShowDialog();
        }

        private void checkCombo2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkCombo2.Checked) // trottola combo
                cbCombo.Enabled = true;
            else cbCombo.Enabled = false;
        }

        // REVIEW
        private void edit_Click(object sender, EventArgs e)
        {
            try
            {
                if (lv.SelectedItems[0] == null) return;
                int indexSelectedItem = lv.SelectedItems[0].Index;

                if (lv.Items[indexSelectedItem].ImageIndex == 2) // già in Review
                {
                    lv.Items[indexSelectedItem].ImageIndex = 0;
                    lv.Items[indexSelectedItem].ToolTipText =
                        lv.Items[indexSelectedItem].ToolTipText.Split('-')[0];
                } else  
                {
                    lv.Items[indexSelectedItem].ImageIndex = 2;
                    lv.Items[indexSelectedItem].ToolTipText =
                        lv.Items[indexSelectedItem].ToolTipText + " - REVIEW";
                }
                review.Enabled = false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Review: " + ex.Message, "ERROR");
            }
        }

        // Thouren
        private void el27_Click(object sender, EventArgs e)
        {
            InsertElement(true, el27, null,
                ((Control)sender).Parent.Tag, false);
        }

        private void el28_Click(object sender, EventArgs e)
        {
            InsertElement(false, el28, el27,
                ((Control)sender).Parent.Tag, false);
        }

        private void el29_Click(object sender, EventArgs e)
        {
            InsertElement(false, el29, el27,
                ((Control)sender).Parent.Tag, false);
        }

        private void el30_Click(object sender, EventArgs e)
        {
            InsertElement(false, el30, el27,
                ((Control)sender).Parent.Tag, false);
        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void gbDed_Enter(object sender, EventArgs e)
        {

        }

        private void j5_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j5);
        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        // add T 
        //decimal[] arrayValues = new decimal[30];
        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                if (lv.SelectedItems[0] == null) return;

                // applico solo ai salti, no ai NJ, no allo short, no asteriscato
                if (!lv.SelectedItems[0].SubItems[4].Text.Equals("J") ||
                    lv.SelectedItems[0].SubItems[2].Text.Equals("0,00") ||
                    lv.SelectedItems[0].SubItems[2].Text.Equals("0.00") ||
                    lv.SelectedItems[0].SubItems[7].Text.Contains("(*)") || 
                    Definizioni.idSegment == 1) return;

                int indexSelectedItem = lv.SelectedItems[0].Index;

                if (lv.Items[indexSelectedItem].SubItems[8].Text.Contains("T")) // già con bonus
                {

                    //lv.SelectedItems[0].SubItems[2].Text = 
                    //    arrayValues[lv.SelectedItems[0].Index] + ""; // - 10%
                    string valBefore = lv.SelectedItems[0].SubItems[2].Text;
                    decimal dec = decimal.Parse(valBefore) * 10;
                    lv.SelectedItems[0].SubItems[2].Text = Math.Round((dec / 11), 2) + ""; // - 10%
                    lv.SelectedItems[0].SubItems[8].Text = "";
                }
                else
                {
                    string valBefore = lv.SelectedItems[0].SubItems[2].Text;
                    decimal dec = decimal.Parse(valBefore);
                    lv.SelectedItems[0].SubItems[2].Text =
                        dec + Math.Round(((dec * 10) / 100), 2) + ""; // + 10%
                    lv.SelectedItems[0].SubItems[8].Text = "T";
                    // mi salvo il valore nell'array
                    //arrayValues[lv.SelectedItems[0].Index] = dec;
                }

                Utility.AggiornoTotali(log, lv, deductions, elements, total);

            }
            catch (Exception ex)
            {
                Utility.WriteLog("Add bonus: " + ex.Message, "ERROR");
            }
        }

        private void prev_Click(object sender, EventArgs e)
        {
            if (Definizioni.currentPart == 1) return;
            DialogResult result = MessageBox.Show("Display the previous skater scores?", "Warning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                Definizioni.currentPart = Definizioni.currentPart - 1;
                ut.DisplayScore();
                Definizioni.currentPart = Definizioni.currentPart + 1;
            }
        }

        private void SingoloForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // *** Modifica del 05/12/2018 - 2.0.0.9 *** //
            if (!flagClose)
                e.Cancel = true;
        }

        private void cbBonusSpin2_CheckedChanged(object sender, EventArgs e)
        {
            if (cbBonusSpin1.Checked)
                cbBonusSpin2.Checked = false;
        }

        private void cbBonusSpin2_CheckedChanged_1(object sender, EventArgs e)
        {
            if (cbBonusSpin2.Checked)
                cbBonusSpin1.Checked = false;
        }

        // add asterisco
        private void asterisco_Click(object sender, EventArgs e)
        {
            try
            {
                if (lv.SelectedItems[0] == null) return;
                int indexSelectedItem = lv.SelectedItems[0].Index;

                if (lv.Items[indexSelectedItem].SubItems[7].Text.Contains("(*)")) // già asteriscato
                {
                    lv.Items[indexSelectedItem].SubItems[7].Text =
                        lv.Items[indexSelectedItem].SubItems[7].Text.Replace("(*)", ""); // elimino asterisco
                    Utility.UpdateElementConAsterisco(indexSelectedItem + 1, false);
                }
                else
                {
                    lv.Items[indexSelectedItem].SubItems[7].Text = "(*)";
                    Utility.UpdateElementConAsterisco(indexSelectedItem + 1, true);
                }
                asterisco.Enabled = false;

                Utility.AggiornoTotali(log, lv, deductions, elements, total);
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Add *: " + ex.Message, "ERROR");
            }
        }

        private void j1_Click(object sender, EventArgs e)
        {
            Utility.SendToSingleJudge("<HURRYUP/>", int.Parse(((Button)sender).Tag.ToString()));
        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }


        // VERIFY SEGMENT
        private void cbVerify_CheckedChanged(object sender, EventArgs e)
        {
            if (cbVerify.Checked)
            {
                cbVerify.FlatAppearance.MouseOverBackColor = Color.Lime;
            }
            else cbVerify.FlatAppearance.MouseOverBackColor = Color.Gainsboro;
        }

        private void newComboSpin_Click(object sender, EventArgs e)
        {
            try
            {
                Definizioni.elemSegment[16] = 0;

            }
            catch (Exception ex)
            {
                Utility.WriteLog("NewComboSpin: " + ex.Message, "ERROR");
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml;
using System.Globalization;

namespace RollartSystemTech
{
    public partial class Average : Form
    {
        XElement xmlElements = null;
        string[] averageArray = null;
        bool flagClose = false;

        public Average()
        {
            InitializeComponent();
            
        }

        private void Average_Load(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < Definizioni.numJudges; i++) lvComp.Columns.Add("J" + (i + 1), 65);
                lvComp.Columns.Add("Average", 100);
                lvComp.Columns[lvComp.Columns.Count - 1].Name = "Average";
                int countRiga = 0;

                // inserisco i components di tutti i giudici
                //foreach (string element in Definizioni.compRicevuti)
                foreach (string element in Definizioni.qoeRicevuti)
                {
                    xmlElements = XElement.Parse(element);

                    int subCount = lvComp.Items[0].SubItems.Count;
                    for (int i = 0; i < 4; i++)
                    {
                        lvComp.Items[i].SubItems.Add(xmlElements.Attribute("comp" + (i + 1)).Value);//.Replace(".", ","));
                        lvComp.Items[i].UseItemStyleForSubItems = false;
                        lvComp.Items[i].SubItems[2 + countRiga].ForeColor = Color.Yellow;
                    }
                    countRiga++;
                }

                averageArray = new string[4];
                // faccio la media
                for (int i = 0; i < 4; i++)
                {
                    decimal average = 0;
                    for (int k = 0; k < Definizioni.numJudges; k++)
                    {
                        if (!lvComp.Items[i].SubItems[2 + k].Text.Equals("")) // 2.0 per danze obbligatorie il secondo e quarto component e'pari a blank
                            average += Decimal.Parse(lvComp.Items[i].SubItems[2 + k].Text,
                            NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.InvariantCulture);
                        //average += decimal.Parse(lvComp.Items[i].SubItems[2 + k].Text);//.Replace('.',','));
                    }
                    average = average / Definizioni.numJudges;
                    average = Decimal.Ceiling(average * 4m) / 4m;

                    //lvComp.Items[i].SubItems.Add(String.Format("{0:0.00}", average));
                    if (average != 0)
                    {
                        lvComp.Items[i].SubItems.Add(average.ToString("0.00", CultureInfo.InvariantCulture));
                        averageArray[i] = (average.ToString("0.00", CultureInfo.InvariantCulture));
                        int count = lvComp.Items[i].SubItems.Count;
                        lvComp.Items[i].SubItems[count - 1].Tag = "Average" + (i + 1);
                        lvComp.Items[i].SubItems[count - 1].Name = "Average" + (i + 1);
                        lvComp.Items[i].SubItems[count - 1].ForeColor = Color.White;
                    } else
                    {
                        lvComp.Items[i].SubItems.Add("");
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void confirm_Click(object sender, EventArgs e)
        {
            lvComp.LabelEdit = false;
            lvComp.ForeColor = Color.Lime;
            send.Enabled = true;
            confirm.Enabled = false;
            flowLayoutPanel1.Enabled = false;
            for (int i=0; i<4; i++)
                lvComp.Items[i].SubItems[lvComp.Items[i].SubItems.Count - 1].ForeColor = Color.Lime;
            //lvComp.Enabled = false;
        }

        private void lvComp_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (!send.Enabled)
            {
                // Get the item at the mouse pointer.
                ListViewHitTestInfo info = lvComp.HitTest(e.X, e.Y);
                ListViewItem.ListViewSubItem subItem = null;
                if (info != null)
                    if (info.Item != null)
                        subItem = info.Item.GetSubItemAt(e.X, e.Y);
                if (subItem != null && subItem.Tag != null)
                {
                    if (subItem.Tag.ToString().StartsWith("Average"))
                    {

                    }
                }
            }
        }

        private void send_Click(object sender, EventArgs e)
        {
            try
            {
                // *** Modifica del 05/12/2018 - 2.0.0.8 *** //
                if (lvComp.Items[1].SubItems[lvComp.Columns.Count - 1].Text.Equals(""))
                    lvComp.Items[1].SubItems[lvComp.Columns.Count - 1].Text = "0.00";
                if (lvComp.Items[3].SubItems[lvComp.Columns.Count - 1].Text.Equals(""))
                    lvComp.Items[3].SubItems[lvComp.Columns.Count - 1].Text = "0.00";

                string averageComp = "<COMPONENTS " +
                                     "comp1 = '" + lvComp.Items[0].SubItems[lvComp.Columns.Count - 1].Text + "' " +
                                     "comp2 = '" + lvComp.Items[1].SubItems[lvComp.Columns.Count - 1].Text + "' " +
                                     "comp3 = '" + lvComp.Items[2].SubItems[lvComp.Columns.Count - 1].Text + "' " +
                                     "comp4 = '" + lvComp.Items[3].SubItems[lvComp.Columns.Count - 1].Text + "'/>";

                Utility.SendBroadcast(averageComp);

                // *** Modifica del 05/12/2018 - 2.0.0.9 *** //
                flagClose = true;
                Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Aumenta(int i)
        {
            try
            {
                //if (decimal.Parse(lvComp.Items[i].SubItems[2 + Definizioni.numJudges].Text).Equals(10)) return;
                if (lvComp.Items[i].SubItems[2 + Definizioni.numJudges].Text.Equals("")) return;
                decimal value = Decimal.Parse(lvComp.Items[i].SubItems[2 + Definizioni.numJudges].Text,
                            NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.InvariantCulture) + 0.25m;

                if ((value - 0.25m) == 10) return;

                lvComp.Items[i].SubItems[2 + Definizioni.numJudges].Text = value.ToString("0.00", CultureInfo.InvariantCulture);

                lvComp.Items[i].SubItems[2 + Definizioni.numJudges].ForeColor = Color.Red;
                if (Decimal.Parse(lvComp.Items[i].SubItems[2 + Definizioni.numJudges].Text,
                                NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.InvariantCulture) ==
                    Decimal.Parse(averageArray[i],
                                NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.InvariantCulture))
                    lvComp.Items[i].SubItems[2 + Definizioni.numJudges].ForeColor = Color.White;
                else lvComp.Items[i].SubItems[2 + Definizioni.numJudges].ForeColor = Color.Red;
            }

            catch (Exception)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void Diminuisci(int i)
        {
            try
            {
                if (lvComp.Items[i].SubItems[2 + Definizioni.numJudges].Text.Equals("")) return;
                decimal value = Decimal.Parse(lvComp.Items[i].SubItems[2 + Definizioni.numJudges].Text,
                                NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.InvariantCulture);

                if (value == 0.25m) return;

                lvComp.Items[i].SubItems[2 + Definizioni.numJudges].Text = (value - 0.25m).ToString("0.00", CultureInfo.InvariantCulture);

                if (Decimal.Parse(lvComp.Items[i].SubItems[2 + Definizioni.numJudges].Text,
                                NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.InvariantCulture) ==
                    Decimal.Parse(averageArray[i],
                                NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.InvariantCulture))
                    lvComp.Items[i].SubItems[2 + Definizioni.numJudges].ForeColor = Color.White;
                else lvComp.Items[i].SubItems[2 + Definizioni.numJudges].ForeColor = Color.Red;
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        // Skating Skills
        private void button1_Click(object sender, EventArgs e)
        {
            Aumenta(0);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Diminuisci(0);
        }

        // Transitions
        private void button3_Click(object sender, EventArgs e)
        {
            Aumenta(1);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Diminuisci(1);
        }

        // Performance
        private void button5_Click(object sender, EventArgs e)
        {
            Aumenta(2);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Diminuisci(2);
        }

        // Choreo
        private void button7_Click(object sender, EventArgs e)
        {
            Aumenta(3);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Diminuisci(3);
        }

        private void lvComp_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = lvComp.Columns[e.ColumnIndex].Width;
        }

        // reset
        private void reset_Click(object sender, EventArgs e)
        {
            lvComp.LabelEdit = true;
            lvComp.ForeColor = Color.Lime;
            send.Enabled = false;
            confirm.Enabled = true;
            flowLayoutPanel1.Enabled = true;
            for (int i = 0; i < 4; i++)
            {
                lvComp.Items[i].SubItems[lvComp.Items[i].SubItems.Count - 1].Text = averageArray[i];
                //lvComp.Items[i].SubItems[lvComp.Items[i].SubItems.Count - 1].BackColor = Color.Gray;
                lvComp.Items[i].SubItems[lvComp.Items[i].SubItems.Count - 1].ForeColor = Color.White;
            }
        }

        private void Average_FormClosing(object sender, FormClosingEventArgs e)
        {
            // *** Modifica del 05/12/2018 - 2.0.0.9 *** //
            if (!flagClose)
                e.Cancel = true;
        }
    }
}
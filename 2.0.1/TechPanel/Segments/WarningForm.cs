﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RollartSystemTech.Segments
{
    public partial class WarningForm : Form
    {
        int sit = 0; // sit mancante
        
        public WarningForm(int comboToRemove)
        {
            InitializeComponent();
            sit = comboToRemove;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Definizioni.confirm = false;
            Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Definizioni.confirm = true;
            Dispose();
        }

        private void WarningForm_Load(object sender, EventArgs e)
        {
            try
            {
                tb.Text = Definizioni.warningMessage;
                if (sit > 0)
                {
                    label2.Text = "Sit Spin not executed. ComboSpin removed.";
                    label2.Visible = true;
                }
                //else if (sit == 0)
                //{
                //    label2.Text = "Sit Spin not executed.";
                //    label2.Visible = true;
                //}
                else
                {
                    label2.Text = "";
                    label2.Visible = false;
                }
            } catch (Exception ex)
            {
                Utility.WriteLog("WarningForm_Load: " + ex.Message, "ERROR");
            }
        }
    }
}

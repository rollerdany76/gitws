﻿using System;

namespace RollartSystemTech
{
    partial class SingoloForm
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SingoloForm));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.j1 = new System.Windows.Forms.Button();
            this.j2 = new System.Windows.Forms.Button();
            this.j3 = new System.Windows.Forms.Button();
            this.j4 = new System.Windows.Forms.Button();
            this.j5 = new System.Windows.Forms.Button();
            this.j6 = new System.Windows.Forms.Button();
            this.j7 = new System.Windows.Forms.Button();
            this.j8 = new System.Windows.Forms.Button();
            this.j9 = new System.Windows.Forms.Button();
            this.referee = new System.Windows.Forms.Button();
            this.startstop = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.ltimer = new System.Windows.Forms.Button();
            this.confirm = new System.Windows.Forms.Button();
            this.average = new System.Windows.Forms.Button();
            this.reset = new System.Windows.Forms.Button();
            this.prev = new System.Windows.Forms.Button();
            this.skip = new System.Windows.Forms.Button();
            this.info = new System.Windows.Forms.Button();
            this.quit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.gbElements = new System.Windows.Forms.GroupBox();
            this.lv = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.note = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.magg = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.error = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cbVerify = new System.Windows.Forms.CheckBox();
            this.review = new System.Windows.Forms.Button();
            this.asterisco = new System.Windows.Forms.Button();
            this.buttonT = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonNext = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.total = new System.Windows.Forms.Label();
            this.deductions = new System.Windows.Forms.Label();
            this.elements = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.openDed = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.labelPartial = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gbDed = new System.Windows.Forms.GroupBox();
            this.ded6 = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.ded5 = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.ded3 = new System.Windows.Forms.NumericUpDown();
            this.ded4 = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.ded2 = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.gbComboJumps = new System.Windows.Forms.GroupBox();
            this.el0_ = new System.Windows.Forms.Button();
            this.half2 = new System.Windows.Forms.Button();
            this.el64__ = new System.Windows.Forms.Button();
            this.newCombo = new System.Windows.Forms.Button();
            this.el64_ = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.under2 = new System.Windows.Forms.Button();
            this.down2 = new System.Windows.Forms.Button();
            this.ritt44 = new System.Windows.Forms.Button();
            this.lutz44 = new System.Windows.Forms.Button();
            this.flip44 = new System.Windows.Forms.Button();
            this.sal44 = new System.Windows.Forms.Button();
            this.toeloop44 = new System.Windows.Forms.Button();
            this.axel44 = new System.Windows.Forms.Button();
            this.ritt33 = new System.Windows.Forms.Button();
            this.ritt22 = new System.Windows.Forms.Button();
            this.ritt11 = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.lutz33 = new System.Windows.Forms.Button();
            this.lutz22 = new System.Windows.Forms.Button();
            this.lutz11 = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.flip33 = new System.Windows.Forms.Button();
            this.flip22 = new System.Windows.Forms.Button();
            this.flip11 = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.sal33 = new System.Windows.Forms.Button();
            this.sal22 = new System.Windows.Forms.Button();
            this.sal11 = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.toeloop33 = new System.Windows.Forms.Button();
            this.toeloop22 = new System.Windows.Forms.Button();
            this.toeloop11 = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.axel33 = new System.Windows.Forms.Button();
            this.axel22 = new System.Windows.Forms.Button();
            this.axel11 = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.gbJumps = new System.Windows.Forms.GroupBox();
            this.el30 = new System.Windows.Forms.Button();
            this.el29 = new System.Windows.Forms.Button();
            this.el28 = new System.Windows.Forms.Button();
            this.el27 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.el0 = new System.Windows.Forms.Button();
            this.half = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.under = new System.Windows.Forms.Button();
            this.down = new System.Windows.Forms.Button();
            this.el21 = new System.Windows.Forms.Button();
            this.el23 = new System.Windows.Forms.Button();
            this.el22 = new System.Windows.Forms.Button();
            this.el19 = new System.Windows.Forms.Button();
            this.el20 = new System.Windows.Forms.Button();
            this.el24 = new System.Windows.Forms.Button();
            this.el17 = new System.Windows.Forms.Button();
            this.el11 = new System.Windows.Forms.Button();
            this.el5 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.el16 = new System.Windows.Forms.Button();
            this.el10 = new System.Windows.Forms.Button();
            this.el4 = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.el15 = new System.Windows.Forms.Button();
            this.el9 = new System.Windows.Forms.Button();
            this.el3 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.el13 = new System.Windows.Forms.Button();
            this.el8 = new System.Windows.Forms.Button();
            this.el2 = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.el14 = new System.Windows.Forms.Button();
            this.el7 = new System.Windows.Forms.Button();
            this.el1 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.el18 = new System.Windows.Forms.Button();
            this.el12 = new System.Windows.Forms.Button();
            this.el6 = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.el64 = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.gbSteps = new System.Windows.Forms.GroupBox();
            this.el73 = new System.Windows.Forms.Button();
            this.el76 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.el63 = new System.Windows.Forms.Button();
            this.el59 = new System.Windows.Forms.Button();
            this.el60 = new System.Windows.Forms.Button();
            this.el61 = new System.Windows.Forms.Button();
            this.el62 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.ded1 = new System.Windows.Forms.NumericUpDown();
            this.gbSpins = new System.Windows.Forms.GroupBox();
            this.label34 = new System.Windows.Forms.Label();
            this.cbBonusSpin1 = new System.Windows.Forms.CheckBox();
            this.cbBonusSpin2 = new System.Windows.Forms.CheckBox();
            this.cbCombo = new System.Windows.Forms.ComboBox();
            this.el26 = new System.Windows.Forms.Button();
            this.el41 = new System.Windows.Forms.Button();
            this.el46 = new System.Windows.Forms.Button();
            this.el50 = new System.Windows.Forms.Button();
            this.el51 = new System.Windows.Forms.Button();
            this.el52 = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.checkCombo2 = new JCS.ToggleSwitch();
            this.newComboSpin = new System.Windows.Forms.Button();
            this.cb49 = new System.Windows.Forms.ComboBox();
            this.cb47 = new System.Windows.Forms.ComboBox();
            this.cb48 = new System.Windows.Forms.ComboBox();
            this.cb45 = new System.Windows.Forms.ComboBox();
            this.cb40 = new System.Windows.Forms.ComboBox();
            this.cb25 = new System.Windows.Forms.ComboBox();
            this.el25 = new System.Windows.Forms.Button();
            this.el40 = new System.Windows.Forms.Button();
            this.el45 = new System.Windows.Forms.Button();
            this.el48 = new System.Windows.Forms.Button();
            this.el47 = new System.Windows.Forms.Button();
            this.el49 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.el65 = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.log = new System.Windows.Forms.TextBox();
            this.shapeContainer7 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rectangleShape7 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.imList = new System.Windows.Forms.ImageList(this.components);
            this.tt2 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.gbElements.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gbDed.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ded6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded2)).BeginInit();
            this.gbComboJumps.SuspendLayout();
            this.gbJumps.SuspendLayout();
            this.panel6.SuspendLayout();
            this.gbSteps.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ded1)).BeginInit();
            this.gbSpins.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.98501F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.94915F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.06585F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 2, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1165, 774);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.flowLayoutPanel1, 3);
            this.flowLayoutPanel1.Controls.Add(this.j1);
            this.flowLayoutPanel1.Controls.Add(this.j2);
            this.flowLayoutPanel1.Controls.Add(this.j3);
            this.flowLayoutPanel1.Controls.Add(this.j4);
            this.flowLayoutPanel1.Controls.Add(this.j5);
            this.flowLayoutPanel1.Controls.Add(this.j6);
            this.flowLayoutPanel1.Controls.Add(this.j7);
            this.flowLayoutPanel1.Controls.Add(this.j8);
            this.flowLayoutPanel1.Controls.Add(this.j9);
            this.flowLayoutPanel1.Controls.Add(this.referee);
            this.flowLayoutPanel1.Controls.Add(this.startstop);
            this.flowLayoutPanel1.Controls.Add(this.panel7);
            this.flowLayoutPanel1.Controls.Add(this.confirm);
            this.flowLayoutPanel1.Controls.Add(this.average);
            this.flowLayoutPanel1.Controls.Add(this.reset);
            this.flowLayoutPanel1.Controls.Add(this.prev);
            this.flowLayoutPanel1.Controls.Add(this.skip);
            this.flowLayoutPanel1.Controls.Add(this.info);
            this.flowLayoutPanel1.Controls.Add(this.quit);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 2);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1159, 80);
            this.flowLayoutPanel1.TabIndex = 9;
            // 
            // j1
            // 
            this.j1.AutoEllipsis = true;
            this.j1.BackColor = System.Drawing.Color.Transparent;
            this.j1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.j1.Dock = System.Windows.Forms.DockStyle.Top;
            this.j1.Enabled = false;
            this.j1.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j1.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j1.ForeColor = System.Drawing.Color.Gray;
            this.j1.Location = new System.Drawing.Point(0, 0);
            this.j1.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.j1.Name = "j1";
            this.j1.Size = new System.Drawing.Size(39, 40);
            this.j1.TabIndex = 76;
            this.j1.Tag = "1";
            this.j1.Text = "J1";
            this.j1.UseVisualStyleBackColor = false;
            this.j1.Click += new System.EventHandler(this.j1_Click);
            this.j1.MouseEnter += new System.EventHandler(this.j1_MouseEnter);
            // 
            // j2
            // 
            this.j2.BackColor = System.Drawing.Color.Transparent;
            this.j2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.j2.Dock = System.Windows.Forms.DockStyle.Top;
            this.j2.Enabled = false;
            this.j2.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j2.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j2.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j2.ForeColor = System.Drawing.Color.Gray;
            this.j2.Location = new System.Drawing.Point(40, 0);
            this.j2.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.j2.Name = "j2";
            this.j2.Size = new System.Drawing.Size(39, 40);
            this.j2.TabIndex = 77;
            this.j2.Tag = "2";
            this.j2.Text = "J2";
            this.j2.UseVisualStyleBackColor = false;
            this.j2.Click += new System.EventHandler(this.j1_Click);
            this.j2.MouseEnter += new System.EventHandler(this.j2_MouseEnter);
            // 
            // j3
            // 
            this.j3.BackColor = System.Drawing.Color.Transparent;
            this.j3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.j3.Dock = System.Windows.Forms.DockStyle.Top;
            this.j3.Enabled = false;
            this.j3.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j3.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j3.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j3.ForeColor = System.Drawing.Color.Gray;
            this.j3.Location = new System.Drawing.Point(80, 0);
            this.j3.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.j3.Name = "j3";
            this.j3.Size = new System.Drawing.Size(39, 40);
            this.j3.TabIndex = 78;
            this.j3.Tag = "3";
            this.j3.Text = "J3";
            this.j3.UseVisualStyleBackColor = false;
            this.j3.Click += new System.EventHandler(this.j1_Click);
            this.j3.MouseEnter += new System.EventHandler(this.j3_MouseEnter);
            // 
            // j4
            // 
            this.j4.BackColor = System.Drawing.Color.Transparent;
            this.j4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.j4.Dock = System.Windows.Forms.DockStyle.Top;
            this.j4.Enabled = false;
            this.j4.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j4.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j4.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j4.ForeColor = System.Drawing.Color.Gray;
            this.j4.Location = new System.Drawing.Point(120, 0);
            this.j4.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.j4.Name = "j4";
            this.j4.Size = new System.Drawing.Size(39, 40);
            this.j4.TabIndex = 79;
            this.j4.Tag = "4";
            this.j4.Text = "J4";
            this.j4.UseVisualStyleBackColor = false;
            this.j4.Click += new System.EventHandler(this.j1_Click);
            this.j4.MouseEnter += new System.EventHandler(this.j4_MouseEnter);
            // 
            // j5
            // 
            this.j5.BackColor = System.Drawing.Color.Transparent;
            this.j5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.j5.Dock = System.Windows.Forms.DockStyle.Top;
            this.j5.Enabled = false;
            this.j5.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j5.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j5.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j5.ForeColor = System.Drawing.Color.Gray;
            this.j5.Location = new System.Drawing.Point(160, 0);
            this.j5.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.j5.Name = "j5";
            this.j5.Size = new System.Drawing.Size(39, 40);
            this.j5.TabIndex = 80;
            this.j5.Tag = "5";
            this.j5.Text = "J5";
            this.j5.UseVisualStyleBackColor = false;
            this.j5.Click += new System.EventHandler(this.j1_Click);
            this.j5.MouseEnter += new System.EventHandler(this.j5_MouseEnter);
            // 
            // j6
            // 
            this.j6.BackColor = System.Drawing.Color.Transparent;
            this.j6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.j6.Dock = System.Windows.Forms.DockStyle.Top;
            this.j6.Enabled = false;
            this.j6.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j6.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j6.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j6.ForeColor = System.Drawing.Color.Gray;
            this.j6.Location = new System.Drawing.Point(200, 0);
            this.j6.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.j6.Name = "j6";
            this.j6.Size = new System.Drawing.Size(39, 40);
            this.j6.TabIndex = 81;
            this.j6.Tag = "6";
            this.j6.Text = "J6";
            this.j6.UseVisualStyleBackColor = false;
            this.j6.Click += new System.EventHandler(this.j1_Click);
            this.j6.MouseEnter += new System.EventHandler(this.j6_MouseEnter);
            // 
            // j7
            // 
            this.j7.BackColor = System.Drawing.Color.Transparent;
            this.j7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.j7.Dock = System.Windows.Forms.DockStyle.Top;
            this.j7.Enabled = false;
            this.j7.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j7.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j7.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j7.ForeColor = System.Drawing.Color.Gray;
            this.j7.Location = new System.Drawing.Point(240, 0);
            this.j7.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.j7.Name = "j7";
            this.j7.Size = new System.Drawing.Size(39, 40);
            this.j7.TabIndex = 82;
            this.j7.Tag = "7";
            this.j7.Text = "J7";
            this.j7.UseVisualStyleBackColor = false;
            this.j7.Click += new System.EventHandler(this.j1_Click);
            this.j7.MouseEnter += new System.EventHandler(this.j7_MouseEnter);
            // 
            // j8
            // 
            this.j8.BackColor = System.Drawing.Color.Transparent;
            this.j8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.j8.Dock = System.Windows.Forms.DockStyle.Top;
            this.j8.Enabled = false;
            this.j8.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j8.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j8.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j8.ForeColor = System.Drawing.Color.Gray;
            this.j8.Location = new System.Drawing.Point(280, 0);
            this.j8.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.j8.Name = "j8";
            this.j8.Size = new System.Drawing.Size(39, 40);
            this.j8.TabIndex = 83;
            this.j8.Tag = "8";
            this.j8.Text = "J8";
            this.j8.UseVisualStyleBackColor = false;
            this.j8.Click += new System.EventHandler(this.j1_Click);
            this.j8.MouseEnter += new System.EventHandler(this.j8_MouseEnter);
            // 
            // j9
            // 
            this.j9.BackColor = System.Drawing.Color.Transparent;
            this.j9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.j9.Dock = System.Windows.Forms.DockStyle.Top;
            this.j9.Enabled = false;
            this.j9.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j9.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j9.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j9.ForeColor = System.Drawing.Color.Gray;
            this.j9.Location = new System.Drawing.Point(320, 0);
            this.j9.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.j9.Name = "j9";
            this.j9.Size = new System.Drawing.Size(39, 40);
            this.j9.TabIndex = 84;
            this.j9.Tag = "9";
            this.j9.Text = "J9";
            this.j9.UseVisualStyleBackColor = false;
            this.j9.Click += new System.EventHandler(this.j1_Click);
            this.j9.MouseEnter += new System.EventHandler(this.j9_MouseEnter);
            // 
            // referee
            // 
            this.referee.AutoSize = true;
            this.referee.BackColor = System.Drawing.Color.Transparent;
            this.referee.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.referee.Cursor = System.Windows.Forms.Cursors.Hand;
            this.referee.Dock = System.Windows.Forms.DockStyle.Top;
            this.referee.FlatAppearance.BorderColor = System.Drawing.Color.Cyan;
            this.referee.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.referee.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.referee.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.referee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.referee.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.referee.ForeColor = System.Drawing.Color.Cyan;
            this.referee.Location = new System.Drawing.Point(363, 0);
            this.referee.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.referee.Name = "referee";
            this.referee.Size = new System.Drawing.Size(45, 40);
            this.referee.TabIndex = 88;
            this.referee.Tag = "0";
            this.referee.Text = "R";
            this.referee.UseVisualStyleBackColor = false;
            this.referee.Visible = false;
            this.referee.Click += new System.EventHandler(this.j1_Click);
            // 
            // startstop
            // 
            this.startstop.BackColor = System.Drawing.Color.DimGray;
            this.startstop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.startstop.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.startstop.FlatAppearance.BorderSize = 2;
            this.startstop.FlatAppearance.MouseDownBackColor = System.Drawing.Color.YellowGreen;
            this.startstop.FlatAppearance.MouseOverBackColor = System.Drawing.Color.ForestGreen;
            this.startstop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.startstop.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startstop.ForeColor = System.Drawing.Color.White;
            this.startstop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.startstop.Location = new System.Drawing.Point(411, 0);
            this.startstop.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.startstop.Name = "startstop";
            this.startstop.Size = new System.Drawing.Size(94, 40);
            this.startstop.TabIndex = 90;
            this.startstop.Text = "START";
            this.startstop.UseVisualStyleBackColor = false;
            this.startstop.Click += new System.EventHandler(this.startstop_Click);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.ltimer);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(505, 0);
            this.panel7.Margin = new System.Windows.Forms.Padding(0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(83, 40);
            this.panel7.TabIndex = 105;
            // 
            // ltimer
            // 
            this.ltimer.BackColor = System.Drawing.Color.Transparent;
            this.ltimer.Cursor = System.Windows.Forms.Cursors.Default;
            this.ltimer.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.ltimer.FlatAppearance.BorderSize = 2;
            this.ltimer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ltimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ltimer.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ltimer.ForeColor = System.Drawing.Color.Magenta;
            this.ltimer.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.ltimer.Location = new System.Drawing.Point(3, 0);
            this.ltimer.Margin = new System.Windows.Forms.Padding(0);
            this.ltimer.Name = "ltimer";
            this.ltimer.Size = new System.Drawing.Size(80, 40);
            this.ltimer.TabIndex = 98;
            this.ltimer.Text = "00:00";
            this.ltimer.UseVisualStyleBackColor = false;
            // 
            // confirm
            // 
            this.confirm.BackColor = System.Drawing.Color.DimGray;
            this.confirm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.confirm.Enabled = false;
            this.confirm.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.confirm.FlatAppearance.BorderSize = 2;
            this.confirm.FlatAppearance.MouseOverBackColor = System.Drawing.Color.ForestGreen;
            this.confirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.confirm.Font = new System.Drawing.Font("Arial Narrow", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirm.ForeColor = System.Drawing.Color.White;
            this.confirm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.confirm.Location = new System.Drawing.Point(591, 0);
            this.confirm.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.confirm.Name = "confirm";
            this.confirm.Size = new System.Drawing.Size(111, 40);
            this.confirm.TabIndex = 95;
            this.confirm.Text = "CONFIRM";
            this.confirm.UseVisualStyleBackColor = false;
            this.confirm.Click += new System.EventHandler(this.button2_Click);
            // 
            // average
            // 
            this.average.BackColor = System.Drawing.Color.DimGray;
            this.average.Cursor = System.Windows.Forms.Cursors.Hand;
            this.average.Enabled = false;
            this.average.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.average.FlatAppearance.BorderSize = 2;
            this.average.FlatAppearance.MouseOverBackColor = System.Drawing.Color.ForestGreen;
            this.average.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.average.Font = new System.Drawing.Font("Arial Narrow", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.average.ForeColor = System.Drawing.Color.White;
            this.average.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.average.Location = new System.Drawing.Point(705, 0);
            this.average.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.average.Name = "average";
            this.average.Size = new System.Drawing.Size(96, 40);
            this.average.TabIndex = 110;
            this.average.Text = "AVERAGE";
            this.average.UseVisualStyleBackColor = false;
            this.average.Click += new System.EventHandler(this.average_Click);
            // 
            // reset
            // 
            this.reset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.reset.Cursor = System.Windows.Forms.Cursors.Hand;
            this.reset.Enabled = false;
            this.reset.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.reset.FlatAppearance.BorderSize = 2;
            this.reset.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.reset.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.reset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.reset.Font = new System.Drawing.Font("Arial Narrow", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reset.ForeColor = System.Drawing.Color.White;
            this.reset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.reset.Location = new System.Drawing.Point(804, 0);
            this.reset.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.reset.Name = "reset";
            this.reset.Size = new System.Drawing.Size(79, 40);
            this.reset.TabIndex = 96;
            this.reset.Text = "RESET";
            this.reset.UseVisualStyleBackColor = false;
            this.reset.Click += new System.EventHandler(this.next_Click);
            // 
            // prev
            // 
            this.prev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prev.AutoSize = true;
            this.prev.BackColor = System.Drawing.Color.White;
            this.prev.Cursor = System.Windows.Forms.Cursors.Hand;
            this.prev.Enabled = false;
            this.prev.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.prev.FlatAppearance.BorderSize = 2;
            this.prev.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.prev.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.prev.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.prev.Font = new System.Drawing.Font("Arial Narrow", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prev.ForeColor = System.Drawing.Color.White;
            this.prev.Image = global::RollartSystemTech.Properties.Resources.display;
            this.prev.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.prev.Location = new System.Drawing.Point(886, 0);
            this.prev.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.prev.Name = "prev";
            this.prev.Size = new System.Drawing.Size(49, 40);
            this.prev.TabIndex = 111;
            this.tt2.SetToolTip(this.prev, "Display the previous skater score");
            this.prev.UseVisualStyleBackColor = false;
            this.prev.Click += new System.EventHandler(this.prev_Click);
            // 
            // skip
            // 
            this.skip.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.skip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.skip.Cursor = System.Windows.Forms.Cursors.Hand;
            this.skip.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.skip.FlatAppearance.BorderSize = 2;
            this.skip.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Navy;
            this.skip.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Navy;
            this.skip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.skip.Font = new System.Drawing.Font("Arial Narrow", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.skip.ForeColor = System.Drawing.Color.White;
            this.skip.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.skip.Location = new System.Drawing.Point(938, 0);
            this.skip.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.skip.Name = "skip";
            this.skip.Size = new System.Drawing.Size(70, 40);
            this.skip.TabIndex = 106;
            this.skip.Text = "SKIP";
            this.tt2.SetToolTip(this.skip, "Skip to the next skater");
            this.skip.UseVisualStyleBackColor = false;
            this.skip.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // info
            // 
            this.info.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.info.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.info.Cursor = System.Windows.Forms.Cursors.Hand;
            this.info.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.info.FlatAppearance.BorderSize = 2;
            this.info.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Purple;
            this.info.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Purple;
            this.info.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.info.Font = new System.Drawing.Font("Arial Narrow", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.info.ForeColor = System.Drawing.Color.White;
            this.info.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.info.Location = new System.Drawing.Point(1011, 0);
            this.info.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(69, 40);
            this.info.TabIndex = 107;
            this.info.Text = "INFO";
            this.tt2.SetToolTip(this.info, "Event Info");
            this.info.UseVisualStyleBackColor = false;
            this.info.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // quit
            // 
            this.quit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.quit.BackColor = System.Drawing.Color.Red;
            this.quit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.quit.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.quit.FlatAppearance.BorderSize = 2;
            this.quit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.quit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Maroon;
            this.quit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.quit.Font = new System.Drawing.Font("Arial Narrow", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quit.ForeColor = System.Drawing.Color.White;
            this.quit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.quit.Location = new System.Drawing.Point(3, 40);
            this.quit.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.quit.Name = "quit";
            this.quit.Size = new System.Drawing.Size(90, 40);
            this.quit.TabIndex = 108;
            this.quit.Text = "QUIT";
            this.quit.UseVisualStyleBackColor = false;
            this.quit.Click += new System.EventHandler(this.button2_Click_2);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(387, 84);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(401, 640);
            this.panel1.TabIndex = 3;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.gbElements);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(401, 640);
            this.panel4.TabIndex = 2;
            // 
            // gbElements
            // 
            this.gbElements.Controls.Add(this.lv);
            this.gbElements.Controls.Add(this.error);
            this.gbElements.Controls.Add(this.panel3);
            this.gbElements.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbElements.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbElements.Location = new System.Drawing.Point(0, 0);
            this.gbElements.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbElements.Name = "gbElements";
            this.gbElements.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbElements.Size = new System.Drawing.Size(401, 640);
            this.gbElements.TabIndex = 1;
            this.gbElements.TabStop = false;
            // 
            // lv
            // 
            this.lv.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.lv.BackColor = System.Drawing.Color.Black;
            this.lv.BackgroundImageTiled = true;
            this.lv.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.note,
            this.magg});
            this.lv.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lv.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv.ForeColor = System.Drawing.Color.White;
            this.lv.FullRowSelect = true;
            this.lv.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lv.Location = new System.Drawing.Point(3, 29);
            this.lv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lv.MultiSelect = false;
            this.lv.Name = "lv";
            this.lv.ShowItemToolTips = true;
            this.lv.Size = new System.Drawing.Size(395, 503);
            this.lv.SmallImageList = this.imageList1;
            this.lv.TabIndex = 95;
            this.lv.UseCompatibleStateImageBehavior = false;
            this.lv.View = System.Windows.Forms.View.Details;
            this.lv.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.lv_ColumnWidthChanging);
            this.lv.SelectedIndexChanged += new System.EventHandler(this.lv_SelectedIndexChanged);
            this.lv.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lv_MouseClick);
            this.lv.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lv_MouseDoubleClick);
            this.lv.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lv_MouseDown);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "";
            this.columnHeader1.Width = 58;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Element";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 81;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Value";
            this.columnHeader3.Width = 53;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Type";
            this.columnHeader4.Width = 106;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "CAT";
            this.columnHeader5.Width = 0;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "NUMCOMBO";
            this.columnHeader6.Width = 0;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "D/U";
            this.columnHeader7.Width = 0;
            // 
            // note
            // 
            this.note.Text = "Note";
            this.note.Width = 62;
            // 
            // magg
            // 
            this.magg.Text = "Time";
            this.magg.Width = 42;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "bullet_green.png");
            this.imageList1.Images.SetKeyName(1, "bullet_red.png");
            this.imageList1.Images.SetKeyName(2, "bullet_yellow.png");
            // 
            // error
            // 
            this.error.AutoEllipsis = true;
            this.error.BackColor = System.Drawing.Color.Yellow;
            this.error.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.error.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.error.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.error.ForeColor = System.Drawing.Color.Red;
            this.error.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.error.Location = new System.Drawing.Point(3, 532);
            this.error.Name = "error";
            this.error.Size = new System.Drawing.Size(395, 67);
            this.error.TabIndex = 94;
            this.error.Text = "error test";
            this.error.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.error.Visible = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.cbVerify);
            this.panel3.Controls.Add(this.review);
            this.panel3.Controls.Add(this.asterisco);
            this.panel3.Controls.Add(this.buttonT);
            this.panel3.Controls.Add(this.cancel);
            this.panel3.Controls.Add(this.back);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(3, 599);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(395, 39);
            this.panel3.TabIndex = 92;
            // 
            // cbVerify
            // 
            this.cbVerify.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbVerify.BackColor = System.Drawing.Color.Gray;
            this.cbVerify.Checked = true;
            this.cbVerify.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbVerify.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbVerify.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbVerify.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.cbVerify.FlatAppearance.BorderSize = 2;
            this.cbVerify.FlatAppearance.CheckedBackColor = System.Drawing.Color.Lime;
            this.cbVerify.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Yellow;
            this.cbVerify.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Lime;
            this.cbVerify.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbVerify.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVerify.ForeColor = System.Drawing.Color.Black;
            this.cbVerify.Location = new System.Drawing.Point(197, 0);
            this.cbVerify.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbVerify.Name = "cbVerify";
            this.cbVerify.Size = new System.Drawing.Size(59, 39);
            this.cbVerify.TabIndex = 178;
            this.cbVerify.Text = "Check";
            this.cbVerify.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbVerify.UseVisualStyleBackColor = false;
            this.cbVerify.CheckedChanged += new System.EventHandler(this.cbVerify_CheckedChanged);
            // 
            // review
            // 
            this.review.BackColor = System.Drawing.Color.DarkRed;
            this.review.Cursor = System.Windows.Forms.Cursors.Hand;
            this.review.Dock = System.Windows.Forms.DockStyle.Right;
            this.review.Enabled = false;
            this.review.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.review.FlatAppearance.BorderSize = 2;
            this.review.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.review.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.review.Font = new System.Drawing.Font("Arial Narrow", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.review.ForeColor = System.Drawing.Color.Yellow;
            this.review.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.review.Location = new System.Drawing.Point(256, 0);
            this.review.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.review.Name = "review";
            this.review.Size = new System.Drawing.Size(75, 39);
            this.review.TabIndex = 95;
            this.review.Text = "Review";
            this.review.UseVisualStyleBackColor = false;
            this.review.Click += new System.EventHandler(this.edit_Click);
            // 
            // asterisco
            // 
            this.asterisco.BackColor = System.Drawing.Color.Blue;
            this.asterisco.Cursor = System.Windows.Forms.Cursors.Hand;
            this.asterisco.Dock = System.Windows.Forms.DockStyle.Right;
            this.asterisco.Enabled = false;
            this.asterisco.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.asterisco.FlatAppearance.BorderSize = 2;
            this.asterisco.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.asterisco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.asterisco.Font = new System.Drawing.Font("Arial Narrow", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.asterisco.ForeColor = System.Drawing.Color.Yellow;
            this.asterisco.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.asterisco.Location = new System.Drawing.Point(331, 0);
            this.asterisco.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.asterisco.Name = "asterisco";
            this.asterisco.Size = new System.Drawing.Size(32, 39);
            this.asterisco.TabIndex = 179;
            this.asterisco.Text = "*";
            this.tt2.SetToolTip(this.asterisco, "Add/Remove * to the selected element");
            this.asterisco.UseVisualStyleBackColor = false;
            this.asterisco.Click += new System.EventHandler(this.asterisco_Click);
            // 
            // buttonT
            // 
            this.buttonT.BackColor = System.Drawing.Color.Blue;
            this.buttonT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonT.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonT.Enabled = false;
            this.buttonT.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonT.FlatAppearance.BorderSize = 2;
            this.buttonT.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.buttonT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonT.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonT.ForeColor = System.Drawing.Color.Yellow;
            this.buttonT.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonT.Location = new System.Drawing.Point(363, 0);
            this.buttonT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonT.Name = "buttonT";
            this.buttonT.Size = new System.Drawing.Size(32, 39);
            this.buttonT.TabIndex = 180;
            this.buttonT.Text = "T";
            this.tt2.SetToolTip(this.buttonT, "Add/Remove Time bonus to the selected jump");
            this.buttonT.UseVisualStyleBackColor = false;
            this.buttonT.Click += new System.EventHandler(this.button5_Click);
            // 
            // cancel
            // 
            this.cancel.BackColor = System.Drawing.Color.DimGray;
            this.cancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cancel.Dock = System.Windows.Forms.DockStyle.Left;
            this.cancel.Enabled = false;
            this.cancel.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.cancel.FlatAppearance.BorderSize = 2;
            this.cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancel.Font = new System.Drawing.Font("Arial Narrow", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancel.ForeColor = System.Drawing.Color.White;
            this.cancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cancel.Location = new System.Drawing.Point(122, 0);
            this.cancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(75, 39);
            this.cancel.TabIndex = 96;
            this.cancel.Text = "Cancel";
            this.cancel.UseVisualStyleBackColor = false;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // back
            // 
            this.back.AutoSize = true;
            this.back.BackColor = System.Drawing.Color.DimGray;
            this.back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.back.Dock = System.Windows.Forms.DockStyle.Left;
            this.back.Enabled = false;
            this.back.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.back.FlatAppearance.BorderSize = 2;
            this.back.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.back.Font = new System.Drawing.Font("Arial Narrow", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.back.ForeColor = System.Drawing.Color.White;
            this.back.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.back.Location = new System.Drawing.Point(0, 0);
            this.back.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(122, 39);
            this.back.TabIndex = 94;
            this.back.Text = "Delete Last";
            this.back.UseVisualStyleBackColor = false;
            this.back.Click += new System.EventHandler(this.clear_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 3);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 550F));
            this.tableLayoutPanel2.Controls.Add(this.buttonNext, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel5, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.openDed, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel8, 5, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 728);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel1.SetRowSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1159, 44);
            this.tableLayoutPanel2.TabIndex = 10;
            this.tableLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel2_Paint);
            // 
            // buttonNext
            // 
            this.buttonNext.AutoEllipsis = true;
            this.buttonNext.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonNext.BackColor = System.Drawing.Color.Black;
            this.buttonNext.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonNext.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonNext.FlatAppearance.BorderSize = 0;
            this.buttonNext.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonNext.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNext.Font = new System.Drawing.Font("Arial Narrow", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNext.ForeColor = System.Drawing.Color.Yellow;
            this.buttonNext.Location = new System.Drawing.Point(399, 0);
            this.buttonNext.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(580, 42);
            this.buttonNext.TabIndex = 180;
            this.buttonNext.Text = "1/100 - ";
            this.buttonNext.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonNext.UseVisualStyleBackColor = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.total);
            this.panel5.Controls.Add(this.deductions);
            this.panel5.Controls.Add(this.elements);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Location = new System.Drawing.Point(156, 2);
            this.panel5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(237, 37);
            this.panel5.TabIndex = 101;
            // 
            // total
            // 
            this.total.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.total.AutoSize = true;
            this.total.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total.ForeColor = System.Drawing.Color.Lime;
            this.total.Location = new System.Drawing.Point(189, 2);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(51, 29);
            this.total.TabIndex = 96;
            this.total.Text = "0.00";
            this.total.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // deductions
            // 
            this.deductions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.deductions.AutoSize = true;
            this.deductions.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deductions.ForeColor = System.Drawing.Color.White;
            this.deductions.Location = new System.Drawing.Point(109, 2);
            this.deductions.Name = "deductions";
            this.deductions.Size = new System.Drawing.Size(51, 29);
            this.deductions.TabIndex = 95;
            this.deductions.Text = "0.00";
            this.deductions.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // elements
            // 
            this.elements.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.elements.AutoSize = true;
            this.elements.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.elements.ForeColor = System.Drawing.Color.White;
            this.elements.Location = new System.Drawing.Point(18, 2);
            this.elements.Name = "elements";
            this.elements.Size = new System.Drawing.Size(51, 29);
            this.elements.TabIndex = 94;
            this.elements.Text = "0.00";
            this.elements.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Lime;
            this.label11.Location = new System.Drawing.Point(191, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 22);
            this.label11.TabIndex = 91;
            this.label11.Text = "Total";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(3, 21);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 20);
            this.label13.TabIndex = 93;
            this.label13.Text = "Base technical";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(102, 21);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 20);
            this.label12.TabIndex = 92;
            this.label12.Text = "Deductions";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // openDed
            // 
            this.openDed.BackColor = System.Drawing.Color.Transparent;
            this.openDed.Cursor = System.Windows.Forms.Cursors.Hand;
            this.openDed.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.openDed.FlatAppearance.BorderSize = 2;
            this.openDed.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.openDed.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.openDed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openDed.ForeColor = System.Drawing.Color.Red;
            this.openDed.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.openDed.Location = new System.Drawing.Point(3, 2);
            this.openDed.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.openDed.Name = "openDed";
            this.openDed.Size = new System.Drawing.Size(147, 37);
            this.openDed.TabIndex = 100;
            this.openDed.Text = "Deductions";
            this.openDed.UseVisualStyleBackColor = false;
            this.openDed.Click += new System.EventHandler(this.openDed_Click);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.labelPartial);
            this.panel8.Location = new System.Drawing.Point(997, 2);
            this.panel8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(358, 40);
            this.panel8.TabIndex = 181;
            // 
            // labelPartial
            // 
            this.labelPartial.AutoSize = true;
            this.labelPartial.BackColor = System.Drawing.Color.Transparent;
            this.labelPartial.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPartial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelPartial.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPartial.ForeColor = System.Drawing.Color.Aqua;
            this.labelPartial.Location = new System.Drawing.Point(0, 0);
            this.labelPartial.Name = "labelPartial";
            this.labelPartial.Size = new System.Drawing.Size(127, 48);
            this.labelPartial.TabIndex = 232;
            this.labelPartial.Tag = "6";
            this.labelPartial.Text = "Partial RANK:\r\nPartial SCORE:";
            this.labelPartial.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gbDed);
            this.panel2.Controls.Add(this.gbComboJumps);
            this.panel2.Controls.Add(this.gbJumps);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 84);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(378, 640);
            this.panel2.TabIndex = 11;
            // 
            // gbDed
            // 
            this.gbDed.Controls.Add(this.ded6);
            this.gbDed.Controls.Add(this.label4);
            this.gbDed.Controls.Add(this.ded5);
            this.gbDed.Controls.Add(this.label2);
            this.gbDed.Controls.Add(this.ded3);
            this.gbDed.Controls.Add(this.ded4);
            this.gbDed.Controls.Add(this.label10);
            this.gbDed.Controls.Add(this.label22);
            this.gbDed.Controls.Add(this.ded2);
            this.gbDed.Controls.Add(this.label23);
            this.gbDed.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gbDed.Location = new System.Drawing.Point(0, 420);
            this.gbDed.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbDed.Name = "gbDed";
            this.gbDed.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbDed.Size = new System.Drawing.Size(378, 220);
            this.gbDed.TabIndex = 3;
            this.gbDed.TabStop = false;
            this.gbDed.Visible = false;
            this.gbDed.Enter += new System.EventHandler(this.gbDed_Enter);
            // 
            // ded6
            // 
            this.ded6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ded6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ded6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ded6.Font = new System.Drawing.Font("Arial Narrow", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded6.ForeColor = System.Drawing.Color.Red;
            this.ded6.Location = new System.Drawing.Point(303, 25);
            this.ded6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ded6.Name = "ded6";
            this.ded6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ded6.Size = new System.Drawing.Size(55, 34);
            this.ded6.TabIndex = 175;
            this.ded6.ValueChanged += new System.EventHandler(this.ded6_ValueChanged);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(-2, 27);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label4.Size = new System.Drawing.Size(299, 26);
            this.label4.TabIndex = 174;
            this.label4.Text = "MISSING ELEMENT";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ded5
            // 
            this.ded5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ded5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ded5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ded5.Font = new System.Drawing.Font("Arial Narrow", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded5.ForeColor = System.Drawing.Color.Red;
            this.ded5.Location = new System.Drawing.Point(303, 161);
            this.ded5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ded5.Name = "ded5";
            this.ded5.Size = new System.Drawing.Size(55, 34);
            this.ded5.TabIndex = 171;
            this.ded5.ValueChanged += new System.EventHandler(this.ded5_ValueChanged);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(-2, 163);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(300, 26);
            this.label2.TabIndex = 166;
            this.label2.Text = "COSTUME VIOLATION";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ded3
            // 
            this.ded3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ded3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ded3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ded3.Font = new System.Drawing.Font("Arial Narrow", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded3.ForeColor = System.Drawing.Color.Red;
            this.ded3.Location = new System.Drawing.Point(303, 93);
            this.ded3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ded3.Name = "ded3";
            this.ded3.Size = new System.Drawing.Size(55, 34);
            this.ded3.TabIndex = 169;
            this.ded3.ValueChanged += new System.EventHandler(this.ded3_ValueChanged);
            // 
            // ded4
            // 
            this.ded4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ded4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ded4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ded4.Font = new System.Drawing.Font("Arial Narrow", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded4.ForeColor = System.Drawing.Color.Red;
            this.ded4.Location = new System.Drawing.Point(303, 59);
            this.ded4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ded4.Name = "ded4";
            this.ded4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ded4.Size = new System.Drawing.Size(55, 34);
            this.ded4.TabIndex = 170;
            this.ded4.ValueChanged += new System.EventHandler(this.ded4_ValueChanged);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label10.Font = new System.Drawing.Font("Arial Narrow", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(-2, 61);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label10.Size = new System.Drawing.Size(299, 26);
            this.label10.TabIndex = 165;
            this.label10.Text = "ILLEGAL ELEMENT";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label22.Font = new System.Drawing.Font("Arial Narrow", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(3, 95);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(294, 26);
            this.label22.TabIndex = 164;
            this.label22.Text = "TIME VIOLATION";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ded2
            // 
            this.ded2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ded2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ded2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ded2.DecimalPlaces = 1;
            this.ded2.Font = new System.Drawing.Font("Arial Narrow", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded2.ForeColor = System.Drawing.Color.Red;
            this.ded2.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.ded2.Location = new System.Drawing.Point(303, 127);
            this.ded2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ded2.Name = "ded2";
            this.ded2.Size = new System.Drawing.Size(55, 34);
            this.ded2.TabIndex = 168;
            this.ded2.ValueChanged += new System.EventHandler(this.ded2_ValueChanged);
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label23.Font = new System.Drawing.Font("Arial Narrow", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(3, 129);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(294, 26);
            this.label23.TabIndex = 163;
            this.label23.Text = "MUSIC VIOLATION";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label23.Click += new System.EventHandler(this.label23_Click);
            // 
            // gbComboJumps
            // 
            this.gbComboJumps.BackColor = System.Drawing.Color.Transparent;
            this.gbComboJumps.Controls.Add(this.el0_);
            this.gbComboJumps.Controls.Add(this.half2);
            this.gbComboJumps.Controls.Add(this.el64__);
            this.gbComboJumps.Controls.Add(this.newCombo);
            this.gbComboJumps.Controls.Add(this.el64_);
            this.gbComboJumps.Controls.Add(this.label32);
            this.gbComboJumps.Controls.Add(this.under2);
            this.gbComboJumps.Controls.Add(this.down2);
            this.gbComboJumps.Controls.Add(this.ritt44);
            this.gbComboJumps.Controls.Add(this.lutz44);
            this.gbComboJumps.Controls.Add(this.flip44);
            this.gbComboJumps.Controls.Add(this.sal44);
            this.gbComboJumps.Controls.Add(this.toeloop44);
            this.gbComboJumps.Controls.Add(this.axel44);
            this.gbComboJumps.Controls.Add(this.ritt33);
            this.gbComboJumps.Controls.Add(this.ritt22);
            this.gbComboJumps.Controls.Add(this.ritt11);
            this.gbComboJumps.Controls.Add(this.label14);
            this.gbComboJumps.Controls.Add(this.lutz33);
            this.gbComboJumps.Controls.Add(this.lutz22);
            this.gbComboJumps.Controls.Add(this.lutz11);
            this.gbComboJumps.Controls.Add(this.label27);
            this.gbComboJumps.Controls.Add(this.flip33);
            this.gbComboJumps.Controls.Add(this.flip22);
            this.gbComboJumps.Controls.Add(this.flip11);
            this.gbComboJumps.Controls.Add(this.label28);
            this.gbComboJumps.Controls.Add(this.sal33);
            this.gbComboJumps.Controls.Add(this.sal22);
            this.gbComboJumps.Controls.Add(this.sal11);
            this.gbComboJumps.Controls.Add(this.label29);
            this.gbComboJumps.Controls.Add(this.toeloop33);
            this.gbComboJumps.Controls.Add(this.toeloop22);
            this.gbComboJumps.Controls.Add(this.toeloop11);
            this.gbComboJumps.Controls.Add(this.label30);
            this.gbComboJumps.Controls.Add(this.axel33);
            this.gbComboJumps.Controls.Add(this.axel22);
            this.gbComboJumps.Controls.Add(this.axel11);
            this.gbComboJumps.Controls.Add(this.label31);
            this.gbComboJumps.Controls.Add(this.button1);
            this.gbComboJumps.Controls.Add(this.button2);
            this.gbComboJumps.Controls.Add(this.button3);
            this.gbComboJumps.Controls.Add(this.button4);
            this.gbComboJumps.Controls.Add(this.label26);
            this.gbComboJumps.Cursor = System.Windows.Forms.Cursors.Default;
            this.gbComboJumps.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbComboJumps.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbComboJumps.Font = new System.Drawing.Font("Arial Narrow", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbComboJumps.ForeColor = System.Drawing.Color.Yellow;
            this.gbComboJumps.Location = new System.Drawing.Point(0, 295);
            this.gbComboJumps.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbComboJumps.Name = "gbComboJumps";
            this.gbComboJumps.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbComboJumps.Size = new System.Drawing.Size(378, 345);
            this.gbComboJumps.TabIndex = 2;
            this.gbComboJumps.TabStop = false;
            this.gbComboJumps.Tag = "2";
            // 
            // el0_
            // 
            this.el0_.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el0_.BackColor = System.Drawing.Color.Transparent;
            this.el0_.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el0_.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el0_.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el0_.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el0_.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el0_.ForeColor = System.Drawing.Color.Yellow;
            this.el0_.Location = new System.Drawing.Point(214, 16);
            this.el0_.Margin = new System.Windows.Forms.Padding(0);
            this.el0_.Name = "el0_";
            this.el0_.Size = new System.Drawing.Size(24, 29);
            this.el0_.TabIndex = 134;
            this.el0_.Text = "W";
            this.el0_.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.el0_.UseVisualStyleBackColor = false;
            this.el0_.Click += new System.EventHandler(this.el0__Click);
            // 
            // half2
            // 
            this.half2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.half2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.half2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.half2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.half2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.half2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.half2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.half2.ForeColor = System.Drawing.Color.Black;
            this.half2.Location = new System.Drawing.Point(44, 50);
            this.half2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.half2.Name = "half2";
            this.half2.Size = new System.Drawing.Size(24, 224);
            this.half2.TabIndex = 133;
            this.half2.Text = "HALFORTATED";
            this.half2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.half2.UseVisualStyleBackColor = false;
            this.half2.Click += new System.EventHandler(this.half2_Click);
            // 
            // el64__
            // 
            this.el64__.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el64__.BackColor = System.Drawing.Color.Transparent;
            this.el64__.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el64__.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.el64__.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el64__.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el64__.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el64__.ForeColor = System.Drawing.Color.Red;
            this.el64__.Location = new System.Drawing.Point(244, 16);
            this.el64__.Margin = new System.Windows.Forms.Padding(0);
            this.el64__.Name = "el64__";
            this.el64__.Size = new System.Drawing.Size(24, 29);
            this.el64__.TabIndex = 132;
            this.el64__.Text = "X";
            this.el64__.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.tt2.SetToolTip(this.el64__, "Failed jump");
            this.el64__.UseVisualStyleBackColor = false;
            this.el64__.Click += new System.EventHandler(this.fail_Click);
            // 
            // newCombo
            // 
            this.newCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.newCombo.BackColor = System.Drawing.Color.Transparent;
            this.newCombo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.newCombo.Enabled = false;
            this.newCombo.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.newCombo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.newCombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.newCombo.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newCombo.ForeColor = System.Drawing.Color.Yellow;
            this.newCombo.Location = new System.Drawing.Point(274, 16);
            this.newCombo.Margin = new System.Windows.Forms.Padding(0);
            this.newCombo.Name = "newCombo";
            this.newCombo.Size = new System.Drawing.Size(54, 29);
            this.newCombo.TabIndex = 131;
            this.newCombo.Text = "New";
            this.newCombo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.newCombo.UseVisualStyleBackColor = false;
            this.newCombo.Click += new System.EventHandler(this.newCombo_Click);
            // 
            // el64_
            // 
            this.el64_.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el64_.BackColor = System.Drawing.Color.Transparent;
            this.el64_.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el64_.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el64_.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el64_.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el64_.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el64_.ForeColor = System.Drawing.Color.Yellow;
            this.el64_.Location = new System.Drawing.Point(334, 16);
            this.el64_.Margin = new System.Windows.Forms.Padding(0);
            this.el64_.Name = "el64_";
            this.el64_.Size = new System.Drawing.Size(24, 29);
            this.el64_.TabIndex = 129;
            this.el64_.Text = "0";
            this.el64_.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.el64_.UseVisualStyleBackColor = false;
            this.el64_.Click += new System.EventHandler(this.noj_Click);
            // 
            // label32
            // 
            this.label32.AutoEllipsis = true;
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Lime;
            this.label32.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(4, 15);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(167, 29);
            this.label32.TabIndex = 128;
            this.label32.Text = "Combo Jump";
            // 
            // under2
            // 
            this.under2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.under2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.under2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.under2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.under2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.under2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.under2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.under2.ForeColor = System.Drawing.Color.Black;
            this.under2.Location = new System.Drawing.Point(15, 50);
            this.under2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.under2.Name = "under2";
            this.under2.Size = new System.Drawing.Size(24, 224);
            this.under2.TabIndex = 65;
            this.under2.Text = "UNDEROTATED";
            this.under2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.under2.UseVisualStyleBackColor = false;
            this.under2.Click += new System.EventHandler(this.under2_Click);
            // 
            // down2
            // 
            this.down2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.down2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.down2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.down2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.down2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.down2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.down2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.down2.ForeColor = System.Drawing.Color.Black;
            this.down2.Location = new System.Drawing.Point(73, 50);
            this.down2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.down2.Name = "down2";
            this.down2.Size = new System.Drawing.Size(24, 224);
            this.down2.TabIndex = 64;
            this.down2.Text = "DOWNGRADED";
            this.down2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.down2.UseVisualStyleBackColor = false;
            this.down2.Click += new System.EventHandler(this.down2_Click);
            // 
            // ritt44
            // 
            this.ritt44.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ritt44.BackColor = System.Drawing.Color.Transparent;
            this.ritt44.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ritt44.Enabled = false;
            this.ritt44.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.ritt44.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.ritt44.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ritt44.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ritt44.ForeColor = System.Drawing.Color.Yellow;
            this.ritt44.Location = new System.Drawing.Point(334, 217);
            this.ritt44.Margin = new System.Windows.Forms.Padding(0);
            this.ritt44.Name = "ritt44";
            this.ritt44.Size = new System.Drawing.Size(24, 29);
            this.ritt44.TabIndex = 108;
            this.ritt44.Text = "4";
            this.ritt44.UseVisualStyleBackColor = false;
            this.ritt44.Click += new System.EventHandler(this.ritt4_Click);
            // 
            // lutz44
            // 
            this.lutz44.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lutz44.BackColor = System.Drawing.Color.Transparent;
            this.lutz44.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lutz44.Enabled = false;
            this.lutz44.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.lutz44.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lutz44.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lutz44.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lutz44.ForeColor = System.Drawing.Color.Yellow;
            this.lutz44.Location = new System.Drawing.Point(334, 183);
            this.lutz44.Margin = new System.Windows.Forms.Padding(0);
            this.lutz44.Name = "lutz44";
            this.lutz44.Size = new System.Drawing.Size(24, 29);
            this.lutz44.TabIndex = 107;
            this.lutz44.Text = "4";
            this.lutz44.UseVisualStyleBackColor = false;
            this.lutz44.Click += new System.EventHandler(this.lutz4_Click);
            // 
            // flip44
            // 
            this.flip44.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flip44.BackColor = System.Drawing.Color.Transparent;
            this.flip44.Cursor = System.Windows.Forms.Cursors.Hand;
            this.flip44.Enabled = false;
            this.flip44.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.flip44.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.flip44.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.flip44.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flip44.ForeColor = System.Drawing.Color.Yellow;
            this.flip44.Location = new System.Drawing.Point(334, 149);
            this.flip44.Margin = new System.Windows.Forms.Padding(0);
            this.flip44.Name = "flip44";
            this.flip44.Size = new System.Drawing.Size(24, 29);
            this.flip44.TabIndex = 106;
            this.flip44.Text = "4";
            this.flip44.UseVisualStyleBackColor = false;
            this.flip44.Click += new System.EventHandler(this.flip4_Click);
            // 
            // sal44
            // 
            this.sal44.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sal44.BackColor = System.Drawing.Color.Transparent;
            this.sal44.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sal44.Enabled = false;
            this.sal44.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.sal44.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.sal44.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sal44.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sal44.ForeColor = System.Drawing.Color.Yellow;
            this.sal44.Location = new System.Drawing.Point(334, 115);
            this.sal44.Margin = new System.Windows.Forms.Padding(0);
            this.sal44.Name = "sal44";
            this.sal44.Size = new System.Drawing.Size(24, 29);
            this.sal44.TabIndex = 105;
            this.sal44.Text = "4";
            this.sal44.UseVisualStyleBackColor = false;
            this.sal44.Click += new System.EventHandler(this.sal4_Click);
            // 
            // toeloop44
            // 
            this.toeloop44.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.toeloop44.BackColor = System.Drawing.Color.Transparent;
            this.toeloop44.Cursor = System.Windows.Forms.Cursors.Hand;
            this.toeloop44.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.toeloop44.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.toeloop44.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.toeloop44.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toeloop44.ForeColor = System.Drawing.Color.Yellow;
            this.toeloop44.Location = new System.Drawing.Point(334, 82);
            this.toeloop44.Margin = new System.Windows.Forms.Padding(0);
            this.toeloop44.Name = "toeloop44";
            this.toeloop44.Size = new System.Drawing.Size(24, 29);
            this.toeloop44.TabIndex = 104;
            this.toeloop44.Text = "4";
            this.toeloop44.UseVisualStyleBackColor = false;
            this.toeloop44.Click += new System.EventHandler(this.toeloop4_Click);
            // 
            // axel44
            // 
            this.axel44.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.axel44.BackColor = System.Drawing.Color.Transparent;
            this.axel44.Cursor = System.Windows.Forms.Cursors.Hand;
            this.axel44.Enabled = false;
            this.axel44.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.axel44.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.axel44.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.axel44.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.axel44.ForeColor = System.Drawing.Color.Yellow;
            this.axel44.Location = new System.Drawing.Point(334, 49);
            this.axel44.Margin = new System.Windows.Forms.Padding(0);
            this.axel44.Name = "axel44";
            this.axel44.Size = new System.Drawing.Size(24, 29);
            this.axel44.TabIndex = 103;
            this.axel44.Text = "4";
            this.axel44.UseVisualStyleBackColor = false;
            this.axel44.Click += new System.EventHandler(this.axel4_Click);
            // 
            // ritt33
            // 
            this.ritt33.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ritt33.BackColor = System.Drawing.Color.Transparent;
            this.ritt33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ritt33.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.ritt33.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.ritt33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ritt33.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ritt33.ForeColor = System.Drawing.Color.Yellow;
            this.ritt33.Location = new System.Drawing.Point(304, 217);
            this.ritt33.Margin = new System.Windows.Forms.Padding(0);
            this.ritt33.Name = "ritt33";
            this.ritt33.Size = new System.Drawing.Size(24, 29);
            this.ritt33.TabIndex = 93;
            this.ritt33.Text = "3";
            this.ritt33.UseVisualStyleBackColor = false;
            this.ritt33.Click += new System.EventHandler(this.ritt3_Click);
            // 
            // ritt22
            // 
            this.ritt22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ritt22.BackColor = System.Drawing.Color.Transparent;
            this.ritt22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ritt22.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.ritt22.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.ritt22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ritt22.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ritt22.ForeColor = System.Drawing.Color.Yellow;
            this.ritt22.Location = new System.Drawing.Point(274, 217);
            this.ritt22.Margin = new System.Windows.Forms.Padding(0);
            this.ritt22.Name = "ritt22";
            this.ritt22.Size = new System.Drawing.Size(24, 29);
            this.ritt22.TabIndex = 92;
            this.ritt22.Text = "2";
            this.ritt22.UseVisualStyleBackColor = false;
            this.ritt22.Click += new System.EventHandler(this.ritt2_Click);
            // 
            // ritt11
            // 
            this.ritt11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ritt11.BackColor = System.Drawing.Color.Transparent;
            this.ritt11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ritt11.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.ritt11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.ritt11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ritt11.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ritt11.ForeColor = System.Drawing.Color.Yellow;
            this.ritt11.Location = new System.Drawing.Point(244, 217);
            this.ritt11.Margin = new System.Windows.Forms.Padding(0);
            this.ritt11.Name = "ritt11";
            this.ritt11.Size = new System.Drawing.Size(24, 29);
            this.ritt11.TabIndex = 91;
            this.ritt11.Text = "1";
            this.ritt11.UseVisualStyleBackColor = false;
            this.ritt11.Click += new System.EventHandler(this.ritt1_Click);
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label14.Font = new System.Drawing.Font("Arial Narrow", 19F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(105, 215);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(268, 32);
            this.label14.TabIndex = 90;
            this.label14.Text = "L O O P";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lutz33
            // 
            this.lutz33.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lutz33.BackColor = System.Drawing.Color.Transparent;
            this.lutz33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lutz33.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.lutz33.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lutz33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lutz33.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lutz33.ForeColor = System.Drawing.Color.Yellow;
            this.lutz33.Location = new System.Drawing.Point(304, 183);
            this.lutz33.Margin = new System.Windows.Forms.Padding(0);
            this.lutz33.Name = "lutz33";
            this.lutz33.Size = new System.Drawing.Size(24, 29);
            this.lutz33.TabIndex = 87;
            this.lutz33.Text = "3";
            this.lutz33.UseVisualStyleBackColor = false;
            this.lutz33.Click += new System.EventHandler(this.lutz3_Click);
            // 
            // lutz22
            // 
            this.lutz22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lutz22.BackColor = System.Drawing.Color.Transparent;
            this.lutz22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lutz22.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.lutz22.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lutz22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lutz22.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lutz22.ForeColor = System.Drawing.Color.Yellow;
            this.lutz22.Location = new System.Drawing.Point(274, 183);
            this.lutz22.Margin = new System.Windows.Forms.Padding(0);
            this.lutz22.Name = "lutz22";
            this.lutz22.Size = new System.Drawing.Size(24, 29);
            this.lutz22.TabIndex = 86;
            this.lutz22.Text = "2";
            this.lutz22.UseVisualStyleBackColor = false;
            this.lutz22.Click += new System.EventHandler(this.lutz2_Click);
            // 
            // lutz11
            // 
            this.lutz11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lutz11.BackColor = System.Drawing.Color.Transparent;
            this.lutz11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lutz11.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.lutz11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lutz11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lutz11.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lutz11.ForeColor = System.Drawing.Color.Yellow;
            this.lutz11.Location = new System.Drawing.Point(244, 183);
            this.lutz11.Margin = new System.Windows.Forms.Padding(0);
            this.lutz11.Name = "lutz11";
            this.lutz11.Size = new System.Drawing.Size(24, 29);
            this.lutz11.TabIndex = 85;
            this.lutz11.Text = "1";
            this.lutz11.UseVisualStyleBackColor = false;
            this.lutz11.Click += new System.EventHandler(this.lutz1_Click);
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label27.Font = new System.Drawing.Font("Arial Narrow", 19F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Green;
            this.label27.Location = new System.Drawing.Point(105, 181);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(268, 32);
            this.label27.TabIndex = 84;
            this.label27.Text = "L U T Z";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flip33
            // 
            this.flip33.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flip33.BackColor = System.Drawing.Color.Transparent;
            this.flip33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.flip33.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.flip33.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.flip33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.flip33.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flip33.ForeColor = System.Drawing.Color.Yellow;
            this.flip33.Location = new System.Drawing.Point(304, 149);
            this.flip33.Margin = new System.Windows.Forms.Padding(0);
            this.flip33.Name = "flip33";
            this.flip33.Size = new System.Drawing.Size(24, 29);
            this.flip33.TabIndex = 81;
            this.flip33.Text = "3";
            this.flip33.UseVisualStyleBackColor = false;
            this.flip33.Click += new System.EventHandler(this.flip3_Click);
            // 
            // flip22
            // 
            this.flip22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flip22.BackColor = System.Drawing.Color.Transparent;
            this.flip22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.flip22.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.flip22.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.flip22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.flip22.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flip22.ForeColor = System.Drawing.Color.Yellow;
            this.flip22.Location = new System.Drawing.Point(274, 149);
            this.flip22.Margin = new System.Windows.Forms.Padding(0);
            this.flip22.Name = "flip22";
            this.flip22.Size = new System.Drawing.Size(24, 29);
            this.flip22.TabIndex = 80;
            this.flip22.Text = "2";
            this.flip22.UseVisualStyleBackColor = false;
            this.flip22.Click += new System.EventHandler(this.flip2_Click);
            // 
            // flip11
            // 
            this.flip11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flip11.BackColor = System.Drawing.Color.Transparent;
            this.flip11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.flip11.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.flip11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.flip11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.flip11.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flip11.ForeColor = System.Drawing.Color.Yellow;
            this.flip11.Location = new System.Drawing.Point(244, 149);
            this.flip11.Margin = new System.Windows.Forms.Padding(0);
            this.flip11.Name = "flip11";
            this.flip11.Size = new System.Drawing.Size(24, 29);
            this.flip11.TabIndex = 79;
            this.flip11.Text = "1";
            this.flip11.UseVisualStyleBackColor = false;
            this.flip11.Click += new System.EventHandler(this.flip1_Click);
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label28.Font = new System.Drawing.Font("Arial Narrow", 19F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label28.Location = new System.Drawing.Point(105, 147);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(268, 32);
            this.label28.TabIndex = 78;
            this.label28.Text = "F L I P";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // sal33
            // 
            this.sal33.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sal33.BackColor = System.Drawing.Color.Transparent;
            this.sal33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sal33.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.sal33.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.sal33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sal33.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sal33.ForeColor = System.Drawing.Color.Yellow;
            this.sal33.Location = new System.Drawing.Point(304, 115);
            this.sal33.Margin = new System.Windows.Forms.Padding(0);
            this.sal33.Name = "sal33";
            this.sal33.Size = new System.Drawing.Size(24, 29);
            this.sal33.TabIndex = 75;
            this.sal33.Text = "3";
            this.sal33.UseVisualStyleBackColor = false;
            this.sal33.Click += new System.EventHandler(this.sal3_Click);
            // 
            // sal22
            // 
            this.sal22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sal22.BackColor = System.Drawing.Color.Transparent;
            this.sal22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sal22.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.sal22.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.sal22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sal22.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sal22.ForeColor = System.Drawing.Color.Yellow;
            this.sal22.Location = new System.Drawing.Point(274, 115);
            this.sal22.Margin = new System.Windows.Forms.Padding(0);
            this.sal22.Name = "sal22";
            this.sal22.Size = new System.Drawing.Size(24, 29);
            this.sal22.TabIndex = 74;
            this.sal22.Text = "2";
            this.sal22.UseVisualStyleBackColor = false;
            this.sal22.Click += new System.EventHandler(this.sal2_Click);
            // 
            // sal11
            // 
            this.sal11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sal11.BackColor = System.Drawing.Color.Transparent;
            this.sal11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sal11.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.sal11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.sal11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sal11.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sal11.ForeColor = System.Drawing.Color.Yellow;
            this.sal11.Location = new System.Drawing.Point(244, 115);
            this.sal11.Margin = new System.Windows.Forms.Padding(0);
            this.sal11.Name = "sal11";
            this.sal11.Size = new System.Drawing.Size(24, 29);
            this.sal11.TabIndex = 73;
            this.sal11.Text = "1";
            this.sal11.UseVisualStyleBackColor = false;
            this.sal11.Click += new System.EventHandler(this.sal1_Click);
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label29.Font = new System.Drawing.Font("Arial Narrow", 19F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Lime;
            this.label29.Location = new System.Drawing.Point(105, 111);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(268, 32);
            this.label29.TabIndex = 72;
            this.label29.Text = "SALCHOW";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toeloop33
            // 
            this.toeloop33.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.toeloop33.BackColor = System.Drawing.Color.Transparent;
            this.toeloop33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.toeloop33.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.toeloop33.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.toeloop33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.toeloop33.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toeloop33.ForeColor = System.Drawing.Color.Yellow;
            this.toeloop33.Location = new System.Drawing.Point(304, 82);
            this.toeloop33.Margin = new System.Windows.Forms.Padding(0);
            this.toeloop33.Name = "toeloop33";
            this.toeloop33.Size = new System.Drawing.Size(24, 29);
            this.toeloop33.TabIndex = 69;
            this.toeloop33.Text = "3";
            this.toeloop33.UseVisualStyleBackColor = false;
            this.toeloop33.Click += new System.EventHandler(this.toeloop3_Click);
            // 
            // toeloop22
            // 
            this.toeloop22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.toeloop22.BackColor = System.Drawing.Color.Transparent;
            this.toeloop22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.toeloop22.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.toeloop22.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.toeloop22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.toeloop22.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toeloop22.ForeColor = System.Drawing.Color.Yellow;
            this.toeloop22.Location = new System.Drawing.Point(274, 82);
            this.toeloop22.Margin = new System.Windows.Forms.Padding(0);
            this.toeloop22.Name = "toeloop22";
            this.toeloop22.Size = new System.Drawing.Size(24, 29);
            this.toeloop22.TabIndex = 68;
            this.toeloop22.Text = "2";
            this.toeloop22.UseVisualStyleBackColor = false;
            this.toeloop22.Click += new System.EventHandler(this.toeloop2_Click);
            // 
            // toeloop11
            // 
            this.toeloop11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.toeloop11.BackColor = System.Drawing.Color.Transparent;
            this.toeloop11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.toeloop11.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.toeloop11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.toeloop11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.toeloop11.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toeloop11.ForeColor = System.Drawing.Color.Yellow;
            this.toeloop11.Location = new System.Drawing.Point(244, 82);
            this.toeloop11.Margin = new System.Windows.Forms.Padding(0);
            this.toeloop11.Name = "toeloop11";
            this.toeloop11.Size = new System.Drawing.Size(24, 29);
            this.toeloop11.TabIndex = 67;
            this.toeloop11.Text = "1";
            this.toeloop11.UseVisualStyleBackColor = false;
            this.toeloop11.Click += new System.EventHandler(this.toeloop1_Click);
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label30.Font = new System.Drawing.Font("Arial Narrow", 19F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label30.Location = new System.Drawing.Point(105, 78);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(268, 32);
            this.label30.TabIndex = 66;
            this.label30.Text = "TOE LOOP";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // axel33
            // 
            this.axel33.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.axel33.BackColor = System.Drawing.Color.Transparent;
            this.axel33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.axel33.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.axel33.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.axel33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.axel33.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.axel33.ForeColor = System.Drawing.Color.Yellow;
            this.axel33.Location = new System.Drawing.Point(304, 49);
            this.axel33.Margin = new System.Windows.Forms.Padding(0);
            this.axel33.Name = "axel33";
            this.axel33.Size = new System.Drawing.Size(24, 29);
            this.axel33.TabIndex = 63;
            this.axel33.Text = "3";
            this.axel33.UseVisualStyleBackColor = false;
            this.axel33.Click += new System.EventHandler(this.axel3_Click);
            // 
            // axel22
            // 
            this.axel22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.axel22.BackColor = System.Drawing.Color.Transparent;
            this.axel22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.axel22.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.axel22.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.axel22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.axel22.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.axel22.ForeColor = System.Drawing.Color.Yellow;
            this.axel22.Location = new System.Drawing.Point(274, 49);
            this.axel22.Margin = new System.Windows.Forms.Padding(0);
            this.axel22.Name = "axel22";
            this.axel22.Size = new System.Drawing.Size(24, 29);
            this.axel22.TabIndex = 62;
            this.axel22.Text = "2";
            this.axel22.UseVisualStyleBackColor = false;
            this.axel22.Click += new System.EventHandler(this.axel2_Click);
            // 
            // axel11
            // 
            this.axel11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.axel11.BackColor = System.Drawing.Color.Transparent;
            this.axel11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.axel11.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.axel11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.axel11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.axel11.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.axel11.ForeColor = System.Drawing.Color.Yellow;
            this.axel11.Location = new System.Drawing.Point(244, 49);
            this.axel11.Margin = new System.Windows.Forms.Padding(0);
            this.axel11.Name = "axel11";
            this.axel11.Size = new System.Drawing.Size(24, 29);
            this.axel11.TabIndex = 61;
            this.axel11.Text = "1";
            this.axel11.UseVisualStyleBackColor = false;
            this.axel11.Click += new System.EventHandler(this.axel1_Click);
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label31.Font = new System.Drawing.Font("Arial Narrow", 19F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label31.Location = new System.Drawing.Point(105, 47);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(268, 32);
            this.label31.TabIndex = 37;
            this.label31.Tag = "6";
            this.label31.Text = "A X E L";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Enabled = false;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Yellow;
            this.button1.Location = new System.Drawing.Point(334, 251);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(24, 29);
            this.button1.TabIndex = 141;
            this.button1.Text = "4";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.el30_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Yellow;
            this.button2.Location = new System.Drawing.Point(304, 251);
            this.button2.Margin = new System.Windows.Forms.Padding(0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(24, 29);
            this.button2.TabIndex = 140;
            this.button2.Text = "3";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.el29_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.BackColor = System.Drawing.Color.Transparent;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.Yellow;
            this.button3.Location = new System.Drawing.Point(274, 251);
            this.button3.Margin = new System.Windows.Forms.Padding(0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(24, 29);
            this.button3.TabIndex = 139;
            this.button3.Text = "2";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.el28_Click);
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.BackColor = System.Drawing.Color.Transparent;
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Yellow;
            this.button4.Location = new System.Drawing.Point(244, 251);
            this.button4.Margin = new System.Windows.Forms.Padding(0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(24, 29);
            this.button4.TabIndex = 138;
            this.button4.Text = "1";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.el27_Click);
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label26.Font = new System.Drawing.Font("Arial Narrow", 19F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label26.Location = new System.Drawing.Point(105, 249);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(268, 32);
            this.label26.TabIndex = 137;
            this.label26.Text = "THOREN";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbJumps
            // 
            this.gbJumps.BackColor = System.Drawing.Color.Transparent;
            this.gbJumps.Controls.Add(this.el30);
            this.gbJumps.Controls.Add(this.el29);
            this.gbJumps.Controls.Add(this.el28);
            this.gbJumps.Controls.Add(this.el27);
            this.gbJumps.Controls.Add(this.label25);
            this.gbJumps.Controls.Add(this.el0);
            this.gbJumps.Controls.Add(this.half);
            this.gbJumps.Controls.Add(this.label33);
            this.gbJumps.Controls.Add(this.under);
            this.gbJumps.Controls.Add(this.down);
            this.gbJumps.Controls.Add(this.el21);
            this.gbJumps.Controls.Add(this.el23);
            this.gbJumps.Controls.Add(this.el22);
            this.gbJumps.Controls.Add(this.el19);
            this.gbJumps.Controls.Add(this.el20);
            this.gbJumps.Controls.Add(this.el24);
            this.gbJumps.Controls.Add(this.el17);
            this.gbJumps.Controls.Add(this.el11);
            this.gbJumps.Controls.Add(this.el5);
            this.gbJumps.Controls.Add(this.label21);
            this.gbJumps.Controls.Add(this.el16);
            this.gbJumps.Controls.Add(this.el10);
            this.gbJumps.Controls.Add(this.el4);
            this.gbJumps.Controls.Add(this.label20);
            this.gbJumps.Controls.Add(this.el15);
            this.gbJumps.Controls.Add(this.el9);
            this.gbJumps.Controls.Add(this.el3);
            this.gbJumps.Controls.Add(this.label19);
            this.gbJumps.Controls.Add(this.el13);
            this.gbJumps.Controls.Add(this.el8);
            this.gbJumps.Controls.Add(this.el2);
            this.gbJumps.Controls.Add(this.label18);
            this.gbJumps.Controls.Add(this.el14);
            this.gbJumps.Controls.Add(this.el7);
            this.gbJumps.Controls.Add(this.el1);
            this.gbJumps.Controls.Add(this.label17);
            this.gbJumps.Controls.Add(this.el18);
            this.gbJumps.Controls.Add(this.el12);
            this.gbJumps.Controls.Add(this.el6);
            this.gbJumps.Controls.Add(this.label16);
            this.gbJumps.Controls.Add(this.el64);
            this.gbJumps.Cursor = System.Windows.Forms.Cursors.Default;
            this.gbJumps.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbJumps.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbJumps.Font = new System.Drawing.Font("Arial Narrow", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbJumps.ForeColor = System.Drawing.Color.Yellow;
            this.gbJumps.Location = new System.Drawing.Point(0, 0);
            this.gbJumps.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbJumps.Name = "gbJumps";
            this.gbJumps.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbJumps.Size = new System.Drawing.Size(378, 295);
            this.gbJumps.TabIndex = 1;
            this.gbJumps.TabStop = false;
            this.gbJumps.Tag = "1";
            // 
            // el30
            // 
            this.el30.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el30.BackColor = System.Drawing.Color.Transparent;
            this.el30.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el30.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el30.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el30.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el30.ForeColor = System.Drawing.Color.Yellow;
            this.el30.Location = new System.Drawing.Point(334, 252);
            this.el30.Margin = new System.Windows.Forms.Padding(0);
            this.el30.Name = "el30";
            this.el30.Size = new System.Drawing.Size(24, 29);
            this.el30.TabIndex = 136;
            this.el30.Text = "4";
            this.el30.UseVisualStyleBackColor = false;
            this.el30.Click += new System.EventHandler(this.el30_Click);
            // 
            // el29
            // 
            this.el29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el29.BackColor = System.Drawing.Color.Transparent;
            this.el29.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el29.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el29.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el29.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el29.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el29.ForeColor = System.Drawing.Color.Yellow;
            this.el29.Location = new System.Drawing.Point(304, 252);
            this.el29.Margin = new System.Windows.Forms.Padding(0);
            this.el29.Name = "el29";
            this.el29.Size = new System.Drawing.Size(24, 29);
            this.el29.TabIndex = 135;
            this.el29.Text = "3";
            this.el29.UseVisualStyleBackColor = false;
            this.el29.Click += new System.EventHandler(this.el29_Click);
            // 
            // el28
            // 
            this.el28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el28.BackColor = System.Drawing.Color.Transparent;
            this.el28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el28.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el28.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el28.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el28.ForeColor = System.Drawing.Color.Yellow;
            this.el28.Location = new System.Drawing.Point(274, 252);
            this.el28.Margin = new System.Windows.Forms.Padding(0);
            this.el28.Name = "el28";
            this.el28.Size = new System.Drawing.Size(24, 29);
            this.el28.TabIndex = 134;
            this.el28.Text = "2";
            this.el28.UseVisualStyleBackColor = false;
            this.el28.Click += new System.EventHandler(this.el28_Click);
            // 
            // el27
            // 
            this.el27.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el27.BackColor = System.Drawing.Color.Transparent;
            this.el27.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el27.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el27.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el27.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el27.ForeColor = System.Drawing.Color.Yellow;
            this.el27.Location = new System.Drawing.Point(244, 252);
            this.el27.Margin = new System.Windows.Forms.Padding(0);
            this.el27.Name = "el27";
            this.el27.Size = new System.Drawing.Size(24, 29);
            this.el27.TabIndex = 133;
            this.el27.Text = "1";
            this.el27.UseVisualStyleBackColor = false;
            this.el27.Click += new System.EventHandler(this.el27_Click);
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label25.Font = new System.Drawing.Font("Arial Narrow", 19F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label25.Location = new System.Drawing.Point(105, 251);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(268, 32);
            this.label25.TabIndex = 132;
            this.label25.Text = "THOREN";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el0
            // 
            this.el0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el0.BackColor = System.Drawing.Color.Transparent;
            this.el0.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el0.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el0.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el0.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el0.ForeColor = System.Drawing.Color.Yellow;
            this.el0.Location = new System.Drawing.Point(304, 19);
            this.el0.Margin = new System.Windows.Forms.Padding(0);
            this.el0.Name = "el0";
            this.el0.Size = new System.Drawing.Size(24, 29);
            this.el0.TabIndex = 131;
            this.el0.Text = "W";
            this.el0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.el0.UseVisualStyleBackColor = false;
            this.el0.Click += new System.EventHandler(this.el0__Click);
            // 
            // half
            // 
            this.half.BackColor = System.Drawing.Color.WhiteSmoke;
            this.half.Cursor = System.Windows.Forms.Cursors.Hand;
            this.half.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.half.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.half.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.half.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.half.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.half.ForeColor = System.Drawing.Color.Black;
            this.half.Location = new System.Drawing.Point(44, 48);
            this.half.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.half.Name = "half";
            this.half.Size = new System.Drawing.Size(24, 232);
            this.half.TabIndex = 130;
            this.half.Text = "HALFROTATED";
            this.half.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.half.UseVisualStyleBackColor = false;
            this.half.Click += new System.EventHandler(this.half_Click);
            // 
            // label33
            // 
            this.label33.AutoEllipsis = true;
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Aqua;
            this.label33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(4, 16);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(137, 29);
            this.label33.TabIndex = 129;
            this.label33.Text = "Solo Jump";
            // 
            // under
            // 
            this.under.BackColor = System.Drawing.Color.WhiteSmoke;
            this.under.Cursor = System.Windows.Forms.Cursors.Hand;
            this.under.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.under.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.under.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.under.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.under.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.under.ForeColor = System.Drawing.Color.Black;
            this.under.Location = new System.Drawing.Point(15, 48);
            this.under.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.under.Name = "under";
            this.under.Size = new System.Drawing.Size(24, 232);
            this.under.TabIndex = 65;
            this.under.Text = "UNDEROTATED";
            this.under.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.under.UseVisualStyleBackColor = false;
            this.under.Click += new System.EventHandler(this.axelT_Click);
            // 
            // down
            // 
            this.down.BackColor = System.Drawing.Color.WhiteSmoke;
            this.down.Cursor = System.Windows.Forms.Cursors.Hand;
            this.down.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.down.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.down.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.down.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.down.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.down.ForeColor = System.Drawing.Color.Black;
            this.down.Location = new System.Drawing.Point(73, 48);
            this.down.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.down.Name = "down";
            this.down.Size = new System.Drawing.Size(24, 232);
            this.down.TabIndex = 64;
            this.down.Text = "DOWNGRADED";
            this.down.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.down.UseVisualStyleBackColor = false;
            this.down.Click += new System.EventHandler(this.axelD_Click);
            // 
            // el21
            // 
            this.el21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el21.BackColor = System.Drawing.Color.Transparent;
            this.el21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el21.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el21.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el21.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el21.ForeColor = System.Drawing.Color.Yellow;
            this.el21.Location = new System.Drawing.Point(334, 219);
            this.el21.Margin = new System.Windows.Forms.Padding(0);
            this.el21.Name = "el21";
            this.el21.Size = new System.Drawing.Size(24, 29);
            this.el21.TabIndex = 108;
            this.el21.Text = "4";
            this.el21.UseVisualStyleBackColor = false;
            this.el21.Click += new System.EventHandler(this.ritt4_Click);
            // 
            // el23
            // 
            this.el23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el23.BackColor = System.Drawing.Color.Transparent;
            this.el23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el23.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el23.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el23.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el23.ForeColor = System.Drawing.Color.Yellow;
            this.el23.Location = new System.Drawing.Point(334, 185);
            this.el23.Margin = new System.Windows.Forms.Padding(0);
            this.el23.Name = "el23";
            this.el23.Size = new System.Drawing.Size(24, 29);
            this.el23.TabIndex = 107;
            this.el23.Text = "4";
            this.el23.UseVisualStyleBackColor = false;
            this.el23.Click += new System.EventHandler(this.lutz4_Click);
            // 
            // el22
            // 
            this.el22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el22.BackColor = System.Drawing.Color.Transparent;
            this.el22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el22.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el22.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el22.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el22.ForeColor = System.Drawing.Color.Yellow;
            this.el22.Location = new System.Drawing.Point(334, 151);
            this.el22.Margin = new System.Windows.Forms.Padding(0);
            this.el22.Name = "el22";
            this.el22.Size = new System.Drawing.Size(24, 29);
            this.el22.TabIndex = 106;
            this.el22.Text = "4";
            this.el22.UseVisualStyleBackColor = false;
            this.el22.Click += new System.EventHandler(this.flip4_Click);
            // 
            // el19
            // 
            this.el19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el19.BackColor = System.Drawing.Color.Transparent;
            this.el19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el19.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el19.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el19.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el19.ForeColor = System.Drawing.Color.Yellow;
            this.el19.Location = new System.Drawing.Point(334, 118);
            this.el19.Margin = new System.Windows.Forms.Padding(0);
            this.el19.Name = "el19";
            this.el19.Size = new System.Drawing.Size(24, 29);
            this.el19.TabIndex = 105;
            this.el19.Text = "4";
            this.el19.UseVisualStyleBackColor = false;
            this.el19.Click += new System.EventHandler(this.sal4_Click);
            // 
            // el20
            // 
            this.el20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el20.BackColor = System.Drawing.Color.Transparent;
            this.el20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el20.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el20.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el20.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el20.ForeColor = System.Drawing.Color.Yellow;
            this.el20.Location = new System.Drawing.Point(334, 84);
            this.el20.Margin = new System.Windows.Forms.Padding(0);
            this.el20.Name = "el20";
            this.el20.Size = new System.Drawing.Size(24, 29);
            this.el20.TabIndex = 104;
            this.el20.Text = "4";
            this.el20.UseVisualStyleBackColor = false;
            this.el20.Click += new System.EventHandler(this.toeloop4_Click);
            // 
            // el24
            // 
            this.el24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el24.BackColor = System.Drawing.Color.Transparent;
            this.el24.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el24.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el24.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el24.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el24.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el24.ForeColor = System.Drawing.Color.Yellow;
            this.el24.Location = new System.Drawing.Point(334, 52);
            this.el24.Margin = new System.Windows.Forms.Padding(0);
            this.el24.Name = "el24";
            this.el24.Size = new System.Drawing.Size(24, 29);
            this.el24.TabIndex = 103;
            this.el24.Text = "4";
            this.el24.UseVisualStyleBackColor = false;
            this.el24.Visible = false;
            this.el24.Click += new System.EventHandler(this.axel4_Click);
            // 
            // el17
            // 
            this.el17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el17.BackColor = System.Drawing.Color.Transparent;
            this.el17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el17.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el17.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el17.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el17.ForeColor = System.Drawing.Color.Yellow;
            this.el17.Location = new System.Drawing.Point(304, 219);
            this.el17.Margin = new System.Windows.Forms.Padding(0);
            this.el17.Name = "el17";
            this.el17.Size = new System.Drawing.Size(24, 29);
            this.el17.TabIndex = 93;
            this.el17.Text = "3";
            this.el17.UseVisualStyleBackColor = false;
            this.el17.Click += new System.EventHandler(this.ritt3_Click);
            // 
            // el11
            // 
            this.el11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el11.BackColor = System.Drawing.Color.Transparent;
            this.el11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el11.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el11.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el11.ForeColor = System.Drawing.Color.Yellow;
            this.el11.Location = new System.Drawing.Point(274, 219);
            this.el11.Margin = new System.Windows.Forms.Padding(0);
            this.el11.Name = "el11";
            this.el11.Size = new System.Drawing.Size(24, 29);
            this.el11.TabIndex = 92;
            this.el11.Text = "2";
            this.el11.UseVisualStyleBackColor = false;
            this.el11.Click += new System.EventHandler(this.ritt2_Click);
            // 
            // el5
            // 
            this.el5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el5.BackColor = System.Drawing.Color.Transparent;
            this.el5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el5.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el5.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el5.ForeColor = System.Drawing.Color.Yellow;
            this.el5.Location = new System.Drawing.Point(244, 219);
            this.el5.Margin = new System.Windows.Forms.Padding(0);
            this.el5.Name = "el5";
            this.el5.Size = new System.Drawing.Size(24, 29);
            this.el5.TabIndex = 91;
            this.el5.Text = "1";
            this.el5.UseVisualStyleBackColor = false;
            this.el5.Click += new System.EventHandler(this.ritt1_Click);
            // 
            // label21
            // 
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label21.Font = new System.Drawing.Font("Arial Narrow", 19F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(105, 217);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(268, 32);
            this.label21.TabIndex = 90;
            this.label21.Text = "L O O P";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el16
            // 
            this.el16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el16.BackColor = System.Drawing.Color.Transparent;
            this.el16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el16.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el16.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el16.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el16.ForeColor = System.Drawing.Color.Yellow;
            this.el16.Location = new System.Drawing.Point(304, 185);
            this.el16.Margin = new System.Windows.Forms.Padding(0);
            this.el16.Name = "el16";
            this.el16.Size = new System.Drawing.Size(24, 29);
            this.el16.TabIndex = 87;
            this.el16.Text = "3";
            this.el16.UseVisualStyleBackColor = false;
            this.el16.Click += new System.EventHandler(this.lutz3_Click);
            // 
            // el10
            // 
            this.el10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el10.BackColor = System.Drawing.Color.Transparent;
            this.el10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el10.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el10.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el10.ForeColor = System.Drawing.Color.Yellow;
            this.el10.Location = new System.Drawing.Point(274, 185);
            this.el10.Margin = new System.Windows.Forms.Padding(0);
            this.el10.Name = "el10";
            this.el10.Size = new System.Drawing.Size(24, 29);
            this.el10.TabIndex = 86;
            this.el10.Text = "2";
            this.el10.UseVisualStyleBackColor = false;
            this.el10.Click += new System.EventHandler(this.lutz2_Click);
            // 
            // el4
            // 
            this.el4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el4.BackColor = System.Drawing.Color.Transparent;
            this.el4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el4.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el4.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el4.ForeColor = System.Drawing.Color.Yellow;
            this.el4.Location = new System.Drawing.Point(244, 185);
            this.el4.Margin = new System.Windows.Forms.Padding(0);
            this.el4.Name = "el4";
            this.el4.Size = new System.Drawing.Size(24, 29);
            this.el4.TabIndex = 85;
            this.el4.Text = "1";
            this.el4.UseVisualStyleBackColor = false;
            this.el4.Click += new System.EventHandler(this.lutz1_Click);
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label20.Font = new System.Drawing.Font("Arial Narrow", 19F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Cyan;
            this.label20.Location = new System.Drawing.Point(105, 183);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(268, 32);
            this.label20.TabIndex = 84;
            this.label20.Text = "L U T Z";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el15
            // 
            this.el15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el15.BackColor = System.Drawing.Color.Transparent;
            this.el15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el15.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el15.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el15.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el15.ForeColor = System.Drawing.Color.Yellow;
            this.el15.Location = new System.Drawing.Point(304, 151);
            this.el15.Margin = new System.Windows.Forms.Padding(0);
            this.el15.Name = "el15";
            this.el15.Size = new System.Drawing.Size(24, 29);
            this.el15.TabIndex = 81;
            this.el15.Text = "3";
            this.el15.UseVisualStyleBackColor = false;
            this.el15.Click += new System.EventHandler(this.flip3_Click);
            // 
            // el9
            // 
            this.el9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el9.BackColor = System.Drawing.Color.Transparent;
            this.el9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el9.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el9.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el9.ForeColor = System.Drawing.Color.Yellow;
            this.el9.Location = new System.Drawing.Point(274, 151);
            this.el9.Margin = new System.Windows.Forms.Padding(0);
            this.el9.Name = "el9";
            this.el9.Size = new System.Drawing.Size(24, 29);
            this.el9.TabIndex = 80;
            this.el9.Text = "2";
            this.el9.UseVisualStyleBackColor = false;
            this.el9.Click += new System.EventHandler(this.flip2_Click);
            // 
            // el3
            // 
            this.el3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el3.BackColor = System.Drawing.Color.Transparent;
            this.el3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el3.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el3.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el3.ForeColor = System.Drawing.Color.Yellow;
            this.el3.Location = new System.Drawing.Point(244, 151);
            this.el3.Margin = new System.Windows.Forms.Padding(0);
            this.el3.Name = "el3";
            this.el3.Size = new System.Drawing.Size(24, 29);
            this.el3.TabIndex = 79;
            this.el3.Text = "1";
            this.el3.UseVisualStyleBackColor = false;
            this.el3.Click += new System.EventHandler(this.flip1_Click);
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label19.Font = new System.Drawing.Font("Arial Narrow", 19F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label19.Location = new System.Drawing.Point(105, 149);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(268, 32);
            this.label19.TabIndex = 78;
            this.label19.Text = "F L I P";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el13
            // 
            this.el13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el13.BackColor = System.Drawing.Color.Transparent;
            this.el13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el13.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el13.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el13.ForeColor = System.Drawing.Color.Yellow;
            this.el13.Location = new System.Drawing.Point(304, 118);
            this.el13.Margin = new System.Windows.Forms.Padding(0);
            this.el13.Name = "el13";
            this.el13.Size = new System.Drawing.Size(24, 29);
            this.el13.TabIndex = 75;
            this.el13.Text = "3";
            this.el13.UseVisualStyleBackColor = false;
            this.el13.Click += new System.EventHandler(this.sal3_Click);
            // 
            // el8
            // 
            this.el8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el8.BackColor = System.Drawing.Color.Transparent;
            this.el8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el8.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el8.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el8.ForeColor = System.Drawing.Color.Yellow;
            this.el8.Location = new System.Drawing.Point(274, 118);
            this.el8.Margin = new System.Windows.Forms.Padding(0);
            this.el8.Name = "el8";
            this.el8.Size = new System.Drawing.Size(24, 29);
            this.el8.TabIndex = 74;
            this.el8.Text = "2";
            this.el8.UseVisualStyleBackColor = false;
            this.el8.Click += new System.EventHandler(this.sal2_Click);
            // 
            // el2
            // 
            this.el2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el2.BackColor = System.Drawing.Color.Transparent;
            this.el2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el2.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el2.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el2.ForeColor = System.Drawing.Color.Yellow;
            this.el2.Location = new System.Drawing.Point(244, 118);
            this.el2.Margin = new System.Windows.Forms.Padding(0);
            this.el2.Name = "el2";
            this.el2.Size = new System.Drawing.Size(24, 29);
            this.el2.TabIndex = 73;
            this.el2.Text = "1";
            this.el2.UseVisualStyleBackColor = false;
            this.el2.Click += new System.EventHandler(this.sal1_Click);
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label18.Font = new System.Drawing.Font("Arial Narrow", 19F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Cyan;
            this.label18.Location = new System.Drawing.Point(105, 116);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(268, 32);
            this.label18.TabIndex = 72;
            this.label18.Text = "SALCHOW";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el14
            // 
            this.el14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el14.BackColor = System.Drawing.Color.Transparent;
            this.el14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el14.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el14.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el14.ForeColor = System.Drawing.Color.Yellow;
            this.el14.Location = new System.Drawing.Point(304, 84);
            this.el14.Margin = new System.Windows.Forms.Padding(0);
            this.el14.Name = "el14";
            this.el14.Size = new System.Drawing.Size(24, 29);
            this.el14.TabIndex = 69;
            this.el14.Text = "3";
            this.el14.UseVisualStyleBackColor = false;
            this.el14.Click += new System.EventHandler(this.toeloop3_Click);
            // 
            // el7
            // 
            this.el7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el7.BackColor = System.Drawing.Color.Transparent;
            this.el7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el7.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el7.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el7.ForeColor = System.Drawing.Color.Yellow;
            this.el7.Location = new System.Drawing.Point(274, 84);
            this.el7.Margin = new System.Windows.Forms.Padding(0);
            this.el7.Name = "el7";
            this.el7.Size = new System.Drawing.Size(24, 29);
            this.el7.TabIndex = 68;
            this.el7.Text = "2";
            this.el7.UseVisualStyleBackColor = false;
            this.el7.Click += new System.EventHandler(this.toeloop2_Click);
            // 
            // el1
            // 
            this.el1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el1.BackColor = System.Drawing.Color.Transparent;
            this.el1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el1.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el1.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el1.ForeColor = System.Drawing.Color.Yellow;
            this.el1.Location = new System.Drawing.Point(244, 84);
            this.el1.Margin = new System.Windows.Forms.Padding(0);
            this.el1.Name = "el1";
            this.el1.Size = new System.Drawing.Size(24, 29);
            this.el1.TabIndex = 67;
            this.el1.Text = "1";
            this.el1.UseVisualStyleBackColor = false;
            this.el1.Click += new System.EventHandler(this.toeloop1_Click);
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label17.Font = new System.Drawing.Font("Arial Narrow", 19F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label17.Location = new System.Drawing.Point(105, 81);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(268, 32);
            this.label17.TabIndex = 66;
            this.label17.Text = "TOE LOOP";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el18
            // 
            this.el18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el18.BackColor = System.Drawing.Color.Transparent;
            this.el18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el18.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el18.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el18.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el18.ForeColor = System.Drawing.Color.Yellow;
            this.el18.Location = new System.Drawing.Point(304, 52);
            this.el18.Margin = new System.Windows.Forms.Padding(0);
            this.el18.Name = "el18";
            this.el18.Size = new System.Drawing.Size(24, 29);
            this.el18.TabIndex = 63;
            this.el18.Text = "3";
            this.el18.UseVisualStyleBackColor = false;
            this.el18.Click += new System.EventHandler(this.axel3_Click);
            // 
            // el12
            // 
            this.el12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el12.BackColor = System.Drawing.Color.Transparent;
            this.el12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el12.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el12.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el12.ForeColor = System.Drawing.Color.Yellow;
            this.el12.Location = new System.Drawing.Point(274, 52);
            this.el12.Margin = new System.Windows.Forms.Padding(0);
            this.el12.Name = "el12";
            this.el12.Size = new System.Drawing.Size(24, 29);
            this.el12.TabIndex = 62;
            this.el12.Text = "2";
            this.el12.UseVisualStyleBackColor = false;
            this.el12.Click += new System.EventHandler(this.axel2_Click);
            // 
            // el6
            // 
            this.el6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el6.BackColor = System.Drawing.Color.Transparent;
            this.el6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el6.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el6.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el6.ForeColor = System.Drawing.Color.Yellow;
            this.el6.Location = new System.Drawing.Point(244, 52);
            this.el6.Margin = new System.Windows.Forms.Padding(0);
            this.el6.Name = "el6";
            this.el6.Size = new System.Drawing.Size(24, 29);
            this.el6.TabIndex = 61;
            this.el6.Text = "1";
            this.el6.UseVisualStyleBackColor = false;
            this.el6.Click += new System.EventHandler(this.axel1_Click);
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label16.Font = new System.Drawing.Font("Arial Narrow", 19F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label16.Location = new System.Drawing.Point(105, 48);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(268, 32);
            this.label16.TabIndex = 37;
            this.label16.Tag = "6";
            this.label16.Text = "A X E L";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el64
            // 
            this.el64.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el64.BackColor = System.Drawing.Color.Transparent;
            this.el64.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el64.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el64.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el64.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el64.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el64.ForeColor = System.Drawing.Color.Yellow;
            this.el64.Location = new System.Drawing.Point(334, 19);
            this.el64.Margin = new System.Windows.Forms.Padding(0);
            this.el64.Name = "el64";
            this.el64.Size = new System.Drawing.Size(24, 29);
            this.el64.TabIndex = 36;
            this.el64.Text = "0";
            this.el64.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.el64.UseVisualStyleBackColor = false;
            this.el64.Click += new System.EventHandler(this.noj_Click);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.gbSteps);
            this.panel6.Controls.Add(this.ded1);
            this.panel6.Controls.Add(this.gbSpins);
            this.panel6.Controls.Add(this.label24);
            this.panel6.Controls.Add(this.log);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(794, 84);
            this.panel6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(368, 640);
            this.panel6.TabIndex = 12;
            // 
            // gbSteps
            // 
            this.gbSteps.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.gbSteps.Controls.Add(this.el73);
            this.gbSteps.Controls.Add(this.el76);
            this.gbSteps.Controls.Add(this.label1);
            this.gbSteps.Controls.Add(this.el63);
            this.gbSteps.Controls.Add(this.el59);
            this.gbSteps.Controls.Add(this.el60);
            this.gbSteps.Controls.Add(this.el61);
            this.gbSteps.Controls.Add(this.el62);
            this.gbSteps.Controls.Add(this.label15);
            this.gbSteps.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbSteps.Location = new System.Drawing.Point(0, 332);
            this.gbSteps.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbSteps.Name = "gbSteps";
            this.gbSteps.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbSteps.Size = new System.Drawing.Size(368, 142);
            this.gbSteps.TabIndex = 4;
            this.gbSteps.TabStop = false;
            // 
            // el73
            // 
            this.el73.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el73.BackColor = System.Drawing.Color.Transparent;
            this.el73.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el73.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el73.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el73.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el73.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el73.ForeColor = System.Drawing.Color.Yellow;
            this.el73.Location = new System.Drawing.Point(174, 95);
            this.el73.Margin = new System.Windows.Forms.Padding(0);
            this.el73.Name = "el73";
            this.el73.Size = new System.Drawing.Size(174, 32);
            this.el73.TabIndex = 179;
            this.el73.Text = "CHOREO SEQUENCE";
            this.el73.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.el73.UseVisualStyleBackColor = false;
            this.el73.Click += new System.EventHandler(this.el73_Click);
            // 
            // el76
            // 
            this.el76.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el76.BackColor = System.Drawing.Color.Transparent;
            this.el76.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el76.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el76.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el76.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el76.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el76.ForeColor = System.Drawing.Color.Yellow;
            this.el76.Location = new System.Drawing.Point(174, 60);
            this.el76.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.el76.Name = "el76";
            this.el76.Size = new System.Drawing.Size(24, 29);
            this.el76.TabIndex = 178;
            this.el76.Text = "0";
            this.el76.UseVisualStyleBackColor = false;
            this.el76.Click += new System.EventHandler(this.s0_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoEllipsis = true;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(157, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(260, 29);
            this.label1.TabIndex = 176;
            this.label1.Text = "Footwork Sequences";
            // 
            // el63
            // 
            this.el63.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el63.BackColor = System.Drawing.Color.Transparent;
            this.el63.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el63.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el63.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el63.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el63.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el63.ForeColor = System.Drawing.Color.Yellow;
            this.el63.Location = new System.Drawing.Point(324, 60);
            this.el63.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.el63.Name = "el63";
            this.el63.Size = new System.Drawing.Size(24, 29);
            this.el63.TabIndex = 62;
            this.el63.Text = "5";
            this.el63.UseVisualStyleBackColor = false;
            this.el63.Click += new System.EventHandler(this.s5_Click);
            // 
            // el59
            // 
            this.el59.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el59.BackColor = System.Drawing.Color.Transparent;
            this.el59.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el59.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el59.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el59.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el59.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el59.ForeColor = System.Drawing.Color.Yellow;
            this.el59.Location = new System.Drawing.Point(204, 60);
            this.el59.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.el59.Name = "el59";
            this.el59.Size = new System.Drawing.Size(24, 29);
            this.el59.TabIndex = 58;
            this.el59.Text = "1";
            this.el59.UseVisualStyleBackColor = false;
            this.el59.Click += new System.EventHandler(this.s1_Click);
            // 
            // el60
            // 
            this.el60.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el60.BackColor = System.Drawing.Color.Transparent;
            this.el60.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el60.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el60.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el60.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el60.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el60.ForeColor = System.Drawing.Color.Yellow;
            this.el60.Location = new System.Drawing.Point(234, 60);
            this.el60.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.el60.Name = "el60";
            this.el60.Size = new System.Drawing.Size(24, 29);
            this.el60.TabIndex = 59;
            this.el60.Text = "2";
            this.el60.UseVisualStyleBackColor = false;
            this.el60.Click += new System.EventHandler(this.s2_Click);
            // 
            // el61
            // 
            this.el61.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el61.BackColor = System.Drawing.Color.Transparent;
            this.el61.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el61.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el61.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el61.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el61.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el61.ForeColor = System.Drawing.Color.Yellow;
            this.el61.Location = new System.Drawing.Point(264, 60);
            this.el61.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.el61.Name = "el61";
            this.el61.Size = new System.Drawing.Size(24, 29);
            this.el61.TabIndex = 60;
            this.el61.Text = "3";
            this.el61.UseVisualStyleBackColor = false;
            this.el61.Click += new System.EventHandler(this.s3_Click);
            // 
            // el62
            // 
            this.el62.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el62.BackColor = System.Drawing.Color.Transparent;
            this.el62.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el62.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el62.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el62.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el62.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el62.ForeColor = System.Drawing.Color.Yellow;
            this.el62.Location = new System.Drawing.Point(294, 60);
            this.el62.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.el62.Name = "el62";
            this.el62.Size = new System.Drawing.Size(24, 29);
            this.el62.TabIndex = 61;
            this.el62.Text = "4";
            this.el62.UseVisualStyleBackColor = false;
            this.el62.Click += new System.EventHandler(this.s4_Click);
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label15.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label15.Location = new System.Drawing.Point(14, 60);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(181, 32);
            this.label15.TabIndex = 38;
            this.label15.Tag = "6";
            this.label15.Text = "STEPS LEVEL";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ded1
            // 
            this.ded1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ded1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ded1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ded1.Font = new System.Drawing.Font("Calibri", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded1.ForeColor = System.Drawing.Color.Red;
            this.ded1.Location = new System.Drawing.Point(273, 489);
            this.ded1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ded1.Name = "ded1";
            this.ded1.Size = new System.Drawing.Size(75, 48);
            this.ded1.TabIndex = 167;
            this.ded1.ValueChanged += new System.EventHandler(this.ded1_ValueChanged);
            // 
            // gbSpins
            // 
            this.gbSpins.BackColor = System.Drawing.Color.Transparent;
            this.gbSpins.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.gbSpins.Controls.Add(this.label34);
            this.gbSpins.Controls.Add(this.cbBonusSpin1);
            this.gbSpins.Controls.Add(this.cbBonusSpin2);
            this.gbSpins.Controls.Add(this.cbCombo);
            this.gbSpins.Controls.Add(this.el26);
            this.gbSpins.Controls.Add(this.el41);
            this.gbSpins.Controls.Add(this.el46);
            this.gbSpins.Controls.Add(this.el50);
            this.gbSpins.Controls.Add(this.el51);
            this.gbSpins.Controls.Add(this.el52);
            this.gbSpins.Controls.Add(this.label35);
            this.gbSpins.Controls.Add(this.checkCombo2);
            this.gbSpins.Controls.Add(this.newComboSpin);
            this.gbSpins.Controls.Add(this.cb49);
            this.gbSpins.Controls.Add(this.cb47);
            this.gbSpins.Controls.Add(this.cb48);
            this.gbSpins.Controls.Add(this.cb45);
            this.gbSpins.Controls.Add(this.cb40);
            this.gbSpins.Controls.Add(this.cb25);
            this.gbSpins.Controls.Add(this.el25);
            this.gbSpins.Controls.Add(this.el40);
            this.gbSpins.Controls.Add(this.el45);
            this.gbSpins.Controls.Add(this.el48);
            this.gbSpins.Controls.Add(this.el47);
            this.gbSpins.Controls.Add(this.el49);
            this.gbSpins.Controls.Add(this.label5);
            this.gbSpins.Controls.Add(this.label3);
            this.gbSpins.Controls.Add(this.label6);
            this.gbSpins.Controls.Add(this.label9);
            this.gbSpins.Controls.Add(this.label8);
            this.gbSpins.Controls.Add(this.label7);
            this.gbSpins.Controls.Add(this.el65);
            this.gbSpins.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbSpins.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbSpins.Location = new System.Drawing.Point(0, 0);
            this.gbSpins.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbSpins.Name = "gbSpins";
            this.gbSpins.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbSpins.Size = new System.Drawing.Size(368, 332);
            this.gbSpins.TabIndex = 2;
            this.gbSpins.TabStop = false;
            this.gbSpins.Tag = "1";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Yellow;
            this.label34.Location = new System.Drawing.Point(221, 68);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(56, 24);
            this.label34.TabIndex = 170;
            this.label34.Text = "Bonus";
            // 
            // cbBonusSpin1
            // 
            this.cbBonusSpin1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbBonusSpin1.AutoSize = true;
            this.cbBonusSpin1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbBonusSpin1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBonusSpin1.ForeColor = System.Drawing.Color.Yellow;
            this.cbBonusSpin1.Location = new System.Drawing.Point(254, 68);
            this.cbBonusSpin1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbBonusSpin1.Name = "cbBonusSpin1";
            this.cbBonusSpin1.Size = new System.Drawing.Size(51, 28);
            this.cbBonusSpin1.TabIndex = 169;
            this.cbBonusSpin1.Text = "+1";
            this.cbBonusSpin1.UseVisualStyleBackColor = true;
            this.cbBonusSpin1.CheckedChanged += new System.EventHandler(this.cbBonusSpin2_CheckedChanged);
            // 
            // cbBonusSpin2
            // 
            this.cbBonusSpin2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbBonusSpin2.AutoSize = true;
            this.cbBonusSpin2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbBonusSpin2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBonusSpin2.ForeColor = System.Drawing.Color.Yellow;
            this.cbBonusSpin2.Location = new System.Drawing.Point(303, 68);
            this.cbBonusSpin2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbBonusSpin2.Name = "cbBonusSpin2";
            this.cbBonusSpin2.Size = new System.Drawing.Size(51, 28);
            this.cbBonusSpin2.TabIndex = 152;
            this.cbBonusSpin2.Text = "+2";
            this.cbBonusSpin2.UseVisualStyleBackColor = true;
            this.cbBonusSpin2.CheckedChanged += new System.EventHandler(this.cbBonusSpin2_CheckedChanged_1);
            // 
            // cbCombo
            // 
            this.cbCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCombo.BackColor = System.Drawing.SystemColors.InfoText;
            this.cbCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCombo.Enabled = false;
            this.cbCombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbCombo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCombo.ForeColor = System.Drawing.Color.Lime;
            this.cbCombo.FormattingEnabled = true;
            this.cbCombo.Items.AddRange(new object[] {
            "0 %",
            "5 %",
            "10 %",
            "15 %",
            "20 %",
            "25 %",
            "30 %",
            "35 %",
            "40 %",
            "45 %",
            "50 %",
            "55 %",
            "60 %",
            "70 %",
            "75 %",
            "80 %",
            "85 %",
            "90 %",
            "95 %",
            "100 %"});
            this.cbCombo.Location = new System.Drawing.Point(204, 26);
            this.cbCombo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbCombo.Name = "cbCombo";
            this.cbCombo.Size = new System.Drawing.Size(67, 33);
            this.cbCombo.TabIndex = 168;
            this.cbCombo.Visible = false;
            // 
            // el26
            // 
            this.el26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el26.BackColor = System.Drawing.Color.Transparent;
            this.el26.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el26.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el26.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el26.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el26.ForeColor = System.Drawing.Color.Yellow;
            this.el26.Location = new System.Drawing.Point(218, 102);
            this.el26.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.el26.Name = "el26";
            this.el26.Size = new System.Drawing.Size(24, 29);
            this.el26.TabIndex = 167;
            this.el26.Tag = "cbUp";
            this.el26.Text = "0";
            this.el26.UseVisualStyleBackColor = false;
            this.el26.Click += new System.EventHandler(this.el26_Click);
            // 
            // el41
            // 
            this.el41.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el41.BackColor = System.Drawing.Color.Transparent;
            this.el41.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el41.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el41.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el41.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el41.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el41.ForeColor = System.Drawing.Color.Yellow;
            this.el41.Location = new System.Drawing.Point(218, 135);
            this.el41.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.el41.Name = "el41";
            this.el41.Size = new System.Drawing.Size(24, 29);
            this.el41.TabIndex = 166;
            this.el41.Tag = "cbSit";
            this.el41.Text = "0";
            this.el41.UseVisualStyleBackColor = false;
            this.el41.Click += new System.EventHandler(this.el26_Click);
            // 
            // el46
            // 
            this.el46.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el46.BackColor = System.Drawing.Color.Transparent;
            this.el46.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el46.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el46.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el46.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el46.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el46.ForeColor = System.Drawing.Color.Yellow;
            this.el46.Location = new System.Drawing.Point(218, 167);
            this.el46.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.el46.Name = "el46";
            this.el46.Size = new System.Drawing.Size(24, 29);
            this.el46.TabIndex = 165;
            this.el46.Tag = "cbCamel";
            this.el46.Text = "0";
            this.el46.UseVisualStyleBackColor = false;
            this.el46.Click += new System.EventHandler(this.el26_Click);
            // 
            // el50
            // 
            this.el50.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el50.BackColor = System.Drawing.Color.Transparent;
            this.el50.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el50.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el50.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el50.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el50.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el50.ForeColor = System.Drawing.Color.Yellow;
            this.el50.Location = new System.Drawing.Point(218, 234);
            this.el50.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.el50.Name = "el50";
            this.el50.Size = new System.Drawing.Size(24, 29);
            this.el50.TabIndex = 164;
            this.el50.Tag = "cbHeel";
            this.el50.Text = "0";
            this.el50.UseVisualStyleBackColor = false;
            this.el50.Click += new System.EventHandler(this.el26_Click);
            // 
            // el51
            // 
            this.el51.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el51.BackColor = System.Drawing.Color.Transparent;
            this.el51.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el51.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el51.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el51.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el51.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el51.ForeColor = System.Drawing.Color.Yellow;
            this.el51.Location = new System.Drawing.Point(218, 201);
            this.el51.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.el51.Name = "el51";
            this.el51.Size = new System.Drawing.Size(24, 29);
            this.el51.TabIndex = 163;
            this.el51.Tag = "cbBroken";
            this.el51.Text = "0";
            this.el51.UseVisualStyleBackColor = false;
            this.el51.Click += new System.EventHandler(this.el26_Click);
            // 
            // el52
            // 
            this.el52.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el52.BackColor = System.Drawing.Color.Transparent;
            this.el52.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el52.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el52.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el52.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el52.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el52.ForeColor = System.Drawing.Color.Yellow;
            this.el52.Location = new System.Drawing.Point(218, 267);
            this.el52.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.el52.Name = "el52";
            this.el52.Size = new System.Drawing.Size(24, 29);
            this.el52.TabIndex = 162;
            this.el52.Tag = "cbInverted";
            this.el52.Text = "0";
            this.el52.UseVisualStyleBackColor = false;
            this.el52.Click += new System.EventHandler(this.el26_Click);
            // 
            // label35
            // 
            this.label35.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label35.AutoEllipsis = true;
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label35.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(286, 14);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(96, 36);
            this.label35.TabIndex = 147;
            this.label35.Text = "Spins";
            // 
            // checkCombo2
            // 
            this.checkCombo2.BackColor = System.Drawing.Color.Black;
            this.checkCombo2.Location = new System.Drawing.Point(21, 24);
            this.checkCombo2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkCombo2.Name = "checkCombo2";
            this.checkCombo2.OffFont = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkCombo2.OffForeColor = System.Drawing.Color.Yellow;
            this.checkCombo2.OffText = "SoloSpin";
            this.checkCombo2.OnFont = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkCombo2.OnForeColor = System.Drawing.Color.Yellow;
            this.checkCombo2.OnText = "ComboSpin";
            this.checkCombo2.Size = new System.Drawing.Size(175, 30);
            this.checkCombo2.Style = JCS.ToggleSwitch.ToggleSwitchStyle.Fancy;
            this.checkCombo2.TabIndex = 161;
            this.checkCombo2.CheckedChanged += new JCS.ToggleSwitch.CheckedChangedDelegate(this.checkCombo2_CheckedChanged);
            // 
            // newComboSpin
            // 
            this.newComboSpin.BackColor = System.Drawing.Color.Transparent;
            this.newComboSpin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.newComboSpin.Enabled = false;
            this.newComboSpin.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.newComboSpin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.newComboSpin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.newComboSpin.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newComboSpin.ForeColor = System.Drawing.Color.Yellow;
            this.newComboSpin.Location = new System.Drawing.Point(21, 64);
            this.newComboSpin.Margin = new System.Windows.Forms.Padding(0);
            this.newComboSpin.Name = "newComboSpin";
            this.newComboSpin.Size = new System.Drawing.Size(105, 29);
            this.newComboSpin.TabIndex = 132;
            this.newComboSpin.Text = "New Spin";
            this.newComboSpin.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.newComboSpin.UseVisualStyleBackColor = false;
            this.newComboSpin.Click += new System.EventHandler(this.newComboSpin_Click);
            // 
            // cb49
            // 
            this.cb49.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb49.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb49.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb49.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb49.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb49.ForeColor = System.Drawing.Color.Yellow;
            this.cb49.FormattingEnabled = true;
            this.cb49.Location = new System.Drawing.Point(250, 268);
            this.cb49.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb49.Name = "cb49";
            this.cb49.Size = new System.Drawing.Size(67, 33);
            this.cb49.TabIndex = 158;
            // 
            // cb47
            // 
            this.cb47.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb47.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb47.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb47.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb47.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb47.ForeColor = System.Drawing.Color.Yellow;
            this.cb47.FormattingEnabled = true;
            this.cb47.Location = new System.Drawing.Point(250, 235);
            this.cb47.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb47.Name = "cb47";
            this.cb47.Size = new System.Drawing.Size(67, 33);
            this.cb47.TabIndex = 157;
            // 
            // cb48
            // 
            this.cb48.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb48.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb48.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb48.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb48.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb48.ForeColor = System.Drawing.Color.Yellow;
            this.cb48.FormattingEnabled = true;
            this.cb48.Location = new System.Drawing.Point(250, 201);
            this.cb48.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb48.Name = "cb48";
            this.cb48.Size = new System.Drawing.Size(67, 33);
            this.cb48.TabIndex = 156;
            // 
            // cb45
            // 
            this.cb45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb45.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb45.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb45.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb45.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb45.ForeColor = System.Drawing.Color.Yellow;
            this.cb45.FormattingEnabled = true;
            this.cb45.Location = new System.Drawing.Point(250, 168);
            this.cb45.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb45.Name = "cb45";
            this.cb45.Size = new System.Drawing.Size(67, 33);
            this.cb45.TabIndex = 155;
            // 
            // cb40
            // 
            this.cb40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb40.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb40.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb40.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb40.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb40.ForeColor = System.Drawing.Color.Yellow;
            this.cb40.FormattingEnabled = true;
            this.cb40.Location = new System.Drawing.Point(250, 135);
            this.cb40.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb40.Name = "cb40";
            this.cb40.Size = new System.Drawing.Size(67, 33);
            this.cb40.TabIndex = 154;
            // 
            // cb25
            // 
            this.cb25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb25.BackColor = System.Drawing.SystemColors.InfoText;
            this.cb25.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb25.ForeColor = System.Drawing.Color.Yellow;
            this.cb25.FormattingEnabled = true;
            this.cb25.Location = new System.Drawing.Point(250, 102);
            this.cb25.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb25.Name = "cb25";
            this.cb25.Size = new System.Drawing.Size(67, 33);
            this.cb25.TabIndex = 153;
            // 
            // el25
            // 
            this.el25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el25.BackColor = System.Drawing.Color.Transparent;
            this.el25.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el25.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el25.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el25.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el25.ForeColor = System.Drawing.Color.Yellow;
            this.el25.Location = new System.Drawing.Point(326, 102);
            this.el25.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.el25.Name = "el25";
            this.el25.Size = new System.Drawing.Size(24, 29);
            this.el25.TabIndex = 52;
            this.el25.Tag = "cbUp";
            this.el25.Text = "1";
            this.el25.UseVisualStyleBackColor = false;
            this.el25.Click += new System.EventHandler(this.us1_Click);
            // 
            // el40
            // 
            this.el40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el40.BackColor = System.Drawing.Color.Transparent;
            this.el40.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el40.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el40.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el40.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el40.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el40.ForeColor = System.Drawing.Color.Yellow;
            this.el40.Location = new System.Drawing.Point(326, 135);
            this.el40.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.el40.Name = "el40";
            this.el40.Size = new System.Drawing.Size(24, 29);
            this.el40.TabIndex = 42;
            this.el40.Tag = "cbSit";
            this.el40.Text = "1";
            this.el40.UseVisualStyleBackColor = false;
            this.el40.Click += new System.EventHandler(this.ss1_Click);
            // 
            // el45
            // 
            this.el45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el45.BackColor = System.Drawing.Color.Transparent;
            this.el45.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el45.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el45.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el45.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el45.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el45.ForeColor = System.Drawing.Color.Yellow;
            this.el45.Location = new System.Drawing.Point(326, 167);
            this.el45.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.el45.Name = "el45";
            this.el45.Size = new System.Drawing.Size(24, 29);
            this.el45.TabIndex = 35;
            this.el45.Tag = "cbCamel";
            this.el45.Text = "1";
            this.el45.UseVisualStyleBackColor = false;
            this.el45.Click += new System.EventHandler(this.cs1_Click);
            // 
            // el48
            // 
            this.el48.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el48.BackColor = System.Drawing.Color.Transparent;
            this.el48.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el48.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el48.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el48.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el48.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el48.ForeColor = System.Drawing.Color.Yellow;
            this.el48.Location = new System.Drawing.Point(326, 201);
            this.el48.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.el48.Name = "el48";
            this.el48.Size = new System.Drawing.Size(24, 29);
            this.el48.TabIndex = 31;
            this.el48.Tag = "cbHeel";
            this.el48.Text = "1";
            this.el48.UseVisualStyleBackColor = false;
            this.el48.Click += new System.EventHandler(this.he3_Click);
            // 
            // el47
            // 
            this.el47.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el47.BackColor = System.Drawing.Color.Transparent;
            this.el47.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el47.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el47.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el47.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el47.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el47.ForeColor = System.Drawing.Color.Yellow;
            this.el47.Location = new System.Drawing.Point(326, 234);
            this.el47.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.el47.Name = "el47";
            this.el47.Size = new System.Drawing.Size(24, 29);
            this.el47.TabIndex = 27;
            this.el47.Tag = "cbBroken";
            this.el47.Text = "1";
            this.el47.UseVisualStyleBackColor = false;
            this.el47.Click += new System.EventHandler(this.br3_Click);
            // 
            // el49
            // 
            this.el49.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el49.BackColor = System.Drawing.Color.Transparent;
            this.el49.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el49.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el49.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el49.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el49.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el49.ForeColor = System.Drawing.Color.Yellow;
            this.el49.Location = new System.Drawing.Point(326, 267);
            this.el49.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.el49.Name = "el49";
            this.el49.Size = new System.Drawing.Size(24, 29);
            this.el49.TabIndex = 22;
            this.el49.Tag = "cbInverted";
            this.el49.Text = "1";
            this.el49.UseVisualStyleBackColor = false;
            this.el49.Click += new System.EventHandler(this.in3_Click);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label5.Location = new System.Drawing.Point(18, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(187, 32);
            this.label5.TabIndex = 38;
            this.label5.Tag = "6";
            this.label5.Text = "UPRIGHT SPIN";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label3.Location = new System.Drawing.Point(18, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(184, 32);
            this.label3.TabIndex = 142;
            this.label3.Tag = "6";
            this.label3.Text = "SIT SPIN";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label6.Location = new System.Drawing.Point(18, 169);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(187, 32);
            this.label6.TabIndex = 143;
            this.label6.Text = "CAMEL SPIN";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label9.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(18, 267);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(211, 32);
            this.label9.TabIndex = 146;
            this.label9.Text = "I N V E R T E D";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(18, 233);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(178, 32);
            this.label8.TabIndex = 145;
            this.label8.Text = "B R O K E N";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label7.Location = new System.Drawing.Point(18, 200);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(151, 32);
            this.label7.TabIndex = 144;
            this.label7.Text = "H E E L";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el65
            // 
            this.el65.BackColor = System.Drawing.Color.Transparent;
            this.el65.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el65.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el65.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el65.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el65.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el65.ForeColor = System.Drawing.Color.Yellow;
            this.el65.Location = new System.Drawing.Point(21, 64);
            this.el65.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.el65.Name = "el65";
            this.el65.Size = new System.Drawing.Size(24, 29);
            this.el65.TabIndex = 101;
            this.el65.Text = "0";
            this.el65.UseVisualStyleBackColor = false;
            this.el65.Visible = false;
            this.el65.Click += new System.EventHandler(this.ns_Click);
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label24.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(168, 492);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(107, 40);
            this.label24.TabIndex = 162;
            this.label24.Tag = "6";
            this.label24.Text = "FALLS";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label24.Click += new System.EventHandler(this.label24_Click);
            // 
            // log
            // 
            this.log.BackColor = System.Drawing.Color.Black;
            this.log.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.log.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.log.Font = new System.Drawing.Font("Courier New", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.log.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.log.Location = new System.Drawing.Point(0, 551);
            this.log.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.log.Multiline = true;
            this.log.Name = "log";
            this.log.ReadOnly = true;
            this.log.Size = new System.Drawing.Size(368, 89);
            this.log.TabIndex = 71;
            this.log.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // shapeContainer7
            // 
            this.shapeContainer7.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer7.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer7.Name = "shapeContainer7";
            this.shapeContainer7.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape7});
            this.shapeContainer7.Size = new System.Drawing.Size(89, 43);
            this.shapeContainer7.TabIndex = 99;
            this.shapeContainer7.TabStop = false;
            // 
            // rectangleShape7
            // 
            this.rectangleShape7.BorderColor = System.Drawing.Color.Magenta;
            this.rectangleShape7.BorderWidth = 2;
            this.rectangleShape7.CornerRadius = 5;
            this.rectangleShape7.FillColor = System.Drawing.Color.Transparent;
            this.rectangleShape7.Location = new System.Drawing.Point(3, 1);
            this.rectangleShape7.Name = "rectangleShape7";
            this.rectangleShape7.Size = new System.Drawing.Size(82, 34);
            // 
            // imList
            // 
            this.imList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imList.ImageStream")));
            this.imList.TransparentColor = System.Drawing.Color.Transparent;
            this.imList.Images.SetKeyName(0, "time.png");
            // 
            // tt2
            // 
            this.tt2.AutoPopDelay = 5000;
            this.tt2.InitialDelay = 100;
            this.tt2.IsBalloon = true;
            this.tt2.ReshowDelay = 100;
            this.tt2.ShowAlways = true;
            this.tt2.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.tt2.ToolTipTitle = "Description:";
            // 
            // SingoloForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1165, 774);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "SingoloForm";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Singolo - ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SingoloForm_FormClosing);
            this.Load += new System.EventHandler(this.SPSingoloForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.gbElements.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.gbDed.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ded6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded2)).EndInit();
            this.gbComboJumps.ResumeLayout(false);
            this.gbComboJumps.PerformLayout();
            this.gbJumps.ResumeLayout(false);
            this.gbJumps.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.gbSteps.ResumeLayout(false);
            this.gbSteps.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ded1)).EndInit();
            this.gbSpins.ResumeLayout(false);
            this.gbSpins.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.GroupBox gbElements;
        private System.Windows.Forms.Button j9;
        private System.Windows.Forms.Button j8;
        private System.Windows.Forms.Button j7;
        private System.Windows.Forms.Button j6;
        private System.Windows.Forms.Button j5;
        private System.Windows.Forms.Button j4;
        private System.Windows.Forms.Button j3;
        private System.Windows.Forms.Button j2;
        private System.Windows.Forms.Button j1;
        private System.Windows.Forms.Button referee;
        private System.Windows.Forms.NumericUpDown ded5;
        private System.Windows.Forms.NumericUpDown ded4;
        private System.Windows.Forms.NumericUpDown ded3;
        private System.Windows.Forms.NumericUpDown ded2;
        private System.Windows.Forms.NumericUpDown ded1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ImageList imList;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox gbComboJumps;
        private System.Windows.Forms.Button under2;
        private System.Windows.Forms.Button down2;
        private System.Windows.Forms.Button ritt44;
        private System.Windows.Forms.Button lutz44;
        private System.Windows.Forms.Button flip44;
        private System.Windows.Forms.Button sal44;
        private System.Windows.Forms.Button toeloop44;
        private System.Windows.Forms.Button axel44;
        private System.Windows.Forms.Button ritt33;
        private System.Windows.Forms.Button ritt22;
        private System.Windows.Forms.Button ritt11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button lutz33;
        private System.Windows.Forms.Button lutz22;
        private System.Windows.Forms.Button lutz11;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button flip33;
        private System.Windows.Forms.Button flip22;
        private System.Windows.Forms.Button flip11;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button sal33;
        private System.Windows.Forms.Button sal22;
        private System.Windows.Forms.Button sal11;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button toeloop33;
        private System.Windows.Forms.Button toeloop22;
        private System.Windows.Forms.Button toeloop11;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button axel33;
        private System.Windows.Forms.Button axel22;
        private System.Windows.Forms.Button axel11;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.GroupBox gbJumps;
        private System.Windows.Forms.Button under;
        private System.Windows.Forms.Button down;
        private System.Windows.Forms.Button el21;
        private System.Windows.Forms.Button el23;
        private System.Windows.Forms.Button el22;
        private System.Windows.Forms.Button el19;
        private System.Windows.Forms.Button el20;
        private System.Windows.Forms.Button el17;
        private System.Windows.Forms.Button el11;
        private System.Windows.Forms.Button el5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button el16;
        private System.Windows.Forms.Button el10;
        private System.Windows.Forms.Button el4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button el15;
        private System.Windows.Forms.Button el9;
        private System.Windows.Forms.Button el3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button el13;
        private System.Windows.Forms.Button el8;
        private System.Windows.Forms.Button el2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button el14;
        private System.Windows.Forms.Button el7;
        private System.Windows.Forms.Button el1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button el18;
        private System.Windows.Forms.Button el12;
        private System.Windows.Forms.Button el6;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button el64;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.GroupBox gbDed;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.GroupBox gbSteps;
        private System.Windows.Forms.TextBox log;
        private System.Windows.Forms.Button el63;
        private System.Windows.Forms.Button el59;
        private System.Windows.Forms.Button el60;
        private System.Windows.Forms.Button el61;
        private System.Windows.Forms.Button el62;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox gbSpins;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button el25;
        private System.Windows.Forms.Button el40;
        private System.Windows.Forms.Button el45;
        private System.Windows.Forms.Button el48;
        private System.Windows.Forms.Button el47;
        private System.Windows.Forms.Button el49;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button el65;
        private System.Windows.Forms.ToolTip tt2;
        private System.Windows.Forms.Button el64_;
        private System.Windows.Forms.Button startstop;
        private System.Windows.Forms.Button confirm;
        private System.Windows.Forms.Button reset;
        private System.Windows.Forms.Button ltimer;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button review;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label total;
        private System.Windows.Forms.Label deductions;
        private System.Windows.Forms.Label elements;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button openDed;
        private System.Windows.Forms.ListView lv;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader note;
        private System.Windows.Forms.Label error;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColumnHeader magg;
        private System.Windows.Forms.Button newCombo;
        private System.Windows.Forms.Panel panel7;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer7;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape7;
        private System.Windows.Forms.Button el64__;
        private System.Windows.Forms.Button el76;
        private System.Windows.Forms.Button skip;
        private System.Windows.Forms.Button info;
        private System.Windows.Forms.Button quit;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.CheckBox cbVerify;
        private System.Windows.Forms.Label labelPartial;
        private System.Windows.Forms.CheckBox cbBonusSpin2;
        private System.Windows.Forms.ComboBox cb25;
        private System.Windows.Forms.ComboBox cb49;
        private System.Windows.Forms.ComboBox cb47;
        private System.Windows.Forms.ComboBox cb48;
        private System.Windows.Forms.ComboBox cb45;
        private System.Windows.Forms.ComboBox cb40;
        private System.Windows.Forms.Button newComboSpin;
        private System.Windows.Forms.Button el24;
        private System.Windows.Forms.Button half;
        private System.Windows.Forms.Button half2;
        private JCS.ToggleSwitch checkCombo2;
        private System.Windows.Forms.Button el73;
        private System.Windows.Forms.NumericUpDown ded6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button average;
        private System.Windows.Forms.Button el0_;
        private System.Windows.Forms.Button el0;
        private System.Windows.Forms.Button el26;
        private System.Windows.Forms.Button el41;
        private System.Windows.Forms.Button el46;
        private System.Windows.Forms.Button el50;
        private System.Windows.Forms.Button el51;
        private System.Windows.Forms.Button el52;
        private System.Windows.Forms.ComboBox cbCombo;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button el30;
        private System.Windows.Forms.Button el29;
        private System.Windows.Forms.Button el28;
        private System.Windows.Forms.Button el27;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button asterisco;
        private System.Windows.Forms.Button buttonT;
        private System.Windows.Forms.Button prev;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.CheckBox cbBonusSpin1;
        private System.Windows.Forms.Label label34;
    }
}


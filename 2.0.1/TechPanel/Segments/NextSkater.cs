﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RollartSystemTech
{
    public partial class NextSkater : Form
    {
        SQLiteDataReader dr = null;

        public NextSkater()
        {
            InitializeComponent();

            InizializzoCOntrolli();
        }

        private void NextSkater_Load(object sender, EventArgs e)
        {
            try
            {
                int numeroSkater = Definizioni.currentPart - 1;

                if (Definizioni.typeResult.Equals("segment"))
                {
                    this.back.BackgroundImage = global::RollartSystemTech.Properties.Resources.segment;
                    name.Text = Utility.skResultSingle[numeroSkater].skater;
                    try
                    {
                        if (!Utility.skResultSingle[numeroSkater].country.Equals(""))
                            flag.BackgroundImage = Image.FromFile(@"flags\" +
                                Utility.skResultSingle[numeroSkater].country + ".gif");
                    } catch (Exception) { }
                    
                    tech.Text += Utility.skResultSingle[numeroSkater].tech_score;
                    comp.Text += Utility.skResultSingle[numeroSkater].pres_score;
                    deductions.Text += Utility.skResultSingle[numeroSkater].deduction;
                    rank.Text = Utility.skResultSingle[numeroSkater].rank;
                    total.Text = Utility.skResultSingle[numeroSkater].total_score;
                    code.Text = Utility.skResultSingle[numeroSkater].seg_code;

                    name2.Visible = false; flag2.Visible = false; rank2.Visible = false;
                    total2.Visible = false; code2.Visible = false; code3.Visible = false;
                    rank3.Visible = false; code4.Visible = false; total3.Visible = false;
                    rank4.Visible = false;

                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        string query =
                        "SELECT G.Total, G.Position, A.Name, A.Country" +
                        " FROM GaraFinal As G, Athletes As A" +
                        " WHERE G.ID_Atleta = A.ID_Atleta" +
                        " AND G.ID_GaraParams = " + Definizioni.idGaraParams +
                        " AND G.ID_Segment = " + Definizioni.idSegment+ " ORDER BY G.Position ASC";
                        command.CommandText = query;
                        dr = command.ExecuteReader();
                        classifica.Text = "Partial Ranking: ";
                        while (dr.Read())
                        {
                            classifica.Text += dr[1].ToString() + ". " ; // position
                            classifica.Text += dr[2].ToString() + " ("; // name 
                            classifica.Text += dr[3].ToString() + ") "; // country
                            classifica.Text += Utility.FormattaQuestoBenedettoDecimale(dr[0].ToString()) + " - "; // total
                            
                        }
                    }
                }
                else if (Definizioni.typeResult.Equals("combined"))
                {
                    
                    this.back.BackgroundImage = global::RollartSystemTech.Properties.Resources.combined;
                    name2.Text = Utility.skResultComb[numeroSkater].skater;
                    try
                    {
                        if (!Utility.skResultComb[numeroSkater].country.Equals(""))
                            flag2.BackgroundImage = Image.FromFile(@"flags\" +
                                Utility.skResultComb[numeroSkater].country + ".gif");
                    } catch (Exception) { }

                    tech.Text += Utility.skResultComb[numeroSkater].tech_score;
                    comp.Text += Utility.skResultComb[numeroSkater].pres_score;
                    deductions.Text += Utility.skResultComb[numeroSkater].deduction;
                    rank2.Text = Utility.skResultComb[numeroSkater].seg_rank;
                    total2.Text = Utility.skResultComb[numeroSkater].seg_score;
                    code2.Text = Utility.skResultComb[numeroSkater].seg_code;
                    rank3.Text += Utility.skResultComb[numeroSkater].prev_rank;
                    code3.Text = Utility.skResultComb[numeroSkater].prev_seg + " " + Utility.skResultComb[numeroSkater].prev_score;
                    code4.Text = Utility.skResultComb[numeroSkater].comb_code;
                    total3.Text = Utility.skResultComb[numeroSkater].total_score;
                    rank4.Text = Utility.skResultComb[numeroSkater].rank;

                    name.Visible = false; flag.Visible = false; rank.Visible = false;
                    total.Visible = false; code.Visible = false;

                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        string query =
                        "SELECT G.TotGara, G.Position, A.Name, A.Country" +
                        " FROM GaraTotal As G, Athletes As A" +
                        " WHERE G.ID_Atleta = A.ID_Atleta" +
                        " AND G.ID_GaraParams = " + Definizioni.idGaraParams;
                        if (Definizioni.idSegment == 2 || Definizioni.idSegment == 5 || Definizioni.idSegment == 6)
                            query += " AND G.PosSegment2 <> 0";
                        query += " ORDER BY G.Position ASC";
                        command.CommandText = query;
                        dr = command.ExecuteReader();
                        classifica.Text = "Partial Ranking: ";
                        while (dr.Read())
                        {
                            classifica.Text += dr[1].ToString() + ". "; // position
                            classifica.Text += dr[2].ToString() + " ("; // name 
                            classifica.Text += dr[3].ToString() + ") "; // country
                            classifica.Text += Utility.FormattaQuestoBenedettoDecimale(dr[0].ToString()) + " - "; // total

                        }
                    }

                }
                timer1.Start();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void InizializzoCOntrolli()
        {
            try
            {
                var pos = this.PointToScreen(name.Location);
                pos = back.PointToClient(pos);
                name.Parent = back; name.Location = pos;
                name.BackColor = Color.Transparent;

                pos = this.PointToScreen(name2.Location);
                pos = back.PointToClient(pos);
                name2.Parent = back; name2.Location = pos;
                name2.BackColor = Color.Transparent;

                pos = this.PointToScreen(comp.Location);
                pos = back.PointToClient(pos);
                comp.Parent = back; comp.Location = pos;
                comp.BackColor = Color.Transparent;

                pos = this.PointToScreen(tech.Location);
                pos = back.PointToClient(pos);
                tech.Parent = back; tech.Location = pos;
                tech.BackColor = Color.Transparent;

                pos = this.PointToScreen(deductions.Location);
                pos = back.PointToClient(pos);
                deductions.Parent = back; deductions.Location = pos;
                deductions.BackColor = Color.Transparent;

                pos = this.PointToScreen(totalLabel.Location);
                pos = back.PointToClient(pos);
                totalLabel.Parent = back; totalLabel.Location = pos;
                totalLabel.BackColor = Color.Transparent;

                pos = this.PointToScreen(rkLabel.Location);
                pos = back.PointToClient(pos);
                rkLabel.Parent = back; rkLabel.Location = pos;
                rkLabel.BackColor = Color.Transparent;

                pos = this.PointToScreen(rank.Location);
                pos = back.PointToClient(pos);
                rank.Parent = back; rank.Location = pos;
                rank.BackColor = Color.Transparent;

                pos = this.PointToScreen(rank2.Location);
                pos = back.PointToClient(pos);
                rank2.Parent = back; rank2.Location = pos;
                rank2.BackColor = Color.Transparent;

                pos = this.PointToScreen(rank3.Location);
                pos = back.PointToClient(pos);
                rank3.Parent = back; rank3.Location = pos;
                rank3.BackColor = Color.Transparent;

                pos = this.PointToScreen(rank4.Location);
                pos = back.PointToClient(pos);
                rank4.Parent = back; rank4.Location = pos;
                rank4.BackColor = Color.Transparent;

                pos = this.PointToScreen(total.Location);
                pos = back.PointToClient(pos);
                total.Parent = back; total.Location = pos;
                total.BackColor = Color.Transparent;

                pos = this.PointToScreen(total2.Location);
                pos = back.PointToClient(pos);
                total2.Parent = back; total2.Location = pos;
                total2.BackColor = Color.Transparent;

                pos = this.PointToScreen(total3.Location);
                pos = back.PointToClient(pos);
                total3.Parent = back; total3.Location = pos;
                total3.BackColor = Color.Transparent;

                pos = this.PointToScreen(code.Location);
                pos = back.PointToClient(pos);
                code.Parent = back; code.Location = pos;
                code.BackColor = Color.Transparent;

                pos = this.PointToScreen(code2.Location);
                pos = back.PointToClient(pos);
                code2.Parent = back; code2.Location = pos;
                code2.BackColor = Color.Transparent;

                pos = this.PointToScreen(code3.Location);
                pos = back.PointToClient(pos);
                code3.Parent = back; code3.Location = pos;
                code3.BackColor = Color.Transparent;

                pos = this.PointToScreen(code4.Location);
                pos = back.PointToClient(pos);
                code4.Parent = back; code4.Location = pos;
                code4.BackColor = Color.Transparent;

                pos = this.PointToScreen(flag.Location);
                pos = back.PointToClient(pos);
                flag.Parent = back; flag.Location = pos;
                flag.BackColor = Color.Transparent;

                pos = this.PointToScreen(flag2.Location);
                pos = back.PointToClient(pos);
                flag2.Parent = back; flag2.Location = pos;
                flag2.BackColor = Color.Transparent;

                pos = this.PointToScreen(rollart2.Location);
                pos = this.PointToClient(pos);
                rollart.Parent = rollart2; rollart.Location = pos;
                rollart.BackColor = Color.Transparent;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void next_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            classifica.Location = new Point(classifica.Location.X + 6, classifica.Location.Y);
            if (classifica.Location.X > classifica.Width)
            {
                classifica.Location = new Point(0 - classifica.Width, classifica.Location.Y);
            }
        }
    }
}

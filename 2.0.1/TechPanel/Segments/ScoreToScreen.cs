﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;

namespace RollartSystemTech
{
    public partial class ScoreToScreen : Form
    {
        SQLiteDataReader dr = null;
        public ScoreToScreen()
        {
            InitializeComponent();
        }

        private void ScoreToScreen_Load(object sender, EventArgs e)
        {
            try
            {
                // 2.0.1 tolgo e aggiungo maximize
                //Utility.CloneDisplays();
                Utility.MaximizeToSecondaryMonitor(this);

                if (Definizioni.preview)
                {
                    // aggiungo se esiste lìimmagine per lo sponsor
                    if (Definizioni.imageSponsor != null)
                    {
                        pictureBox3.BackgroundImage = Definizioni.imageSponsor;
                    }
                    return;
                }

                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    string query =
                    "SELECT G.BaseTech,G.FinalTech,G.BaseArtistic,G.Deductions,G.Total,T.Position, A.Name, A.Country,T.TotGara" +
                    " FROM GaraFinal As G, Participants As P, Athletes As A, GaraTotal As T" +
                    " WHERE G.ID_GaraParams = P.ID_GaraParams" +
                    " AND G.ID_GaraParams = T.ID_GaraParams" +
                    " AND P.ID_Atleta = T.ID_Atleta" +
                    " AND G.NumPartecipante = P.NumStartingList" +
                    " AND G.ID_Segment = P.ID_Segment" +
                    " AND P.ID_Atleta = A.ID_Atleta" +
                    " AND G.ID_GaraParams = " + Definizioni.idGaraParams +
                    " AND G.ID_Segment = " + Definizioni.idSegment +
                    " AND G.NumPartecipante = " + Definizioni.currentPart;
                    
                    command.CommandText = query;
                    dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        //elements.Text = dr[0].ToString();
                        techEl.Text = String.Format("{0:0.00}", decimal.Parse(dr[1].ToString()));
                        ded.Text = String.Format("{0:0.00}", decimal.Parse(dr[3].ToString()));
                        comp.Text = String.Format("{0:0.00}", decimal.Parse(dr[2].ToString()));
                        total.Text = String.Format("{0:0.00}", decimal.Parse(dr[4].ToString()));
                        rank.Text = dr[5].ToString();
                        name.Text = dr[6].ToString();
                        if (!dr[7].ToString().Equals(""))
                            name.Text = name.Text + "(" + dr[7].ToString() + ")";
                        final.Text = String.Format("{0:0.00}", decimal.Parse(dr[8].ToString()));
                    }
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (RollartSystemTech.Properties.Settings.Default.SwitchDisplay)
            {
                // 2.0.1 tolgo 
                //Utility.InternalDisplay();
            }
            Dispose();
        }

    }
}

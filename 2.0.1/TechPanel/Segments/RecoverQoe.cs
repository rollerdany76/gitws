﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RollartSystemTech
{
    public partial class RecoverQoe : Form
    {
        public RecoverQoe()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int giudici = Definizioni.qoeRicevuti.Length;
                for (int i = 0; i < giudici; i++)
                {
                    if (i == 0) Definizioni.qoeRicevuti[i] = tb1.Text;
                    if (i == 1) Definizioni.qoeRicevuti[i] = tb2.Text;
                    if (i == 2) Definizioni.qoeRicevuti[i] = tb3.Text;
                    if (i == 3) Definizioni.qoeRicevuti[i] = tb4.Text;
                    if (i == 4) Definizioni.qoeRicevuti[i] = tb5.Text;
                }

                Dispose();
            }
            catch 
            {
                Dispose();
            }   
        }

        private void RecoverQoe_Load(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < Definizioni.numJudges; i++)
                {
                    if (i == 0) tb1.Enabled = true;
                    if (i == 1) tb2.Enabled = true;
                    if (i == 2) tb3.Enabled = true;
                    if (i == 3) tb4.Enabled = true;
                    if (i == 4) tb5.Enabled = true; 
                }
                int giudici = Definizioni.qoeRicevuti.Length;
                for (int i = 0; i < giudici; i++)
                {
                    if (Definizioni.qoeRicevuti[i] != null && i == 0)
                    {
                        tb1.Text = Definizioni.qoeRicevuti[i];
                        tb1.ReadOnly = true;
                    }
                    if (Definizioni.qoeRicevuti[i] != null && i == 1)
                    {
                        tb2.Text = Definizioni.qoeRicevuti[i];
                        tb2.ReadOnly = true;
                    }
                    if (Definizioni.qoeRicevuti[i] != null && i == 2)
                    {
                        tb3.Text = Definizioni.qoeRicevuti[i];
                        tb3.ReadOnly = true;
                    }
                    if (Definizioni.qoeRicevuti[i] != null && i == 3)
                    {
                        tb4.Text = Definizioni.qoeRicevuti[i];
                        tb4.ReadOnly = true;
                    }
                    if (Definizioni.qoeRicevuti[i] != null && i == 4)
                    {
                        tb5.Text = Definizioni.qoeRicevuti[i];
                        tb5.ReadOnly = true;
                    }
                }
            }
            catch 
            {
                Dispose();
            }
        }
    }
}

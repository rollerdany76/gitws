﻿namespace RollartSystemTech
{
    partial class NextSkater
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.next = new System.Windows.Forms.Button();
            this.deductions = new System.Windows.Forms.Label();
            this.comp = new System.Windows.Forms.Label();
            this.tech = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.Label();
            this.flag = new System.Windows.Forms.PictureBox();
            this.back = new System.Windows.Forms.PictureBox();
            this.totalLabel = new System.Windows.Forms.Label();
            this.rkLabel = new System.Windows.Forms.Label();
            this.rollart2 = new System.Windows.Forms.Label();
            this.rank = new System.Windows.Forms.Label();
            this.total = new System.Windows.Forms.Label();
            this.code = new System.Windows.Forms.Label();
            this.rollart = new System.Windows.Forms.Label();
            this.flag2 = new System.Windows.Forms.PictureBox();
            this.name2 = new System.Windows.Forms.Label();
            this.code2 = new System.Windows.Forms.Label();
            this.total2 = new System.Windows.Forms.Label();
            this.rank2 = new System.Windows.Forms.Label();
            this.code3 = new System.Windows.Forms.Label();
            this.rank3 = new System.Windows.Forms.Label();
            this.code4 = new System.Windows.Forms.Label();
            this.total3 = new System.Windows.Forms.Label();
            this.rank4 = new System.Windows.Forms.Label();
            this.classifica = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.flag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.back)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flag2)).BeginInit();
            this.SuspendLayout();
            // 
            // next
            // 
            this.next.Cursor = System.Windows.Forms.Cursors.Hand;
            this.next.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.next.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.next.FlatAppearance.BorderSize = 2;
            this.next.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.next.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.next.ForeColor = System.Drawing.Color.White;
            this.next.Location = new System.Drawing.Point(704, 13);
            this.next.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.next.Name = "next";
            this.next.Size = new System.Drawing.Size(166, 37);
            this.next.TabIndex = 185;
            this.next.Text = "Next";
            this.next.UseVisualStyleBackColor = true;
            this.next.Click += new System.EventHandler(this.next_Click);
            // 
            // deductions
            // 
            this.deductions.AutoEllipsis = true;
            this.deductions.AutoSize = true;
            this.deductions.BackColor = System.Drawing.Color.Transparent;
            this.deductions.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deductions.ForeColor = System.Drawing.Color.White;
            this.deductions.Location = new System.Drawing.Point(638, 389);
            this.deductions.Name = "deductions";
            this.deductions.Size = new System.Drawing.Size(137, 29);
            this.deductions.TabIndex = 5;
            this.deductions.Text = "Deductions ";
            // 
            // comp
            // 
            this.comp.AutoEllipsis = true;
            this.comp.AutoSize = true;
            this.comp.BackColor = System.Drawing.Color.Transparent;
            this.comp.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp.ForeColor = System.Drawing.Color.White;
            this.comp.Location = new System.Drawing.Point(394, 389);
            this.comp.Name = "comp";
            this.comp.Size = new System.Drawing.Size(152, 29);
            this.comp.TabIndex = 4;
            this.comp.Text = "Components ";
            // 
            // tech
            // 
            this.tech.AutoEllipsis = true;
            this.tech.AutoSize = true;
            this.tech.BackColor = System.Drawing.Color.Transparent;
            this.tech.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tech.ForeColor = System.Drawing.Color.White;
            this.tech.Location = new System.Drawing.Point(158, 389);
            this.tech.Name = "tech";
            this.tech.Size = new System.Drawing.Size(119, 29);
            this.tech.TabIndex = 3;
            this.tech.Text = "Technical ";
            // 
            // name
            // 
            this.name.AutoEllipsis = true;
            this.name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.name.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.name.Font = new System.Drawing.Font("Tahoma", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.name.Location = new System.Drawing.Point(92, 323);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(565, 47);
            this.name.TabIndex = 2;
            this.name.Text = "name";
            this.name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flag
            // 
            this.flag.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.flag.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.flag.Location = new System.Drawing.Point(17, 320);
            this.flag.Name = "flag";
            this.flag.Size = new System.Drawing.Size(64, 51);
            this.flag.TabIndex = 1;
            this.flag.TabStop = false;
            // 
            // back
            // 
            this.back.BackgroundImage = global::RollartSystemTech.Properties.Resources.combined;
            this.back.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.back.Location = new System.Drawing.Point(12, 268);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(855, 155);
            this.back.TabIndex = 0;
            this.back.TabStop = false;
            // 
            // totalLabel
            // 
            this.totalLabel.AutoEllipsis = true;
            this.totalLabel.AutoSize = true;
            this.totalLabel.BackColor = System.Drawing.Color.Transparent;
            this.totalLabel.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalLabel.ForeColor = System.Drawing.Color.White;
            this.totalLabel.Location = new System.Drawing.Point(736, 274);
            this.totalLabel.Name = "totalLabel";
            this.totalLabel.Size = new System.Drawing.Size(75, 33);
            this.totalLabel.TabIndex = 186;
            this.totalLabel.Text = "Total";
            // 
            // rkLabel
            // 
            this.rkLabel.AutoEllipsis = true;
            this.rkLabel.AutoSize = true;
            this.rkLabel.BackColor = System.Drawing.Color.Transparent;
            this.rkLabel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rkLabel.ForeColor = System.Drawing.Color.White;
            this.rkLabel.Location = new System.Drawing.Point(825, 279);
            this.rkLabel.Name = "rkLabel";
            this.rkLabel.Size = new System.Drawing.Size(34, 24);
            this.rkLabel.TabIndex = 187;
            this.rkLabel.Text = "RK";
            // 
            // rollart2
            // 
            this.rollart2.AutoEllipsis = true;
            this.rollart2.BackColor = System.Drawing.Color.Transparent;
            this.rollart2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rollart2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rollart2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.rollart2.Location = new System.Drawing.Point(12, 20);
            this.rollart2.Name = "rollart2";
            this.rollart2.Size = new System.Drawing.Size(496, 70);
            this.rollart2.TabIndex = 188;
            this.rollart2.Text = "  RollArt WORLD SKATE";
            this.rollart2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rank
            // 
            this.rank.AutoEllipsis = true;
            this.rank.BackColor = System.Drawing.Color.Maroon;
            this.rank.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rank.ForeColor = System.Drawing.Color.White;
            this.rank.Location = new System.Drawing.Point(814, 328);
            this.rank.Name = "rank";
            this.rank.Size = new System.Drawing.Size(56, 35);
            this.rank.TabIndex = 189;
            this.rank.Text = "28";
            this.rank.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // total
            // 
            this.total.AutoEllipsis = true;
            this.total.BackColor = System.Drawing.Color.Maroon;
            this.total.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total.ForeColor = System.Drawing.Color.White;
            this.total.Location = new System.Drawing.Point(728, 322);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(99, 48);
            this.total.TabIndex = 190;
            this.total.Text = "128,99";
            this.total.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // code
            // 
            this.code.AutoEllipsis = true;
            this.code.BackColor = System.Drawing.Color.Maroon;
            this.code.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.code.ForeColor = System.Drawing.Color.White;
            this.code.Location = new System.Drawing.Point(670, 322);
            this.code.Name = "code";
            this.code.Size = new System.Drawing.Size(54, 48);
            this.code.TabIndex = 191;
            this.code.Text = "SP";
            this.code.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rollart
            // 
            this.rollart.AutoEllipsis = true;
            this.rollart.AutoSize = true;
            this.rollart.BackColor = System.Drawing.Color.Transparent;
            this.rollart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rollart.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rollart.ForeColor = System.Drawing.Color.LightGray;
            this.rollart.Location = new System.Drawing.Point(12, 32);
            this.rollart.Name = "rollart";
            this.rollart.Size = new System.Drawing.Size(467, 48);
            this.rollart.TabIndex = 192;
            this.rollart.Text = "RollArt WORLD SKATE";
            // 
            // flag2
            // 
            this.flag2.BackColor = System.Drawing.Color.Transparent;
            this.flag2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.flag2.Location = new System.Drawing.Point(17, 305);
            this.flag2.Name = "flag2";
            this.flag2.Size = new System.Drawing.Size(64, 40);
            this.flag2.TabIndex = 193;
            this.flag2.TabStop = false;
            // 
            // name2
            // 
            this.name2.AutoEllipsis = true;
            this.name2.BackColor = System.Drawing.Color.Transparent;
            this.name2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.name2.Font = new System.Drawing.Font("Tahoma", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.name2.Location = new System.Drawing.Point(92, 305);
            this.name2.Name = "name2";
            this.name2.Size = new System.Drawing.Size(565, 40);
            this.name2.TabIndex = 194;
            this.name2.Text = "label1";
            this.name2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // code2
            // 
            this.code2.AutoEllipsis = true;
            this.code2.BackColor = System.Drawing.Color.Transparent;
            this.code2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.code2.ForeColor = System.Drawing.Color.White;
            this.code2.Location = new System.Drawing.Point(672, 306);
            this.code2.Name = "code2";
            this.code2.Size = new System.Drawing.Size(54, 41);
            this.code2.TabIndex = 197;
            this.code2.Text = "SP";
            this.code2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // total2
            // 
            this.total2.AutoEllipsis = true;
            this.total2.BackColor = System.Drawing.Color.Transparent;
            this.total2.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total2.ForeColor = System.Drawing.Color.White;
            this.total2.Location = new System.Drawing.Point(730, 306);
            this.total2.Name = "total2";
            this.total2.Size = new System.Drawing.Size(99, 41);
            this.total2.TabIndex = 196;
            this.total2.Text = "128,99";
            this.total2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rank2
            // 
            this.rank2.AutoEllipsis = true;
            this.rank2.BackColor = System.Drawing.Color.Transparent;
            this.rank2.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rank2.ForeColor = System.Drawing.Color.White;
            this.rank2.Location = new System.Drawing.Point(816, 306);
            this.rank2.Name = "rank2";
            this.rank2.Size = new System.Drawing.Size(56, 39);
            this.rank2.TabIndex = 195;
            this.rank2.Text = "28";
            this.rank2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // code3
            // 
            this.code3.AutoEllipsis = true;
            this.code3.AutoSize = true;
            this.code3.BackColor = System.Drawing.Color.Transparent;
            this.code3.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.code3.ForeColor = System.Drawing.Color.White;
            this.code3.Location = new System.Drawing.Point(223, 351);
            this.code3.Name = "code3";
            this.code3.Size = new System.Drawing.Size(253, 29);
            this.code3.TabIndex = 198;
            this.code3.Text = "Previous Code + score";
            // 
            // rank3
            // 
            this.rank3.AutoEllipsis = true;
            this.rank3.AutoSize = true;
            this.rank3.BackColor = System.Drawing.Color.Transparent;
            this.rank3.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rank3.ForeColor = System.Drawing.Color.White;
            this.rank3.Location = new System.Drawing.Point(488, 351);
            this.rank3.Name = "rank3";
            this.rank3.Size = new System.Drawing.Size(74, 29);
            this.rank3.TabIndex = 199;
            this.rank3.Text = "Rank ";
            // 
            // code4
            // 
            this.code4.AutoEllipsis = true;
            this.code4.BackColor = System.Drawing.Color.Transparent;
            this.code4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.code4.ForeColor = System.Drawing.Color.White;
            this.code4.Location = new System.Drawing.Point(672, 351);
            this.code4.Name = "code4";
            this.code4.Size = new System.Drawing.Size(54, 35);
            this.code4.TabIndex = 200;
            this.code4.Text = "SP";
            this.code4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // total3
            // 
            this.total3.AutoEllipsis = true;
            this.total3.BackColor = System.Drawing.Color.Transparent;
            this.total3.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total3.ForeColor = System.Drawing.Color.White;
            this.total3.Location = new System.Drawing.Point(730, 351);
            this.total3.Name = "total3";
            this.total3.Size = new System.Drawing.Size(99, 35);
            this.total3.TabIndex = 201;
            this.total3.Text = "128,99";
            this.total3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rank4
            // 
            this.rank4.AutoEllipsis = true;
            this.rank4.BackColor = System.Drawing.Color.Transparent;
            this.rank4.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rank4.ForeColor = System.Drawing.Color.White;
            this.rank4.Location = new System.Drawing.Point(814, 351);
            this.rank4.Name = "rank4";
            this.rank4.Size = new System.Drawing.Size(56, 35);
            this.rank4.TabIndex = 202;
            this.rank4.Text = "28";
            this.rank4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // classifica
            // 
            this.classifica.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.classifica.AutoSize = true;
            this.classifica.BackColor = System.Drawing.Color.Transparent;
            this.classifica.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.classifica.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.classifica.Location = new System.Drawing.Point(-1, 457);
            this.classifica.Name = "classifica";
            this.classifica.Size = new System.Drawing.Size(0, 21);
            this.classifica.TabIndex = 203;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Yellow;
            this.label1.Location = new System.Drawing.Point(659, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 72);
            this.label1.TabIndex = 204;
            // 
            // NextSkater
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(879, 498);
            this.ControlBox = false;
            this.Controls.Add(this.next);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.classifica);
            this.Controls.Add(this.rank4);
            this.Controls.Add(this.total3);
            this.Controls.Add(this.code4);
            this.Controls.Add(this.rank3);
            this.Controls.Add(this.code3);
            this.Controls.Add(this.code2);
            this.Controls.Add(this.total2);
            this.Controls.Add(this.rank2);
            this.Controls.Add(this.name2);
            this.Controls.Add(this.flag2);
            this.Controls.Add(this.rollart);
            this.Controls.Add(this.rollart2);
            this.Controls.Add(this.code);
            this.Controls.Add(this.total);
            this.Controls.Add(this.rank);
            this.Controls.Add(this.rkLabel);
            this.Controls.Add(this.totalLabel);
            this.Controls.Add(this.flag);
            this.Controls.Add(this.deductions);
            this.Controls.Add(this.name);
            this.Controls.Add(this.comp);
            this.Controls.Add(this.tech);
            this.Controls.Add(this.back);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "NextSkater";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.NextSkater_Load);
            ((System.ComponentModel.ISupportInitialize)(this.flag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.back)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flag2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button next;
        private System.Windows.Forms.Label name;
        private System.Windows.Forms.PictureBox flag;
        private System.Windows.Forms.Label tech;
        private System.Windows.Forms.Label deductions;
        private System.Windows.Forms.Label comp;
        private System.Windows.Forms.PictureBox back;
        private System.Windows.Forms.Label totalLabel;
        private System.Windows.Forms.Label rkLabel;
        private System.Windows.Forms.Label rollart2;
        private System.Windows.Forms.Label rank;
        private System.Windows.Forms.Label total;
        private System.Windows.Forms.Label code;
        private System.Windows.Forms.Label rollart;
        private System.Windows.Forms.PictureBox flag2;
        private System.Windows.Forms.Label name2;
        private System.Windows.Forms.Label code2;
        private System.Windows.Forms.Label total2;
        private System.Windows.Forms.Label rank2;
        private System.Windows.Forms.Label code3;
        private System.Windows.Forms.Label rank3;
        private System.Windows.Forms.Label code4;
        private System.Windows.Forms.Label total3;
        private System.Windows.Forms.Label rank4;
        private System.Windows.Forms.Label classifica;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
    }
}
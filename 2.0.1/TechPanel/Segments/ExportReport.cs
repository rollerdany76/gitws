﻿using CrystalDecisions.Windows.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace RollartSystemTech.Segments
{
    public partial class ExportReport : Form
    {
        int idSegm = 0;
        string desc = "";
        PrintDocument printDoc = new PrintDocument();
        RollartSystemTech.Report.SkatingOrder skOrderReport = null;
        RollartSystemTech.Report.RefereeSheet refereeSheetReport = null;

        public ExportReport(int idSegment, string description)
        {
            InitializeComponent();
            idSegm = idSegment;
            desc = description;
        }

        private void ExportReport_Load(object sender, EventArgs e)
        {
            try
            {
                PopulateInstalledPrintersCombo();

            }
            catch (Exception ex)
            {
                MessageBox.Show("ExportReport_Load: Error... " + ex.Message, "RollArt Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        // button Close 
        private void button1_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        // button OK
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (!cb1.Checked && !cb2.Checked)
                    Dispose();
                waiting.Visible = true;
                pb.Visible = true; pb.Update(); pb.Refresh();

                if (rb1.Checked)
                    ExportInPdf();
                else Print();

                pb.Visible = false;
                waiting.Visible = false;
                Dispose();
            }
            catch (Exception ex)
            {
                pb.Visible = false;
                waiting.Visible = false;
                MessageBox.Show("Print: Error... " + ex.StackTrace, "RollArt Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void ExportInPdf()
        {
            if (cb1.Checked) // skating order
            {
                OpenSkatingOrderReport();

                skOrderReport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat,
                   RollartSystemTech.Properties.Settings.Default.EventFolder +
                   "\\" + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year + " " +
                   desc.Replace(" - ", "_").Replace("RESULTS", "SKATING ORDER") + ".pdf");
            }
            if (cb2.Checked) // referee sheet
            {
                OpenRefereeSheetReport();

                refereeSheetReport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat,
                   RollartSystemTech.Properties.Settings.Default.EventFolder +
                   "\\" + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year + " " +
                   desc.Replace(" - ", "_").Replace("RESULTS", "SKATING ORDER").Replace("SKATING ORDER", "REFEREE SHEET") + ".pdf");
            }

            if (cb3.Checked) // judges sheet
            {
                OpenJudgeSheetReport();


            }

            System.Diagnostics.Process.Start(RollartSystemTech.Properties.Settings.Default.EventFolder);
        }

        private void Print()
        {
            if (cb1.Checked) // skating order
            {
                OpenSkatingOrderReport();

                skOrderReport.PrintToPrinter(int.Parse(copie.Value.ToString()), false, 0, 0);
            }
            if (cb2.Checked) // referee sheet
            {
                OpenRefereeSheetReport();

                refereeSheetReport.PrintToPrinter(int.Parse(copie.Value.ToString()), false, 0, 0);
            }

            if (cb3.Checked) // judges sheet
            {
                OpenJudgeSheetReport();

                //refereeSheetReport.PrintToPrinter(int.Parse(copie.Value.ToString()), true, 0, 0);
            }
        }

        #region Report
        private void OpenSkatingOrderReport()
        {
            skOrderReport =
                    new RollartSystemTech.Report.SkatingOrder();
            crViewer.ReportSource = skOrderReport;
            crViewer.SelectionFormula = "{GaraParams1.ID_GaraParams} = " + Definizioni.idGaraParams +
                                " AND {GaraParams1.ID_Segment} = " + idSegm;
            crViewer.Refresh();
            Thread.Sleep(500);
        }

        private void OpenRefereeSheetReport()
        {
            refereeSheetReport =
                    new RollartSystemTech.Report.RefereeSheet();
            crViewer.ReportSource = refereeSheetReport;
            crViewer.SelectionFormula = "{GaraParams1.ID_GaraParams} = " + Definizioni.idGaraParams +
                                " AND {GaraParams1.ID_Segment} = " + idSegm;
            crViewer.Refresh();
            Thread.Sleep(500);
        }

        private void OpenJudgeSheetReport()
        {
            refereeSheetReport =
                    new RollartSystemTech.Report.RefereeSheet();
            crViewer.ReportSource = refereeSheetReport;
            crViewer.SelectionFormula = "{GaraParams1.ID_GaraParams} = " + Definizioni.idGaraParams +
                                " AND {GaraParams1.ID_Segment} = " + idSegm;
            crViewer.Refresh();
            Thread.Sleep(500);
        }

        #endregion

        #region Printers
        private void PopulateInstalledPrintersCombo()
        {
            // Add list of installed printers found to the combo box.
            // The pkInstalledPrinters string will be used to provide the display string.
            int i;
            string pkInstalledPrinters;

            for (i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                pkInstalledPrinters = PrinterSettings.InstalledPrinters[i];
                cbPrinter.Items.Add(pkInstalledPrinters);
                if (printDoc.PrinterSettings.IsDefaultPrinter)
                {
                    cbPrinter.Text = printDoc.PrinterSettings.PrinterName;
                }
            }
        }
        
        #endregion
        private void rb2_CheckedChanged(object sender, EventArgs e)
        {
            if (rb2.Checked)
                gb.Enabled = true;
        }

        private void rb1_CheckedChanged(object sender, EventArgs e)
        {
            if (rb1.Checked)
                gb.Enabled = false;
        }
    }
}

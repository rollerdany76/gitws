﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Windows;
using System.Threading;

namespace RollartSystemTech
{
    public partial class DisplayScore : Form
    {
        SQLiteDataReader dr = null;

        public DisplayScore()
        {
            InitializeComponent();
        }

        private void ScoreToScreen_Load(object sender, EventArgs e)
        {
            try
            {
                // 2.0.1 aggiungo maximize
                //Utility.MaximizeToSecondaryMonitor(this);
                if (RollartSystemTech.Properties.Settings.Default.StaticScreen &&
                    RollartSystemTech.Properties.Settings.Default.SwitchDisplay)
                    Utility.MoveWindowToProjector(this.Handle);
                
                // aggiungo se esiste l'immagine per lo sponsor
                if (Definizioni.imageSponsor != null)
                {
                    pictureBox3.BackgroundImage = Definizioni.imageSponsor;
                }
            }
            catch (Exception)
            {
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //if (RollartSystemTech.Properties.Settings.Default.SwitchDisplay)
            //{
            //Utility.InternalDisplay();
            //}
            Dispose();
            //Close();
        }

        private void LoadResults()
        {
            try
            {
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    string query =
                    "SELECT G.BaseTech,G.FinalTech,G.BaseArtistic,G.Deductions,G.Total,T.Position, A.Name, A.Country,T.TotGara" +
                    " FROM GaraFinal As G, Participants As P, Athletes As A, GaraTotal As T" +
                    " WHERE G.ID_GaraParams = P.ID_GaraParams" +
                    " AND G.ID_GaraParams = T.ID_GaraParams" +
                    " AND P.ID_Atleta = T.ID_Atleta" +
                    " AND G.NumPartecipante = P.NumStartingList" +
                    " AND G.ID_Segment = P.ID_Segment" +
                    " AND P.ID_Atleta = A.ID_Atleta" +
                    " AND G.ID_GaraParams = " + Definizioni.idGaraParams +
                    " AND G.ID_Segment = " + Definizioni.idSegment +
                    " AND G.NumPartecipante = " + Definizioni.currentPart;

                    command.CommandText = query;
                    dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        techEl.Text = String.Format("{0:0.00}", decimal.Parse(dr[1].ToString()));
                        ded.Text = String.Format("{0:0.00}", decimal.Parse(dr[3].ToString()));
                        comp.Text = String.Format("{0:0.00}", decimal.Parse(dr[2].ToString()));
                        total.Text = String.Format("{0:0.00}", decimal.Parse(dr[4].ToString()));
                        rank.Text = dr[5].ToString();
                        name.Text = dr[6].ToString();
                        if (!dr[7].ToString().Equals(""))
                            name.Text = Definizioni.currentPart + ". " + name.Text + " - " + dr[7].ToString() + "";
                        final.Text = String.Format("{0:0.00}", decimal.Parse(dr[8].ToString()));
                    }
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LoadSkater()
        {
            try
            {
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    string query =
                    "SELECT A.Name, A.Country FROM Participants As P, Athletes As A" +
                    " WHERE P.ID_Atleta = A.ID_Atleta" +
                    " AND P.ID_GaraParams = " + Definizioni.idGaraParams + 
                    " AND P.ID_Segment = " + Definizioni.idSegment + 
                    " AND P.NumStartingList = " + Definizioni.currentPart;

                    command.CommandText = query;
                    dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        order.Text = Definizioni.currentPart + ". " + 
                            dr[0].ToString() + " ("  + dr[1].ToString() + ")";
                    }
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void panel2_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (Definizioni.preview) return;
                if (panel2.Visible)
                {
                    //this.TopMost = true;
                    LoadResults();
                    //this.TopMost = false;
                }
            }
            catch (Exception)
            {

            }
        }

        private void panel1_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (Definizioni.preview) return;
                if (panel1.Visible)
                {
                    //this.TopMost = true;
                    LoadResults();
                    //this.TopMost = false;
                }
            }
            catch (Exception)
            {

            }
        }

        private void panelBlank_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (Definizioni.preview) return;
                if (panelBlank.Visible)
                {
                    LoadSkater();
                }
                //Thread.Sleep(RollartSystemTech.Properties.Settings.Default.TimeSkaterInfo);
                //Close();
            }
            catch (Exception)
            {

            }
        }

        private void DisplayScore_Shown(object sender, EventArgs e)
        {
            //if (!Definizioni.preview)
            //    LoadSkater();
        }
    }

}

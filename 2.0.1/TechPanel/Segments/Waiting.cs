﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RollartSystemTech
{
    public partial class Waiting : Form
    {
        //public bool exitFromWaiting = false;
        bool flagClose = false;

        public Waiting()
        {
            InitializeComponent();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Stop waiting values from judges? \r\n(You can click on CONFIRM after) ", "Warning",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //Definizioni.stopFromWaiting = true;
                Definizioni.exitFromWaiting = true;
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            //if (MessageBox.Show("Exit waiting values from judges? \r\n(You can NOT click on CONFIRM after) ", "Warning",
            //    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            //{
            //    Definizioni.exitFromWaiting = true;
            //}
        }

        private void Waiting_FormClosing(object sender, FormClosingEventArgs e)
        {
            // *** Modifica del 05/12/2018 - 2.0.0.9 *** //
            if (!flagClose)
                e.Cancel = true;
        }
    }
}

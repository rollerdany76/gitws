﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace RollartSystemTech
{
    public partial class Judges : Form
    {
        public static ArrayList judges = null;
        Utility ut = null;
        
        public Judges()
        {
            InitializeComponent();
        }

        private void Judges_Load(object sender, EventArgs e)
        {
            try
            {
                ut = new Utility(null);
                for (int i = 0; i < 9; i++)
                {
                    ((TextBox)this.Controls.Find("j" + (i + 1), true)[0]).Enabled = false;
                    ((Label)this.Controls.Find("ju" + (i + 1), true)[0]).Enabled = false;
                }
                for (int i = 0; i < Definizioni.numOfJudges; i++)
                {
                    ((TextBox)this.Controls.Find("j" + (i + 1), true)[0]).Enabled = true;
                    ((Label)this.Controls.Find("ju" + (i + 1), true)[0]).Enabled = true;
                }

                // recupero i giudici per l'auto-completion dei nomi
                ut.GetGiudici();
                if (Definizioni.giudici != null)
                {
                    var source = new AutoCompleteStringCollection();
                    string[] names = (string[])Definizioni.giudici.ToArray(typeof(string));
                    source.AddRange(names);
                    tp.AutoCompleteCustomSource = source;
                    dataop.AutoCompleteCustomSource = source;
                    ass.AutoCompleteCustomSource = source;
                    contr.AutoCompleteCustomSource = source;
                    referee.AutoCompleteCustomSource = source;
                    j1.AutoCompleteCustomSource = source;
                    j2.AutoCompleteCustomSource = source;
                    j3.AutoCompleteCustomSource = source;
                    j4.AutoCompleteCustomSource = source;
                    j5.AutoCompleteCustomSource = source;
                    j6.AutoCompleteCustomSource = source;
                    j7.AutoCompleteCustomSource = source;
                    j8.AutoCompleteCustomSource = source;
                    j9.AutoCompleteCustomSource = source;
                }
            } 
            catch (Exception ex)
            {
                Utility.WriteLog(ex.InnerException.Message, "ERROR");
            }
        }

        private void confirmNewEvent_Click(object sender, EventArgs e)
        {
            try
            {
                if (tp.TextLength == 0)
                {
                    label1.ForeColor = Color.Red;
                    tp.Focus();
                    return;
                }
                if (dataop.TextLength == 0)
                {
                    label2.ForeColor = Color.Red;
                    dataop.Focus();
                    return;
                }
                //if (ass.TextLength == 0)
                //{
                //    MessageBox.Show(Definizioni.resources["judge3"].ToString());
                //    return;
                //}
                //if (referee.TextLength == 0)
                //{
                //    MessageBox.Show(Definizioni.resources["judge1"].ToString());
                //    return;
                //}
                //if (contr.TextLength == 0)
                //{
                //    MessageBox.Show("Insert Controller name");
                //    return;
                //}
                for (int i = 0; i < Definizioni.numOfJudges; i++)
                {
                    if (((TextBox)this.Controls.Find("j" + (i + 1), true)[0]).TextLength == 0)
                    {
                        ((Label)this.Controls.Find("ju" + (i + 1), true)[0]).ForeColor = Color.Red;
                        ((TextBox)this.Controls.Find("j" + (i + 1), true)[0]).Focus();
                        return;
                    }
                }
                judges = new ArrayList();
                judges.Add(tp.Text);
                judges.Add(dataop.Text);
                judges.Add(ass.Text);
                judges.Add(contr.Text);
                judges.Add(referee.Text);
                for (int i = 0; i < Definizioni.numOfJudges; i++)
                {
                    judges.Add(((TextBox)this.Controls.Find("j" + (i + 1), true)[0]).Text);
                }

                //Dispose();
                Close();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex.InnerException.Message, "ERROR");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Dispose();
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 9; i++)
            {
                ((TextBox)this.Controls.Find("j" + (i + 1), true)[0]).Text = "";
            }
            tp.Text = "";
            dataop.Text = "";
            ass.Text = "";
            referee.Text = "";
            contr.Text = "";
            tp.Focus();
        }

        private void tp_TextChanged(object sender, EventArgs e)
        {
            label1.ForeColor = Color.Black;
        }

        private void dataop_TextChanged(object sender, EventArgs e)
        {
            label2.ForeColor = Color.Black;
        }

        private void j1_TextChanged(object sender, EventArgs e)
        {
            string name = ((TextBox)sender).Name;
            ((Label)this.Controls.Find(name.Insert(1,"u"), true)[0]).ForeColor = Color.Black;
        }
    }
}

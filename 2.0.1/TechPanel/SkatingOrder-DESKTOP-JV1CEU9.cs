﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SQLite;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using System.Threading.Tasks;
using CrystalDecisions.Shared;

namespace RollartSystemTech
{
    public partial class SkatingOrder : Form
    {
        int idSegm = 0, idSpecialita = 0;
        bool completed = false;
        Utility ut = null;

        public SkatingOrder(object dettaglioGara)
        {
            InitializeComponent();
            idSegm = int.Parse(dettaglioGara.ToString());
            ut = new Utility(null);
        }

        private void SkatingOrder_Load(object sender, EventArgs e)
        {
            try
            {
                SQLiteDataReader dr2 = null, dr = null;
                /*************************************************************************
                * Modifica del 07/08/2018
                * recupero anche le info sul segmento
                * ***********************************************************************/
                SQLiteCommand cmd = new SQLiteCommand(
                            "select S.Name, Completed, LastPartecipant, Partecipants, NumJudges, A.Name, C.Name, P.Name" +
                            " from GaraParams as A, Segments as S, Category as C, Specialita as P " +
                            " where ID_GaraParams = " + Definizioni.idGaraParams + 
                            " AND A.ID_SEGMENT = S.ID_SEGMENTS" +
                            " AND A.ID_CATEGORY = C.ID_CATEGORY" +
                            " AND A.ID_SPECIALITA = P.ID_SPECIALITA" +
                            " AND A.ID_SEGMENT = " + idSegm, Definizioni.conn);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    //l1.Text = "SKATING ORDER ● " + dr[7].ToString() + " " + dr[6].ToString() + " " + dr[0].ToString().ToUpper(); // segment name
                    if (dr[2].ToString().Equals(dr[3].ToString())) // segmento completato o iniziato
                    {
                        up.Enabled = false;
                        down.Enabled = false;
                        confirm.Enabled = false;
                        print.Visible = false;
                        dado.Visible = false;
                    }
                    gara.Text = dr[5].ToString();
                    // status
                    if (dr[1].ToString().Equals("Y"))
                    {
                        status.Image = global::RollartSystemTech.Properties.Resources.bullet_green;
                        status.Text = "     Completed";
                        completed = true;
                        l1.Text = "RESULTS ● " + dr[7].ToString() + " " + dr[6].ToString() + " " + dr[0].ToString().ToUpper(); // segment name
                    }
                    else if (dr[1].ToString().Equals("N") && !dr[2].ToString().Equals("0"))
                    {
                        status.Image = global::RollartSystemTech.Properties.Resources.bullet_yellow;
                        status.Text = "     In progress";
                        dado.Visible = false;
                        l1.Text = "SKATING ORDER ● " + dr[7].ToString() + " " + dr[6].ToString() + " " + dr[0].ToString().ToUpper(); // segment name
                    }
                    else
                    { 
                        panSkater.Visible = true;
                        print.Visible = true;
                        dado.Visible = true;
                        status.Image = global::RollartSystemTech.Properties.Resources.bullet_red;
                        status.Text = "     Not Started";
                        l1.Text = "SKATING ORDER ● " + dr[7].ToString() + " " + dr[6].ToString() + " " + dr[0].ToString().ToUpper(); // segment name
                    }


                    string query = "select NumStartingList, B.Name, A.ID_Atleta, Societa, Country, B.ID_Specialita" +
                                " from Participants as A, Athletes as B" +
                                " where ID_GaraParams = " + Definizioni.idGaraParams +
                                " AND A.ID_SEGMENT = " + idSegm +
                                " AND A.ID_Atleta = B.ID_Atleta";

                    if (dr[1].ToString().Equals("Y"))
                    {
                        query = "select NumStartingList, B.Name, A.ID_Atleta, Societa, Country, B.ID_Specialita, Position" +
                                " from Participants as A, Athletes as B, GaraFinal as G" +
                                " where A.ID_GaraParams = " + Definizioni.idGaraParams +
                                " AND A.ID_SEGMENT = G.ID_SEGMENT" +
                                " AND A.ID_GaraParams = G.ID_GaraParams" +
                                " AND A.ID_SEGMENT = " + idSegm +
                                " AND B.ID_Atleta = G.ID_Atleta" +
                                " AND A.ID_Atleta = B.ID_Atleta ORDER BY Position";
                        lbCompetitors.Columns[0].Text = "Rank";
                        //lbCompetitors.Columns[10].Text = "PDF";
                        lbCompetitors.Columns[10].Width = 0;
                        lbCompetitors.HotTracking = true;

                    }
                    else // non iniziato
                    {
                        lbCompetitors.Columns[0].Text = "Order";
                        lbCompetitors.Columns[10].Width = 60;
                        lbCompetitors.HotTracking = false;
                    }

                    cmd = new SQLiteCommand(query, Definizioni.conn);
                    dr = cmd.ExecuteReader();
                    int numAtleti = 0;
                    while (dr.Read())
                    {
                        lbCompetitors.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "", "", "", "", "", "", "" }, -1));
                        lbCompetitors.Items[numAtleti].SubItems[0].Text = "" + (numAtleti + 1);
                        lbCompetitors.Items[numAtleti].SubItems[1].Text = dr[1].ToString().ToUpper(); // name
                        lbCompetitors.Items[numAtleti].SubItems[2].Text = dr[2].ToString(); // ID_Atleta
                        lbCompetitors.Items[numAtleti].SubItems[3].Text = dr[3].ToString().ToUpper(); // societa
                        lbCompetitors.Items[numAtleti].SubItems[4].Text = dr[4].ToString().ToUpper(); // country

                        idSpecialita = int.Parse(dr[5].ToString());
                        lbCompetitors.Items[numAtleti].SubItems[11].Text = idSpecialita + ""; // country

                        /*************************************************************************
                         * Modifica del 07/08/2018
                         * recupero anche le info sul segmento dell'atleta
                         * ***********************************************************************/
                        cmd = new SQLiteCommand(
                                        "select BaseTech, FinalTech, BaseArtistic, Deductions, Total, Position " +
                                        " from Participants as A, GaraFinal as B" +
                                        " where A.ID_GaraParams = " + Definizioni.idGaraParams +
                                        "   AND A.ID_GaraParams = B.ID_GaraParams" +
                                        "   AND A.ID_SEGMENT = " + idSegm +
                                        "   AND A.ID_Atleta = " + dr[2].ToString() +
                                        "   AND A.ID_Segment = B.ID_Segment" +
                                        "   AND A.ID_Atleta = B.ID_Atleta", Definizioni.conn);
                        dr2 = cmd.ExecuteReader();
                        while (dr2.Read())
                        {
                            lbCompetitors.Items[numAtleti].SubItems[5].Text = String.Format("{0:0.00}", decimal.Parse(dr2[0].ToString())); // BaseTech
                            lbCompetitors.Items[numAtleti].SubItems[6].Text = String.Format("{0:0.00}", decimal.Parse(dr2[1].ToString())); // FinalTech
                            lbCompetitors.Items[numAtleti].SubItems[7].Text = String.Format("{0:0.00}", decimal.Parse(dr2[2].ToString())); // BaseArtistic

                            lbCompetitors.Items[numAtleti].SubItems[8].Text = String.Format("{0:0}", dr2[3].ToString()); // Deductions
                            lbCompetitors.Items[numAtleti].SubItems[9].Text = String.Format("{0:0.00}", decimal.Parse(dr2[4].ToString())); // Total
                            lbCompetitors.Items[numAtleti].SubItems[10].Text = dr2[5].ToString(); // Position

                            if (dr2[5].ToString().Equals("1")) lbCompetitors.Items[numAtleti].ImageIndex = 0;
                            else if (dr2[5].ToString().Equals("2")) lbCompetitors.Items[numAtleti].ImageIndex = 1;
                            else if (dr2[5].ToString().Equals("3")) lbCompetitors.Items[numAtleti].ImageIndex = 2;
                            lbCompetitors.Items[numAtleti].ForeColor = Color.Blue;
                        }
                        if (dr2.HasRows) // se segmento completato
                        {
                            lbCompetitors.Items[numAtleti].ImageIndex = 0;
                            lbCompetitors.Items[numAtleti].BackColor = Color.LightGray;
                        }
                        /*************************************************************************/

                        numAtleti++;
                    }
                }

                // recupero autocompletion per specialità
                ut.GetSkatersPerDisciplina(idSpecialita);
                if (Definizioni.nomi != null)
                {
                    var source = new AutoCompleteStringCollection();
                    string[] names = (string[])Definizioni.nomi.ToArray(typeof(string));
                    source.AddRange(names);
                    namep.AutoCompleteCustomSource = source;
                }

            } 
            catch (Exception ex)
            {
                MessageBox.Show("SkatingOrder_Load: Error..... " + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void confirmNewEvent_Click(object sender, EventArgs e)
        {
            try
            {
                // elimino tutti gli elements relativi al competitor da 'Gara'
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "DELETE FROM Participants WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                        " AND ID_Segment = " + idSegm;
                    command.ExecuteNonQuery();
                }

                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    foreach (ListViewItem lvi in lbCompetitors.Items)
                    {
                        command.CommandText = "INSERT INTO Participants " +
                        "VALUES (" + Definizioni.idGaraParams + "," + idSegm +
                        ",'" + lvi.SubItems[2].Text.Replace("'", "''") +
                        "','" + lvi.SubItems[0].Text.Replace("'", "''") + "')";

                        command.ExecuteNonQuery();
                    }
                }
                MessageBox.Show(Definizioni.resources["applied"].ToString(), "Rollart System", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show(Definizioni.resources["notapplied"].ToString(), "Rollart System Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        
        private void down_Click(object sender, EventArgs e)
        {
            try
            {
                ListViewItem selectedItem = null;
                if (lbCompetitors.SelectedItems.Count > 0)
                {
                    selectedItem = lbCompetitors.SelectedItems[0];

                    int indx = selectedItem.Index;
                    int totl = lbCompetitors.Items.Count;

                    // Se atleta ha già eseguito disabilito l'ordine d'entrata
                    if (!selectedItem.SubItems[10].Text.Equals("")) // se rank valorizzato
                    {
                        return;
                    }

                    if (indx == totl - 1)
                    {
                        if (lbCompetitors.Items[0].ForeColor == Color.Blue) // primo elemento ha già eseguito
                            return;
                        lbCompetitors.Items.Remove(selectedItem);
                        lbCompetitors.Items.Insert(0, selectedItem);
                    }
                    else
                    {

                        lbCompetitors.Items.Remove(selectedItem);
                        lbCompetitors.Items.Insert(indx + 1, selectedItem);
                    }
                }
                int index = 1;
                foreach (ListViewItem lvi in lbCompetitors.Items)
                {
                    lvi.Text = index + "";
                    lvi.BackColor = Color.White;
                    index++;
                }
                selectedItem.BackColor = Color.LightSteelBlue;
            }
            catch (Exception ex)
            {
                MessageBox.Show("MoveDown: Error..... " + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void up_Click(object sender, EventArgs e)
        {
            try
            {
                ListViewItem selectedItem = null;
                if (lbCompetitors.SelectedItems.Count > 0)
                {
                    selectedItem = lbCompetitors.SelectedItems[0];

                    int indx = selectedItem.Index;
                    int totl = lbCompetitors.Items.Count;

                    // Se atleta ha già eseguito disabilito l'ordine d'entrata
                    if (!selectedItem.SubItems[10].Text.Equals("")) // se rank valorizzato
                    {
                        return;
                    }

                    if (indx == 0)
                    {
                        lbCompetitors.Items.Remove(selectedItem);
                        lbCompetitors.Items.Insert(totl - 1, selectedItem);
                    }
                    else
                    {
                        if (lbCompetitors.Items[indx - 1].ForeColor == Color.Blue) // elemento precedente ha già eseguito
                            return;
                        lbCompetitors.Items.Remove(selectedItem);
                        lbCompetitors.Items.Insert(indx - 1, selectedItem);
                    }
                }
                int index = 1;
                foreach (ListViewItem lvi in lbCompetitors.Items)
                {
                    lvi.Text = index + "";
                    lvi.BackColor = Color.White;
                    index++;
                }
                selectedItem.BackColor = Color.LightSteelBlue;
            }
            catch (Exception ex)
            {
                MessageBox.Show("MoveDown: Error..... " + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void lbCompetitors_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void lbCompetitors_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void lbCompetitors_ItemActivate(object sender, EventArgs e)
        {
            try
            {
                if (completed)
                {
                    if (lbCompetitors.SelectedItems.Count > 0)
                    {
                        pb.Visible = true;
                        pb.Refresh();
                        pb.Update();
                        var item = lbCompetitors.SelectedItems[0];
                        string position = item.SubItems[10].Text;

                        cr.ShowGroupTreeButton = false;
                        cr.ReportSource = panelSkater;
                        cr.SelectionFormula = "{Classifica_Intestazione1.ID_GaraParams} = " + Definizioni.idGaraParams +
                                " AND {Classifica_Intestazione1.ID_Segment} = " + idSegm +
                                " AND {Classifica_Intestazione1.Position} = " + position;
                        cr.DisplayToolbar = true;
                        cr.DisplayStatusBar = true;
                        cr.DisplayBackgroundEdge = true;
                        cr.RefreshReport();
                        pb.Visible = false;
                        cr.BringToFront();
                        button1.BringToFront();
                    }
                }
            }
            catch (Exception)
            {
                pb.Visible = false;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            lbCompetitors.BringToFront();
            button1.SendToBack();
            cr.SendToBack();
        }

        // stampa ordine di entrata
        private void print_Click(object sender, EventArgs e)
        {
            try
            {
                pb.Visible = true;
                pb.Update(); dado.Refresh();

                RollartSystemTech.Report.SkatingOrder skOrderReport = 
                    new RollartSystemTech.Report.SkatingOrder();

                cr.ReportSource = skOrderReport;
                cr.SelectionFormula = "{GaraParams1.ID_GaraParams} = " + Definizioni.idGaraParams +
                                    " AND {GaraParams1.ID_Segment} = " + idSegm;
                cr.Refresh();
                skOrderReport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat,
                   RollartSystemTech.Properties.Settings.Default.EventFolder + 
                   "\\" + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year + " " + 
                   l1.Text.Replace(" - ","_") + ".pdf");

                System.Diagnostics.Process.Start(RollartSystemTech.Properties.Settings.Default.EventFolder +
                   "\\" + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year + " " +
                   l1.Text.Replace(" - ", "_") + ".pdf");

                pb.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Print: Error... " + ex.StackTrace, "RollArt Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                pb.Visible = false;
            }
        }
                
        private void lbCompetitors_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                updateskater.Enabled = false;
                add.Enabled = false;
                delete.Enabled = false;
                if (lbCompetitors.SelectedIndices.Count > 0)
                {
                    namep.Text = lbCompetitors.SelectedItems[0].SubItems[1].Text;
                    club.Text = lbCompetitors.SelectedItems[0].SubItems[3].Text;
                    naz.Text = lbCompetitors.SelectedItems[0].SubItems[4].Text;
                    namep.Focus();
                    updateskater.Enabled = true;
                    delete.Enabled = true;
                    hidden.Text = lbCompetitors.SelectedItems[0].SubItems[2].Text;// id_atleta
                }
                else
                {
                    add.Enabled = true;
                    
                    namep.Text = "";
                    club.Text = "";
                    naz.Text = "";
                    namep.Focus();
                    hidden.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("lbCompetitors_SelectedIndexChanged: " + ex.StackTrace, "RollArt Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void updateskater_Click(object sender, EventArgs e)
        {
            try
            { 
                updateskater.Enabled = false;
                foreach (ListViewItem lvi in lbCompetitors.Items)
                {
                    if (lvi.SubItems[2].Text.Equals(hidden.Text))
                    {
                        lvi.SubItems[1].Text = namep.Text;
                        lvi.SubItems[3].Text = club.Text;
                        lvi.SubItems[4].Text = naz.Text;
                        using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                        {
                            command.CommandText = "UPDATE Athletes SET " +
                            " Name = '" + namep.Text.ToString().Replace("'", "''") + "', Societa = '" + club.Text.ToString().Replace("'", "''") + "'," +
                            " Country = '" + naz.Text.ToString().Replace("'", "''") + "'" +
                            " WHERE ID_Atleta = " + hidden.Text;
                            command.ExecuteNonQuery();
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("updateskater_Click: " + ex.StackTrace, "RollArt Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void delete_Click(object sender, EventArgs e)
        {
            try
            {
                if (lbCompetitors.SelectedIndices.Count == 0) return;
                if (lbCompetitors.Items.Count > 0)
                {
                    DialogResult res = MessageBox.Show("This will delete competitor " +
                            lbCompetitors.SelectedItems[0].SubItems[1].Text.ToString().ToUpper() + ". Are you sure? ",
                        "RollartSystem", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    if (res == DialogResult.Yes)
                    {
                        // modifica del 26/11/2018 BUG trovato da Bruno Acena
                        Definizioni.numPart--;
                        using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                        {
                            cmd.CommandText = "UPDATE GaraParams SET Partecipants = '" + Definizioni.numPart + "'" +
                                " WHERE ID_GARAPARAMS = " + Definizioni.idGaraParams +
                                " AND ID_SEGMENT = " + idSegm;
                            cmd.ExecuteReader();
                        }
                        using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                        {
                            cmd.CommandText = "UPDATE GaraTotal SET Position = '-'" +
                                " WHERE ID_GARAPARAMS = " + Definizioni.idGaraParams +
                                " AND ID_Atleta = " + lbCompetitors.SelectedItems[0].SubItems[2].Text.ToString();
                            cmd.ExecuteReader();
                        }

                        using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                        {

                            cmd.CommandText = "DELETE FROM Participants WHERE ID_Atleta = "
                                + lbCompetitors.SelectedItems[0].SubItems[2].Text.ToString() +
                                " AND ID_GARAPARAMS = " + Definizioni.idGaraParams +
                                " AND ID_SEGMENT = " + idSegm;
                            cmd.ExecuteReader();

                            lbCompetitors.Items.RemoveAt(lbCompetitors.SelectedIndices[0]);
                            for (int i = 0; i < lbCompetitors.Items.Count; i++)
                            {
                                lbCompetitors.Items[i].SubItems[0].Text = "" + (i + 1);
                            }
                        }
                    }

                    delete.Enabled = true;
                    namep.Focus();

                    
                }
                else
                {
                    delete.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("delete_Click: " + ex.StackTrace, "RollArt Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void add_Click(object sender, EventArgs e)
        {
            try
            {
                if (namep.TextLength == 0)
                {
                    //MessageBox.Show(Definizioni.resources["event2"].ToString());
                    namep.BackColor = Color.LemonChiffon;
                    namep.Focus();
                    return;
                }

                int tempPart = lbCompetitors.Items.Count;
                lbCompetitors.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "", "", "", "", "", "", "" }, -1));
                lbCompetitors.Items[tempPart].SubItems[0].Text = "" + (tempPart + 1);
                lbCompetitors.Items[tempPart].SubItems[1].Text = namep.Text.ToUpper();
                lbCompetitors.Items[tempPart].SubItems[3].Text = club.Text.ToUpper();
                lbCompetitors.Items[tempPart].SubItems[4].Text = naz.Text.ToUpper();
                lbCompetitors.Items[tempPart].SubItems[11].Text = idSpecialita + "";

                // aggiorno il db
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    // modifica del 26/11/2018 BUG trovato da Bruno Acena
                    Definizioni.numPart++;
                    using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                    {
                        cmd.CommandText = "UPDATE GaraParams SET Partecipants = '" + Definizioni.numPart + "'" +
                            " WHERE ID_GARAPARAMS = " + Definizioni.idGaraParams +
                            " AND ID_SEGMENT = " + idSegm;
                        cmd.ExecuteReader();
                    }

                    command.CommandText = "INSERT INTO Athletes(Name, Societa, Country, Region, ID_Specialita)" +
                            " VALUES('" + namep.Text.ToString().Replace("'", "''") + "','" + club.Text.ToString().Replace("'", "''") + "','" + naz.Text.ToString().Replace("'", "''") + "','" + 
                                        //    region.Text + "','" + idSpecialita + "')";
                                                "','" + idSpecialita + "')";
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception) // atleta già presente
                    {
                        //MessageBox.Show("Athlete " + lb.Items[i].SubItems[1].Text + " already inserted.");
                    }

                    command.CommandText = "select ID_Atleta from Athletes " +
                                " WHERE Name = '" + namep.Text + "'" +
                                " AND Societa = '" + club.Text + "'" +
                                " AND Country = '" + naz.Text + "'" +
                                //" AND Region = '" + region.Text + "'" +
                                " AND ID_Specialita = " + idSpecialita + "";
                    SQLiteDataReader dr = command.ExecuteReader();
                    string idAtleta = "";
                    while (dr.Read())
                    {
                        idAtleta = dr[0].ToString();
                        lbCompetitors.Items[tempPart].SubItems[2].Text = idAtleta + "";
                    }
                    dr.Close();

                    command.CommandText = "INSERT INTO Participants(ID_GaraParams,ID_Segment,ID_Atleta,NumStartingList)" +
                                        " VALUES('" + Definizioni.idGaraParams + "','" + idSegm +
                                        "','" + idAtleta + "','" + (lbCompetitors.Items.Count) + "')";
                    command.ExecuteNonQuery();
                }
                lbCompetitors.Items[lbCompetitors.Items.Count - 1].EnsureVisible();

                namep.Text = "";
                club.Text = "";
                naz.Text = "";
                namep.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("add_Click: " + ex.StackTrace, "RollArt Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void namep_TextChanged(object sender, EventArgs e)
        {
            namep.BackColor = Color.White;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                pb.Visible = true;
                pb.Update(); dado.Refresh();

                ListView.ListViewItemCollection list = lbCompetitors.Items;
                Random rng = new Random();
                int n = list.Count;
                while (n > 1)
                {
                    n--;
                    int k = rng.Next(n + 1);
                    ListViewItem value1 = (ListViewItem)list[k];
                    ListViewItem value2 = (ListViewItem)list[n];
                    list[k] = new ListViewItem();
                    list[n] = new ListViewItem();
                    list[k] = value2;
                    list[n] = value1;
                }

                for (int i = 0; i < lbCompetitors.Items.Count; i++)
                {
                    lbCompetitors.Items[i].SubItems[0].Text = "" + (i + 1);
                }

                pb.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("RandomOrder: " + ex.StackTrace, "RollArt Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                pb.Visible = false;
            }
        }

        private void namep_Leave(object sender, EventArgs e)
        {
            try
            {
                //if (e.KeyCode == Keys.Enter)
                {
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "SELECT Societa, Country, Region FROM Athletes WHERE Name = '"
                            + namep.Text.TrimEnd().Replace("'", "''") + "'";
                        SQLiteDataReader dr = command.ExecuteReader();
                        if (!dr.HasRows)
                        {
                            return;
                        }
                        while (dr.Read())
                        {
                            club.Text = dr[0].ToString();
                            naz.Text = dr[1].ToString();
                            //region.Text = dr[2].ToString();
                        }
                        dr.Close();
                    }
                    if (club.Text.Equals("")) club.Focus();
                    //else if (region.Text.Equals("")) region.Focus();
                    else if (naz.Text.Equals("")) naz.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("namep_KeyDown: " + ex.StackTrace, "RollArt Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void namep_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "SELECT Societa, Country, Region FROM Athletes WHERE Name = '"
                            + namep.Text.TrimEnd() + "'";
                        SQLiteDataReader dr = command.ExecuteReader();
                        if (!dr.HasRows)
                        {
                            return;
                        }
                        while (dr.Read())
                        {
                            club.Text = dr[0].ToString();
                            naz.Text = dr[1].ToString();
                            //region.Text = dr[2].ToString();
                        }
                        dr.Close();
                    }
                    if (club.Text.Equals("")) club.Focus();
                    //else if (region.Text.Equals("")) region.Focus();
                    else if (naz.Text.Equals("")) naz.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("namep_KeyDown: " + ex.StackTrace, "RollArt Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RollartSystemTech
{
    public partial class TimerForm : Form
    {
        public TimerForm()
        {
            InitializeComponent();
        }

        private void TimerForm_Load(object sender, EventArgs e)
        {
            try
            {
                Utility.clockVideo = new Video.Clock();
                Utility.timerVideo = new Video.Timer();
            }
            catch (Exception)
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void ts1_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ts1.Checked)
                {
                    Utility.clockVideo.mode = "ON";
                    gbTimer.Enabled = true;
                }
                else
                {
                    Utility.clockVideo.mode = "OFF";
                    gbTimer.Enabled = false;
                }
                Utility.Video_ClockCommand();
            }
            catch (Exception)
            {

            }
        }

        private void BuildCommand(string mode)
        {
            try
            {
                Utility.timerVideo.mode = mode;
                if (rbSmall.Checked) Utility.timerVideo.size = "SMALL";
                else Utility.timerVideo.size = "LARGE";

                decimal seconds = (upMinutes.Value * 60) + upSeconds.Value;
                Utility.timerVideo.period = "" + seconds;
                Utility.Video_TimerCommand();
            }
            catch (Exception ex)
            {
                error.Text = ex.Message;
            }
        }

        private void play_Click(object sender, EventArgs e)
        {
            BuildCommand("START");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BuildCommand("PAUSE");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            BuildCommand("STOP");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BuildCommand("RESUME");
        }

        private void upMinutes_ValueChanged(object sender, EventArgs e)
        {
            timer.Text = String.Format("{0:00}",upMinutes.Value) + ":" +
                String.Format("{0:00}", upSeconds.Value);
        }

        private void gbTimer_Enter(object sender, EventArgs e)
        {

        }
    }
}

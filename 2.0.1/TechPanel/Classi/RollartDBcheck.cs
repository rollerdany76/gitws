﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;

namespace RollartSystemTech
{
  class RollartDBcheck
  {
    private SQLiteConnection dbConnection;
    const String CreateAccessControl =
      "CREATE TABLE AccessControl(IDAccess INTEGER PRIMARY KEY AUTOINCREMENT," +
      " Username STRING, Password CHAR, LastAccess TEXT, IPAddress TEXT)";
    const String CreateAthletes =
      "CREATE TABLE Athletes (ID_Atleta INTEGER PRIMARY KEY AUTOINCREMENT," +
      " Name CHAR, Societa CHAR, Country CHAR, Region STRING, ID_Specialita INTEGER)";
    const String CreateCategory =
      "CREATE TABLE [Category] ( [ID_Category] INTEGER NOT NULL, [Name] CHAR)";
    const String CreateComponents =
      "CREATE TABLE [Components] ( [Code] CHAR, [Name] CHAR, [Description] CHAR)";
    const String CreateElements =
      "CREATE TABLE Elements (ID_Element INTEGER NOT NULL PRIMARY KEY, Name CHAR," +
      " Level INTEGER, Code CHAR UNIQUE, Level3 NUMERIC, Level2 NUMERIC, " +
      " Level1 NUMERIC, Base NUMERIC, BaseDown NUMERIC, BaseHalf NUMERIC DEFAULT 0," +
      " LevelNeg1 NUMERIC, LevelNeg2 NUMERIC, LevelNeg3 NUMERIC, ElementsCombo NUMERIC," +
      " ElementsComboDown NUMERIC DEFAULT 0, ElementsComboHalf NUMERIC DEFAULT 0," +
      " ID_Segment INTEGER, ID_ElementsCat INTEGER)";
    const String CreateElementsCat =
      "CREATE TABLE [ElementsCat] ( [ID_ElementsCat] INTEGER PRIMARY KEY NOT NULL, [Code] TEXT NULL, [Name] TEXT NULL, [Category] TEXT NULL )";
    const String CreateGara =
      "CREATE TABLE Gara (ID_GaraParams INTEGER, ID_Segment INTEGER, NumPartecipante INTEGER," +
      " ProgressivoEl INTEGER, Element CHAR, Pen CHAR, Value NUMERIC, ValueFinal NUMERIC," +
      " NumCombo INTEGER, qoe_1 TEXT DEFAULT 0, qoe_2 TEXT DEFAULT 0, qoe_3 TEXT DEFAULT 0," +
      " qoe_4 TEXT DEFAULT 0, qoe_5 TEXT DEFAULT 0, qoe_6 TEXT DEFAULT 0, qoe_7 TEXT DEFAULT 0," +
      " qoe_8 TEXT DEFAULT 0, qoe_9 TEXT DEFAULT 0, qoev_1 NUMERIC DEFAULT 0, qoev_2 NUMERIC DEFAULT 0," +
      " qoev_3 NUMERIC DEFAULT 0, qoev_4 NUMERIC DEFAULT 0, qoev_5 NUMERIC DEFAULT 0, qoev_6 NUMERIC DEFAULT 0," +
      " qoev_7 NUMERIC DEFAULT 0, qoev_8 NUMERIC DEFAULT 0, qoev_9 NUMERIC DEFAULT 0, Bonus CHAR, ProgrCombo INTEGER DEFAULT (0))";
    const String CreateGaraFinal =
      "CREATE TABLE [GaraFinal] ( [ID_GaraParams] INTEGER, [ID_Segment] INTEGER, [NumPartecipante] INTEGER," +
      " [BaseTech] NUMERIC, [FinalTech] NUMERIC, [BaseArtistic] NUMERIC, [Deductions] NUMERIC," +
      " [Total] NUMERIC, [Position] INTEGER, [ID_Atleta] INTEGER)";
    const String CreateGaraParams =
      "CREATE TABLE GaraParams (ID_GaraParams INTEGER, Name CHAR, Place CHAR, Date TEXT, DateEnd TEXT," +
      " ID_Segment INTEGER, ID_Specialita INTEGER, ID_Category INTEGER, Partecipants INTEGER, Sex CHAR," +
      " Completed CHAR, LastPartecipant INTEGER, NumJudges INTEGER, Factor DECIMAL)";
    const String CreateGaraTotal =
      "CREATE TABLE GaraTotal (ID_GaraParams INTEGER, ID_Atleta INTEGER, Position INTEGER," +
      " Atleta CHAR, TotGara NUMERIC, PosSegment1 INTEGER, PosSegment2 INTEGER)";
    const String CreateJudges =
      "CREATE TABLE Judges (ID_Judge INTEGER PRIMARY KEY AUTOINCREMENT, Name CHAR, Region CHAR, Country CHAR)";
    const String CreatePanelJudge =
      "CREATE TABLE [PanelJudge] ( [ID_Judge] INTEGER, [ID_GaraParams] INTEGER, [Role] CHAR)";
    const String CreateParticipants =
      "CREATE TABLE [Participants] ( [ID_GaraParams] INTEGER, [ID_Segment] INTEGER, [ID_Atleta] INTEGER, [NumStartingList] INTEGER)";
    const String CreateSegmentParams =
      "CREATE TABLE SegmentParams (ID_Params INTEGER, ID_Segment INTEGER, ID_Specialita INTEGER, NumElems INTEGER," +
      " NumJumps INTEGER, NumSpins INTEGER, NumSteps INTEGER, Durata INTEGER, NumSoloJumps INTEGER," +
      " NumComboJumps INTEGER, MaxElemComboJumps INTEGER, MinElemComboJumps INTEGER, NumSoloSpins INTEGER," +
      " NumComboSpins INTEGER, Factor DECIMAL, ID_Category INTEGER, ComboJumpDD INTEGER, ComboJumpTD INTEGER," +
      " ComboJumpTT INTEGER, MaxElemComboSpins INTEGER, MaxHeelSpin INTEGER, MaxSameSpin INTEGER," +
      " MaxSameJump INTEGER, BonusValue DECIMAL, SitMandatory INTEGER)";
    const String CreateSegmentParamsDance =
      "CREATE TABLE SegmentParamsDance (ID_Params INTEGER, ID_Segment INTEGER, ID_Specialita INTEGER," +
      " ID_Category INTEGER, Durata INTEGER, Factor DECIMAL, NoHoldStepSequence INTEGER," +
      " DanceHoldStepSequence INTEGER, PatternDanceSeq INTEGER, DanceLift INTEGER," +
      " NoHoldSynchroClusterSeq INTEGER, TravellingSeq INTEGER, StepSeqSoloDance INTEGER," +
      " CircularStepSeq INTEGER, NumElements INTEGER, Choreo INTEGER, HoldClustSeq INTEGER)";
    const String CreateSegmentParamsPairs =
      "CREATE TABLE SegmentParamsPairs (ID_SegParamsPairs INTEGER, ID_Segment INTEGER," +
      " ID_Specialita INTEGER, ID_Category INTEGER, Durata INTEGER, Factor DECIMAL," +
      " NumElems INTEGER, NumPosLift INTEGER, NumComboLift INTEGER, NumTwistJumps INTEGER," +
      " NumThrowJumps INTEGER, NumSideJumps INTEGER, NumSideComboJumps INTEGER," +
      " NumContactSpins INTEGER, NumSideSpins INTEGER, NumSpirals INTEGER, NumSteps INTEGER," +
      " ComboLiftL1 INTEGER, ComboLiftL2 INTEGER, ComboLiftL3 INTEGER, ComboLiftL4 INTEGER," +
      " ComboLiftL5 INTEGER, PercentageCombo INTEGER, ComboJumpDD INTEGER, ComboJumpTD INTEGER," +
      " ComboJumpTT INTEGER, BonusValue DECIMAL)";
    const String CreateSegmentParamsPrecision =
      "CREATE TABLE SegmentParamsPrecision (ID_Params INTEGER, ID_Segment INTEGER," +
      " ID_Specialita INTEGER, ID_Category INTEGER, Durata INTEGER, Factor DECIMAL," +
      " NoHoldBlock INTEGER, LinearBlock INTEGER, WheelsTravelling INTEGER," +
      " WheelsRotating INTEGER, CirclesTravelling INTEGER, CirclesRotating INTEGER," +
      " LinearLine INTEGER, Intersections INTEGER, Combined INTEGER, Creative INTEGER," +
      " LinePivoting INTEGER, BlockPivoting INTEGER)";
    const String CreateSegments =
      "CREATE TABLE [Segments] ( [ID_Segments] INTEGER NOT NULL PRIMARY KEY, [Code] TEXT NULL, [Name] TEXT NULL )";
    const String CreateSpecialita =
      "CREATE TABLE [Specialita] ( [ID_Specialita] INTEGER NOT NULL PRIMARY KEY, [Code] TEXT NULL, [Name] TEXT NULL )";

    public RollartDBcheck(SQLiteConnection con)
    {
      dbConnection = con;
    }

    ~RollartDBcheck()
    {
      // Destructor
    }

    private bool TableExists(String tableName)
    {
      bool Exists = false;
      // Create a database command
      using (var command = new SQLiteCommand(dbConnection))
      {
        command.CommandText = "SELECT name FROM sqlite_master WHERE name='" + tableName + "'";
        var name = command.ExecuteScalar();

        // check table exists or not 
        // if exist do nothing 
        if ((name != null) && (name.ToString().ToLower() == tableName.ToLower()))
        {
          Exists = true;
        }
      }
      return Exists;
    }

    private bool ColumnExists(String tableName, String ColumnName)
    {
      bool Exists = false;
      SQLiteCommand command = new SQLiteCommand("PRAGMA table_info('" + tableName + "')", dbConnection);
      DataTable dataTable = new DataTable();
      SQLiteDataAdapter dataAdapter = new SQLiteDataAdapter(command);
      dataAdapter.Fill(dataTable);
      foreach (DataRow row in dataTable.Rows)
      {
        String name = (string)row[dataTable.Columns[1]];
        if (ColumnName.ToLower() == name.ToLower())
        {
          Exists = true;
          break;
        }
      }
      return Exists;
    }

    public bool CheckTables()
    {
      bool resp = true;
      using (var command = new SQLiteCommand(dbConnection))
      {
        if (!TableExists("AccessControl"))
        {
          // AccessControl table doen't exist, create the table
          command.CommandText = CreateAccessControl;
          command.ExecuteNonQuery();
        }
        if (!TableExists("Athletes"))
        {
          // Athletes table doen't exist, create the table
          command.CommandText = CreateAthletes;
          command.ExecuteNonQuery();
        }
        if (!TableExists("Category"))
        {
          // Category table doen't exist, create the table
          command.CommandText = CreateCategory;
          command.ExecuteNonQuery();
        }
        if (!TableExists("Components"))
        {
          // Components table doen't exist, create the table
          command.CommandText = CreateComponents;
          command.ExecuteNonQuery();
        }
        if (!TableExists("Elements"))
        {
          // Elements table doen't exist, create the table
          command.CommandText = CreateElements;
          command.ExecuteNonQuery();
        }
        if (!TableExists("ElementsCat"))
        {
          // ElementsCat table doen't exist, create the table
          command.CommandText = CreateElementsCat;
          command.ExecuteNonQuery();
        }
        if (!TableExists("Gara"))
        {
          // Gara table doen't exist, create the table
          command.CommandText = CreateGara;
          command.ExecuteNonQuery();
        }
        if (!TableExists("GaraFinal"))
        {
          // GaraFinal table doen't exist, create the table
          command.CommandText = CreateGaraFinal;
          command.ExecuteNonQuery();
        }
        if (!TableExists("GaraParams"))
        {
          // GaraParams table doen't exist, create the table
          command.CommandText = CreateGaraParams;
          command.ExecuteNonQuery();
        }
        if (!TableExists("GaraTotal"))
        {
          // GaraTotal table doen't exist, create the table
          command.CommandText = CreateGaraTotal;
          command.ExecuteNonQuery();
        }
        if (!TableExists("Judges"))
        {
          // Judges table doen't exist, create the table
          command.CommandText = CreateJudges;
          command.ExecuteNonQuery();
        }
        if (!TableExists("PanelJudge"))
        {
          // PanelJudge table doen't exist, create the table
          command.CommandText = CreatePanelJudge;
          command.ExecuteNonQuery();
        }
        if (!TableExists("Participants"))
        {
          // Participants table doen't exist, create the table
          command.CommandText = CreateParticipants;
          command.ExecuteNonQuery();
        }
        if (!TableExists("SegmentParams"))
        {
          // SegmentParams table doen't exist, create the table
          command.CommandText = CreateSegmentParams;
          command.ExecuteNonQuery();
        }
        if (!TableExists("SegmentParamsDance"))
        {
          // SegmentParamsDance table doen't exist, create the table
          command.CommandText = CreateSegmentParamsDance;
          command.ExecuteNonQuery();
        }
        if (!TableExists("SegmentParamsPairs"))
        {
          // SegmentParamsPairs table doen't exist, create the table
          command.CommandText = CreateSegmentParamsPairs;
          command.ExecuteNonQuery();
        }
        if (!TableExists("SegmentParamsPrecision"))
        {
          // SegmentParamsPrecision table doen't exist, create the table
          command.CommandText = CreateSegmentParamsPrecision;
          command.ExecuteNonQuery();
        }
        if (!TableExists("Segments"))
        {
          // Segments table doen't exist, create the table
          command.CommandText = CreateSegments;
          command.ExecuteNonQuery();
        }
        if (!TableExists("Specialita"))
        {
          // Specialita table doen't exist, create the table
          command.CommandText = CreateSpecialita;
          command.ExecuteNonQuery();
        }
      }
      return resp;
    }

    public bool UpdateTables()
    {
      bool resp = true;
      using (var command = new SQLiteCommand(dbConnection))
      {
        // Added Version 2.0.1
        if (!ColumnExists("Participants", "InfoFIeld1"))
        {
          command.CommandText = "ALTER TABLE[Participants] ADD [InfoFIeld1] TEXT NULL";
          command.ExecuteNonQuery();
          command.CommandText = "ALTER TABLE[Participants] ADD [InfoFIeld2] TEXT NULL";
          command.ExecuteNonQuery();
          command.CommandText = "ALTER TABLE[Participants] ADD [InfoFIeld3] TEXT NULL";
          command.ExecuteNonQuery();
        }
        // NOTE: SQLITE only supports ADD of a single column per command
        // If you want to delete columns or change data types then you must
        // Create a new temptable, copy from current table to new temptable, drop old table
        // rename new temptable as the old table name
      }
      return resp;
    }
  }
}

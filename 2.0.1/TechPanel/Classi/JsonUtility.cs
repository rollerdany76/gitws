﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace RollartSystemTech
{
    public class JsonUtility
    {
        public string LoadJson(string fileName)
        {
            string json = "";
            string extension = Path.GetExtension(fileName);
            if (!extension.Equals(".json")) return "Error. Not a JSON file";

            using (StreamReader r = new StreamReader(fileName))
            {
                json = r.ReadToEnd();
            }
            return json;
        }

        public int ReadIdEventFromJson(string fileName)
        {
            int idfile = 0;
            //JsonTextReader reader = new JsonTextReader(new StringReader(fileName));

            using (StreamReader file = File.OpenText(fileName))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                //JObject o2 = (JObject)JToken.ReadFrom(reader);
                while (reader.Read())
                {
                    if (reader.Value != null)
                    {
                        if (reader.Value.Equals("idfile"))
                        {
                            reader.Read();
                            idfile = int.Parse(reader.Value.ToString());
                            break;
                        }
                    }
                }
            }
            return idfile;
        }

        #region JSON UTILITY, Segment Model
        public class Atleta
        {
            public int idskater { get; set; }
            public int order { get; set; }
            public string name { get; set; }
            public string name2 { get; set; }
            public string club { get; set; }
            public string nation { get; set; }
            public int number { get; set; }
            public string region { get; set; }
            public int completed { get; set; }
            public List<Video.IN_Skater.Info> info { get; set; }
        }

        public class Skaters
        {
            public int numskaters { get; set; }
            public List<Atleta> list { get; set; }
            public List<int> skatersforgroup { get; set; }
        }

        public class Segment
        {
            public int idsegment { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public Skaters skaters { get; set; }
            public string status { get; set; }
            public int idsegmentdetail { get; set; }
        }

        public class Giudice
        {
            public int idjudge { get; set; }
            public int num { get; set; }
            public string name { get; set; }
            public string name2 { get; set; }
            public string role { get; set; }
            public string nation { get; set; }
            public string ip { get; set; }
            public string handle { get; set; }
        }

        public class Judges
        {
            public int numJudges { get; set; }
            public List<Giudice> list { get; set; }
        }

        public class Category
        {
            public int idcategory { get; set; }
            public int idcompetition { get; set; }
            public string name { get; set; }
            public List<Segment> segment { get; set; }
            public Judges judges { get; set; }
        }

        public class Discipline
        {
            public int iddiscipline { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public List<Category> category { get; set; }
        }

        public class Event
        {
            public string name { get; set; }
            public string place { get; set; }
            public string date { get; set; }
            public int idevent { get; set; }
            public string description { get; set; }
            public List<Discipline> discipline { get; set; }
            public string filename { get; set; }
            public int idfile { get; set; }
        }

        public class StartSegment
        {
            public int idfile { get; set; }
            public string creationdate { get; set; }
            public List<Event> @event { get; set; }

            public StartSegment(List<Event> ev)//, int id, string date) 
            {
                this.@event = ev;
            }
        }

        public class RootObject
        {
            public StartSegment StartSegment { get; set; }
        }
#endregion

    }

}

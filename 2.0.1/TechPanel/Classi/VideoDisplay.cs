﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace RollartSystemTech
{
    /********************************************************************************
    *  Classi per gestire la connessione e i messaggi verso il sistema video
    *  
    *   IN : TP --> VS
    *   OUT: TP <-- VS
     * *****************************************************************************/
    class VideoDisplay
    {
        public static TcpClient videoSocket = null;

        // Messages TO Display FROM Tech
        public static JObject blankTOdisplay = null, skaterTOdisplay = null, progressTOdisplay = null,
                              resultsegmentTOdisplay = null; //, resultcombTOdisplay = null;

        // Messages TO Tech FROM Display
        public static JObject eventlistFROMdisplay = null, eventorderFROMdisplay = null, skaterinfoFROMdisplay = null,
                              eventresultFROMdisplay = null; //, eventresultFROMdisplay = null;


        public static Main main = null;
        public static NetworkStream techVideoStream = null;
        public Thread listenThread;
        bool exitFlag = false;
        ASCIIEncoding encoder = null;
        JsonUtility ju = null;

        public VideoDisplay(Main mainForm)
        {
            exitFlag = false;
            ju = new JsonUtility();
            encoder = new ASCIIEncoding();
            if (mainForm != null)
                main = mainForm;
        }

        int count = 0;
        public void TryToConnectToVideo()
        {
            // dopo 5 tentativi esco e devo connettermi manualmente
            while (true)
            {
                
                if (Definizioni.stateVideo != 2) // se video display non connesso
                {
                    if (count == 5)
                    {
                        UpdateButtonsState("KO");
                    }
                    ConnectToVideoDisplay();
                    count++;
                    if (count < 5 && Definizioni.stateVideo != 2) UpdateButtonsState("" + count);
                }
                else
                {
                    try
                    {
                        // verifio se il VideoScreen si e disconnesso
                        //techVideoStream = videoSocket.GetStream();
                        //techVideoStream.Read();
                    }
                    catch (Exception) { }
                }

                Thread.Sleep(Properties.Settings.Default.PingTimeVideo);
                try
                {

                    if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                    {

                    }
                    else
                    {
                        Definizioni.stateVideo = 0;
                        videoSocket.Close();
                        UpdateButtonsState("KO");
                    }
                }
                catch (Exception) { }
            }
        }


        // mi connetto al sistema video
        public void ConnectToVideoDisplay()
        {
            try
            {
                videoSocket = new TcpClient();
                videoSocket.Connect(RollartSystemTech.Properties.Settings.Default.VideoIP,
                        RollartSystemTech.Properties.Settings.Default.VideoPort);

                Utility.WriteLog("TechPanel connected to VIDEO SCREEN (" +
                    RollartSystemTech.Properties.Settings.Default.VideoIP + ":" +
                    RollartSystemTech.Properties.Settings.Default.VideoPort + ")", "OK");

                listenThread = new Thread(RicevoDatiDalVideo);
                listenThread.Start();
                Thread.Sleep(100);

                if (!LoadCommands())
                    return;

                UpdateButtonsState("OK");

                InvioDatiAlVideo(blankTOdisplay.ToString());
            }
            catch (Exception ex)
            {
                UpdateButtonsState("KO");
                //Utility.WriteLog("VIDEO DISPLAY not connected. Error:" + ex.Message, "ERROR");
            }
        }

        public bool LoadCommands()
        {
            try
            {
                using (StreamReader reader = File.OpenText(
                    RollartSystemTech.Properties.Settings.Default.CommandsFolder + "\\blank.json"))
                {
                    blankTOdisplay = (JObject)JToken.ReadFrom(new JsonTextReader(reader));
                }
                using (StreamReader reader = File.OpenText(
                    RollartSystemTech.Properties.Settings.Default.CommandsFolder + "\\skater.json"))
                {
                    skaterTOdisplay = (JObject)JToken.ReadFrom(new JsonTextReader(reader));
                }
                using (StreamReader reader = File.OpenText(
                    RollartSystemTech.Properties.Settings.Default.CommandsFolder + "\\resultsegment.json"))
                {
                    resultsegmentTOdisplay = (JObject)JToken.ReadFrom(new JsonTextReader(reader));
                }
                using (StreamReader reader = File.OpenText(
                    RollartSystemTech.Properties.Settings.Default.CommandsFolder + "\\progress.json"))
                {
                    progressTOdisplay = (JObject)JToken.ReadFrom(new JsonTextReader(reader));
                }

                return true;

            }
            catch (Exception ex)
            {
                Utility.WriteLog("LoadCommands():" + ex.Message, "ERROR");
                return false;
            }
        }

        public void UpdateCommands(ArrayList listValues, JObject oggetto)
        {
            try
            {
                int count = 1;
                foreach (var x in oggetto)
                {
                    string name = x.Key;
                    if (!name.Equals("command"))
                    {
                        oggetto[name] = listValues[count].ToString();
                        count++;
                    }
                  
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("UpdateCommands:" + ex.Message, "ERROR");
            }
        }

        private void RicevoDatiDalVideo(object client)
        {

            techVideoStream = videoSocket.GetStream();

            byte[] message = new byte[4096];
            int bytesRead;

            while (true)
            {
                if (exitFlag) break;
                bytesRead = 0;

                try
                {
                    bytesRead = techVideoStream.Read(message, 0, message.Length);
                }
                catch (SocketException se)
                {
                    ManageVideoError("Socket Exception:" + se.InnerException.Message, "ERROR");
                    break;
                }
                catch (IOException se)
                {
                    var socketExept = se.InnerException as SocketException;
                    if (socketExept == null || socketExept.ErrorCode != 10060)
                        ManageVideoError("HandleClientComm() - IOException: " + socketExept.Message +
                            " ErrorCode:" + socketExept.ErrorCode, "KO");
                    bytesRead = 0;
                    break;
                }

                if (bytesRead == 0)
                {
                    ManageVideoError("Video display has been disconnected", "KO");
                    break;
                }

                string stringaRicevuta = encoder.GetString(message, 0, bytesRead);
                // verifico il messaggio del server video...
                VerificoMessaggioVideo(stringaRicevuta);
            }

        }

        public bool VerificoMessaggioVideo(string stringaRicevuta)
        {
            try
            {
                //var jsonString = JsonConvert.DeserializeObject(stringaRicevuta);

                Video.Response resResponse = JsonConvert.DeserializeObject<Video.Response>(stringaRicevuta);

                // From Tech to Video and "response" from Video
                if (resResponse.response != null)
                {
                    Form formTimer = Application.OpenForms["TimerForm"];
                    Form formMain = Application.OpenForms["Main"];

                    if (resResponse.response.Equals("success"))
                    {
                        Utility.WriteLog("<-- RVD System. Json data: " + stringaRicevuta, "INFO");
                        if (formTimer != null)
                        {
                            var iconDisplay = formTimer.Controls.Find("display", true);
                            if (iconDisplay.Length > 0)
                            {
                                ((Label)iconDisplay[0]).Image = Properties.Resources.videoscreenOk;
                                ((Label)iconDisplay[0]).Text = "";
                            }
                        }

                        return true;
                    }
                    else
                    {
                        Utility.WriteLog("<-- RVD System. Json data: " + stringaRicevuta, "WARNING");
                        if (formTimer != null)
                        {
                            var iconDisplay = formTimer.Controls.Find("display", true);
                            if (iconDisplay.Length > 0)
                            {
                                ((Label)iconDisplay[0]).Image = Properties.Resources.videoscreenKo;
                                //((Label)iconDisplay[0]).Text = "Error";
                            }
                        }

                        return false;
                    }
                }

                // From Video to Tech 
                Video.IN_Command resCommand = JsonConvert.DeserializeObject<Video.IN_Command>(stringaRicevuta);
                string stringToVideo = "";

                if (resCommand.command == null) return false;
                else
                {
                    switch (resCommand.command)
                    {
                        case "event_list":
                            Utility.LoadEventsForVIdeo();
                            stringToVideo = JsonConvert.SerializeObject(Utility.events4video);
                            InvioDatiAlVideo(stringToVideo);
                            break;
                        case "result":
                            // Send results
                            Utility.GetResults(resCommand.id, resCommand.segment, resCommand.type);
                            stringToVideo = JsonConvert.SerializeObject(Utility.result4Video);
                            InvioDatiAlVideo(stringToVideo);
                            break;
                        case "order":
                            // send segment skating order  
                            Utility.GetOrderForEvent(resCommand.id, resCommand.segment);
                            stringToVideo = JsonConvert.SerializeObject(Utility.order4video);
                            InvioDatiAlVideo(stringToVideo);
                            break;
                        case "info":
                            Utility.GetInfoForSkater(resCommand.id, resCommand.segment, resCommand.order);
                            stringToVideo = JsonConvert.SerializeObject(Utility.skaterinfo4video);
                            InvioDatiAlVideo(stringToVideo);
                            break;
                    }
                }

                InvioDatiAlVideo(stringToVideo);

                return true;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("VerificoMessaggioVideo():" + ex.Message, "ERROR");
                return false;
            }
        }

        public void ManageVideoError(string errorMsg, string errorType)
        {
            Utility.WriteLog(errorMsg, errorType);
            UpdateButtonsState(errorType);
        }

        public static void InvioDatiAlVideo(string dati)
        {
            try
            {
                byte[] outStream = System.Text.Encoding.UTF8.GetBytes(dati);
                techVideoStream.Write(outStream, 0, outStream.Length);
                techVideoStream.Flush();
                Utility.WriteLog("--> RVD System. Json data:" + dati.Replace("\r\n",""), "OK");
            }
            catch (Exception ex)
            {
                Utility.WriteLog("InvioDatiAlVideo():" + ex.Message, "ERROR");
            }
        }

        public void DisconnectVideoDisplay()
        {
            //exitFlag = true;
            if (videoSocket != null)
            {
                if (videoSocket.Connected)
                {
                    videoSocket.Close();
                    UpdateButtonsState("KO");
                    videoSocket = null;
                }
            }
            
            
        }

        public void UpdateButtonsState(string msg)
        {
            try
            {
                // cambio stato bottone su EventsForm
                if (main != null)
                {
                    var topStrip = main.Controls.Find("statusStrip", true);
                    if (topStrip.Length > 0)
                    {
                        var toolstrip1Items = topStrip[0] as ToolStrip;
                        var btnVideo = toolstrip1Items.Items.Find("tsVideo", true); //<--get BtnRead on toolstrip Item.Find                                                                                       //btnGiudice[0].Enabled = false;
                        btnVideo[0].ToolTipText = msg;
                        btnVideo[0].Enabled = true;
                        //Definizioni.stateVideo = 0;
                        var buttonConnect = btnVideo[0] as ToolStripDropDownItem;
                        if (msg.Equals("OK"))
                        {
                            btnVideo[0].ToolTipText = "VideoScreen Connected (" +
                                    RollartSystemTech.Properties.Settings.Default.VideoIP + ":" +
                                    RollartSystemTech.Properties.Settings.Default.VideoPort + ")";
                            btnVideo[0].Image = RollartSystemTech.Properties.Resources.displayblu;
                            btnVideo[0].Text = "Video (Connected)";
                            Definizioni.stateVideo = 2;
                            buttonConnect.DropDownItems[0].Tag = 2; // connesso
                            buttonConnect.DropDownItems[0].Text = "Disconnect";
                            buttonConnect.DropDownItems[0].Enabled = true;
                        }
                        else if (msg.Equals("KO"))
                        {
                            //if (Definizioni.stateVideo != 0)
                            {
                                btnVideo[0].ToolTipText = "VideoScreen Disconnected (" +
                                    RollartSystemTech.Properties.Settings.Default.VideoIP + ":" +
                                    RollartSystemTech.Properties.Settings.Default.VideoPort + ")";
                                btnVideo[0].Image = RollartSystemTech.Properties.Resources.displayred;
                                btnVideo[0].Text = "Video (Disconnected)";
                                Definizioni.stateVideo = 0;
                                buttonConnect.DropDownItems[0].Tag = 1; // disconnesso
                                buttonConnect.DropDownItems[0].Text = "Connect";
                                buttonConnect.DropDownItems[0].Enabled = true;
                            }
                        }
                        else
                        {
                            btnVideo[0].ToolTipText = msg  + " attempt to connect to VideoScreen (" +
                                    RollartSystemTech.Properties.Settings.Default.VideoIP + ":" +
                                    RollartSystemTech.Properties.Settings.Default.VideoPort + ")";
                            buttonConnect.DropDownItems[0].Enabled = false;
                            btnVideo[0].Image = RollartSystemTech.Properties.Resources.displayyellow;
                            btnVideo[0].Text = "Video (connecting..." + msg + "/5) ";
                            Definizioni.stateVideo = 1;
                        }
                    }
                }
                
            }
            catch (Exception)
            {
            }
        }

    }

    #region VIDEO DISPLAY classes

    public class Video
    {
        /*
         *  For all commands to the display application, the application will respond ù
         *  with a JSON response packet as follows:
         *  “status”: “success”
         *  “status”: “illegal”
            TP --> VS
        */
        public class Response
        {
            public string response { get; set; }
            public string status { get; set; }
        }

        /*
            TP --> VS
        */
        public class Blank
        {
            public string command { get; set; }
        }

        /*
            TP --> VS
            mode “OFF” or “ON”. Off removes the clock display, ON adds the clock to the display
        */
        public class Clock
        {
            public string command { get; set; }
            public string mode { get; set; }
        }

        /*
         * TP --> VS
         * mode “STOP”, “START”, “PAUSE” or “RESUME”.
                STOP removes the timer from the display.
                START loads the timer with the period and displays timer on the screen.
                PAUSE stops the timer counting down and leaves timer on screen at paused period.
                RESUME resumes the timer from the paused value.
           period Timer start period in seconds as numeric string. E.g. 5 minutes = 300 seconds.
           size Timer size, “SMALL” or “LARGE”.
                */
        public class Timer
        {
            public string command { get; set; }
            public string mode { get; set; }
            public string period { get; set; }
            public string size { get; set; }
        }

        /*
         * TP --> VS
         *  Leader Two decimal place total technical score “0.00” to “999.99” of the current leaders technical score. 
         *  If there is no leader score either the leader score is sent as “0.00” or “” or the leader field may be omitted from the JSON command (Second form of command).
            Current technical score at this point of routine “0.00” to “999.99”.
            // TP --> VS
        */
        public class ProgressiveTotal
        {
            public string command { get; set; }
            public string leader { get; set; }
            public string current { get; set; }
        }

        /*
         * TP --> VS
         *  “command” : “skater”,
            “skater” : “skater name”,
            “country” : “3 digit country code”,
            “order” : “skating order”,
            “segment” : “code”,
            "info" :
                [
                    {"row" : "1", "msg" : "Music: La donna cannone"},
                    {"row" : "2", "msg" : "Coach: Maria Rita Zenobi, Michele Terruzzi"}
                ]

         *  Skater Name is string up to 40 characters maximum with single skater name or both skater’s names.
            Country Country is the 3 character international country code, e.g. ‘AUS’. Omit this field if no country is to be displayed.
            Order Order is the current skaters order. Omit this field if no order is to be displayed.
            Segment Segment is the 2 character segment code. Omit this field if no segment code is to be displayed.
            Info Info is additional text about the skater such as coaches, choreographer, music etc. This text can be up to 40 characters 
            long for each message and can have up to 4 entries in an array. Omit this JSON array if no information text is to be displayed.
        */
        public class Skater
        {
            public string command { get; set; }
            public string skater { get; set; }
            public string country { get; set; }
            public string order { get; set; }
            public string segment { get; set; } // code
            //public List<Info> info { get; set; }
            public string info { get; set; }
        }

        public class Info
        {
            public string row { get; set; }
            public string msg { get; set; }
        }

        /*
         * TP --> VS
         *  Type	Type of result, either “single” for a single segment result or “combined” for a combined segment result.
            Skater	Name is string up to 40 characters maximum with single skater name or both skater’s names.
            Country	Country is the 3 character international country code, e.g. ‘AUS’.  Omit this field if no country is to be displayed.
            Order	Order is the current skaters order. Omit this field if no order is to be displayed.
            Segment	Segment is the 2 character segment code. Omit this field if no segment code is to be displayed.
            Rank	Rank is the current skaters ranking for this segment. Omit this field if no order is to be displayed.
            
            Seg_code Seg_code is the 2 character segment code.
            Tech_score Technical score for the current segment.
            Pres_score Presentation score for the current segment.
            Deduction	Deductions for the current segment.
            Prev_score Total score for the previous segment.
            Prev_rank Skater rank for the previous segment.
            Seg_score Total score for the current segment.
            Seg_rank Skater rank for the current segment.
            Total_score	Total score for the combined segments.
            Total_rank	Skater rank for the combined segments. Omit for sing segment result.
            Rank Rank is the current skaters ranking to this point in time (segment or final).
            Comb_code Comb_code is the 2 character combined segments code.
            
        */
        public class Result_Single
        {
            public string command { get; set; }
            public string type { get; set; }
            public string skater { get; set; }
            public string country { get; set; }
            public string order { get; set; }
            public string seg_code { get; set; } 
            public string tech_score { get; set; } 
            public string pres_score { get; set; } 
            public string deduction { get; set; } 
            public string total_score { get; set; } 
            public string rank { get; set; }
        }
        // 17 campi
        public class Result_Combined
        {
            public string command { get; set; }
            public string type { get; set; }
            public string skater { get; set; }
            public string country { get; set; }
            public string order { get; set; }
            public string tech_score { get; set; } 
            public string pres_score { get; set; } 
            public string deduction { get; set; } 
            public string prev_score { get; set; } 
            public string prev_rank { get; set; } 
            public string prev_seg { get; set; } 
            public string seg_score { get; set; } 
            public string seg_rank { get; set; } 
            public string seg_code { get; set; } 
            public string total_score { get; set; } 
            public string rank { get; set; } 
            public string comb_code { get; set; } 
        }

        public class IN_Skater
        {

            public class Info_Response
            {
                public string response { get; set; }
                public string id { get; set; }
                public string segment { get; set; }
                public string order { get; set; }
                public string name { get; set; }
                public string country { get; set; }
                public string skater_info { get; set; }
            }

            public class Info
            {
                public string skater_info { get; set; }
            }
        }

        /* The skating order JSON command is:
            {
            “command” : “order”,
            “id” : “event_id”,
            “segment” : “segment_id”
            }
            Where:
            id Event unique ID from event list.
            Segment Event segment code from event list.
            The response is a list of the order of skaters for the event
            {
            “response” : “event_order”,
            “id” : “event_id”,
            “segment” : “segment_id”
            “skaters”:[
            {“order”:”skater_order”, ”name”:”skater_name”, ”country”:”skater_country”},
            {“order”:”skater_order”, ”name”:”skater_name”, ”country”:”skater_country”},
            {“order”:”skater_order”, ”name”:”skater_name”, ”country”:”skater_country”}
            ]
            }
            Field values are:
            id Event unique ID.
            Segment Event segment code.
            Order Skating Order as number string.
            Name Text String of Skater Name (40 chars max).
            Country 3 Character ISO country code string.
            */
        public class IN_EventOrder
        {

            public class EventOrder_Response
            {
                public string response { get; set; }
                public string id { get; set; }
                public string segment { get; set; }
                public string segname { get; set; }
                public List<Skater> skaters { get; set; }
            }

            public class Skater
            {
                public string order { get; set; }
                public string name { get; set; }
                public string country { get; set; }
            }
        }

        /* The Event List JSON command is:
            {
            “command” : “event_list”
            }
            The response is a list of the available events
            {
            “response” : “event_list”,
            “events”:[
            {“id”:”event_id”, ”name”:”event_name”, ”segment”:”segment_code”, “segname”:”Segment_name”, ”discipline”:”event_discipline”, “category”: “event_category”},
            {“id”:”event_id”, ”name”:”event_name”, ”segment”:”segment_id”, “segname”:”Segment_name”, ”discipline”:”event_discipline”, “category”: “event_category”},
            {“id”:”event_id”, ”name”:”event_name”, ”segment”:”segment_id”, “segname”:”Segment_name”, ”discipline”:”event_discipline”, “category”: “event_category”}
            ]
            }
            Where the events array contains an element for each event in the competition.
            Field values are:
            id Event unique ID.
            Name Text String for event name (40 chars max).
            Segment Segment code as a number.
            Segname Text string for segment.
            Discipline Text string for event discipline.
            Category Text String for event Category.
            */
        public class IN_EventList
        {
            public class EventList_Response
            {
                public string response { get; set; }
                public string competition { get; set; }
                public List<Events> events { get; set; }
            }

            public class Events
            {
                public string id { get; set; }
                public string name { get; set; }
                public string segment { get; set; }
                public string segname { get; set; }
                public string discipline { get; set; }
                public string category { get; set; }
            }
        }

        /* The event result JSON command is:
            {
            “command” : “result”,
            “id” : “event_id”,
            “segment” : “segment_id”
            “type” : “result_type”
            }
            Where:
            id Event unique ID from event list.
            Segment Event segment code from event list.
            Type Result type “0” = final result, “1” = segment result.
            The response is a list of the order of skaters for the event
            {
            “response” : “event_result”,
            “id” : “event_id”,
            “segment” : “segment_id”
            “type” : “result_type”
            “skaters”:[
            {“rank”:”skater_rank”, ”name”:”skater_name”, ”country”:”skater_country”},
            {“rank”:”skater_rank”, ”name”:”skater_name”, ”country”:”skater_country”},
            {“rank”:”skater_rank”, ”name”:”skater_name”, ”country”:”skater_country”}
            ]
            }
            Field values are:
            id Event unique ID.
            Segment Event segment code.
            Type Result type “0” = final result, “1” = segment result.
            Rank Skater placing as number string.
            Name Text String of Skater Name (40 chars max).
            Country 3 Character ISO country code string.
         * */
        public class IN_EventResult
        {
            public class EventResult_Response
            {
                public string response { get; set; }
                public string id { get; set; }
                public string segment { get; set; }
                public string type { get; set; }
                public string heading { get; set; }
                public string footer { get; set; }
                public List<SkaterRank> skaters { get; set; }
            }

            public class SkaterRank
            {
                public string rank { get; set; }
                public string name { get; set; }
                public string country { get; set; }
                public string score { get; set; }
            }
        }

        public class IN_Command
        {
            public string command { get; set; }
            // Order_Request, Info_Request
            public string id { get; set; }
            public string segment { get; set; }
            // Info_Request
            public string order { get; set; }
            // Result_Request
            public string type { get; set; }
        }

    }

    public class TcpClientWithTimeout
    {
        protected string _hostname;
        protected int _port;
        protected int _timeout_milliseconds;
        protected TcpClient connection;
        protected bool connected;
        protected Exception exception;

        public TcpClientWithTimeout(string hostname, int port, int timeout_milliseconds)
        {
            _hostname = hostname;
            _port = port;
            _timeout_milliseconds = timeout_milliseconds;
        }
        public TcpClient Connect()
        {
            // kick off the thread that tries to connect
            connected = false;
            exception = null;
            Thread thread = new Thread(new ThreadStart(BeginConnect));
            thread.IsBackground = true; // So that a failed connection attempt 
                                        // wont prevent the process from terminating while it does the long timeout
            thread.Start();

            // wait for either the timeout or the thread to finish
            thread.Join(_timeout_milliseconds);

            if (connected == true)
            {
                // it succeeded, so return the connection
                thread.Abort();
                return connection;
            }
            if (exception != null)
            {
                // it crashed, so return the exception to the caller
                thread.Abort();
                throw exception;
            }
            else
            {
                // if it gets here, it timed out, so abort the thread and throw an exception
                thread.Abort();
                string message = string.Format("TcpClient connection to {0}:{1} timed out",
                  _hostname, _port);
                throw new TimeoutException(message);
            }
        }
        protected void BeginConnect()
        {
            try
            {
                connection = new TcpClient(_hostname, _port);
                // record that it succeeded, for the main thread to return to the caller
                connected = true;
            }
            catch (Exception ex)
            {
                // record the exception for the main thread to re-throw back to the calling code
                exception = ex;
            }
        }
    }

    #endregion
}

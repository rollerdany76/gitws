﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;

namespace RollartSystemTech
{
  class FileHash
  {
    public FileHash()
    {
        // Constructor
    }

    ~FileHash()
    {
      // Destructor
    }

    private string GetChecksum(string file, String PrivateKey)
    {
      if (File.Exists(file))
      {
        String FileText = File.ReadAllText(file);
        FileText = FileText + PrivateKey;
        var md5 = MD5.Create();
        byte[] checksum = md5.ComputeHash(Encoding.UTF8.GetBytes(FileText));
        return BitConverter.ToString(checksum).Replace("-", String.Empty);
      }
      else
      {
        return "";
      }
    }

    public bool GenerateFileHash(string file, String PrivateKey)
    {
      if (File.Exists(file))
      {
        String HashFile = Path.ChangeExtension(file, ".md5");
        if (File.Exists(HashFile))
        {
          File.Delete(HashFile);
        }
        using (StreamWriter sw = File.CreateText(HashFile))
        {
          sw.WriteLine(GetChecksum(file, PrivateKey));
        }
        return true;
      }
      else
      {
        return false;
      }
    }

    public bool VerifyFileHash(string file, String PrivateKey)
    {
      String HashFile = Path.ChangeExtension(file, ".md5");
      if (File.Exists(file) && File.Exists(HashFile))
      {
        String FileHash = GetChecksum(file, PrivateKey);
        String StoredHash = "";
        using (StreamReader sr = File.OpenText(HashFile))
        {
          StoredHash = sr.ReadLine();
        }
        if (FileHash == StoredHash)
        {
          return true;
        }
      }
      return false;
    }
  }
}

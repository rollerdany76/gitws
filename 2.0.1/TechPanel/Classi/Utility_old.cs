﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Xml.Linq;
using System.Data.SQLite;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using System.Data;
using System.Threading;
using System.Net.Sockets;
using System.Drawing.Imaging;
using System.Collections;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Net.NetworkInformation;
using Newtonsoft.Json;
using System.Globalization;

namespace RollartSystemTech
{
    class Utility
    {
        SQLiteCommand cmdSelect = null;
        SQLiteDataReader dr = null;
        IEnumerable<XAttribute> attributes = null;
        XElement xmlElements = null;
        Waiting w = null;

        Color rigaSoloJump = Color.Aqua;
        Color rigaCombo = Color.GreenYellow;
        Color rigaSpin = Color.Gold;
        Color rigaComboSpin = Color.LightSalmon;
        Color rigaSteps = Color.White;
        Color rigaDed = Color.Red;

        //ScoreToScreen sts = null;
        //ScoreToScreen2 sts2 = null;
        public static DisplayScore ds = null;
        CheckQoe qoeForm = null;
        private volatile bool _shouldStop;
        public static Main main = null;
        public static TextBox log = null;

        #region Variabili per la gestione del VideoScreen
        VideoDisplay vd = null;
        public static Video.Blank blank = null;
        public static Video.Skater skInfo = null;
        public static Video.ProgressiveTotal skProg = null;
        public static List<Video.Result_Single> skResultSingle = null;
        public static List<Video.Result_Combined> skResultComb = null;
        public static Video.Clock clockVideo = null;
        public static Video.Timer timerVideo = null;
        public static string stringToVideo = "";
        public static Video.IN_EventList.EventList_Response events4video = null;
        public static Video.IN_EventOrder.EventOrder_Response order4video = null;
        public static Video.IN_Skater.Info_Response skaterinfo4video = null;
        public static Video.IN_EventResult.EventResult_Response result4Video = null;
        #endregion

        /***************************************************************
         * QUERY
         * *************************************************************/
        //string qu1 = "SELECT * FROM Elements where id_segment = ";
        string qu2 = "SELECT * FROM SegmentParams where id_segment = @idSegment " +
                                                " and id_specialita = @idSpecialita" +
                                                " and id_category =   @idCategoria";

        public Utility(Main mainForm)
        {
            if (mainForm != null)
            {
                main = mainForm;
                log = (TextBox)main.Controls.Find("log", true)[0];
                vd = new VideoDisplay(main);
            }
            
        }

        public static void WriteLog(string text, string messageType)
        {

            try
            {
                using (StreamWriter w = File.AppendText("RST_Log.txt"))
                {
                    w.Write(DateTime.Now.ToString("dd/MM/yy HH:mm:ss") + " - ");
                    if (messageType.Equals("ERROR")) w.Write("***ERROR*** --> " + text + "\r\n");
                    else w.Write(text + "\r\n");
                }
                if (log != null)
                {
                    log.Text += text + "\r\n";
                    log.Refresh();
                    log.SelectionStart = log.Text.Length;
                    log.ScrollToCaret();
                }
            }
            catch (Exception)
            { }
        }

        #region Average, Confirm, Complete segment

        // AVERAGE
        public void AskAverage()
        {
            //int tipoFunz = 0;
            Button average = ((Button)Definizioni.currentForm.Controls.Find("average", true)[0]);
            Button ltimer = ((Button)Definizioni.currentForm.Controls.Find("ltimer", true)[0]);
            TextBox log = ((TextBox)Definizioni.currentForm.Controls.Find("log", true)[0]);
            Definizioni.exitFromPing = true;

            Utility.SendBroadcast("<AVERAGE/>");
            //Definizioni.stopFromWaiting = false;
            Definizioni.exitFromWaiting = false;
            _shouldStop = true;
            DisableButtons();

            ((Label)Definizioni.currentForm.Controls.Find("error", true)[0]).Text = "";

            for (int i = 0; i < Definizioni.numJudges; i++)
            //for (int i = 0; i < Definizioni.connectedJudges.Length; i++)
            {
                ((Button)Definizioni.currentForm.Controls.Find("j" + (i + 1), true)[0]).ForeColor = Color.Red;
                ((Button)Definizioni.currentForm.Controls.Find("j" + (i + 1), true)[0]).FlatAppearance.BorderColor = Color.Red;
                ((Button)Definizioni.currentForm.Controls.Find("j" + (i + 1), true)[0]).BackColor = Color.Yellow;
            }
            RicevoDati(log);

            // Attendo i components dai giudici 
            w = new Waiting();
            Thread attesaThread = new Thread(WaitForm);
            Thread workerThread = new Thread(WaitGiudici);
            attesaThread.Start();
            workerThread.Start();
            workerThread.Join();
            EnableButtons();

            if (Definizioni.exitFromWaiting)
            {
                return;
            }

            // gestione inserimento manuale dei punteggi dei giudici che si sono disconnessi
            //if (Definizioni.exitFromWaiting)
            //{
                //tipoFunz = 1;
            //}

            Average av = new Average();
            av.ShowDialog();
            Definizioni.currentForm.Refresh();
            Definizioni.exitFromPing = false;
            average.Enabled = false;

        }

        // CONFIRM
        public bool ConfirmSegment()
        {
            Label error = ((Label)Definizioni.currentForm.Controls.Find("error", true)[0]);
            Button ltimer = ((Button)Definizioni.currentForm.Controls.Find("ltimer", true)[0]);
            TextBox log = ((TextBox)Definizioni.currentForm.Controls.Find("log", true)[0]);

            _shouldStop = true;
            Utility.SendBroadcast("<CONFIRM/>");
            
            for (int i = 0; i < Definizioni.numJudges; i++)
            {
                ((Button)Definizioni.currentForm.Controls.Find("j" + (i + 1), true)[0]).ForeColor = Color.Red;
                ((Button)Definizioni.currentForm.Controls.Find("j" + (i + 1), true)[0]).FlatAppearance.BorderColor = Color.Red;
                ((Button)Definizioni.currentForm.Controls.Find("j" + (i + 1), true)[0]).BackColor = Color.Yellow;
            }
            error.Text = "";
            RicevoDati(log);

            // Attendo punteggi dai giudici
            w = new Waiting();
            Thread attesaThread = new Thread(WaitForm);
            Thread workerThread = new Thread(WaitGiudici);

            attesaThread.Start();
            workerThread.Start();
            workerThread.Join();

            // gestione exit forzato
            if (Definizioni.exitFromWaiting)
            {
                return false;               
            }

            for (int i = 0; i < Definizioni.qoeRicevuti.Length; i++)
            {
                if (Definizioni.qoeRicevuti[i] == null)
                {
                    RecoverQoe rqoe = new RecoverQoe();
                    rqoe.ShowDialog();
                    break;
                }
            }

            ltimer.Text = "00:00";

            Definizioni.currentForm.Refresh();
            error.Text = Definizioni.resources["panel7"].ToString();
            error.Refresh();

            ParseElementsAdnSaveinDB();

            Definizioni.currentForm.Refresh();
            CompleteSegment(error);
            return true;
        }

        // Complete Segment
        public void CompleteSegment(Label error)
        {
            error.Visible = true;
            Thread.Sleep(100);
            // calcolo il tecnico finale
            error.Text = Definizioni.resources["panel3"].ToString(); error.Refresh();
            CalcoloTecnicoFinale();

            // components
            error.Text = Definizioni.resources["panel4"].ToString(); error.Refresh();
            CalcoloComponents();

            // deductions [dalla 2.0.0.10 -modifica del 19/01/2019]
            InseriscoDeductions();

            // Totale
            error.Text = Definizioni.resources["panel5"].ToString(); error.Refresh();
            CalcoloTotale();

            // classifica del segmento
            error.Text = Definizioni.resources["panel6"].ToString(); error.Refresh();
            int classProvv = CalcoloClassificaSegmento();

            // classifica parziale 
            CalcoloClassificaGeneraleParziale();
            error.Text = "";

            // Visualizzo QOE ricevuti dai giudici
            ListView lv = ((ListView)Definizioni.currentForm.Controls.Find("lv", true)[0]);
            Button buttonNext = ((Button)Definizioni.currentForm.Controls.Find("buttonNext", true)[0]);

            Thread.Sleep(1000);
            qoeForm = new CheckQoe(lv, buttonNext.Text);
            qoeForm.BringToFront();
            qoeForm.ShowDialog();

            DisplayScore(false);

            // aggiorno il campo LastPartecipant in Gara
            UpdateLastPartecipant();
        }

        public void DisplayScore(bool previousSkater)
        {
            try
            {
                // Video Screen Connected
                if (Definizioni.stateVideo == 2)
                {
                    Video_ResultCommand();
                    NextSkater ns = new NextSkater();
                    ns.ShowDialog();
                } 
                else
                {
                    // 2.0.1
                    if (previousSkater && Definizioni.staticScreen && Definizioni.displaySwitch)
                    {
                        if (Definizioni.screenType == "1")
                        {
                            ((Panel)(Utility.ds).Controls.Find("panel1", true)[0]).Visible = true;
                            ((Panel)(Utility.ds).Controls.Find("panel1", true)[0]).BringToFront();
                        }
                        else
                        {
                            ((Panel)(Utility.ds).Controls.Find("panel2", true)[0]).Visible = true;
                            ((Panel)(Utility.ds).Controls.Find("panel2", true)[0]).BringToFront();
                        }
                        if (!MaximizeToSecondaryMonitor(Utility.ds))
                        {
                            Utility.PrimaryMonitor(Utility.ds);
                            Utility.ds.ShowDialog();
                        }
                        else
                        {
                            MessageBox.Show("Displaying scores...", "Rollart",
                               MessageBoxButtons.OK, MessageBoxIcon.Information);

                            ((Panel)(Utility.ds).Controls.Find("panelBlank", true)[0]).Visible = true;
                            ((Panel)(Utility.ds).Controls.Find("panelBlank", true)[0]).BringToFront();
                            ((Panel)(Utility.ds).Controls.Find("panel1", true)[0]).Visible = false;
                            ((Panel)(Utility.ds).Controls.Find("panel2", true)[0]).Visible = false;

                        }
                        

                    }
                    // visualizzo foto per schermo
                    //if (Definizioni.staticScreen)
                    //{
                        //if (Definizioni.screenType.Equals("1")) sts = new ScoreToScreen();
                        //else sts2 = new ScoreToScreen2();
                        //if (Definizioni.displaySwitch)
                        //{
                        //    Screen[] screens = Screen.AllScreens;
                        //    if (screens.Length > 1)
                        //    {
                        //        Rectangle bounds = screens[1].Bounds;
                        //        if (Definizioni.screenType.Equals("1"))
                        //        {
                        //            sts.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                        //            sts.StartPosition = FormStartPosition.Manual;
                        //        }
                        //        else
                        //        {
                        //            sts2.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                        //            sts2.StartPosition = FormStartPosition.Manual;
                        //        }
                        //    }
                        //}

                        //if (Definizioni.screenType.Equals("1")) sts.ShowDialog();
                        //else sts2.ShowDialog();
                        //if (Definizioni.screenType.Equals("1")) sts.Show();
                        //else sts2.Show();
                    //}
                }
            }
            catch (Exception)
            { }

        }

        public void MoveWindowToProjector()
        {
            Rectangle rectMonitor;

            //Get All the screens associated with this Monitor
            Screen[] screens = Screen.AllScreens;

            // Get Monitor Count
            int iMonitorCount = Screen.AllScreens.Length;

            Thread.Sleep(1000);
            //if (IsProjectorMode)
            if (iMonitorCount > 1) // If monitor Count 2 or more
            {
                //Get the Dimension of the monitor
                rectMonitor = Screen.AllScreens[1].WorkingArea;
            }
            else
            {
                //Get the Dimension of the monitor
                rectMonitor = Screen.AllScreens[0].WorkingArea;
            }
        }

        // 2.0.1
        public static bool MaximizeToSecondaryMonitor(Form frm)
        {
            var secondaryScreen = System.Windows.Forms.Screen.AllScreens.Where(s => !s.Primary).FirstOrDefault();
            if (secondaryScreen != null)
            {
                var workingArea = secondaryScreen.WorkingArea;
                frm.Left = workingArea.Left;
                frm.Top = workingArea.Top;
                frm.Width = workingArea.Width;
                frm.Height = workingArea.Height;
                frm.Location = secondaryScreen.WorkingArea.Location;
                frm.WindowState = FormWindowState.Maximized;
                return true;
            }
            return false;
        }

        public static void PrimaryMonitor(Form frm)
        {
            var primaryScreen = System.Windows.Forms.Screen.AllScreens.Where(s => s.Primary).FirstOrDefault();

            var workingArea = primaryScreen.WorkingArea;
            frm.Left = workingArea.Left;
            frm.Top = workingArea.Top;
            frm.Width = workingArea.Width;
            frm.Height = workingArea.Height;
            frm.Location = primaryScreen.WorkingArea.Location;
            //frm.WindowState = FormWindowState.Maximized;

        }

        public void EnableButtons()
        {
            ((Button)Definizioni.currentForm.Controls.Find("confirm", true)[0]).Enabled = true;
            ((Button)Definizioni.currentForm.Controls.Find("back", true)[0]).Enabled = true;
            //((Button)Definizioni.currentForm.Controls.Find("edit", true)[0]).Enabled = true;
            ((Button)Definizioni.currentForm.Controls.Find("startstop", true)[0]).Enabled = true;
            ((Button)Definizioni.currentForm.Controls.Find("average", true)[0]).Enabled = true;
        }

        public void DisableButtons()
        {
            ((Button)Definizioni.currentForm.Controls.Find("confirm", true)[0]).Enabled = false;
            ((Button)Definizioni.currentForm.Controls.Find("back", true)[0]).Enabled = false;
            //((Button)Definizioni.currentForm.Controls.Find("edit", true)[0]).Enabled = false;
            ((Button)Definizioni.currentForm.Controls.Find("startstop", true)[0]).Enabled = false;
            ((Button)Definizioni.currentForm.Controls.Find("average", true)[0]).Enabled = false;
        }

        #endregion

        #region database
        /****************************************************************
         *  FUNZIONI DEL DB
         * *************************************************************/
        public void LoadValuesFromDBForAll(System.Windows.Forms.Control.ControlCollection ctrl, TextBox log, ToolTip tt, string spec)
        {
            try
            {
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    // elements
                    DataTable dt = new DataTable();
                    cmd.CommandText = "SELECT * FROM Elements";
                    dt.Load(cmd.ExecuteReader());

                    int id = 0;
                    foreach (DataRow r in dt.Rows)
                    {
                        id = int.Parse(r[0].ToString());
                        if (ctrl.Find("el" + id, true).Length > 0)
                        {
                            ctrl.Find("el" + id, true)[0].Tag = r;
                            tt.SetToolTip(ctrl.Find("el" + id, true)[0], r[1].ToString() + ";" +
                                r[3].ToString());
                        
                        }
                    }

                    // percentuali per le trottole
                    foreach (string percentuale in Definizioni.percentualiSpins)
                    {
                        for (int i = 1; i < 400; i++)
                        {
                            if (ctrl.Find("cb" + i, true).Length > 0)
                                ((ComboBox)ctrl.Find("cb" + i, true)[0]).Items.Add(percentuale + " %");
                        }
                    }

                    // parametri del segmento
                    DataTable dt2 = new DataTable();
                    cmd.CommandText = qu2.Replace("SegmentParams", spec)
                                         .Replace("@idSegment", "" + Definizioni.idSegment)
                                         .Replace("@idSpecialita", "" + Definizioni.idSpecialita)
                                         .Replace("@idCategoria", "" + Definizioni.idCategoria);
                    SQLiteDataReader sqdr = cmd.ExecuteReader();
                    dt2.Load(sqdr);
                    foreach (DataRow r in dt2.Rows)
                    {
                        if (spec == "SegmentParams")
                        {
                            //for (int i = 0; i < 17; i++)
                            for (int i = 0; i < 22; i++) // 1.0.4 inseriti 5 campi in più
                            {
                                Definizioni.paramSegment[i] = decimal.Parse(r[i + 3].ToString());
                            }
                            Definizioni.factor = decimal.Parse(r[14].ToString());
                        }
                        else if (spec == "SegmentParamsPairs")
                        {
                            //for (int i = 0; i < 22; i++)
                            for (int i = 0; i < 23; i++) // 1.0.4 inserito 1 campo in più
                                Definizioni.paramSegment[i] = decimal.Parse(r[i + 4].ToString());
                            Definizioni.factor = decimal.Parse(r[5].ToString());
                        }
                        else if (spec == "SegmentParamsDance")
                        {
                            for (int i = 0; i < 13; i++)
                                Definizioni.paramSegment[i] = decimal.Parse(r[i + 4].ToString());
                            Definizioni.factor = decimal.Parse(r[5].ToString());
                        }
                        else if (spec == "SegmentParamsPrecision")
                        {
                            //for (int i = 0; i < 12; i++)
                            for (int i = 0; i < 14; i++) // 2.0 inseriti due campi in più
                                Definizioni.paramSegment[i] = decimal.Parse(r[i + 4].ToString());
                            Definizioni.factor = decimal.Parse(r[5].ToString());
                        }

                        // Salvo il Factor
                        using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                        {
                            command.CommandText =" UPDATE GaraParams SET Factor = '" + Definizioni.factor.ToString().Replace(',','.') +
                                "' WHERE ID_GaraParams = " + Definizioni.idGaraParams + 
                                " AND ID_Segment = " + Definizioni.idSegment;
                            command.ExecuteNonQuery();
                        }
                    }
                }

            }
            catch (SQLiteException ex)
            {
                WriteLog("***LoadValuesFromDB: " + ex.Message, "ERROR");
            }
        }

        ///<summary> Leggo la password di Login per il data operator </summary>
        /// 0=password ok, 1=no righe, 2=password errata, 3=database error
        public static int CheckPassword(string username, string password)
        {
            try
            {
                string passwordDB = "";
                string sql = "SELECT Password FROM AccessControl WHERE Username = '" + username + "'";
                // recupero password
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = sql;
                    SQLiteDataReader dr = command.ExecuteReader();
                    if (!dr.HasRows) return 1;
                    while (dr.Read())
                    {
                        passwordDB = dr[0].ToString(); // password
                        if (!passwordDB.Equals(password)) return 2;
                    }
                    
                }
                return 0;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex.Message, "ERROR");
                return 3;
            }
        }

        public static void ScrivoElementoOnDB(ListView lv, TextBox log, bool delete)
        {
            try
            {
                int numLVElem = lv.Items.Count;
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    if (delete) // elimino record perchè ho cliccato su "back"
                    {
                        command.CommandText = "DELETE FROM Gara WHERE ID_GaraParams = @param1 AND " +
                            " NumPartecipante = @param2 AND ProgressivoEl = @param3 AND" +
                            " ID_Segment = @param4";
                        command.Parameters.AddWithValue("@param1", Definizioni.idGaraParams);
                        command.Parameters.AddWithValue("@param2", Definizioni.currentPart);
                        command.Parameters.AddWithValue("@param3", "" + (numLVElem + 1));
                        command.Parameters.AddWithValue("@param4", Definizioni.idSegment);
                    }
                    else if (Definizioni.updating) // aggiorno record perchè ho cliccato su "edit"
                    {
                        numLVElem = lv.SelectedItems[0].Index;
                        //command.CommandText = "UPDATE Gara SET Element=@param5, Value=@param6, pen=@param7  WHERE ID_GaraParams = @param1 " + 
                        //    "AND ID_Segment = @param2 AND NumPartecipante = @param3 AND ProgressivoEl = @param4";
                        // *** Modifica del 10/07/2018 - 1.0.4 Inserite due nuove colonne Bonus e ProgrCombo
                        command.CommandText = "UPDATE Gara SET Element=@param5, Value=@param6, pen=@param7, Bonus=@param8 " +
                            "WHERE ID_GaraParams = @param1 AND ID_Segment = @param2 AND NumPartecipante = @param3 AND ProgressivoEl = @param4";
                        command.Parameters.AddWithValue("@param1", Definizioni.idGaraParams);
                        command.Parameters.AddWithValue("@param2", Definizioni.idSegment);
                        command.Parameters.AddWithValue("@param3", Definizioni.currentPart);
                        command.Parameters.AddWithValue("@param4", "" + (numLVElem + 1));

                        command.Parameters.AddWithValue("@param5", lv.Items[numLVElem].SubItems[1].Text.Replace("<", "").TrimEnd()); // nome el
                        command.Parameters.AddWithValue("@param6", lv.Items[numLVElem].SubItems[2].Text.Replace(',', '.')); // value
                        if (lv.Items[numLVElem].SubItems[6].Text.Equals("D"))
                            command.Parameters.AddWithValue("@param7", "<<<"); // pen
                        else if (lv.Items[numLVElem].SubItems[6].Text.Equals("U"))
                            command.Parameters.AddWithValue("@param7", "<"); // pen
                        else if (lv.Items[numLVElem].SubItems[6].Text.Equals("H"))
                            command.Parameters.AddWithValue("@param7", "<<"); // pen
                        else command.Parameters.AddWithValue("@param7", " "); // pen

                        //if (lv.Items[numLVElem].SubItems[7].Text.Contains("*"))
                        //    command.Parameters["@param7"].Value = command.Parameters["@param7"].Value + "*";

                        // Bonus
                        command.Parameters.AddWithValue("@param8", "");
                        if (!lv.Items[numLVElem].SubItems[8].Text.Equals("")) // time
                            command.Parameters["@param8"].Value = command.Parameters["@param8"].Value + "T";
                        if (lv.Items[numLVElem].SubItems[7].Text.Contains("%")) // percentuale
                            command.Parameters["@param8"].Value = command.Parameters["@param8"].Value + "%";
                        if (lv.Items[numLVElem].SubItems[7].Text.Contains("B")) // percentuale
                            command.Parameters["@param8"].Value = command.Parameters["@param8"].Value + "+";
                        if (lv.Items[numLVElem].SubItems[7].Text.Contains("*"))
                            command.Parameters["@param8"].Value = command.Parameters["@param8"].Value + "*";
                    }
                    else
                    {
                        //command.CommandText = "INSERT INTO Gara(ID_GaraParams,ID_Segment, NumPartecipante,ProgressivoEl,Element,Value,NumCombo,Pen)" +
                        //    " VALUES(@param1,@param8,@param2,@param3,@param4,@param5,@param6,@param7)";
                        // *** Modifica del 10/07/2018 - 1.0.4 Inserite due nuove colonne Bonus e ProgrCombo
                        command.CommandText = "INSERT INTO Gara(ID_GaraParams,ID_Segment, NumPartecipante,ProgressivoEl,Element,Value,NumCombo,Pen,Bonus,ProgrCombo)" +
                            " VALUES(@param1,@param2,@param3,@param4,@param5,@param6,@param7,@param8,@param9,@param10)";

                        command.Parameters.AddWithValue("@param1", Definizioni.idGaraParams);
                        command.Parameters.AddWithValue("@param2", Definizioni.idSegment);
                        command.Parameters.AddWithValue("@param3", Definizioni.currentPart);
                        command.Parameters.AddWithValue("@param4", "" + numLVElem);
                        command.Parameters.AddWithValue("@param5", lv.Items[numLVElem - 1].SubItems[1].Text.Replace("<", "").TrimEnd()); // nome el
                        command.Parameters.AddWithValue("@param6", lv.Items[numLVElem - 1].SubItems[2].Text.Replace(',', '.')); // value
                        command.Parameters.AddWithValue("@param7", "" + lv.Items[numLVElem - 1].SubItems[5].Text); // num combo
                        if (lv.Items[numLVElem - 1].SubItems[6].Text.Equals("D"))
                            command.Parameters.AddWithValue("@param8", "<<<"); // pen
                        else if (lv.Items[numLVElem - 1].SubItems[6].Text.Equals("U"))
                            command.Parameters.AddWithValue("@param8", "<"); // pen
                        else if (lv.Items[numLVElem - 1].SubItems[6].Text.Equals("H"))
                            command.Parameters.AddWithValue("@param8", "<<"); // pen
                        else command.Parameters.AddWithValue("@param8", " "); // pen

                        // Bonus
                        command.Parameters.AddWithValue("@param9", "");
                        if (!lv.Items[numLVElem - 1].SubItems[8].Text.Equals("")) // time
                            command.Parameters["@param9"].Value = command.Parameters["@param9"].Value + "T";
                        if (lv.Items[numLVElem - 1].SubItems[7].Text.Contains("%")) // percentuale
                            command.Parameters["@param9"].Value = command.Parameters["@param9"].Value + "%";
                        if (lv.Items[numLVElem - 1].SubItems[7].Text.Contains("B")) // percentuale
                            command.Parameters["@param9"].Value = command.Parameters["@param9"].Value + "+";
                        if (lv.Items[numLVElem - 1].SubItems[7].Text.Contains("*"))
                            command.Parameters["@param9"].Value = command.Parameters["@param9"].Value + "*";

                        // gestione progressivo combo (sia per trottole che per salti)
                        command.Parameters.AddWithValue("@param10", "");
                        if (!lv.Items[numLVElem - 1].SubItems[5].Text.Equals("0")) // se si tratta di combo
                        {
                            command.Parameters["@param10"].Value = Definizioni.numComboElement;
                        }
                        else command.Parameters["@param10"].Value = "";
                    }
                    //command.CommandText
                    command.ExecuteNonQuery();
                }
            }
            catch (SQLiteException ex)
            {
                WriteLog("***ScrivoElementoOnDB: " + ex.Message, "ERROR");
            }
        }

        public void RollbackCompetitor(TextBox log)
        {
            try
            {
                // elimino tutti gli elements relativi al competitor da 'Gara'
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "DELETE FROM Gara WHERE ID_GaraParams = @param1 AND " +
                        " ID_Segment = @param2 AND NumPartecipante = @param3";
                    command.Parameters.AddWithValue("@param1", Definizioni.idGaraParams);
                    command.Parameters.AddWithValue("@param2", Definizioni.idSegment);
                    command.Parameters.AddWithValue("@param3", Definizioni.currentPart);
                    command.ExecuteNonQuery();
                    WriteLog("***RollbackCompetitor1: " + command.CommandText, "QUERY");
                }
                // elimino tutti il record relativo al competitor da GaraFinal
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "DELETE FROM GaraFinal WHERE ID_GaraParams = @param1 AND " +
                        " ID_Segment = @param2 AND NumPartecipante = @param3";
                    command.Parameters.AddWithValue("@param1", Definizioni.idGaraParams);
                    command.Parameters.AddWithValue("@param2", Definizioni.idSegment);
                    command.Parameters.AddWithValue("@param3", Definizioni.currentPart);
                    command.ExecuteNonQuery();
                    WriteLog("***RollbackCompetitor2: " + command.CommandText, "QUERY");
                }
                Definizioni.currentPart = Definizioni.currentPart - 1;
                // aggiorno il lastpartecipant da GaraParams
                //UpdateLastPartecipant();
            }
            catch (SQLiteException ex)
            {
                WriteLog("***RollbackCompetitor: " + ex.Message, "ERROR");
            }
        }
        
        public void LoadParticipant(TextBox log, Button buttonNext, Label partial)
        {
            try
            {

                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "select NumStartingList, Name, Country, B.ID_Atleta from Participants as A, Athletes as B where ID_GaraParams = " +
                                            Definizioni.idGaraParams + " and NumStartingList = " + Definizioni.currentPart +
                                          " AND A.ID_Atleta = B.ID_Atleta AND ID_Segment = " + Definizioni.idSegment;
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        buttonNext.Text = Definizioni.currentPart + "/" +
                            Definizioni.numPart + " - " + dr[1].ToString() +
                            " (" + dr[2].ToString() + ")";
                        Definizioni.idAtleta = int.Parse(dr[3].ToString());
                    }
                }
                // recupero se esiste la posizione e il totale dell'atleta nel precedente segmento di gara
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    //command.CommandText = "SELECT TotGara,Position FROM GaraTotal WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                    //                      " AND ID_Atleta = " + Definizioni.idAtleta;
                    // 2.0.1 Recupero anche il codice del segmento
                    command.CommandText = "SELECT Total,Position,Code FROM GaraFinal, Segments " +
                                          "WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                                          " AND ID_Segments = ID_Segment" +
                                          " AND ID_Atleta = " + Definizioni.idAtleta;
                    SQLiteDataReader dr2 = command.ExecuteReader();

                    partial.Text = "Partial RANK : -\r\n" +
                                   "Partial SCORE: -";
                    Definizioni.typeResult = "segment";

                    while (dr2.Read())
                    {
                        partial.Text = "Partial RANK : " + dr2[1].ToString() + "\r\n" +
                                       "Partial SCORE: " + dr2[0].ToString();

                        // 2.0.1 Gestione Video
                        if (Definizioni.stateVideo == 2)
                        {
                            Definizioni.typeResult = "combined";
                            skResultComb[Definizioni.currentPart - 1].prev_rank = dr2[1].ToString();
                            skResultComb[Definizioni.currentPart - 1].prev_score = FormattaQuestoBenedettoDecimale(dr2[0]);
                            skResultComb[Definizioni.currentPart - 1].prev_seg = dr2[2].ToString();
                        }
                    }
                }

                // 2.0.1 In caso di chiusura forzata del programma, per evitare di caricare gli elementi di un
                // segmento non completato, cancello tutti gli elementi della tabella Gara e GaraFinal
                // del partecipante che sta eseguendo
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "DELETE FROM Gara WHERE ID_GaraParams = @param1 AND " +
                        " NumPartecipante = @param2 AND ID_Segment = @param3";
                    command.Parameters.AddWithValue("@param1", Definizioni.idGaraParams);
                    command.Parameters.AddWithValue("@param2", Definizioni.currentPart);
                    command.Parameters.AddWithValue("@param3", Definizioni.idSegment);
                    command.ExecuteNonQuery();
                }
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "DELETE FROM GaraFinal WHERE ID_GaraParams = @param1 AND " +
                        " ID_Segment = @param2 AND NumPartecipante = @param3";
                    command.Parameters.AddWithValue("@param1", Definizioni.idGaraParams);
                    command.Parameters.AddWithValue("@param2", Definizioni.idSegment);
                    command.Parameters.AddWithValue("@param3", Definizioni.currentPart);
                    command.ExecuteNonQuery();
                }
            }
            catch (SQLiteException ex)
            {
                WriteLog("***LoadParticipant: " + ex.Message, "ERROR");
            }
        }

        public static string FormattaQuestoBenedettoDecimale(object valoreDatabase)
        {
            string valoreFormattato = "";
            double dbValue;
            try
            {
                dbValue = double.Parse(valoreDatabase.ToString());
                return dbValue.ToString("0.00", CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                valoreFormattato = valoreDatabase.ToString().Replace(",", ".");
                return valoreFormattato;
            }
        }


        private void InseriscoComponents(IEnumerable<XElement> components, 
            SQLiteCommand command, ref string log)
        {
            try
            {
                using (command)
                {
                    if (!Definizioni.compAlreadyInserted)
                    {
                        command.CommandText = "INSERT INTO Gara(ID_GaraParams,ID_Segment,NumPartecipante,Element)" +
                                            " VALUES(@param1,@param2,@param3,@element)";
                        command.Parameters.AddWithValue("@param1", Definizioni.idGaraParams);
                        command.Parameters.AddWithValue("@param2", Definizioni.idSegment);
                        command.Parameters.AddWithValue("@param3", Definizioni.currentPart);
                        for (int i = 1; i < 6; i++)
                        {
                            command.Parameters.AddWithValue("@element", "comp" + i);
                            command.ExecuteNonQuery();
                        }
                        Definizioni.compAlreadyInserted = true;
                        //judgeQoeToView += " COMPONENTS\r\n";
                    }
                    command.CommandText = "UPDATE Gara SET qoev_@numJ = @value WHERE ID_GaraParams = @param1 " +
                        " AND ID_Segment = @param2 AND NumPartecipante = @param3 AND Element = @element";
                    command.Parameters.AddWithValue("@param1", Definizioni.idGaraParams);
                    command.Parameters.AddWithValue("@param2", Definizioni.idSegment);
                    command.Parameters.AddWithValue("@param3", Definizioni.currentPart);
                    command.CommandText = command.CommandText.Replace("@numJ", "" + Definizioni.numJ);

                    foreach (XElement nodo in components)
                    {
                        {
                            // comp1
                            command.Parameters.AddWithValue("@element", nodo.Attribute("comp1").Name);
                            command.Parameters.AddWithValue("@value", nodo.Attribute("comp1").Value.Replace(',', '.'));
                            command.ExecuteNonQuery();
                            log += "Comp = " + nodo.Attribute("comp1").Value.Replace(',', '.');
                            Definizioni.judgeQoeToView += "Skating Skill --> " + nodo.Attribute("comp1").Value.Replace(',', '.');

                            // comp2
                            command.Parameters.AddWithValue("@element", nodo.Attribute("comp2").Name);
                            if (Definizioni.idSegment > 10) // tutti i segmenti tranne gli obbliga danza
                                command.Parameters.AddWithValue("@value", "0");
                            else command.Parameters.AddWithValue("@value", nodo.Attribute("comp2").Value.Replace(',', '.'));
                            command.ExecuteNonQuery();
                            log += ";" + nodo.Attribute("comp2").Value.Replace(',', '.');
                            Definizioni.judgeQoeToView += "\r\nTransitions --> " + nodo.Attribute("comp2").Value.Replace(',', '.');

                            // comp3
                            command.Parameters.AddWithValue("@element", nodo.Attribute("comp3").Name);
                            command.Parameters.AddWithValue("@value", nodo.Attribute("comp3").Value.Replace(',', '.'));
                            command.ExecuteNonQuery();
                            log += ";" + nodo.Attribute("comp3").Value.Replace(',', '.');
                            Definizioni.judgeQoeToView += "\r\nPerformance --> " + nodo.Attribute("comp3").Value.Replace(',', '.');

                            // comp4
                            command.Parameters.AddWithValue("@element", nodo.Attribute("comp4").Name);
                            if (Definizioni.idSegment > 10) // tutti i segmenti tranne gli obbliga danza
                                command.Parameters.AddWithValue("@value", "0");
                            else command.Parameters.AddWithValue("@value", nodo.Attribute("comp4").Value.Replace(',', '.'));
                            command.ExecuteNonQuery();
                            log += ";" + nodo.Attribute("comp4").Value.Replace(',', '.');
                            Definizioni.judgeQoeToView += "\r\nChoreography --> " + nodo.Attribute("comp4").Value.Replace(',', '.');

                        }
                    }
                }
            }
            catch (SQLiteException ex)
            {
                WriteLog("***InseriscoComponents: " + ex.Message, "ERROR");
            }
        }

        private void InseriscoElements(IEnumerable<XElement> elements, 
            SQLiteCommand command, ref string log)
        {
            try
            {
                // Modifica del 31-01-2018
                // ciclo sugli elementi della tabella GARA invece che su quelli arrivati dai giudici
                using (SQLiteCommand commandSelect = Definizioni.conn.CreateCommand())
                {
                    commandSelect.CommandText = "SELECT ProgressivoEL, Element, Value FROM GARA WHERE ID_GaraParams = " + Definizioni.idGaraParams + 
                                          " AND NumPartecipante = " + Definizioni.currentPart +
                                          " AND ID_Segment = " + Definizioni.idSegment;
                    SQLiteDataReader dr2 = commandSelect.ExecuteReader();
                    while (dr2.Read())
                    {
                        // progressivoEl corrente della tabella Gara
                        string idElemGara = dr2[0].ToString();
                        string codEl = dr2[1].ToString();
                        string baseValue = dr2[2].ToString();

                        //if (!baseValue.Equals("0")) // Aggiorno solo per valori non pari a Zero
                        {
                            // Verifico se esiste l'elemento tra quelli restituiti dai giudici
                            XElement resultId = elements.Descendants("ELEMENT")
                                .FirstOrDefault(el => el.Attribute("ID")?.Value == idElemGara);

                            if (resultId != null)
                            {
                                foreach (var currentEl in elements.Descendants("ELEMENT"))
                                {
                                    if (currentEl.Attribute("ID").Value.Equals(idElemGara))
                                    {
                                        Definizioni.valueqoe = currentEl.Attribute("QOE").Value;

                                        // se salto con penalità prendo il codice del salto -1
                                        if (currentEl.Attribute("PENALTY").Value.Equals("D"))
                                            //if (currentEl.Attribute("PENALTY").Value.Equals("D") ||
                                            //currentEl.Attribute("PENALTY").Value.Equals("H") ||
                                            //currentEl.Attribute("PENALTY").Value.Equals("U"))
                                        {
                                            if (currentEl.Attribute("NAME").Value.StartsWith("2"))
                                                codEl = currentEl.Attribute("NAME").Value.Replace("2", "1");
                                            else if (currentEl.Attribute("NAME").Value.StartsWith("3"))
                                                codEl = currentEl.Attribute("NAME").Value.Replace("3", "2");
                                            else if (currentEl.Attribute("NAME").Value.StartsWith("4"))
                                                codEl = currentEl.Attribute("NAME").Value.Replace("4", "3");
                                        }
                                        break;
                                    }
                                }

                            }

                            // recupero i qoe dell'elemento con il codice specificato nella tabella Gara
                            cmdSelect.Parameters.AddWithValue("@codeEl", codEl.TrimEnd());
                            cmdSelect.CommandText = Definizioni.querygoe;
                            dr = cmdSelect.ExecuteReader();

                            while (dr.Read())
                            {
                                if (Definizioni.valueqoe.Equals("+3")) Definizioni.valuegoe = dr[0].ToString();
                                else if (Definizioni.valueqoe.Equals("+2")) Definizioni.valuegoe = dr[1].ToString();
                                else if (Definizioni.valueqoe.Equals("+1")) Definizioni.valuegoe = dr[2].ToString();
                                else if (Definizioni.valueqoe.Equals("0")) Definizioni.valuegoe = "0,00";
                                else if (Definizioni.valueqoe.Equals("-1")) Definizioni.valuegoe = dr[4].ToString();
                                else if (Definizioni.valueqoe.Equals("-2")) Definizioni.valuegoe = dr[5].ToString();
                                else if (Definizioni.valueqoe.Equals("-3")) Definizioni.valuegoe = dr[6].ToString();
                            }
                            dr.Close();

                            Definizioni.judgeQoeToView += codEl + " --> " + Definizioni.valueqoe + "\r\n";
                            if (!codEl.StartsWith("comp"))
                                log += codEl + " = " + Definizioni.valueqoe + ";";

                            Definizioni.querystr = Definizioni.query;
                            Definizioni.querystr = Definizioni.querystr.Replace("@qoe", "'" + Definizioni.valueqoe.Replace(',', '.') + "'");
                            Definizioni.querystr = Definizioni.querystr.Replace("@goev", "'" + Definizioni.valuegoe.Replace(',', '.') + "'");
                            Definizioni.querystr += " and ProgressivoEl = '" + idElemGara + "'";

                            command.CommandText = Definizioni.querystr;
                            command.ExecuteNonQuery();

                            Thread.Sleep(20);
                        }
                    }
                }
            }
            catch (SQLiteException ex)
            {
                WriteLog("***InseriscoElements: " + ex.Message, "ERROR");
            }
        }

        // 2.0.0.10 - 19/01/2019 - Inserisco tutte le deductions in Gara
        private void InseriscoDeductions()
        {
            try
            {
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "INSERT INTO Gara(ID_GaraParams,ID_Segment,NumPartecipante,Element,ValueFinal)" +
                                            " VALUES(@param1,@param2,@param3,@param4,@param5)";
                    command.Parameters.AddWithValue("@param1", Definizioni.idGaraParams);
                    command.Parameters.AddWithValue("@param2", Definizioni.idSegment);
                    command.Parameters.AddWithValue("@param3", Definizioni.currentPart);
                    for (int i = 0; i < 6; i++)
                    {
                        if (Definizioni.arDeductions[i] > 0) // se la deduction e' maggiore di zero inserisco
                        {
                            command.Parameters.AddWithValue("@param4", "ded" + (i + 1));
                            command.Parameters.AddWithValue("@param5", Definizioni.arDeductions[i]);
                            command.ExecuteNonQuery();
                        }
                        
                    } 
                }
            }
            catch (Exception ex)
            {
                WriteLog("***InseriscoDeductions: " + ex.Message, "ERROR");
            }
        }

        public void CalcoloTecnicoFinale()
        {
            try
            {
                decimal valueElFinal = 0;
                decimal[] elems = new decimal[Definizioni.numJudges];
                SQLiteCommand commandUpdate = Definizioni.conn.CreateCommand();

                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT * FROM Gara WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                        " AND ID_Segment = " + Definizioni.idSegment +
                        " AND NumPartecipante = " + Definizioni.currentPart + 
                        " AND ProgressivoEl is not null";
                    dr = command.ExecuteReader();
                    //WriteLog("***CalcoloTecnicoFinale1: " + command.CommandText, "QUERY");
                    if (Definizioni.numJudges <= 3) // fino a 3 giudici
                    {
                        while (dr.Read())
                        {
                            valueElFinal = 0;
                            //if (!dr[3].ToString().Equals("") && !dr[5].ToString().Contains("*"))
                            if (!dr[3].ToString().Equals("") && !dr[27].ToString().Contains("*")) // *** colonna Bonus aggiunta 1.0.4
                            {
                                //valueElFinal = 0;
                                {
                                    for (int i = 0; i < Definizioni.numJudges; i++)
                                    {
                                        valueElFinal += decimal.Parse(dr[18 + i].ToString());
                                    }
                                    valueElFinal = Decimal.Round(valueElFinal / Definizioni.numJudges, 2) +
                                            decimal.Parse(dr[6].ToString()); // aggiungo il valore base 
                                }

                                // se negativo imposto a Zero
                                if (valueElFinal < 0) valueElFinal = 0;

                                commandUpdate.CommandText = ("UPDATE Gara SET ValueFinal = '" + valueElFinal.ToString().Replace(',', '.') +
                                "' WHERE ID_GaraParams = " + Definizioni.idGaraParams + 
                                " AND ID_Segment = " + Definizioni.idSegment +
                                " AND NumPartecipante = " + Definizioni.currentPart +
                                " AND ProgressivoEl = " + dr[3]).Replace(',', '.');
                                commandUpdate.ExecuteNonQuery();
                                WriteLog("***UpdateToFinalValue: " + commandUpdate.CommandText, "QUERY");
                            }
                            // sommo il valore finale dell'elemento al tech finale
                            Definizioni.techFinal += valueElFinal;
                        }
                    }
                    else // da 4 giudici in su
                    {
                        while (dr.Read())
                        {
                            valueElFinal = 0;
                            //if (!dr[3].ToString().Equals("") && !dr[5].ToString().Contains("*"))
                            if (!dr[3].ToString().Equals("") && !dr[27].ToString().Contains("*")) // *** colonna Bonus aggiunta 1.0.4
                            {

                                {
                                    for (int i = 0; i < Definizioni.numJudges; i++)
                                    {
                                         elems[i] = Decimal.Parse(dr[18 + i].ToString());
                                         valueElFinal += elems[i];
                                    }
                                    decimal min = Lowest(elems); // minimo
                                    decimal max = Highest(elems); // max
                                    valueElFinal = valueElFinal - min - max; //tolgo il minimo e il massimo
                                    valueElFinal = Decimal.Round(valueElFinal / (Definizioni.numJudges - 2), 2) +
                                            decimal.Parse(dr[6].ToString()); // aggiungo il valore base 
                                    
                                    // se negativo imposto a Zero
                                    if (valueElFinal < 0) valueElFinal = 0;

                                    commandUpdate.CommandText = "UPDATE Gara SET ValueFinal = '" + valueElFinal.ToString().Replace(',','.') +
                                "' WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                                " AND ID_Segment = " + Definizioni.idSegment + " AND NumPartecipante = " + Definizioni.currentPart +
                                " AND ProgressivoEl = " + dr[3];
                                    commandUpdate.ExecuteNonQuery();
                                    WriteLog("***UpdateToFinalValue: " + commandUpdate.CommandText, "QUERY");
                                }
                            }
                            // sommo il valore finale dell'elemento al tech finale
                            Definizioni.techFinal += valueElFinal;
                        }
                    }
                    dr.Close();
                }

                // 2.0.1 Gestione Video Display
                if (Definizioni.stateVideo == 2)
                {
                    skResultSingle[Definizioni.currentPart - 1].tech_score = FormattaQuestoBenedettoDecimale(Definizioni.techFinal);
                    skResultComb[Definizioni.currentPart - 1].tech_score = FormattaQuestoBenedettoDecimale(Definizioni.techFinal);
                }
            }
            catch (Exception ex)
            {
                WriteLog("***UpdateToFinalValue: " + ex.Message, "ERROR");
                
            }            
        }

        public void CalcoloComponents()
        {
            try
            {
                decimal valueComp = 0;
                int k = 1;
                decimal[] comps = new decimal[Definizioni.numJudges];
                SQLiteCommand commandUpdate = Definizioni.conn.CreateCommand();

                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT * FROM Gara WHERE ID_GaraParams = "
                        + Definizioni.idGaraParams + " AND ID_Segment = " + Definizioni.idSegment +
                        " AND NumPartecipante = " + Definizioni.currentPart + " AND ProgressivoEl is null";
                    dr = command.ExecuteReader();
                    //WriteLog("***CalcoloComponents1: " + command.CommandText, "QUERY");
                    if (Definizioni.numJudges <= 3) // fino a 3 giudici
                    {
                        while (dr.Read())
                        {
                            valueComp = 0;
                            {
                                for (int i = 0; i < Definizioni.numJudges; i++)
                                {
                                    valueComp += decimal.Parse(dr[18 + i].ToString());
                                }
                                valueComp = Decimal.Round(valueComp / Definizioni.numJudges, 2); 
                            }
                            commandUpdate.CommandText = ("UPDATE Gara SET ValueFinal = '" + valueComp +
                            "' WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                            " AND ID_Segment = " + Definizioni.idSegment + " AND NumPartecipante = " + Definizioni.currentPart + 
                            " AND Element = 'comp" + k + "'").Replace(',', '.');
                            commandUpdate.ExecuteNonQuery();
                            k++;
                            // sommo le components
                            Definizioni.artImpr += valueComp;
                            
                            WriteLog("***UpdateComponent: " + commandUpdate.CommandText,
                                "QUERY");
                        }
                        
                    }
                    else // da 4 giudici in su
                    {
                        while (dr.Read())
                        {
                            valueComp = 0;
                            {
                                for (int i = 0; i < Definizioni.numJudges; i++)
                                {
                                    comps[i] = Decimal.Parse(dr[18 + i].ToString());
                                    valueComp += comps[i];
                                }
                                decimal min = Lowest(comps); // minimo
                                decimal max = Highest(comps); // max
                                valueComp = valueComp - min - max; //tolgo il minimo e il massimo
                                valueComp = Decimal.Round(valueComp / (Definizioni.numJudges - 2), 2);

                                commandUpdate.CommandText = ("UPDATE Gara SET ValueFinal = '" + valueComp +
                            "' WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                            " AND ID_Segment = " + Definizioni.idSegment + " AND NumPartecipante = " + Definizioni.currentPart +
                            " AND Element = 'comp" + k + "'").Replace(',', '.');
                                commandUpdate.ExecuteNonQuery();
                                k++;
                                // sommo le components
                                Definizioni.artImpr += valueComp;
                                
                                WriteLog("***UpdateComponent: " + commandUpdate.CommandText,
                                "QUERY");
                            }
                        }
                    }
                    dr.Close();

                    // moltiplico per il fattore di molt
                    Definizioni.artImpr = Definizioni.artImpr * Definizioni.factor;
                    // arrotondo a due cifre decimali
                    Definizioni.artImpr = Math.Round(Definizioni.artImpr, 2);
                }

                // 2.0.1 Gestione Video Display
                if (Definizioni.stateVideo == 2)
                {
                    skResultSingle[Definizioni.currentPart - 1].pres_score = FormattaQuestoBenedettoDecimale(Definizioni.artImpr);
                    skResultSingle[Definizioni.currentPart - 1].deduction = FormattaQuestoBenedettoDecimale(Definizioni.deductionsDec);
                    skResultComb[Definizioni.currentPart - 1].pres_score = FormattaQuestoBenedettoDecimale(Definizioni.artImpr);
                    skResultComb[Definizioni.currentPart - 1].deduction = FormattaQuestoBenedettoDecimale(Definizioni.deductionsDec);
                }
            }
            catch (Exception ex)
            {
                WriteLog("***UpdateComponent: " + ex.Message, "ERROR");
            }
        }

        public decimal Highest(params decimal[] inputs)
        {
          return inputs.Max();
        }

        public decimal Lowest(params decimal[] inputs)
        {
          return inputs.Min();
        }

        public void CalcoloTotale()
        {
            try
            {
                // calcolo totale 
                Definizioni.totale = Definizioni.techFinal + Definizioni.artImpr + Definizioni.deductionsDec;

                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "INSERT INTO GaraFinal(ID_GaraParams,ID_Segment,NumPartecipante,BaseTech," +
                "FinalTech,BaseArtistic,Deductions,Total,ID_Atleta) VALUES(@param1,@param8,@param2,@param3,@param4,@param5,@param6,@param7,@param9)";
                    command.Parameters.AddWithValue("@param1", Definizioni.idGaraParams); // ID_GaraParams
                    command.Parameters.AddWithValue("@param8", Definizioni.idSegment); // ID_Segment
                    command.Parameters.AddWithValue("@param2", Definizioni.currentPart); //NumPartecipante
                    command.Parameters.AddWithValue("@param3", Definizioni.elementsDec); //BaseTech
                    command.Parameters.AddWithValue("@param4", Definizioni.techFinal); //FinalTech
                    command.Parameters.AddWithValue("@param5", Definizioni.artImpr); //Components
                    command.Parameters.AddWithValue("@param6", Definizioni.deductionsDec); //Deductions               
                    command.Parameters.AddWithValue("@param7", Definizioni.totale); //Totale
                    command.Parameters.AddWithValue("@param9", Definizioni.idAtleta); //ID_Atleta
                    command.ExecuteNonQuery();
                    WriteLog("***InsertFinalValues: " +
                        command.CommandText
                        .Replace("@param1", "" + Definizioni.idGaraParams)
                        .Replace("@param8", "" + Definizioni.idSegment)
                        .Replace("@param2", "" + Definizioni.currentPart)
                        .Replace("@param3", "" + Definizioni.elementsDec.ToString().Replace(",", "."))
                        .Replace("@param4", "" + Definizioni.techFinal.ToString().Replace(",", "."))
                        .Replace("@param5", "" + Definizioni.artImpr.ToString().Replace(",", "."))
                        .Replace("@param6", "" + Definizioni.deductionsDec.ToString().Replace(",", "."))
                        .Replace("@param7", "" + Definizioni.totale.ToString().Replace(",", "."))
                        .Replace("@param9", "" + Definizioni.idAtleta)
                        , "QUERY");
                }

                // 2.0.1 Gestione Video Display
                if (Definizioni.stateVideo == 2)
                {
                    if (skResultSingle != null)
                        skResultSingle[Definizioni.currentPart - 1].total_score = FormattaQuestoBenedettoDecimale(Definizioni.totale);
                    if (skResultComb != null)
                        skResultComb[Definizioni.currentPart - 1].seg_score = FormattaQuestoBenedettoDecimale(Definizioni.totale);
                }
            }
            catch (SQLiteException ex)
            {
                WriteLog("***InsertFinalValues: " + ex.Message, "ERROR");
            }
        }
        
        public int CalcoloClassificaSegmento()
        {
            try
            {
                int classifica = 1;
                SQLiteCommand commandUpdate = Definizioni.conn.CreateCommand();
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT NumPartecipante FROM GaraFinal WHERE ID_GaraParams = " + Definizioni.idGaraParams 
                        + " AND ID_Segment = " + Definizioni.idSegment + " ORDER BY Total DESC";

                    // gestione pari meriti - 13/03/2018
                    if (Definizioni.idSegment == 1 || Definizioni.idSegment == 4) // SP e SD
                        command.CommandText += ", FinalTech DESC"; // vince il tecnico più alto
                    else
                        command.CommandText += ", BaseArtistic DESC"; // vincono i components più alti

                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        commandUpdate.CommandText = "UPDATE GaraFinal SET Position = " +
                            "@param1 WHERE ID_GaraParams = " + Definizioni.idGaraParams + 
                            " AND ID_Segment = " + Definizioni.idSegment + " AND NumPartecipante = @param2";
                        commandUpdate.Parameters.AddWithValue("@param1", classifica); // position
                        commandUpdate.Parameters.AddWithValue("@param2", dr[0].ToString()); // num partecipante
                        commandUpdate.ExecuteNonQuery();
                        classifica++;
                    }
                    
                    dr.Close();
                }
                // classifica parziale della singola gara
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT Position FROM GaraFinal WHERE ID_GaraParams = " 
                        + Definizioni.idGaraParams + " AND ID_Segment = " + Definizioni.idSegment
                        + " AND NumPartecipante = " + Definizioni.currentPart;
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        classifica = int.Parse(dr[0].ToString());
                    }
                    dr.Close();
                    //WriteLog("Partial Segment Rank: " + classifica, "OK");
                }

                // 2.0.1 Gestione Video Display
                if (Definizioni.stateVideo == 2)
                {
                    skResultSingle[Definizioni.currentPart - 1].rank = classifica.ToString();
                    skResultComb[Definizioni.currentPart - 1].seg_rank = classifica.ToString();
                }

                return classifica;
            }
            catch (SQLiteException ex)
            {
                WriteLog("PartialRank: " + ex.Message, "ERROR");
            }
            return -1;
        }

        public void CalcoloClassificaGeneraleParziale()
        {
            try
            {
                // recupero l'ID_Atleta, Name Atleta, Ordine entrata, totale e posizione gara in corso 
                int position = 1, posSegment = 1;
                string nameAtleta = "", total = "";
                SQLiteCommand commandUpdate = Definizioni.conn.CreateCommand();

                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT A.[ID_Atleta], C.[Name], SUM(Total) as total " +
                                         " FROM GaraFinal A, Athletes C" +
                                         " WHERE A.ID_GaraParams = " + Definizioni.idGaraParams +
                                         " AND A.[ID_Atleta] = " + Definizioni.idAtleta +
                                         " AND A.[ID_Atleta] = C.[ID_Atleta] " +
                                         " GROUP BY C.[ID_Atleta]  ORDER BY total DESC, A.ID_GaraParams";

                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        Definizioni.idAtleta = int.Parse(dr[0].ToString());
                        nameAtleta = dr[1].ToString();
                        total = dr[2].ToString().ToString().Replace(",", ".");

                        // 2.0.1 Gestione Video Display
                        if (Definizioni.stateVideo == 2)
                        {
                            skResultComb[Definizioni.currentPart - 1].total_score = FormattaQuestoBenedettoDecimale(dr[2]);
                        }
                        using (SQLiteCommand command2 = Definizioni.conn.CreateCommand())
                        {
                            command2.CommandText = "SELECT ID_Atleta, TotGara FROM GaraTotal WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                                 " AND ID_Atleta = " + Definizioni.idAtleta;
                            SQLiteDataReader dr2 = command2.ExecuteReader();
                            if (!dr2.HasRows) // se in GaraTotal il record non c'è faccio INSERT
                            {
                                using (SQLiteCommand command3 = Definizioni.conn.CreateCommand())
                                {
                                    command3.CommandText = "INSERT INTO GaraTotal" +
                                    " VALUES(@param1,@param2,@param3,@param4,@param5,0,0)";
                                    command3.Parameters.AddWithValue("@param1", Definizioni.idGaraParams); // ID_GaraParams
                                    command3.Parameters.AddWithValue("@param2", Definizioni.idAtleta); // ID_Atleta
                                    command3.Parameters.AddWithValue("@param3", 0); // position
                                    command3.Parameters.AddWithValue("@param4", nameAtleta); //Atleta
                                    command3.Parameters.AddWithValue("@param5", total); // totale
                                    command3.ExecuteNonQuery();
                                }

                                // 
                            }
                            else // ...altrimenti faccio update del totale
                            {
                                commandUpdate.CommandText = "UPDATE GaraTotal SET TotGara = '" + total + "'";
                                commandUpdate.CommandText += " WHERE ID_GaraParams = " + Definizioni.idGaraParams + " AND ID_Atleta = " + Definizioni.idAtleta;
                                commandUpdate.ExecuteNonQuery();
                            }
                            
                        }
                    }
                    dr.Close();
                }

                // aggiorno posizione del segmento
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT ID_Atleta FROM GaraFinal WHERE ID_GaraParams = " 
                        + Definizioni.idGaraParams + " AND ID_Segment = " + Definizioni.idSegment 
                        + " ORDER BY Total DESC";
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        if (Definizioni.idSegment == 1 || Definizioni.idSegment == 4 || Definizioni.idSegment == 11 || Definizioni.idSegment == 12) // CD, SP e SD - PosSegment1
                        {
                            commandUpdate.CommandText = "UPDATE GaraTotal SET PosSegment1 = " + posSegment +
                            " WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                            " AND ID_Atleta = " + dr[0].ToString();
                        }
                        else if (Definizioni.idSegment == 2 || Definizioni.idSegment == 5 || Definizioni.idSegment == 6) // LP, FD, Synchro - PosSegment2
                        {
                            commandUpdate.CommandText = "UPDATE GaraTotal SET PosSegment2 = " + posSegment +
                            " WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                            " AND ID_Atleta = " + dr[0].ToString();
                        }

                        commandUpdate.ExecuteNonQuery();
                        posSegment++;
                    }

                    dr.Close();
                }

                // aggiorno posizione generale totale
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT ID_Atleta FROM GaraTotal WHERE ID_GaraParams = " + Definizioni.idGaraParams;

                    // recupero solo la classifica di chi ha eseguito fino a quel momento
                    // modifica del 25/06/2018
                    // Per Obbl danza, style dance, short program e synchro non cambio nulla
                    // Per LP, FD
                    // *** modifica del 17/09/2018 ***//
                    if (Definizioni.idSegment == 2 || Definizioni.idSegment == 5)
                        command.CommandText += " AND PosSegment2 <> 0";

                    // gestione parimeriti - 13/03/2018
                    command.CommandText += " ORDER BY TotGara DESC, PosSegment2 ASC"; // vince il LP/FD più alto

                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        commandUpdate.CommandText = "UPDATE GaraTotal SET Position = " +
                            "@param1 WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                            " AND ID_Atleta = @param2";
                        
                        if (Definizioni.idSegment == 11 || Definizioni.idSegment == 12) // per gli obbliga
                        {
                            commandUpdate.CommandText = "UPDATE GaraTotal SET Position = " +
                            "@param1, PosSegment1 = @param3 WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                            " AND ID_Atleta = @param2" ;
                            commandUpdate.Parameters.AddWithValue("@param3", position); // position
                        }

                        commandUpdate.Parameters.AddWithValue("@param1", position); // position
                        commandUpdate.Parameters.AddWithValue("@param2", dr[0].ToString()); // id_atleta
                        commandUpdate.ExecuteNonQuery();

                        // 2.0.1 Gestione Video Display
                        if (Definizioni.stateVideo == 2)
                        {
                            skResultComb[Definizioni.currentPart - 1].rank = position.ToString();
                        }
                        position++;
                    }

                    dr.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void UpdateLastPartecipant()
        {
            try
            {
                Definizioni.lastPart = Definizioni.currentPart;
                using (SQLiteCommand commandUpdate = Definizioni.conn.CreateCommand())
                {
                    commandUpdate.CommandText = "UPDATE GaraParams SET LastPartecipant = " +
                        "@param1 WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                        " AND ID_Segment = " + Definizioni.idSegment + "";
                    commandUpdate.Parameters.AddWithValue("@param1", Definizioni.currentPart); 
                    commandUpdate.ExecuteNonQuery();
                    WriteLog("***UpdateLastSkater: " + commandUpdate.CommandText
                        .Replace("@param1", "" + Definizioni.currentPart), "QUERY");
                }
            }
            catch (SQLiteException ex)
            {
                WriteLog("UpdateLastSkater: " + ex.Message, "ERROR");
            }
        }

        public static void UpdateElementConAsterisco(int id, bool addAsterisco)
        {
            try
            {
                string asterisco = "";
                if (addAsterisco) asterisco = "*";

                using (SQLiteCommand commandUpdate = Definizioni.conn.CreateCommand())
                {
                    commandUpdate.CommandText = "UPDATE Gara SET Bonus = '" + asterisco + "'" +
                        " WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                        " AND ID_Segment = " + Definizioni.idSegment +
                        " AND NumPartecipante = " + Definizioni.currentPart +
                        " AND ProgressivoEl = " + id;
                    commandUpdate.ExecuteNonQuery();
                    WriteLog("***UpdateElementConAsterisco: " + commandUpdate.CommandText, "QUERY");
                }
            }
            catch (SQLiteException ex)
            {
                WriteLog("UpdateElementConAsterisco: " + ex.Message, "ERROR");
            }
        }

        // 2.0.0.10 - 16/02/2019 BUG non aggiornava il db con l'elemento T
        public static void UpdateElementWithTimeBonus(int id, bool addT, string value)
        {
            try
            {
                string tempo = "";
                if (addT) tempo = "T";

                using (SQLiteCommand commandUpdate = Definizioni.conn.CreateCommand())
                {
                    commandUpdate.CommandText = "UPDATE Gara SET Bonus = '" + tempo + "', Value = '" +
                        value + "'" +
                        " WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                        " AND ID_Segment = " + Definizioni.idSegment +
                        " AND NumPartecipante = " + Definizioni.currentPart +
                        " AND ProgressivoEl = " + id;
                    commandUpdate.ExecuteNonQuery();
                    WriteLog("***UpdateElementWithTimeBonus: " + commandUpdate.CommandText, "QUERY");
                }
            }
            catch (SQLiteException ex)
            {
                WriteLog("UpdateElementWithTimeBonus: " + ex.Message, "ERROR");
            }
        }

        public void GetNameSocieta()
        {
            try
            {
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT Name,Societa,Country FROM Athletes ORDER BY Name ASC";
                    SQLiteDataReader dr = command.ExecuteReader();
                    if (dr.HasRows)
                    {
                        //DataRow 
                        Definizioni.nomi = new ArrayList();
                        Definizioni.societa = new ArrayList();
                        Definizioni.nazioni = new ArrayList();
                    }
                    while (dr.Read())
                    {
                        Definizioni.nomi.Add(dr[0]);
                        Definizioni.societa.Add(dr[1]);
                        Definizioni.nazioni.Add(dr[2]);
                    }
                    dr.Close();
                }
            }
            catch (SQLiteException ex)
            {
                WriteLog("GetNameSurnames: " + ex.Message, "ERROR");
            }
        }

        public void GetSkatersPerDisciplina(int idSpec)
        {
            try
            {
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT Name FROM Athletes WHERE ID_Specialita = " 
                        + idSpec + " ORDER BY Name ASC";
                    SQLiteDataReader dr = command.ExecuteReader();
                    if (dr.HasRows)
                    {
                        //DataRow 
                        Definizioni.nomi = new ArrayList();
                    }
                    while (dr.Read())
                    {
                        Definizioni.nomi.Add(dr[0]);
                    }
                    dr.Close();
                }
            }
            catch (SQLiteException ex)
            {
                WriteLog("GetNameSurnames: " + ex.Message, "ERROR");
            }
        }

        public void GetGiudici()
        {
            try
            {
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT * FROM Judges ORDER BY Name ASC";
                    SQLiteDataReader dr = command.ExecuteReader();
                    if (dr.HasRows)
                    {
                        Definizioni.giudici = new ArrayList();
                    }
                    while (dr.Read())
                    {
                        Definizioni.giudici.Add(dr[1]);
                    }
                    dr.Close();
                }
            }
            catch (SQLiteException ex)
            {
                WriteLog("GetJudges: " + ex.Message, "ERROR");
            }
        }
		
        public void ParseElementsAdnSaveinDB()
        {
            try
            {
                foreach (string element in Definizioni.qoeRicevuti)
                {
                    xmlElements = XElement.Parse(element);
                    Definizioni.query = "UPDATE Gara SET qoe_@numJ=@qoe, qoev_@numJ=@goev WHERE ID_GaraParams = ";
                    Definizioni.querygoe = "SELECT Level3,Level2,Level1,Base,LevelNeg1,LevelNeg2,LevelNeg3,ID_ElementsCat FROM Elements" +
                            " WHERE Code = @codeEl";
                    Definizioni.queryval = "SELECT Element, Pen FROM Gara WHERE ProgressivoEl >= @el and NumCombo <> 0 " +
                                           " AND (ProgressivoEl = ProgressivoEl + 1) AND ID_GaraParams = ";
                    cmdSelect = Definizioni.conn.CreateCommand();
                    Definizioni.valuegoe = "";
                    Definizioni.numJ = 0;
                    int idGaraParams = 0, idSegment = 0;
                    Definizioni.querystr = "";
                    Definizioni.artImpr = 0;
                    Definizioni.techFinal = 0;
                    Definizioni.judgeQoeToView = "";

                    if (xmlElements.Name.LocalName.Equals("SEGMENT"))
                    {
                        attributes = xmlElements.Attributes();
                        foreach (XAttribute attr in attributes)
                        {
                            if (attr.Name.LocalName.Equals("Gara"))
                            {
                                idGaraParams = int.Parse(attr.Value);
                                Definizioni.query += idGaraParams + " and ";
                                Definizioni.queryval += idGaraParams + " and ";
                            }
                            if (attr.Name.LocalName.Equals("Segment"))
                            {
                                idSegment = int.Parse(attr.Value);
                                Definizioni.query += " ID_Segment = " + idSegment + " and ";
                                Definizioni.queryval += " ID_Segment = " + idSegment + " and ";
                            }
                            if (attr.Name.LocalName.Equals("Partecipante"))
                            {
                                //numPart = int.Parse(attr.Value);
                                Definizioni.query += " NumPartecipante = " + Definizioni.currentPart;
                                Definizioni.queryval += " NumPartecipante = " + Definizioni.currentPart;

                            }
                            if (attr.Name.LocalName.Equals("numCurrentJudge"))
                                Definizioni.numJ = int.Parse(attr.Value);
                        }

                        IEnumerable<XElement> elements = xmlElements.Elements("ELEMENTS");
                        IEnumerable<XElement> components = xmlElements.Elements("COMPONENTS");

                        try
                        {
                            Definizioni.query = Definizioni.query.Replace("@numJ", "" + Definizioni.numJ);
                            Definizioni.valueqoe = "";
                            Definizioni.valueTot = "";
                            string log = "JUDGE " + Definizioni.numJ + " --> ";
                            using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                            {
                                // inserisco elements
                                InseriscoElements(elements, command, ref log);

                                // inserisco components
                                InseriscoComponents(components, command, ref log);
                            }
                            WriteLog(log, "");
                        }
                        catch (SQLiteException ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    //connectedJudges[numJ - 1] = true;
                }
            }
            catch (Exception ex)
            {
                WriteLog("ParseElementsAdnSaveinDB: " + ex.Message, "ERROR");
            }
        }

        public static bool CheckIfColumnExists(string tableName, string columnName)
        {
            using (SQLiteCommand command = Definizioni.conn.CreateCommand())
            {
                command.CommandText = string.Format("PRAGMA table_info({0})", tableName);

                var reader = command.ExecuteReader();
                int nameIndex = reader.GetOrdinal("Name");
                while (reader.Read())
                {
                    if (reader.GetString(nameIndex).Equals(columnName))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        ///<summary> carico le info dell'atleta che sta eseguendo - dalla versione 2.0.1 
        ///ed eventualmente punteggio tecnico base provvisorio più alto</summary>
        public static void LoadSkater()
        {
            try
            {
                if (Definizioni.stateVideo != 2) return;

                int idAtleta = 0;
                skInfo = new Video.Skater();
                skInfo.command = "skater";

                //skResultSingle.Add(new Video.Result_Single());
                skResultSingle[Definizioni.currentPart - 1].command = "result";
                skResultSingle[Definizioni.currentPart - 1].type = "segment";

                //skResultComb.Add(new Video.Result_Combined());
                skResultComb[Definizioni.currentPart - 1].command = "result";
                skResultComb[Definizioni.currentPart - 1].type = "combined";
                skResultComb[Definizioni.currentPart - 1].comb_code = Properties.Settings.Default.CombinedSegment;

                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT B.Name, B.Country, B.ID_Atleta, S.Code " +
                                          "FROM Participants as A, Athletes as B, Segments as S " +
                                          "WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                                          "  AND A.NumStartingList = " + Definizioni.currentPart +
                                          "  AND A.ID_Atleta = B.ID_Atleta " +
                                          "  AND A.ID_Segment = " + Definizioni.idSegment +
                                          "  AND A.ID_Segment = S.ID_Segments";
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        // Skater
                        skInfo.skater = dr[0].ToString();
                        skInfo.order = Definizioni.currentPart.ToString();
                        skInfo.country = dr[1].ToString();
                        skInfo.segment = dr[3].ToString();
                        
                        idAtleta = int.Parse(dr[2].ToString());

                        // Results
                        skResultSingle[Definizioni.currentPart - 1].skater = dr[0].ToString();
                        skResultSingle[Definizioni.currentPart - 1].order = Definizioni.currentPart.ToString();
                        skResultSingle[Definizioni.currentPart - 1].country = dr[1].ToString();
                        skResultSingle[Definizioni.currentPart - 1].seg_code = dr[3].ToString();

                        skResultComb[Definizioni.currentPart - 1].skater = dr[0].ToString();
                        skResultComb[Definizioni.currentPart - 1].order = Definizioni.currentPart.ToString();
                        skResultComb[Definizioni.currentPart - 1].country = dr[1].ToString();
                        skResultComb[Definizioni.currentPart - 1].seg_code = dr[3].ToString();
                    }
                }

                // recupero le info dell'atleta 
                GetInfoSkater(idAtleta);

                // 2.0.1 Gestione Video
                skProg = new Video.ProgressiveTotal();
                skProg.command = "progress";
                skProg.leader = "";
                skProg.current = "";

                if (Definizioni.currentPart > 1)
                {
                    // recupero il base tech più alto
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "SELECT BaseTech FROM GaraFinal " +
                                              "WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                                              " AND ID_Segment = " + Definizioni.idSegment +
                                              " AND Position = 1";
                        SQLiteDataReader dr = command.ExecuteReader();

                        while (dr.Read())
                        {
                            skProg.leader = FormattaQuestoBenedettoDecimale(dr[0]);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("LoadSkater():" + ex.Message + "", "ERROR");
            }
        }

        public static bool GetInfoSkater(int idAtleta)
        {
            try
            {
                skInfo.info = "";
                // recupero le INFO dell'atleta
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT InfoFIeld1 FROM Participants " +
                        "WHERE ID_Atleta = " + idAtleta + " AND " +
                        "ID_GaraParams = " + Definizioni.idGaraParams + " AND " +
                        "ID_Segment = " + Definizioni.idSegment;
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        skInfo.info = dr[0].ToString().TrimEnd();
                    }
                    if (dr.HasRows)
                        return true;
                    else return false;
                }

            }
            catch (Exception)
            {
                return false;
            }
        }

        ///<summary> carico l'ordine di entrata - dalla versione 2.0.1 
        ///</summary>
        public static void LoadEventsForVIdeo()
        {
            try
            {
                events4video = new Video.IN_EventList.EventList_Response();
                events4video.response = "event_list";
                events4video.competition = "RollArt Events";
                events4video.events = new List<Video.IN_EventList.Events>();

                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT A.ID_GaraParams, A.Name, A.ID_Segment, S.Name, C.Name, D.Name  " +
                                          "FROM GaraParams as A, Segments as S, Category as C, Specialita as D " +
                                          "WHERE A.ID_Segment = S.ID_Segments" +
                                          " AND A.ID_Category = C.ID_Category" +
                                          " AND A.ID_Specialita = D.ID_Specialita";
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        Video.IN_EventList.Events eventoSingolo = new Video.IN_EventList.Events();
                        eventoSingolo.id = GetEventID(dr[0].ToString(), dr[2].ToString());
                        //eventoSingolo.name = dr[1].ToString() + " " + dr[4].ToString() + " " + dr[5].ToString() + " " + dr[3].ToString();
                        eventoSingolo.name = dr[4].ToString() + " " + dr[5].ToString() + " : " + dr[3].ToString();
                        eventoSingolo.segname = dr[3].ToString();
                        eventoSingolo.segment = dr[2].ToString();
                        eventoSingolo.category = dr[4].ToString();
                        eventoSingolo.discipline = dr[5].ToString();

                        events4video.events.Add(eventoSingolo);
                    }
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("LoadEventsForVIdeo():" + ex.Message + "", "ERROR");
            }
        }

        public static string GetEventID(string gara, string segment)
        {
            return gara + int.Parse(segment.ToString()).ToString("000");
        }

        ///<summary> restituisco l'ordine di entrata in pista del segmento selezionato - dalla versione 2.0.1 
        ///</summary>
        public static void GetOrderForEvent(string id, string segment)
        {
            try
            {
                order4video = new Video.IN_EventOrder.EventOrder_Response();
                order4video.response = "event_order";
                order4video.id = id;
                order4video.segment = segment;
                order4video.skaters = new List<Video.IN_EventOrder.Skater>();

                string idGaraParams = id.Remove(id.Length - 3);

                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT S.Name, P.NumStartingList, A.Name, A.Country " +
                                          "FROM Segments as S, Participants as P, Athletes as A " +
                                          "WHERE P.ID_Segment = S.ID_Segments" +
                                          " AND P.ID_GaraParams = " + idGaraParams +
                                          " AND P.ID_Segment = " + segment +
                                          " AND A.ID_Atleta = P.ID_Atleta";
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        order4video.segname = dr[0].ToString();

                        Video.IN_EventOrder.Skater skater = new Video.IN_EventOrder.Skater();
                        skater.order = dr[1].ToString();
                        skater.name = dr[2].ToString();
                        skater.country = dr[3].ToString();
                        order4video.skaters.Add(skater);
                    }
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("GetOrderForEvent():" + ex.Message + "", "ERROR");
            }
        }

        ///<summary> restituisco le info del pattinatore - dalla versione 2.0.1 
        ///</summary>
        public static void GetInfoForSkater(string id, string segment, string order)
        {
            try
            {
                skaterinfo4video = new Video.IN_Skater.Info_Response();
                skaterinfo4video.response = "skater_info";
                skaterinfo4video.id = id;
                skaterinfo4video.segment = segment;
                skaterinfo4video.order = order;
                skaterinfo4video.skater_info = "";

                string idGaraParams = id.Remove(id.Length - 3);
                int idAtleta = 0;

                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT A.ID_Atleta, A.Name, A.Country " +
                                          "FROM Segments as S, Participants as P, Athletes as A " +
                                          "WHERE P.ID_Segment = S.ID_Segments" +
                                          " AND P.ID_GaraParams = " + idGaraParams +
                                          " AND A.ID_Atleta = P.ID_Atleta" +
                                          " AND P.NumStartingList = " + order;
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        skaterinfo4video.name = dr[1].ToString();
                        skaterinfo4video.country = dr[2].ToString();
                        idAtleta = int.Parse(dr[0].ToString());
                    }
                }
                GetInfoSkater(idAtleta);
            }
            catch (Exception ex)
            {
                Utility.WriteLog("GetOrderForEvent():" + ex.Message + "", "ERROR");
            }
        }

        ///<summary> restituisco le info del pattinatore - dalla versione 2.0.1 
        ///          Type Result type “0” = final result, “1” = segment result.
        ///</summary>
        public static void GetResults(string id, string segment, string type)
        {
            try
            {
                result4Video = new Video.IN_EventResult.EventResult_Response();
                result4Video.response = "event_result";
                result4Video.id = id;
                result4Video.segment = segment;
                result4Video.type = type;
                result4Video.footer = "Qantas (static sponsor, info not managed)";
                result4Video.skaters = new List<Video.IN_EventResult.SkaterRank>();

                string idGaraParams = id.Remove(id.Length - 3);

                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    SQLiteDataReader dr = null;

                    if (type.Equals("1")) // segment
                    {
                        command.CommandText = "SELECT DISTINCT Position, A.Name, A.Country, Total, C.Name, D.Name, S.Name " +
                                          "FROM GaraFinal as F, Athletes as A, GaraParams as G, Category as C, Specialita as D, Segments as S " +
                                          "WHERE F.ID_GaraParams = " + idGaraParams +
                                          " AND F.ID_Segment = " + segment +
                                          " AND G.ID_GaraParams = F.ID_GaraParams" +
                                          " AND G.ID_Specialita = D.ID_Specialita" +
                                          " AND G.ID_Category = C.ID_Category" +
                                          " AND F.ID_Segment = S.ID_Segments" +
                                          " AND A.ID_Atleta = F.ID_Atleta" +
                                          " ORDER BY Position";
                        dr = command.ExecuteReader();
                        while (dr.Read())
                        {
                            result4Video.heading = dr[4].ToString() + " " +
                                                   dr[5].ToString() + " " +
                                                   dr[6].ToString() + " Result";
                            Video.IN_EventResult.SkaterRank skater = new Video.IN_EventResult.SkaterRank();
                            skater.rank = dr[0].ToString();
                            skater.name = dr[1].ToString();
                            skater.country = dr[2].ToString();
                            skater.score = FormattaQuestoBenedettoDecimale(dr[3]);
                            result4Video.skaters.Add(skater);
                        }

                    }
                    else if(type.Equals("0")) // final
                    {
                        command.CommandText = "SELECT DISTINCT Position, A.Name, A.Country, TotGara, C.Name, D.Name " +
                                          "FROM GaraTotal as F, Athletes as A, GaraParams as G, Category as C, Specialita as D " +
                                          "WHERE F.ID_GaraParams = " + idGaraParams +
                                          " AND A.ID_Atleta = F.ID_Atleta" +
                                          " AND G.ID_GaraParams = F.ID_GaraParams" +
                                          " AND D.ID_Specialita = G.ID_Specialita" +
                                          " AND C.ID_Category = G.ID_Category" +
                                          " ORDER BY Position";
                        dr = command.ExecuteReader();
                        while (dr.Read())
                        {
                            result4Video.heading = dr[4].ToString() + " " +
                                                   dr[5].ToString() + " Final Result";
                            Video.IN_EventResult.SkaterRank skater = new Video.IN_EventResult.SkaterRank();
                            skater.rank = dr[0].ToString();
                            skater.name = dr[1].ToString();
                            skater.country = dr[2].ToString();
                            skater.score = FormattaQuestoBenedettoDecimale(dr[3].ToString());
                            result4Video.skaters.Add(skater);
                        }
                    }
                    
                    
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("GetOrderForEvent():" + ex.Message + "", "ERROR");
            }
        }

        #endregion

        #region Video Display
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        private static extern long SetDisplayConfig(uint numPathArrayElements,
        IntPtr pathArray, uint numModeArrayElements, IntPtr modeArray, uint flags);

        static UInt32 SDC_TOPOLOGY_INTERNAL = 0x00000001;
        static UInt32 SDC_TOPOLOGY_CLONE = 0x00000002;
        static UInt32 SDC_TOPOLOGY_EXTEND = 0x00000004;
        static UInt32 SDC_TOPOLOGY_EXTERNAL = 0x00000008;
        static UInt32 SDC_APPLY = 0x00000080;

        public static void CloneDisplays()
        {
            SetDisplayConfig(0, IntPtr.Zero, 0, IntPtr.Zero, (SDC_APPLY | SDC_TOPOLOGY_CLONE));
        }

        public static void ExtendDisplays()
        {
            SetDisplayConfig(0, IntPtr.Zero, 0, IntPtr.Zero, (SDC_APPLY | SDC_TOPOLOGY_EXTEND));
        }

        public static void ExternalDisplay()
        {
            SetDisplayConfig(0, IntPtr.Zero, 0, IntPtr.Zero, (SDC_APPLY | SDC_TOPOLOGY_EXTERNAL));
        }

        public static void InternalDisplay()
        {
            SetDisplayConfig(0, IntPtr.Zero, 0, IntPtr.Zero, (SDC_APPLY | SDC_TOPOLOGY_INTERNAL));
        }

        public static Process DisplayChangerExtend = new Process
        {
            StartInfo =
            {
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                FileName = "DisplaySwitch.exe",
                Arguments = "/clone",
                UseShellExecute = false
            }
        };

        public static Process DisplayChangerTechOnly = new Process
        {
            StartInfo =
            {
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                FileName = "DisplaySwitch.exe",
                Arguments = "/internal",
                UseShellExecute = false

            }
        };

        // BLANK command
        public static void Video_BlankCommand()
        {
            try
            {
                if (Definizioni.stateVideo == 2)
                {
                    blank = new Video.Blank();
                    blank.command = "blank";
                    stringToVideo = JsonConvert.SerializeObject(blank, Newtonsoft.Json.Formatting.Indented);
                    VideoDisplay.InvioDatiAlVideo(stringToVideo);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Video_BlankCommand():" + ex.Message + "", "ERROR");
            }
        }

        // Skater info
        public static void Video_SkaterCommand()
        {
            try
            {
                if (Definizioni.stateVideo == 2)
                {
                    stringToVideo =
                    JsonConvert.SerializeObject(skInfo, Newtonsoft.Json.Formatting.Indented);
                    VideoDisplay.InvioDatiAlVideo(stringToVideo);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Video_SkaterCommand():" + ex.Message + "", "ERROR");
            }
        }

        // Progress info
        public static void Video_ProgressCommand()
        {
            try
            {
                if (Definizioni.stateVideo == 2)
                {
                    stringToVideo =
                    JsonConvert.SerializeObject(skProg, Newtonsoft.Json.Formatting.Indented);
                    VideoDisplay.InvioDatiAlVideo(stringToVideo);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Video_ProgressCommand():" + ex.Message + "", "ERROR");
            }
            
        }

        // Result segment 
        public static void Video_ResultCommand()
        {
            try
            {
                if (Definizioni.stateVideo == 2)
                {
                    if (Definizioni.typeResult.Equals("segment"))
                        stringToVideo = JsonConvert.SerializeObject(skResultSingle[Definizioni.currentPart - 1],
                            Newtonsoft.Json.Formatting.Indented);
                    else
                        stringToVideo = JsonConvert.SerializeObject(skResultComb[Definizioni.currentPart - 1],
                            Newtonsoft.Json.Formatting.Indented);
                    VideoDisplay.InvioDatiAlVideo(stringToVideo);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Video_ResultCommand():" + ex.Message + "", "ERROR");
            }
        }

        // Display Clock
        public static void Video_ClockCommand()
        {
            try
            {
                if (Definizioni.stateVideo == 2)
                {
                    clockVideo.command = "clock";
                    stringToVideo = JsonConvert.SerializeObject(clockVideo, Newtonsoft.Json.Formatting.Indented);
                    VideoDisplay.InvioDatiAlVideo(stringToVideo);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Video_ClockCommand():" + ex.Message + "", "ERROR");
            }
        }

        // Display Timer
        public static void Video_TimerCommand()
        {
            try
            {
                if (Definizioni.stateVideo == 2)
                {
                    timerVideo.command = "timer";
                    stringToVideo = JsonConvert.SerializeObject(timerVideo, Newtonsoft.Json.Formatting.Indented);
                    VideoDisplay.InvioDatiAlVideo(stringToVideo);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Video_TimerCommand():" + ex.Message + "", "ERROR");
            }
            
        }

        #endregion

        #region Nuovo metodo di connessione giudici - dalla 1.0.4

        public bool exitFlag = true;
        static int counterConnections = 0;
        public HandleJudgeClient hjc = null;
        TcpListener listenerSocket = null;
        TcpClient client;

        // 1 thread per gestire le connessioni con i client
        public void WaitingJudges(object main)
        {
            try
            {
                listenerSocket = new TcpListener(IPAddress.Any, 
                    RollartSystemTech.Properties.Settings.Default.JudgePort);
                listenerSocket.Start();
                WriteLog(">> " + "Server Started", "OK");

                while (exitFlag) // Add your exit flag here
                {
                    if (!listenerSocket.Pending())
                    {
                        Thread.Sleep(100);
                    }
                    else
                    {
                        counterConnections += 1;
                        client = listenerSocket.AcceptTcpClient();

                        // Start a thread to handle this client...
                        hjc = new HandleJudgeClient((Main)main);
                        hjc.startClient(client, Convert.ToString(counterConnections), (Main)main);                        
                    }
                }
            }
            catch (SocketException e)
            {
                WriteLog("SocketException: {0}" + e.Message, "ERROR");
            }
            finally
            {
                // Stop listening for new clients.
                listenerSocket.Stop();
                if (client != null) client.Close();
            }
        }

        public void DisconnectJudges()
        {
            exitFlag = false;
        }

        public static void SendBroadcast(string dati)
        {
            int i = 0;
            try
            {
                string log = "";
                for (int j = 0; j < Definizioni.numJudges; j++)
                    log += (j + 1) + ",";
                 
                Utility.WriteLog(">> " + dati + " to Judge " + log.TrimEnd(','), "OK");

                for (i = 0; i < Definizioni.numJudges; i++)
                {
                    if (Main.jclient.list[i].handle != null)
                        Main.jclient.list[i].handle.SendToJudge(dati, (i + 1));
                        //EventsForm.jclient.list[i].handle.SendToJudge(dati, i);
                }
                
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Judge :" + (i + 1) + " (" + ex.Message + ")", "ERROR");
            }
        }

        public static void SendToSingleJudge(string dati, int giudice)
        {
            try
            {
                if (Main.jclient.list[giudice - 1].handle != null)
                    Main.jclient.list[giudice - 1].handle.SendToJudge(dati, giudice - 1);
            }
            catch (Exception ex)
            {
                Utility.WriteLog("SendToSingleJudge:" + ex.Message, "ERROR");
            }
        }

        public static bool InvioDati(ListView lv, TextBox log)
        {
            string elementXML = "";
            try
            {
                bool ret = true;
                int numLVElem = lv.Items.Count - 1;
                string updateBool = "F", nullBool = "F";
                if (Definizioni.updating) // se sto modificando
                {
                    numLVElem = lv.SelectedItems[0].Index;
                    updateBool = "T";
                }
                // Modifica del 02/05/2017
                string valueEl = lv.Items[numLVElem].SubItems[2].Text;
                if (decimal.Parse(valueEl).Equals(0))
                {
                    nullBool = "T";
                }

                // Modifica del 20-12-2016
                // se trottola combo faccio arrivare al giudice solo la prima trottola e la rinomino in CombSpin o ContactSpin
                if (lv.Items[numLVElem].SubItems[3].Text.StartsWith("CombSpin ") ||
                    lv.Items[numLVElem].SubItems[3].Text.StartsWith("SideSpin ") || // %%% modifica del 31/03/2018
                    lv.Items[numLVElem].SubItems[3].Text.StartsWith("ContactSpin "))
                {
                    lv.Items[numLVElem].ToolTipText = lv.Items[numLVElem].SubItems[3].Text.Split(' ')[0];
                    // modifica del 30 Gennaio 2018 - Se combospin faccio arrivare al giudice trottola non null anche se NS come prima combo
                    nullBool = "";
                }

                elementXML = "<element NUM = '" + lv.Items[numLVElem].SubItems[0].Text + "'" + // Num elemento
                                            " NAME = '" + lv.Items[numLVElem].SubItems[1].Text.Replace("<", "") + "'" + // Nome elemento
                                            " DESC = '" + lv.Items[numLVElem].ToolTipText;
                if (lv.Items[numLVElem].SubItems[7].Text.Contains("*"))
                    elementXML += " *'";
                else elementXML += "'";
                elementXML += " CAT = '" + lv.Items[numLVElem].SubItems[4].Text + "'" + // cat elemento
                                            " TYPE = '" + lv.Items[numLVElem].SubItems[3].Text + "'" + // type elemento
                                            " PENALTY = '" + lv.Items[numLVElem].SubItems[6].Text + "'" + // downgrade, halfrotated o underotated D/U/H
                                            " ID = '" + (lv.Items[numLVElem].Index + 1) + "'" +
                                            " UPDATE = '" + updateBool + "'" +
                                            " NULL = '" + nullBool + "'" +
                                            " NUMCOMBO = '" + lv.Items[numLVElem].SubItems[5].Text + "' />"; // numero elemento in catena
                Utility.SendBroadcast(elementXML);
                return ret;
            }
            catch (Exception ex)
            {
                MessageBox.Show("InvioDati: " + elementXML + "\r\nError!!!\r\n" + ex.Message);
                WriteLog("InvioDati: " + ex.Message, "ERROR");
                return false;
            }

        }

        public void RicevoDati(TextBox log)
        {
            try
            {
                Definizioni.qoeRicevuti = new string[Definizioni.numJudges];
            }
            catch (Exception ex)
            {
                WriteLog("RicevoDati: " + ex.Message, "ERROR");
            }
        }

        #endregion - 2.0

        public void WaitGiudici()
        {
            TimeSpan now = new TimeSpan(DateTime.Now.Ticks);
            TimeSpan elapsed;

            while (_shouldStop)
            {

                // dopo un minuto controllo i giudici
                elapsed = new TimeSpan(DateTime.Now.Ticks);
                ((Button)Definizioni.currentForm.Controls.Find("ltimer", true)[0]).Text = string.Format("{0:00}''", (elapsed.TotalSeconds - now.TotalSeconds));
                ((Button)Definizioni.currentForm.Controls.Find("ltimer", true)[0]).Refresh();
                ((FlowLayoutPanel)Definizioni.currentForm.Controls.Find("flowLayoutPanel1", true)[0]).Refresh();
                //((SpinningCircles)Definizioni.currentForm.Controls.Find("spinningCircles1", true)[0]).Refresh();

                        /********** Modifica del 26/04/2017 ******************************
                         * Se sono passati 60 secondi abilito il bottone di stop nella
                         * maschera di waiting, in modo da poter interrompere l'attesa dei
                         * giudici
                         * ***************************************************************/
                        //if (elapsed.TotalSeconds - now.TotalSeconds > 60)
                        //{
                        //    string message = Definizioni.resources["panel2"].ToString();

                        //    for (int i = 0; i < Definizioni.qoeRicevuti.Length; i++)
                        //    {
                        //        if (Definizioni.qoeRicevuti[i] == null)
                        //            message += "Judge " + (i + 1) + ": waiting...\r\n";
                        //        //else message += "Judge " + (i + 1) + ": " + Definizioni.qoeRicevuti[i] + " \r\n";
                        //    }

                        //    if (MessageBox.Show(message, "Warning",
                        //        MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                        //    {
                        //        break;
                        //    }
                        //    else
                        //    {
                        //        // continuo ad aspettare
                        //        now = new TimeSpan(DateTime.Now.Ticks);
                        //    }
                        //}
                        //else if (Definizioni.exitFromWaiting)
                        //{
                        //    InvioComandoAlGiudice("<CLEARCONFIRM/>");
                        //    break;
                        //}

                if (Definizioni.exitFromWaiting)
                {
                    SendBroadcast("<CLEARCONFIRM/>");
                    break;
                //}
                // gestione inserimento manuale dei punteggi dei giudici che si sono disconnessi
                //if (Definizioni.exitFromWaiting)
                //{

                }

                _shouldStop = false;
                for (int i = 0; i < Definizioni.numJudges; i++)
                //for (int i = 0; i < Definizioni.connectedJudges.Length; i++)
                {
                    if (Definizioni.qoeRicevuti[i] != null)
                    {
                        ((Button)Definizioni.currentForm.Controls.Find("j" + (i + 1), true)[0]).ForeColor = Color.Lime;
                        ((Button)Definizioni.currentForm.Controls.Find("j" + (i + 1), true)[0]).FlatAppearance.BorderColor = Color.Lime;
                        ((Button)Definizioni.currentForm.Controls.Find("j" + (i + 1), true)[0]).BackColor = Color.Transparent;
                    }
                    else
                    {
                        //((Button)Definizioni.currentForm.Controls.Find("j" + (i + 1), true)[0]).ForeColor = Color.Red;
                        //((Button)Definizioni.currentForm.Controls.Find("j" + (i + 1), true)[0]).FlatAppearance.BorderColor = Color.Red;
                        //((Button)Definizioni.currentForm.Controls.Find("j" + (i + 1), true)[0]).BackColor = Color.Yellow;
                        _shouldStop = true;
                    }
                }
                //Thread.Sleep(500);
                Thread.Sleep(100);
                //spinning.Visible = false;
            }
            if (w != null)
            {
                w.Dispose();
                w = null;
            }
        }

        public void WaitForm()
        {
            //SpinningCircles spinning = ((SpinningCircles)Definizioni.currentForm.Controls.Find("spinningCircles1", true)[0]);
            //spinning.Visible = true;
            w.ShowDialog();
            
        }

        public static bool PingGiudice(int num)
        {
            Ping pingSender = new Ping();
            PingOptions options = new PingOptions();
            bool res = false;
            // Use the default Ttl value which is 128,
            // but change the fragmentation behavior.
            options.DontFragment = true;
            string data = "ciao";
            byte[] buffer = Encoding.ASCII.GetBytes(data);
            int timeout = 120;

            // verifico che il giudice è connesso ed è abilitato il ping
            PingReply reply = pingSender.Send(Main.jclient.list[num].ip, timeout, buffer, options);
            if (reply.Status == IPStatus.Success)
            {
                res = true;
            }

            return res;
        }

        public static void AggiornoTotali(TextBox log, ListView lv, 
            Label deductions, Label elements,Label total)
        {
            try
            {
                Definizioni.elementsDec = 0;
                foreach (ListViewItem lvi in lv.Items)
                {
                    // se l'elemento contiene * non aggiungo
                    if (!lvi.SubItems[7].Text.Contains("*"))
                        Definizioni.elementsDec += decimal.Parse(lvi.SubItems[2].Text);
                }
                Definizioni.deductionsDec = decimal.Parse(deductions.Text);
                Definizioni.totale = Definizioni.elementsDec + Definizioni.deductionsDec;
                elements.Text = String.Format("{0:0.00}", Definizioni.elementsDec);
                total.Text = String.Format("{0:0.00}", Definizioni.totale);

                // 2.0.1 Video - invio al Video il totale tecnico
                if (Definizioni.stateVideo == 2)
                {
                    if (Definizioni.elementsDec > 0)
                    {
                        skProg.current = FormattaQuestoBenedettoDecimale(elements.Text);
                        Video_ProgressCommand();
                    }
                }
                    
            }
            catch (Exception ex)
            {
                log.Text += "AggiornoTotElements: " + ex.Message;
            }
        }
        
        public static string EstraiInfo(string infos, int index)
        {
            return infos.Split(':')[index];
        }

        public static String GetPercentage(Decimal value, Decimal total, Int32 places)
        {
            Decimal percent = 0;
            String retval = string.Empty;
            String strplaces = new String('0', places);

            if (value == 0 || total == 0)
            {
                percent = 0;
            }

            else
            {
                percent = Decimal.Divide(value, total) * 100 - 100;
                if (places > 0)
                {
                    strplaces = "." + strplaces;
                }
            }

            retval = percent.ToString("#" + strplaces);
            return retval;
        }

        public static string GetIP()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        public static string GetSymbol(string hexSymbol)
        {
            try
            {
                string copyrightUnicode = hexSymbol;
                int value = int.Parse(copyrightUnicode, System.Globalization.NumberStyles.HexNumber);
                return char.ConvertFromUtf32(value).ToString();
            } catch (Exception)
            {
                return "10%";
            }
        }

        public static void FitControlFont(Control control)
        {
            if (control.Text.Length == 0)
            {
                return;
            }

            Font currentFont = control.Font;
            Graphics graphics = control.CreateGraphics();
            SizeF newSize = graphics.MeasureString(control.Text, control.Font);
            graphics.Dispose();

            float factorX = control.Width / newSize.Width;
            float factorY = control.Height / newSize.Height;
            float factor = factorX > factorY ? factorY : factorX;
            if (control.InvokeRequired)
            {
                control.Invoke(new MethodInvoker(delegate { control.Font = new Font(currentFont.Name, currentFont.SizeInPoints * factor); }));
            }
            else
            {
                control.Font = new Font(currentFont.Name, currentFont.SizeInPoints * factor);
            }
        }
    

    }

    public class HandleJudgeClient
    {
        public TcpClient clientSocket;
        public NetworkStream networkStream = null;
        string clNo;
        public static TextBox log = null;
        Main mainForm = null;
        public int numGiudice = 0;
        public JudgeClient jcCurrent = null;
        string idMsgCurrent = "";

        public HandleJudgeClient (Main main)
        {
            mainForm = main;
        }

        public void startClient(TcpClient inClientSocket, string clineNo, Main main)
        {
            mainForm = main;
            this.clientSocket = inClientSocket;
            this.clNo = clineNo;

            Thread ctThread = new Thread(ReceiveFromJudge);
            ctThread.Start();


        }

        private void ReceiveFromJudge()
        {
            //string datidecifrati = "";
            byte[] message = new byte[8192];
            int bytesRead = 0;
            string stringaGiudice = "";

            while (true)
            {
                bytesRead = 0;
                try
                {
                    if (clientSocket != null)
                    {
                        if (clientSocket.Connected)
                        {
                            //blocks until a client sends a message
                            networkStream = clientSocket.GetStream();
                            int timeout = networkStream.ReadTimeout;
                            bytesRead = networkStream.Read(message, 0, message.Length);
                            //Utility.WriteLog(bytesRead + " bytes ricevuti dal Judge " + clNo + "\r\n", "WARNING");
                        }
                        else
                        {
                            MessageBox.Show("socket null");
                        }
                    } 
                }
                catch (SocketException se)
                {
                    //a socket error has occured
                    Utility.WriteLog("SocketException (" + se.Message + ")" + se.ErrorCode, "ERROR");
                    ManageJudgeError("Socket Error, not connected");
                    break;
                }
                catch (IOException se)
                {
                    //a i/o error has occured
                    Utility.WriteLog("IOException (" + se.Message + ")", "ERROR");
                    ManageJudgeError("I/O Error, not connected");
                    break;
                }

                if (bytesRead == 0)
                {
                    //the client has disconnected from the server
                    ManageJudgeError("Not connected");
                    break;
                }

                try
                {
                    stringaGiudice = new ASCIIEncoding().GetString(message, 0, bytesRead);

                    // ACK
                    if (stringaGiudice.StartsWith("ACK"))
                    {
                        ManageMsgFromGiudice(stringaGiudice);
                        //numGiudice = int.Parse(stringaGiudice.Substring(3, 1));
                    }
                    else if (stringaGiudice.StartsWith("PNG"))
                    {
                        ManageMsgFromGiudice(stringaGiudice);
                        //numGiudice = int.Parse(stringaGiudice.Substring(3, 1));
                    }
                    // tutti gli altri messaggi
                    else
                    {
                        int index = stringaGiudice.IndexOf("numCurrentJudge = ");
                        int numGiudice = int.Parse(stringaGiudice.Substring(index + 19, 1)) - 1;
                        Definizioni.qoeRicevuti[numGiudice] = stringaGiudice;
                        //Definizioni.connectedJudges[numGiudice] = true;
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog("<< " + stringaGiudice + ex.Message, "WARNING");
                    ManageJudgeError("Generic Error, not connected or password wrong");
                }
            }
        }

        public void ManageJudgeError(string errorMsg)
        {

            try
            {
                foreach (JudgeClient judge in Main.jclient.list)
                {
                    if (judge.handle != null && clientSocket != null)
                    {
                        if (judge.handle.clientSocket != null)
                        {
                        if (judge.handle.clientSocket.Client.Handle == clientSocket.Client.Handle)
                        {
                            Utility.WriteLog("Judge " + judge.num
                                + " [handle=" + judge.handle.clientSocket.Client.Handle + "] has been disconnected", "OK");
                            // gestione bottoni Main.TOP
                            var topStrip = mainForm.Controls.Find("topStrip", true);
                            if (topStrip.Length > 0)
                            {
                                var toolstrip1Items = topStrip[0] as ToolStrip;
                                var btnGiudice = toolstrip1Items.Items.Find("tsJudge" +
                                    judge.num, true); //<--get BtnRead on toolstrip Item.Find
                                btnGiudice[0].Tag = "";                                                                                     //btnGiudice[0].Enabled = false;
                                btnGiudice[0].ToolTipText = errorMsg;
                                btnGiudice[0].Image = RollartSystemTech.Properties.Resources.male_user_red;
                                //break;
                            }
                            // gestione listview Giudici
                            if (mainForm.Controls.Find("lv3", true) != null)
                            {
                                if (mainForm.Controls.Find("lv3", true).Length > 0)
                                {
                                    var lvJudges = mainForm.Controls.Find("lv3", true)[0] as ListView;
                                    foreach (ListViewItem item in lvJudges.Items)
                                    {
                                            if (item.SubItems[2].Text.Equals("Judge " + judge.num))
                                        {
                                            item.ImageIndex = 3; // disconnesso
                                                item.SubItems[3].Text = "NOT Connected";
                                            break;
                                        }
                                    }
                                }
                            }

                            // gestione bottoni Segment.JUDGES 
                            if (Definizioni.started)
                            {
                                // Singolo
                                if (mainForm.sps != null)
                                {
                                    var btnGiudice = mainForm.sps.Controls.Find("j" + judge.num, true);
                                    btnGiudice[0].Tag = judge;
                                    btnGiudice[0].ForeColor = Color.Lime;
                                }


                            }

                            // cancello socket e stato
                            judge.handle.clientSocket = null;
                            judge.state = "0";
                            }
                        }
                    }
                }
            } catch (Exception) { }
        }

        public void SendToJudge(string dati, int numJudge)
        {
            byte[] outStream = null;
            try
            {
                {
                    outStream = System.Text.Encoding.ASCII.GetBytes(dati);
                    networkStream.Write(outStream, 0, outStream.Length);
                    networkStream.Flush();
                    //Utility.WriteLog(">> " + dati + " to Judge" + numJudge, "OK");
                    //if (numJudge == 0) Utility.WriteLog(">> " + dati, "OK");
                    //else Utility.WriteLog(">> " + dati + " to Judge" + numJudge, "OK");
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex.Message, "ERROR");
            }

        }

        // CONTROLLO i MESSAGGI ogni 2 secondi
        public void CheckMessages(object numMessage)
        {
            while (true)
            {
                Thread.Sleep(2000);
                //Utility.WriteLog("Messaggio " + numMessage, "OK");
                if (int.Parse(idMsgCurrent.ToString()) == jcCurrent.id_msg)
                //if (Main.garaCorrente.numMessages == jcCurrent.id_msg)
                {
                    //Utility.WriteLog("Messaggio " + idMsgCurrent
                    //+ " arrivato al giudice " + jcCurrent.num, "OK");
                    break;
                }
                else // Recupero messaggio e reinvio stringa al giudice
                {
                    Utility.WriteLog("Messaggio " + idMsgCurrent +
                     " non arrivato al giudice " + jcCurrent.num, "ERROR");
                }

            }
        }

        // button TAG--> "0": not connected, "1": connected, "-1"_ not logged
        public void ManageMsgFromGiudice(string dati)
        {
            try
            {
                int numGiudi = int.Parse(dati.Substring(3, 1));
                ListView lv3 = null;
                if ((mainForm.Controls.Find("lv3", true)) != null)
                {
                    lv3 = (ListView)mainForm.Controls.Find("lv3", true)[0];
                }
                //ListView lv3 = (ListView)mainForm.Controls.Find("lv3", true)[0];
                //var topStrip = mainForm.Controls.Find("topStrip", true);
                //var toolstrip1Items = topStrip[0] as ToolStrip;
                //var btnGiudice = toolstrip1Items.Items.Find("tsJudge" + numGiudi, true); //<--get BtnRead on toolstrip Item.Find

                // [ACKn:OK o ACKn:KO] es. ACK2OK
                if (dati.StartsWith("ACK"))
                {
                    //bool giaEsistente = false;

                    foreach (JudgeClient judge in Main.jclient.list)
                    {
                        if (judge.num == numGiudi)
                        {
                            //giaEsistente = true;
                            judge.handle = this;
                            judge.num = numGiudi;
                            judge.state = "1"; // connesso
                            judge.ip = dati.Split(':')[1];
                            if (lv3 != null)
                            {
                                if (lv3.Items.Count != 0)
                                {
                                    foreach (ListViewItem lvi in lv3.Items)
                                    {
                                        if (lvi.SubItems[2].Text.Equals("Judge " + judge.num))
                                        {
                                            lvi.Tag = judge;
                                            lvi.ImageIndex = 2;
                                            lvi.SubItems[3].Text = "Connected";
                                            lvi.ToolTipText = judge.ip;
                                        }
                                    }
                                }
                            }
                            jcCurrent = judge;
                            break;
                        }
                    }

                    SendToJudge("Login:OK", numGiudi);

                    Utility.WriteLog("<< " + dati, "OK");
                }

                // [LOGn:ChiaveRandom] es. LOG3:sdfsdfaferergewwvx
                //else if (dati.StartsWith("LOG"))
                //{
                //    string passwordRicevuta = dati.Split(':')[1];
                //    string ipRicevuto = dati.Split(':')[2];

                //    Utility.WriteLog("<< " + dati.Substring(0, 4) + " Judge logging...", "OK");

                //    // verifico se è un giudice autenticato
                //    if (!UtilityDB.CheckLoginJudge(numGiudi, passwordRicevuta, ipRicevuto))
                //    {
                //        foreach (JudgeClient judge in EventsForm.jclient.list)
                //        {
                //            if (judge.num == numGiudi)
                //            {
                //                judge.state = "-1";

                //                btnGiudice[0].Tag = judge;
                //                btnGiudice[0].ToolTipText = "Authentication failed";
                //                btnGiudice[0].Image = TechPanel.Properties.Resources.male_user_yellow;

                //                // gestione listview Giudici
                //                var lvJudges = mainForm.Controls.Find("lv3", true)[0] as ListView;
                //                foreach (ListViewItem item in lvJudges.Items)
                //                {
                //                    if (item.SubItems[0].Text.StartsWith("Judge " + judge.num))
                //                    {
                //                        item.ImageIndex = 44; // non autenticato
                //                        break;
                //                    }
                //                }

                //                // gestione bottoni Segment.JUDGES
                //                if (Definizioni.started)
                //                {
                //                    var allJudges = EventsForm.ev.Controls.Find("judges1", true)[0] as TechPanel.Controlli.Judges;
                //                    var btnGiudiceSegment = allJudges.Controls.Find("j" + judge.num, true);
                //                    btnGiudiceSegment[0].Tag = judge;
                //                    btnGiudiceSegment[0].ForeColor = Color.Yellow;
                //                }
                //            }

                //        }

                //        SendToJudge("Login:KO", numGiudi);
                //        Utility.WriteLog("Judge " + numGiudi + " NOT logged", "KO");
                //    }
                //    else
                //    {
                //        foreach (JudgeClient judge in EventsForm.jclient.list)
                //        {
                //            if (judge.num == numGiudi)
                //            {
                //                judge.state = "2";
                //                judge.ip = ipRicevuto;
                //                btnGiudice[0].Tag = judge;
                //                btnGiudice[0].ToolTipText = "Authenticated";
                //                btnGiudice[0].Image = TechPanel.Properties.Resources.male_user_green;

                //                // gestione listview Giudici
                //                var lvJudges = mainForm.Controls.Find("lv3", true)[0] as ListView;
                //                foreach (ListViewItem item in lvJudges.Items)
                //                {
                //                    if (item.SubItems[0].Text.StartsWith("Judge " + judge.num))
                //                    {
                //                        item.ImageIndex = 42; // connesso
                //                        break;
                //                    }
                //                }

                //                // gestione bottoni Segment.JUDGES
                //                if (Definizioni.started)
                //                {
                //                    var allJudges = EventsForm.ev.Controls.Find("judges1", true)[0] as TechPanel.Controlli.Judges;
                //                    var btnGiudiceSegment = allJudges.Controls.Find("j" + judge.num, true);
                //                    btnGiudiceSegment[0].Tag = judge;
                //                    btnGiudiceSegment[0].ForeColor = Color.Lime;
                //                }

                //            }
                //        }
                //        SendToJudge("Login:OK", numGiudi);
                //        Utility.WriteLog("Judge " + numGiudi + " logged", "OK");
                //    }
                //}

                // [PNGn:Hello] es. PNG4:Hello
                else if (dati.StartsWith("PNG"))
                {
                    
                    Utility.WriteLog("<< " + dati, "OK");
                }

                // [ELE1:SyPz:IDx] es. ELE1:S38P2:3
                //else if (dati.StartsWith("ELE"))
                //{
                //    // Aggiorno la tabella Messages
                //    string idMsg = dati.Split(':')[2];
                //    UtilityDB.UpdateMessageOnDB(numGiudi, idMsg);

                //    Main.garaCorrente.judges.list[numGiudi - 1].id_msg = int.Parse(idMsg);
                //    //Utility.WriteLog("<< " + dati, "OK");
                //}

                // Ricevo qoe per l'elemento
                // [QOEn:GxSyPz:IDx:QOEx] 
                //else if (dati.StartsWith("QOE"))
                //{
                //    if (Properties.Settings.Default.QoeLive)
                //    {
                //        // Aggiorno la tabella Gara con il qoe del giudice
                //        string progressivoEl = dati.Split(':')[2].Split('.')[0];
                //        string qoe = dati.Split(':')[2].Split('.')[1];
                //        decimal baseValue = 0;
                //        UtilityDB.AggiornoQoe(numGiudi, progressivoEl, qoe);
                //        if (UtilityDB.CheckQoeBeforeUpdate(progressivoEl, out baseValue))
                //        {
                //            decimal valueElfinal = UtilityDB.UpdateValueFinalofSingleElement(baseValue);
                //            UpdateEverythingAfterQoe(valueElfinal, int.Parse(progressivoEl), qoe, numGiudi);
                //        }
                //    }

                //    //SendToJudge("Ack", 0);
                //}

            }
            catch (Exception ex)
            {
                Utility.WriteLog("ManageMsgFromGiudice: " + ex.Message, "ERROR");
            }
        }

        //Hashtable qoes = new Hashtable();
        //public void UpdateEverythingAfterQoe(decimal valueFinal, int progressivoEl, string qoe, int numGiudi)
        //{

        //    Main.garaCorrente.currentSkater.currentFinalTech = 0;
        //    Main.garaCorrente.currentSkater.currentTotalScore = 0;

        //    // aggiorno la listview con il valore finale dell'elemento singolo
        //    ListView lvElements = (ListView)Main.garaCorrente.everyForm.Controls.Find("lv", true)[0];
        //    lvElements.Items[progressivoEl - 1].SubItems[9].Text =
        //        String.Format("{0:0.00}", valueFinal);
        //    lvElements.Items[progressivoEl - 1].ToolTipText = lvElements.Items[progressivoEl - 1].ToolTipText
        //        + " test";

        //    // aggiorno le label di Everything
        //    // FINAL  TECH

        //    foreach (ListViewItem lvi in lvElements.Items)
        //    {
        //        // imposto la proprietà Tag dell'Item
        //        if ((lvi.Index + 1) == progressivoEl)
        //        {
        //            Main.garaCorrente.currentSkater.matrixQoes[numGiudi - 1, progressivoEl - 1] = qoe;
        //        }
        //        // se l'elemento non ha i qoe ancora aggiungo il valore base
        //        if (lvi.SubItems[9].Text.Equals(""))
        //            Main.garaCorrente.currentSkater.currentFinalTech += decimal.Parse(lvi.SubItems[2].Text);
        //        else
        //            Main.garaCorrente.currentSkater.currentFinalTech += decimal.Parse(lvi.SubItems[9].Text);
        //    }
        //    //Main.garaCorrente.currentSkater.currentFinalTech += valueFinal;
        //    Label finalTech = (Label)Main.garaCorrente.everyForm.Controls.Find("finalTech", true)[0];
        //    finalTech.Text = String.Format("{0:0.00}", Main.garaCorrente.currentSkater.currentFinalTech);

        //    // TOTAL SCORE 
        //    Main.garaCorrente.currentSkater.currentTotalScore =
        //        Main.garaCorrente.currentSkater.currentFinalTech -
        //        Main.garaCorrente.currentSkater.currentDeductions;
        //    Label currentTotalScore = (Label)Main.garaCorrente.everyForm.Controls.Find("total", true)[0];
        //    currentTotalScore.Text = String.Format("{0:0.00}", Main.garaCorrente.currentSkater.currentTotalScore);


        //}
        
        /*
        // AVERAGE
        public static bool AskAverage()
        {
            Button average = ((Button)Main.garaCorrente.everyForm.Controls.Find("average", true)[0]);
            Button ltimer = ((Button)Main.garaCorrente.everyForm.Controls.Find("ltimer", true)[0]);
            SpinningCircles spinning = ((SpinningCircles)Main.garaCorrente.everyForm.Controls.Find("spinningCircles1", true)[0]);
            TextBox log = ((TextBox)Main.garaCorrente.everyForm.Controls.Find("log", true)[0]);

            Definizioni.exitFromPing = true;
            if (average.Text.Equals("STOP"))
            {
                spinning.Visible = false;
                Definizioni.exitFromWaiting = true;
                return false;
            }
            Definizioni.exitFromWaiting = false;
            //_shouldStop = true;
            spinning.Visible = true;
            //DisableButtons();

            average.Text = "STOP";
            average.BackColor = Color.Red;
            ((Label)Main.garaCorrente.everyForm.Controls.Find("error", true)[0]).Text = "";
            //RicevoDati(log);

            // Attendo i components dai giudici             
            //Thread workerThread = new Thread(WaitGiudici);
            //workerThread.Start();
            //workerThread.Join();

            //EnableButtons();

            spinning.Visible = false;
            if (Definizioni.exitFromWaiting)
            {
                average.BackColor = Color.Red;
                average.Text = "AVERAGE";
                return false;
            }

            Average av = new Average();
            av.ShowDialog();
            Main.garaCorrente.everyForm.Refresh();
            spinning.Visible = false;
            average.Text = "AVERAGE";
            average.BackColor = Color.DimGray;
            Definizioni.exitFromPing = false;
            average.Enabled = false;

            return true;
        }

        // CONFIRM
        public static bool ConfirmSegment()
        {
            Control.ControlCollection f = Main.garaCorrente.everyForm.Controls;

            Button confirm = ((Button)f.Find("confirm", true)[0]);
            Button ltimer = ((Button)f.Find("ltimer", true)[0]);
            SpinningCircles spinning = ((SpinningCircles)f.Find("spinningCircles1", true)[0]);
            TextBox log = ((TextBox)f.Find("log", true)[0]);
            Label error = ((Label)f.Find("error", true)[0]);
            Label labConfirm = ((Label)f.Find("labConfirm", true)[0]);
            Panel panConfirm = ((Panel)f.Find("panelConfirm", true)[0]);

            ((Button)f.Find("back", true)[0]).Enabled = false;
            ((Button)f.Find("edit", true)[0]).Enabled = false;
            ((Button)f.Find("startstop", true)[0]).Enabled = false;

            // invio ai giudici la Conferma e attendo
            SendBroadcast("Confirm:");

            spinning.Visible = true;
            spinning.Refresh();
            confirm.Enabled = true;
            confirm.Text = "STOP";
            for (int i = 0; i < Main.garaCorrente.numJudges; i++)
            {
                //((Button)Definizioni.currentForm.Controls.Find("j" + (i + 1), true)[0]).ForeColor = Color.Red;
                //((Button)Definizioni.currentForm.Controls.Find("j" + (i + 1), true)[0]).FlatAppearance.BorderColor = Color.Red;
                ((Button)Main.garaCorrente.everyForm.Controls.Find("j" + (i + 1), true)[0]).BackColor = Color.Yellow;
            }
            error.Text = "";
            //RicevoDati(log);

            // Attendo punteggi dai giudici
            //Thread workerThread = new Thread(WaitGiudici);
            //workerThread.Start();
            //workerThread.Join();

            if (Definizioni.exitFromWaiting)
            {
                spinning.Visible = false;
                return false;
            }

            for (int i = 0; i < Definizioni.qoeRicevuti.Length; i++)
            {
                if (Definizioni.qoeRicevuti[i] == null)
                {
                    RecoverQoe rqoe = new RecoverQoe();
                    rqoe.ShowDialog();
                    break;
                }
            }
            ltimer.Text = "00:00";
            Main.garaCorrente.everyForm.Refresh();
            error.Text = Definizioni.resources["panel7"].ToString();
            error.Refresh();

            //UtilityDB.ParseElementsAdnSaveinDB();

            Main.garaCorrente.everyForm.Refresh();
            CompleteSegment();
            spinning.Visible = false;
            confirm.Text = "CONFIRM";

            return true;
        }

    */
    }

    #region Judges
    public class JudgeClient
    {
        public int num { get; set; }
        public string ip { get; set; }
        public HandleJudgeClient handle { get; set; }
        public string state { get; set; }
        public string message { get; set; }
        public int id_msg { get; set; }
    }

    public class JudgesClient
    {
        public int numJudges { get; set; }
        public List<JudgeClient> list { get; set; }
        public void InitializeJudge()
        {
            list = new List<JudgeClient>(9);
            for (int i = 0; i < 9; i++)
            {
                list.Add(new JudgeClient());
                list[i].ip = "";
                list[i].num = i + 1;
                list[i].handle = null;
                list[i].state = "0"; // disconnesso
                list[i].message = "";
                list[i].id_msg = 0;
            }
        }
    }
    #endregion

    public class ComboBoxItem
    {
       string displayValue;
       string hiddenValue;

       //Constructor
       public ComboBoxItem (string d, string h)
       {
            displayValue = d;
            hiddenValue = h;
       }

       //Accessor
       public string HiddenValue
       {
            get
            {
                 return hiddenValue;
            }
       }

       //Override ToString method
       public override string ToString()
       {
            return displayValue;
       }
    }

    public class GifImage
    {
        private Image gifImage;
        private FrameDimension dimension;
        private int frameCount;
        private int currentFrame = -1;
        private bool reverse;
        private int step = 1;

        public GifImage(string path)
        {
            gifImage = Image.FromFile(path);
            //initialize
            dimension = new FrameDimension(gifImage.FrameDimensionsList[0]);
            //gets the GUID
            //total frames in the animation
            frameCount = gifImage.GetFrameCount(dimension);
        }

        public bool ReverseAtEnd
        {
            //whether the gif should play backwards when it reaches the end
            get { return reverse; }
            set { reverse = value; }
        }

        public Image GetNextFrame()
        {

            currentFrame += step;

            //if the animation reaches a boundary...
            if (currentFrame >= frameCount || currentFrame < 1)
            {
                if (reverse)
                {
                    step *= -1;
                    //...reverse the count
                    //apply it
                    currentFrame += step;
                }
                else
                {
                    currentFrame = 0;
                    //...or start over
                }
            }
            return GetFrame(currentFrame);
        }

        public Image GetFrame(int index)
        {
            gifImage.SelectActiveFrame(dimension, index);
            //find the frame
            return (Image)gifImage.Clone();
            //return a copy of it
        }
    }
}

﻿"RollArt" is a registered trademark

Version 2.0.0.10
Release date: 31/01/2019

Contents of this README:
1.  Changes since version 2.0.0.9
2.  Known Issues at Release

---------------------------------------------------------------------------------------------------

1. Changes since version 2.0.0.9: 

Bug fixes for:

 * TechPanel: 
	FREE SKATING
	- Axel Jump mandatory: removed the check for Tots. 
	- Combo Spins: in case of sit spin mandatory and if the first combospin contained two valid spins 
	  (eg. Camel-Camel) and the second combospin one NLSit and only one spin confirmed (eg. Heel-NLSit-NLCamel),
	  RollArt put the asterisk (*) to all the combospins, instead of the second one only.
	
	MISSING SKATER
	- If one or more skaters is missing (skipped during the segment), they were included equally in the final results.
	
	IMPORT/EXPORT Competitors
	- Import issue and competitors grid not refreshed
	- 

Changes and Improvements:

	EVENT MANAGEMENT
	- Possibility to change the Judge panel after the event insertion.
		Button Update: enabled for each official (TS, DO, CO, AS, RF, Judges) even if connected
		Button Add: enabled for CO, AS, RF (if missing) or a judge until Judge 9. Not appliable if one or more judges are connected 
		Button Delete: enabled for CO, AS, RF or a judge (from judge 2). Not appliable if one or more judges are connected 
		Button Up/Down: enabled for judges only. Not appliable if one or more judges are connected 

	REPORTS
	- Deductions
		In "Judges Panel" report all the deductions (falls, music violation, illegal or missing element, 
		costume violation) are now detailed.
	- Improved way to export final and segment results and judge panel report. New naming convention used. 

 * Resource file resource.en.txt
	* NO CHANGES
	
 * JudgePanel
	* NO CHANGES

---------------------------------------------------------------------------------------------------

2. Known Issues in 2.0.0.9:

For known issues at release of 2.0.0.9, please send mail to:
   rollart@worldskate.org

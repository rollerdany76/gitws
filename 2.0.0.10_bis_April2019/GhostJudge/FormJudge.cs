﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using System.Collections;
using System.Diagnostics;
using System.Globalization;

namespace RollartSystemJudge
{
    public partial class FormJudge : Form
    {
        #region VARIABILI e COSTANTI

        private Thread listenThread;
        bool exitFlag = false, flagAverage = false, judgeConnected = false, flagClose = false;
        int currentElem = 0, idGaraParams = 0, idSegment = 0, numPart = 0, numCurrentJudge = 0; //, numCurrentPort = 0;
        int idDiscipline = 0, idCategory = 0;
        //string currentFile = "", rootFolder = "", iptech = "";;
        string rollbackFile = "", c1 = "", c2 = "", c3 = "", c4 = ""; // nome del file xml
        decimal[] arDecComp = new decimal[4];
        private Point mouse_offset;

        NetworkStream techPanelStream = null;
        XmlDocument xml = null;
        ASCIIEncoding encoder = null;
        ToolTip tt = null;
        TimeSpan time = new TimeSpan();

        public static bool notready = false;
        public static string elementsXML = "", compXML = "";

        TcpClient clientSocket = null;
        #endregion

        /************************************************************************************************************************
         * Messaggi ricevuti dal pannello tecnico:
         * <CONNECT/>
         * <DISCONNECT/>
         * <START Gara='id' Segment='idSegment' Partecipante = 'x'/>   --> Ha inizio la gara e viene abilitato il pannello giudice;
         * <RESUME Gara='id' Segment='idSegment' Partecipante = 'x'/>  --> Viene ripristinata la gara
         * <CONFIRM/>                                                  --> Il pannello tecnico ha confermato tutti gli elementi
         * <QUIT/>                                                     --> Chiudo il pannello giudice
         * <ANNULLA/>                                                  --> Elimino l'ultimo elemento
         * <DEDUCTIONS ded1="" ded2="" ded3="" ded4="" ded5="" ded6=""/>
         * <element NUM="numero_elemento" 
         *          NAME="nome_elemento" 
         *          DESC = "descrizione_elemento" 
         *          CAT="categoria_elemento"  
         *          TYPE="tipo_el" 
         *          PENALTY="D/U/H" 
         *          ID="id_elemento" 
         *          UPDATE="T/F" 
         *          NUMCOMBO="numero_el_nella_combinazione"
         *          />   
         * -----------------------------------------------------------------------------------------
         * Messaggio inviato al pannello tecnico:
         * <SEGMENT Segment='ID'>
         *      <ELEMENTS>
         *          <ELEMENT NUM="numero_elemento" NAME="nome_elemento" QOE="qoe" ID="id_elemento" "/>   
         *      ...
         *      </ELEMENTS>     
         *      <COMPONENTS comp1="" comp2="" comp3="" comp4="" comp5="">
         * <SEGMENT/>
         * 
         * **********************************************************************************************************************/

        public FormJudge()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
            encoder = new ASCIIEncoding();
            tt = new ToolTip();
        }

        private void FormJudge_Load(object sender, EventArgs e)
        {
            try
            {
                LoadSettings();
                //ConnectTechPanel();

                // Metodi nuovi di connessione
                Thread ctThread = new Thread(TryToConnect);
                ctThread.Start();

                j1.Text = "Judge " + numCurrentJudge;
                time = TimeSpan.Zero;
                this.Text = System.AppDomain.CurrentDomain.FriendlyName + " - "
                    + System.IO.File.GetLastWriteTime(
                    System.AppDomain.CurrentDomain.FriendlyName).ToString("dd/MM/yy HH:mm:ss") + ". " +
                    Application.ProductName + " vers. " + Application.ProductVersion;
            }
            catch (Exception ex)
            {
                WriteLog("FormJudge_Load (" + ex.InnerException + ")", "ERROR");
            }
        }

        private void WriteLog(string text, string messageType)
        {
            try
            {
                using (StreamWriter w = File.AppendText("RS_J" + numCurrentJudge
                    + ".log"))
                {
                    w.Write(DateTime.Now.ToString("dd/MM/yy HH:mm:ss") + " - ");
                    if (messageType.Equals("ERROR"))
                    {
                        w.Write("***ERROR*** --> " + text + "\r\n");
                        tp.ForeColor = Color.Red;
                        tp.FlatAppearance.BorderColor = Color.Red;
                    }
                    else w.Write(text + "\r\n");
                }

                if (log != null)
                {
                    log.Focus();
                    log.Text += DateTime.Now.ToString("dd/MM/yy HH:mm:ss") + " - ";
                    if (messageType.Equals("ERROR")) log.Text += "***ERROR*** --> " + text + "\r\n";
                    else log.Text += text + "\r\n";
                    log.Refresh();
                    log.SelectionStart = log.Text.Length;
                    log.ScrollToCaret();
                }
            }
            catch (Exception ex)
            {
                log.Text += ex.Message;
            }
        }

        // modifica 18/02/2015
        // modifica 24/07/2018
        public void LoadSettings()
        {
            try
            {
                label3.Text = "IP Judge: " + GetIP();
                numCurrentJudge = Properties.Settings.Default.CurrentJudge;
                if (Properties.Settings.Default.Log) logButton.Visible = true; else logButton.Visible = false;
                if (Properties.Settings.Default.Exit) quit.Visible = true; else quit.Visible = false;
                if (Properties.Settings.Default.Random) random.Visible = true; else random.Visible = false;

                j1.Text = "Judge " + numCurrentJudge;
                tt.SetToolTip(j1, GetIP());
                log.Text = "IP Judge: " + GetIP();

                // recupero l'IP del pannello tecnico
                tt.SetToolTip(tp, Properties.Settings.Default.TechIP + ":" + Properties.Settings.Default.TechPort);
                log.Text += "\r\nIP Tech : " + Properties.Settings.Default.TechIP + ":" + Properties.Settings.Default.TechPort + "\r\n";

                // carico i components
                c1 = Properties.Settings.Default.Component1; comp1.Text = Properties.Settings.Default.Component1;
                c2 = Properties.Settings.Default.Component2; comp2.Text = Properties.Settings.Default.Component2;
                c3 = Properties.Settings.Default.Component3; comp3.Text = Properties.Settings.Default.Component3;
                c4 = Properties.Settings.Default.Component4; comp4.Text = Properties.Settings.Default.Component4;
                //c5 = Properties.Settings.Default.Component5; comp5.Text = Properties.Settings.Default.Component5;

                // verifico se devo fare rollback
                //rollbackFile = (xml.SelectNodes("/Settings/Judge/@Rollback").Item(0).Value);

            }
            catch (Exception ex)
            {
                WriteLog("LoadSettings(): " + ex.Message, "ERROR");
            }
        }

        private string GetIP()
        {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                    break;
                }
            }
            return localIP;
        }

        #region TECHPANEL NEW
        int count = 0;
        public void TryToConnect()
        {
            // dopo 10 tentativi esco e devo connettermi manualmente
            while (true)
            {
                if (count == 5) return;
                if (!judgeConnected)
                {
                    NewConnectTechPanel(); 
                }

                Thread.Sleep(Properties.Settings.Default.ThreadWaiting);

                try
                {
                    
                    if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                    {
                        
                    }
                    else
                    {
                        judgeConnected = false;
                        clientSocket.Close();
                        SetJudgeButton(Color.Orange, true, Properties.Resources.Message16, "ERROR");
                    }
                }
                catch (Exception) { }
            }
        }

        public void NewConnectTechPanel()
        {
            try
            {
                count++;
                judgeConnected = false;
                SetJudgeButton(Color.Yellow, true, "(" + count + ") " + 
                    Properties.Resources.Message1 + Properties.Settings.Default.TechIP + ":" + 
                    Properties.Settings.Default.TechPort, "OK");
                clientSocket = new TcpClient();
                clientSocket.Connect(Properties.Settings.Default.TechIP,
                                     Properties.Settings.Default.TechPort);
                WriteLog("Judge " + numCurrentJudge + Properties.Resources.Message2, "OK");
                ComunicoConIlPannello();

                InvioDatiAlServer("ACK" + numCurrentJudge + ":" + GetIP(), false);
                settings.Visible = false;
            }
            catch (Exception ex)
            {
                SetJudgeButton(Color.LightGray, true, Properties.Resources.Message1 + ": " + ex.Message, "ERROR");
            }
        }

        public void InvioDatiAlServer(string dati, bool cifro)
        {
            try
            {
                Thread.Sleep(100);
                byte[] outStream = null;
                outStream = System.Text.Encoding.ASCII.GetBytes(dati);
                techPanelStream.Write(outStream, 0, outStream.Length);
                techPanelStream.Flush();
                WriteLog("--> " + dati, "OK");
            }
            catch (SocketException ex)
            {
                SetJudgeButton(Color.Orange, true, "SendDataToServer():" + ex.Message, "ERROR");
            }
        }

        // thread per ricevere le comunicazioni dal pannello
        public void ComunicoConIlPannello()
        {
            listenThread = new Thread(HandleClientComm);
            listenThread.Start();
        }

        private void HandleClientComm(object client)
        {
            techPanelStream = clientSocket.GetStream();
            string datidecifrati = "";
            byte[] message = new byte[4096];
            int bytesRead;

            while (true)
            {
                if (exitFlag) break;
                bytesRead = 0;

                try
                {
                    //blocks until a client sends a message
                    bytesRead = techPanelStream.Read(message, 0, message.Length);
                    
                }
                catch (SocketException se)
                {
                    //a socket error has occured
                    judgeConnected = false;
                    SetJudgeButton(Color.Red, true, "HandleClientComm() - SocketException: " + se.InnerException.Message + " Stack:" + se.InnerException.StackTrace, "ERROR");
                    break;
                }
                catch (IOException se)
                {
                    //a i/o error has occured
                    var socketExept = se.InnerException as SocketException;
                    if (socketExept == null || socketExept.ErrorCode != 10060)
                        SetJudgeButton(Color.Red, true, "HandleClientComm() - IOException: " +
                            socketExept.Message + " ErrorCode:" + socketExept.ErrorCode,
                            "ERROR");
                    // if it is the receive timeout, then reading ended
                    bytesRead = 0;
                    judgeConnected = false;
                    break;
                }

                if (bytesRead == 0)
                {
                    judgeConnected = false;
                    SetJudgeButton(Color.LightGray, true, Properties.Resources.Message4, "OK");
                    break;
                }

                // verifico il messaggio del server...
                datidecifrati = encoder.GetString(message, 0, bytesRead);

                WriteLog(datidecifrati, "OK");

                ParseElementReceived(datidecifrati);
            } 
        }      
        
        private void SetJudgeButton(Color color, bool enabled, string message, string typeMessage)
        {
            WriteLog(message, typeMessage);
            tp.ForeColor = color;
            tp.FlatAppearance.BorderColor = color;
            tp.Enabled = enabled;
        }

        #endregion

        #region //TECHPANEL OLD
        /********************************************************************************
         *  Thread per gestire i messaggi da e verso il pannello tecnico
         * *****************************************************************************
        public void ConnectTechPanel()
        {
            try
            {
                this.tcpListener = new TcpListener(IPAddress.Any, numCurrentPort);
                this.listenThread = new Thread(new ThreadStart(ListenForReferee));
                this.listenThread.Start();
            }
            catch (Exception ex)
            {
                WriteLog("ConnectTechPanel(): " + messages[0].ToString() + ex.Message, "ERROR");
            }

        }

        private void ListenForReferee()
        {
            try
            {
                this.tcpListener.Start();
                while (true)
                {
                    //blocks until a client has connected to the server
                    TcpClient client = this.tcpListener.AcceptTcpClient();
                    //create a thread to handle communication with connected client
                    Thread clientThread = new Thread(new ParameterizedThreadStart(HandleClientComm));
                    clientThread.Start(client);
                    if (exitFlag) break;
                }
            }
            catch (SocketException ex)
            {
                WriteLog("ListenForReferee():" + messages[1].ToString() + ex.Message, "ERROR");
            }
        }

        private void HandleClientComm(object client)
        {
            //try
            {
                TcpClient tcpClient = (TcpClient)client;
                techPanelStream = tcpClient.GetStream();

                byte[] message = new byte[4096];
                int bytesRead;

                while (true)
                {
                    if (exitFlag) break;
                    bytesRead = 0;

                    try
                    {
                        //blocks until a client sends a message
                        //WriteLog("Prima della lettura", "INFO");
                        bytesRead = techPanelStream.Read(message, 0, message.Length);
                        //WriteLog("Dopo la lettura: byte letti --> " + message.Length, "INFO");
                    }
                    catch (SocketException se)
                    {
                        //a socket error has occured
                        WriteLog("HandleClientComm() - SocketException: " + se.InnerException.Message + " Stack:" + se.InnerException.StackTrace, "ERROR");
                        ClearAllValues();
                        tp.ForeColor = Color.Red;
                        tp.FlatAppearance.BorderColor = Color.Red;
                        break;
                    }
                    catch (IOException se)
                    {
                        //a i/o error has occured
                        var socketExept = se.InnerException as SocketException;
                        //if (socketExept == null || socketExept.ErrorCode != 10060)
                        //    // if it's not the "expected" exception, let's not hide the error
                        //    throw se;
                        //// if it is the receive timeout, then reading ended
                        ////bytesRead = 0;

                        if (socketExept == null || socketExept.ErrorCode != 10054)
                            WriteLog("HandleClientComm() - IOException: " + socketExept.Data + " ErrorCode:" + socketExept.ErrorCode, "ERROR");
                        break;
                    }

                    if (bytesRead == 0)
                    {
                        WriteLog("HandleClientComm() - bytesRead=0: " + messages[3].ToString(), "ERROR");
                        break;
                    }

                    // verifico il messaggio del server...
                    ParseElementReceived(encoder.GetString(message, 0, bytesRead));
                    Thread.Sleep(200);
                }
                tcpClient.Close();
            }
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.InnerException.Message);
            //}
        }
        */

        // Riempi tabella elementi in base ai valori ricevuti dal pannello tecnico
        public void ParseElementReceived(string element)
        {
            try
            {
                WriteLog("TechPanel --> " + element, "");

                // Gestione LOGIN 
                if (element.StartsWith("Login") && element.Substring(6, 2).Equals("KO"))
                {
                    SetJudgeButton(Color.Yellow, true, Properties.Resources.Message11, "WARNING");
                    judgeConnected = false;
                    return;
                }
                else if (element.StartsWith("Login") && element.Substring(6, 2).Equals("OK"))
                {
                    SetJudgeButton(Color.Lime, true, Properties.Resources.Message12, "");
                    judgeConnected = true;
                    return;
                }

                XElement xmlElement = XElement.Parse(element);
                string localName = xmlElement.Name.LocalName;

                switch (localName)
                {
                    #region CONNECT         
                    case "CONNECT":
                        //quit.Enabled = false;
                        ltimer.Text = "00:00";
                        time = TimeSpan.Zero;
                        ltimer.ForeColor = Color.White;
                        tp.ForeColor = Color.Lime;
                        tp.FlatAppearance.BorderColor = Color.Lime;
                        settings.Visible = false;
                        return;
                    #endregion

                    #region DISCONNECT
                    case "DISCONNECT":
                        quit.Enabled = true;
                        ltimer.Text = "00:00";
                        time = TimeSpan.Zero;
                        ltimer.ForeColor = Color.White;
                        tp.ForeColor = Color.Red;
                        tp.FlatAppearance.BorderColor = Color.Red;
                        ClearAllValues();
                        settings.Visible = true;
                        return;
                    #endregion

                    #region START
                    case "START":
                        ClearAllValues();
                        splitContainer1.Enabled = true;
                        tp.ForeColor = Color.Lime;
                        tp.FlatAppearance.BorderColor = Color.Lime;
                        idGaraParams = int.Parse(xmlElement.Attribute("Gara").Value);
                        idSegment = int.Parse(xmlElement.Attribute("Segment").Value);
                        
                        // **** 1.0.4 Modifica del 27/06/2018 **** //
                        // Ricevo anche la categoria e la disciplina
                        idCategory = int.Parse(xmlElement.Attribute("Category").Value);
                        idDiscipline = int.Parse(xmlElement.Attribute("Discipline").Value);

                        numPart = int.Parse(xmlElement.Attribute("Partecipante").Value);
                        buttonNext.Text = "Competitor: " + numPart + "";
                        WriteLog("*****************************************************************************", "");
                        WriteLog("Event = " + idGaraParams + ", Segment = " + idSegment + ", Discipline = " + idDiscipline + ", Category = " + idCategory +
                               ", Skater = " + xmlElement.Attribute("Partecipante").Value, "");
                        WriteLog("*****************************************************************************", "");

                        CheckCategoryAndDiscipline();
                        
                        //CreoFileSegmento();
                        ltimer.Text = "00:00";
                        falls.Text = "";
                        return;
                    #endregion

                    #region RESUME
                    case "RESUME":
                        splitContainer1.Enabled = true;
                        tp.ForeColor = Color.Lime;
                        tp.FlatAppearance.BorderColor = Color.Lime;
                        idGaraParams = int.Parse(xmlElement.Attribute("Gara").Value);
                        idSegment = int.Parse(xmlElement.Attribute("Segment").Value);

                        // **** 1.0.4 Modifica del 27/06/2018 **** //
                        // Ricevo anche la categoria e la disciplina
                        idCategory = int.Parse(xmlElement.Attribute("Category").Value);
                        idDiscipline = int.Parse(xmlElement.Attribute("Discipline").Value);

                        numPart = int.Parse(xmlElement.Attribute("Partecipante").Value);
                        buttonNext.Text = "Competitor: " + numPart + "";
                        WriteLog("*****************************************************************************", "");
                        WriteLog("Event = " + idGaraParams + ", Segment = " + idSegment + ", Discipline = " + idDiscipline + ", Category = " + idCategory +
                               ", Skater = " + xmlElement.Attribute("Partecipante").Value, "");
                        WriteLog("*****************************************************************************", "");

                        // verifico se fare rollback
                        if (rollbackFile.Equals("")) return;
                        else
                        {
                            DoRollback(rollbackFile);
                        }
                        return;
                    #endregion

                    #region DEDUCTIONS
                    case "DEDUCTIONS":
                        if (int.Parse(xmlElement.Attribute("ded1").Value) != 0)
                        {
                            falls.Text = "Falls: " + (int.Parse(xmlElement.Attribute("ded1").Value));
                            //falls.Visible = true;
                        }
                        
                        //ded1.Value = decimal.Parse(xmlElement.Attribute("ded1").Value);
                        //ded2.Value = decimal.Parse(xmlElement.Attribute("ded2").Value);
                        //ded3.Value = decimal.Parse(xmlElement.Attribute("ded3").Value);
                        //ded4.Value = decimal.Parse(xmlElement.Attribute("ded4").Value);
                        //ded5.Value = decimal.Parse(xmlElement.Attribute("ded5").Value);
                        return;
                    #endregion

                    #region ANNULLA
                    case "ANNULLA":
                        lv.Items[lv.Items.Count - 1].Remove();
                        currentElem--;
                        return;
                    #endregion

                    #region PING
                    case "PING":
                        byte[] byOK = Encoding.ASCII.GetBytes("PING Judge " + numCurrentJudge + " OK");
                        if (techPanelStream != null)
                        {
                            techPanelStream.Write(byOK, 0, byOK.Length);
                            techPanelStream.Flush();
                        }
                        //tp.ForeColor = Color.Lime;
                        //tp.FlatAppearance.BorderColor = Color.Lime;
                        return;
                    #endregion

                    #region update
                    case "element":
                        string attrNum = xmlElement.Attribute("NUM").Value;
                        string attrName = xmlElement.Attribute("NAME").Value;
                        string attrDesc = xmlElement.Attribute("DESC").Value;
                        string attrId = xmlElement.Attribute("ID").Value;
                        string attrPen = xmlElement.Attribute("PENALTY").Value;
                        string attrUpd = xmlElement.Attribute("UPDATE").Value;
                        string attrNumCombo = xmlElement.Attribute("NUMCOMBO").Value;
                        string attrNull = xmlElement.Attribute("NULL").Value;

                        if (attrUpd.Equals("T")) // modifica elemento
                        {
                            int indexLv = 0; // indice dell'elemento da modificare
                            for (int i = 0; i < lv.Items.Count; i++)
                            {
                                if (lv.Items[i].SubItems[5].Text.Equals(attrId))
                                    indexLv = i;
                            }
                            // DESC
                            lv.Items[indexLv].SubItems[1].Text = attrDesc;
                            //if (attrName.Contains("*")) lv.Items[currentElem].SubItems.Add(attrDesc + "*");
                            //else lv.Items[currentElem].SubItems.Add(attrDesc); 

                            // PENALTY
                            if (attrPen.Equals("D")) lv.Items[indexLv].SubItems[2].Text = "<<<"; // downgraded
                            else if (attrPen.Equals("H")) lv.Items[indexLv].SubItems[2].Text = "<<";// halfrotated
                            else if (attrPen.Equals("U")) lv.Items[indexLv].SubItems[2].Text = "<"; // underrotated
                            else lv.Items[indexLv].SubItems[2].Text = "";

                            // QOE
                            lv.Items[indexLv].SubItems[3].Text = "";

                            // ELEMENT NAME - non visibile
                            lv.Items[indexLv].SubItems[4].Text = attrName;
                            lv.Items[indexLv].ToolTipText = attrName;

                            lv.Items[indexLv].BackColor = Color.Black;
                            lv.Items[indexLv].ForeColor = Color.Yellow;

                            if (attrNull.Equals("T")) //se l'elemento è nullo o no level ('T')
                            {
                                lv.Items[indexLv].SubItems[3].Text = "0";
                                lv.Items[indexLv].ForeColor = Color.LightGray;
                                lv.Items[indexLv].SubItems[1].ForeColor = Color.LightGray;
                                lv.Items[indexLv].SubItems[2].ForeColor = Color.LightGray;
                                lv.Items[indexLv].SubItems[3].ForeColor = Color.LightGray;
                                lv.Items[indexLv].SubItems[1].BackColor = Color.Black;
                                lv.Items[indexLv].Selected = false;
                            }
                            else
                            {
                                lv.Items[indexLv].UseItemStyleForSubItems = false;
                                lv.Items[indexLv].SubItems[1].BackColor = Color.Red;
                                lv.Items[indexLv].SubItems[1].ForeColor = Color.Yellow;
                            }
                        }
                        #endregion

                    #region insert element
                        else // inserimento nuovo elemento
                        {
                            // NUM
                            if (attrNum.Equals("")) lv.Items.Add(attrNum);
                            else lv.Items.Add(attrNum + ".");

                            // DESC
                            lv.Items[currentElem].SubItems.Add(attrDesc);
                            //if (attrName.Contains("*")) lv.Items[currentElem].SubItems.Add(attrDesc + "*");
                            //else lv.Items[currentElem].SubItems.Add(attrDesc); 

                            // PENALTY
                            if (attrPen.Equals("D")) lv.Items[currentElem].SubItems.Add("<<<"); // downgraded
                            else if (attrPen.Equals("H")) lv.Items[currentElem].SubItems.Add("<<"); // halfrotated
                            else if (attrPen.Equals("U")) lv.Items[currentElem].SubItems.Add("<"); // underrotated
                            else lv.Items[currentElem].SubItems.Add("");

                            //if (attrName.Contains("*"))

                            // QOE
                            lv.Items[currentElem].SubItems.Add("");

                            // ELEMENT NAME - non visibile
                            lv.Items[currentElem].SubItems.Add(attrName);
                            lv.Items[currentElem].ToolTipText = attrName;

                            // ID
                            lv.Items[currentElem].SubItems.Add(attrId);

                            lv.Items[currentElem].ForeColor = Color.Yellow;
                            lv.Items[currentElem].SubItems[1].BackColor = Color.Black;
                            lv.Items[currentElem].BackColor = Color.Black;

                            if (attrNull.Equals("T")) //se l'elemento è nullo o no level ('T')
                            {
                                lv.Items[currentElem].SubItems[3].Text = "0";
                                lv.Items[currentElem].ForeColor = Color.LightGray;
                                lv.Items[currentElem].SubItems[1].ForeColor = Color.LightGray;
                                lv.Items[currentElem].SubItems[2].ForeColor = Color.LightGray;
                                lv.Items[currentElem].SubItems[3].ForeColor = Color.LightGray;
                                lv.Items[currentElem].SubItems[1].BackColor = Color.Black;
                                lv.Items[currentElem].Selected = false;
                            }

                            // modifica del 26/05/2017
                            //if (currentElem == 0) lv.Items[currentElem].Selected = true;
                            //else
                            //{
                            //    int precElement = currentElem - 1;
                            //    // se all'elemento precedente è stato assegnato già un qoe seleziono il successivo
                            //    // altrimenti rimango su quello corrente
                            //    if (!lv.Items[precElement].SubItems[3].Text.Equals("") &&
                            //         lv.Items[precElement].ForeColor != Color.LightGray) 
                            //        lv.Items[currentElem].Selected = true;
                            //}

                            // modifica del 5-12-2017
                            lv.Select();
                            foreach (ListViewItem lvi in lv.Items)
                            {
                                string qoe = lvi.SubItems[3].Text;
                                int indiceEl = lvi.Index + 1;
                                if (lvi.ForeColor != System.Drawing.Color.LightGray &&
                                    lvi.SubItems[3].Text.Equals("")) // selezionabile
                                {
                                    lvi.Selected = true;
                                    lvi.Focused = true;
                                    lvi.EnsureVisible();
                                    break;
                                }
                            }

                            currentElem++;
                        }
                        return;
                    #endregion

                    #region CONFIRM, CLEARCONFIRM
                    case "CONFIRM":
                        sendMarks.Enabled = true;
                        ltimer.Text = "00:00";
                        ltimer.ForeColor = Color.White;
                        time = TimeSpan.Zero;
                        timer1.Enabled = true;
                        //timer1.Stop();
                        //timer1.Start();
                        // abilito o riabilito la listview con gli elementi e i components
                        splitContainer1.Enabled = true;
                        gbComponents.Enabled = true;
                        return;
                    case "CLEARCONFIRM":
                        sendMarks.Enabled = false;
                        ltimer.Text = "00:00";
                        ltimer.ForeColor = Color.White;
                        time = TimeSpan.Zero;
                        //timer1.Enabled = false;
                        splitContainer1.Enabled = true;
                        gbComponents.Enabled = true;
                        flowLayoutPanel2.Enabled = true;
                        return;
                    #endregion

                    #region QUIT
                    case "QUIT":
                        Process.Start(Application.ExecutablePath);
                        Process.GetCurrentProcess().Kill();
                        return;
                    #endregion

                    #region AVERAGE, Components, HURRY UP!
                    case "AVERAGE":
                        Thread.Sleep(1000);
                        sendComponents.Enabled = true;
                        clear.Enabled = false;
                        //pb1.Visible = true;
                        return;

                    case "COMPONENTS":
                        flagAverage = true;

                        arDecComp[0] = Decimal.Parse(xmlElement.Attribute("comp1").Value,
                            NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.InvariantCulture);
                        //arDecComp[0] = decimal.Parse(xmlElement.Attribute("comp1").Value);
                        labss.Text = xmlElement.Attribute("comp1").Value.Replace(',', '.');
                        labss.BackColor = Color.Cyan;

                        arDecComp[1] = Decimal.Parse(xmlElement.Attribute("comp2").Value,
                            NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.InvariantCulture);
                        //arDecComp[1] = decimal.Parse(xmlElement.Attribute("comp2").Value);
                        labtr.Text = xmlElement.Attribute("comp2").Value.Replace(',', '.');
                        labtr.BackColor = Color.Cyan;

                        arDecComp[2] = Decimal.Parse(xmlElement.Attribute("comp3").Value,
                            NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.InvariantCulture);
                        //arDecComp[2] = decimal.Parse(xmlElement.Attribute("comp3").Value)
                        labpe.Text = xmlElement.Attribute("comp3").Value.Replace(',', '.');
                        labpe.BackColor = Color.Cyan;

                        arDecComp[3] = Decimal.Parse(xmlElement.Attribute("comp4").Value,
                            NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.InvariantCulture);
                        //arDecComp[3] = decimal.Parse(xmlElement.Attribute("comp4").Value);
                        labco.Text = xmlElement.Attribute("comp4").Value.Replace(',', '.');
                        labco.BackColor = Color.Cyan;

                        return;

                        //***** Modifica del 08/06/2018
                    case "HURRYUP":
                        // Invio un Suono
                        System.Media.SystemSounds.Asterisk.Play();
                        //scroll.Text = RollartSystemJudge.Properties.Resources.Message15;
                        return;

                        #endregion
                }

            }
            catch (Exception ex)
            {
                WriteLog("ParseElementReceived():" + ex.InnerException, "ERROR");
            }
        }

        #endregion

        #region Confirm
        // Invia QOE e al pannello tecnico
        private void button16_Click(object sender, EventArgs e)
        {
            // invio qoe, components al pannello tecnico (pres giuria)
            // verifico se sono stati assegnati tutti i punteggi e in caso positivo invio il punteggio al 
            // pannello tecnico
            try
            {
                notready = false;
                string log = "TechPanel <-- <ELEMENTS = ";
                string segmentXML = "<SEGMENT Gara='" + idGaraParams +
                    "' Segment = '" + idSegment + "' Partecipante = '" + numPart +
                    "' numCurrentJudge = '" + numCurrentJudge + "'>";
                elementsXML = "<ELEMENTS>";
                string pen = "";
                for (int i = 0; i < lv.Items.Count; i++)
                {
                    if (lv.Items[i].SubItems[2].Text.Equals("<")) pen = "U";
                    else if (lv.Items[i].SubItems[2].Text.Equals("<<")) pen = "H";
                    else if (lv.Items[i].SubItems[2].Text.Equals("<<<")) pen = "D";
                    else pen = "";
                    elementsXML += "<ELEMENT NUM = '" + lv.Items[i].SubItems[0].Text.Replace(".", "") + "'" + // Num elemento
                        " NAME = '" + lv.Items[i].SubItems[4].Text.Replace("<", "") + "'" +  // codice elemento
                        " QOE = '" + lv.Items[i].SubItems[3].Text + "'" +                    //qoe
                        " PENALTY = '" + pen + "'" +
                        " ID = '" + lv.Items[i].SubItems[5].Text + "'/>"; // --> modifica del 13/05/2017
                                                                          //" ID = '" + (i + 1) + "'/>";
                    log += "'" + lv.Items[i].SubItems[3].Text + "';";
                    // verifico se è stato assegnato il QOE
                    if (lv.Items[i].SubItems[3].Text.Equals(""))
                    {
                        //notready = true;
                        //lv.Items[i].BackColor = Color.Red;
                        lv.Items[i].Selected = true;
                        MessageBox.Show(Properties.Resources.Message3 + lv.Items[i].SubItems[0].Text.Replace(".", "")
                            , "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                // verifico se sono stati assegnati tutti i components
                if (idSegment != 11 && idSegment != 12)
                //if (idSegment != 3)
                {
                    if (labss.Text.Length == 0 || labtr.Text.Length == 0 ||
                        labpe.Text.Length == 0 || labco.Text.Length == 0)
                    {
                        MessageBox.Show(Properties.Resources.Message5
                                , "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                } else
                {
                    if (labss.Text.Length == 0 || labpe.Text.Length == 0)
                    {
                        MessageBox.Show(Properties.Resources.Message5
                                , "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                // verifico se in caso di average, il giudice si è discostato di 0,50 in più
                // o in meno rispetto alla media
                // Se False esco
                if (flagAverage)
                    if (!CheckMedia()) return;

                elementsXML += "</ELEMENTS>";
                segmentXML += elementsXML;
                compXML = "<COMPONENTS comp1 = '" + labss.Text + "'" + // skating skill
                    " comp2 = '" + labtr.Text + "'" + // transitions
                    " comp3 = '" + labpe.Text + "'" + // perf
                    " comp4 = '" + labco.Text + "'" + // choreo
                    " comp5 = '" + labin.Text + "'/>"; // interpretation
                segmentXML = segmentXML + compXML + "</SEGMENT>";

                //timer1.Enabled = false;

                // Costruisco il log
                log += " Components = '" +
                       labss.Text + "';'" +
                       labtr.Text + "';'" +
                       labpe.Text + "';'" +
                       labco.Text + "';'";
                log += " Time = " + ltimer.Text + "/>";

                WriteLog(log, "");
                //InsertLineInFile();

                // nuovo metodo di invio dati al server
                InvioDatiAlServer(segmentXML, false);

                //timer1.Stop();
                ltimer.ForeColor = Color.White;
                ltimer.Text = "00:00";
                sendMarks.FlatAppearance.BorderColor = Color.Black;
                sendMarks.ForeColor = Color.Lime;

                //UpdateRollbackSettings("");

                flowLayoutPanel2.Enabled = false;
                gbComponents.Enabled = false;
                sendMarks.Enabled = false;
                flagAverage = false;
                comp1.PerformClick();

                return;
            }
            catch (Exception ex)
            {
                WriteLog("Confirm():" + ex.StackTrace, "ERROR");
                ClearAllValues();
                return;
            }
        }
        #endregion
        
        #region QOE
        private void AssegnaQOE(string qoe)
        {
            try
            {
                if (lv.SelectedItems.Count == 0) return;

                lv.SelectedItems[0].SubItems[3].Text = qoe;
                lv.SelectedItems[0].ForeColor = Color.GreenYellow;
                lv.SelectedItems[0].SubItems[1].ForeColor = Color.GreenYellow;
                lv.SelectedItems[0].SubItems[2].ForeColor = Color.GreenYellow;
                lv.SelectedItems[0].SubItems[3].ForeColor = Color.GreenYellow;
                lv.SelectedItems[0].SubItems[1].BackColor = Color.Black;


                int count = 0;
                int indexSelected = lv.SelectedItems[0].Index;
                foreach (ListViewItem lvi in lv.Items)
                {
                    if (lvi.Index >= indexSelected)
                    {
                        if (lvi.ForeColor != System.Drawing.Color.LightGray) // selezionabile
                        {
                            count++;
                            lvi.Selected = true;
                            lvi.Focused = true;
                            lvi.EnsureVisible();
                            if (lvi.SubItems[3].Text.Equals("") || count == 2) return;
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                WriteLog("AssegnaQOE():" + ex.StackTrace, "ERROR");
            }
        }

        private void lv_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (lv.SelectedItems.Count == 0) return;
                int index = lv.SelectedItems[0].Index;
                int count = lv.Items.Count;
                if (lv.SelectedItems[0].ForeColor == System.Drawing.Color.LightGray)
                {
                    if (index == 0)
                    {
                        lv.Items[0].Selected = false;
                        return;
                    }
                    index++;
                    if (index < count) lv.Items[index].Selected = true;
                    if (index == count) lv.Items[0].Selected = true;
                }
            }
            catch (Exception ex)
            {
                WriteLog("lv_SelectedIndexChanged():" + ex.StackTrace, "ERROR");
            }
            
        }

        private void button6_Click(object sender, EventArgs e)
        {
            AssegnaQOE("-3");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AssegnaQOE("-2");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AssegnaQOE("-1");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AssegnaQOE("0");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            AssegnaQOE("+1");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            AssegnaQOE("+2");
        }

        private void button12_Click(object sender, EventArgs e)
        {
            AssegnaQOE("+3");
        }
        #endregion

        #region COMPONENTS

        public void DrawComponents(int num, int a, int b, int c, int d)
        {

            gbComponents.Enabled = true;
            gbComponents.Tag = (this.Controls.Find("comp" + num, true)[0]).Tag;

            Button btn = this.Controls.Find("comp" + num, true)[0] as Button;
            btn.FlatAppearance.BorderColor = Color.Red;
            etichetta.Text = btn.Text;

            btn = this.Controls.Find("comp" + a, true)[0] as Button;
            btn.FlatAppearance.BorderColor = Color.Silver;
            btn = this.Controls.Find("comp" + b, true)[0] as Button;
            btn.FlatAppearance.BorderColor = Color.Silver;
            btn = this.Controls.Find("comp" + c, true)[0] as Button;
            btn.FlatAppearance.BorderColor = Color.Silver;

            panelDecine.Enabled = false; hideDecine.Visible = true;
            panelUnita.Enabled = true; hideUnita.Visible = false;

            this.Controls.Find("pb" + num, true)[0].Visible = true;
            this.Controls.Find("pb" + a, true)[0].Visible = false;
            this.Controls.Find("pb" + b, true)[0].Visible = false;
            this.Controls.Find("pb" + c, true)[0].Visible = false;
        }

        private bool CheckMedia()
        {
            comp1.FlatAppearance.BorderColor = Color.Silver;
            comp2.FlatAppearance.BorderColor = Color.Silver;
            comp3.FlatAppearance.BorderColor = Color.Silver;
            comp4.FlatAppearance.BorderColor = Color.Silver;
            int flagOutOfRange = 0;
            string stMessage = "";

            decimal dec1 = Decimal.Parse(labss.Text,
                                        NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.InvariantCulture);
            decimal dec2 = Decimal.Parse(labtr.Text,
                                        NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.InvariantCulture);
            decimal dec3 = Decimal.Parse(labpe.Text,
                                        NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.InvariantCulture);
            decimal dec4 = Decimal.Parse(labco.Text,
                                        NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.InvariantCulture);

            if (dec1 - arDecComp[0] > 0.50M || arDecComp[0] - dec1 > 0.50M)
            {
                comp1.FlatAppearance.BorderColor = Color.Red;
                flagOutOfRange = 1;
                stMessage += "\r\n - " + Properties.Settings.Default.Component1 + ": " + arDecComp[0];
            }
            if (dec2 - arDecComp[1] > 0.50M || arDecComp[1] - dec2 > 0.50M)
            {
                comp2.FlatAppearance.BorderColor = Color.Red;
                flagOutOfRange = 2;
                stMessage += "\r\n - " + Properties.Settings.Default.Component2 + ": " + arDecComp[1];
            }
            if (dec3 - arDecComp[2] > 0.50M || arDecComp[2] - dec3 > 0.50M)
            {
                comp3.FlatAppearance.BorderColor = Color.Red;
                flagOutOfRange = 3;
                stMessage += "\r\n - " + Properties.Settings.Default.Component3 + ": " + arDecComp[2];
            }
            if (dec4 - arDecComp[3] > 0.50M || arDecComp[3] - dec4 > 0.50M)
            {
                comp4.FlatAppearance.BorderColor = Color.Red;
                flagOutOfRange = 4;
                stMessage += "\r\n - " + Properties.Settings.Default.Component4 + ": " + arDecComp[3];
            }

            if (flagOutOfRange > 0)
            {
                MessageBox.Show(Properties.Resources.Message6 + "\r\n" + stMessage, "Warning",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else return true;
        }

        public void CheckCategoryAndDiscipline()
        {
            button19.Enabled = true; button19.BackColor = Color.LimeGreen;// 10
            button17.Enabled = true; button17.BackColor = Color.Lime;// 9
            button18.Enabled = true; button18.BackColor = Color.LightGreen;// 8
            switch (idCategory)
            {
                case 1: // cadet
                case 5: // tods
                case 6: // minis
                case 7: // espoir
                    button19.Enabled = false; button19.BackColor = Color.Gray;// 10
                    button17.Enabled = false; button17.BackColor = Color.Gray;// 9
                    button18.Enabled = false; button18.BackColor = Color.Gray;// 8
                    break;
                case 2: // youth
                    button19.Enabled = false; button19.BackColor = Color.Gray;// 10
                    button17.Enabled = false; button17.BackColor = Color.Gray;// 9
                    break;
                case 3: // junior
                    button19.Enabled = false; button19.BackColor = Color.Gray;// 10
                    break;
                default: // senior
                    break;
            }

            comp2.Enabled = true;
            pb2.Enabled = true;
            comp4.Enabled = true;
            pb4.Enabled = true;
            switch (idSegment)
            {
                case 11:
                case 12:
                case 3: // compulsory
                    comp2.Enabled = false;
                    pb2.Enabled = false;
                    comp4.Enabled = false;
                    pb4.Enabled = false;
                    break;
                default:
                    
                    break;
            }
        }

        // Invio i soli COMPONENTS per il calcolo della media
        private void sendComponents_Click(object sender, EventArgs e)
        {
            try
            {
                //////////////////////
                //if (idSegment != 3)
                if (idSegment != 11 && idSegment != 12)
                {
                    if (labss.Text.Length == 0 || labtr.Text.Length == 0 ||
                        labpe.Text.Length == 0 || labco.Text.Length == 0)
                    {
                        MessageBox.Show(Properties.Resources.Message7
                                , "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                else // obbliga
                {
                    if (labss.Text.Length == 0 || labpe.Text.Length == 0)
                    {
                        MessageBox.Show(Properties.Resources.Message7
                                , "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                string compXML = "<COMPONENTS numCurrentJudge = '" + numCurrentJudge + "' comp1 = '" + labss.Text + "'" + // skating skill
                    " comp2 = '" + labtr.Text + "'" + // transitions
                    " comp3 = '" + labpe.Text + "'" + // perf
                    " comp4 = '" + labco.Text + "'" + // choreo
                    "/>";

                InvioDatiAlServer(compXML, false);

                sendComponents.Enabled = false;
                sendComponents.FlatAppearance.BorderColor = Color.Black;
                sendComponents.ForeColor = Color.Cyan;

                comp1.PerformClick();
            }
            catch (Exception ex)
            {
                WriteLog("sendComponents_Click(): " + ex.StackTrace, "ERROR");
            }
        }

        // skating skill - tag 1
        private void comp1_Click(object sender, EventArgs e)
        {
            DrawComponents(1, 2, 3, 4, 5);
        }
        // transitions - tag 2
        private void button8_Click(object sender, EventArgs e)
        {
            DrawComponents(2, 1, 3, 4, 5);
        }
        // performance - tag 3
        private void button9_Click(object sender, EventArgs e)
        {
            DrawComponents(3, 2, 1, 4, 5);
        }
        // choreo - tag 4
        private void button11_Click(object sender, EventArgs e)
        {
            DrawComponents(4, 2, 3, 1, 5);
        }
        // clear
        private void clear_Click(object sender, EventArgs e)
        {
            switch (int.Parse(gbComponents.Tag.ToString()))
            {
                case 1: labss.Text = ""; break;
                case 2: labtr.Text = ""; break;
                case 3: labpe.Text = ""; break;
                case 4: labco.Text = ""; break;
                case 5: labin.Text = ""; break;
                default: break;
            }
            //confirm.Enabled = false;
        }

        private void SetComponents(int punteggio)
        {
            button21.Enabled = true; button21.BackColor = Color.FromArgb(255, 224, 192);// 25
            button22.Enabled = true; button22.BackColor = Color.Gold;// 50
            button23.Enabled = true; button23.BackColor = Color.FromArgb(255, 192, 128);// 75

            // se assegno Zero, inibisco lo 00
            if (punteggio == 0) button20.Enabled = false;
            else button20.Enabled = true;

            if (score.Text.Equals("")) score.Text = punteggio + ".00";
            else if (score.Text.Equals("10.00")) score.Text = punteggio + ".00";
            else score.Text = punteggio + "." + score.Text.Substring(2);

            panelDecine.Enabled = true; hideDecine.Visible = false;
            panelUnita.Enabled = false; hideUnita.Visible = true;

            switch (idCategory)
            {
                case 1: // cadet
                case 5: // tods
                case 6: // minis
                case 7: // espoir
                    if (punteggio == 7)
                    {
                        button21.Enabled = false; button21.BackColor = Color.Gray;// 25
                        button22.Enabled = false; button22.BackColor = Color.Gray;// 50
                        button23.Enabled = false; button23.BackColor = Color.Gray;// 75
                    }
                    break;
                case 2: // youth
                    if (punteggio == 8)
                    {
                        button21.Enabled = false; button21.BackColor = Color.Gray;// 25
                        button22.Enabled = false; button22.BackColor = Color.Gray;// 50
                        button23.Enabled = false; button23.BackColor = Color.Gray;// 75
                    }
                    break;
                case 3: // junior
                    if (punteggio == 9)
                    {
                        button21.Enabled = false; button21.BackColor = Color.Gray;// 25
                        button22.Enabled = false; button22.BackColor = Color.Gray;// 50
                        button23.Enabled = false; button23.BackColor = Color.Gray;// 75
                    }
                    if (punteggio == 10)
                    {
                        button21.Enabled = false; button21.BackColor = Color.Gray;// 25
                        button22.Enabled = false; button22.BackColor = Color.Gray;// 50
                        button23.Enabled = false; button23.BackColor = Color.Gray;// 75
                    }
                    break;
                default: // senior
                    break;
            }
        }

        private void button8_Click_1(object sender, EventArgs e)
        {
            SetComponents(0);
        }

        private void button9_Click_1(object sender, EventArgs e)
        {
            SetComponents(1);
        }

        private void button11_Click_1(object sender, EventArgs e)
        {
            SetComponents(2);
        }

        private void button10_Click_1(object sender, EventArgs e)
        {
            SetComponents(3);
        }

        private void button16_Click_1(object sender, EventArgs e)
        {
            SetComponents(4);
        }

        private void button15_Click_2(object sender, EventArgs e)
        {
            SetComponents(5);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            SetComponents(6);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            SetComponents(7);
        }

        private void button18_Click(object sender, EventArgs e)
        {
            SetComponents(8);
        }

        private void button17_Click(object sender, EventArgs e)
        {
            SetComponents(9);
        }

        private void button19_Click(object sender, EventArgs e)
        {
            ConfirmComponent("");
        }

        private void ConfirmComponent(string decine)
        {
            if (decine.Equals("")) score.Text = "10.00";
            else score.Text = score.Text.Substring(0, 2) + decine;
            switch (int.Parse(gbComponents.Tag.ToString()))
            {
                case 1:
                    labss.Text = score.Text;
                    labss.Visible = true;
                    labss.BackColor = Color.Chartreuse;
                    comp1.FlatAppearance.BorderColor = Color.Silver;
                    this.Controls.Find("pb1", true)[0].Visible = false;
                    if (comp2.Enabled) comp2.PerformClick();
                    else comp3.PerformClick();
                    break;
                case 2:
                    labtr.Text = score.Text;
                    labtr.Visible = true;
                    labtr.BackColor = Color.Chartreuse;
                    comp2.FlatAppearance.BorderColor = Color.Silver;
                    this.Controls.Find("pb2", true)[0].Visible = false;
                    comp3.PerformClick();
                    break;
                case 3:
                    labpe.Text = score.Text;
                    labpe.Visible = true;
                    labpe.BackColor = Color.Chartreuse;
                    comp3.FlatAppearance.BorderColor = Color.Silver;
                    this.Controls.Find("pb3", true)[0].Visible = false;
                    if (comp4.Enabled)
                    {
                        comp4.PerformClick();
                    } else // solo obbligatori danza
                    {
                        etichetta.Text = "";
                        if (sendMarks.Enabled) sendMarks.Focus();
                        else if (sendComponents.Enabled) sendComponents.Focus();
                        hideDecine.Visible = true;
                    }
                    break;
                case 4:
                    labco.Text = score.Text;
                    labco.Visible = true;
                    labco.BackColor = Color.Chartreuse;
                    comp4.FlatAppearance.BorderColor = Color.Silver;
                    this.Controls.Find("pb4", true)[0].Visible = false;
                    etichetta.Text = "";
                    if (sendMarks.Enabled) sendMarks.Focus();
                    else if (sendComponents.Enabled) sendComponents.Focus();
                    hideDecine.Visible = true;
                    break;
                default: break;

            }
            score.Refresh();
            score.Update();
            Thread.Sleep(200);

            panelDecine.Enabled = false; panelUnita.Enabled = true;
            score.Text = "";
        }

        private void button20_Click(object sender, EventArgs e)
        {
            ConfirmComponent("00");
        }

        private void FormJudge_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouse_offset.X, mouse_offset.Y);
                this.Location = mousePos; //move the form to the desired location
            }
        }

        private void FormJudge_MouseDown(object sender, MouseEventArgs e)
        {
            mouse_offset = new Point(-e.X, -e.Y);
        }

        private void button21_Click(object sender, EventArgs e)
        {
            ConfirmComponent("25");
        }

        private void button7_Click_2(object sender, EventArgs e)
        {
            clientSocket.Close();
        }

        // provo a ricollegarmi
        private void tp_Click(object sender, EventArgs e)
        {
            if (!judgeConnected)
            {
                NewConnectTechPanel();
                //Thread ctThread = new Thread(TryToConnect);
                //ctThread.Start();
            }
        }

        private void button23_Click(object sender, EventArgs e)
        {
            ConfirmComponent("50");
        }

        private void button22_Click(object sender, EventArgs e)
        {
            ConfirmComponent("75");
        }
        // ip tech
        private void button7_Click_3(object sender, EventArgs e)
        {
            try
            {
                ipForm ip = new ipForm();
                ip.ShowDialog();

                numCurrentJudge = Properties.Settings.Default.CurrentJudge;
                j1.Text = "Judge " + numCurrentJudge;
                tt.SetToolTip(j1, GetIP());
                log.Text = "IP Judge: " + GetIP();

                // recupero l'IP del pannello tecnico
                tt.SetToolTip(tp, Properties.Settings.Default.TechIP + ":" + Properties.Settings.Default.TechPort);
                log.Text += "\r\nIP Tech : " + Properties.Settings.Default.TechIP + ":" + Properties.Settings.Default.TechPort + "\r\n";
            }
            catch (Exception ex)
            {
                WriteLog("Settings: " + ex.Message, "ERROR");
            }
        }

        private void FormJudge_FormClosing(object sender, FormClosingEventArgs e)
        {
            // *** Modifica del 10/02/2019 - 2.0.0.10 *** //
            if (!flagClose)
                e.Cancel = true;
        }

        private void clear_Click_1(object sender, EventArgs e)
        {
            comp1.FlatAppearance.BorderColor = Color.Silver;
            comp2.FlatAppearance.BorderColor = Color.Silver;
            comp3.FlatAppearance.BorderColor = Color.Silver;
            comp4.FlatAppearance.BorderColor = Color.Silver;

            labss.Text = ""; labss.Visible = false;
            labtr.Text = ""; labtr.Visible = false;
            labpe.Text = ""; labpe.Visible = false;
            labco.Text = ""; labco.Visible = false;

            etichetta.Text = "";
            score.Text = "";
            comp1.PerformClick();
        }
        #endregion

        #region ROLLBACK
        public void UpdateRollbackSettings(string newValue)
        {
            try
            {
                // aggiorno il file settings.xml
                XmlNode node = xml.SelectNodes("/Settings/Judge").Item(0);
                node.Attributes[3].Value = newValue;
                xml.Save("settingJudge.xml");
            }
            catch (XmlException ex)
            {
                WriteLog("UpdateRollbackSettings(): " + ex.StackTrace, "ERROR");
            }
        }

        public void DoRollback(string file)
        {
            try
            {
                string[] temp = null;
                ListViewItem lvi = null;
                int indexLv = 0;
                using (StreamReader reader = new StreamReader(file))
                {
                    while (reader.Peek() >= 0)
                    {
                        string line = reader.ReadLine();
                        if (line.StartsWith("comp"))
                        {
                            temp = line.Split('=');
                            if (line.StartsWith("comp1")) labss.Text = temp[1];
                            if (line.StartsWith("comp2")) labtr.Text = temp[1];
                            if (line.StartsWith("comp3")) labpe.Text = temp[1];
                            if (line.StartsWith("comp4")) labco.Text = temp[1];
                            if (line.StartsWith("comp5")) labin.Text = temp[1];
                        }
                        else
                        {
                            temp = line.Split(';');
                            if (temp[0].Equals("")) temp[0] = " ";
                            lvi = new ListViewItem(temp);
                            lv.Items.Add(lvi);

                            indexLv++;
                        }
                    }
                }

                foreach (ListViewItem item in lv.Items)
                {
                    if (!item.SubItems[3].Text.Equals(""))
                        item.ForeColor = Color.GreenYellow;
                    if (item.SubItems[4].Text.Equals("NJ") ||
                        item.SubItems[4].Text.Equals("NS"))
                        item.ForeColor = Color.LightGray;
                    //if (item.SubItems[2].Text.Equals("<<"))
                    //    item.ForeColor = Color.LightGray;

                }
                splitContainer1.Enabled = true;
            }
            catch (Exception ex)
            {
                WriteLog("DoRollback(): " + ex.StackTrace, "ERROR");
            }
        }
        #endregion

        #region GESTIONE FINESTRA
        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (sendMarks.Enabled)
                {
                    time = time.Add(TimeSpan.FromMilliseconds(1000));
                    ltimer.Text = string.Format("{0:00}:{1:00}", time.Minutes,
                        time.Seconds);
                    //if (ltimer.Text.Equals("01:00"))
                    if (time.Seconds > 30) // quando supero i 30 secondi
                    {
                        ltimer.ForeColor = Color.Yellow;
                        if (IsOdd(time.Seconds))
                        {
                            sendMarks.ForeColor = Color.Yellow;
                            sendMarks.FlatAppearance.BorderColor = Color.Yellow;
                        }
                        else
                        {
                            sendMarks.ForeColor = Color.Lime;
                            sendMarks.FlatAppearance.BorderColor = Color.Lime;
                        }

                        } else if (time.Minutes > 0) // quando supero il minuto
                    {
                        ltimer.ForeColor = Color.Red;
                        if (IsOdd(time.Seconds))
                        {
                            sendMarks.ForeColor = Color.Red;
                            sendMarks.FlatAppearance.BorderColor = Color.Red;
                            // Suono
                            System.Media.SystemSounds.Asterisk.Play();
                        }
                        else
                        {
                            sendMarks.ForeColor = Color.Yellow;
                            sendMarks.FlatAppearance.BorderColor = Color.Yellow;
                        }
                    }
                }

                //*** Modifica del 31/05/2018 
                RefreshStatus();
            }
            catch (Exception ex)
            {
                WriteLog("timer1_Tick():" + ex.StackTrace, "ERROR");
            }
        }

        public static bool IsOdd(int value)
        {
            return value % 2 != 0;
        }

        public void ClearAllValues()
        {
            lv.Items.Clear();
            sendMarks.Enabled = false;
            //confirm.Enabled = false;
            //ded1.Value = 0; ded2.Value = 0; ded3.Value = 0; ded4.Value = 0; ded5.Value = 0;
            flowLayoutPanel2.Enabled = true;
            labss.Text = ""; labtr.Text = ""; labpe.Text = ""; labco.Text = "";
            labss.Visible = false; labtr.Visible = false; labpe.Visible = false; labco.Visible = false;
            gbComponents.Enabled = true;
            comp1.BackColor = Color.Transparent;
            comp2.BackColor = Color.Transparent;
            comp3.BackColor = Color.Transparent;
            comp4.BackColor = Color.Transparent;
            comp1.ForeColor = Color.Yellow;
            comp2.ForeColor = Color.Yellow;
            comp3.ForeColor = Color.Yellow;
            comp4.ForeColor = Color.Yellow;
            comp1.Text = c1; comp2.Text = c2; comp3.Text = c3; comp4.Text = c4;
            panelDecine.Enabled = false; panelUnita.Enabled = true;
            currentElem = 0;
            score.Text = "";
            clear.Enabled = true;
            falls.Text = "";

        }

        // Quit Button
        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = MessageBox.Show(Properties.Resources.Message8, "Warning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                if (result == DialogResult.Yes)
                {
                    // *** Modifica del 10/02/2019 - 2.0.0.10 *** //
                    flagClose = true;
                    Process.GetCurrentProcess().Kill();
                }
            }
            catch (Exception ex)
            {
                WriteLog("Quit():" + ex.StackTrace, "QUIT");
            }

        }

        // Log Button
        private void logButton_Click(object sender, EventArgs e)
        {
            if (log.Visible) log.Visible = false;
            else log.Visible = true;
        }

        //public void InsertLineInFile()
        //{
        //    try
        //    {
        //        using (StreamWriter writer = new StreamWriter(currentFile))
        //        {
        //            foreach (ListViewItem lvi in lv.Items)
        //            {
        //                writer.WriteLine(
        //                        lvi.SubItems[0].Text + ";" +
        //                        lvi.SubItems[1].Text + ";" +
        //                        lvi.SubItems[2].Text + ";" +
        //                        lvi.SubItems[3].Text + ";" +
        //                        lvi.SubItems[4].Text + "");
        //            }
        //            writer.WriteLine("comp1=" + labss.Text);
        //            writer.WriteLine("comp2=" + labtr.Text);
        //            writer.WriteLine("comp3=" + labpe.Text);
        //            writer.WriteLine("comp4=" + labco.Text);
        //            writer.WriteLine("comp5=" + labin.Text);
        //        }
        //        Thread.Sleep(200);
        //    }
        //    catch (Exception ex)
        //    {
        //        WriteLog("InsertLineInFile(): " + ex.StackTrace, "ERROR");
        //        tp.ForeColor = Color.Lime;
        //        tp.FlatAppearance.BorderColor = Color.Lime;
        //    }
        //}

        #endregion

        # region Random Values
        // Gestione Random Valori
        private void button7_Click_1(object sender, EventArgs e)
        {
            try
            {
                Random ran = new Random();
                // elements
                foreach (ListViewItem item in lv.Items)
                {
                    if (item.ForeColor != Color.LightGray)
                        item.SubItems[3].Text = AssegnaRandomElements(ran.Next(0, 6));
                }

                // components
                //comp2.PerformClick();

                labss.Text = AssegnaRandomComps(ran.Next(1, 10), ran.Next(0, 3));
                labss.Visible = true;
                //comp3.PerformClick();

                labtr.Text = AssegnaRandomComps(ran.Next(1, 10), ran.Next(0, 3));
                labtr.Visible = true;
                //comp4.PerformClick();

                labpe.Text = AssegnaRandomComps(ran.Next(1, 10), ran.Next(0, 3));
                labpe.Visible = true;

                labco.Text = AssegnaRandomComps(ran.Next(1, 10), ran.Next(0, 3));
                labco.Visible = true;
            }
            catch (Exception ex)
            {
                WriteLog("Assign Random:" + ex.StackTrace, "ERROR");
            }
        }

        private string AssegnaRandomElements(int random)
        {
            string res = "";
            if (random == 0) res = "-3";
            else if (random == 1) res = "-2";
            else if (random == 2) res = "-1";
            else if (random == 3) res = "0";
            else if (random == 4) res = "+1";
            else if (random == 5) res = "+2";
            else if (random == 6) res = "+3";
            return res;
        }

        private string AssegnaRandomComps(int ranUnita, int ranDecine)
        {
            string res = "";
            if (ranDecine == 0) res = ranUnita + ".00";
            else if (ranDecine == 1) res = ranUnita + ".25";
            else if (ranDecine == 2) res = ranUnita + ".75";
            else if (ranDecine == 3) res = ranUnita + ".50";

            return res;
        }

        #endregion

        #region Battery Status
        //*** Aggiunta del 31/05/2018
        private void RefreshStatus()
        {
            PowerStatus power = SystemInformation.PowerStatus;
            // Show the main power status
            //switch (power.PowerLineStatus)
            //{
            //    case PowerLineStatus.Online:
            //        MainsPower.Checked = true;
            //        break;

            //    case PowerLineStatus.Offline:
            //        MainsPower.Checked = false;
            //        break;

            //    case PowerLineStatus.Unknown:
            //        MainsPower.CheckState = CheckState.Indeterminate;
            //        break;
            //}

            // Battery Remaining
            float secondsRemaining = power.BatteryLifePercent;
            //if (secondsRemaining >= 0)
            //{
            //    BatteryLifeRemaining.Text = string.Format("{0} min", secondsRemaining / 60) + "\r\n";
            //}
            //else
            //{
            //    BatteryLifeRemaining.Text = string.Empty;
            //}
            if (power.PowerLineStatus == PowerLineStatus.Offline)
            {
                
                // Power level
                int powerPercent = (int)(power.BatteryLifePercent * 100);
                BatteryLifeRemaining.Text = powerPercent + " %";
                if (powerPercent > 66)
                    BatteryIndicator.Image = RollartSystemJudge.Properties.Resources.battery_full;
                else if (powerPercent <= 66 && powerPercent > 33)
                    BatteryIndicator.Image = RollartSystemJudge.Properties.Resources.battery_half;
                else if (powerPercent <= 33)
                    BatteryIndicator.Image = RollartSystemJudge.Properties.Resources.battery_low;
                else if (powerPercent == 100)
                    BatteryIndicator.Image = RollartSystemJudge.Properties.Resources.battery_plug;

            }
            if (power.PowerLineStatus == PowerLineStatus.Online)
            {
                // Power level
                int powerPercent = (int)(power.BatteryLifePercent * 100);
                BatteryLifeRemaining.Text = powerPercent + " %";
                BatteryIndicator.Image = RollartSystemJudge.Properties.Resources.battery_plug;
            }
                // Battery Status

            }
        #endregion
    }

}

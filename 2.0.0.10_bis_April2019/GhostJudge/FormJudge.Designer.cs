﻿namespace RollartSystemJudge
{
    partial class FormJudge
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormJudge));
            this.panel1 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.j1 = new System.Windows.Forms.Button();
            this.tp = new System.Windows.Forms.Button();
            this.buttonNext = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.BatteryLifeRemaining = new System.Windows.Forms.Label();
            this.BatteryIndicator = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.log = new System.Windows.Forms.TextBox();
            this.random = new System.Windows.Forms.Button();
            this.settings = new System.Windows.Forms.Button();
            this.logButton = new System.Windows.Forms.Button();
            this.quit = new System.Windows.Forms.Button();
            this.sendMarks = new System.Windows.Forms.Button();
            this.ltimer = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lv = new System.Windows.Forms.ListView();
            this.num = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.desc = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pen = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.qoe = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.id = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.button6 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.gbComponents = new System.Windows.Forms.GroupBox();
            this.falls = new System.Windows.Forms.Label();
            this.clear = new System.Windows.Forms.Button();
            this.hideDecine = new System.Windows.Forms.Panel();
            this.hideUnita = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labco = new System.Windows.Forms.Label();
            this.labpe = new System.Windows.Forms.Label();
            this.pb4 = new System.Windows.Forms.PictureBox();
            this.pb3 = new System.Windows.Forms.PictureBox();
            this.pb2 = new System.Windows.Forms.PictureBox();
            this.pb1 = new System.Windows.Forms.PictureBox();
            this.labtr = new System.Windows.Forms.Label();
            this.labss = new System.Windows.Forms.Label();
            this.comp1 = new System.Windows.Forms.Button();
            this.comp2 = new System.Windows.Forms.Button();
            this.comp3 = new System.Windows.Forms.Button();
            this.comp4 = new System.Windows.Forms.Button();
            this.etichetta = new System.Windows.Forms.Label();
            this.score = new System.Windows.Forms.Label();
            this.panelDecine = new System.Windows.Forms.TableLayoutPanel();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.panelUnita = new System.Windows.Forms.TableLayoutPanel();
            this.button19 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.labin = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.sendComponents = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader27 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader28 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader29 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader30 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader31 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader32 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader33 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader34 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader35 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader36 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BatteryIndicator)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.gbComponents.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).BeginInit();
            this.panelDecine.SuspendLayout();
            this.panelUnita.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.flowLayoutPanel4);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1072, 38);
            this.panel1.TabIndex = 0;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BackColor = System.Drawing.Color.Black;
            this.flowLayoutPanel4.Controls.Add(this.j1);
            this.flowLayoutPanel4.Controls.Add(this.tp);
            this.flowLayoutPanel4.Controls.Add(this.buttonNext);
            this.flowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(541, 34);
            this.flowLayoutPanel4.TabIndex = 154;
            // 
            // j1
            // 
            this.j1.BackColor = System.Drawing.Color.Transparent;
            this.j1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j1.Cursor = System.Windows.Forms.Cursors.Default;
            this.j1.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.j1.FlatAppearance.BorderSize = 0;
            this.j1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j1.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j1.ForeColor = System.Drawing.Color.Lime;
            this.j1.Image = global::RollartSystemJudge.Properties.Resources.premium_support;
            this.j1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.j1.Location = new System.Drawing.Point(1, 1);
            this.j1.Margin = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.j1.Name = "j1";
            this.j1.Size = new System.Drawing.Size(121, 35);
            this.j1.TabIndex = 155;
            this.j1.Tag = "1";
            this.j1.Text = "Judge";
            this.j1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.j1.UseVisualStyleBackColor = false;
            // 
            // tp
            // 
            this.tp.BackColor = System.Drawing.Color.Transparent;
            this.tp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tp.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.tp.FlatAppearance.BorderSize = 0;
            this.tp.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.tp.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.tp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.tp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tp.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp.ForeColor = System.Drawing.Color.Red;
            this.tp.Image = global::RollartSystemJudge.Properties.Resources.ceo;
            this.tp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tp.Location = new System.Drawing.Point(123, 1);
            this.tp.Margin = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.tp.Name = "tp";
            this.tp.Size = new System.Drawing.Size(132, 35);
            this.tp.TabIndex = 156;
            this.tp.Tag = "3";
            this.tp.Text = "Tech Panel";
            this.tp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tp.UseVisualStyleBackColor = false;
            this.tp.Click += new System.EventHandler(this.tp_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.BackColor = System.Drawing.Color.Black;
            this.buttonNext.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonNext.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNext.ForeColor = System.Drawing.Color.Yellow;
            this.buttonNext.Location = new System.Drawing.Point(258, 0);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(207, 36);
            this.buttonNext.TabIndex = 159;
            this.buttonNext.Text = "Skater ";
            this.buttonNext.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.BatteryLifeRemaining);
            this.panel5.Controls.Add(this.BatteryIndicator);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(541, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(527, 34);
            this.panel5.TabIndex = 159;
            // 
            // BatteryLifeRemaining
            // 
            this.BatteryLifeRemaining.BackColor = System.Drawing.Color.Black;
            this.BatteryLifeRemaining.Dock = System.Windows.Forms.DockStyle.Right;
            this.BatteryLifeRemaining.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BatteryLifeRemaining.ForeColor = System.Drawing.Color.White;
            this.BatteryLifeRemaining.Location = new System.Drawing.Point(360, 0);
            this.BatteryLifeRemaining.Name = "BatteryLifeRemaining";
            this.BatteryLifeRemaining.Size = new System.Drawing.Size(118, 34);
            this.BatteryLifeRemaining.TabIndex = 161;
            this.BatteryLifeRemaining.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BatteryIndicator
            // 
            this.BatteryIndicator.BackColor = System.Drawing.Color.Black;
            this.BatteryIndicator.Dock = System.Windows.Forms.DockStyle.Right;
            this.BatteryIndicator.Image = global::RollartSystemJudge.Properties.Resources.battery_plug;
            this.BatteryIndicator.Location = new System.Drawing.Point(478, 0);
            this.BatteryIndicator.Name = "BatteryIndicator";
            this.BatteryIndicator.Size = new System.Drawing.Size(39, 34);
            this.BatteryIndicator.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BatteryIndicator.TabIndex = 160;
            this.BatteryIndicator.TabStop = false;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Dock = System.Windows.Forms.DockStyle.Right;
            this.label1.Location = new System.Drawing.Point(517, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(10, 34);
            this.label1.TabIndex = 159;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.log);
            this.panel2.Controls.Add(this.random);
            this.panel2.Controls.Add(this.settings);
            this.panel2.Controls.Add(this.logButton);
            this.panel2.Controls.Add(this.quit);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel2.Location = new System.Drawing.Point(0, 659);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1072, 50);
            this.panel2.TabIndex = 1;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label3);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(187, 46);
            this.panel7.TabIndex = 175;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Black;
            this.label3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label3.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(187, 32);
            this.label3.TabIndex = 175;
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // log
            // 
            this.log.BackColor = System.Drawing.Color.Black;
            this.log.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.log.Dock = System.Windows.Forms.DockStyle.Right;
            this.log.Font = new System.Drawing.Font("Courier New", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.log.ForeColor = System.Drawing.Color.White;
            this.log.Location = new System.Drawing.Point(228, 0);
            this.log.Multiline = true;
            this.log.Name = "log";
            this.log.ReadOnly = true;
            this.log.Size = new System.Drawing.Size(646, 46);
            this.log.TabIndex = 173;
            this.log.Visible = false;
            // 
            // random
            // 
            this.random.BackColor = System.Drawing.Color.Transparent;
            this.random.Cursor = System.Windows.Forms.Cursors.Hand;
            this.random.Dock = System.Windows.Forms.DockStyle.Right;
            this.random.FlatAppearance.BorderSize = 0;
            this.random.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Aqua;
            this.random.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.random.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.random.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.random.Location = new System.Drawing.Point(874, 0);
            this.random.Name = "random";
            this.random.Size = new System.Drawing.Size(24, 46);
            this.random.TabIndex = 172;
            this.random.UseVisualStyleBackColor = false;
            this.random.Click += new System.EventHandler(this.button7_Click_1);
            // 
            // settings
            // 
            this.settings.BackColor = System.Drawing.Color.Black;
            this.settings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.settings.Dock = System.Windows.Forms.DockStyle.Right;
            this.settings.FlatAppearance.BorderSize = 0;
            this.settings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.settings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.settings.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.settings.Image = global::RollartSystemJudge.Properties.Resources.settings;
            this.settings.Location = new System.Drawing.Point(898, 0);
            this.settings.Name = "settings";
            this.settings.Size = new System.Drawing.Size(74, 46);
            this.settings.TabIndex = 176;
            this.settings.Text = "Settings";
            this.settings.UseVisualStyleBackColor = false;
            this.settings.Click += new System.EventHandler(this.button7_Click_3);
            // 
            // logButton
            // 
            this.logButton.BackColor = System.Drawing.Color.Transparent;
            this.logButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.logButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.logButton.FlatAppearance.BorderSize = 0;
            this.logButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Navy;
            this.logButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.logButton.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.logButton.Location = new System.Drawing.Point(972, 0);
            this.logButton.Name = "logButton";
            this.logButton.Size = new System.Drawing.Size(47, 46);
            this.logButton.TabIndex = 170;
            this.logButton.Text = "Log";
            this.logButton.UseVisualStyleBackColor = false;
            this.logButton.Click += new System.EventHandler(this.logButton_Click);
            // 
            // quit
            // 
            this.quit.BackColor = System.Drawing.Color.Transparent;
            this.quit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.quit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.quit.Dock = System.Windows.Forms.DockStyle.Right;
            this.quit.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.quit.FlatAppearance.BorderSize = 0;
            this.quit.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.quit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.quit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.quit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.quit.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quit.ForeColor = System.Drawing.Color.Red;
            this.quit.Location = new System.Drawing.Point(1019, 0);
            this.quit.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.quit.Name = "quit";
            this.quit.Size = new System.Drawing.Size(49, 46);
            this.quit.TabIndex = 158;
            this.quit.Text = "Exit";
            this.quit.UseVisualStyleBackColor = false;
            this.quit.Click += new System.EventHandler(this.button7_Click);
            // 
            // sendMarks
            // 
            this.sendMarks.BackColor = System.Drawing.Color.Black;
            this.sendMarks.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sendMarks.Dock = System.Windows.Forms.DockStyle.Right;
            this.sendMarks.Enabled = false;
            this.sendMarks.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.sendMarks.FlatAppearance.BorderSize = 3;
            this.sendMarks.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.sendMarks.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.sendMarks.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sendMarks.Font = new System.Drawing.Font("Trebuchet MS", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendMarks.ForeColor = System.Drawing.Color.Lime;
            this.sendMarks.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.sendMarks.Location = new System.Drawing.Point(378, 0);
            this.sendMarks.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.sendMarks.Name = "sendMarks";
            this.sendMarks.Size = new System.Drawing.Size(225, 72);
            this.sendMarks.TabIndex = 0;
            this.sendMarks.Text = "SEND MARKS";
            this.sendMarks.UseVisualStyleBackColor = false;
            this.sendMarks.Click += new System.EventHandler(this.button16_Click);
            // 
            // ltimer
            // 
            this.ltimer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ltimer.BackColor = System.Drawing.Color.Transparent;
            this.ltimer.Cursor = System.Windows.Forms.Cursors.Default;
            this.ltimer.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.ltimer.FlatAppearance.BorderSize = 0;
            this.ltimer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ltimer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ltimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ltimer.Font = new System.Drawing.Font("Trebuchet MS", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ltimer.ForeColor = System.Drawing.Color.Yellow;
            this.ltimer.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.ltimer.Location = new System.Drawing.Point(275, 3);
            this.ltimer.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.ltimer.Name = "ltimer";
            this.ltimer.Size = new System.Drawing.Size(100, 64);
            this.ltimer.TabIndex = 160;
            this.ltimer.Text = "00:00";
            this.ltimer.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Black;
            this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label2.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(3, 519);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(457, 38);
            this.label2.TabIndex = 1;
            this.label2.Text = "\"<\": Under rotated  \"<<\": Half rotated  \"<<<\": Downgraded";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.splitContainer1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 38);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1072, 621);
            this.panel3.TabIndex = 2;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel2);
            this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel6);
            this.splitContainer1.Panel2.Controls.Add(this.panel4);
            this.splitContainer1.Size = new System.Drawing.Size(1072, 621);
            this.splitContainer1.SplitterDistance = 463;
            this.splitContainer1.TabIndex = 4;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.lv, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 93.17773F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.822262F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(463, 557);
            this.tableLayoutPanel2.TabIndex = 5;
            // 
            // lv
            // 
            this.lv.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.lv.BackColor = System.Drawing.Color.Black;
            this.lv.BackgroundImageTiled = true;
            this.lv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lv.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.num,
            this.desc,
            this.pen,
            this.qoe,
            this.name,
            this.id});
            this.lv.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lv.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv.ForeColor = System.Drawing.Color.Yellow;
            this.lv.FullRowSelect = true;
            this.lv.GridLines = true;
            this.lv.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lv.HideSelection = false;
            this.lv.Location = new System.Drawing.Point(3, 3);
            this.lv.MultiSelect = false;
            this.lv.Name = "lv";
            this.lv.ShowItemToolTips = true;
            this.lv.Size = new System.Drawing.Size(457, 513);
            this.lv.TabIndex = 0;
            this.lv.UseCompatibleStateImageBehavior = false;
            this.lv.View = System.Windows.Forms.View.Details;
            this.lv.SelectedIndexChanged += new System.EventHandler(this.lv_SelectedIndexChanged);
            // 
            // num
            // 
            this.num.Text = "#";
            this.num.Width = 43;
            // 
            // desc
            // 
            this.desc.Text = "Element";
            this.desc.Width = 258;
            // 
            // pen
            // 
            this.pen.Text = "Penalty";
            this.pen.Width = 92;
            // 
            // qoe
            // 
            this.qoe.Text = "QOE";
            this.qoe.Width = 63;
            // 
            // name
            // 
            this.name.Text = "code";
            this.name.Width = 0;
            // 
            // id
            // 
            this.id.Text = "id";
            this.id.Width = 0;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.BackColor = System.Drawing.Color.Black;
            this.flowLayoutPanel2.Controls.Add(this.button6);
            this.flowLayoutPanel2.Controls.Add(this.button1);
            this.flowLayoutPanel2.Controls.Add(this.button3);
            this.flowLayoutPanel2.Controls.Add(this.button2);
            this.flowLayoutPanel2.Controls.Add(this.button5);
            this.flowLayoutPanel2.Controls.Add(this.button4);
            this.flowLayoutPanel2.Controls.Add(this.button12);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 557);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(463, 64);
            this.flowLayoutPanel2.TabIndex = 4;
            // 
            // button6
            // 
            this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button6.BackColor = System.Drawing.Color.Transparent;
            this.button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.button6.FlatAppearance.BorderSize = 2;
            this.button6.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Maroon;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.Red;
            this.button6.Location = new System.Drawing.Point(3, 3);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(58, 58);
            this.button6.TabIndex = 0;
            this.button6.Text = "-3";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button1.FlatAppearance.BorderSize = 2;
            this.button1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button1.Location = new System.Drawing.Point(67, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(58, 58);
            this.button1.TabIndex = 1;
            this.button1.Text = "-2";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.BackColor = System.Drawing.Color.Transparent;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button3.FlatAppearance.BorderSize = 2;
            this.button3.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button3.Location = new System.Drawing.Point(131, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(58, 58);
            this.button3.TabIndex = 2;
            this.button3.Text = "-1";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button2.FlatAppearance.BorderSize = 2;
            this.button2.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gold;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button2.Location = new System.Drawing.Point(195, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(58, 58);
            this.button2.TabIndex = 3;
            this.button2.Text = "0";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button5.BackColor = System.Drawing.Color.Transparent;
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button5.FlatAppearance.BorderSize = 2;
            this.button5.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Blue;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button5.Location = new System.Drawing.Point(259, 3);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(58, 58);
            this.button5.TabIndex = 4;
            this.button5.Text = "+1";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.BackColor = System.Drawing.Color.Transparent;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
            this.button4.FlatAppearance.BorderSize = 2;
            this.button4.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Cyan;
            this.button4.Location = new System.Drawing.Point(323, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(58, 58);
            this.button4.TabIndex = 5;
            this.button4.Text = "+2";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button12
            // 
            this.button12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button12.AutoSize = true;
            this.button12.BackColor = System.Drawing.Color.Transparent;
            this.button12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button12.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button12.FlatAppearance.BorderSize = 2;
            this.button12.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = System.Drawing.Color.Lime;
            this.button12.Location = new System.Drawing.Point(387, 3);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(67, 58);
            this.button12.TabIndex = 6;
            this.button12.Text = "+3";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.gbComponents);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(605, 547);
            this.panel6.TabIndex = 6;
            // 
            // gbComponents
            // 
            this.gbComponents.BackColor = System.Drawing.Color.Transparent;
            this.gbComponents.Controls.Add(this.falls);
            this.gbComponents.Controls.Add(this.clear);
            this.gbComponents.Controls.Add(this.hideDecine);
            this.gbComponents.Controls.Add(this.hideUnita);
            this.gbComponents.Controls.Add(this.tableLayoutPanel1);
            this.gbComponents.Controls.Add(this.etichetta);
            this.gbComponents.Controls.Add(this.score);
            this.gbComponents.Controls.Add(this.panelDecine);
            this.gbComponents.Controls.Add(this.panelUnita);
            this.gbComponents.Controls.Add(this.labin);
            this.gbComponents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbComponents.Font = new System.Drawing.Font("Trebuchet MS", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbComponents.ForeColor = System.Drawing.Color.White;
            this.gbComponents.Location = new System.Drawing.Point(0, 0);
            this.gbComponents.Name = "gbComponents";
            this.gbComponents.Size = new System.Drawing.Size(605, 547);
            this.gbComponents.TabIndex = 6;
            this.gbComponents.TabStop = false;
            this.gbComponents.Tag = "0";
            this.gbComponents.Text = "Components";
            // 
            // falls
            // 
            this.falls.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.falls.BackColor = System.Drawing.Color.Transparent;
            this.falls.Font = new System.Drawing.Font("Trebuchet MS", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.falls.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.falls.Location = new System.Drawing.Point(419, 480);
            this.falls.Name = "falls";
            this.falls.Size = new System.Drawing.Size(178, 65);
            this.falls.TabIndex = 185;
            this.falls.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // clear
            // 
            this.clear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.clear.BackColor = System.Drawing.Color.Black;
            this.clear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.clear.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.clear.FlatAppearance.BorderSize = 2;
            this.clear.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.clear.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.clear.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clear.ForeColor = System.Drawing.Color.White;
            this.clear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.clear.Location = new System.Drawing.Point(436, 22);
            this.clear.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(163, 40);
            this.clear.TabIndex = 170;
            this.clear.Text = "Clear Components";
            this.clear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.clear.UseVisualStyleBackColor = false;
            this.clear.Click += new System.EventHandler(this.clear_Click_1);
            // 
            // hideDecine
            // 
            this.hideDecine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hideDecine.Location = new System.Drawing.Point(14, 295);
            this.hideDecine.Name = "hideDecine";
            this.hideDecine.Size = new System.Drawing.Size(571, 50);
            this.hideDecine.TabIndex = 178;
            // 
            // hideUnita
            // 
            this.hideUnita.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hideUnita.Location = new System.Drawing.Point(14, 244);
            this.hideUnita.Name = "hideUnita";
            this.hideUnita.Size = new System.Drawing.Size(571, 50);
            this.hideUnita.TabIndex = 179;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.labco, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.labpe, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.pb4, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.pb3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.pb2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.pb1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.labtr, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.labss, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.comp1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.comp2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.comp3, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.comp4, 3, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(26, 90);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40.27778F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 59.72222F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(551, 132);
            this.tableLayoutPanel1.TabIndex = 184;
            // 
            // labco
            // 
            this.labco.AutoSize = true;
            this.labco.BackColor = System.Drawing.Color.Chartreuse;
            this.labco.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labco.Font = new System.Drawing.Font("Trebuchet MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labco.ForeColor = System.Drawing.Color.Black;
            this.labco.Location = new System.Drawing.Point(431, 87);
            this.labco.Margin = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.labco.Name = "labco";
            this.labco.Size = new System.Drawing.Size(100, 45);
            this.labco.TabIndex = 188;
            this.labco.Text = "0.00";
            this.labco.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labco.Visible = false;
            // 
            // labpe
            // 
            this.labpe.BackColor = System.Drawing.Color.Chartreuse;
            this.labpe.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labpe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labpe.Font = new System.Drawing.Font("Trebuchet MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labpe.ForeColor = System.Drawing.Color.Black;
            this.labpe.Location = new System.Drawing.Point(294, 87);
            this.labpe.Margin = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.labpe.Name = "labpe";
            this.labpe.Size = new System.Drawing.Size(97, 45);
            this.labpe.TabIndex = 187;
            this.labpe.Text = "0.00";
            this.labpe.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labpe.Visible = false;
            // 
            // pb4
            // 
            this.pb4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pb4.BackgroundImage = global::RollartSystemJudge.Properties.Resources.arrow_bottom_icon_32;
            this.pb4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb4.Location = new System.Drawing.Point(464, 3);
            this.pb4.Name = "pb4";
            this.pb4.Size = new System.Drawing.Size(33, 29);
            this.pb4.TabIndex = 192;
            this.pb4.TabStop = false;
            this.pb4.Visible = false;
            // 
            // pb3
            // 
            this.pb3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pb3.BackgroundImage = global::RollartSystemJudge.Properties.Resources.arrow_bottom_icon_32;
            this.pb3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb3.Location = new System.Drawing.Point(326, 3);
            this.pb3.Name = "pb3";
            this.pb3.Size = new System.Drawing.Size(33, 29);
            this.pb3.TabIndex = 191;
            this.pb3.TabStop = false;
            this.pb3.Visible = false;
            // 
            // pb2
            // 
            this.pb2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pb2.BackgroundImage = global::RollartSystemJudge.Properties.Resources.arrow_bottom_icon_32;
            this.pb2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb2.Location = new System.Drawing.Point(189, 3);
            this.pb2.Name = "pb2";
            this.pb2.Size = new System.Drawing.Size(33, 29);
            this.pb2.TabIndex = 190;
            this.pb2.TabStop = false;
            this.pb2.Visible = false;
            // 
            // pb1
            // 
            this.pb1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pb1.BackgroundImage = global::RollartSystemJudge.Properties.Resources.arrow_bottom_icon_32;
            this.pb1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb1.Location = new System.Drawing.Point(52, 3);
            this.pb1.Name = "pb1";
            this.pb1.Size = new System.Drawing.Size(33, 29);
            this.pb1.TabIndex = 189;
            this.pb1.TabStop = false;
            this.pb1.Visible = false;
            // 
            // labtr
            // 
            this.labtr.BackColor = System.Drawing.Color.Chartreuse;
            this.labtr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labtr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labtr.Font = new System.Drawing.Font("Trebuchet MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labtr.ForeColor = System.Drawing.Color.Black;
            this.labtr.Location = new System.Drawing.Point(157, 87);
            this.labtr.Margin = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.labtr.Name = "labtr";
            this.labtr.Size = new System.Drawing.Size(97, 45);
            this.labtr.TabIndex = 186;
            this.labtr.Text = "0.00";
            this.labtr.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labtr.Visible = false;
            // 
            // labss
            // 
            this.labss.BackColor = System.Drawing.Color.Chartreuse;
            this.labss.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labss.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labss.Font = new System.Drawing.Font("Trebuchet MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labss.ForeColor = System.Drawing.Color.Black;
            this.labss.Location = new System.Drawing.Point(20, 87);
            this.labss.Margin = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.labss.Name = "labss";
            this.labss.Size = new System.Drawing.Size(97, 45);
            this.labss.TabIndex = 185;
            this.labss.Text = "0.00";
            this.labss.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labss.Visible = false;
            // 
            // comp1
            // 
            this.comp1.AutoEllipsis = true;
            this.comp1.BackColor = System.Drawing.Color.Black;
            this.comp1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.comp1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.comp1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comp1.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.comp1.FlatAppearance.BorderSize = 2;
            this.comp1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Yellow;
            this.comp1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.comp1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.comp1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comp1.Font = new System.Drawing.Font("Arial Narrow", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp1.ForeColor = System.Drawing.Color.Yellow;
            this.comp1.Location = new System.Drawing.Point(3, 35);
            this.comp1.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.comp1.Name = "comp1";
            this.comp1.Size = new System.Drawing.Size(131, 49);
            this.comp1.TabIndex = 0;
            this.comp1.Tag = "1";
            this.comp1.UseVisualStyleBackColor = false;
            this.comp1.Click += new System.EventHandler(this.comp1_Click);
            // 
            // comp2
            // 
            this.comp2.AutoEllipsis = true;
            this.comp2.BackColor = System.Drawing.Color.Black;
            this.comp2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.comp2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.comp2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comp2.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.comp2.FlatAppearance.BorderSize = 2;
            this.comp2.FlatAppearance.CheckedBackColor = System.Drawing.Color.Yellow;
            this.comp2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.comp2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.comp2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comp2.Font = new System.Drawing.Font("Arial Narrow", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp2.ForeColor = System.Drawing.Color.Yellow;
            this.comp2.Location = new System.Drawing.Point(140, 35);
            this.comp2.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.comp2.Name = "comp2";
            this.comp2.Size = new System.Drawing.Size(131, 49);
            this.comp2.TabIndex = 1;
            this.comp2.Tag = "2";
            this.comp2.UseVisualStyleBackColor = false;
            this.comp2.Click += new System.EventHandler(this.button8_Click);
            // 
            // comp3
            // 
            this.comp3.AutoEllipsis = true;
            this.comp3.BackColor = System.Drawing.Color.Black;
            this.comp3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.comp3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.comp3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comp3.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.comp3.FlatAppearance.BorderSize = 2;
            this.comp3.FlatAppearance.CheckedBackColor = System.Drawing.Color.Yellow;
            this.comp3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.comp3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.comp3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comp3.Font = new System.Drawing.Font("Arial Narrow", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp3.ForeColor = System.Drawing.Color.Yellow;
            this.comp3.Location = new System.Drawing.Point(277, 35);
            this.comp3.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.comp3.Name = "comp3";
            this.comp3.Size = new System.Drawing.Size(131, 49);
            this.comp3.TabIndex = 2;
            this.comp3.Tag = "3";
            this.comp3.UseVisualStyleBackColor = false;
            this.comp3.Click += new System.EventHandler(this.button9_Click);
            // 
            // comp4
            // 
            this.comp4.AutoEllipsis = true;
            this.comp4.BackColor = System.Drawing.Color.Black;
            this.comp4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.comp4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.comp4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comp4.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.comp4.FlatAppearance.BorderSize = 2;
            this.comp4.FlatAppearance.CheckedBackColor = System.Drawing.Color.Yellow;
            this.comp4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.comp4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.comp4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comp4.Font = new System.Drawing.Font("Arial Narrow", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp4.ForeColor = System.Drawing.Color.Yellow;
            this.comp4.Location = new System.Drawing.Point(414, 35);
            this.comp4.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.comp4.Name = "comp4";
            this.comp4.Size = new System.Drawing.Size(134, 49);
            this.comp4.TabIndex = 3;
            this.comp4.Tag = "4";
            this.comp4.UseVisualStyleBackColor = false;
            this.comp4.Click += new System.EventHandler(this.button11_Click);
            // 
            // etichetta
            // 
            this.etichetta.Font = new System.Drawing.Font("Trebuchet MS", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.etichetta.ForeColor = System.Drawing.Color.Yellow;
            this.etichetta.Location = new System.Drawing.Point(32, 363);
            this.etichetta.Name = "etichetta";
            this.etichetta.Size = new System.Drawing.Size(311, 53);
            this.etichetta.TabIndex = 171;
            this.etichetta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // score
            // 
            this.score.Font = new System.Drawing.Font("Trebuchet MS", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.score.ForeColor = System.Drawing.Color.Yellow;
            this.score.Location = new System.Drawing.Point(349, 363);
            this.score.Name = "score";
            this.score.Size = new System.Drawing.Size(195, 53);
            this.score.TabIndex = 170;
            this.score.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelDecine
            // 
            this.panelDecine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelDecine.ColumnCount = 4;
            this.panelDecine.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.panelDecine.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.panelDecine.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.panelDecine.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.panelDecine.Controls.Add(this.button22, 3, 0);
            this.panelDecine.Controls.Add(this.button23, 2, 0);
            this.panelDecine.Controls.Add(this.button20, 0, 0);
            this.panelDecine.Controls.Add(this.button21, 1, 0);
            this.panelDecine.Location = new System.Drawing.Point(27, 295);
            this.panelDecine.Name = "panelDecine";
            this.panelDecine.RowCount = 1;
            this.panelDecine.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panelDecine.Size = new System.Drawing.Size(550, 50);
            this.panelDecine.TabIndex = 177;
            // 
            // button22
            // 
            this.button22.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button22.BackColor = System.Drawing.Color.Gold;
            this.button22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button22.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button22.FlatAppearance.BorderSize = 0;
            this.button22.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button22.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button22.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan;
            this.button22.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button22.ForeColor = System.Drawing.Color.Black;
            this.button22.Location = new System.Drawing.Point(411, 0);
            this.button22.Margin = new System.Windows.Forms.Padding(0);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(139, 50);
            this.button22.TabIndex = 13;
            this.button22.Text = "75";
            this.button22.UseVisualStyleBackColor = false;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // button23
            // 
            this.button23.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button23.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button23.FlatAppearance.BorderSize = 0;
            this.button23.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button23.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button23.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan;
            this.button23.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button23.ForeColor = System.Drawing.Color.Black;
            this.button23.Location = new System.Drawing.Point(274, 0);
            this.button23.Margin = new System.Windows.Forms.Padding(0);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(137, 50);
            this.button23.TabIndex = 12;
            this.button23.Text = "50";
            this.button23.UseVisualStyleBackColor = false;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // button20
            // 
            this.button20.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button20.BackColor = System.Drawing.Color.White;
            this.button20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button20.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button20.FlatAppearance.BorderSize = 0;
            this.button20.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button20.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button20.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan;
            this.button20.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button20.ForeColor = System.Drawing.Color.Black;
            this.button20.Location = new System.Drawing.Point(0, 0);
            this.button20.Margin = new System.Windows.Forms.Padding(0);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(137, 50);
            this.button20.TabIndex = 89;
            this.button20.Text = "00";
            this.button20.UseVisualStyleBackColor = false;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button21
            // 
            this.button21.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.button21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button21.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button21.FlatAppearance.BorderSize = 0;
            this.button21.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button21.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button21.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan;
            this.button21.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button21.ForeColor = System.Drawing.Color.Black;
            this.button21.Location = new System.Drawing.Point(137, 0);
            this.button21.Margin = new System.Windows.Forms.Padding(0);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(137, 50);
            this.button21.TabIndex = 11;
            this.button21.Text = "25";
            this.button21.UseVisualStyleBackColor = false;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // panelUnita
            // 
            this.panelUnita.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelUnita.ColumnCount = 11;
            this.panelUnita.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.panelUnita.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.panelUnita.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.panelUnita.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.panelUnita.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.panelUnita.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.panelUnita.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.panelUnita.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.panelUnita.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.panelUnita.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.panelUnita.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.panelUnita.Controls.Add(this.button19, 10, 0);
            this.panelUnita.Controls.Add(this.button8, 0, 0);
            this.panelUnita.Controls.Add(this.button17, 9, 0);
            this.panelUnita.Controls.Add(this.button18, 8, 0);
            this.panelUnita.Controls.Add(this.button13, 7, 0);
            this.panelUnita.Controls.Add(this.button14, 6, 0);
            this.panelUnita.Controls.Add(this.button15, 5, 0);
            this.panelUnita.Controls.Add(this.button16, 4, 0);
            this.panelUnita.Controls.Add(this.button10, 3, 0);
            this.panelUnita.Controls.Add(this.button11, 2, 0);
            this.panelUnita.Controls.Add(this.button9, 1, 0);
            this.panelUnita.Location = new System.Drawing.Point(27, 244);
            this.panelUnita.Name = "panelUnita";
            this.panelUnita.RowCount = 1;
            this.panelUnita.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panelUnita.Size = new System.Drawing.Size(550, 50);
            this.panelUnita.TabIndex = 176;
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.Color.LimeGreen;
            this.button19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button19.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button19.FlatAppearance.BorderSize = 0;
            this.button19.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button19.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button19.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan;
            this.button19.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button19.ForeColor = System.Drawing.Color.Black;
            this.button19.Location = new System.Drawing.Point(490, 0);
            this.button19.Margin = new System.Windows.Forms.Padding(0);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(60, 50);
            this.button19.TabIndex = 10;
            this.button19.Text = "10";
            this.button19.UseVisualStyleBackColor = false;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.SkyBlue;
            this.button8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button8.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.Color.Black;
            this.button8.Location = new System.Drawing.Point(0, 0);
            this.button8.Margin = new System.Windows.Forms.Padding(0);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(49, 50);
            this.button8.TabIndex = 0;
            this.button8.Text = "0";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click_1);
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.Lime;
            this.button17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button17.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button17.FlatAppearance.BorderSize = 0;
            this.button17.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button17.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button17.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan;
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.ForeColor = System.Drawing.Color.Black;
            this.button17.Location = new System.Drawing.Point(441, 0);
            this.button17.Margin = new System.Windows.Forms.Padding(0);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(49, 50);
            this.button17.TabIndex = 9;
            this.button17.Text = "9";
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.Color.LightGreen;
            this.button18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button18.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button18.FlatAppearance.BorderSize = 0;
            this.button18.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button18.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button18.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan;
            this.button18.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.ForeColor = System.Drawing.Color.Black;
            this.button18.Location = new System.Drawing.Point(392, 0);
            this.button18.Margin = new System.Windows.Forms.Padding(0);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(49, 50);
            this.button18.TabIndex = 8;
            this.button18.Text = "8";
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Honeydew;
            this.button13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button13.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button13.FlatAppearance.BorderSize = 0;
            this.button13.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan;
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.ForeColor = System.Drawing.Color.Black;
            this.button13.Location = new System.Drawing.Point(343, 0);
            this.button13.Margin = new System.Windows.Forms.Padding(0);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(49, 50);
            this.button13.TabIndex = 7;
            this.button13.Text = "7";
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.Ivory;
            this.button14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button14.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan;
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.ForeColor = System.Drawing.Color.Black;
            this.button14.Location = new System.Drawing.Point(294, 0);
            this.button14.Margin = new System.Windows.Forms.Padding(0);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(49, 50);
            this.button14.TabIndex = 6;
            this.button14.Text = "6";
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.White;
            this.button15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button15.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button15.FlatAppearance.BorderSize = 0;
            this.button15.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button15.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button15.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan;
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.ForeColor = System.Drawing.Color.Black;
            this.button15.Location = new System.Drawing.Point(245, 0);
            this.button15.Margin = new System.Windows.Forms.Padding(0);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(49, 50);
            this.button15.TabIndex = 5;
            this.button15.Text = "5";
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.button15_Click_2);
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.Azure;
            this.button16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button16.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button16.FlatAppearance.BorderSize = 0;
            this.button16.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button16.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button16.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan;
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.ForeColor = System.Drawing.Color.Black;
            this.button16.Location = new System.Drawing.Point(196, 0);
            this.button16.Margin = new System.Windows.Forms.Padding(0);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(49, 50);
            this.button16.TabIndex = 4;
            this.button16.Text = "4";
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.button16_Click_1);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.LightCyan;
            this.button10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button10.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button10.FlatAppearance.BorderSize = 0;
            this.button10.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan;
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.ForeColor = System.Drawing.Color.Black;
            this.button10.Location = new System.Drawing.Point(147, 0);
            this.button10.Margin = new System.Windows.Forms.Padding(0);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(49, 50);
            this.button10.TabIndex = 3;
            this.button10.Text = "3";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click_1);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.PaleTurquoise;
            this.button11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button11.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan;
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = System.Drawing.Color.Black;
            this.button11.Location = new System.Drawing.Point(98, 0);
            this.button11.Margin = new System.Windows.Forms.Padding(0);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(49, 50);
            this.button11.TabIndex = 2;
            this.button11.Text = "2";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click_1);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.PowderBlue;
            this.button9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button9.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.Color.Black;
            this.button9.Location = new System.Drawing.Point(49, 0);
            this.button9.Margin = new System.Windows.Forms.Padding(0);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(49, 50);
            this.button9.TabIndex = 1;
            this.button9.Text = "1";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click_1);
            // 
            // labin
            // 
            this.labin.AutoSize = true;
            this.labin.BackColor = System.Drawing.Color.Lime;
            this.labin.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labin.ForeColor = System.Drawing.Color.Black;
            this.labin.Location = new System.Drawing.Point(448, 103);
            this.labin.Name = "labin";
            this.labin.Size = new System.Drawing.Size(0, 24);
            this.labin.TabIndex = 71;
            this.labin.Visible = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Black;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.sendComponents);
            this.panel4.Controls.Add(this.ltimer);
            this.panel4.Controls.Add(this.sendMarks);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 547);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(605, 74);
            this.panel4.TabIndex = 4;
            // 
            // sendComponents
            // 
            this.sendComponents.BackColor = System.Drawing.Color.Black;
            this.sendComponents.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sendComponents.Dock = System.Windows.Forms.DockStyle.Left;
            this.sendComponents.Enabled = false;
            this.sendComponents.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.sendComponents.FlatAppearance.BorderSize = 3;
            this.sendComponents.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.sendComponents.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.sendComponents.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sendComponents.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendComponents.ForeColor = System.Drawing.Color.Cyan;
            this.sendComponents.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.sendComponents.Location = new System.Drawing.Point(0, 0);
            this.sendComponents.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.sendComponents.Name = "sendComponents";
            this.sendComponents.Size = new System.Drawing.Size(272, 72);
            this.sendComponents.TabIndex = 168;
            this.sendComponents.Text = "SEND COMPONENTS";
            this.sendComponents.UseVisualStyleBackColor = false;
            this.sendComponents.Click += new System.EventHandler(this.sendComponents_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "#";
            this.columnHeader1.Width = 43;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Element";
            this.columnHeader2.Width = 197;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Penalty";
            this.columnHeader3.Width = 74;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "QOE";
            this.columnHeader4.Width = 70;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "code";
            this.columnHeader5.Width = 0;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "id";
            this.columnHeader6.Width = 0;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "#";
            this.columnHeader7.Width = 43;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Element";
            this.columnHeader8.Width = 197;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Penalty";
            this.columnHeader9.Width = 74;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "QOE";
            this.columnHeader10.Width = 70;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "code";
            this.columnHeader11.Width = 0;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "id";
            this.columnHeader12.Width = 0;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "#";
            this.columnHeader13.Width = 43;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Element";
            this.columnHeader14.Width = 197;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Penalty";
            this.columnHeader15.Width = 74;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "QOE";
            this.columnHeader16.Width = 70;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "code";
            this.columnHeader17.Width = 0;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "id";
            this.columnHeader18.Width = 0;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "#";
            this.columnHeader19.Width = 43;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Element";
            this.columnHeader20.Width = 197;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "Penalty";
            this.columnHeader21.Width = 74;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "QOE";
            this.columnHeader22.Width = 70;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "code";
            this.columnHeader23.Width = 0;
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "id";
            this.columnHeader24.Width = 0;
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "#";
            this.columnHeader25.Width = 43;
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "Element";
            this.columnHeader26.Width = 197;
            // 
            // columnHeader27
            // 
            this.columnHeader27.Text = "Penalty";
            this.columnHeader27.Width = 74;
            // 
            // columnHeader28
            // 
            this.columnHeader28.Text = "QOE";
            this.columnHeader28.Width = 70;
            // 
            // columnHeader29
            // 
            this.columnHeader29.Text = "code";
            this.columnHeader29.Width = 0;
            // 
            // columnHeader30
            // 
            this.columnHeader30.Text = "id";
            this.columnHeader30.Width = 0;
            // 
            // columnHeader31
            // 
            this.columnHeader31.Text = "#";
            this.columnHeader31.Width = 43;
            // 
            // columnHeader32
            // 
            this.columnHeader32.Text = "Element";
            this.columnHeader32.Width = 197;
            // 
            // columnHeader33
            // 
            this.columnHeader33.Text = "Penalty";
            this.columnHeader33.Width = 74;
            // 
            // columnHeader34
            // 
            this.columnHeader34.Text = "QOE";
            this.columnHeader34.Width = 70;
            // 
            // columnHeader35
            // 
            this.columnHeader35.Text = "code";
            this.columnHeader35.Width = 0;
            // 
            // columnHeader36
            // 
            this.columnHeader36.Text = "id";
            this.columnHeader36.Width = 0;
            // 
            // FormJudge
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1072, 709);
            this.ControlBox = false;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "FormJudge";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rollart System - Judge Panel";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormJudge_FormClosing);
            this.Load += new System.EventHandler(this.FormJudge_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormJudge_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FormJudge_MouseMove);
            this.panel1.ResumeLayout(false);
            this.flowLayoutPanel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BatteryIndicator)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.gbComponents.ResumeLayout(false);
            this.gbComponents.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).EndInit();
            this.panelDecine.ResumeLayout(false);
            this.panelUnita.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label labin;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.ListView lv;
        //private MyListView lv;
        private System.Windows.Forms.ColumnHeader num;
        private System.Windows.Forms.ColumnHeader desc;
        private System.Windows.Forms.ColumnHeader pen;
        private System.Windows.Forms.ColumnHeader qoe;
        private System.Windows.Forms.GroupBox gbComponents;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Button j1;
        private System.Windows.Forms.Button tp;
        private System.Windows.Forms.Button sendMarks;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ColumnHeader name;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button ltimer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ColumnHeader id;
        private System.Windows.Forms.Button sendComponents;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ColumnHeader columnHeader30;
        //private MyListView lv;
        private System.Windows.Forms.ColumnHeader columnHeader31;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.ColumnHeader columnHeader33;
        private System.Windows.Forms.ColumnHeader columnHeader34;
        private System.Windows.Forms.ColumnHeader columnHeader35;
        private System.Windows.Forms.ColumnHeader columnHeader36;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Label etichetta;
        private System.Windows.Forms.Label score;
        private System.Windows.Forms.Panel hideUnita;
        private System.Windows.Forms.Panel hideDecine;
        private System.Windows.Forms.TableLayoutPanel panelDecine;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.TableLayoutPanel panelUnita;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label labco;
        private System.Windows.Forms.Label labpe;
        private System.Windows.Forms.Label labtr;
        private System.Windows.Forms.Label labss;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button comp4;
        private System.Windows.Forms.Button comp1;
        private System.Windows.Forms.Button comp2;
        private System.Windows.Forms.Button comp3;
        private System.Windows.Forms.Button logButton;
        private System.Windows.Forms.Button quit;
        private System.Windows.Forms.Button random;
        private System.Windows.Forms.TextBox log;
        private System.Windows.Forms.PictureBox pb1;
        private System.Windows.Forms.PictureBox pb4;
        private System.Windows.Forms.PictureBox pb3;
        private System.Windows.Forms.PictureBox pb2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label buttonNext;
        private System.Windows.Forms.Label BatteryLifeRemaining;
        private System.Windows.Forms.PictureBox BatteryIndicator;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label falls;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button settings;
    }
}


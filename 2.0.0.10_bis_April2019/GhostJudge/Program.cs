﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;

namespace RollartSystemJudge
{
    static class Program
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // *** Modifica del 10/02/2019 - 2.0.0.10 *** //

            Mutex mutex = new Mutex(false, System.Diagnostics.Process.GetCurrentProcess().ProcessName);
            if (!RollartSystemJudge.Properties.Settings.Default.Random)
            {
                if (!mutex.WaitOne(TimeSpan.FromSeconds(2), false))
                {
                    MessageBox.Show("Application JudgePanel is already running...", "", MessageBoxButtons.OK);
                    return;
                }
            }

            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new FormJudge());
            }
            finally { mutex.ReleaseMutex(); } 
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using RollartSystemTech;
using System.Data.SQLite;
using System.Collections;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Drawing.Imaging;
using System.Net;
using CrystalDecisions.Shared;
using System.Diagnostics;
using System.Globalization;

namespace Ghost
{
    public partial class EventsForm : Form
    {
        # region VARIABILI E COSTANTI
        public SingoloForm sps = null;
        public CoppieForm cf = null;
        public DanzaForm df = null;
        SplashScreen sc = null;

        Event ev = null;
        Utility ut = null;
        ToolTip tt = null;
        Judges ju = null;
        private Point mouse_offset;
        bool flagJudgesInserted = false, styleDance = false, compulsory1 = false, compulsory2 = false; //, oneJudgeNotConnected = false;
        ASCIIEncoding asen = new ASCIIEncoding();
        private GifImage gifImage = null;
        private string filePath = @"waiting.gif";

        //public static HandleJudgeClient judgeClient = null;
        public static JudgesClient jclient = null;
        Thread workerThread = null;

        bool flagClose = false, exitCheck = false;
        int indexNodoSelezionato = 0;
        string recentFolder = ""; //2.0.0.10 - 28/01/2019
        string formato = ".pdf"; // 2.0.0.10_bis 15/04/2019
        SQLiteTransaction transaction = null;
        #endregion

        public EventsForm()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
            backgroundWorker1.WorkerSupportsCancellation = true;
            backgroundWorker1.WorkerReportsProgress = true;
        }
        
        private void EventsForm_Load(object sender, EventArgs e)
        {
            try
            {
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

                OpenConnection();
                sc = new SplashScreen();
                sc.ShowDialog();
                if (backgroundWorker1.IsBusy != true)
                {
                    sc.InitializeProgress(0, 100, 0);
                    sc.Show();
                    this.WindowState = FormWindowState.Minimized;
                    this.ShowInTaskbar = false;
                    // Start the asynchronous operation.
                    backgroundWorker1.RunWorkerAsync();
                }
                sc.UpdateLabel = "Forms loading...";

                //Control.CheckForIllegalCrossThreadCalls = false; 
                ev = new Event();
                ut = new Utility(this);
                tt = new ToolTip();

                message2.Text += "RollArt TechPanel - vers. " + Application.ProductVersion
                               + ". ©2018-2019 - All rights reserved. Last update "
                               + System.IO.File.GetLastWriteTime(
                                 System.AppDomain.CurrentDomain.FriendlyName).ToString("dd/MM/yy HH:mm:ss");
                    
                message2.Text += " - IP Address: " + Utility.GetIP();

                Utility.WriteLog("°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°° NEW SESSION °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°", "OK");
                Utility.WriteLog(message2.Text, "OK");

                LoadSettings();
                LoadResources();

                gifImage = new GifImage(filePath);

                // recupero gli atleti per l'auto-completion dei nomi
                ut.GetNameSocieta();
                
                ju = new Judges();

                // *** Modifica del 28/06/2018 Nuovo metodo di connessione ****//
                jclient = new JudgesClient();
                jclient.InitializeJudge();

                // avvio il thread per la connessione con i giudici
                workerThread = new Thread(ut.WaitingJudgesAndVideo);
                workerThread.Start(this);

                button1.PerformClick();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "RollArt System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        
        # region DB CONNECTION
        public void OpenConnection()
        {
            try
            {
                string connString =
                    RollartSystemTech.Properties.Settings.Default.dbconnection;

                //connString += "Password=r0ll4rts1st3m;";
                Definizioni.conn = new SQLiteConnection(connString);
                // cifratura database del 31/08/2018
                //Definizioni.conn.SetPassword("r0ll4rts1st3m");
                
                Definizioni.conn.Open();

                //Definizioni.conn.ChangePassword("r0ll4rts1st3m");
                //Definizioni.conn.ChangePassword("");

                Utility.WriteLog(//Definizioni.conn.DataSource + " - " + 
                    Definizioni.conn.ConnectionString, "Ok");

                File.SetAttributes(@"C:\\RollArtSystem\\rolljudge2.s3db", FileAttributes.Normal);

            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex.Message, "ERROR");
                message.Text = "ERROR" + ex.Message;
            }
        }

        public void CloseConnection()
        {
            try
            {
                Definizioni.conn.Close();
                Utility.WriteLog(Definizioni.conn.DataSource + " - " + Definizioni.conn.ConnectionString, "Ok");
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex.Message, "ERROR");
                message.Text = "ERROR" + ex.Message;
            }
        }

        private void backup_Click(object sender, EventArgs e)
        {
            try
            {
                sf1.InitialDirectory = RollartSystemTech.Properties.Settings.Default.dbfolder;
                sf1.ShowDialog();

                if (sf1.FileName != "")
                {
                    File.Copy(RollartSystemTech.Properties.Settings.Default.dbfolder + "\\" +
                        Definizioni.conn.DataSource + ".s3db", sf1.FileName);
                    MessageBox.Show(sf1.FileName + " saved with success.", "RollArt System", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("Backup database:" + ex.Message, "ERROR");
            }
        }

        private void restore_Click(object sender, EventArgs e)
        {
            string dsDB = Definizioni.conn.DataSource;
            message.Text = "";
            try
            {
                of1.Filter = "File SQLite| *.s3db";
                of1.FileName = "rolljudge2";
                of1.Title = "Restore rollart database...";
                of1.InitialDirectory = RollartSystemTech.Properties.Settings.Default.dbfolder;
                if (of1.ShowDialog() == DialogResult.OK)
                {
                    
                    // chiudo il db rolljudge2
                    Definizioni.conn.Close();
                    Definizioni.conn.Dispose();
                    Definizioni.conn = null;
                    //this.cr.ReportSource = null;
                    FinalReport1.Close(); //FinalReport1.Dispose();
                    ResultReport1.Close(); //ResultReport1.Dispose();
                    PanelReport1.Close(); //PanelReport1.Dispose();
                    
                     //var newFilePath = Path.Combine(RollartSystemTech.Properties.Settings.Default.BackupFolder,
                     //        dsDB + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second + "_" +
                     //            DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Year + ".s3db");
                    var backupFile = Path.Combine(RollartSystemTech.Properties.Settings.Default.BackupFolder,
                             dsDB + DateTime.Now.ToString("_yyyyMMddHHmmss") + ".s3db");
                    var oldFilePath = Path.Combine(RollartSystemTech.Properties.Settings.Default.dbfolder, dsDB + ".s3db");

                    if (of1.FileName != oldFilePath)
                    {
                        // rinomino il db rolljudge2
                        File.Copy(oldFilePath, backupFile);

                        GC.Collect();
                        GC.WaitForPendingFinalizers();

                        if (!IsFileLocked(new FileInfo(oldFilePath)))
                            File.Delete(oldFilePath);

                        // copio il db da restorare
                        File.Copy(of1.FileName,
                              RollartSystemTech.Properties.Settings.Default.dbfolder + "\\rolljudge2.s3db");

                    }
                    OpenConnection();
                    Thread.Sleep(500);
                    reload.PerformClick();
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("Restore database:" + ex.Message, "ERROR");

                OpenConnection();
                Thread.Sleep(500);
                reload.PerformClick();

            }
        }

        public static Boolean IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                //Don't change FileAccess to ReadWrite, 
                //because if a file is in readOnly, it fails.
                stream = file.Open
                (
                    FileMode.Open,
                    FileAccess.Read,
                    FileShare.None
                );
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        # endregion

        #region NEW EVENT
        // new event button
        private void buTech_Click(object sender, EventArgs e)
        {
            try
            {
                buTech.BackColor = Color.FromArgb(175, 202, 11);
                button1.BackColor = Color.Transparent; button2.BackColor = Color.Transparent;
                button2.BackColor = Color.Transparent; button5.BackColor = Color.Transparent;
                message.Text = "";
                panNewEvent.Visible = true;
                panNewEvent.BringToFront();
                name.Focus();
                titolo.Text = "Events Management --> New Event";
                titolo.ImageIndex = 20;
                // recupero tutti gli atleti per l'auto-completion dei nomi
                //if (Definizioni.nomi != null)
                //{
                //    var source = new AutoCompleteStringCollection();
                //    string[] names = (string[])Definizioni.nomi.ToArray(typeof(string));
                //    source.AddRange(names);
                //    namep.AutoCompleteCustomSource = source;
                //}
                // recupero le specialità
                cbEvent.Items.Clear();
                cbEvent.Items.AddRange(ev.GetSpecialita().ToArray());

                // recupero le categorie
                cbCategory.Items.Clear();
                cbCategory.Items.AddRange(ev.GetCategory().ToArray());

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        //string[] names = null;
        private void cbEvent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetSegmentsFromCategoryDiscipline();

                // recupero autocompletion per specialità
                ut.GetSkatersPerDisciplina(Definizioni.idSpecialita);
                if (Definizioni.nomi != null)
                {
                    var source = new AutoCompleteStringCollection();
                    string[] names = (string[])Definizioni.nomi.ToArray(typeof(string));
                    source.AddRange(names);
                    namep.AutoCompleteCustomSource = source;
                }

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void cbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetSegmentsFromCategoryDiscipline();

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        // 2.0.0.8 - nuovao metodo per recuperar ei segmenti di gara in base ai parametri esistenti
        // modifica del 3/12/2018, dopo segnalazione di Barry Andrews
        private void GetSegmentsFromCategoryDiscipline()
        {
            try
            {
                if (cbEvent.SelectedIndex == -1) return;
                int idSpecialita = ev.GetIDSpecialita(cbEvent.SelectedItem.ToString());
                Definizioni.idSpecialita = idSpecialita;

                if (cbCategory.SelectedIndex == -1) return;
                int idCategoria = ev.GetIDCategory(cbCategory.SelectedItem.ToString());

                cb1.Visible = false; cb2.Visible = false; cb3.Visible = false;
                cb1.Checked = false; cb2.Checked = false; cb3.Checked = false;

                List<string[]> segments = ev.GetSegments(idSpecialita, idCategoria);
                if (segments.Count != 0)
                {
                    label23.Visible = false;
                    int comboBoxNumb = 1;
                    foreach (string[] seg in segments.ToArray())
                    {
                        ((CheckBox)this.Controls.Find("cb" + comboBoxNumb, true)[0]).Tag = seg[0];
                        ((CheckBox)this.Controls.Find("cb" + comboBoxNumb, true)[0]).Text = seg[1];
                        ((CheckBox)this.Controls.Find("cb" + comboBoxNumb, true)[0]).Visible = true;
                        ((CheckBox)this.Controls.Find("cb" + comboBoxNumb, true)[0]).Checked = true;
                        comboBoxNumb++;
                    }
                } else // no events
                {
                    label23.Visible = true;
                }
                switch (idSpecialita)
                {
                    // singoli e coppie artistico
                    case 1:
                        
                        ch2.Text = "Skater";
                        label6.Text = "Skater *";
                        break;
                    case 2:
                        
                        label6.Text = "Skater *";
                        ch2.Text = "Skater";
                        break;
                    case 3:
                        
                        ch2.Text = "Pair";
                        label6.Text = " Pair *";
                        break;
                    // danza e solo dance
                    case 4:
                        
                        ch2.Text = "Couple";
                        label6.Text = "Couple *";
                        break;
                    case 5:
                    case 6:
                        
                        ch2.Text = "Skater";
                        label6.Text = "Skater *";
                        break;
                    // precision
                    case 7:
                    case 8:
                        
                        ch2.Text = "Group";
                        label6.Text = "Group *";
                        break;
                }

                // cancello la lista atleti se cambio categoria o disciplina
                if (lv2.Items.Count > 0)
                {
                    lv2.Items.Clear();
                    namep.Text = ""; naz.Text = ""; club.Text = ""; region.Text = "";
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                return;
            }
        }

        // add partecipante 
        private void add_Click(object sender, EventArgs e)
        {
            try
            {
                if (namep.TextLength == 0)
                {
                    message.Text = Definizioni.resources["event2"].ToString();
                    return;
                }
                
                int tempPart = lv2.Items.Count;
                lv2.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "" }, -1));
                lv2.Items[tempPart].SubItems[0].Text = "" + (tempPart + 1);
                lv2.Items[tempPart].SubItems[1].Text = namep.Text.TrimEnd();
                lv2.Items[tempPart].SubItems[2].Text = club.Text.TrimEnd();
                lv2.Items[tempPart].SubItems[3].Text = region.Text.TrimEnd();
                lv2.Items[tempPart].SubItems[4].Text = naz.Text.TrimEnd();
                lv2.Items[tempPart].SubItems[5].Text = Definizioni.idSpecialita + "";

                Definizioni.numPartecipanti++;

                if (Definizioni.numPartecipanti == 0)
                {
                    confirmNewEvent.Enabled = false;
                }
                else if (flagJudgesInserted)
                    confirmNewEvent.Enabled = true;
                lv2.Items[lv2.Items.Count - 1].EnsureVisible();
                namep.ResetText();
                club.ResetText();
                region.ResetText();
                naz.ResetText();
                namep.Focus();

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void namep_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "SELECT Societa, Country, Region FROM Athletes WHERE Name = '"
                            + namep.Text.TrimEnd().Replace("'", "''") + "'";
                        SQLiteDataReader dr = command.ExecuteReader();
                        if (!dr.HasRows)
                        {
                            return;
                        }
                        while (dr.Read())
                        {
                            club.Text = dr[0].ToString().TrimEnd();
                            naz.Text = dr[1].ToString().TrimEnd();
                            region.Text = dr[2].ToString().TrimEnd();
                        }
                        dr.Close();
                    }
                    if (club.Text.Equals("")) club.Focus();
                    else if (region.Text.Equals("")) region.Focus();
                    else if (naz.Text.Equals("")) naz.Focus();
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        // delete partecipante
        private void delete_Click(object sender, EventArgs e)
        {
            try
            {
                if (lv2.SelectedIndices.Count == 0) return;
                if (lv2.Items.Count > 0)
                {
                    Definizioni.numPartecipanti--;
                    lv2.Items.RemoveAt(lv2.SelectedIndices[0]);
                    for (int i = 0; i < lv2.Items.Count; i++)
                    {
                        lv2.Items[i].SubItems[0].Text = "" + (i + 1);
                    }
                    delete.Enabled = true;
                    if (Definizioni.numPartecipanti == 0)
                    {
                        confirmNewEvent.Enabled = false;
                    }
                    namep.Focus();
                }
                else
                {
                    confirmNewEvent.Enabled = false;
                    delete.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        // select Skater 
        private void lv2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                updateskater.Enabled = false;
                if (lv2.SelectedIndices.Count > 0)
                {
                    namep.Text = lv2.SelectedItems[0].SubItems[1].Text.TrimEnd();
                    club.Text = lv2.SelectedItems[0].SubItems[2].Text.TrimEnd();
                    region.Text = lv2.SelectedItems[0].SubItems[3].Text.TrimEnd();
                    naz.Text = lv2.SelectedItems[0].SubItems[4].Text.TrimEnd();
                    namep.Focus();
                    updateskater.Enabled = true;
                    hidden.Text = lv2.SelectedItems[0].Text;
                } else
                {
                    namep.Text = "";
                    club.Text = "";
                    naz.Text = "";
                    region.Text = "";
                    namep.Focus();
                    hidden.Text = "";
                    add.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        // Update skater
        private void updateskater_Click(object sender, EventArgs e)
        {
            try
            {
                updateskater.Enabled = false;
                foreach (ListViewItem lvi in lv2.Items)
                {
                    if (lvi.Text.Equals(hidden.Text))
                    {
                        lvi.SubItems[1].Text = namep.Text.TrimEnd();
                        lvi.SubItems[2].Text = club.Text.TrimEnd();
                        lvi.SubItems[3].Text = region.Text.TrimEnd();
                        lvi.SubItems[4].Text = naz.Text.TrimEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }


        // conferma evento
        private void confirmNewEvent_Click(object sender, EventArgs e)
        {
            try
            {
                message.BackColor = Color.Transparent;
                // verifica se è stata scelto il nome
                if (name.TextLength == 0)
                {
                    message.Text = Definizioni.resources["event10"].ToString();
                    name.Focus();
                    return;
                }
                // verifica se è stata scelta la categoria
                if (cbCategory.SelectedItem == null)
                {
                    message.Text = Definizioni.resources["event1"].ToString();
                    cbCategory.Focus();
                    return;
                }
                // verifica se è stato scelto il segmento di gara
                bool almostOneSegment = false;
                for (int i=0; i<3; i++)
                {
                    if (((CheckBox)this.Controls.Find("cb" + (i + 1), true)[0]).Checked)
                    {
                        almostOneSegment = true;
                    }
                }
                if (!almostOneSegment)
                {
                    message.BackColor = Color.Yellow;
                    message.Text = Definizioni.resources["event5"].ToString();
                    return;
                }

                // 2.0.0.10 - 29/01/2019 Se skaters zero esco
                if (lv2.Items.Count == 0)
                {
                    message.Text = "Insert competitors";
                    message.BackColor = Color.Yellow;
                    return;
                }
                //if (cb1.Visible && cb2.Visible)
                //{
                //    if (!cb1.Checked && !cb2.Checked)
                //    {
                //        message.Text = Definizioni.resources["event5"].ToString();
                //        return;
                //    }
                //}
                //if (cb4.Visible && cb5.Visible)
                //{
                //    if (!cb4.Checked && !cb5.Checked)
                //    {
                //        message.Text = Definizioni.resources["event5"].ToString();
                //        return;
                //    }
                //}
                pb1.Visible = true;

                Definizioni.numPartecipanti = lv2.Items.Count;
                Definizioni.nomeGara = name.Text;
                /****************************************************/
                Competition gara = new Competition();
                gara.name = Definizioni.nomeGara;
                gara.place = place.Text;
                gara.datefrom = dateFrom.Value.ToString("dd/MM/yyyy");
                gara.dateto = dateTo.Value.ToString("dd/MM/yyyy");
                /****************************************************/
                SQLiteCommand cmd = new SQLiteCommand("select MAX(ID_GaraParams) from GaraParams",
                    Definizioni.conn);
                SQLiteDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (dr[0].ToString().Equals("")) Definizioni.idGaraParams = 1;
                    else Definizioni.idGaraParams = dr.GetInt32(0) + 1;
                }
                pb1.Value++; pb1.Refresh();
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    // 2.0.0.10_bis - Recupero l'id della categoria e della specialita dal db invece che dall'indice
                    //int idCategoria = cbCategory.SelectedIndex + 1;
                    //int idSpecialità = cbEvent.SelectedIndex + 1;
                    int idCategoria = ev.GetIDCategory(cbCategory.SelectedItem.ToString());
                    int idSpecialità = ev.GetIDSpecialita(cbEvent.SelectedItem.ToString());
                    //int numJudges = lv1.Items.Count - 3;
                    Definizioni.numJudges = Definizioni.numOfJudges;

                    /****************************************************/
                    gara.idGara = Definizioni.idGaraParams;
                    gara.idDiscipline = idSpecialità;
                    gara.discipline = cbEvent.SelectedItem.ToString();
                    gara.idCategory = idCategoria;
                    gara.category = cbCategory.SelectedItem.ToString();
                    gara.numJudges = Definizioni.numJudges;
                    gara.segment1 = "";
                    gara.segment2 = "";
                    gara.segment3 = "";
                    gara.segment4 = "";
                    /****************************************************/

                    command.CommandText = "INSERT INTO GaraParams(ID_GaraParams,Name,Place,Date,DateEnd,ID_Segment,ID_Specialita,ID_Category,Partecipants,Sex,Completed,LastPartecipant,NumJudges)" +
                        " VALUES(@idgara,@name,@place,@date,@dateend,@idsegment,@spec,@category,@part,@sex,'N',0,@judges)";
                    command.Parameters.AddWithValue("@idgara", Definizioni.idGaraParams);
                    command.Parameters.AddWithValue("@name", Definizioni.nomeGara);
                    command.Parameters.AddWithValue("@place", place.Text.TrimEnd());
                    command.Parameters.AddWithValue("@date", dateFrom.Value.ToString("dd/MM/yyyy"));
                    command.Parameters.AddWithValue("@dateend", dateTo.Value.ToString("dd/MM/yyyy"));
                    command.Parameters.AddWithValue("@part", Definizioni.numPartecipanti);
                    command.Parameters.AddWithValue("@judges", Definizioni.numJudges);

                    /*** singolo M/F ***/
                    if (idSpecialità == 1 || idSpecialità == 2)
                    {
                        if (idSpecialità == 1) command.Parameters.AddWithValue("@spec", 1);
                        if (idSpecialità == 2) command.Parameters.AddWithValue("@spec", 2);
                        command.Parameters.AddWithValue("@category", idCategoria);
                        if (idSpecialità == 1) command.Parameters.AddWithValue("@sex", "M");
                        if (idSpecialità == 2) command.Parameters.AddWithValue("@sex", "F");

                        if (idSpecialità == 1) gara.sex = "M";
                        if (idSpecialità == 2) gara.sex = "F";

                        if (cb1.Checked) // Short
                        {
                            Definizioni.idSegment = int.Parse(cb1.Tag.ToString());
                            command.Parameters.AddWithValue("@idsegment", cb1.Tag);
                            command.ExecuteNonQuery();
                            /****************************************************/
                            gara.segment1 = cb1.Text;
                            gara.idSegment1 = Definizioni.idSegment;
                            /****************************************************/
                        }
                        if (cb2.Checked) // Lungo
                        {
                            Definizioni.idSegment = int.Parse(cb2.Tag.ToString());
                            command.Parameters.AddWithValue("@idsegment", cb2.Tag);
                            command.ExecuteNonQuery();
                            /****************************************************/
                            gara.segment2 = cb2.Text;
                            gara.idSegment2 = Definizioni.idSegment;
                            /****************************************************/
                        }
                    }
                    /*** coppie artistico ***/
                    else if (idSpecialità == 3)
                    {
                        command.Parameters.AddWithValue("@spec", 3);
                        command.Parameters.AddWithValue("@category", idCategoria);
                        //2.0.1 command.Parameters.AddWithValue("@sex", "");
                        command.Parameters.AddWithValue("@sex", "A");
                        if (cb1.Checked) // Short
                        {
                            Definizioni.idSegment = int.Parse(cb1.Tag.ToString());
                            command.Parameters.AddWithValue("@idsegment", cb1.Tag);
                            command.ExecuteNonQuery();
                            /****************************************************/
                            gara.segment1 = cb1.Text;
                            gara.idSegment1 = Definizioni.idSegment;
                            gara.sex = "A";
                            /****************************************************/
                        }
                        if (cb2.Checked) // Lungo
                        {
                            Definizioni.idSegment = int.Parse(cb2.Tag.ToString());
                            command.Parameters.AddWithValue("@idsegment", cb2.Tag);
                            command.ExecuteNonQuery();
                            /****************************************************/
                            gara.segment2 = cb2.Text;
                            gara.idSegment2 = Definizioni.idSegment;
                            gara.sex = "A";
                            /****************************************************/
                        }
                    }
                    /*** coppie danza - solo dance M/F ***/
                    else if (idSpecialità == 4 || idSpecialità == 5 || idSpecialità == 6)
                    {
                        if (idSpecialità == 4)
                        {
                            command.Parameters.AddWithValue("@sex", "D");
                            command.Parameters.AddWithValue("@spec", 4);
                            gara.sex = "D";
                        }
                        else if (idSpecialità == 5)
                        {
                            command.Parameters.AddWithValue("@spec", 5);
                            command.Parameters.AddWithValue("@sex", "M");
                            gara.sex = "M";
                        }
                        else if (idSpecialità == 6)
                        {
                            command.Parameters.AddWithValue("@spec", 6);
                            command.Parameters.AddWithValue("@sex", "F");
                            gara.sex = "F";
                        }

                        command.Parameters.AddWithValue("@category", idCategoria);

                        for (int i = 0; i < 3; i++)
                        {
                            CheckBox cbSegmento = ((CheckBox)this.Controls.Find("cb" + (i + 1), true)[0]);
                            if (cbSegmento.Checked)
                            {
                                Definizioni.idSegment = int.Parse(cbSegmento.Tag.ToString());
                                command.Parameters.AddWithValue("@idsegment", Definizioni.idSegment);
                                command.ExecuteNonQuery();
                                /****************************************************/
                                if (Definizioni.idSegment == 4) // style dance
                                {
                                    gara.segment1 = cbSegmento.Text;
                                    gara.idSegment1 = Definizioni.idSegment;
                                }
                                else
                                // 2.0.1 BUG FIX - 
                                //if (Definizioni.idSegment == 3) // free dance
                                if (Definizioni.idSegment == 5) // free dance
                                {
                                    gara.segment2 = cbSegmento.Text;
                                    gara.idSegment2 = Definizioni.idSegment;
                                }
                                else
                                    if (Definizioni.idSegment == 11) // CD1
                                {
                                    gara.segment3 = cbSegmento.Text;
                                    gara.idSegment3 = Definizioni.idSegment;
                                }
                                else
                                    if (Definizioni.idSegment == 12) // CD2
                                {
                                    gara.segment4 = cbSegmento.Text;
                                    gara.idSegment4 = Definizioni.idSegment;
                                }
                                /****************************************************/
                            }
                        }
                    }
                    /*** sincro/show ***/
                    else if (idSpecialità == 7)
                    {
                        if (idSpecialità == 7) command.Parameters.AddWithValue("@spec", 7);
                        command.Parameters.AddWithValue("@category", idCategoria);
                        command.Parameters.AddWithValue("@sex", "");
                        if (cb1.Checked) // Lungo
                        {
                            Definizioni.idSegment = int.Parse(cb1.Tag.ToString());
                            command.Parameters.AddWithValue("@idsegment", cb1.Tag);
                            command.ExecuteNonQuery();
                            /****************************************************/
                            gara.segment1 = cb1.Text;
                            gara.idSegment1 = Definizioni.idSegment;
                            gara.sex = "S";
                            /****************************************************/
                        }
                    }
                    pb1.Value++; pb1.Refresh();
                    int[] ids = new int[lv2.Items.Count];
                    /****************************************************/
                    gara.list = new List<Skater>(lv2.Items.Count);
                    gara.numSkaters = lv2.Items.Count;
                    /****************************************************/

                    bool RegionPresente = Utility.CheckIfColumnExists("Athletes", "Region");
                    bool speciaPresente = Utility.CheckIfColumnExists("Athletes", "ID_Specialita");

                    if (RegionPresente && speciaPresente)
                        command.CommandText = "INSERT INTO Athletes(Name, Societa, Country, Region, ID_Specialita)" +
                            " VALUES(@param1,@param2,@param3,@param4,@param5)";
                    else
                        command.CommandText = "INSERT INTO Athletes(Name, Societa, Country)" +
                            " VALUES(@param1,@param2,@param3)";
                    int k = 0;
                    for (int i = 0; i < lv2.Items.Count; i++)
                    {
                        command.Parameters.AddWithValue("@param1", lv2.Items[i].SubItems[1].Text.TrimEnd()); // nome
                        command.Parameters.AddWithValue("@param2", lv2.Items[i].SubItems[2].Text.TrimEnd()); // societa
                        command.Parameters.AddWithValue("@param3", lv2.Items[i].SubItems[4].Text.TrimEnd()); // nazione
                        if (RegionPresente && speciaPresente)
                        {
                            command.Parameters.AddWithValue("@param4", lv2.Items[i].SubItems[3].Text.TrimEnd()); // region
                            command.Parameters.AddWithValue("@param5", lv2.Items[i].SubItems[5].Text.TrimEnd()); // spec
                        }
                        try
                        {
                            command.ExecuteNonQuery();
                        }
                        catch (Exception)
                        {
                            //MessageBox.Show("Athlete " + lb.Items[i].SubItems[1].Text + " already inserted.");
                        }

                        /****************************************************/
                        gara.list.Add(new Skater());
                        gara.list[i].order = (i + 1);
                        gara.list[i].name = lv2.Items[i].SubItems[1].Text;
                        gara.list[i].club = lv2.Items[i].SubItems[2].Text;
                        gara.list[i].region = lv2.Items[i].SubItems[3].Text;
                        gara.list[i].nation = lv2.Items[i].SubItems[4].Text;
                        gara.list[i].discipline = lv2.Items[i].SubItems[5].Text;
                        /****************************************************/

                        // recupero l'ID dell'Atleta
                        if (RegionPresente && speciaPresente)
                            cmd = new SQLiteCommand("select ID_Atleta from Athletes " +
                                " WHERE Name = '" + lv2.Items[i].SubItems[1].Text.TrimEnd().Replace("'", "''") + "'" +
                                " AND Societa = '" + lv2.Items[i].SubItems[2].Text.TrimEnd().Replace("'", "''") + "'" +
                                " AND Country = '" + lv2.Items[i].SubItems[4].Text.TrimEnd().Replace("'", "''") + "'" +
                                " AND Region = '" + lv2.Items[i].SubItems[3].Text.TrimEnd().Replace("'", "''") + "'" +
                                " AND ID_Specialita = " + lv2.Items[i].SubItems[5].Text.Replace("'", "''") + "", Definizioni.conn);
                        else
                            cmd = new SQLiteCommand("select ID_Atleta from Athletes " +
                                " WHERE Name = '" + lv2.Items[i].SubItems[1].Text.TrimEnd().Replace("'", "''") + "'" +
                                " AND Societa = '" + lv2.Items[i].SubItems[2].Text.TrimEnd().Replace("'", "''") + "'" +
                                " AND Country = '" + lv2.Items[i].SubItems[4].Text.TrimEnd().Replace("'", "''") + "'", Definizioni.conn);
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            ids[k] = int.Parse(dr[0].ToString());
                            /****************************************************/
                            gara.list[i].idskater = ids[k];
                            /****************************************************/
                            k++;
                            break;
                            
                        }
                    }
                    pb1.Value++; pb1.Refresh();
                    for (int v = 1; v < 4; v++)
                    {
                        if (((CheckBox)this.Controls.Find("cb" + v, true)[0]).Checked)
                        {
                            command.CommandText = "INSERT INTO Participants(ID_GaraParams,ID_Segment,ID_Atleta,NumStartingList)" +
                                        " VALUES(@param1,@param2,@param3,@param4)";
                            command.Parameters.AddWithValue("@param1", Definizioni.idGaraParams);
                            command.Parameters.AddWithValue("@param2",
                                ((CheckBox)this.Controls.Find("cb" + v, true)[0]).Tag); // id_segment 

                            for (int i = 0; i < lv2.Items.Count; i++)
                            {
                                command.Parameters.AddWithValue("@param3", ids[i]); // ID_Atleta
                                command.Parameters.AddWithValue("@param4", (i + 1) + ""); // NumStartingList
                                command.ExecuteNonQuery();
                            }
                        }
                    }
                    pb1.Value++; pb1.Refresh();

                    // inserisco giudici
                    int[] idGiudici = new int[Judges.judges.Count];
                    /****************************************************/
                    gara.listOfficials = new List<Official>(Judges.judges.Count);
                    /****************************************************/
                    command.CommandText = "INSERT INTO Judges(Name, Region, Country)" +
                            " VALUES(@param1,'','')";
                    int b = 0;
                    for (int i = 0; i < Judges.judges.Count; i++)
                    {
                        if (!Judges.judges[i].ToString().TrimEnd().Equals(""))
                        {
                            // 2.0.0.10 - 31/01/2019 - Modifica che permette di non reinserire 
                            // un giudice già esistente

                            cmd = new SQLiteCommand("select ID_Judge from Judges " +
                                " WHERE Name = '" + Judges.judges[i].ToString().TrimEnd().Replace("'", "''")
                                + "'", Definizioni.conn);
                            dr = cmd.ExecuteReader();
                            if (dr.HasRows) // se esiste recupero solo idGiudice
                            {
                                while (dr.Read())
                                {
                                    idGiudici[b] = int.Parse(dr[0].ToString());
                                    b++;
                                    break;
                                }
                            }
                            else // altrimenti inserisco e poi recuper id
                            {
                                dr.Close();
                                command.Parameters.AddWithValue("@param1",
                                Judges.judges[i].ToString().TrimEnd()); // nome giudice
                                try
                                {
                                    command.ExecuteNonQuery();
                                }
                                catch (Exception)
                                {
                                    //MessageBox.Show("Judge " + Judges.judges[i].ToString() + " already inserted.");
                                }

                                // recupero l'ID del Giudice
                                cmd = new SQLiteCommand("select ID_Judge from Judges " +
                                    " WHERE Name = '" + Judges.judges[i].ToString().TrimEnd().Replace("'", "''")
                                    + "'", Definizioni.conn);
                                dr = cmd.ExecuteReader();
                                while (dr.Read())
                                {
                                    idGiudici[b] = int.Parse(dr[0].ToString());
                                    b++;
                                    break;
                                }
                            }
                            /****************************************************/

                            //command.Parameters.AddWithValue("@param1",
                            //    Judges.judges[i].ToString().TrimEnd()); // nome giudice
                            //try
                            //{
                            //    command.ExecuteNonQuery();
                            //}
                            //catch (Exception)
                            //{
                            //    //MessageBox.Show("Judge " + Judges.judges[i].ToString() + " already inserted.");
                            //}

                            //// recupero l'ID del Giudice
                            //cmd = new SQLiteCommand("select ID_Judge from Judges " +
                            //    " WHERE Name = '" + Judges.judges[i].ToString().TrimEnd().Replace("'", "''")
                            //    + "'", Definizioni.conn);
                            //dr = cmd.ExecuteReader();
                            //while (dr.Read())
                            //{
                            //    idGiudici[b] = int.Parse(dr[0].ToString());
                            //    b++;
                            //    break;
                            //}
                        } else
                        {
                            idGiudici[b] = 0;
                            b++;
                        }
                    }
                    pb1.Value++; pb1.Refresh();
                    command.CommandText = "INSERT INTO PanelJudge(ID_Judge, ID_GaraParams,Role)" +
                        " VALUES(@param3,@param1,@param2)";
                    command.Parameters.AddWithValue("@param1", Definizioni.idGaraParams);
                    for (int i = 0; i < Judges.judges.Count; i++)
                    {
                        if (i == 0) command.Parameters.AddWithValue("@param2", "Specialist"); // role
                        else if (i == 1) command.Parameters.AddWithValue("@param2", "Data Operator"); // role
                        else if (i == 2) command.Parameters.AddWithValue("@param2", "Assistant"); // role
                        else if (i == 3) command.Parameters.AddWithValue("@param2", "Controller"); // role
                        else if (i == 4) command.Parameters.AddWithValue("@param2", "Referee"); // role
                        else command.Parameters.AddWithValue("@param2", "Judge " + (i - 4)); // role
                        command.Parameters.AddWithValue("@param3", idGiudici[i]); // name
                        command.ExecuteNonQuery();

                        /****************************************************/
                        gara.listOfficials.Add(new Official());
                        gara.listOfficials[i].idofficial = idGiudici[i];
                        gara.listOfficials[i].name = Judges.judges[i].ToString().TrimEnd();
                        gara.listOfficials[i].role = command.Parameters["@param2"].Value.ToString();
                        /****************************************************/
                    }
                    pb1.Value++; pb1.Refresh();

                    // Export to xml
                    System.Xml.Serialization.XmlSerializer writer =
                        new System.Xml.Serialization.XmlSerializer(typeof(Competition));


                    var path = RollartSystemTech.Properties.Settings.Default.EventFolder +
                        "//event_" + "G" + Definizioni.idGaraParams + ".xml";
                        //"S" + Definizioni.idSpecialita +
                        //"C" + Definizioni.idCategoria + ".xml";

                    System.IO.FileStream file = System.IO.File.Create(path);

                    writer.Serialize(file, gara);
                    file.Close();

                    // Clear New Event page
                    ClearNewEvent();
                    message.Text = Definizioni.resources["event"].ToString().Replace("@","'" + Definizioni.nomeGara + "'");
                    pb1.Value = 0; pb1.Visible = false;

                    name.Focus(); 
                }

            }
            catch (Exception ex)
            {
                message.BackColor = Color.Yellow;
                message.Text = ex.Message;
                pb1.Value = 0; pb1.Visible = false;
            }
        }
        
        // nuovo evento
        public void ClearNewEvent()
        {
            name.Clear();
            place.Clear();
            dateFrom.Value = DateTime.Now;
            dateTo.Value = DateTime.Now;
            //lv1.Items.Clear(); // lascio i giudici
            lv2.Items.Clear();
            confirmNewEvent.Enabled = false;
            cb1.Visible = false; cb2.Visible = false; cb3.Visible = false;
            
            // recupero le specialitÃ
            cbEvent.Items.Clear();
            cbEvent.Items.AddRange(ev.GetSpecialita().ToArray());

            // recupero le categorie
            cbCategory.Items.Clear();
            cbCategory.Items.AddRange(ev.GetCategory().ToArray());
        }
        
        // select judges
        private void browse_Click(object sender, EventArgs e)
        {
            try
            { 
                ju.ShowDialog();
                if (Judges.judges == null) return;
            
                flagJudgesInserted = true;
                lv1.Items.Clear();
                for (int i = 0; i < Judges.judges.Count; i++)
                {
                    if (i == 0)
                    {
                        lv1.Items.Add("Technical Specialist: " + Judges.judges[i].ToString(), 0);
                        //lv1.Items[0].ToolTipText = "Technical Specialist";
                    }
                    else if (i == 1)
                    {
                        lv1.Items.Add("Data Operator: " + Judges.judges[i].ToString(), 0);
                        //lv1.Items[1].ToolTipText = "Data Operator";
                    }
                    else if (i == 2)
                    {
                        if (!Judges.judges[i].ToString().Equals(""))
                        {
                            lv1.Items.Add("Assistant: " + Judges.judges[i].ToString(), 0);
                            //lv1.Items[2].ToolTipText = "Assistant";
                        }
                    }
                    else if (i == 3)
                    {
                        if (!Judges.judges[i].ToString().Equals(""))
                        {
                            lv1.Items.Add("Controller: " + Judges.judges[i].ToString(), 0);
                            //lv1.Items[2].ToolTipText = "Controller";
                        }
                    }
                    else if (i == 4)
                    {
                        if (!Judges.judges[i].ToString().Equals(""))
                        {
                            lv1.Items.Add("Referee: " + Judges.judges[i].ToString(), 0);
                            //lv1.Items[2].ToolTipText = "Referee";
                        }
                    }
                    else 
                    {
                        lv1.Items.Add("Judge " + (i - 4) + ": " + Judges.judges[i].ToString(), 1);
                        //lv1.Items[i].ToolTipText = "Judge " + (i - 4);
                    }
                }
                if (Definizioni.numPartecipanti != 0)
                {
                    confirmNewEvent.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
}

        #endregion

        #region START EVENT

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                buTech.BackColor = Color.Transparent;
                button1.BackColor = Color.FromArgb(175, 202, 11); button2.BackColor = Color.Transparent;
                button2.BackColor = Color.Transparent; button5.BackColor = Color.Transparent;
                panStartEvent.Visible = true;
                panStartEvent.BringToFront();
                titolo.Text = "Events Management --> Start Event";
                titolo.ImageIndex = 21;
                LoadEvents();
                message.Text = "";
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        public void LoadEvents()
        {
            try
            {
                tv.Nodes[0].Nodes.Clear();
                SQLiteDataReader dr = null;
                int numEvents = 0;
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT DISTINCT G.Name, G.Place, G.Date, G.ID_GaraParams, S.Name, C.Name " +
                        " FROM GaraParams G, Specialita S, Category C" +
                        " WHERE G.ID_Specialita = S.ID_Specialita AND G.ID_Category = C.ID_Category";
                    dr = command.ExecuteReader();
                    TreeNode tn = null;
                    while (dr.Read())
                    {
                        tn = new TreeNode();
                        tn.Text = dr[0].ToString();
                        tn.ToolTipText = dr[1].ToString() + " - " + dr[2].ToString();
                        tn.Name = dr[3].ToString(); // id_Garaparams
                        tn.ImageIndex = 41;
                        tn.SelectedImageIndex = 47;
                        tv.Nodes[0].Nodes.Add(tn);
                        numEvents++;
                    }
                    dr.Close();

                    gara.Visible = false;
                    connect.Visible = false;
                    save.Visible = false;

                    tv.Nodes[0].Text = "Events (" + numEvents + ")    ";
                    tv.Nodes[0].ToolTipText = RollartSystemTech.Properties.Settings.Default.dbfolder + "\\rolljudge2.s3db";
                    tv.ExpandAll();
                    tv.Nodes[0].ExpandAll();
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        // Reload
        private void reload_Click(object sender, EventArgs e)
        {
            LoadEvents();
        }

        // Reset
        private void reset_Click(object sender, EventArgs e)
        {
            if (tv.SelectedNode.Level == 0) return;
            DialogResult res = MessageBox.Show(Definizioni.resources["event3"].ToString() + " '" +
                 tv.SelectedNode.Text + "' ?", "RollartSystem", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning,
                    MessageBoxDefaultButton.Button2);

            if (res == DialogResult.Yes)
            {
                try
                {
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "DELETE FROM Gara WHERE ID_GaraParams = " + tv.SelectedNode.Name;
                        command.ExecuteNonQuery();
                        command.Cancel();
                        command.CommandText = "DELETE FROM GaraFinal WHERE ID_GaraParams = " + tv.SelectedNode.Name;
                        command.ExecuteNonQuery();
                        command.Cancel();
                        command.CommandText = "DELETE FROM GaraTotal WHERE ID_GaraParams = " + tv.SelectedNode.Name;
                        command.ExecuteNonQuery();
                    }
                    using (SQLiteCommand commandUpdate = Definizioni.conn.CreateCommand())
                    {
                        commandUpdate.CommandText = "UPDATE GaraParams SET LastPartecipant = 0, Completed = 'N'" +
                            " WHERE ID_GaraParams = " + tv.SelectedNode.Name + "";
                        commandUpdate.ExecuteNonQuery();
                    }
                    LoadEvents();
                }
                catch (SQLiteException ex)
                {
                    message.Text = ex.Message;
                }
                catch (Exception ex)
                {
                    message.Text = ex.Message;
                }

            }

        }

        // Delete Competition
        private void deletegare_Click(object sender, EventArgs e)
        {
            
            if (tv.SelectedNode.Level == 0) return;

            //*** 2.0 gestione cartelle
            //if (tv.SelectedNode.Tag != null)
            //{
            //    if (tv.SelectedNode.Tag.Equals("folder"))
            //    {
            //        // cartella vuota elimino e basta
            //        if (tv.SelectedNode.Nodes.Count == 0)
            //        {
            //            System.IO.Directory.Delete(
            //                    RollartSystemTech.Properties.Settings.Default.EventFolder + "\\" + tv.SelectedNode.Text);
            //            tv.Nodes.Remove(tv.SelectedNode);
            //        }
            //        // se cartella non vuota sposto prima gli eventi e poi cancello
            //        else
            //        {

            //        }

            //        LoadEvents();
            //    }
            //}

            //// singoli eventi
            //else
            {
                DialogResult res = MessageBox.Show(Definizioni.resources["event4"].ToString() + " '" +
                tv.SelectedNode.Text + "' ?", "RollartSystem", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning,
                MessageBoxDefaultButton.Button2);

                if (res == DialogResult.Yes)
                {
                    try
                    {
                        using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                        {
                            command.CommandText = "DELETE FROM Gara WHERE ID_GaraParams = " + tv.SelectedNode.Name;
                            command.ExecuteNonQuery();
                            command.Cancel();
                            command.CommandText = "DELETE FROM GaraParams WHERE ID_GaraParams = " + tv.SelectedNode.Name;
                            command.ExecuteNonQuery();
                            command.Cancel();
                            command.CommandText = "DELETE FROM GaraFinal WHERE ID_GaraParams = " + tv.SelectedNode.Name;
                            command.ExecuteNonQuery();
                            command.Cancel();
                            command.CommandText = "DELETE FROM GaraTotal WHERE ID_GaraParams = " + tv.SelectedNode.Name;
                            command.ExecuteNonQuery();
                            command.Cancel();
                            command.CommandText = "DELETE FROM PanelJudge WHERE ID_GaraParams = " + tv.SelectedNode.Name;
                            command.ExecuteNonQuery();
                            command.Cancel();
                            command.CommandText = "DELETE FROM Participants WHERE ID_GaraParams = " + tv.SelectedNode.Name;
                            command.ExecuteNonQuery();
                        }

                        tv.Nodes.Remove(tv.SelectedNode);
                        LoadEvents();
                    }
                    catch (SQLiteException ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        // Delete all competitions
        private void deleteall_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show(Definizioni.resources["event9"].ToString() + " ?", "RollartSystem", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning,
                    MessageBoxDefaultButton.Button2);

            if (res == DialogResult.Yes)
            {
                try
                {
                    foreach (string idgara in tv.Nodes[0].Nodes)
                    {
                        using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                        {
                            command.CommandText = "DELETE FROM Gara WHERE ID_GaraParams = " + idgara;
                            command.ExecuteNonQuery();
                            command.Cancel();
                            command.CommandText = "DELETE FROM GaraParams WHERE ID_GaraParams = " + idgara;
                            command.ExecuteNonQuery();
                            command.Cancel();
                            command.CommandText = "DELETE FROM GaraFinal WHERE ID_GaraParams = " + idgara;
                            command.ExecuteNonQuery();
                            command.Cancel();
                            command.CommandText = "DELETE FROM GaraTotal WHERE ID_GaraParams = " + idgara;
                            command.ExecuteNonQuery();
                            command.Cancel();
                            command.CommandText = "DELETE FROM PanelJudge WHERE ID_GaraParams = " + idgara;
                            command.ExecuteNonQuery();
                            command.Cancel();
                            command.CommandText = "DELETE FROM Participants WHERE ID_GaraParams = " + idgara;
                            command.ExecuteNonQuery();
                            tv.Nodes[0].Nodes.RemoveByKey(idgara);
                        }
                    }

                    LoadEvents();
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        public void SetInvisibleButtons()
        {
            //n1.Text = ""; n2.Text = ""; n3.Text = "";
            b1.Visible = false; b2.Visible = false; b3.Visible = false;
            b4.Visible = false; b5.Visible = false; b6.Visible = false;
            b7.Visible = false; b8.Visible = false; b9.Visible = false;
            l9.Visible = false; l10.Visible = false; l11.Visible = false;
            lv3.Items.Clear(); lv3.Visible = false;
            o1.Visible = false; o2.Visible = false; o3.Visible = false;
            check1.Visible = false; check2.Visible = false; check3.Visible = false;

        }

        // selezione gara
        private void tv_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            SelectNode(false, null, e);
        }

        public void SelectNode(bool afterSelect, TreeViewEventArgs e, TreeNodeMouseClickEventArgs eClick)
        {
            try
            {
                connect.Visible = false;
                save.Visible = true;
                styleDance = false;
                compulsory1 = false;
                compulsory2 = false;
                bool changeJudge = true;

                if (eClick != null)
                {
                    tv.SelectedNode = eClick.Node;
                    if (eClick.Button == MouseButtons.Right) menu.Items.Clear();
                }

                TreeNode nodoSelezionato = null;

                if (e != null) nodoSelezionato = e.Node;
                else if (eClick != null) nodoSelezionato = eClick.Node;
                else nodoSelezionato = tv.SelectedNode;

                indexNodoSelezionato = nodoSelezionato.Index;

                if (nodoSelezionato.Level == 0)
                {
                    backup.Enabled = true;
                    restore.Enabled = true;

                    gara.Visible = false;

                    removeJudge.Visible = false;
                    newJudge.Visible = false;
                    upJudge.Visible = false;
                    downJudge.Visible = false;
                    
                    tv.ContextMenuStrip = menu2;
                }
                ////*** 2.0 nuova gestione delle cartelle
                //else if (nodoSelezionato.Level > 0 && nodoSelezionato.Tag != null)
                //{
                //    backup.Enabled = false;
                //    restore.Enabled = false;
                //    deletegare.Enabled = true;
                //    reset.Enabled = false;

                //    tv.ContextMenuStrip = null;
                //}
                else if (nodoSelezionato.Level > 0)
                {
                    backup.Enabled = false;
                    restore.Enabled = false;
                    deletegare.Enabled = true;
                    reset.Enabled = true;

                    tv.ContextMenuStrip = menu;

                    gara.Visible = true;
                    SetInvisibleButtons();
                    SQLiteDataReader dr = null;
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "SELECT " +
                            " G.Name, G.Date, G.DateEnd, G.Place, G.NumJudges, G.Partecipants, " +
                            " C.Name, S.Name, SP.Name, G.ID_GaraParams, G.Completed, G.LastPartecipant, " +
                            " G.ID_Specialita, G.ID_Segment, G.ID_Category" +
                            " FROM GaraParams G, Segments S, Category C, Specialita SP" +
                            " WHERE G.ID_Category = C.ID_Category AND " +
                            " G.ID_Specialita = SP.ID_Specialita AND" +
                            " G.ID_Segment = S.ID_Segments AND " +
                            " G.ID_GaraParams = " + nodoSelezionato.Name;
                        dr = command.ExecuteReader();
                        while (dr.Read())
                        {
                            l1.Text = dr[0].ToString(); // name
                            l2.Text = dr[1].ToString(); // date
                            l3.Text = dr[2].ToString(); // date end
                            l4.Text = dr[3].ToString(); // place
                            l5.Text = dr[4].ToString(); // num judges
                            l6.Text = dr[5].ToString(); // num part
                            l8.Text = dr[6].ToString(); // categoria
                            l7.Text = dr[8].ToString(); // spec
                            Definizioni.idGaraParams = int.Parse(dr[9].ToString());
                            Definizioni.idSpecialita = int.Parse(dr[12].ToString());
                            Definizioni.idCategoria = int.Parse(dr[14].ToString());
                            Definizioni.numJudges = int.Parse(dr[4].ToString());
                            Definizioni.numPart = int.Parse(dr[5].ToString());
                            Definizioni.lastPart = int.Parse(dr[11].ToString());
                            int last = int.Parse(dr[11].ToString());
                            int idSegm = int.Parse(dr[13].ToString());
                            Definizioni.completed = dr[10].ToString();

                            l20.Text = "G" + Definizioni.idGaraParams +
                                       "S" + Definizioni.idSpecialita +
                                       "C" + Definizioni.idCategoria;
                            if (Definizioni.numJudges > 0)
                            {
                                //connect.Visible = true;
                                //Definizioni.clientsJudge = new TcpClient[Definizioni.numJudges];
                            }
                            ViewControls(dr, Definizioni.completed, idSegm, last);

                            // aggiungo voci al menu contestuale
                            if (eClick != null)
                            {
                                if (eClick.Button == MouseButtons.Right)
                                {
                                    menu.Tag = Definizioni.idGaraParams;
                                    menu.Items.Add("" + dr[7].ToString());
                                    if (!Definizioni.completed.Equals("Y"))
                                        menu.Items[menu.Items.Count - 1].Enabled = false;
                                    else
                                    {
                                        menu.Items[menu.Items.Count - 1].Tag = idSegm;
                                    }
                                }
                            }

                            // 2.0.0.10 - 23/01/2019 
                            //verifico se abilitare i tasti del cambio giudice
                            if (Definizioni.lastPart > 0)
                                changeJudge = false;
                        }
                        if (styleDance)
                        {
                            l9.Visible = false;
                            labPattern.Visible = true; cbPattern.Visible = true;
                        }
                        if (compulsory1)
                        {
                            l9.Visible = false;
                            cbCD1.Visible = true;
                            cbCD1.SelectedIndex = 0;
                        }
                        if (compulsory2)
                        {
                            l10.Visible = false;
                            cbCD2.Visible = true;
                            cbCD2.SelectedIndex = 0;
                        }
                        dr.Close();

                        
                    }
                    judges.PerformClick();

                    if (eClick != null)
                    {
                        if (eClick.Button == MouseButtons.Right)
                        {
                            bool completedComp = true;
                            foreach (ToolStripItem item in menu.Items)
                            {
                                if (!item.Enabled)
                                {
                                    completedComp = false;
                                    break;
                                }
                            }
                            if (completedComp)
                            {
                                menu.Items.Add(new ToolStripSeparator());
                                menu.Items.Add("Export All");

                                for (int i = 0; i < menu.Items.Count - 1; i++)
                                {
                                    if (menu.Items[i].Tag != null)
                                        menu.Items[menu.Items.Count - 1].Tag += menu.Items[i].Tag.ToString() + ";";
                                }


                            }
                            menu.Show();
                        }
                    }
                }

                // 2.0.0.10 - 23/01/2019 - inserito per poter modificare i giudici
                if (changeJudge)
                {
                    removeJudge.Visible = true;
                    newJudge.Visible = true;
                    upJudge.Visible = true;
                    downJudge.Visible = true;
                } else
                {
                    removeJudge.Visible = false;
                    newJudge.Visible = false;
                    upJudge.Visible = false;
                    downJudge.Visible = false;
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void tv_AfterSelect(object sender, TreeViewEventArgs e)
        {
            SelectNode(true, e, null);
        }

        // menu contestuale
        private void menu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                
                ToolStripItem tsi = e.ClickedItem;
                string folderName = "";
                int countReports = 1, countSegments = 0; 
                string idGara = menu.Tag.ToString();
                string[] idSegments = tsi.Tag.ToString().TrimEnd(';').Split(';');

                menu.Close();

                DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                if (recentFolder.Equals(""))
                    fd1.SelectedPath = RollartSystemTech.Properties.Settings.Default.EventFolder;
                else fd1.SelectedPath = recentFolder;
                if (fd1.ShowDialog() == DialogResult.OK)
                {
                    folderName = fd1.SelectedPath;
                    recentFolder = fd1.SelectedPath;
                }
                else return;

                pb.Visible = true;
                pb.Refresh();
                pb.Update();
                label5.Visible = true;
                label5.Text = "Waiting..."; label5.Refresh(); label5.Update();

                if (!menu.Items[0].Enabled) // non abilitato (primo segmento di gara non presente)
                    countSegments++;

                ExportOptions ex = new ExportOptions();
                ex.ExportDestinationOptions = dest;
                ex.ExportDestinationType = ExportDestinationType.DiskFile;
                ex.ExportFormatType = ExportFormatType.PortableDocFormat;

                int vociMenu = 0;
                foreach (string idsegment in idSegments)
                {
                    // pannello

                    this.cr.ReportSource = this.PanelReport1;
                    cr.SelectionFormula = "{Classifica_Intestazione1.ID_GaraParams} = " + idGara +
                            " AND {Classifica_Intestazione1.ID_Segment} = " + idsegment;

                    // 2.0.0.10
                    //dest.DiskFileName = folderName + "\\" + l7.Text + "_" + l8.Text.Substring(0, 3) + "_" + menu.Items[countSegments].Text + 
                    //"_Panel.pdf";
                    if (e.ClickedItem.Text.Equals("Export All"))
                        dest.DiskFileName = fd1.SelectedPath + "\\" + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year +
                    " PANEL - " + l7.Text + " " + l8.Text + " " + menu.Items[vociMenu].Text.ToUpper() + ".pdf";//
                    else dest.DiskFileName = fd1.SelectedPath + "\\" + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year + 
                    " PANEL - " + l7.Text + " " + l8.Text + " " + e.ClickedItem.Text.ToUpper() + ".pdf";

                    ex.ExportDestinationOptions = dest;
                    this.cr.RefreshReport();
                    PanelReport1.Export(ex);
                    label5.Text = "" + countReports++ + " exported"; label5.Refresh(); label5.Update();
                    // 2.0.0.10
                    Thread.Sleep(500);

                    // classifica
                    this.cr.ReportSource = this.ResultReport1;
                    cr.SelectionFormula = "{Classifica_Intestazione1.ID_GaraParams} = " + idGara +
                            " AND {Classifica_Intestazione1.ID_Segment} = " + idsegment;
                    // 2.0.0.10
                    //dest.DiskFileName = folderName + "\\" + l7.Text + "_" + l8.Text.Substring(0, 3) + "_" + menu.Items[countSegments].Text +
                    //   "_Results.pdf";
                    if (e.ClickedItem.Text.Equals("Export All"))
                        dest.DiskFileName = fd1.SelectedPath + "\\" + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year +
                    " RESULTS - " + l7.Text + " " + l8.Text + " " + menu.Items[vociMenu].Text.ToUpper() + ".pdf";//
                    else dest.DiskFileName = fd1.SelectedPath + "\\" + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year +
                    " RESULTS - " + l7.Text + " " + l8.Text + " " + e.ClickedItem.Text.ToUpper() + ".pdf";

                    ex.ExportDestinationOptions = dest;
                    this.cr.RefreshReport();
                    ResultReport1.Export(ex);
                    label5.Text = "" + countReports++ + " exported"; label5.Refresh(); label5.Update();
                    countSegments++;

                    // 2.0.0.10
                    Thread.Sleep(500);
                    vociMenu++;
                }

                // final
                this.cr.ReportSource = this.FinalReport1;
                cr.SelectionFormula = "{GaraTotal1.ID_GaraParams} = " + idGara + "";
                // 2.0.0.10
                //dest.DiskFileName = folderName + "\\" + l7.Text + "_" + l8.Text.Substring(0, 3) + "" +
                //"_Final.pdf";
                dest.DiskFileName = fd1.SelectedPath + "\\" + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year +
                    " FINAL - " + l7.Text + " " + l8.Text + ".pdf";

                ex.ExportDestinationOptions = dest;
                this.cr.RefreshReport();
                FinalReport1.Export(ex);
                label5.Text = "" + countReports++ + " exported"; label5.Refresh(); label5.Update();

                pb.Visible = false;
                label5.Visible = false;
                //MessageBox.Show("Reports exported correctly.");

                // OPEN FOLDER
                Process.Start("explorer.exe", folderName);

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                pb.Visible = false;
                label5.Visible = false;
            }
            

        }

        public void ViewControls(SQLiteDataReader dr, string completed, int idSegm, int lastPart)
        {
            labPattern.Visible = false; cbPattern.Visible = false;
            cbCD1.Visible = false; cbCD2.Visible = false;
            panStyle.Location = new Point(7, 420);

            if (idSegm == 1 || idSegm == 6 || idSegm == 11)
            {
                l9.Visible = true;
                l9.Text = dr[7].ToString();
                b7.Visible = true; l9.Visible = true;
                b7.ForeColor = Color.DeepSkyBlue;
                b7.Tag = Definizioni.idGaraParams + "_" + idSegm;
                b1.Text = "Start";
                if (completed.Equals("Y"))
                {
                    b2.Visible = true;
                    b2.Enabled = true; b2.ForeColor = Color.DeepSkyBlue;
                    b2.Tag = idSegm;
                    o1.Text = "Results";
                    o1.ForeColor = Color.Orange;
                    //check1.Visible = true;
                }
                else
                {
                    b1.Visible = true;
                    //b1.Enabled = false;
                    b1.ForeColor = Color.DeepSkyBlue;
                    b1.Tag = idSegm + "_" + lastPart;
                    if (idSegm == 11) compulsory1 = true;
                    if (lastPart != 0) b1.Text = "Start (from " + (lastPart + 1) + ")";
                    o1.Text = "Skating Order";
                    o1.ForeColor = Color.DeepSkyBlue;
                    check1.Visible = false;
                }
                o1.Visible = true;
                o1.Tag = idSegm;
                check1.Tag = idSegm;
            }
            if (idSegm == 2 || idSegm == 4 || idSegm == 12)
            {
                l10.Text = dr[7].ToString();
                b8.Visible = true; l10.Visible = true;
                b8.Enabled = true; b8.ForeColor = Color.DeepSkyBlue;
                b8.Tag = Definizioni.idGaraParams + "_" + idSegm;
                b3.Text = "Start";
                if (completed.Equals("Y"))
                {
                    b4.Visible = true;
                    b4.Enabled = true; b4.ForeColor = Color.DeepSkyBlue;
                    b4.Tag = idSegm;
                    o2.Text = "Results";
                    o2.ForeColor = Color.Orange;
                    //check2.Visible = true;
                }
                else
                {
                    b3.Visible = true; 
                    b3.Tag = idSegm + "_" + lastPart;
                    if (idSegm == 4) styleDance = true;
                    else if (idSegm == 12) compulsory2 = true;
                    if (lastPart != 0) b3.Text = "Start (from " + (lastPart + 1) + ")";
                    o2.Text = "Skating Order";
                    o2.ForeColor = Color.DeepSkyBlue;
                    check2.Visible = false;
                }
                o2.Visible = true;
                o2.Tag = idSegm;
                check2.Tag = idSegm;
            }
            if (idSegm == 5)
            {
                l11.Text = dr[7].ToString();
                b9.Visible = true; l11.Visible = true;
                b9.Enabled = true; b9.ForeColor = Color.DeepSkyBlue;
                b9.Tag = Definizioni.idGaraParams + "_" + idSegm;
                b5.Text = "Start";
                if (completed.Equals("Y"))
                {
                    b6.Visible = true;
                    b6.Enabled = true; b6.ForeColor = Color.DeepSkyBlue;
                    b6.Tag = idSegm;
                    o3.Text = "Results";
                    o3.ForeColor = Color.Orange;
                    //check3.Visible = true;
                }
                else
                {
                    b5.Visible = true;
                    b5.Tag = idSegm + "_" + lastPart;
                    if (lastPart != 0) b5.Text = "Start (from " + (lastPart + 1) + ")";
                    o3.Text = "Skating Order";
                    o3.ForeColor = Color.DeepSkyBlue;
                    check3.Visible = false;
                }
                o3.Visible = true;
                o3.Tag = idSegm;
                check3.Tag = idSegm;
            }


        }

        private void l4_TextChanged(object sender, EventArgs e)
        {
            save.Enabled = true;
        }

        private void l1_TextChanged(object sender, EventArgs e)
        {
            save.Enabled = true;
        }

        // update competition
        private void save_Click(object sender, EventArgs e)
        {
            try
            {
                using (SQLiteCommand commandUpdate = Definizioni.conn.CreateCommand())
                {
                    commandUpdate.CommandText = "UPDATE GaraParams SET Name = '" + l1.Text.ToString().Replace("'", "''") + "', Place = '" + l4.Text.ToString().Replace("'", "''") +
                        "' WHERE ID_GaraParams = " + Definizioni.idGaraParams + "";
                    commandUpdate.ExecuteNonQuery();
                    save.Enabled = false;
                }
                reload.PerformClick();
            }
            catch (SQLiteException ex)
            {
                message.Text = ex.Message;
            }

        }

        // Judge panel
        private void button2_Click(object sender, EventArgs e)
        {
            lv3.Visible = false;
        }

        private void judges_Click(object sender, EventArgs e)
        {
            try
            {
                message.Text = "";
                if (lv3.Visible) lv3.Visible = false;
                else
                {
                    lv3.Visible = true;
                    lv3.BringToFront();
                }
                lv3.Items.Clear();
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT Name, Role, P.ID_Judge, Country " +
                        " FROM PanelJudge P, Judges J WHERE P.ID_Judge = J.ID_Judge" +
                        " AND ID_GaraParams = " + Definizioni.idGaraParams;
                    SQLiteDataReader dr = command.ExecuteReader();
                    ListViewItem lvi = null;
                    int countGiudici = 0;
                    while (dr.Read())
                    {
                        // *** modifica del 24/01/2019 - 2.0.0.10 aggiungo ID giudice ****//
                        // *** modifica del 28/01/2019 - 2.0.0.10 aggiungo Country    ****//
                        lvi = new ListViewItem(new string[] { "", "", "", "", "" }, -1);
                        if (dr[1].ToString().StartsWith("Judge"))
                        {
                            // *** modifica del 28/06/2018 - 1.0.4 ****//
                            // verifico se giudice già connesso
                            lvi.SubItems[3].Text = "NOT Connected";
                            lvi.ImageIndex = 3;
                            lvi.SubItems[0].Text = dr[0].ToString(); // nome giudice
                            //2.0.0.10
                            lvi.SubItems[1].Text = dr[3].ToString(); // country 
                            if (jclient.list[countGiudici].state == "1")
                            {
                                lvi.ImageIndex = 2;
                                lvi.SubItems[3].Text = "Connected";
                            }
                            
                            countGiudici++;
                        }
                        else
                        {
                            lvi.ImageIndex = 0;
                            lvi.SubItems[0].Text = dr[0].ToString();
                            lvi.SubItems[1].Text = dr[3].ToString(); // country 
                        }
                        lvi.SubItems[2].Text = dr[1].ToString();
                        lvi.ToolTipText = dr[1].ToString();
                        // *** modifica del 24/01/2019 - 2.0.0.10 aggiungo ID giudice ****//
                        lvi.SubItems[4].Text = dr[2].ToString(); // ID_Judge
                        lv3.Items.Add(lvi);

                    }
                    dr.Close();
                }

                // se segmento già finito
                //if (Definizioni.completed.Equals("Y"))
                //{
                //    for (int i = 0; i < Definizioni.numJudges; i++)
                //    {
                //        lv3.Items[3 + i].ImageIndex = 1;
                //        lv3.Items[3 + i].SubItems[2].Text = "";
                //    }
                //}


            }
            catch (SQLiteException ex)
            {
                message.Text = ex.Message;
            }

        }

        // Start Event
        public void StartEvent(Button bStart)
        {
            try
            {
                // *** modifica del 28/06/2018 - 1.0.4 ****//
                // verifico se tutti i giudici sono connessi
                string giudiciNonConnessi = "Following judges are NOT Connected: \r\n";
                bool notReady = false;
                int numGiudiciGara = 0;

                Definizioni.numJudges = int.Parse(l5.Text);

                foreach (JudgeClient jc in jclient.list)
                {
                    if (jc.state != "1" && (numGiudiciGara + 1) <= Definizioni.numJudges)
                    {
                        giudiciNonConnessi += "- Judge " + (numGiudiciGara + 1) + "\r\n";
                        notReady = true;
                    }
                    numGiudiciGara++;
                }

                if (notReady)
                {
                    MessageBox.Show(giudiciNonConnessi, "RollArt", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                string stEvento = "";
                message.Text = "";
                Definizioni.idSegment = int.Parse(bStart.Tag.ToString().Split('_')[0]);
                Definizioni.lastPart = int.Parse(bStart.Tag.ToString().Split('_')[1]);
                // verifica style
                if (cbPattern.SelectedIndex
                    < 0 && bStart == b3 && Definizioni.idSegment == 4)
                {
                    message.Text = Definizioni.resources["event7"].ToString();
                    cbPattern.Focus();
                    return;
                }
                // verifica compulsory
                if (cbCD1.SelectedIndex <= 0 && bStart == b1 && Definizioni.idSegment == 11)
                {
                    message.Text = Definizioni.resources["event7"].ToString();
                    cbCD1.Focus();
                    return;
                }
                if (cbCD2.SelectedIndex <= 0 && bStart == b3 && Definizioni.idSegment == 12)
                {
                    message.Text = Definizioni.resources["event7"].ToString();
                    cbCD2.Focus();
                    return;
                }

                // se gara già iniziata faccio decidere da quale atleta partire
                //if (Definizioni.lastPart > 0)
                //{
                //    StartSegment ss = new StartSegment();
                //    ss.ShowDialog();
                //    if (Definizioni.dispose) return;
                //}

                if (bStart == b1) stEvento = l9.Text;
                else if (bStart == b3) stEvento = l10.Text;
                else if (bStart == b5) stEvento = l11.Text;
                Definizioni.dettaglioGara = l1.Text + "@" + l2.Text + "@" + l4.Text + "@" +
                            l7.Text + "@" + l8.Text + "@" + l5.Text + "@" + l6.Text + "@" + stEvento;

                Utility.WriteLog("********************************************************************************************", "OK");
                Utility.WriteLog("* " + l1.Text.ToUpper() + " ", "OK"); // nome gara
                Utility.WriteLog("* " + l2.Text.ToUpper() + " - " + l4.Text + "          Judges: " + l5.Text + " Skaters: " + l6.Text + " ", "OK"); // data - place - num judges - num skaters
                Utility.WriteLog("* " + l7.Text.ToUpper() + " " + l8.Text + " " + stEvento.ToUpper() + " ", "OK"); // discipline category segment
                Utility.WriteLog("********************************************************************************************", "OK");

                //2.0.0.10
                exitCheck = true;

                switch (Definizioni.idSegment)
                {
                    case 1:
                    case 2: // Lungo
                        if (Definizioni.idSpecialita == 3) // Coppie Artistico
                        {
                            cf = new CoppieForm();
                            cf.ShowDialog();
                        }
                        else // Singolo
                        {
                            sps = new SingoloForm();
                            sps.ShowDialog();
                        }
                        break;
                    case 3:
                    case 4:
                    case 5:
                    case 11:
                    case 12:
                    case 6: // Danza - Solo Dance - Precision
                        if (Definizioni.idSegment == 4) //style dance
                            Definizioni.currentPattern = cbPattern.SelectedItem.ToString();
                        else if (Definizioni.idSegment == 11) // compulsory 1
                        {
                            Definizioni.currentPattern = cbCD1.SelectedItem.ToString();
                        }
                        else if (Definizioni.idSegment == 12) // compulsory 1
                        {
                            Definizioni.currentPattern = cbCD2.SelectedItem.ToString();
                        }

                        df = new DanzaForm();
                        df.ShowDialog();
                        break;
                    default:
                        break;
                }

                reload.PerformClick();
                //b1.Enabled = false; b3.Enabled = false; b5.Enabled = false;
                //this.Show();

                // **** 1.0.4 Modifica del 27/06/2018 **** //
                // Backup db
                //File.Copy(RollartSystemTech.Properties.Settings.Default.dbfolder + "\\" +
                //                Definizioni.conn.DataSource + ".s3db",
                //          RollartSystemTech.Properties.Settings.Default.BackupFolder + "\\" +
                //                Definizioni.conn.DataSource + "_" + DateTime.Now.ToShortTimeString() + "_" +
                //                DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Year + ".s3db");

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void b1_Click(object sender, EventArgs e)
        {
            StartEvent(b1);
        }

        private void b3_Click(object sender, EventArgs e)
        {
            StartEvent(b3);
        }

        private void b5_Click(object sender, EventArgs e)
        {
            StartEvent(b5);
        }

        // skating order
        public void SkatingOrder(Button bSkOrder)
        {
            SkatingOrder so = new SkatingOrder(bSkOrder.Tag);
            so.ShowDialog();
            l6.Text = Definizioni.numPart + "";
        }

        private void o1_Click(object sender, EventArgs e)
        {
            SkatingOrder(o1);
        }

        private void o2_Click(object sender, EventArgs e)
        {
            SkatingOrder(o2);
        }

        private void o3_Click(object sender, EventArgs e)
        {
            SkatingOrder(o3);
        }

        // Event Results
        public void Results(Button bResults)
        {
            try
            {
                back.Visible = true;
                panel.Visible = true;
                classifica.Visible = true;
                result.Visible = true;
                reload.Enabled = false;
                connect.Visible = false;
                save.Visible = false;
                reset.Enabled = false;
                deletegare.Enabled = false;
                Definizioni.idSegment = int.Parse(bResults.Tag.ToString());
                resultsPanel.Visible = true;
                resultsPanel.BringToFront();
                
                this.cr.ReportSource = null;
                this.cr.RefreshReport();
                classifica.PerformClick();

                // 2.0.0.10 - 28/01/2019 aggiungo label non visibile per salvarmi il segment name
                if (bResults == b2) segmentName.Text = l9.Text;
                else if (bResults == b4) segmentName.Text = l10.Text;
                else if (bResults == b6) segmentName.Text = l11.Text;
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void b2_Click(object sender, EventArgs e)
        {
            Results(b2);
        }

        private void b4_Click(object sender, EventArgs e)
        {
            Results(b4);
        }

        private void b6_Click(object sender, EventArgs e)
        {
            Results(b6);
        }

        private void back_Click(object sender, EventArgs e)
        {
            back.Visible = false;
            panel.Visible = false;
            classifica.Visible = false;
            result.Visible = true;
            reload.Enabled = true;
            //connect.Visible = true;
            save.Visible = true;
            reset.Enabled = true;
            deletegare.Enabled = true;
            resultsPanel.Visible = false;
        }

        // pannello giudici
        private void panel_Click(object sender, EventArgs e)
        {
            try
            {
                cr.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
                //this.PanelReport1.DataSourceConnections[0].SetConnection(strServer, strDatabase, strUserID, strPwd);
                this.cr.ReportSource = this.PanelReport1;
                cr.SelectionFormula = "{Classifica_Intestazione1.ID_GaraParams} = " + Definizioni.idGaraParams +
                        " AND {Classifica_Intestazione1.ID_Segment} = " + Definizioni.idSegment;
                this.cr.RefreshReport();
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }

        }

        // classifica per segmento
        private void classifica_Click(object sender, EventArgs e)
        {
            try
            {
                cr.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
                this.cr.ReportSource = this.ResultReport1;
                cr.SelectionFormula = "{Classifica_Intestazione1.ID_GaraParams} = " + Definizioni.idGaraParams +
                        " AND {Classifica_Intestazione1.ID_Segment} = " + Definizioni.idSegment;
                this.cr.RefreshReport();

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        // classifica finale
        private void result_Click(object sender, EventArgs e)
        {
            try
            {
                cr.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
                this.cr.ReportSource = this.FinalReport1;
                cr.SelectionFormula = "{GaraTotal1.ID_GaraParams} = " + Definizioni.idGaraParams + "";
                this.cr.RefreshReport();
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        // Export All
        private void button3_Click_1(object sender, EventArgs e)
        {
            try
            {
                pb.Visible = true;
                pb.Refresh();

                DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                //dest.DiskFileName = l1.Text.Substring(0, 8) + "_" + l7.Text + "_" + l8.Text.Substring(0, 3) + "_" +
                //    "_Panel.pdf";
                // 2.0.0.10 - 28/01/2019 faccio scegliere dove salvare i file
                if (recentFolder.Equals(""))
                    fd1.SelectedPath = RollartSystemTech.Properties.Settings.Default.EventFolder;
                else fd1.SelectedPath = recentFolder;

                if (fd1.ShowDialog() == DialogResult.OK)
                {
                    recentFolder = fd1.SelectedPath;

                    string nomeFile = fd1.SelectedPath + "\\" + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year + " ";
                    nomeFile += "PANEL - " + l7.Text + " " + l8.Text + " " + segmentName.Text.ToUpper() + formato;
                    dest.DiskFileName = nomeFile;

                    ExportOptions ex = new ExportOptions();
                    ex.ExportDestinationOptions = dest;
                    ex.ExportDestinationType = ExportDestinationType.DiskFile;
                    ex.ExportFormatType = ExportFormatType.PortableDocFormat;

                    // pannello
                    this.cr.ReportSource = this.PanelReport1;
                    cr.SelectionFormula = "{Classifica_Intestazione1.ID_GaraParams} = " + Definizioni.idGaraParams +
                            " AND {Classifica_Intestazione1.ID_Segment} = " + Definizioni.idSegment;
                    this.cr.RefreshReport();
                    PanelReport1.Export(ex);
                    

                    // classifica
                    this.cr.ReportSource = this.ResultReport1;
                    cr.SelectionFormula = "{Classifica_Intestazione1.ID_GaraParams} = " + Definizioni.idGaraParams +
                            " AND {Classifica_Intestazione1.ID_Segment} = " + Definizioni.idSegment;

                    //dest.DiskFileName = l1.Text.Substring(0, 8) + "_" + l7.Text + "_" + l8.Text.Substring(0, 3) + "_" +
                    //    "_Results.pdf";
                    nomeFile = fd1.SelectedPath + "\\" + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year + " ";
                    nomeFile += "RESULTS - " + l7.Text + " " + l8.Text + " " + segmentName.Text.ToUpper() + formato;
                    dest.DiskFileName = nomeFile;

                    ex.ExportDestinationOptions = dest;
                    this.cr.RefreshReport();
                    ResultReport1.Export(ex);

                    // final
                    this.cr.ReportSource = this.FinalReport1;
                    cr.SelectionFormula = "{GaraTotal1.ID_GaraParams} = " + Definizioni.idGaraParams + "";
                    //dest.DiskFileName = l1.Text.Substring(0, 8) + "_" + l7.Text + "_" + l8.Text.Substring(0, 3) + "_" +
                    //    "_Final.pdf";
                    nomeFile = fd1.SelectedPath + "\\" + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year + " ";
                    nomeFile += "FINAL ● " + l7.Text + " " + l8.Text + ".pdf";
                    dest.DiskFileName = nomeFile;

                    ex.ExportDestinationOptions = dest;
                    this.cr.RefreshReport();
                    FinalReport1.Export(ex);

                    pb.Visible = false;
                    // 2.0.0.10 - 28/01/2019
                    //MessageBox.Show("Reports exported correctly.");
                    Process.Start("explorer.exe", recentFolder);
                } else
                {
                    pb.Visible = false;
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                pb.Visible = false;
            }
        }

        // Reset segment
        public void DeleteSegment(Button bDelete)
        {
            try
            {
                int idGaraParam = int.Parse(bDelete.Tag.ToString().Split('_')[0]);
                int idSegm = int.Parse(bDelete.Tag.ToString().Split('_')[1]);

                DialogResult res = MessageBox.Show(Definizioni.resources["event8"].ToString() + "?",
                    "RollartSystem", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning,
                    MessageBoxDefaultButton.Button2);

                if (res == DialogResult.Yes)
                {

                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "DELETE FROM Gara WHERE ID_GaraParams = " + idGaraParam +
                            " AND ID_Segment = " + idSegm;
                        command.ExecuteNonQuery();
                        command.Cancel();
                        command.CommandText = "DELETE FROM GaraFinal WHERE ID_GaraParams = " + idGaraParam +
                            " AND ID_Segment = " + idSegm;
                        command.ExecuteNonQuery();
                        command.Cancel();
                        // 2.0.0.10 - 27/01/2019 non cancello la GaraTotal
                        //command.CommandText = "DELETE FROM GaraTotal WHERE ID_GaraParams = " + idGaraParam;
                        //command.ExecuteNonQuery();
                    }
                    // 2.0.0.10 - 27/01/2019 modifica per NON cancellare tutta la tabella GaraTotal ma ripristinare la 
                    // classifica del segmento nel caso ci sia un segmento eseguito
                    // negli altri casi cancello tutta la tabella
                    // aggiorno i risultati (classifica e totale) da garaFinal a garaTotal
                        SQLiteCommand command2 = Definizioni.conn.CreateCommand();
                    command2.CommandText = "SELECT ID_Atleta, Position, SUM(Total) FROM GaraFinal WHERE ID_GaraParams = " + idGaraParam
                        + " GROUP BY ID_Atleta ORDER BY Total DESC";
                    SQLiteDataReader dr = command2.ExecuteReader();
                    int position = 1;
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            using (SQLiteCommand commandUpdate = Definizioni.conn.CreateCommand())
                            {
                                commandUpdate.CommandText = "UPDATE GaraTotal SET Position = " + position;
                                commandUpdate.CommandText += ", TotGara = '" + dr[2].ToString().Replace(",", ".") + "'";
                                if (idSegm == 1 || idSegm == 4 || idSegm == 11 || idSegm == 12)
                                    commandUpdate.CommandText += ", PosSegment1 = '0'";
                                else if (idSegm == 2 || idSegm == 5 || idSegm == 6)
                                    commandUpdate.CommandText += ", PosSegment2 = '0'";

                                commandUpdate.CommandText += " WHERE ID_GaraParams = " + idGaraParam;
                                commandUpdate.CommandText += " AND ID_Atleta = " + dr[0].ToString();
                                commandUpdate.ExecuteNonQuery();
                            }
                            position++;
                        }
                    } else
                    {
                        using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                        {
                            command.CommandText = "DELETE FROM GaraTotal WHERE ID_GaraParams = " + idGaraParam;
                            command.ExecuteNonQuery();
                        }
                    }
                    /****************/
                    using (SQLiteCommand commandUpdate = Definizioni.conn.CreateCommand())
                    {
                        commandUpdate.CommandText = "UPDATE GaraParams SET LastPartecipant = 0, Completed = 'N'" +
                            " WHERE ID_GaraParams = " + idGaraParam + " AND ID_Segment = " + idSegm;
                        commandUpdate.ExecuteNonQuery();
                    }
                    reload.PerformClick();

                    foreach (TreeNode tn in tv.Nodes[0].Nodes)
                    {
                        if (tn.Name.Equals(idGaraParam + ""))
                        {
                            
                            tv.SelectedNode = tn;
                            tv.Select();
                            break;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void b7_Click(object sender, EventArgs e)
        {
            DeleteSegment(b7);
        }

        private void b8_Click(object sender, EventArgs e)
        {
            DeleteSegment(b8);
        }

        private void b9_Click(object sender, EventArgs e)
        {
            DeleteSegment(b9);
        }

        // Import xml event
        private void importEvent_Click_1(object sender, EventArgs e)
        {
            try
            {
                message.Text = "";
                of1.InitialDirectory = RollartSystemTech.Properties.Settings.Default.dbfolder;
                of1.Title = "Import event from xml file";
                of1.Filter = "File XML| *.xml";
                of1.FileName = "";
                of1.RestoreDirectory = true;

                if (of1.ShowDialog() == DialogResult.OK)
                {
                    transaction = Definizioni.conn.BeginTransaction();

                    DataSet dataSet = new DataSet();
                    dataSet.ReadXml(of1.FileName);

                    DataTable dt = dataSet.Tables[0];
                    var o1 = dt.Rows[0][0];// idGaraParams
                    var o2 = dt.Rows[0][1];// idDiscipline
                    var o3 = dt.Rows[0][2];// idCategory
                    var o4 = dt.Rows[0][3];// idSegment1
                    var o5 = dt.Rows[0][4];// idSegment2
                    var o6 = dt.Rows[0][5];// idSegment3
                    var o7 = dt.Rows[0][6];// idSegment4
                    var o8 = dt.Rows[0][7];// name
                    var o9 = dt.Rows[0][8];// place
                    var o10 = dt.Rows[0][9];// datfrom
                    var o11 = dt.Rows[0][10];// dateto
                    var o12 = dt.Rows[0][11];// sex
                    var o13 = dt.Rows[0][12];// discipline
                    var o14 = dt.Rows[0][13];// category
                    var o15 = dt.Rows[0][14];// segment1
                    var o16 = dt.Rows[0][15];// segment2
                    var o17 = dt.Rows[0][16];// segment3
                    var o18 = dt.Rows[0][17];// segment4
                    var o19 = dt.Rows[0][18];// numJudges
                    var o20 = dt.Rows[0][20];// numSkaters 

                    int idGaraParams = 1;
                    // recupero l'ID
                    SQLiteCommand cmd = new SQLiteCommand("select MAX(ID_GaraParams) from GaraParams", Definizioni.conn);
                    SQLiteDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        if (dr[0].ToString().Equals("")) idGaraParams = 1;
                        else idGaraParams = dr.GetInt32(0) + 1;
                    }


                    // Tabella GaraParams
                    for (int i = 0; i < 4; i++) // ciclo per i 4 segmenti
                    {
                        int idseg = int.Parse(dt.Rows[0][3 + i].ToString());

                        if (idseg != 0)
                        {
                            using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                            {
                                command.Transaction = transaction;
                                command.CommandText =
                                "INSERT INTO GaraParams(ID_GaraParams,Name,Place,Date,DateEnd,ID_Segment,ID_Specialita,ID_Category,Partecipants,Sex,Completed,LastPartecipant,NumJudges)" +
                                " VALUES(" + idGaraParams + ",'" + o8.ToString().TrimEnd().Replace("'", "''") + "','" + o9.ToString().Replace("'", "''") + "','" + o10 + "','" + o11 +
                                         "'," + idseg + ",'" + o2 + "','" + o3 + "','" + o20 + "','" + o12 +
                                         "','N',0,'" + o19 + "')";
                                command.ExecuteNonQuery();
                            }
                        }
                    }

                    // tabella Athletes, Participants
                    //var listSkaters = dt.Rows[0][21]; // list skaters

                    bool RegionPresente = Utility.CheckIfColumnExists("Athletes", "Region");
                    bool speciaPresente = Utility.CheckIfColumnExists("Athletes", "ID_Specialita");

                    int atletiGiapresenti = 0;
                    int[] ids = new int[dataSet.Tables[4].DataSet.Tables[4].Rows.Count];
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        if (RegionPresente && speciaPresente)
                            command.CommandText = "INSERT INTO Athletes(Name, Societa, Country, Region, ID_Specialita)" +
                                " VALUES(@param1,@param2,@param3,@param4,@param5)";
                        else
                            command.CommandText = "INSERT INTO Athletes(Name, Societa, Country)" +
                                " VALUES(@param1,@param2,@param3)";
                        int k = 0;
                        for (int i = 0; i < dataSet.Tables[4].DataSet.Tables[4].Rows.Count; i++)
                        {
                            command.Transaction = transaction;
                            //command.Parameters.AddWithValue("@param0", dataSet.Tables[1].DataSet.Tables[2].Rows[i][0]); // ID
                            command.Parameters.AddWithValue("@param1", dataSet.Tables[4].DataSet.Tables[4].Rows[i][2].ToString().TrimEnd()); // nome
                            command.Parameters.AddWithValue("@param2", dataSet.Tables[4].DataSet.Tables[4].Rows[i][3].ToString().TrimEnd()); // societa
                            command.Parameters.AddWithValue("@param3", dataSet.Tables[4].DataSet.Tables[4].Rows[i][4].ToString().TrimEnd()); // nazione
                            if (RegionPresente && speciaPresente)
                            {
                                command.Parameters.AddWithValue("@param4", dataSet.Tables[4].DataSet.Tables[4].Rows[i][5].ToString().TrimEnd()); // region
                                command.Parameters.AddWithValue("@param5", dataSet.Tables[4].DataSet.Tables[4].Rows[i][6].ToString()); // spec
                            }
                            try
                            {
                                command.ExecuteNonQuery();
                            }
                            catch (Exception)
                            {
                                atletiGiapresenti++;
                                //MessageBox.Show("Athlete " + lb.Items[i].SubItems[1].Text + " already inserted.");
                            }

                            // recupero l'ID dell'Atleta
                            if (RegionPresente && speciaPresente)
                                cmd = new SQLiteCommand("select ID_Atleta from Athletes " +
                                    " WHERE Name = '" + dataSet.Tables[4].DataSet.Tables[4].Rows[i][2] + "'" +
                                    " AND Societa = '" + dataSet.Tables[4].DataSet.Tables[4].Rows[i][3] + "'" +
                                    " AND Country = '" + dataSet.Tables[4].DataSet.Tables[4].Rows[i][4] + "'" +
                                    " AND Region = '" + dataSet.Tables[4].DataSet.Tables[4].Rows[i][5] + "'" +
                                    " AND ID_Specialita = " + dataSet.Tables[4].DataSet.Tables[4].Rows[i][6] + "", Definizioni.conn);
                            else
                                cmd = new SQLiteCommand("select ID_Atleta from Athletes " +
                                    " WHERE Name = '" + dataSet.Tables[4].DataSet.Tables[4].Rows[i][2] + "'" +
                                    " AND Societa = '" + dataSet.Tables[4].DataSet.Tables[4].Rows[i][3] + "'" +
                                    " AND Country = '" + dataSet.Tables[4].DataSet.Tables[4].Rows[i][4] + "'", Definizioni.conn);
                            dr = cmd.ExecuteReader();
                            while (dr.Read())
                            {
                                ids[k] = int.Parse(dr[0].ToString());
                                k++;
                            }
                        }

                        for (int i = 0; i < 4; i++) // ciclo per i 4 segmenti
                        {
                            int idseg = int.Parse(dt.Rows[0][3 + i].ToString());

                            if (idseg != 0)
                            {
                                command.Transaction = transaction;
                                command.CommandText = "INSERT INTO Participants(ID_GaraParams,ID_Segment,ID_Atleta,NumStartingList)" +
                                            " VALUES(@param1,@param2,@param3,@param4)";
                                command.Parameters.AddWithValue("@param1", idGaraParams);
                                command.Parameters.AddWithValue("@param2", idseg); // id_segment 
                                for (int j = 0; j < dataSet.Tables[4].DataSet.Tables[4].Rows.Count; j++)
                                {
                                    command.Parameters.AddWithValue("@param3", ids[j]); // ID_Atleta
                                    command.Parameters.AddWithValue("@param4", dataSet.Tables[4].DataSet.Tables[4].Rows[j][1]); // order
                                    command.ExecuteNonQuery();
                                }
                                
                            }
                        }
                    }

                    // inserisco giudici
                    int[] idGiudici = new int[dataSet.Tables[1].DataSet.Tables[2].Rows.Count];
                    var listJudges = dataSet.Tables[1].DataSet.Tables[2].Rows[0][0]; // id Official
                    int giudiciGiapresenti = 0;
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.Transaction = transaction;
                        command.CommandText = "INSERT INTO Judges(Name, Region, Country) VALUES(@param1,'','')";
                        int b = 0;
                        for (int i = 0; i < dataSet.Tables[1].DataSet.Tables[2].Rows.Count; i++)
                        {
                            
                            command.Parameters.AddWithValue("@param1",
                                dataSet.Tables[1].DataSet.Tables[2].Rows[i][1].ToString()); // nome giudice
                            try
                            {
                                command.ExecuteNonQuery();
                            }
                            catch (Exception)
                            {
                                giudiciGiapresenti++;
                                //MessageBox.Show("Judge " + Judges.judges[i].ToString() + " already inserted.");
                            }

                            // recupero l'ID del Giudice
                            cmd = new SQLiteCommand("select ID_Judge from Judges " +
                                " WHERE Name = '" + dataSet.Tables[1].DataSet.Tables[2].Rows[i][1].ToString().Replace("'","''")
                                + "'", Definizioni.conn);
                            dr = cmd.ExecuteReader();
                            idGiudici[b] = 0;
                            while (dr.Read())
                            {
                                idGiudici[b] = int.Parse(dr[0].ToString());
                                b++;
                                break; // solo il primo
                            }
                        }

                        command.CommandText = "INSERT INTO PanelJudge(ID_Judge, ID_GaraParams,Role)" +
                            " VALUES(@param3,@param1,@param2)";
                        command.Parameters.AddWithValue("@param1", idGaraParams);
                        for (int i = 0; i < dataSet.Tables[1].DataSet.Tables[2].Rows.Count; i++)
                        {
                            command.Parameters.AddWithValue("@param2", dataSet.Tables[1].DataSet.Tables[2].Rows[i][2]); // role
                            command.Parameters.AddWithValue("@param3", idGiudici[i]); // name
                            command.ExecuteNonQuery();

                        }
                    }

                    string mess = Definizioni.resources["event11"].ToString() + "\r\n";

                    if (atletiGiapresenti > 0) mess += " - " + atletiGiapresenti + " of " +
                            dataSet.Tables[4].DataSet.Tables[4].Rows.Count 
                            + Definizioni.resources["event14"].ToString() + "\r\n";
                    if (giudiciGiapresenti > 0) mess += " - " + giudiciGiapresenti + " of " +
                            dataSet.Tables[1].DataSet.Tables[2].Rows.Count + 
                              Definizioni.resources["event12"].ToString() + "\r\n";
                    // 2.0.0.10_bis faccio commit per evitare di importare a metà una gara
                    transaction.Commit();

                    MessageBox.Show(mess, "RollArt System", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    reload.PerformClick();

                    foreach (TreeNode tn in tv.Nodes[0].Nodes)
                    {
                        if (tn.Name.Equals(idGaraParams + ""))
                        {
                            tv.SelectedNode = tn;
                            tv.Select();
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                MessageBox.Show(Definizioni.resources["event13"].ToString() + "\r\n(" + ex.Message + ")", "RollArt System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        # endregion

        #region VARIE
        private void panel1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.WindowState==FormWindowState.Maximized)
                this.WindowState = FormWindowState.Normal;
            else this.WindowState = FormWindowState.Maximized;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            flagClose = true;
            CloseConnection();
            System.Environment.Exit(0);
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouse_offset.X, mouse_offset.Y);
                this.Location = mousePos; //move the form to the desired location
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouse_offset = new Point(-e.X, -e.Y);
        }

        public void LoadResources()
        {
            try
            {
                string line;
                System.IO.StreamReader file = new System.IO.StreamReader(@"resources.en.txt");
                Definizioni.resources = new Hashtable();
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Contains('='))
                        Definizioni.resources.Add(line.Split('=')[0], line.Split('=')[1]);
                }
                file.Close();
                Utility.WriteLog(" - resources.en.txt loaded", "OK");
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex.Message, "ERROR");
                message.Text = "ERROR" + ex.Message;
            }
        }

        public void LoadSettings()
        {
            try
            {
                //Definizioni.xml = new XmlDocument();
                //Definizioni.xml.Load("settings.xml");
                //XmlNode root = Definizioni.xml.DocumentElement;

                // recupero il numero di giudici
                //Definizioni.numOfJudges = int.Parse(Definizioni.xml.SelectNodes("/Settings/GeneralSettings/@NumJudges").Item(0).Value);
                Definizioni.numOfJudges = RollartSystemTech.Properties.Settings.Default.NumJudges;
                Definizioni.judgesSettings = new string[Definizioni.numOfJudges];
                nJudges.Value = Definizioni.numOfJudges;

                //Definizioni.nodiGiudici = Definizioni.xml.SelectNodes("/Settings/Judges/*");

                //Definizioni.rootFolder = Definizioni.xml.SelectNodes("/Settings/GeneralSettings/@RootFolder").Item(0).Value;
                Definizioni.rootFolder = RollartSystemTech.Properties.Settings.Default.EventFolder;

                //Definizioni.staticScreen = bool.Parse(Definizioni.xml.SelectNodes("/Settings/GeneralSettings/@StaticScreen").Item(0).Value);
                Definizioni.staticScreen = RollartSystemTech.Properties.Settings.Default.StaticScreen;

                //Definizioni.screenType = Definizioni.xml.SelectNodes("/Settings/GeneralSettings/@StaticScreenType").Item(0).Value;
                Definizioni.screenType = RollartSystemTech.Properties.Settings.Default.ScreenType;

                //Definizioni.percentualiSpins = Definizioni.xml.SelectNodes("/Settings/Spins/Percentages/@Range").Item(0).Value.Split(',');
                Definizioni.percentualiSpins = RollartSystemTech.Properties.Settings.Default.SpinPercentages.Split(',');

                //Definizioni.pingTime = int.Parse(Definizioni.xml.SelectNodes("/Settings/Judges/@PingTime").Item(0).Value);
                Definizioni.pingTime = RollartSystemTech.Properties.Settings.Default.PingTime;

                Utility.WriteLog(" - Settings loaded", "OK");
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex.InnerException.Message, "ERROR");
                message.Text = "ERROR" + ex.InnerException.Message;
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            buTech.BackColor = Color.Transparent;
            button1.BackColor = Color.Transparent; button2.BackColor = Color.Transparent;
            button2.BackColor = Color.Transparent; button5.BackColor = Color.Transparent;
            //this.Opacity = 0.80f;
            SettingsForm sf = new SettingsForm();
            sf.ShowDialog();
            //this.Opacity = 1.00f;
        }

        private void log_Click(object sender, EventArgs e)
        {
            buTech.BackColor = Color.Transparent;
            button1.BackColor = Color.Transparent; button2.BackColor = Color.Transparent;
            button2.BackColor = Color.Transparent; button5.BackColor = Color.FromArgb(175, 202, 11);
            titolo.Text = "Events Management --> Log Event";
            titolo.ImageIndex = 14;
            panLog.Visible = true;
            panLog.BringToFront();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        #endregion

        # region SPLASH SCREEN
        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            sc.Progress = e.ProgressPercentage;
            sc.UpdateLabel = "Loading..." + e.ProgressPercentage + " % completed.";
        }

        private void pb_Click(object sender, EventArgs e)
        {

        }

        private void nJudges_ValueChanged(object sender, EventArgs e)
        {
            Definizioni.numOfJudges = (int)nJudges.Value;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            for (int i = 0; i <= 100; i++)
            {
                if (worker.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    // Perform a time consuming operation and report progress.
                    System.Threading.Thread.Sleep(30);
                    worker.ReportProgress((i));
                }
            }
            worker.ReportProgress(100);
        }

        private void tv_DrawNode(object sender, DrawTreeNodeEventArgs e)
        {
            //try
            //{
            //    if (!e.Node.TreeView.Focused && e.Node == e.Node.TreeView.SelectedNode)
            //    {
            //        Font treeFont = e.Node.NodeFont ?? e.Node.TreeView.Font;
            //        e.Graphics.FillRectangle(Brushes.Gray, e.Bounds);
            //        ControlPaint.DrawFocusRectangle(e.Graphics, e.Bounds, SystemColors.HighlightText, SystemColors.Highlight);
            //        TextRenderer.DrawText(e.Graphics, e.Node.Text, treeFont, e.Bounds, SystemColors.HighlightText, TextFormatFlags.GlyphOverhangPadding);
            //    }
            //    else
            //        e.DrawDefault = true;
            //}
            //catch (Exception ex)
            //{
            //    message.Text = ex.Message;
            //}
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(@"RST_Log.txt");
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
            
        }

        private void namep_TabIndexChanged(object sender, EventArgs e)
        {
            
        }

        // crea una nuova cartella degli eventi
        private void newFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                TreeNode tn = new TreeNode();
                tn.Text = "New Events group " + CountFolders();
                tn.ToolTipText = "new group of events";
                //tn.Name = dr[3].ToString(); 
                tn.ImageIndex = 48;
                tn.SelectedImageIndex = 48;
                tn.Tag = "folder";

                // creo la directory 
                System.IO.Directory.CreateDirectory(
                    RollartSystemTech.Properties.Settings.Default.EventFolder + "\\" + tn.Text);

                tv.Nodes[0].Nodes.Add(tn);
                tv.ExpandAll();
                tv.Nodes[0].ExpandAll();

                tv.LabelEdit = true;
                tv.Nodes[0].Nodes[tv.Nodes[0].Nodes.Count - 1].BeginEdit(); //activates editing of last node
            }
            catch (Exception ex)
            {
                Utility.WriteLog("New Folder: " + ex.Message, "ERROR");
            }
        }

        private int CountFolders()
        {
            int numFolders = 1;
            foreach (TreeNode tn in tv.Nodes)
            {
                if (tn.Tag != null)
                {
                    if (tn.Tag.Equals("folder"))
                        numFolders++;
                }
            }
            return numFolders;
        }

        private void tv_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            try
            {
                System.IO.Directory.Move(
                    RollartSystemTech.Properties.Settings.Default.EventFolder + "\\" + e.Node.Text,
                    RollartSystemTech.Properties.Settings.Default.EventFolder + "\\" + e.Label);

                tv.LabelEdit = false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("AfterLabelEdit: " + ex.Message, "ERROR");
            }
        }

        private void tv_ItemDrag(object sender, ItemDragEventArgs e)
        {
            DoDragDrop(e.Item, DragDropEffects.Move);
        }

        private void tv_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void tv_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                // Retrieve the client coordinates of the drop location.
                Point targetPoint = tv.PointToClient(new Point(e.X, e.Y));

                // Retrieve the node at the drop location.
                TreeNode targetNode = tv.GetNodeAt(targetPoint);

                // Retrieve the node that was dragged.
                TreeNode draggedNode = (TreeNode)e.Data.GetData(typeof(TreeNode));

                // Sanity check
                if (draggedNode == null)
                {
                    return;
                }

                // Did the user drop on a valid target node?
                if (targetNode == null)
                {
                    // The user dropped the node on the treeview control instead
                    // of another node so lets place the node at the bottom of the tree.
                    draggedNode.Remove();
                    tv.Nodes.Add(draggedNode);
                    draggedNode.Expand();
                }
                else
                {
                    TreeNode parentNode = targetNode;

                    // Confirm that the node at the drop location is not 
                    // the dragged node and that target node isn't null
                    // (for example if you drag outside the control)
                    if (!draggedNode.Equals(targetNode) && targetNode != null)
                    {
                        bool canDrop = true;

                        // Crawl our way up from the node we dropped on to find out if
                        // if the target node is our parent. 
                        while (canDrop && (parentNode != null))
                        {
                            canDrop = !Object.ReferenceEquals(draggedNode, parentNode);
                            parentNode = parentNode.Parent;
                        }

                        // Is this a valid drop location?
                        if (canDrop)
                        {
                            // Yes. Move the node, expand it, and select it.
                            draggedNode.Remove();
                            targetNode.Nodes.Add(draggedNode);
                            targetNode.Expand();
                        }
                    }


                }
                // Optional: Select the dropped node and navigate (however you do it)
                tv.SelectedNode = draggedNode;
                // NavigateToContent(draggedNode.Tag);
            }
            catch (Exception ex)
            {
                Utility.WriteLog("DragDrop: " + ex.Message, "ERROR");
            }
        }

        private void panStyle_Paint(object sender, PaintEventArgs e)
        {

        }

        private void check3_Click(object sender, EventArgs e)
        {
            //CheckScores cs = new CheckScores(int .Parse(check3.Tag.ToString()));
            //cs.ShowDialog();
        }

        private void check2_Click(object sender, EventArgs e)
        {
            //CheckScores cs = new CheckScores(int.Parse(check2.Tag.ToString()));
            //cs.ShowDialog();
        }

        private void check1_Click(object sender, EventArgs e)
        {
            //CheckScores cs = new CheckScores(int.Parse(check1.Tag.ToString()));
            //cs.ShowDialog();
        }

        private void name_Leave(object sender, EventArgs e)
        {
            try
            {
                name.Text = name.Text.Substring(0,1).ToUpper() + name.Text.Substring(1);
            }
            catch (Exception)
            {

            }
        }

        private void place_Leave(object sender, EventArgs e)
        {
            try
            {
                place.Text = place.Text.Substring(0, 1).ToUpper() + place.Text.Substring(1);
            }
            catch (Exception)
            {

            }
        }

        private void EventsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // *** Modifica del 05/12/2018 - 2.0.0.9 *** //
            if (!flagClose)
                e.Cancel = true;
        }

        private void namep_Leave(object sender, EventArgs e)
        {
            try
            {
                //namep.Text = namep.Text.First().ToString().ToUpper() + namep.Text.Substring(1);
                club.Text = "";
                region.Text = "";
                naz.Text = "";
                add.Enabled = true;
                //add.Text = "";
                {
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        bool RegionPresente = Utility.CheckIfColumnExists("Athletes", "Region");
                        
                        if (RegionPresente)
                            command.CommandText = "SELECT Societa, Country, ID_Atleta, Region FROM Athletes WHERE Name = '"
                            + namep.Text.Replace("'","''") + "'";
                        else
                            command.CommandText = "SELECT Societa, Country, ID_Atleta FROM Athletes WHERE Name = '"
                            + namep.Text.Replace("'", "''") + "'";
                        SQLiteDataReader dr = command.ExecuteReader();
                        if (!dr.HasRows)
                        {
                            // se atleta non presente abilito gli altri campi
                            club.Enabled = true;
                            region.Enabled = true;
                            naz.Enabled = true;

                            if (lv2.SelectedIndices.Count > 0)
                            {
                                namep.Text = lv2.SelectedItems[0].SubItems[1].Text;
                                club.Text = lv2.SelectedItems[0].SubItems[2].Text;
                                region.Text = lv2.SelectedItems[0].SubItems[3].Text;
                                naz.Text = lv2.SelectedItems[0].SubItems[4].Text;
                                //namep.Focus();
                                updateskater.Enabled = true;
                                hidden.Text = lv2.SelectedItems[0].Text;
                                add.Enabled = false;
                            }
                            
                            if (naz.Text.Equals("")) naz.Focus();
                            else if (club.Text.Equals("")) club.Focus();
                            else if (region.Text.Equals("")) region.Focus();
                            return;
                        }
                        while (dr.Read())
                        {
                            if (!dr[0].ToString().Equals(""))
                                club.Text = dr[0].ToString();
                            if (!dr[1].ToString().Equals(""))
                                naz.Text = dr[1].ToString();
                            // id atleta
                            if (!dr[2].ToString().Equals(""))
                                id.Text = dr[2].ToString();

                            if (RegionPresente)
                            {
                                if (!dr[3].ToString().Equals(""))
                                    region.Text = dr[3].ToString();
                            }
                            if (lv2.SelectedIndices.Count > 0)
                               add.Enabled = false;
                        }
                        dr.Close();
                    }
                    club.Enabled = false;
                    region.Enabled = false;
                    naz.Enabled = false;
                    add.Focus();
                }
            }
            catch (Exception)
            {
                //message.Text = ex.Message;
            }
        }

        private void lv3_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (lv3.SelectedItems == null) return;
                if (lv3.SelectedItems.Count == 0) return;

                ListViewItem selectedItem = lv3.SelectedItems[0];
                string numJudge = selectedItem.SubItems[2].Text;
                string state = selectedItem.SubItems[3].Text;
                
                if (numJudge.StartsWith("Judge") && state.Equals("Connected"))
                {
                    object judge = selectedItem.Tag;
                    DialogResult result = MessageBox.Show("Restart " + numJudge + " ? ", "RollArt",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == DialogResult.Yes)
                    {
                        Utility.SendToSingleJudge("<QUIT/>", int.Parse(numJudge.Substring(6)));
                    }
                }
            }

            catch (Exception ex)
            {
                Utility.WriteLog("MouseDoubleClick: " + ex.Message, "ERROR");
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            sc.Close();
            this.WindowState = FormWindowState.Maximized;
            this.ShowInTaskbar = true;
        }

        #endregion

        #region 2.0.0.10
        // *** Modifica del 22/01/2019 - 2.0.0.10 *** //
        private void lv3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (lv3.SelectedItems == null)
                {
                    changeName.Enabled = false;
                    removeJudge.Enabled = false;
                   return;
                }
                if (lv3.SelectedItems.Count == 0)
                {
                    changeName.Enabled = false;
                    removeJudge.Enabled = false;
                    return;
                }
                ListViewItem selectedItem = lv3.SelectedItems[0];
                string numJudge = selectedItem.SubItems[2].Text;
                changeName.Enabled = true;

                if (numJudge.Equals("Assistant") || numJudge.Equals("Controller") || numJudge.Equals("Referee"))
                {
                    removeJudge.Enabled = true;
                    upJudge.Enabled = false;
                    downJudge.Enabled = false;
                }
                else if (numJudge.StartsWith("Judge"))
                {
                    removeJudge.Enabled = true;
                    upJudge.Enabled = true;
                    downJudge.Enabled = true;
                }
                else
                {
                    removeJudge.Enabled = false;
                    upJudge.Enabled = false;
                    downJudge.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Select Judge: " + ex.Message, "ERROR");
            }
        }

        // cancello il giudice
        private void removeJudge_Click(object sender, EventArgs e)
        {
            try
            {
                if (lv3.SelectedItems == null) return;
                if (lv3.SelectedItems.Count == 0) return;

                ListViewItem selectedItem = lv3.SelectedItems[0];
                DialogResult result;
                // se non e un giudice
                if (!selectedItem.SubItems[2].Text.StartsWith("Judge"))
                {
                    result = MessageBox.Show("Remove the " + selectedItem.SubItems[2].Text + " (" +
                    selectedItem.SubItems[0].Text + ") from the panel? ", "RollArt",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == DialogResult.Yes)
                    {
                        // cancello l'item nella lista
                        selectedItem.Remove();

                        using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                        {
                            command.CommandText = "UPDATE PanelJudge SET ID_Judge = '0' " +
                                "WHERE Role = '" + selectedItem.SubItems[2].Text + "' AND ID_GaraParams = " + Definizioni.idGaraParams;

                            command.ExecuteNonQuery();
                        }
                    }
                }
                else
                {
                    string Judge = selectedItem.SubItems[2].Text.Substring(selectedItem.SubItems[2].Text.Length - 1);
                    int numJudge = int.Parse(Judge);
                    int indiceJdaCancellare = selectedItem.Index;

                    if (numJudge == 1 && Definizioni.numJudges == 1) return;

                    // controllo se giudice connesso
                    if (CheckIfConnected()) return;

                    result = MessageBox.Show("Remove Judge " + numJudge + " (" +
                        selectedItem.SubItems[0].Text + ") from the Judges panel? ", "RollArt",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == DialogResult.Yes)
                    {
                        // cancello l'item nella lista
                        selectedItem.Remove();

                        using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                        {
                            command.CommandText = "DELETE FROM PanelJudge WHERE " +
                                " ID_GaraParams = " + Definizioni.idGaraParams +
                                " AND Role = 'Judge " + numJudge + "'";
                            command.ExecuteNonQuery();
                        }

                        for (int i = 0; i < Definizioni.numJudges - numJudge; i++)
                        {
                            // riordino gli altri giudici
                            using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                            {
                                command.CommandText = "UPDATE PanelJudge SET Role = 'Judge " + (numJudge + i) + "' WHERE " +
                                    " ID_GaraParams = " + Definizioni.idGaraParams +
                                    " AND Role = '" + lv3.Items[indiceJdaCancellare + i].SubItems[2].Text + "'";
                                command.ExecuteNonQuery();
                            }

                            lv3.Items[indiceJdaCancellare + i].SubItems[2].Text =
                                "Judge " + (numJudge + i);
                        }

                        // aggiorno il numero di giudici totali per la gara corrente
                        Definizioni.numJudges--;
                        using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                        {
                            command.CommandText = "UPDATE GaraParams SET NumJudges = '" + Definizioni.numJudges + "' WHERE " +
                                    " ID_GaraParams = " + Definizioni.idGaraParams;
                            command.ExecuteNonQuery();
                            l5.Text = Definizioni.numJudges + "";
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Remove Judge: " + ex.Message, "ERROR");
            }
        }

        // aggiorno il nome del giudice 
        private void lv3_AfterLabelEdit(object sender, LabelEditEventArgs e)
        {
            try
            {
                if (e.Label == null)
                {
                    lv3.LabelEdit = false;
                    return;
                }
                // se contiene caratteri speciali o string vuota 
                if (hasSpecialChar(e.Label.Trim()) || e.Label.Equals("") || e.Label.Trim().Equals(""))
                {
                    e.CancelEdit = true;
                    lv3.LabelEdit = false;
                    return;
                }
                string nuovoNomeGiudice = e.Label.Trim().ToUpper();
                lv3.Items[e.Item].SubItems[0].Text = nuovoNomeGiudice;
                string ruolo = lv3.Items[e.Item].SubItems[2].Text;

                int idGiudice = 0;
                string country = "";
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT ID_Judge, Country FROM Judges " +
                        " WHERE Name = '" + nuovoNomeGiudice.Replace("'", "''") + "'";
                    SQLiteDataReader dr = command.ExecuteReader();

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            // giudice gia esistente
                            idGiudice = int.Parse(dr[0].ToString());
                            country = dr[1].ToString(); //country
                            lv3.Items[e.Item].SubItems[1].Text = dr[1].ToString(); //country
                            break;
                        }
                    } else
                    {
                        dr.Close();
                        command.CommandText = "INSERT INTO Judges(Name, Region, Country) VALUES(@param1,'','')";
                        command.Parameters.AddWithValue("@param1", nuovoNomeGiudice);
                        try
                        {
                            command.ExecuteNonQuery();
                        }
                        catch (Exception)
                        {
                            // giudice già presente nel db
                        }
                        command.CommandText = "SELECT ID_Judge FROM Judges WHERE Name = '" + nuovoNomeGiudice.Replace("'", "''") + "'";
                        dr = command.ExecuteReader();
                        while (dr.Read())
                        {
                            idGiudice = int.Parse(dr[0].ToString());
                            break;
                        }
                        lv3.Items[e.Item].SubItems[1].Text = "";
                    }  
                }

                // aggiorno il nome del giudice nel pannello
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "UPDATE  PanelJudge SET ID_Judge = " + idGiudice + " WHERE " +
                        " ID_GaraParams = " + Definizioni.idGaraParams + " AND Role = '" + ruolo + "'";
                    command.ExecuteNonQuery();
                }
                    
                lv3.LabelEdit = false;                
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Judge name update: " + ex.Message, "ERROR");
                lv3.LabelEdit = false;
            }
        }

        // aggiorno il nome del giudice
        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                if (lv3.SelectedItems == null) return;
                if (lv3.SelectedItems.Count == 0) return;

                lv3.LabelEdit = true;
                ListViewItem selectedItem = lv3.SelectedItems[0];
                selectedItem.BeginEdit();
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Change Judge Name: " + ex.Message, "ERROR");
            }
        }

        // sposto Su il giudice
        private void upJudge_Click(object sender, EventArgs e)
        {
            try
            {
                if (lv3.SelectedItems == null) return;
                if (lv3.SelectedItems.Count == 0) return;

                ListViewItem selectedItem = lv3.SelectedItems[0];
                string Judge = selectedItem.SubItems[2].Text;

                // controllo se giudice connesso
                if (CheckIfConnected()) return;

                int currentIndex = selectedItem.Index;
                int currentJudge = int.Parse(Judge.Substring(Judge.Length - 1));
                string currentId = selectedItem.SubItems[4].Text;

                int totl = lv3.Items.Count;

                if (currentJudge == 1)
                {
                    return;
                }
                else
                {
                    int previousJudge = currentJudge - 1;
                    string previousid = lv3.Items[currentIndex - 1].SubItems[4].Text;

                    lv3.Items.Remove(selectedItem);
                    lv3.Items.Insert(currentIndex - 1, selectedItem);
                    // aggiorno il giudice che sto spostando sotto
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "UPDATE PanelJudge SET ID_Judge = '" + currentId + "' WHERE " +
                            " ID_GaraParams = " + Definizioni.idGaraParams +
                            " AND Role = 'Judge " + previousJudge + "'";
                        command.ExecuteNonQuery();
                    }
                    lv3.Items[currentIndex].SubItems[2].Text = "Judge " + currentJudge;
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "UPDATE PanelJudge SET ID_Judge = '" + previousid + "' WHERE " +
                            " ID_GaraParams = " + Definizioni.idGaraParams +
                            " AND Role = 'Judge " + currentJudge + "'";
                        command.ExecuteNonQuery();
                    }
                    lv3.Items[currentIndex - 1].SubItems[2].Text = "Judge " + previousJudge;
                    lv3.Items[currentIndex - 1].Selected = true;
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Move Up judge: " + ex.Message, "ERROR");
            }
        }

        // sposto Giu il giudice
        private void downJudge_Click(object sender, EventArgs e)
        {
            try
            {
                if (lv3.SelectedItems == null) return;
                if (lv3.SelectedItems.Count == 0) return;

                ListViewItem selectedItem = lv3.SelectedItems[0];
                string Judge = selectedItem.SubItems[2].Text;

                // controllo se giudice connesso
                if (CheckIfConnected()) return;

                int currentIndex = selectedItem.Index;
                int currentJudge = int.Parse(Judge.Substring(Judge.Length - 1));
                string currentId = selectedItem.SubItems[4].Text;

                int totl = lv3.Items.Count;

                if (currentIndex == totl - 1)
                {
                    return;
                }
                else
                {
                    int nextJudge = currentJudge + 1;
                    string nextid = lv3.Items[currentIndex + 1].SubItems[4].Text;

                    lv3.Items.Remove(selectedItem);
                    lv3.Items.Insert(currentIndex + 1, selectedItem);
                    // aggiorno il giudice che sto spostando sotto
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "UPDATE PanelJudge SET ID_Judge = '" + currentId + "' WHERE " +
                            " ID_GaraParams = " + Definizioni.idGaraParams +
                            " AND Role = 'Judge " + nextJudge + "'";
                        command.ExecuteNonQuery();
                        
                    }
                    lv3.Items[currentIndex].SubItems[2].Text = "Judge " + currentJudge;
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "UPDATE PanelJudge SET ID_Judge = '" + nextid + "' WHERE " +
                            " ID_GaraParams = " + Definizioni.idGaraParams +
                            " AND Role = 'Judge " + currentJudge + "'";
                        command.ExecuteNonQuery();
                    }

                    lv3.Items[currentIndex + 1].SubItems[2].Text = "Judge " + nextJudge;
                    lv3.Items[currentIndex + 1].Selected = true;
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("Move Down judge: " + ex.Message, "ERROR");
            }
        }

        // aggiungo un nuovo giudice
        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                string ufficialiGiàInseriti = "";

                // controllo se giudice connesso
                if (CheckIfConnected()) return;

                // verifico quali ufficiali sono stati già inseriti
                foreach (ListViewItem lvi in lv3.Items)
                {
                    if (lvi.SubItems[2].Text.Equals("Controller") ||
                        lvi.SubItems[2].Text.Equals("Assistant") ||
                        lvi.SubItems[2].Text.Equals("Referee"))
                        ufficialiGiàInseriti += lvi.SubItems[2].Text + ";";
                }
                AddJudge aj = new AddJudge(ufficialiGiàInseriti);
                aj.ShowDialog();

                l5.Text = Definizioni.numJudges + "";
                tv.SelectedNode = tv.Nodes[0].Nodes[indexNodoSelezionato];
                SelectNode(false, null, null);
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Add new official: " + ex.Message, "ERROR");
            }
        }

        public bool CheckIfConnected()
        {
            string judgeConnected = "";
            foreach (JudgeClient judge in EventsForm.jclient.list)
            {
                if (judge.state.Equals("1"))
                {
                    judgeConnected += "Judge " + judge.num + ", ";
                }
            }
            if (!judgeConnected.Equals(""))
            {
                MessageBox.Show("One or more Judges are already connected (" + judgeConnected.TrimEnd(new Char[] { ' ', ','}) + 
                        ").\r\nDisconnect them and keep on.", "RollArt", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return true;
            }
            return false;
        }

        public static bool hasSpecialChar(string input)
        {
            string specialChar = @"\|!#$%&/()=?»«@£§€{};<>_,°çé{}[]^+:";
            foreach (var item in specialChar)
            {
                if (input.Contains(item)) return true;
            }

            return false;
        }

        #endregion

    }

}


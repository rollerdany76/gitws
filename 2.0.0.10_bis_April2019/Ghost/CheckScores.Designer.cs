﻿namespace RollartSystemTech
{
    partial class CheckScores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckScores));
            this.panel1 = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.rank = new System.Windows.Forms.ToolStripTextBox();
            this.numSkaters = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.numPartecipante = new System.Windows.Forms.ToolStripLabel();
            this.name = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.waiting = new System.Windows.Forms.ToolStripLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgElements = new System.Windows.Forms.DataGridView();
            this.gb = new System.Windows.Forms.GroupBox();
            this.element = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.cb4 = new System.Windows.Forms.CheckBox();
            this.rb4 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.cb3 = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboPerc = new System.Windows.Forms.ComboBox();
            this.cb2 = new System.Windows.Forms.CheckBox();
            this.cb1 = new System.Windows.Forms.CheckBox();
            this.rb3 = new System.Windows.Forms.RadioButton();
            this.rb2 = new System.Windows.Forms.RadioButton();
            this.rb1 = new System.Windows.Forms.RadioButton();
            this.cbElements = new System.Windows.Forms.ComboBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.message = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgElements)).BeginInit();
            this.gb.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.toolStrip1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(788, 48);
            this.panel1.TabIndex = 0;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.rank,
            this.numSkaters,
            this.toolStripSeparator2,
            this.toolStripButton3,
            this.toolStripButton4,
            this.toolStripSeparator3,
            this.numPartecipante,
            this.name,
            this.toolStripSeparator1,
            this.waiting});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(788, 27);
            this.toolStrip1.TabIndex = 129;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::RollartSystemTech.Properties.Resources.playback_prev_icon_24;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(24, 24);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.ToolTipText = "Rank 1";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = global::RollartSystemTech.Properties.Resources.prev_24;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(24, 24);
            this.toolStripButton2.Text = "toolStripButton2";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // rank
            // 
            this.rank.Name = "rank";
            this.rank.Size = new System.Drawing.Size(31, 27);
            // 
            // numSkaters
            // 
            this.numSkaters.Name = "numSkaters";
            this.numSkaters.Size = new System.Drawing.Size(78, 24);
            this.numSkaters.Text = "toolStripLabel1";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::RollartSystemTech.Properties.Resources.playback_play_icon_24;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(24, 24);
            this.toolStripButton3.Text = "toolStripButton3";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = global::RollartSystemTech.Properties.Resources.playback_next_icon_241;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(24, 24);
            this.toolStripButton4.Text = "toolStripButton4";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 27);
            // 
            // numPartecipante
            // 
            this.numPartecipante.Name = "numPartecipante";
            this.numPartecipante.Size = new System.Drawing.Size(78, 24);
            this.numPartecipante.Text = "toolStripLabel1";
            // 
            // name
            // 
            this.name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.Size = new System.Drawing.Size(300, 27);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // waiting
            // 
            this.waiting.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.waiting.Image = global::RollartSystemTech.Properties.Resources.loading24x24;
            this.waiting.Name = "waiting";
            this.waiting.Size = new System.Drawing.Size(20, 24);
            this.waiting.Text = "toolStripLabel1";
            this.waiting.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgElements);
            this.panel2.Controls.Add(this.gb);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(0, 48);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(788, 585);
            this.panel2.TabIndex = 1;
            // 
            // dgElements
            // 
            this.dgElements.AllowUserToAddRows = false;
            this.dgElements.AllowUserToDeleteRows = false;
            this.dgElements.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Honeydew;
            this.dgElements.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgElements.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgElements.BackgroundColor = System.Drawing.Color.White;
            this.dgElements.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgElements.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgElements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgElements.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgElements.GridColor = System.Drawing.Color.Silver;
            this.dgElements.Location = new System.Drawing.Point(0, 0);
            this.dgElements.Margin = new System.Windows.Forms.Padding(2);
            this.dgElements.Name = "dgElements";
            this.dgElements.ReadOnly = true;
            this.dgElements.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgElements.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgElements.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgElements.ShowEditingIcon = false;
            this.dgElements.Size = new System.Drawing.Size(788, 464);
            this.dgElements.TabIndex = 10;
            this.dgElements.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgElements_DataError);
            this.dgElements.SelectionChanged += new System.EventHandler(this.dgElements_SelectionChanged);
            // 
            // gb
            // 
            this.gb.Controls.Add(this.element);
            this.gb.Controls.Add(this.button2);
            this.gb.Controls.Add(this.button1);
            this.gb.Controls.Add(this.cb4);
            this.gb.Controls.Add(this.rb4);
            this.gb.Controls.Add(this.label3);
            this.gb.Controls.Add(this.cb3);
            this.gb.Controls.Add(this.label2);
            this.gb.Controls.Add(this.comboPerc);
            this.gb.Controls.Add(this.cb2);
            this.gb.Controls.Add(this.cb1);
            this.gb.Controls.Add(this.rb3);
            this.gb.Controls.Add(this.rb2);
            this.gb.Controls.Add(this.rb1);
            this.gb.Controls.Add(this.cbElements);
            this.gb.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gb.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb.Location = new System.Drawing.Point(0, 464);
            this.gb.Name = "gb";
            this.gb.Size = new System.Drawing.Size(788, 121);
            this.gb.TabIndex = 136;
            this.gb.TabStop = false;
            // 
            // element
            // 
            this.element.AutoSize = true;
            this.element.Location = new System.Drawing.Point(298, 54);
            this.element.Name = "element";
            this.element.Size = new System.Drawing.Size(47, 18);
            this.element.TabIndex = 146;
            this.element.Text = "label1";
            // 
            // button2
            // 
            this.button2.Image = global::RollartSystemTech.Properties.Resources.undo_icon_24;
            this.button2.Location = new System.Drawing.Point(720, 76);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(47, 31);
            this.button2.TabIndex = 145;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(593, 76);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(122, 31);
            this.button1.TabIndex = 144;
            this.button1.Text = "Update element";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cb4
            // 
            this.cb4.AutoSize = true;
            this.cb4.Enabled = false;
            this.cb4.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb4.ForeColor = System.Drawing.Color.Black;
            this.cb4.Image = global::RollartSystemTech.Properties.Resources.time1;
            this.cb4.Location = new System.Drawing.Point(490, 27);
            this.cb4.Name = "cb4";
            this.cb4.Size = new System.Drawing.Size(55, 26);
            this.cb4.TabIndex = 143;
            this.cb4.Text = "T";
            this.cb4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cb4.UseVisualStyleBackColor = true;
            // 
            // rb4
            // 
            this.rb4.AutoSize = true;
            this.rb4.Enabled = false;
            this.rb4.ForeColor = System.Drawing.Color.Black;
            this.rb4.Location = new System.Drawing.Point(15, 60);
            this.rb4.Name = "rb4";
            this.rb4.Size = new System.Drawing.Size(89, 22);
            this.rb4.TabIndex = 142;
            this.rb4.TabStop = true;
            this.rb4.Text = "Complete";
            this.rb4.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(590, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 18);
            this.label3.TabIndex = 141;
            this.label3.Text = "Add percentage";
            // 
            // cb3
            // 
            this.cb3.AutoSize = true;
            this.cb3.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb3.ForeColor = System.Drawing.Color.Black;
            this.cb3.Location = new System.Drawing.Point(490, 54);
            this.cb3.Name = "cb3";
            this.cb3.Size = new System.Drawing.Size(42, 33);
            this.cb3.TabIndex = 140;
            this.cb3.Text = "*";
            this.cb3.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(296, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 18);
            this.label2.TabIndex = 139;
            this.label2.Text = "label2";
            // 
            // comboPerc
            // 
            this.comboPerc.BackColor = System.Drawing.Color.White;
            this.comboPerc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboPerc.Enabled = false;
            this.comboPerc.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboPerc.FormattingEnabled = true;
            this.comboPerc.Items.AddRange(new object[] {
            "0%",
            "5%",
            "10%",
            "15%",
            "20%",
            "25%",
            "30%",
            "35%",
            "40%",
            "45%",
            "50%",
            "55%",
            "60%",
            "65%",
            "70%",
            "75%",
            "80%",
            "85%",
            "90%",
            "95%",
            "100%"});
            this.comboPerc.Location = new System.Drawing.Point(689, 30);
            this.comboPerc.Name = "comboPerc";
            this.comboPerc.Size = new System.Drawing.Size(78, 24);
            this.comboPerc.TabIndex = 138;
            // 
            // cb2
            // 
            this.cb2.AutoSize = true;
            this.cb2.Enabled = false;
            this.cb2.Image = global::RollartSystemTech.Properties.Resources.font_bold_icon_16;
            this.cb2.Location = new System.Drawing.Point(351, 59);
            this.cb2.Name = "cb2";
            this.cb2.Size = new System.Drawing.Size(113, 22);
            this.cb2.TabIndex = 137;
            this.cb2.Text = "(+2 points)";
            this.cb2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cb2.UseVisualStyleBackColor = true;
            // 
            // cb1
            // 
            this.cb1.AutoSize = true;
            this.cb1.Enabled = false;
            this.cb1.Location = new System.Drawing.Point(351, 31);
            this.cb1.Name = "cb1";
            this.cb1.Size = new System.Drawing.Size(72, 22);
            this.cb1.TabIndex = 136;
            this.cb1.Text = "Combo";
            this.cb1.UseVisualStyleBackColor = true;
            // 
            // rb3
            // 
            this.rb3.AutoSize = true;
            this.rb3.Enabled = false;
            this.rb3.ForeColor = System.Drawing.Color.Blue;
            this.rb3.Location = new System.Drawing.Point(159, 85);
            this.rb3.Name = "rb3";
            this.rb3.Size = new System.Drawing.Size(134, 22);
            this.rb3.TabIndex = 135;
            this.rb3.TabStop = true;
            this.rb3.Text = "<<< Downgraded";
            this.rb3.UseVisualStyleBackColor = true;
            // 
            // rb2
            // 
            this.rb2.AutoSize = true;
            this.rb2.Enabled = false;
            this.rb2.ForeColor = System.Drawing.Color.Blue;
            this.rb2.Location = new System.Drawing.Point(15, 85);
            this.rb2.Name = "rb2";
            this.rb2.Size = new System.Drawing.Size(126, 22);
            this.rb2.TabIndex = 134;
            this.rb2.TabStop = true;
            this.rb2.Text = "<< Half-rotated";
            this.rb2.UseVisualStyleBackColor = true;
            // 
            // rb1
            // 
            this.rb1.AutoSize = true;
            this.rb1.Enabled = false;
            this.rb1.ForeColor = System.Drawing.Color.Blue;
            this.rb1.Location = new System.Drawing.Point(159, 60);
            this.rb1.Name = "rb1";
            this.rb1.Size = new System.Drawing.Size(131, 22);
            this.rb1.TabIndex = 133;
            this.rb1.TabStop = true;
            this.rb1.Text = "< Under-rotated";
            this.rb1.UseVisualStyleBackColor = true;
            // 
            // cbElements
            // 
            this.cbElements.BackColor = System.Drawing.SystemColors.Info;
            this.cbElements.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbElements.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbElements.FormattingEnabled = true;
            this.cbElements.Location = new System.Drawing.Point(15, 29);
            this.cbElements.Name = "cbElements";
            this.cbElements.Size = new System.Drawing.Size(275, 26);
            this.cbElements.TabIndex = 132;
            this.cbElements.SelectedIndexChanged += new System.EventHandler(this.cbElements_SelectedIndexChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.message);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 633);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(788, 29);
            this.panel3.TabIndex = 2;
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.message.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.message.ForeColor = System.Drawing.Color.Red;
            this.message.Location = new System.Drawing.Point(0, 13);
            this.message.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(36, 16);
            this.message.TabIndex = 0;
            this.message.Text = "label1";
            // 
            // CheckScores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 662);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "CheckScores";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "CheckScores";
            this.Load += new System.EventHandler(this.CheckScores_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgElements)).EndInit();
            this.gb.ResumeLayout(false);
            this.gb.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripTextBox rank;
        private System.Windows.Forms.ToolStripLabel numSkaters;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripTextBox name;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel waiting;
        private System.Windows.Forms.ToolStripLabel numPartecipante;
        private System.Windows.Forms.DataGridView dgElements;
        private System.Windows.Forms.GroupBox gb;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox cb4;
        private System.Windows.Forms.RadioButton rb4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cb3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboPerc;
        private System.Windows.Forms.CheckBox cb2;
        private System.Windows.Forms.CheckBox cb1;
        private System.Windows.Forms.RadioButton rb3;
        private System.Windows.Forms.RadioButton rb2;
        private System.Windows.Forms.RadioButton rb1;
        private System.Windows.Forms.ComboBox cbElements;
        private System.Windows.Forms.Label element;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using RollartSystemTech;
using System.Data.SQLite;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Diagnostics;
using System.Threading;

namespace Ghost
{
    public partial class SettingsForm : Form
    {
        //XmlDocument xml = null;
        string ipTech = "", oldValue = "";
        Event ev = null;
        List<DataRow> listParams = null;
        List<DataRow> listParamsPairs = null;
        List<DataRow> listElements = null;
        DataTable dt = null;
        private Point mouse_offset;
        bool staticScreen = false;
        string disciplina = "1";

        public SettingsForm()
        {
            InitializeComponent();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            LoadSettings();
            ev = new Event();
            buTech.PerformClick();
            message.Text = "";
        }

        public void LoadSettings()
        {
            try
            {
                Definizioni.numJudges = RollartSystemTech.Properties.Settings.Default.NumJudges;
                nJudges.Value = Definizioni.numJudges;

                Definizioni.rootFolder = RollartSystemTech.Properties.Settings.Default.EventFolder;
                this.root.Text = Definizioni.rootFolder;

                // IP Tech
                ipTech = GetIP();
                ((IPAddressControlLib.IPAddressControl)this.Controls.Find("ipT", true)[0]).Text =
                        ipTech;

                // Judge Port
                port.Text = "" + RollartSystemTech.Properties.Settings.Default.JudgePort;

                // Ping Time
                cbSec.SelectedItem = "" + RollartSystemTech.Properties.Settings.Default.PingTime; 

                // staticscreen
                staticScreen = RollartSystemTech.Properties.Settings.Default.StaticScreen;
                if (staticScreen)
                {
                    ts1.Checked = true;
                }
                else ts1.Checked = false;

                // screen type
                Definizioni.screenType = RollartSystemTech.Properties.Settings.Default.ScreenType;

                if (Definizioni.screenType.Equals("1"))
                    rb11.Checked = true;
                else rb12.Checked = true;
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        // SAVE SETTINGS
        private void save_Click(object sender, EventArgs e)
        {
            try
            {
                err4.Text = ""; message.Text = "";
                if (titolo.Text.Equals("Settings --> Database"))
                {
                    RollartSystemTech.Properties.Settings.Default.BackupFolder =
                        backupfolder.Text;

                    err4.Text = Definizioni.resources["settings3"].ToString();
                }
                else if (titolo.Text.Equals("Settings --> General"))
                {
                    // save number of judges
                    RollartSystemTech.Properties.Settings.Default.NumJudges = int.Parse(nJudges.Value.ToString());

                    // Judge Port
                    RollartSystemTech.Properties.Settings.Default.JudgePort = int.Parse(port.Text);

                    // Ping time
                    RollartSystemTech.Properties.Settings.Default.PingTime = int.Parse(cbSec.SelectedItem.ToString());
                    
                    // save root
                    RollartSystemTech.Properties.Settings.Default.EventFolder = root.Text;

                    // static screen
                    if (ts1.Checked)
                        RollartSystemTech.Properties.Settings.Default.StaticScreen = true;
                    else
                        RollartSystemTech.Properties.Settings.Default.StaticScreen = false;

                    // screen type
                    if (rb11.Checked)
                        RollartSystemTech.Properties.Settings.Default.ScreenType = "1";
                    else
                        RollartSystemTech.Properties.Settings.Default.ScreenType = "2";

                    err4.Text = Definizioni.resources["settings4"].ToString();
                }

                RollartSystemTech.Properties.Settings.Default.Save();
            }
            catch (SQLiteException ex)
            {
                message.Text = ex.Message;
            }
        }

        # region TechPanel
        private void back_Click(object sender, EventArgs e)
        {
            panTech.BringToFront();
            save.Visible = true;
            titolo.Text = "Settings --> General";
            //titolo.ImageIndex = 11;
            err4.Text = ""; message.Text = "";
        }
        // browse
        private void button14_Click(object sender, EventArgs e)
        {
            if (fd1.ShowDialog() == DialogResult.OK)
            {
                root.Text = fd1.SelectedPath;
            }
        }

        public static string GetIP()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        //public void EnableIPs()
        //{
        //    ip1.Enabled = false; ip2.Enabled = false; ip3.Enabled = false; ip4.Enabled = false;
        //    ip5.Enabled = false; ip6.Enabled = false; ip7.Enabled = false; ip8.Enabled = false;
        //    ip9.Enabled = false;
        //    switch ((int)nJudges.Value)
        //    {
        //        case 1:
        //            ip1.Enabled = true;
        //            break;
        //        case 2:
        //            ip1.Enabled = true; ip2.Enabled = true;
        //            break;
        //        case 3:
        //            ip1.Enabled = true; ip2.Enabled = true; ip3.Enabled = true;
        //            break;
        //        case 4:
        //            ip1.Enabled = true; ip2.Enabled = true; ip3.Enabled = true; ip4.Enabled = true;
        //            break;
        //        case 5:
        //            ip1.Enabled = true; ip2.Enabled = true; ip3.Enabled = true; ip4.Enabled = true;
        //            ip5.Enabled = true;
        //            break;
        //        case 6:
        //            ip1.Enabled = true; ip2.Enabled = true; ip3.Enabled = true; ip4.Enabled = true;
        //            ip5.Enabled = true; ip6.Enabled = true;
        //            break;
        //        case 7:
        //            ip1.Enabled = true; ip2.Enabled = true; ip3.Enabled = true; ip4.Enabled = true;
        //            ip5.Enabled = true; ip6.Enabled = true; ip7.Enabled = true;
        //            break;
        //        case 8:
        //            ip1.Enabled = true; ip2.Enabled = true; ip3.Enabled = true; ip4.Enabled = true;
        //            ip5.Enabled = true; ip6.Enabled = true; ip7.Enabled = true; ip8.Enabled = true;
        //            break;
        //        case 9:
        //            ip1.Enabled = true; ip2.Enabled = true; ip3.Enabled = true; ip4.Enabled = true;
        //            ip5.Enabled = true; ip6.Enabled = true; ip7.Enabled = true; ip8.Enabled = true; ip9.Enabled = true;
        //            break;
        //    }
        //}

        private void nJudges_ValueChanged(object sender, EventArgs e)
        {
            save.Enabled = true; save.BackColor = Color.MediumPurple;
            //EnableIPs();
        }

        private void ipT_TextChanged(object sender, EventArgs e)
        {
            save.Enabled = true; save.BackColor = Color.MediumPurple;
        }

        private void root_TextChanged(object sender, EventArgs e)
        {
            save.Enabled = true; save.BackColor = Color.MediumPurple;
        }

        private void checkCombo2_CheckedChanged(object sender, EventArgs e)
        {
            save.Enabled = true; save.BackColor = Color.MediumPurple;
            if (!ts1.Checked)
            {
                groupBox1.Enabled = false;
            }
            else
            {
                groupBox1.Enabled = true;
            }
        }

        #endregion

        # region Parameters
        // parametri dei singoli/coppie/danza 
        public void LoadParameters()
        {
            try
            {
                listParams = new List<DataRow>();
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    // singoli
                    DataTable dt = new DataTable();
                    cmd.CommandText = "SELECT * FROM SegmentParams";
                    SQLiteDataReader sqdr = cmd.ExecuteReader();
                    dt.Load(sqdr);
                    foreach (DataRow r in dt.Rows)
                    {
                        listParams.Add(r);
                    }
                }

                listParamsPairs = new List<DataRow>();
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    // coppie
                    DataTable dt = new DataTable();
                    cmd.CommandText = "SELECT * FROM SegmentParamsPairs";
                    SQLiteDataReader sqdr = cmd.ExecuteReader();
                    dt.Load(sqdr);
                    foreach (DataRow r in dt.Rows)
                    {
                        listParamsPairs.Add(r);
                    }
                }

            }
            catch (SQLiteException ex)
            {
                message.Text = ex.Message;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            // recupero le categorie
            cbCategory.Items.Clear();
            cbCategory.Items.AddRange(ev.GetCategory().ToArray());
            cb2.Items.Clear();
            cb2.Items.AddRange(ev.GetCategory().ToArray());
            cbCategoryDance.Items.Clear();
            cbCategoryDance.Items.AddRange(ev.GetCategory().ToArray());
            cbPrecision.Items.Clear();
            cbPrecision.Items.AddRange(ev.GetCategory().ToArray());
            panParameters.BringToFront();
            LoadParameters();
            save.Visible = false;
            titolo.Text = "Settings --> Parameters";
            //titolo.ImageIndex = 12;
            err4.Text = ""; message.Text = "";
        }

        // SINGOLI
        private void cbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idCategoria = cbCategory.SelectedIndex + 1;
            if (idCategoria == 5 || idCategoria == 6)
            {
                label13.Text = "Long Program Boys";
                label16.Text = "Long Program Girls";
                label14.Visible = false; label15.Visible = false;
            }
            else if (idCategoria == 1)
            {
                label13.Text = "Short Program Boys";
                label16.Text = "Short Program Girls";
                label14.Text = "Long Program Boys";
                label15.Text = "Long Program Girls";
                label14.Visible = true; label15.Visible = true;
            }
            else
            {
                label13.Text = "Short Program Men";
                label16.Text = "Short Program Ladies";
                label14.Text = "Long Program Men";
                label15.Text = "Long Program Ladies";
                label14.Visible = true; label15.Visible = true;
            }
            GetParamsSingoli(idCategoria);
        }

        public void GetParamsSingoli(int cat)
        {
            try
            {
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    // singoli
                    dt = new DataTable();
                    cmd.CommandText = "SELECT * FROM SegmentParams " +
                        "where id_category = " + cat + " ORDER BY ID_Segment ASC";
                    SQLiteDataReader sqdr = cmd.ExecuteReader();
                    dt.Load(sqdr);
                    
                    dg1.DataSource = dt;
                    dg1.Columns[0].Visible = false;
                    dg1.Columns[1].Visible = false;
                    dg1.Columns[2].Visible = false;
                    dg1.Columns[7].HeaderText = "Segment time (sec)";
                    dg1.Columns[15].Visible = false;
                    for (int i = 0; i < dg1.ColumnCount; i++)
                        dg1.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    //dg1.Columns[7].ToolTipText = "Insert value in seconds";
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void dg1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    int index = int.Parse(dg1[0, e.RowIndex].Value.ToString());
                    if (!dg1[e.ColumnIndex, e.RowIndex].Value.Equals(""))
                    {
                        if (dg1[e.ColumnIndex, e.RowIndex].Value.ToString().Equals(oldValue)) return;
                        command.CommandText = "UPDATE SegmentParams SET " +
                            " " + dg1.Columns[e.ColumnIndex].Name + " = '" + dg1[e.ColumnIndex, e.RowIndex].Value + "'" +
                            " WHERE ID_Params = " + index;
                        command.ExecuteNonQuery();
                        MessageBox.Show("Value updated!");
                    }
                }

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void dg1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            oldValue = dg1[e.ColumnIndex, e.RowIndex].Value.ToString();
        }

        // COPPIE ARTISTICO
        private void cb2_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idCategoria = cb2.SelectedIndex + 1;
            GetParamsCoppie(idCategoria);
            if (idCategoria > 4)
            {
                label20.Text = "Long Program";
                label19.Visible = false; 
            }
            else
            {
                label20.Text = "Short Program";
                label19.Text = "Long Program";
                label19.Visible = true; 
            }
        }

        public void GetParamsCoppie(int cat)
        {
            try
            {
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    // coppie
                    dt = new DataTable();
                    cmd.CommandText = "SELECT * FROM SegmentParamsPairs " +
                        "where id_category = " + cat + " ORDER BY ID_Segment ASC";
                    SQLiteDataReader sqdr = cmd.ExecuteReader();
                    dt.Load(sqdr);
                    dg2.DataSource = dt;
                    dg2.Columns[0].Visible = false;
                    dg2.Columns[1].Visible = false;
                    dg2.Columns[2].Visible = false;
                    dg2.Columns[3].Visible = false;
                    for (int i = 0; i < dg2.ColumnCount; i++)
                        dg2.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    dg2.Columns[4].HeaderText = "Segment time (sec)";
                    //dg2.Columns[4].ToolTipText = "Insert value in seconds";
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void dg2_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            oldValue = dg2[e.ColumnIndex, e.RowIndex].Value.ToString();
        }

        private void dg2_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    int index = int.Parse(dg2[0, e.RowIndex].Value.ToString());
                    if (!dg2[e.ColumnIndex, e.RowIndex].Value.Equals(""))
                    {
                        if (dg2[e.ColumnIndex, e.RowIndex].Value.ToString().Equals(oldValue)) return;
                        command.CommandText = "UPDATE SegmentParamsPairs SET " +
                            " " + dg2.Columns[e.ColumnIndex].Name + " = '" + dg2[e.ColumnIndex, e.RowIndex].Value + "'" +
                            " WHERE ID_SegParamsPairs = " + index;
                        command.ExecuteNonQuery();
                        MessageBox.Show("Value updated!");
                    }
                }

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        // Dance - Solo Dance
        private void cbCategoryDance_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idCategoria = cbCategoryDance.SelectedIndex + 1;
            GetParamsDance(idCategoria);
            if (idCategoria == 4 || idCategoria == 3) // jun e sen
            {
                label39.Text = "Style Dance";
            }
            else
            {
                label39.Text = "Compulsory Dances";
            }

        }

        public void GetParamsDance(int cat)
        {
            try
            {
                int idSpec = 0;
                if (rb1.Checked) idSpec = int.Parse(rb1.Tag.ToString());
                if (rb2.Checked) idSpec = int.Parse(rb2.Tag.ToString());
                if (rb3.Checked) idSpec = int.Parse(rb3.Tag.ToString());
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    dt = new DataTable();
                    cmd.CommandText = "SELECT * FROM SegmentParamsDance" +
                        " where id_category = " + cat + " AND id_specialita = "
                        + idSpec + " ORDER BY ID_Segment ASC";
                    SQLiteDataReader sqdr = cmd.ExecuteReader();
                    dt.Load(sqdr);
                    dgDance.DataSource = dt;
                    dgDance.Columns[0].Visible = false;
                    dgDance.Columns[1].Visible = false;
                    dgDance.Columns[2].Visible = false;
                    dgDance.Columns[3].Visible = false;
                    for (int i = 0; i < dgDance.ColumnCount; i++)
                        dgDance.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    dgDance.Columns[4].HeaderText = "Segment time (sec)";
                    //dgDance.Columns[4].ToolTipText = "Insert value in seconds";
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void dgDance_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            oldValue = dgDance[e.ColumnIndex, e.RowIndex].Value.ToString();
        }

        private void dgDance_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    int index = int.Parse(dgDance[0, e.RowIndex].Value.ToString());
                    if (!dgDance[e.ColumnIndex, e.RowIndex].Value.Equals(""))
                    {
                        if (dgDance[e.ColumnIndex, e.RowIndex].Value.ToString().Equals(oldValue)) return;
                        command.CommandText = "UPDATE SegmentParamsDance SET " +
                            " " + dgDance.Columns[e.ColumnIndex].Name + " = '" + dgDance[e.ColumnIndex, e.RowIndex].Value + "'" +
                            " WHERE ID_Params = " + index;
                        command.ExecuteNonQuery();
                        MessageBox.Show("Value updated!");
                    }
                }

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        // Precision
        public void GetParamsPrecision(int cat)
        {
            try
            {
                //int idSpec = 0;
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    dt = new DataTable();
                    cmd.CommandText = "SELECT * FROM SegmentParamsPrecision" +
                        " where id_category = " + cat + " ORDER BY ID_Segment ASC";
                    SQLiteDataReader sqdr = cmd.ExecuteReader();
                    dt.Load(sqdr);
                    dgPrecision.DataSource = dt;
                    dgPrecision.Columns[0].Visible = false;
                    dgPrecision.Columns[1].Visible = false;
                    dgPrecision.Columns[2].Visible = false;
                    dgPrecision.Columns[3].Visible = false;
                    for (int i=0; i<dgPrecision.ColumnCount; i++)
                        dgPrecision.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    dgPrecision.Columns[4].HeaderText = "Segment time (sec)";
                    //dgPrecision.Columns[4].ToolTipText = "Insert value in seconds";
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void rb1_CheckedChanged(object sender, EventArgs e)
        {
            int idCategoria = cbCategoryDance.SelectedIndex + 1;
            GetParamsDance(idCategoria);
        }

        private void rb2_CheckedChanged(object sender, EventArgs e)
        {
            int idCategoria = cbCategoryDance.SelectedIndex + 1;
            GetParamsDance(idCategoria);
        }

        private void rb3_CheckedChanged(object sender, EventArgs e)
        {
            int idCategoria = cbCategoryDance.SelectedIndex + 1;
            GetParamsDance(idCategoria);
        }


        #endregion

        # region Elements
        public void LoadElements()
        {
            try
            {
                listElements = new List<DataRow>();
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    // singoli
                    DataTable dt = new DataTable();
                    cmd.CommandText = "SELECT * FROM Elements";
                    SQLiteDataReader sqdr = cmd.ExecuteReader();
                    dt.Load(sqdr);
                    foreach (DataRow r in dt.Rows)
                    {
                        listElements.Add(r);
                    }
                }
            }
            catch (SQLiteException ex)
            {
                message.Text = ex.Message;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // recupero le categorie
            cb3.Items.Clear();
            cb3.Items.AddRange(ev.GetCatElements().ToArray());
            //LoadElements();
            panElements.BringToFront();
            save.Visible = false;
            titolo.Text = "Settings --> Elements";
            //titolo.ImageIndex = 5;
            err4.Text = ""; message.Text = "";
            
        }

        private void cb3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            { 
                int idCatEl = cb3.SelectedIndex + 1;
                GetElements(idCatEl);
                label45.Text = cb3.SelectedItem.ToString().ToUpper();
                panEl.Visible = true;
                label4.Visible = true;
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        public void GetElements(int cat)
        {
            try
            {
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    // coppie
                    dt = new DataTable();
                    cmd.CommandText = "SELECT * FROM Elements " +
                        "where id_elementscat = " + cat + " ORDER BY ID_Element ASC";
                    SQLiteDataReader sqdr = cmd.ExecuteReader();
                    dt.Load(sqdr);
                    dg3.DataSource = dt;
                    dg3.Columns[0].HeaderText = "ID"; dg3.Columns[0].Width = 50;
                    dg3.Columns[1].Width = 150;
                    dg3.Columns[2].Visible = false;
                    dg3.Columns[4].HeaderText = "+3"; dg3.Columns[4].Width = 40;
                    dg3.Columns[5].HeaderText = "+2"; dg3.Columns[5].Width = 40;
                    dg3.Columns[6].HeaderText = "+1"; dg3.Columns[6].Width = 40;
                    dg3.Columns[8].HeaderText = "Under"; dg3.Columns[8].Visible = false;
                    dg3.Columns[9].HeaderText = "Half"; dg3.Columns[9].Visible = false;
                    dg3.Columns[10].HeaderText = "-1"; dg3.Columns[10].Width = 40;
                    dg3.Columns[11].HeaderText = "-2"; dg3.Columns[11].Width = 40;
                    dg3.Columns[12].HeaderText = "-3"; dg3.Columns[12].Width = 40;
                    dg3.Columns[13].HeaderText = "Combo"; dg3.Columns[13].Visible = false;
                    dg3.Columns[14].HeaderText = "ComboUnder"; dg3.Columns[14].Visible = false;
                    dg3.Columns[15].HeaderText = "ComboHalf"; dg3.Columns[15].Visible = false;
                    dg3.Columns[16].Visible = false;
                    dg3.Columns[17].Visible = false;

                    if (cat == 1) // salti
                    {
                        dg3.Columns[8].Visible = true;
                        dg3.Columns[9].Visible = true;
                        dg3.Columns[13].Visible = true;
                        dg3.Columns[14].Visible = true;
                        dg3.Columns[15].Visible = true;
                    } else if (cat == 4 || cat == 8) // salti coppia
                    {
                        dg3.Columns[8].Visible = true;
                        dg3.Columns[9].Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void dg3_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            //oldValue = dg3[e.ColumnIndex, e.RowIndex].Value.ToString();
        }

        private void dg3_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //try
            //{
            //    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
            //    {
            //        int index = int.Parse(dg3[0, e.RowIndex].Value.ToString());
            //        if (!dg3[e.ColumnIndex, e.RowIndex].Value.Equals(""))
            //        {
            //            if (dg3[e.ColumnIndex, e.RowIndex].Value.ToString().Equals(oldValue)) return;
            //            command.CommandText = "UPDATE Elements SET " +
            //                " " + dg3.Columns[e.ColumnIndex].Name + " = '" + dg3[e.ColumnIndex, e.RowIndex].Value + "'" +
            //                " WHERE ID_Element = " + index;
            //            command.ExecuteNonQuery();
            //            MessageBox.Show("Element updated!");
            //        }
            //    }

            //}
            //catch (Exception ex)
            //{
            //    message.Text = ex.Message;
            //}
        }

        // print 
        private void label4_Click(object sender, EventArgs e)
        {
            try
            {
                //Assign printPreviewDialog properties
                this.printDocument1.DefaultPageSettings.Landscape = true;
                printPreviewDialog1.Document = printDocument1;
                printPreviewDialog1.PrintPreviewControl.Zoom = 1;
                printPreviewDialog1.ShowDialog();
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            try
            {
                Bitmap dataGridViewImage = new Bitmap(this.panEl.Width, this.panEl.Height);
                //dg3.DrawToBitmap(dataGridViewImage, new Rectangle(0, 50, this.dg3.Width, this.dg3.Height));
                panEl.DrawToBitmap(dataGridViewImage, new Rectangle(0, 10, this.panEl.Width, this.panEl.Height));
                e.Graphics.DrawImage(dataGridViewImage, 0, 0);
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        #endregion

        # region Database
        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                panDB.BringToFront();
                save.Visible = true;
                titolo.Text = "Settings --> Database";
                lv1.Items.Clear();
                lv2.Items.Clear();
                lv3.Items.Clear();
                err4.Text = ""; message.Text = "";
                foreach (string spec in ev.GetSpecialita())
                {
                    lv1.Items.Add(spec);
                }
                foreach (string cat in ev.GetCategory())
                {
                    lv2.Items.Add(cat);
                }
                foreach (string seg in ev.GetSegments())
                {
                    lv3.Items.Add(seg);
                }
                dbName.Text =
                    RollartSystemTech.Properties.Settings.Default.dbconnection.Split(';')[0].Substring(14);
                backupfolder.Text = RollartSystemTech.Properties.Settings.Default.BackupFolder;
                }
            catch (SQLiteException ex)
            {
                message.Text = ex.Message;
            }
        }

        private void dbName_TextChanged(object sender, EventArgs e)
        {
            save.Enabled = true; save.BackColor = Color.MediumPurple;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                message.Text = "";

                string newDB = dbName.Text.Replace("rolljudge2.s3db", "rolljudge2_");

                string newName = DateTime.Now.ToString("yyyyMMddHHmmss");
                //string newName = DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + "_" +
                //    DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second;

                using (var source = new SQLiteConnection("Data Source=" + dbName.Text + "; Version=3;"))
                using (var destination = new SQLiteConnection("Data Source=" + newDB + newName + ".s3db;foreign keys=True"))
                {
                    source.Open();
                    destination.Open();
                    source.BackupDatabase(destination, "main", "main", -1, null, 0);

                    source.Close();
                    destination.Close();

                    if (!System.IO.Directory.Exists(backupfolder.Text))
                    {
                        System.IO.Directory.CreateDirectory(backupfolder.Text);
                    }
                    File.Move(newDB + newName + ".s3db",
                            backupfolder.Text + "\\rolljudge2_" + newName + ".s3db");

                    mess1.Text = Definizioni.resources["backupcompleted"].ToString() +
                        "rolljudge2_" + newName + ".s3db saved in " + backupfolder.Text;

                    Process.Start("explorer.exe", backupfolder.Text);

                    source.Open();
                }             
            }
            catch (SQLiteException ex)
            {
                mess1.Text += Definizioni.resources["backuperror"].ToString() + ex.StackTrace;
            }
        }

        private void lv1_AfterLabelEdit(object sender, LabelEditEventArgs e)
        {
            try
            {
                if (e == null) return;
                if (e.Label == null) return;
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    
                    if (!e.Label.Equals(""))
                    {
                        command.CommandText = "UPDATE Specialita SET " +
                            " Name = '" + e.Label.Replace("'", "''") + "'" +
                            " WHERE ID_Specialita = " + (e.Item + 1);
                        command.ExecuteNonQuery();
                        message.Text = "Discipline name updated!";
                    }
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void lv2_AfterLabelEdit(object sender, LabelEditEventArgs e)
        {
            try
            {
                if (e == null) return;
                if (e.Label == null) return;
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    if (!e.Label.Equals(""))
                    {
                        command.CommandText = "UPDATE Category SET " +
                            " Name = '" + e.Label.Replace("'", "''") + "'" +
                            " WHERE ID_Category = " + (e.Item + 1);
                        command.ExecuteNonQuery();
                        message.Text = "Category name updated!";
                    }
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void lv3_AfterLabelEdit(object sender, LabelEditEventArgs e)
        {
            try
            {
                if (e == null) return;
                if (e.Label == null) return;
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    if (!e.Label.Equals(""))
                    {
                        command.CommandText = "UPDATE Segments SET " +
                            " Name = '" + e.Label.Replace("'", "''") + "'" +
                            " WHERE ID_Segments = " + (e.Item + 1);
                        command.ExecuteNonQuery();
                        message.Text = "Segment name updated!";
                    }
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void browse_Click(object sender, EventArgs e)
        {
            message.Text = "";
            if (fd1.ShowDialog() == DialogResult.OK)
            {
                backupfolder.Text = fd1.SelectedPath;
            }
        }

        #endregion

        # region Competitors
        private void button1_Click(object sender, EventArgs e)
        {
            panCompetitors.BringToFront();
            save.Visible = false;
            titolo.Text = "Settings --> Competitors";
            err2.Text = ""; message.Text = "";
        }

        public void GetCompetitors()
        {
            message.Text = ""; err2.Text = "";
            found2.Visible = false;
            {
                dg4.DataSource = null; 

                bool RegionPresente = Utility.CheckIfColumnExists("Athletes", "Region");
                bool speciaPresente = Utility.CheckIfColumnExists("Athletes", "ID_Specialita");

                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    dt = new DataTable();

                    if (RegionPresente && speciaPresente)
                        cmd.CommandText = "SELECT A.ID_Atleta, A.Name, A.Societa, A.Country, A.Region, S.Name as Spec FROM Athletes as A, Specialita as S WHERE " +
                            "A.ID_Specialita = S.ID_Specialita AND " +
                            "A.ID_Specialita IN (" + disciplina + ") AND " +
                            "A.Name like '" + name.Text + "%' AND " +
                            "A.Societa like '" + club.Text + "%' AND " +
                            "A.Region like '" + region2.Text + "%' AND " +
                            "A.Country like '" + country.Text + "%' ORDER BY A.Name ASC";
                    else
                        cmd.CommandText = "SELECT A.Name, A.Societa, A.Country FROM Athletes as A WHERE " +
                            "A.Name like '" + name.Text + "%' AND " +
                            "A.Societa like '" + club.Text + "%' AND " +
                            "A.Country like '" + country.Text + "%' ORDER BY Name ASC";
                    SQLiteDataReader sqdr = cmd.ExecuteReader();

                    dt.Load(sqdr);

                    dg4.AutoGenerateColumns = false;
                    dg4.Columns["dg4ID"].DataPropertyName = "ID_Atleta"; dg4.Columns["dg4ID"].Visible = false;
                    dg4.Columns["dg4Name"].DataPropertyName = "Name";
                    dg4.Columns["dg4Societa"].DataPropertyName = "Societa";
                    dg4.Columns["dg4Country"].DataPropertyName = "Country";
                    ((DataGridViewTextBoxColumn)dg4.Columns["dg4Country"]).MaxInputLength = 3;

                    if (RegionPresente && speciaPresente)
                    {
                        dg4.Columns["dg4Region"].DataPropertyName = "Region";
                        dg4.Columns["dg4Spec"].DataPropertyName = "Spec";
                        //dg4.Columns["dg4Spec"].DataPropertyName = "ID_Specialita"; dg4.Columns["dg4Spec"].Visible = false;
                    }
                    dg4.DataSource = dt;
                }

                dg4.ClearSelection();

                if (dg4.RowCount > 0)
                {
                    found2.Text = dt.Rows.Count + " found";
                    found2.Visible = true;
                }
                else
                {
                }
            }
        }

        private void dg4_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                oldValue = dg4[e.ColumnIndex, e.RowIndex].Value.ToString();
            }
            catch (Exception)
            {
                //message.Text = ex.Message;
            }
        }

        private void dg4_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                message.Text = ""; err2.Text = "";
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    int index = int.Parse(dg4[0, e.RowIndex].Value.ToString());
                    if (!dg4[e.ColumnIndex, e.RowIndex].Value.Equals(""))
                    {
                        if (dg4[e.ColumnIndex, e.RowIndex].Value.ToString().Equals(oldValue)) return;
                        command.CommandText = "UPDATE Athletes SET " +
                            " " + dg4.Columns[e.ColumnIndex].Name.Remove(0, 3) + " = '" + 
                                  dg4[e.ColumnIndex, e.RowIndex].Value.ToString().Replace("'", "''").ToUpper() + "'" +
                            " WHERE ID_Atleta = " + index;
                        command.ExecuteNonQuery();
                        err2.Text = "Value updated";
                        dg4[e.ColumnIndex, e.RowIndex].Style.ForeColor = Color.Red;
                    }
                }
            }
            catch (SQLiteException ex)
            {
                if (ex.ErrorCode == 19)
                {
                    message.Text = "Same competitor inserted.";
                }
                else message.Text = ex.Message;
            }
        }

        // add competitor
        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                string nome = name.Text;
                string regione = region2.Text;
                string paese = country.Text;
                string societa = club.Text;

                if (name.TextLength == 0)
                {
                    err2.Text = Definizioni.resources["settings1"].ToString();
                    name.Focus();
                    return;
                }

                bool RegionPresente = Utility.CheckIfColumnExists("Athletes", "Region");
                bool speciaPresente = Utility.CheckIfColumnExists("Athletes", "ID_Specialita");

                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    if (RegionPresente && speciaPresente)
                        cmd.CommandText = "INSERT INTO Athletes (Name, Societa, Country, Region, ID_Specialita) VALUES('" +
                            name.Text.TrimEnd().ToUpper().Replace("'", "''") + "','" + club.Text.TrimEnd().ToUpper().Replace("'", "''") + "','" + country.Text.TrimEnd().ToUpper().Replace("'", "''") + "','" + 
                            region2.Text.TrimEnd().ToUpper().Replace("'", "''") + "'," + disciplina + ")";
                    else
                        cmd.CommandText = "INSERT INTO Athletes (Name, Societa, Country) VALUES('" +
                            name.Text.TrimEnd().ToUpper().Replace("'", "''") + "','" + club.Text.TrimEnd().ToUpper().Replace("'", "''") + "','" + country.Text.TrimEnd().ToUpper().Replace("'", "''") + "')";

                    cmd.ExecuteReader();
                    name.Clear(); club.Clear(); country.Clear(); region2.Clear();
                    GetCompetitors();
                }

                foreach (DataGridViewRow dgvr in dg4.Rows)
                {
                    if (dgvr.Cells[1].Value.Equals(nome) && dgvr.Cells[2].Value.Equals(societa) &&
                        dgvr.Cells[3].Value.Equals(regione) && dgvr.Cells[4].Value.Equals(paese))
                    {
                        dgvr.Selected = true;
                        break;
                    }
                }

                if (dg4.SelectedRows.Count > 0)
                    dg4.FirstDisplayedScrollingRowIndex = dg4.SelectedRows[0].Index;
                name.Focus();
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }

        }
        // search
        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                GetCompetitors();
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void DisplayDataCompetitors()
        {
            try
            {
                SetLoading(true, pb2);

                Thread.Sleep(1000);
                GetCompetitors();

                SetLoading(false, pb2);
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        // delete 
        private void delete_Click(object sender, EventArgs e)
        {
            try
            {
                if (dg4.SelectedRows.Count > 0)
                {
                    DialogResult res;
                    if (dg4.SelectedRows.Count == 1)
                        res = MessageBox.Show("This will delete competitor " + 
                            dg4.SelectedRows[0].Cells[1].Value.ToString().ToUpper() + ". Are you sure? ",
                        "RollartSystem", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    else res = MessageBox.Show("This will delete " + dg4.SelectedRows.Count + " competitors. Are you sure? ",
                            "RollartSystem", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    if (res == DialogResult.Yes)
                    {
                        int selezionati = dg4.SelectedRows.Count;
                        for (int i = 0; i < selezionati; i++)
                        {
                            using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                            {
                                cmd.CommandText = "DELETE FROM Athletes WHERE ID_Atleta = "
                                + dg4[0, dg4.SelectedRows[selezionati - 1 - i].Index].Value;
                                cmd.ExecuteReader();
                                dg4.Rows.RemoveAt(dg4.SelectedRows[selezionati - 1 - i].Index);
                                
                            }
                        }

                        GetCompetitors();
                        found2.Text = dt.Rows.Count + " found";
                    }
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked) disciplina = "1";
            label27.Text = "Skater*";
            add.Enabled = true;
            radioButton6.ForeColor = Color.Blue; radioButton2.ForeColor = Color.Blue; radioButton3.ForeColor = Color.Blue;
            radioButton4.ForeColor = Color.Blue; radioButton5.ForeColor = Color.Blue; radioButton7.ForeColor = Color.Blue;
            radioButton1.ForeColor = Color.Red; radioButton8.ForeColor = Color.Blue;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked) disciplina = "3";
            label27.Text = "Pair*";
            add.Enabled = true;
            radioButton1.ForeColor = Color.Blue; radioButton6.ForeColor = Color.Blue; radioButton3.ForeColor = Color.Blue;
            radioButton4.ForeColor = Color.Blue; radioButton5.ForeColor = Color.Blue; radioButton7.ForeColor = Color.Blue;
            radioButton2.ForeColor = Color.Red; radioButton8.ForeColor = Color.Blue;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked) disciplina = "4";
            label27.Text = "Couple*";
            add.Enabled = true;
            radioButton1.ForeColor = Color.Blue; radioButton2.ForeColor = Color.Blue; radioButton6.ForeColor = Color.Blue;
            radioButton4.ForeColor = Color.Blue; radioButton5.ForeColor = Color.Blue; radioButton7.ForeColor = Color.Blue;
            radioButton3.ForeColor = Color.Red; radioButton8.ForeColor = Color.Blue;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton4.Checked) disciplina = "5";
            label27.Text = "Skater*";
            add.Enabled = true;
            radioButton1.ForeColor = Color.Blue; radioButton2.ForeColor = Color.Blue; radioButton3.ForeColor = Color.Blue;
            radioButton6.ForeColor = Color.Blue; radioButton5.ForeColor = Color.Blue; radioButton7.ForeColor = Color.Blue;
            radioButton4.ForeColor = Color.Red; radioButton8.ForeColor = Color.Blue;
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton5.Checked) disciplina = "7";
            label27.Text = "Group*";
            add.Enabled = true;
            radioButton1.ForeColor = Color.Blue; radioButton2.ForeColor = Color.Blue; radioButton3.ForeColor = Color.Blue;
            radioButton4.ForeColor = Color.Blue; radioButton5.ForeColor = Color.Blue; radioButton7.ForeColor = Color.Blue;
            radioButton5.ForeColor = Color.Red; radioButton6.ForeColor = Color.Blue;
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton6.Checked) disciplina = "1,2,3,4,5,6,7,8,9,10";
            add.Enabled = false;
            radioButton1.ForeColor = Color.Blue; radioButton2.ForeColor = Color.Blue; radioButton3.ForeColor = Color.Blue;
            radioButton4.ForeColor = Color.Blue; radioButton5.ForeColor = Color.Blue; radioButton7.ForeColor = Color.Blue;
            radioButton6.ForeColor = Color.Red; radioButton8.ForeColor = Color.Blue;
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton7.Checked) disciplina = "2";
            label27.Text = "Skater*";
            add.Enabled = true;
            radioButton1.ForeColor = Color.Blue; radioButton2.ForeColor = Color.Blue; radioButton3.ForeColor = Color.Blue;
            radioButton4.ForeColor = Color.Blue; radioButton5.ForeColor = Color.Blue; radioButton6.ForeColor = Color.Blue;
            radioButton7.ForeColor = Color.Red; radioButton8.ForeColor = Color.Blue;
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton8.Checked) disciplina = "6";
            label27.Text = "Skater*";
            add.Enabled = true;
            radioButton1.ForeColor = Color.Blue; radioButton2.ForeColor = Color.Blue; radioButton3.ForeColor = Color.Blue;
            radioButton4.ForeColor = Color.Blue; radioButton5.ForeColor = Color.Blue; radioButton7.ForeColor = Color.Blue;
            radioButton8.ForeColor = Color.Red; radioButton6.ForeColor = Color.Blue;
        }

        // Import Skaters from XML
        private void button19_Click(object sender, EventArgs e)
        {
            try
            {
                message.Text = "";
                pb2.Visible = true;
                openXML.InitialDirectory = RollartSystemTech.Properties.Settings.Default.dbfolder;
                openXML.Title = "Import competitors to Rollart database";
                openXML.RestoreDirectory = true;
                openXML.FileName = "Rollart_competitors"; 

                if (openXML.ShowDialog() == DialogResult.OK)
                {
                    DataSet dataSet = new DataSet();
                    dataSet.ReadXml(openXML.FileName);

                    // Inserisco i giudici nel db
                    // se già esiste non inserisco
                    int countInserted = 0;

                    bool RegionPresente = Utility.CheckIfColumnExists("Athletes", "Region");
                    bool speciaPresente = Utility.CheckIfColumnExists("Athletes", "ID_Specialita");

                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        if (RegionPresente && speciaPresente)
                        {
                            // 2.0.0.10 - 07/02/2019 BUG Aggiungo anche l'id discipline 
                            command.CommandText = "INSERT INTO Athletes (Name, Societa, Country, Region, ID_Specialita)";
                            //" VALUES (@param1,@param2,@param3,@param4,@param5)";
                            command.CommandText += " SELECT @param1, @param2, @param3, @param4, ID_Specialita FROM Specialita as S WHERE S.Name = @param5";
                        }
                        else
                            command.CommandText = "INSERT INTO Athletes (Name, Societa, Country)" +
                                                  " VALUES (@param1,@param2,@param3)";
                        //int b = 0;
                        //for (int i = 0; i < dg4.RowCount; i++)
                        for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                        {
                            command.Parameters.AddWithValue("@param1", dataSet.Tables[0].Rows[i][1].ToString()); // nome atleta
                            command.Parameters.AddWithValue("@param2", dataSet.Tables[0].Rows[i][2].ToString()); // società atleta
                            command.Parameters.AddWithValue("@param3", dataSet.Tables[0].Rows[i][4].ToString()); // nation atleta
                            if (RegionPresente && speciaPresente)
                            {
                                command.Parameters.AddWithValue("@param4", dataSet.Tables[0].Rows[i][3].ToString()); // region atleta
                                command.Parameters.AddWithValue("@param5", dataSet.Tables[0].Rows[i][5]); // spec atleta
                            }
                            try
                            {
                                command.ExecuteNonQuery();
                                countInserted++;
                            }
                            catch (Exception)
                            {
                                //MessageBox.Show("Judge " + Judges.judges[i].ToString() + " already inserted.");
                            }
                        }
                    }
                    if (countInserted == 0) MessageBox.Show("No new competitors found.", "RollArt Tech Panel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        MessageBox.Show(countInserted + " competitors imported with success.", "RollArt Tech Panel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        // Find all skaters 
                        //Thread threadInput = new Thread(DisplayDataCompetitors);
                        //threadInput.Start();
                        GetCompetitors();
                    }
                    
                }
                pb2.Visible = false;
            }
            catch (Exception ex)
            {
                message.Text = "File error: " + ex.Message;
                pb2.Visible = false;
            }
        }

        // Export Skaters to XML
        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                if (dg4.Rows.Count == 0)
                {
                    //found2.Text = "";
                    return;
                }
                pb2.Visible = true;

                saveFileDialog1.InitialDirectory = RollartSystemTech.Properties.Settings.Default.dbfolder;
                saveFileDialog1.Title = "Export Competitors to xml file";
                if (disciplina.Contains(","))
                    saveFileDialog1.FileName = "Rollart_Competitors";
                else
                    saveFileDialog1.FileName = "Rollart_Competitors_" + dg4.Rows[0].Cells[5].Value;
                saveFileDialog1.Filter = "xml file|*.xml|all files|*.*";
                saveFileDialog1.RestoreDirectory = true;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    DataTable dt = new DataTable();
                    foreach (DataGridViewColumn col in dg4.Columns)
                    {
                        dt.Columns.Add(col.HeaderText);
                    }
                    foreach (DataGridViewRow row in dg4.Rows)
                    {
                        DataRow dRow = dt.NewRow();
                        foreach (DataGridViewCell cell in row.Cells)
                        {
                            dRow[cell.ColumnIndex] = cell.Value;
                        }
                        dt.Rows.Add(dRow);
                    }

                    dt.TableName = "Competitor";
                    try
                    {
                        dt.WriteXml(this.saveFileDialog1.FileName, XmlWriteMode.WriteSchema);
                        dt.Dispose();
                        Process.Start("explorer.exe", saveFileDialog1.FileName);
                    }
                    catch (NullReferenceException) { }
                }

                pb2.Visible = false;
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                pb2.Visible = false;
            }
        }

        #endregion

        #region Judges
        private void button13_Click(object sender, EventArgs e)
        {
            panJudges.BringToFront();
            save.Visible = false;
            titolo.Text = "Settings --> Officials";
            err4.Text = ""; message.Text = "";
        }

        private void dg5_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                oldValue = dg5[e.ColumnIndex, e.RowIndex].Value.ToString();
            }
            catch (Exception)
            {
                //message.Text = ex.Message;
            }
            //oldValue = dg5[e.ColumnIndex, e.RowIndex].Value.ToString();
        }

        private void dg5_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int i = 1;
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                foreach (DataGridViewRow row in dg5.Rows)
                {
                    row.HeaderCell.Value = i + ") ";
                    i++;
                }
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
            catch (Exception ex)
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
                message.Text = ex.Message;
            }
        }

        private void dg5_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                message.Text = ""; err3.Text = "";
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    int index = int.Parse(dg5[0, e.RowIndex].Value.ToString());
                    if (!dg5[e.ColumnIndex, e.RowIndex].Value.Equals(""))
                    {
                        if (dg5[e.ColumnIndex, e.RowIndex].Value.ToString().Equals(oldValue)) return;
                        command.CommandText = "UPDATE Judges SET " +
                            " " + dg5.Columns[e.ColumnIndex].Name.Remove(0,3) + " = '" 
                                + dg5[e.ColumnIndex, e.RowIndex].Value.ToString().ToUpper().Replace("'", "''") + "'" +
                            " WHERE ID_Judge = " + index;
                        command.ExecuteNonQuery();
                        err3.Text = "Value updated";
                    }
                }

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        // add judge
        private void button12_Click(object sender, EventArgs e)
        {
            try
            {
                string nome = name2.Text;
                string regione = region.Text;
                string paese = country2.Text;

                if (name2.TextLength == 0)
                {
                    err3.Text = Definizioni.resources["settings2"].ToString();
                    return;
                }
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    cmd.CommandText = "INSERT INTO Judges (Name, Region, Country) VALUES('" +
                        name2.Text.ToString().Replace("'", "''") + "','" + region.Text.ToString().Replace("'", "''") + "','" + country2.Text.ToString().Replace("'", "''") + "')";
                    cmd.ExecuteReader();
                    name2.Clear(); region.Clear(); country2.Clear();
                    DisplayDataOfficals();
                    //threadInput.Start();
                }

                foreach (DataGridViewRow dgvr in dg5.Rows)
                {
                    if (dgvr.Cells[1].Value.Equals(nome) && dgvr.Cells[2].Value.Equals(regione) &&
                        dgvr.Cells[3].Value.Equals(paese))
                    {
                        dgvr.Selected = true;
                        break;
                    }
                }

                dg5.FirstDisplayedScrollingRowIndex = dg5.SelectedRows[0].Index;
                name2.Focus();
            }
            catch (Exception ex)
            {
                err3.Text = "Official already inserted. (" + ex.Message + ")";
                pb.Visible = false;
            }
        }
        // search
        private void button11_Click(object sender, EventArgs e)
        {
            try
            {
                GetJudges();
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
            
        }

        private void DisplayDataOfficals()
        {
            try
            {
                SetLoading(true, pb);

                Thread.Sleep(2000);
                GetJudges();

                SetLoading(false, pb);
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        public void GetJudges()
        {
            message.Text = ""; err3.Text = "";
            found.Visible = false;
            //this.Invoke((MethodInvoker)delegate
            {
                dg5.DataSource = null; dg5.Refresh();
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    dt = new DataTable();
                    cmd.CommandText = "SELECT * FROM Judges WHERE " +
                        "Name like '" + name2.Text + "%' AND " +
                        "Region like '" + region.Text + "%' AND " +
                        "Country like '" + country2.Text + "%' ORDER BY Name ASC";
                    SQLiteDataReader sqdr = cmd.ExecuteReader();
                    dt.Load(sqdr);

                    dg5.AutoGenerateColumns = false;
                    dg5.Columns["id"].DataPropertyName = "ID_Judge"; dg5.Columns["id"].Visible = false;
                    dg5.Columns["dg5Name"].DataPropertyName = "Name";
                    dg5.Columns["dg5Region"].DataPropertyName = "Region";
                    dg5.Columns["dg5Country"].DataPropertyName = "Country";
                    ((DataGridViewTextBoxColumn)dg5.Columns["dg5Country"]).MaxInputLength = 3;
                    dg5.DataSource = dt;
                }

                dg5.ClearSelection();
                dg5.Visible = true;
                if (dg5.Rows.Count > 0)
                {
                    dg5.Columns[0].Visible = false;
                }

                if (dg5.RowCount > 0)
                {
                    found.Text = dt.Rows.Count + " found";
                    found.Visible = true;
                }
            }
        }


        private void SetLoading(bool displayLoader, PictureBox pb)
        {
            if (displayLoader)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    pb.Visible = true;
                    this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                });
            }
            else
            {
                this.Invoke((MethodInvoker)delegate
                {
                    pb.Visible = false;
                    this.Cursor = System.Windows.Forms.Cursors.Default;
                });
            }
        }

        // delete
        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                if (dg5.SelectedRows.Count > 0)
                {
                    DialogResult res;
                    if (dg5.SelectedRows.Count == 1)
                            res = MessageBox.Show("This will delete official number " + (dg5.SelectedRows[0].Index + 1) + ") " +
                                dg5.SelectedRows[0].Cells[1].Value + ". Are you sure? ", 
                            "RollartSystem", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    else res = MessageBox.Show("This will delete " + dg5.SelectedRows.Count + " officials. Are you sure ? ",
                            "RollartSystem", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

                    if (res == DialogResult.Yes)
                    {
                        int selezionati = dg5.SelectedRows.Count;
                        for (int i = 0; i < selezionati; i++)
                        {
                            using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                            {
                                cmd.CommandText = "DELETE FROM Judges WHERE ID_Judge = "
                                    + dg5[0, dg5.SelectedRows[selezionati - 1 - i].Index].Value;
                                cmd.ExecuteReader();
                                dg5.Rows.RemoveAt(dg5.SelectedRows[selezionati - 1 - i].Index);
                            }
                        }
                        found.Text = dg5.Rows.Count + " found";
                    }
                }
            }
            catch (Exception ex)
            {
                err3.Text = ex.Message;
                pb.Visible = false;
            }
        }

        // Export OFFICIALS to xml
        private void button17_Click(object sender, EventArgs e)
        {
            try
            {
                if (dg5.Rows.Count == 0)
                {
                    //found2.Text = "";
                    return;
                }
                pb.Visible = true;

                saveFileDialog1.InitialDirectory = RollartSystemTech.Properties.Settings.Default.dbfolder;
                saveFileDialog1.Title = "Export Officials to xml file";
                saveFileDialog1.FileName = "Rollart_Officials";
                saveFileDialog1.Filter = "xml file|*.xml|all files|*.*";
                saveFileDialog1.RestoreDirectory = true;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    DataTable dt = new DataTable();
                    foreach (DataGridViewColumn col in dg5.Columns)
                    {
                        dt.Columns.Add(col.HeaderText);
                    }
                    foreach (DataGridViewRow row in dg5.Rows)
                    {
                        DataRow dRow = dt.NewRow();
                        foreach (DataGridViewCell cell in row.Cells)
                        {
                            dRow[cell.ColumnIndex] = cell.Value;
                        }
                        dt.Rows.Add(dRow);
                    }
                    dt.TableName = "Official";
                    try
                    {
                        dt.WriteXml(this.saveFileDialog1.FileName, XmlWriteMode.WriteSchema);
                        dt.Dispose();
                        Process.Start("explorer.exe", saveFileDialog1.FileName);
                    }
                    catch (NullReferenceException) { }
                }

                pb.Visible = false;
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                pb.Visible = false;
            }
        }

        // Import OFFICIALS to xml
        private void button16_Click(object sender, EventArgs e)
        {
            try
            {
                message.Text = "";
                pb.Visible = true;
                openXML.InitialDirectory = RollartSystemTech.Properties.Settings.Default.dbfolder;
                openXML.Title = "Import Officials to Rollart database";
                openXML.RestoreDirectory = true;
                openXML.FileName = "Rollart_Officials";

                if (openXML.ShowDialog() == DialogResult.OK)
                {
                    DataSet dataSet = new DataSet();
                    dataSet.ReadXml(openXML.FileName);

                    // Inserisco i giudici nel db
                    // se già esiste non inserisco
                    int countInserted = 0;
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "INSERT INTO Judges(Name, Region, Country)" +
                            " VALUES(@param1,@param2,@param3)";
                        //int b = 0;
                        for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                        {
                            command.Parameters.AddWithValue("@param1", dataSet.Tables[0].Rows[i][1].ToString().ToUpper()); // nome giudice
                            command.Parameters.AddWithValue("@param2", dataSet.Tables[0].Rows[i][2].ToString().ToUpper()); // region giudice
                            command.Parameters.AddWithValue("@param3", dataSet.Tables[0].Rows[i][3].ToString().ToUpper()); // nation giudice
                            try
                            {
                                command.ExecuteNonQuery();
                                countInserted++;
                            }
                            catch (Exception)
                            {
                                //MessageBox.Show("Judge " + Judges.judges[i].ToString() + " already inserted.");
                            }
                        }
                    }
                    if (countInserted == 0) MessageBox.Show("No new officials found.", "RollArt Tech Panel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        MessageBox.Show(countInserted + " officials imported with success.", "RollArt Tech Panel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        // Find
                        button11.PerformClick();
                    }

                }
                pb.Visible = false;
            }
            catch (Exception ex)
            {
                message.Text = "File error: " + ex.Message;
                pb.Visible = false;
            }
            
        }

        #endregion

        private void button5_Click_1(object sender, EventArgs e)
        {
            Dispose();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouse_offset = new Point(-e.X, -e.Y);
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouse_offset.X, mouse_offset.Y);
                this.Location = mousePos; //move the form to the desired location
            }
        }
        
        private void label42_Click(object sender, EventArgs e)
        {
            Definizioni.preview = true;
            if (RollartSystemTech.Properties.Settings.Default.SwitchDisplay)
            {
                Utility.CloneDisplays();
            }
            if (rb11.Checked)
            {
                ScoreToScreen sts = new ScoreToScreen();

                sts.ShowDialog();
            }
            if (rb12.Checked)
            {
                ScoreToScreen2 sts2 = new ScoreToScreen2();
                sts2.ShowDialog();
            }
            Definizioni.preview = false;
        }

        private void cb4_CheckedChanged(object sender, EventArgs e)
        {
            if (cb4.Checked)
                RollartSystemTech.Properties.Settings.Default.SwitchDisplay = true;
            else
                RollartSystemTech.Properties.Settings.Default.SwitchDisplay = false;
        }

        private void button15_Click(object sender, EventArgs e)
        {
            if (of2.ShowDialog() == DialogResult.OK)
            {
                pb1.BackgroundImage = Image.FromFile(of2.FileName);
                Definizioni.imageSponsor = pb1.BackgroundImage;
            }
        }

        private void rb11_CheckedChanged(object sender, EventArgs e)
        {
            if (rb11.Checked)
                Definizioni.screenType = "1";
        }

        private void cbPrecision_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idCategoria = cbPrecision.SelectedIndex + 1;
            GetParamsPrecision(idCategoria);
        }

        private void rb12_CheckedChanged(object sender, EventArgs e)
        {
            if (rb12.Checked)
                Definizioni.screenType = "2";
        }

        
    }


}


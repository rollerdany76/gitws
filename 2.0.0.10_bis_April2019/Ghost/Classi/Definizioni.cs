﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Xml;
using System.IO;
using System.Collections;
using System.Data.SQLite;
using System.Windows.Forms;
using System.Drawing;

namespace Ghost
{
    class Definizioni
    {
        // judge panel connection
        public static TcpClient[] clientsJudge = null;
        public static Stream stmJudge = null;
        public static Stream stm = null;
        public static byte[] ba = null;
        public static ASCIIEncoding asen = new ASCIIEncoding();
        public static Boolean[] connectedJudges = null;
        public static int pingTime = 1000; // in ms
        // Settings.xml
        public static XmlDocument xml = null;
        public static XmlNodeList nodiGiudici = null;
        public static int numJudges = 0;
        public static string[] judgesSettings = null;
        public static string rootFolder = "";
        public static bool staticScreen = false;
		public static string screenType = "1";
        public static bool preview = false;
        public static bool displaySwitch = true;
        // resources.txt
        public static Hashtable resources = null;
        // database
        public static SQLiteConnection conn = null;
        // gara
        public static int idSegment = 0, idGaraParams = 0, idSpecialita = 0, idCategoria = 0, idAtleta = 0;
        public static string dettaglioGara = "", currentPattern = "";
        public static string saltoPrec = "", valueqoe = "", valueTot = "", valuegoe = "";
        public static string queryval = "", query = "", querygoe = "", querystr = "", judgeQoeToView = "", nomeGara = "";
        public static int indexBeforeUpd = 0, numJ = 0, totJ = 0, numPartecipanti = 0, numComboElement = 0;
        public static int currentPart = 0, lastPart = 0, numPart = 0;
        public static decimal totale = 0, deductionsDec = 0, elementsDec = 0, artImpr = 0, techFinal = 0, factor = 0;
        public static decimal[] paramSegment = new decimal[30]; // parametri del segmento
        public static decimal[] elemSegment = new decimal[30]; // elementi inseriti
        public static bool downJump = false, underJump = false, halfJump = false, compAlreadyInserted = false, updating = false;
        public static string[] qoeRicevuti = null; //, compRicevuti = null;
        public static ArrayList nomi,societa,nazioni,giudici;
        public static string[] percentualiSpins = null;
        public static bool stopFromWaiting = false, exitFromWaiting = false, exitFromPing = false, dispose = false, started = false;
        public static Form currentForm = null;
        public static Image imageSponsor = null;
        public static bool confirm = false;
        public static string warningMessage = "";
        public static decimal[] arDeductions;
        public static bool skipped = false;

        // New Event
        public static int numOfJudges = 0;
        public static string completed = "";

        public Definizioni()
        {

        }


    }


}

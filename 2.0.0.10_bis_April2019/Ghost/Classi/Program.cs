﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using RollartSystemTech;
using System.Threading;

namespace Ghost
{
    static class Program
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        //[STAThread]
        //static void Main()
        //{
        //    Application.EnableVisualStyles();
        //    Application.SetCompatibleTextRenderingDefault(false);
        //    Application.Run(new EventsForm());
            
        //}
        static Mutex mutex = new Mutex(false,
            System.Diagnostics.Process.GetCurrentProcess().ProcessName);
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new EventsForm());

            if (RollartSystemTech.Properties.Settings.Default.SingleInstance)
            {
                if (!mutex.WaitOne(TimeSpan.FromSeconds(2), false))
                {
                    MessageBox.Show("Application RollartTechPanel is already running...", "", MessageBoxButtons.OK);
                    return;
                }
            }

            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new EventsForm());
            }
            finally { mutex.ReleaseMutex(); } // I find this more explicit

        }
    }
}

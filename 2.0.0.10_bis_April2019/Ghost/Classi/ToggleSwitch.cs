﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Ghost
{
    public partial class ToggleSwitch : UserControl
    {
        public Color ColorToggleOn { get; set; }
        public bool istoggleStatus { get; set; }

        public ToggleSwitch()
        {
            InitializeComponent();
        }

        private void ToggleSwitch_Load(object sender, EventArgs e)
        {
            if (istoggleStatus)
            {
                isON();
            }
            else
            {
                isOFF();
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            SetToggleControlStatus(istoggleStatus);
        }

        private void SetToggleControlStatus(bool isToggle)
        {
            if (isToggle)
            {
                isToggle = false;
                this.BackColor = Color.Silver;
                this.label1.Dock = DockStyle.Left;
                this.label2.TextAlign = ContentAlignment.MiddleRight;
                this.label2.Text = "OFF";
            }
            else
            {
                isToggle = true;
                this.BackColor = this.ColorToggleOn;
                this.label1.Dock = DockStyle.Right;
                this.label2.TextAlign = ContentAlignment.MiddleLeft;
                this.label2.Text = "ON";
            }
            istoggleStatus = isToggle;
        }

        private void isOFF()
        {
            this.BackColor = Color.Silver;
            this.label1.Dock = DockStyle.Left;
            this.label2.TextAlign = ContentAlignment.MiddleRight;
            this.label2.Text = "OFF";
        }

        private void isON()
        {
            this.BackColor = ColorToggleOn;
            this.label1.Dock = DockStyle.Right;
            this.label2.TextAlign = ContentAlignment.MiddleLeft;
            this.label2.Text = "ON";
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (istoggleStatus)
            {
                isON();
            }
            else
            {
                isOFF();
            }
            base.OnPaint(e);
        }

        private void label2_MouseDown(object sender, MouseEventArgs e)
        {
            SetToggleControlStatus(istoggleStatus);
        }


    }

}

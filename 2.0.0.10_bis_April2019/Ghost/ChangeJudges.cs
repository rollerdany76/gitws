﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SQLite;

namespace Ghost
{
    public partial class ChangeJudges : Form
    {
        public static ArrayList judges = null;
        Utility ut = null;
        
        public ChangeJudges()
        {
            InitializeComponent();
        }

        private void Judges_Load(object sender, EventArgs e)
        {
            try
            {
                // carico i giudici
                SQLiteDataReader dr = null;
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    command.CommandText = "SELECT Role, P.ID_Judge, Name, Country FROM PanelJudge as P, Judges as J" +
                        " WHERE P.ID_Judge = J.ID_Judge AND ID_GaraParams = " + Definizioni.idGaraParams;
                    dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        string role = dr[0].ToString();
                        string name = dr[2].ToString();
                        string id = dr[1].ToString();

                        if (role.Equals("Specialist")) { tp.Text = name; tp.Tag = id; }
                        else if (role.Equals("Data Operator")) { dataop.Text = name; dataop.Tag = id; }
                        else if (role.Equals("Assistant")) { ass.Text = name; ass.Tag = id; }
                        else if (role.Equals("Controller")) { contr.Text = name; contr.Tag = id; }
                        else if (role.Equals("Referee")) { referee.Text = name; referee.Tag = id; }
                        // giudici
                        else if (role.Equals("Judge 1")) { j1.Text = name; j1.Tag = id; }
                        else if (role.Equals("Judge 2")) { j2.Text = name; j2.Tag = id; }
                        else if (role.Equals("Judge 3")) { j3.Text = name; j3.Tag = id; }
                        else if (role.Equals("Judge 4")) { j4.Text = name; j4.Tag = id; }
                        else if (role.Equals("Judge 5")) { j5.Text = name; j5.Tag = id; }
                        else if (role.Equals("Judge 6")) { j6.Text = name; j6.Tag = id; }
                        else if (role.Equals("Judge 7")) { j7.Text = name; j7.Tag = id; }
                        else if (role.Equals("Judge 8")) { j8.Text = name; j8.Tag = id; }
                        else if (role.Equals("Judge 9")) { j9.Text = name; j9.Tag = id; }
                    }
                    dr.Close();
                }

                // recupero i giudici per l'auto-completion dei nomi
                ut = new Utility(null);
                ut.GetGiudici();
                if (Definizioni.giudici != null)
                {
                    var source = new AutoCompleteStringCollection();
                    string[] names = (string[])Definizioni.giudici.ToArray(typeof(string));
                    source.AddRange(names);
                    tp.AutoCompleteCustomSource = source;
                    dataop.AutoCompleteCustomSource = source;
                    ass.AutoCompleteCustomSource = source;
                    contr.AutoCompleteCustomSource = source;
                    referee.AutoCompleteCustomSource = source;
                    j1.AutoCompleteCustomSource = source;
                    j2.AutoCompleteCustomSource = source;
                    j3.AutoCompleteCustomSource = source;
                    j4.AutoCompleteCustomSource = source;
                    j5.AutoCompleteCustomSource = source;
                    j6.AutoCompleteCustomSource = source;
                    j7.AutoCompleteCustomSource = source;
                    j8.AutoCompleteCustomSource = source;
                    j9.AutoCompleteCustomSource = source;
                }
            } 
            catch (Exception ex)
            {
                Utility.WriteLog(ex.InnerException.Message, "ERROR");
            }
        }

        private void confirmNewEvent_Click(object sender, EventArgs e)
        {
            try
            {
                if (tp.TextLength == 0)
                {
                    label1.ForeColor = Color.Red;
                    tp.Focus();
                    return;
                }
                if (dataop.TextLength == 0)
                {
                    label2.ForeColor = Color.Red;
                    dataop.Focus();
                    return;
                }
                for (int i = 0; i < Definizioni.numOfJudges; i++)
                {
                    if (((TextBox)this.Controls.Find("j" + (i + 1), true)[0]).TextLength == 0)
                    {
                        ((Label)this.Controls.Find("ju" + (i + 1), true)[0]).ForeColor = Color.Red;
                        ((TextBox)this.Controls.Find("j" + (i + 1), true)[0]).Focus();
                        return;
                    }
                }
                judges = new ArrayList();
                judges.Add(tp.Text);
                judges.Add(dataop.Text);
                judges.Add(ass.Text);
                judges.Add(contr.Text);
                judges.Add(referee.Text);
                for (int i = 0; i < Definizioni.numOfJudges; i++)
                {
                    judges.Add(((TextBox)this.Controls.Find("j" + (i + 1), true)[0]).Text);
                }

                //Dispose();
                Close();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex.InnerException.Message, "ERROR");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 9; i++)
            {
                ((TextBox)this.Controls.Find("j" + (i + 1), true)[0]).Text = "";
            }
            tp.Text = "";
            dataop.Text = "";
            ass.Text = "";
            referee.Text = "";
            contr.Text = "";
            tp.Focus();
        }

        private void tp_TextChanged(object sender, EventArgs e)
        {
            label1.ForeColor = Color.Black;
        }

        private void dataop_TextChanged(object sender, EventArgs e)
        {
            label2.ForeColor = Color.Black;
        }

        private void j1_TextChanged(object sender, EventArgs e)
        {
            string name = ((TextBox)sender).Name;
            ((Label)this.Controls.Find(name.Insert(1,"u"), true)[0]).ForeColor = Color.Black;
        }
    }
}

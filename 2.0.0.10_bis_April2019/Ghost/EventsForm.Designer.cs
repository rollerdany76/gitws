﻿namespace Ghost
{
    partial class EventsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EventsForm));
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Events", 46, 46);
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.titolo = new System.Windows.Forms.Button();
            this.imageList3 = new System.Windows.Forms.ImageList(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.menu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buTech = new System.Windows.Forms.Button();
            this.panStartEvent = new System.Windows.Forms.Panel();
            this.reload = new System.Windows.Forms.Button();
            this.deletegare = new System.Windows.Forms.Button();
            this.gara = new System.Windows.Forms.Panel();
            this.segmentName = new System.Windows.Forms.Label();
            this.lv3 = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.country = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.judgeName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.id_judge = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.upJudge = new System.Windows.Forms.Button();
            this.downJudge = new System.Windows.Forms.Button();
            this.changeName = new System.Windows.Forms.Button();
            this.newJudge = new System.Windows.Forms.Button();
            this.removeJudge = new System.Windows.Forms.Button();
            this.check3 = new System.Windows.Forms.Button();
            this.check1 = new System.Windows.Forms.Button();
            this.cbCD1 = new System.Windows.Forms.ComboBox();
            this.l9 = new System.Windows.Forms.Label();
            this.save = new System.Windows.Forms.Button();
            this.l4 = new System.Windows.Forms.TextBox();
            this.l1 = new System.Windows.Forms.TextBox();
            this.cbPattern = new System.Windows.Forms.ComboBox();
            this.labPattern = new System.Windows.Forms.Label();
            this.panStyle = new System.Windows.Forms.Panel();
            this.check2 = new System.Windows.Forms.Button();
            this.cbCD2 = new System.Windows.Forms.ComboBox();
            this.l10 = new System.Windows.Forms.Label();
            this.b3 = new System.Windows.Forms.Button();
            this.b4 = new System.Windows.Forms.Button();
            this.b8 = new System.Windows.Forms.Button();
            this.o2 = new System.Windows.Forms.Button();
            this.l20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.o3 = new System.Windows.Forms.Button();
            this.b9 = new System.Windows.Forms.Button();
            this.b7 = new System.Windows.Forms.Button();
            this.b6 = new System.Windows.Forms.Button();
            this.b5 = new System.Windows.Forms.Button();
            this.b2 = new System.Windows.Forms.Button();
            this.b1 = new System.Windows.Forms.Button();
            this.l11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.l8 = new System.Windows.Forms.Label();
            this.l7 = new System.Windows.Forms.Label();
            this.o1 = new System.Windows.Forms.Button();
            this.l6 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.judges = new System.Windows.Forms.Button();
            this.l5 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.l3 = new System.Windows.Forms.Label();
            this.l2 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.connect = new System.Windows.Forms.Button();
            this.reset = new System.Windows.Forms.Button();
            this.restore = new System.Windows.Forms.Button();
            this.backup = new System.Windows.Forms.Button();
            this.pb = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tv = new System.Windows.Forms.TreeView();
            this.resultsPanel = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.result = new System.Windows.Forms.Button();
            this.classifica = new System.Windows.Forms.Button();
            this.panel = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.cr = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.deleteall = new System.Windows.Forms.Button();
            this.panNewEvent = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.id = new System.Windows.Forms.Label();
            this.region = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.hidden = new System.Windows.Forms.Label();
            this.updateskater = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.club = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.naz = new System.Windows.Forms.TextBox();
            this.add = new System.Windows.Forms.Button();
            this.lv2 = new System.Windows.Forms.ListView();
            this.ch1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ch2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label8 = new System.Windows.Forms.Label();
            this.namep = new System.Windows.Forms.TextBox();
            this.delete = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pb1 = new System.Windows.Forms.ProgressBar();
            this.nJudges = new System.Windows.Forms.NumericUpDown();
            this.confirmNewEvent = new System.Windows.Forms.Button();
            this.lv1 = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label7 = new System.Windows.Forms.Label();
            this.browse = new System.Windows.Forms.Button();
            this.cb3 = new System.Windows.Forms.CheckBox();
            this.cb2 = new System.Windows.Forms.CheckBox();
            this.cb1 = new System.Windows.Forms.CheckBox();
            this.cbCategory = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.cbEvent = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.place = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.dateFrom = new System.Windows.Forms.DateTimePicker();
            this.dateTo = new System.Windows.Forms.DateTimePicker();
            this.panLog = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            this.log = new System.Windows.Forms.TextBox();
            this.of1 = new System.Windows.Forms.OpenFileDialog();
            this.fd1 = new System.Windows.Forms.FolderBrowserDialog();
            this.message = new System.Windows.Forms.Label();
            this.message2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.sf1 = new System.Windows.Forms.SaveFileDialog();
            this.menu2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importEvent = new System.Windows.Forms.ToolStripMenuItem();
            this.FinalReport1 = new RollartSystemTech.Report.FinalReport();
            this.PanelReport1 = new RollartSystemTech.Report.PanelReport();
            this.ResultReport1 = new RollartSystemTech.Report.ResultReport();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panStartEvent.SuspendLayout();
            this.gara.SuspendLayout();
            this.panStyle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb)).BeginInit();
            this.resultsPanel.SuspendLayout();
            this.panNewEvent.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nJudges)).BeginInit();
            this.panLog.SuspendLayout();
            this.panel2.SuspendLayout();
            this.menu2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(95)))), ((int)(((byte)(89)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.titolo);
            this.panel1.Cursor = System.Windows.Forms.Cursors.NoMove2D;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1207, 49);
            this.panel1.TabIndex = 0;
            this.panel1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDoubleClick);
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(86)))), ((int)(((byte)(82)))));
            this.pictureBox1.BackgroundImage = global::RollartSystemTech.Properties.Resources.icon_artistic;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox1.Location = new System.Drawing.Point(1149, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(58, 49);
            this.pictureBox1.TabIndex = 97;
            this.pictureBox1.TabStop = false;
            // 
            // titolo
            // 
            this.titolo.AutoSize = true;
            this.titolo.BackColor = System.Drawing.Color.Transparent;
            this.titolo.Cursor = System.Windows.Forms.Cursors.Default;
            this.titolo.FlatAppearance.BorderColor = System.Drawing.Color.SlateGray;
            this.titolo.FlatAppearance.BorderSize = 0;
            this.titolo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.titolo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.titolo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.titolo.Font = new System.Drawing.Font("Trebuchet MS", 20F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titolo.ForeColor = System.Drawing.Color.White;
            this.titolo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.titolo.Location = new System.Drawing.Point(3, 3);
            this.titolo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.titolo.Name = "titolo";
            this.titolo.Size = new System.Drawing.Size(558, 53);
            this.titolo.TabIndex = 96;
            this.titolo.Text = "RollArt - Technical panel";
            this.titolo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.titolo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.titolo.UseVisualStyleBackColor = false;
            // 
            // imageList3
            // 
            this.imageList3.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList3.ImageStream")));
            this.imageList3.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList3.Images.SetKeyName(0, "export.png");
            this.imageList3.Images.SetKeyName(1, "file_format_doc.png");
            this.imageList3.Images.SetKeyName(2, "file_format_pdf.png");
            this.imageList3.Images.SetKeyName(3, "acrobat.png");
            this.imageList3.Images.SetKeyName(4, "add_32.png");
            this.imageList3.Images.SetKeyName(5, "arrow_left_32.png");
            this.imageList3.Images.SetKeyName(6, "arrow_right_32.png");
            this.imageList3.Images.SetKeyName(7, "balance_32.png");
            this.imageList3.Images.SetKeyName(8, "checkmark_32.png");
            this.imageList3.Images.SetKeyName(9, "cross_32.png");
            this.imageList3.Images.SetKeyName(10, "diskette_32.png");
            this.imageList3.Images.SetKeyName(11, "gear_32.png");
            this.imageList3.Images.SetKeyName(12, "help_32.png");
            this.imageList3.Images.SetKeyName(13, "home_32.png");
            this.imageList3.Images.SetKeyName(14, "info_32.png");
            this.imageList3.Images.SetKeyName(15, "plan_1_a_32.png");
            this.imageList3.Images.SetKeyName(16, "plan_1_b_32.png");
            this.imageList3.Images.SetKeyName(17, "plan_1_c_32.png");
            this.imageList3.Images.SetKeyName(18, "plan_1_d_32.png");
            this.imageList3.Images.SetKeyName(19, "plan_1_e_32.png");
            this.imageList3.Images.SetKeyName(20, "star_32.png");
            this.imageList3.Images.SetKeyName(21, "trophy_32.png");
            this.imageList3.Images.SetKeyName(22, "warehouse_32.png");
            this.imageList3.Images.SetKeyName(23, "world_32.png");
            this.imageList3.Images.SetKeyName(24, "zoom.png");
            this.imageList3.Images.SetKeyName(25, "da_blu.png");
            this.imageList3.Images.SetKeyName(26, "da_green.png");
            this.imageList3.Images.SetKeyName(27, "da_or.png");
            this.imageList3.Images.SetKeyName(28, "free_blu.png");
            this.imageList3.Images.SetKeyName(29, "free_green.png");
            this.imageList3.Images.SetKeyName(30, "free_or.png");
            this.imageList3.Images.SetKeyName(31, "pa_blu.png");
            this.imageList3.Images.SetKeyName(32, "pa_green.png");
            this.imageList3.Images.SetKeyName(33, "pa_or.png");
            this.imageList3.Images.SetKeyName(34, "sd_blu.png");
            this.imageList3.Images.SetKeyName(35, "sd_green.png");
            this.imageList3.Images.SetKeyName(36, "sd_or.png");
            this.imageList3.Images.SetKeyName(37, "sy_blu.png");
            this.imageList3.Images.SetKeyName(38, "sy_green.png");
            this.imageList3.Images.SetKeyName(39, "sy_or.png");
            this.imageList3.Images.SetKeyName(40, "tick.png");
            this.imageList3.Images.SetKeyName(41, "cup.png");
            this.imageList3.Images.SetKeyName(42, "data.png");
            this.imageList3.Images.SetKeyName(43, "dataok.png");
            this.imageList3.Images.SetKeyName(44, "Plus.png");
            this.imageList3.Images.SetKeyName(45, "refresh.png");
            this.imageList3.Images.SetKeyName(46, "world.png");
            this.imageList3.Images.SetKeyName(47, "star.png");
            this.imageList3.Images.SetKeyName(48, "folder.png");
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "ceo.png");
            this.imageList1.Images.SetKeyName(1, "male-user.png");
            this.imageList1.Images.SetKeyName(2, "male-user-green.png");
            this.imageList1.Images.SetKeyName(3, "male-user-red.png");
            this.imageList1.Images.SetKeyName(4, "male-user-yellow.png");
            this.imageList1.Images.SetKeyName(5, "reload.png");
            this.imageList1.Images.SetKeyName(6, "contact_card_icon&16.png");
            this.imageList1.Images.SetKeyName(7, "refresh_icon&16.png");
            this.imageList1.Images.SetKeyName(8, "round_minus_icon&16.png");
            this.imageList1.Images.SetKeyName(9, "round_plus_icon&16.png");
            this.imageList1.Images.SetKeyName(10, "arrow_bottom_icon&16.png");
            this.imageList1.Images.SetKeyName(11, "arrow_top_icon&16.png");
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 49);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel1.ContextMenuStrip = this.menu;
            this.splitContainer1.Panel1.Controls.Add(this.panel3);
            this.splitContainer1.Panel1.Controls.Add(this.button4);
            this.splitContainer1.Panel1.Controls.Add(this.button5);
            this.splitContainer1.Panel1.Controls.Add(this.button2);
            this.splitContainer1.Panel1.Controls.Add(this.button1);
            this.splitContainer1.Panel1.Controls.Add(this.buTech);
            this.splitContainer1.Panel1MinSize = 45;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.splitContainer1.Panel2.Controls.Add(this.panStartEvent);
            this.splitContainer1.Panel2.Controls.Add(this.panNewEvent);
            this.splitContainer1.Panel2.Controls.Add(this.panLog);
            this.splitContainer1.Size = new System.Drawing.Size(1207, 684);
            this.splitContainer1.SplitterDistance = 170;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 2;
            // 
            // menu
            // 
            this.menu.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(61, 4);
            this.menu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menu_ItemClicked);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel3.Controls.Add(this.pictureBox2);
            this.panel3.Location = new System.Drawing.Point(0, 226);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(168, 374);
            this.panel3.TabIndex = 108;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox2.BackgroundImage = global::RollartSystemTech.Properties.Resources.WS_Logo_Positive;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(35, 215);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 124);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Transparent;
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.Dock = System.Windows.Forms.DockStyle.Top;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(236)))), ((int)(((byte)(179)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Image = global::RollartSystemTech.Properties.Resources.logout;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(0, 181);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(168, 45);
            this.button4.TabIndex = 107;
            this.button4.Text = "Exit";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button5_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Transparent;
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.Dock = System.Windows.Forms.DockStyle.Top;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(236)))), ((int)(((byte)(179)))));
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.Black;
            this.button5.Image = global::RollartSystemTech.Properties.Resources.info;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(0, 136);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(168, 45);
            this.button5.TabIndex = 105;
            this.button5.Text = "Log";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.log_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(236)))), ((int)(((byte)(179)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Image = global::RollartSystemTech.Properties.Resources.settings;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(0, 91);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(168, 45);
            this.button2.TabIndex = 104;
            this.button2.Text = "Settings";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(236)))), ((int)(((byte)(179)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Image = global::RollartSystemTech.Properties.Resources.cup;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(0, 46);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(168, 45);
            this.button1.TabIndex = 96;
            this.button1.Text = "Event List";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buTech
            // 
            this.buTech.BackColor = System.Drawing.Color.Transparent;
            this.buTech.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buTech.Dock = System.Windows.Forms.DockStyle.Top;
            this.buTech.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.buTech.FlatAppearance.BorderSize = 0;
            this.buTech.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.buTech.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(236)))), ((int)(((byte)(179)))));
            this.buTech.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buTech.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buTech.ForeColor = System.Drawing.Color.Black;
            this.buTech.Image = ((System.Drawing.Image)(resources.GetObject("buTech.Image")));
            this.buTech.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buTech.Location = new System.Drawing.Point(0, 0);
            this.buTech.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buTech.Name = "buTech";
            this.buTech.Size = new System.Drawing.Size(168, 46);
            this.buTech.TabIndex = 95;
            this.buTech.Text = "New Event";
            this.buTech.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buTech.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buTech.UseVisualStyleBackColor = false;
            this.buTech.Click += new System.EventHandler(this.buTech_Click);
            // 
            // panStartEvent
            // 
            this.panStartEvent.AutoScroll = true;
            this.panStartEvent.BackColor = System.Drawing.Color.White;
            this.panStartEvent.Controls.Add(this.reload);
            this.panStartEvent.Controls.Add(this.deletegare);
            this.panStartEvent.Controls.Add(this.gara);
            this.panStartEvent.Controls.Add(this.connect);
            this.panStartEvent.Controls.Add(this.reset);
            this.panStartEvent.Controls.Add(this.restore);
            this.panStartEvent.Controls.Add(this.backup);
            this.panStartEvent.Controls.Add(this.pb);
            this.panStartEvent.Controls.Add(this.label5);
            this.panStartEvent.Controls.Add(this.tv);
            this.panStartEvent.Controls.Add(this.resultsPanel);
            this.panStartEvent.Controls.Add(this.deleteall);
            this.panStartEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panStartEvent.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panStartEvent.Location = new System.Drawing.Point(0, 0);
            this.panStartEvent.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panStartEvent.Name = "panStartEvent";
            this.panStartEvent.Size = new System.Drawing.Size(1034, 682);
            this.panStartEvent.TabIndex = 1;
            this.panStartEvent.Visible = false;
            // 
            // reload
            // 
            this.reload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.reload.BackColor = System.Drawing.Color.White;
            this.reload.Cursor = System.Windows.Forms.Cursors.Hand;
            this.reload.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.reload.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.reload.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.reload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.reload.Font = new System.Drawing.Font("Trebuchet MS", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reload.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.reload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.reload.Location = new System.Drawing.Point(10, 553);
            this.reload.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.reload.Name = "reload";
            this.reload.Size = new System.Drawing.Size(95, 47);
            this.reload.TabIndex = 136;
            this.reload.Text = "Reload";
            this.reload.UseVisualStyleBackColor = false;
            this.reload.Click += new System.EventHandler(this.reload_Click);
            // 
            // deletegare
            // 
            this.deletegare.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deletegare.BackColor = System.Drawing.Color.White;
            this.deletegare.Cursor = System.Windows.Forms.Cursors.Hand;
            this.deletegare.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.deletegare.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.deletegare.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.deletegare.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deletegare.Font = new System.Drawing.Font("Trebuchet MS", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deletegare.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.deletegare.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.deletegare.Location = new System.Drawing.Point(111, 553);
            this.deletegare.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.deletegare.Name = "deletegare";
            this.deletegare.Size = new System.Drawing.Size(78, 47);
            this.deletegare.TabIndex = 138;
            this.deletegare.Text = "Delete";
            this.deletegare.UseVisualStyleBackColor = false;
            this.deletegare.Click += new System.EventHandler(this.deletegare_Click);
            // 
            // gara
            // 
            this.gara.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gara.BackgroundImage = global::RollartSystemTech.Properties.Resources.backgara;
            this.gara.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.gara.Controls.Add(this.segmentName);
            this.gara.Controls.Add(this.lv3);
            this.gara.Controls.Add(this.upJudge);
            this.gara.Controls.Add(this.downJudge);
            this.gara.Controls.Add(this.changeName);
            this.gara.Controls.Add(this.newJudge);
            this.gara.Controls.Add(this.removeJudge);
            this.gara.Controls.Add(this.check3);
            this.gara.Controls.Add(this.check1);
            this.gara.Controls.Add(this.cbCD1);
            this.gara.Controls.Add(this.l9);
            this.gara.Controls.Add(this.save);
            this.gara.Controls.Add(this.l4);
            this.gara.Controls.Add(this.l1);
            this.gara.Controls.Add(this.cbPattern);
            this.gara.Controls.Add(this.labPattern);
            this.gara.Controls.Add(this.panStyle);
            this.gara.Controls.Add(this.l20);
            this.gara.Controls.Add(this.label22);
            this.gara.Controls.Add(this.o3);
            this.gara.Controls.Add(this.b9);
            this.gara.Controls.Add(this.b7);
            this.gara.Controls.Add(this.b6);
            this.gara.Controls.Add(this.b5);
            this.gara.Controls.Add(this.b2);
            this.gara.Controls.Add(this.b1);
            this.gara.Controls.Add(this.l11);
            this.gara.Controls.Add(this.label14);
            this.gara.Controls.Add(this.label20);
            this.gara.Controls.Add(this.l8);
            this.gara.Controls.Add(this.l7);
            this.gara.Controls.Add(this.o1);
            this.gara.Controls.Add(this.l6);
            this.gara.Controls.Add(this.label19);
            this.gara.Controls.Add(this.judges);
            this.gara.Controls.Add(this.l5);
            this.gara.Controls.Add(this.label18);
            this.gara.Controls.Add(this.label17);
            this.gara.Controls.Add(this.l3);
            this.gara.Controls.Add(this.l2);
            this.gara.Controls.Add(this.label13);
            this.gara.Controls.Add(this.label11);
            this.gara.Location = new System.Drawing.Point(463, 7);
            this.gara.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gara.Name = "gara";
            this.gara.Size = new System.Drawing.Size(561, 542);
            this.gara.TabIndex = 137;
            this.gara.Visible = false;
            // 
            // segmentName
            // 
            this.segmentName.BackColor = System.Drawing.Color.Transparent;
            this.segmentName.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.segmentName.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.segmentName.Location = new System.Drawing.Point(12, 514);
            this.segmentName.Name = "segmentName";
            this.segmentName.Size = new System.Drawing.Size(138, 24);
            this.segmentName.TabIndex = 178;
            this.segmentName.Tag = "";
            this.segmentName.Text = "Short Program";
            this.segmentName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.segmentName.Visible = false;
            // 
            // lv3
            // 
            this.lv3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lv3.AutoArrange = false;
            this.lv3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.lv3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lv3.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.country,
            this.judgeName,
            this.columnHeader9,
            this.id_judge});
            this.lv3.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv3.FullRowSelect = true;
            this.lv3.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lv3.HideSelection = false;
            this.lv3.LabelWrap = false;
            this.lv3.Location = new System.Drawing.Point(25, 150);
            this.lv3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lv3.MultiSelect = false;
            this.lv3.Name = "lv3";
            this.lv3.ShowGroups = false;
            this.lv3.ShowItemToolTips = true;
            this.lv3.Size = new System.Drawing.Size(474, 208);
            this.lv3.SmallImageList = this.imageList1;
            this.lv3.TabIndex = 143;
            this.lv3.UseCompatibleStateImageBehavior = false;
            this.lv3.View = System.Windows.Forms.View.Details;
            this.lv3.Visible = false;
            this.lv3.AfterLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.lv3_AfterLabelEdit);
            this.lv3.SelectedIndexChanged += new System.EventHandler(this.lv3_SelectedIndexChanged);
            this.lv3.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lv3_MouseDoubleClick);
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "";
            this.columnHeader7.Width = 200;
            // 
            // country
            // 
            this.country.Text = "";
            this.country.Width = 35;
            // 
            // judgeName
            // 
            this.judgeName.Text = "";
            this.judgeName.Width = 100;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Width = 120;
            // 
            // id_judge
            // 
            this.id_judge.Width = 0;
            // 
            // upJudge
            // 
            this.upJudge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.upJudge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.upJudge.Cursor = System.Windows.Forms.Cursors.Hand;
            this.upJudge.Enabled = false;
            this.upJudge.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.upJudge.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.upJudge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.upJudge.Font = new System.Drawing.Font("Trebuchet MS", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upJudge.ForeColor = System.Drawing.Color.Black;
            this.upJudge.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.upJudge.ImageIndex = 11;
            this.upJudge.ImageList = this.imageList1;
            this.upJudge.Location = new System.Drawing.Point(495, 293);
            this.upJudge.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.upJudge.Name = "upJudge";
            this.upJudge.Size = new System.Drawing.Size(46, 27);
            this.upJudge.TabIndex = 176;
            this.upJudge.Tag = "1";
            this.upJudge.Text = "Up";
            this.upJudge.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.upJudge.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.upJudge.UseVisualStyleBackColor = false;
            this.upJudge.Visible = false;
            this.upJudge.Click += new System.EventHandler(this.upJudge_Click);
            // 
            // downJudge
            // 
            this.downJudge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.downJudge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.downJudge.Cursor = System.Windows.Forms.Cursors.Hand;
            this.downJudge.Enabled = false;
            this.downJudge.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.downJudge.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.downJudge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.downJudge.Font = new System.Drawing.Font("Trebuchet MS", 5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.downJudge.ForeColor = System.Drawing.Color.Black;
            this.downJudge.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.downJudge.ImageIndex = 10;
            this.downJudge.ImageList = this.imageList1;
            this.downJudge.Location = new System.Drawing.Point(494, 318);
            this.downJudge.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.downJudge.Name = "downJudge";
            this.downJudge.Size = new System.Drawing.Size(47, 27);
            this.downJudge.TabIndex = 177;
            this.downJudge.Tag = "1";
            this.downJudge.Text = "Down";
            this.downJudge.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.downJudge.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.downJudge.UseVisualStyleBackColor = false;
            this.downJudge.Visible = false;
            this.downJudge.Click += new System.EventHandler(this.downJudge_Click);
            // 
            // changeName
            // 
            this.changeName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.changeName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.changeName.Cursor = System.Windows.Forms.Cursors.Hand;
            this.changeName.Enabled = false;
            this.changeName.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.changeName.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.changeName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.changeName.Font = new System.Drawing.Font("Trebuchet MS", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeName.ForeColor = System.Drawing.Color.Black;
            this.changeName.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.changeName.ImageIndex = 6;
            this.changeName.ImageList = this.imageList1;
            this.changeName.Location = new System.Drawing.Point(495, 164);
            this.changeName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.changeName.Name = "changeName";
            this.changeName.Size = new System.Drawing.Size(46, 37);
            this.changeName.TabIndex = 173;
            this.changeName.Tag = "1";
            this.changeName.Text = "Update";
            this.changeName.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.changeName.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.changeName.UseVisualStyleBackColor = false;
            this.changeName.Click += new System.EventHandler(this.button8_Click);
            // 
            // newJudge
            // 
            this.newJudge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.newJudge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.newJudge.Cursor = System.Windows.Forms.Cursors.Hand;
            this.newJudge.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.newJudge.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.newJudge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.newJudge.Font = new System.Drawing.Font("Trebuchet MS", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newJudge.ForeColor = System.Drawing.Color.Black;
            this.newJudge.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.newJudge.ImageIndex = 9;
            this.newJudge.ImageList = this.imageList1;
            this.newJudge.Location = new System.Drawing.Point(495, 200);
            this.newJudge.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.newJudge.Name = "newJudge";
            this.newJudge.Size = new System.Drawing.Size(46, 37);
            this.newJudge.TabIndex = 174;
            this.newJudge.Tag = "1";
            this.newJudge.Text = "Add";
            this.newJudge.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.newJudge.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.newJudge.UseVisualStyleBackColor = false;
            this.newJudge.Visible = false;
            this.newJudge.Click += new System.EventHandler(this.button9_Click);
            // 
            // removeJudge
            // 
            this.removeJudge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.removeJudge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.removeJudge.Cursor = System.Windows.Forms.Cursors.Hand;
            this.removeJudge.Enabled = false;
            this.removeJudge.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.removeJudge.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.removeJudge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.removeJudge.Font = new System.Drawing.Font("Trebuchet MS", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeJudge.ForeColor = System.Drawing.Color.Black;
            this.removeJudge.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.removeJudge.ImageIndex = 8;
            this.removeJudge.ImageList = this.imageList1;
            this.removeJudge.Location = new System.Drawing.Point(495, 236);
            this.removeJudge.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.removeJudge.Name = "removeJudge";
            this.removeJudge.Size = new System.Drawing.Size(46, 37);
            this.removeJudge.TabIndex = 175;
            this.removeJudge.Tag = "1";
            this.removeJudge.Text = "Delete";
            this.removeJudge.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.removeJudge.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.removeJudge.UseVisualStyleBackColor = false;
            this.removeJudge.Visible = false;
            this.removeJudge.Click += new System.EventHandler(this.removeJudge_Click);
            // 
            // check3
            // 
            this.check3.BackColor = System.Drawing.Color.White;
            this.check3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.check3.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan;
            this.check3.FlatAppearance.BorderSize = 0;
            this.check3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.check3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.check3.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check3.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.check3.Image = ((System.Drawing.Image)(resources.GetObject("check3.Image")));
            this.check3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.check3.Location = new System.Drawing.Point(519, 477);
            this.check3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.check3.Name = "check3";
            this.check3.Size = new System.Drawing.Size(35, 30);
            this.check3.TabIndex = 172;
            this.check3.Tag = "1";
            this.check3.UseVisualStyleBackColor = false;
            this.check3.Visible = false;
            this.check3.Click += new System.EventHandler(this.check3_Click);
            // 
            // check1
            // 
            this.check1.BackColor = System.Drawing.Color.White;
            this.check1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.check1.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan;
            this.check1.FlatAppearance.BorderSize = 0;
            this.check1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.check1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.check1.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check1.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.check1.Image = ((System.Drawing.Image)(resources.GetObject("check1.Image")));
            this.check1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.check1.Location = new System.Drawing.Point(519, 382);
            this.check1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.check1.Name = "check1";
            this.check1.Size = new System.Drawing.Size(35, 30);
            this.check1.TabIndex = 171;
            this.check1.Tag = "1";
            this.check1.UseVisualStyleBackColor = false;
            this.check1.Visible = false;
            this.check1.Click += new System.EventHandler(this.check1_Click);
            // 
            // cbCD1
            // 
            this.cbCD1.BackColor = System.Drawing.Color.AntiqueWhite;
            this.cbCD1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCD1.DropDownWidth = 170;
            this.cbCD1.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCD1.FormattingEnabled = true;
            this.cbCD1.Items.AddRange(new object[] {
            "<COMPULSORY 1>",
            "Argentine Tango",
            "Argentine Tango (Solo)",
            "Association Waltz",
            "Blues",
            "Canasta Tango",
            "Carlos Tango",
            "Castel March",
            "City Blues",
            "Denver Shuffle",
            "Easy Paso",
            "Fourteen Step",
            "Harris Tango",
            "Kilian",
            "Italian Foxtrot Solo",
            "La Vista Cha Cha",
            "Midnight Blues",
            "Paso Doble",
            "Quick Step",
            "Rocker Foxtrot",
            "Shaken Samba",
            "Starlight Waltz",
            "Tango Delancha",
            "Tango Delanco",
            "Terenzi",
            "Tudor Waltz",
            "Viennese Waltz",
            "Westminster Waltz"});
            this.cbCD1.Location = new System.Drawing.Point(25, 386);
            this.cbCD1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbCD1.Name = "cbCD1";
            this.cbCD1.Size = new System.Drawing.Size(150, 26);
            this.cbCD1.TabIndex = 170;
            // 
            // l9
            // 
            this.l9.BackColor = System.Drawing.Color.Transparent;
            this.l9.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l9.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.l9.Location = new System.Drawing.Point(12, 386);
            this.l9.Name = "l9";
            this.l9.Size = new System.Drawing.Size(174, 24);
            this.l9.TabIndex = 149;
            this.l9.Tag = "";
            this.l9.Text = "Compulsory Dance";
            this.l9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.l9.Visible = false;
            // 
            // save
            // 
            this.save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.save.BackColor = System.Drawing.Color.White;
            this.save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.save.Enabled = false;
            this.save.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan;
            this.save.FlatAppearance.BorderSize = 0;
            this.save.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.save.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.save.ForeColor = System.Drawing.Color.DarkCyan;
            this.save.Image = global::RollartSystemTech.Properties.Resources.save_icon_24;
            this.save.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.save.Location = new System.Drawing.Point(524, 47);
            this.save.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(32, 27);
            this.save.TabIndex = 147;
            this.save.Tag = "1";
            this.save.UseVisualStyleBackColor = false;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // l4
            // 
            this.l4.BackColor = System.Drawing.Color.SeaShell;
            this.l4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.l4.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l4.Location = new System.Drawing.Point(185, 70);
            this.l4.MaxLength = 30;
            this.l4.Name = "l4";
            this.l4.Size = new System.Drawing.Size(312, 19);
            this.l4.TabIndex = 169;
            this.l4.TextChanged += new System.EventHandler(this.l4_TextChanged);
            // 
            // l1
            // 
            this.l1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.l1.BackColor = System.Drawing.Color.DarkOrange;
            this.l1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.l1.Font = new System.Drawing.Font("Trebuchet MS", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l1.ForeColor = System.Drawing.Color.White;
            this.l1.Location = new System.Drawing.Point(3, 20);
            this.l1.MaxLength = 55;
            this.l1.Name = "l1";
            this.l1.Size = new System.Drawing.Size(556, 24);
            this.l1.TabIndex = 168;
            this.l1.Text = "sdfsdf";
            this.l1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.l1.TextChanged += new System.EventHandler(this.l1_TextChanged);
            // 
            // cbPattern
            // 
            this.cbPattern.BackColor = System.Drawing.Color.AntiqueWhite;
            this.cbPattern.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPattern.DropDownWidth = 170;
            this.cbPattern.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPattern.FormattingEnabled = true;
            this.cbPattern.Items.AddRange(new object[] {
            "Argentine Tango",
            "Argentine Tango (Solo)",
            "Association Waltz",
            "Blues",
            "Canasta Tango",
            "Carlos Tango",
            "Castel March",
            "City Blues",
            "Denver Shuffle",
            "Easy Paso",
            "Fourteen Step",
            "Harris Tango",
            "Kilian",
            "Italian Foxtrot Solo",
            "La Vista Cha Cha",
            "Midnight Blues",
            "Paso Doble",
            "Quick Step",
            "Rocker Foxtrot",
            "Shaken Samba",
            "Starlight Waltz",
            "Tango Delancha",
            "Tango Delanco",
            "Terenzi",
            "Tudor Waltz",
            "Viennese Waltz",
            "Westminster Waltz"});
            this.cbPattern.Location = new System.Drawing.Point(321, 384);
            this.cbPattern.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbPattern.Name = "cbPattern";
            this.cbPattern.Size = new System.Drawing.Size(198, 26);
            this.cbPattern.TabIndex = 167;
            // 
            // labPattern
            // 
            this.labPattern.AutoSize = true;
            this.labPattern.BackColor = System.Drawing.Color.Transparent;
            this.labPattern.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPattern.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.labPattern.Location = new System.Drawing.Point(129, 388);
            this.labPattern.Name = "labPattern";
            this.labPattern.Size = new System.Drawing.Size(184, 22);
            this.labPattern.TabIndex = 166;
            this.labPattern.Tag = "";
            this.labPattern.Text = "Select Pattern Sequence";
            // 
            // panStyle
            // 
            this.panStyle.Controls.Add(this.check2);
            this.panStyle.Controls.Add(this.cbCD2);
            this.panStyle.Controls.Add(this.l10);
            this.panStyle.Controls.Add(this.b3);
            this.panStyle.Controls.Add(this.b4);
            this.panStyle.Controls.Add(this.b8);
            this.panStyle.Controls.Add(this.o2);
            this.panStyle.Location = new System.Drawing.Point(7, 427);
            this.panStyle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panStyle.Name = "panStyle";
            this.panStyle.Size = new System.Drawing.Size(548, 41);
            this.panStyle.TabIndex = 165;
            this.panStyle.Paint += new System.Windows.Forms.PaintEventHandler(this.panStyle_Paint);
            // 
            // check2
            // 
            this.check2.BackColor = System.Drawing.Color.White;
            this.check2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.check2.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan;
            this.check2.FlatAppearance.BorderSize = 0;
            this.check2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.check2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.check2.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check2.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.check2.Image = ((System.Drawing.Image)(resources.GetObject("check2.Image")));
            this.check2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.check2.Location = new System.Drawing.Point(512, 4);
            this.check2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.check2.Name = "check2";
            this.check2.Size = new System.Drawing.Size(35, 30);
            this.check2.TabIndex = 172;
            this.check2.Tag = "1";
            this.check2.UseVisualStyleBackColor = false;
            this.check2.Visible = false;
            this.check2.Click += new System.EventHandler(this.check2_Click);
            // 
            // cbCD2
            // 
            this.cbCD2.BackColor = System.Drawing.Color.AntiqueWhite;
            this.cbCD2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCD2.DropDownWidth = 170;
            this.cbCD2.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCD2.FormattingEnabled = true;
            this.cbCD2.Items.AddRange(new object[] {
            "<COMPULSORY 2>",
            "Argentine Tango",
            "Argentine Tango (Solo)",
            "Association Waltz",
            "Blues",
            "Canasta Tango",
            "Carlos Tango",
            "Castel March",
            "City Blues",
            "Denver Shuffle",
            "Easy Paso",
            "Fourteen Step",
            "Harris Tango",
            "Kilian",
            "Italian Foxtrot Solo",
            "La Vista Cha Cha",
            "Midnight Blues",
            "Paso Doble",
            "Quick Step",
            "Rocker Foxtrot",
            "Shaken Samba",
            "Starlight Waltz",
            "Tango Delancha",
            "Tango Delanco",
            "Terenzi",
            "Tudor Waltz",
            "Viennese Waltz",
            "Westminster Waltz"});
            this.cbCD2.Location = new System.Drawing.Point(18, 7);
            this.cbCD2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbCD2.Name = "cbCD2";
            this.cbCD2.Size = new System.Drawing.Size(150, 26);
            this.cbCD2.TabIndex = 171;
            // 
            // l10
            // 
            this.l10.BackColor = System.Drawing.Color.Transparent;
            this.l10.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l10.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.l10.Location = new System.Drawing.Point(37, 7);
            this.l10.Name = "l10";
            this.l10.Size = new System.Drawing.Size(138, 24);
            this.l10.TabIndex = 150;
            this.l10.Tag = "";
            this.l10.Text = "Short Program";
            this.l10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // b3
            // 
            this.b3.BackColor = System.Drawing.Color.White;
            this.b3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b3.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.b3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.b3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b3.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b3.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.b3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b3.Location = new System.Drawing.Point(183, 4);
            this.b3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.b3.Name = "b3";
            this.b3.Size = new System.Drawing.Size(122, 32);
            this.b3.TabIndex = 154;
            this.b3.Text = "Start";
            this.b3.UseVisualStyleBackColor = false;
            this.b3.Visible = false;
            this.b3.Click += new System.EventHandler(this.b3_Click);
            // 
            // b4
            // 
            this.b4.BackColor = System.Drawing.Color.White;
            this.b4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b4.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.b4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.b4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b4.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b4.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.b4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b4.Location = new System.Drawing.Point(183, 4);
            this.b4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.b4.Name = "b4";
            this.b4.Size = new System.Drawing.Size(122, 32);
            this.b4.TabIndex = 155;
            this.b4.Text = "View Results";
            this.b4.UseVisualStyleBackColor = false;
            this.b4.Visible = false;
            this.b4.Click += new System.EventHandler(this.b4_Click);
            // 
            // b8
            // 
            this.b8.BackColor = System.Drawing.Color.White;
            this.b8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b8.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.b8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.b8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b8.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b8.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.b8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b8.Location = new System.Drawing.Point(312, 4);
            this.b8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.b8.Name = "b8";
            this.b8.Size = new System.Drawing.Size(73, 32);
            this.b8.TabIndex = 159;
            this.b8.Text = "Delete";
            this.b8.UseVisualStyleBackColor = false;
            this.b8.Visible = false;
            this.b8.Click += new System.EventHandler(this.b8_Click);
            // 
            // o2
            // 
            this.o2.BackColor = System.Drawing.Color.White;
            this.o2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.o2.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.o2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.o2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.o2.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.o2.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.o2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.o2.Location = new System.Drawing.Point(392, 4);
            this.o2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.o2.Name = "o2";
            this.o2.Size = new System.Drawing.Size(120, 32);
            this.o2.TabIndex = 161;
            this.o2.Text = "Skating Order";
            this.o2.UseVisualStyleBackColor = false;
            this.o2.Visible = false;
            this.o2.Click += new System.EventHandler(this.o2_Click);
            // 
            // l20
            // 
            this.l20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.l20.AutoSize = true;
            this.l20.BackColor = System.Drawing.Color.Transparent;
            this.l20.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l20.ForeColor = System.Drawing.Color.Black;
            this.l20.Location = new System.Drawing.Point(492, 3);
            this.l20.Name = "l20";
            this.l20.Size = new System.Drawing.Size(41, 16);
            this.l20.TabIndex = 164;
            this.l20.Tag = "";
            this.l20.Text = "idxxxx";
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.SlateGray;
            this.label22.Location = new System.Drawing.Point(443, 3);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(56, 18);
            this.label22.TabIndex = 163;
            this.label22.Tag = "";
            this.label22.Text = "Event ID:";
            // 
            // o3
            // 
            this.o3.BackColor = System.Drawing.Color.White;
            this.o3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.o3.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.o3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.o3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.o3.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.o3.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.o3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.o3.Location = new System.Drawing.Point(399, 464);
            this.o3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.o3.Name = "o3";
            this.o3.Size = new System.Drawing.Size(120, 32);
            this.o3.TabIndex = 162;
            this.o3.Text = "Skating Order";
            this.o3.UseVisualStyleBackColor = false;
            this.o3.Click += new System.EventHandler(this.o3_Click);
            // 
            // b9
            // 
            this.b9.BackColor = System.Drawing.Color.White;
            this.b9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b9.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.b9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.b9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b9.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b9.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.b9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b9.Location = new System.Drawing.Point(319, 464);
            this.b9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.b9.Name = "b9";
            this.b9.Size = new System.Drawing.Size(73, 32);
            this.b9.TabIndex = 160;
            this.b9.Text = "Delete";
            this.b9.UseVisualStyleBackColor = false;
            this.b9.Click += new System.EventHandler(this.b9_Click);
            // 
            // b7
            // 
            this.b7.BackColor = System.Drawing.Color.White;
            this.b7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b7.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.b7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.b7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b7.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b7.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.b7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b7.Location = new System.Drawing.Point(319, 383);
            this.b7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.b7.Name = "b7";
            this.b7.Size = new System.Drawing.Size(73, 32);
            this.b7.TabIndex = 158;
            this.b7.Text = "Delete";
            this.b7.UseVisualStyleBackColor = false;
            this.b7.Visible = false;
            this.b7.Click += new System.EventHandler(this.b7_Click);
            // 
            // b6
            // 
            this.b6.BackColor = System.Drawing.Color.White;
            this.b6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b6.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.b6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.b6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b6.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b6.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.b6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b6.Location = new System.Drawing.Point(190, 464);
            this.b6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.b6.Name = "b6";
            this.b6.Size = new System.Drawing.Size(122, 32);
            this.b6.TabIndex = 157;
            this.b6.Text = "View Results";
            this.b6.UseVisualStyleBackColor = false;
            this.b6.Click += new System.EventHandler(this.b6_Click);
            // 
            // b5
            // 
            this.b5.BackColor = System.Drawing.Color.White;
            this.b5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b5.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.b5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.b5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b5.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b5.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.b5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b5.Location = new System.Drawing.Point(190, 464);
            this.b5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.b5.Name = "b5";
            this.b5.Size = new System.Drawing.Size(121, 32);
            this.b5.TabIndex = 156;
            this.b5.Text = "Start";
            this.b5.UseVisualStyleBackColor = false;
            this.b5.Click += new System.EventHandler(this.b5_Click);
            // 
            // b2
            // 
            this.b2.BackColor = System.Drawing.Color.White;
            this.b2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b2.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.b2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.b2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b2.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b2.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.b2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b2.Location = new System.Drawing.Point(190, 383);
            this.b2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.b2.Name = "b2";
            this.b2.Size = new System.Drawing.Size(121, 32);
            this.b2.TabIndex = 153;
            this.b2.Text = "View Results";
            this.b2.UseVisualStyleBackColor = false;
            this.b2.Visible = false;
            this.b2.Click += new System.EventHandler(this.b2_Click);
            // 
            // b1
            // 
            this.b1.BackColor = System.Drawing.Color.White;
            this.b1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b1.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.b1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.b1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b1.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b1.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.b1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b1.Location = new System.Drawing.Point(190, 383);
            this.b1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.b1.Name = "b1";
            this.b1.Size = new System.Drawing.Size(121, 32);
            this.b1.TabIndex = 152;
            this.b1.Text = "Start";
            this.b1.UseVisualStyleBackColor = false;
            this.b1.Visible = false;
            this.b1.Click += new System.EventHandler(this.b1_Click);
            // 
            // l11
            // 
            this.l11.BackColor = System.Drawing.Color.Transparent;
            this.l11.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l11.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.l11.Location = new System.Drawing.Point(43, 468);
            this.l11.Name = "l11";
            this.l11.Size = new System.Drawing.Size(138, 24);
            this.l11.TabIndex = 151;
            this.l11.Tag = "";
            this.l11.Text = "Short Program";
            this.l11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.SlateGray;
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label14.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(363, 120);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 24);
            this.label14.TabIndex = 148;
            this.label14.Tag = "";
            this.label14.Text = "Category";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.SlateGray;
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label20.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(96, 120);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(81, 24);
            this.label20.TabIndex = 147;
            this.label20.Tag = "";
            this.label20.Text = "Discipline";
            // 
            // l8
            // 
            this.l8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.l8.AutoSize = true;
            this.l8.BackColor = System.Drawing.Color.Transparent;
            this.l8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.l8.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l8.ForeColor = System.Drawing.Color.Black;
            this.l8.Location = new System.Drawing.Point(451, 120);
            this.l8.Name = "l8";
            this.l8.Size = new System.Drawing.Size(81, 24);
            this.l8.TabIndex = 146;
            this.l8.Tag = "";
            this.l8.Text = "Categoria";
            this.l8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // l7
            // 
            this.l7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.l7.AutoSize = true;
            this.l7.BackColor = System.Drawing.Color.Transparent;
            this.l7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.l7.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l7.ForeColor = System.Drawing.Color.Black;
            this.l7.Location = new System.Drawing.Point(187, 120);
            this.l7.Name = "l7";
            this.l7.Size = new System.Drawing.Size(82, 24);
            this.l7.TabIndex = 145;
            this.l7.Tag = "";
            this.l7.Text = "Specialità";
            // 
            // o1
            // 
            this.o1.BackColor = System.Drawing.Color.White;
            this.o1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.o1.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.o1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.o1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.o1.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.o1.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.o1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.o1.Location = new System.Drawing.Point(399, 383);
            this.o1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.o1.Name = "o1";
            this.o1.Size = new System.Drawing.Size(120, 32);
            this.o1.TabIndex = 144;
            this.o1.Text = "Skating Order";
            this.o1.UseVisualStyleBackColor = false;
            this.o1.Visible = false;
            this.o1.Click += new System.EventHandler(this.o1_Click);
            // 
            // l6
            // 
            this.l6.AutoSize = true;
            this.l6.BackColor = System.Drawing.Color.Transparent;
            this.l6.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l6.ForeColor = System.Drawing.Color.Black;
            this.l6.Location = new System.Drawing.Point(490, 96);
            this.l6.Name = "l6";
            this.l6.Size = new System.Drawing.Size(135, 22);
            this.l6.TabIndex = 143;
            this.l6.Tag = "";
            this.l6.Text = "Competitor name";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.SlateGray;
            this.label19.Location = new System.Drawing.Point(383, 96);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(104, 22);
            this.label19.TabIndex = 0;
            this.label19.Tag = "";
            this.label19.Text = "Competitors:";
            // 
            // judges
            // 
            this.judges.BackColor = System.Drawing.Color.White;
            this.judges.Cursor = System.Windows.Forms.Cursors.Hand;
            this.judges.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan;
            this.judges.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.judges.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.judges.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.judges.ForeColor = System.Drawing.Color.SlateGray;
            this.judges.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.judges.Location = new System.Drawing.Point(38, 164);
            this.judges.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.judges.Name = "judges";
            this.judges.Size = new System.Drawing.Size(137, 32);
            this.judges.TabIndex = 141;
            this.judges.Text = "Judges Panel";
            this.judges.UseVisualStyleBackColor = false;
            this.judges.Click += new System.EventHandler(this.judges_Click);
            // 
            // l5
            // 
            this.l5.AutoSize = true;
            this.l5.BackColor = System.Drawing.Color.Transparent;
            this.l5.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l5.ForeColor = System.Drawing.Color.Black;
            this.l5.Location = new System.Drawing.Point(181, 96);
            this.l5.Name = "l5";
            this.l5.Size = new System.Drawing.Size(135, 22);
            this.l5.TabIndex = 140;
            this.l5.Tag = "";
            this.l5.Text = "Competitor name";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.SlateGray;
            this.label18.Location = new System.Drawing.Point(114, 94);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(64, 22);
            this.label18.TabIndex = 139;
            this.label18.Tag = "";
            this.label18.Text = "Judges:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.SlateGray;
            this.label17.Location = new System.Drawing.Point(126, 70);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 22);
            this.label17.TabIndex = 137;
            this.label17.Tag = "";
            this.label17.Text = "Place:";
            // 
            // l3
            // 
            this.l3.AutoSize = true;
            this.l3.BackColor = System.Drawing.Color.Transparent;
            this.l3.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l3.ForeColor = System.Drawing.Color.Black;
            this.l3.Location = new System.Drawing.Point(322, 47);
            this.l3.Name = "l3";
            this.l3.Size = new System.Drawing.Size(43, 22);
            this.l3.TabIndex = 136;
            this.l3.Tag = "";
            this.l3.Text = "Date";
            // 
            // l2
            // 
            this.l2.AutoSize = true;
            this.l2.BackColor = System.Drawing.Color.Transparent;
            this.l2.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l2.ForeColor = System.Drawing.Color.Black;
            this.l2.Location = new System.Drawing.Point(181, 47);
            this.l2.Name = "l2";
            this.l2.Size = new System.Drawing.Size(43, 22);
            this.l2.TabIndex = 135;
            this.l2.Tag = "";
            this.l2.Text = "Date";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.DarkCyan;
            this.label13.Location = new System.Drawing.Point(286, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(34, 22);
            this.label13.TabIndex = 134;
            this.label13.Tag = "";
            this.label13.Text = "To:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.SlateGray;
            this.label11.Location = new System.Drawing.Point(92, 47);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 22);
            this.label11.TabIndex = 133;
            this.label11.Tag = "";
            this.label11.Text = "Date from:";
            // 
            // connect
            // 
            this.connect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.connect.BackColor = System.Drawing.Color.White;
            this.connect.Cursor = System.Windows.Forms.Cursors.Hand;
            this.connect.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan;
            this.connect.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.connect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.connect.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connect.ForeColor = System.Drawing.Color.DarkCyan;
            this.connect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.connect.Location = new System.Drawing.Point(706, 553);
            this.connect.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.connect.Name = "connect";
            this.connect.Size = new System.Drawing.Size(152, 47);
            this.connect.TabIndex = 140;
            this.connect.Tag = "1";
            this.connect.Text = "Connect Judges";
            this.connect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.connect.UseVisualStyleBackColor = false;
            this.connect.Visible = false;
            // 
            // reset
            // 
            this.reset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.reset.BackColor = System.Drawing.Color.White;
            this.reset.Cursor = System.Windows.Forms.Cursors.Hand;
            this.reset.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.reset.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.reset.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.reset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.reset.Font = new System.Drawing.Font("Trebuchet MS", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reset.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.reset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.reset.Location = new System.Drawing.Point(195, 553);
            this.reset.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.reset.Name = "reset";
            this.reset.Size = new System.Drawing.Size(74, 47);
            this.reset.TabIndex = 139;
            this.reset.Text = "Reset";
            this.reset.UseVisualStyleBackColor = false;
            this.reset.Click += new System.EventHandler(this.reset_Click);
            // 
            // restore
            // 
            this.restore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.restore.BackColor = System.Drawing.Color.White;
            this.restore.Cursor = System.Windows.Forms.Cursors.Hand;
            this.restore.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.restore.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.restore.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.restore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.restore.Font = new System.Drawing.Font("Trebuchet MS", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.restore.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.restore.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.restore.Location = new System.Drawing.Point(368, 553);
            this.restore.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.restore.Name = "restore";
            this.restore.Size = new System.Drawing.Size(89, 47);
            this.restore.TabIndex = 152;
            this.restore.Text = "Restore";
            this.restore.UseVisualStyleBackColor = false;
            this.restore.Click += new System.EventHandler(this.restore_Click);
            // 
            // backup
            // 
            this.backup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.backup.BackColor = System.Drawing.Color.White;
            this.backup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backup.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.backup.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.backup.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.backup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backup.Font = new System.Drawing.Font("Trebuchet MS", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backup.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.backup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.backup.Location = new System.Drawing.Point(275, 553);
            this.backup.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.backup.Name = "backup";
            this.backup.Size = new System.Drawing.Size(87, 47);
            this.backup.TabIndex = 151;
            this.backup.Text = "Backup";
            this.backup.UseVisualStyleBackColor = false;
            this.backup.Click += new System.EventHandler(this.backup_Click);
            // 
            // pb
            // 
            this.pb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pb.Image = ((System.Drawing.Image)(resources.GetObject("pb.Image")));
            this.pb.Location = new System.Drawing.Point(455, 553);
            this.pb.Name = "pb";
            this.pb.Size = new System.Drawing.Size(54, 47);
            this.pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb.TabIndex = 141;
            this.pb.TabStop = false;
            this.pb.Visible = false;
            this.pb.Click += new System.EventHandler(this.pb_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(512, 566);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 24);
            this.label5.TabIndex = 151;
            this.label5.Tag = "";
            this.label5.Text = "0";
            this.label5.Visible = false;
            // 
            // tv
            // 
            this.tv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tv.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tv.ContextMenuStrip = this.menu;
            this.tv.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tv.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tv.FullRowSelect = true;
            this.tv.HideSelection = false;
            this.tv.ImageIndex = 41;
            this.tv.ImageList = this.imageList3;
            this.tv.Indent = 27;
            this.tv.ItemHeight = 24;
            this.tv.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.tv.Location = new System.Drawing.Point(9, 7);
            this.tv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tv.Name = "tv";
            treeNode1.ImageIndex = 46;
            treeNode1.Name = "Nodo0";
            treeNode1.NodeFont = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            treeNode1.SelectedImageIndex = 46;
            treeNode1.Text = "Events";
            this.tv.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
            this.tv.SelectedImageIndex = 8;
            this.tv.ShowNodeToolTips = true;
            this.tv.Size = new System.Drawing.Size(448, 540);
            this.tv.TabIndex = 1;
            this.tv.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.tv_AfterLabelEdit);
            this.tv.DrawNode += new System.Windows.Forms.DrawTreeNodeEventHandler(this.tv_DrawNode);
            this.tv.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.tv_ItemDrag);
            this.tv.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tv_AfterSelect);
            this.tv.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tv_NodeMouseClick);
            this.tv.DragDrop += new System.Windows.Forms.DragEventHandler(this.tv_DragDrop);
            this.tv.DragEnter += new System.Windows.Forms.DragEventHandler(this.tv_DragEnter);
            // 
            // resultsPanel
            // 
            this.resultsPanel.BackColor = System.Drawing.Color.White;
            this.resultsPanel.Controls.Add(this.button7);
            this.resultsPanel.Controls.Add(this.button3);
            this.resultsPanel.Controls.Add(this.result);
            this.resultsPanel.Controls.Add(this.classifica);
            this.resultsPanel.Controls.Add(this.panel);
            this.resultsPanel.Controls.Add(this.back);
            this.resultsPanel.Controls.Add(this.cr);
            this.resultsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultsPanel.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultsPanel.Location = new System.Drawing.Point(0, 0);
            this.resultsPanel.Name = "resultsPanel";
            this.resultsPanel.Size = new System.Drawing.Size(1034, 682);
            this.resultsPanel.TabIndex = 148;
            this.resultsPanel.Visible = false;
            // 
            // button7
            // 
            this.button7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button7.BackColor = System.Drawing.Color.White;
            this.button7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button7.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Trebuchet MS", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(904, 553);
            this.button7.Margin = new System.Windows.Forms.Padding(0);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(120, 47);
            this.button7.TabIndex = 137;
            this.button7.Text = "Export to xml";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Visible = false;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.DarkCyan;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.Location = new System.Drawing.Point(494, 553);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(112, 47);
            this.button3.TabIndex = 150;
            this.button3.Tag = "1";
            this.button3.Text = "Export All";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // result
            // 
            this.result.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.result.BackColor = System.Drawing.Color.White;
            this.result.Cursor = System.Windows.Forms.Cursors.Hand;
            this.result.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan;
            this.result.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.result.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.result.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.result.ForeColor = System.Drawing.Color.DarkCyan;
            this.result.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.result.ImageIndex = 2;
            this.result.ImageList = this.imageList3;
            this.result.Location = new System.Drawing.Point(608, 553);
            this.result.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(89, 47);
            this.result.TabIndex = 149;
            this.result.Tag = "1";
            this.result.Text = "Results";
            this.result.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.result.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.result.UseVisualStyleBackColor = false;
            this.result.Visible = false;
            this.result.Click += new System.EventHandler(this.result_Click);
            // 
            // classifica
            // 
            this.classifica.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.classifica.BackColor = System.Drawing.Color.White;
            this.classifica.Cursor = System.Windows.Forms.Cursors.Hand;
            this.classifica.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan;
            this.classifica.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.classifica.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.classifica.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.classifica.ForeColor = System.Drawing.Color.DarkCyan;
            this.classifica.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.classifica.ImageIndex = 2;
            this.classifica.ImageList = this.imageList3;
            this.classifica.Location = new System.Drawing.Point(699, 553);
            this.classifica.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.classifica.Name = "classifica";
            this.classifica.Size = new System.Drawing.Size(124, 47);
            this.classifica.TabIndex = 148;
            this.classifica.Tag = "1";
            this.classifica.Text = "Classification";
            this.classifica.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.classifica.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.classifica.UseVisualStyleBackColor = false;
            this.classifica.Visible = false;
            this.classifica.Click += new System.EventHandler(this.classifica_Click);
            // 
            // panel
            // 
            this.panel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel.BackColor = System.Drawing.Color.White;
            this.panel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan;
            this.panel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.panel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.panel.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel.ForeColor = System.Drawing.Color.DarkCyan;
            this.panel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.panel.ImageIndex = 2;
            this.panel.ImageList = this.imageList3;
            this.panel.Location = new System.Drawing.Point(825, 553);
            this.panel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(135, 47);
            this.panel.TabIndex = 147;
            this.panel.Tag = "1";
            this.panel.Text = "Judges Scores";
            this.panel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.panel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.panel.UseVisualStyleBackColor = false;
            this.panel.Visible = false;
            this.panel.Click += new System.EventHandler(this.panel_Click);
            // 
            // back
            // 
            this.back.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.back.BackColor = System.Drawing.Color.White;
            this.back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.back.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan;
            this.back.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.back.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.back.ForeColor = System.Drawing.Color.DarkCyan;
            this.back.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.back.Location = new System.Drawing.Point(962, 553);
            this.back.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(60, 47);
            this.back.TabIndex = 146;
            this.back.Tag = "1";
            this.back.Text = "Back";
            this.back.UseVisualStyleBackColor = false;
            this.back.Visible = false;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // cr
            // 
            this.cr.ActiveViewIndex = -1;
            this.cr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cr.Cursor = System.Windows.Forms.Cursors.Default;
            this.cr.EnableDrillDown = false;
            this.cr.Location = new System.Drawing.Point(0, 0);
            this.cr.Name = "cr";
            this.cr.Size = new System.Drawing.Size(1034, 551);
            this.cr.TabIndex = 0;
            // 
            // deleteall
            // 
            this.deleteall.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deleteall.BackColor = System.Drawing.Color.White;
            this.deleteall.Cursor = System.Windows.Forms.Cursors.Hand;
            this.deleteall.Enabled = false;
            this.deleteall.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.deleteall.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.deleteall.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteall.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteall.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.deleteall.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.deleteall.Location = new System.Drawing.Point(143, 553);
            this.deleteall.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.deleteall.Name = "deleteall";
            this.deleteall.Size = new System.Drawing.Size(84, 47);
            this.deleteall.TabIndex = 150;
            this.deleteall.Text = "Delete all";
            this.deleteall.UseVisualStyleBackColor = false;
            this.deleteall.Visible = false;
            // 
            // panNewEvent
            // 
            this.panNewEvent.AutoScroll = true;
            this.panNewEvent.BackColor = System.Drawing.Color.White;
            this.panNewEvent.Controls.Add(this.label23);
            this.panNewEvent.Controls.Add(this.label9);
            this.panNewEvent.Controls.Add(this.groupBox1);
            this.panNewEvent.Controls.Add(this.pb1);
            this.panNewEvent.Controls.Add(this.nJudges);
            this.panNewEvent.Controls.Add(this.confirmNewEvent);
            this.panNewEvent.Controls.Add(this.lv1);
            this.panNewEvent.Controls.Add(this.label7);
            this.panNewEvent.Controls.Add(this.browse);
            this.panNewEvent.Controls.Add(this.cb3);
            this.panNewEvent.Controls.Add(this.cb2);
            this.panNewEvent.Controls.Add(this.cb1);
            this.panNewEvent.Controls.Add(this.cbCategory);
            this.panNewEvent.Controls.Add(this.label16);
            this.panNewEvent.Controls.Add(this.label15);
            this.panNewEvent.Controls.Add(this.cbEvent);
            this.panNewEvent.Controls.Add(this.label12);
            this.panNewEvent.Controls.Add(this.label3);
            this.panNewEvent.Controls.Add(this.label2);
            this.panNewEvent.Controls.Add(this.place);
            this.panNewEvent.Controls.Add(this.label4);
            this.panNewEvent.Controls.Add(this.name);
            this.panNewEvent.Controls.Add(this.dateFrom);
            this.panNewEvent.Controls.Add(this.dateTo);
            this.panNewEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panNewEvent.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panNewEvent.Location = new System.Drawing.Point(0, 0);
            this.panNewEvent.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panNewEvent.Name = "panNewEvent";
            this.panNewEvent.Size = new System.Drawing.Size(1034, 682);
            this.panNewEvent.TabIndex = 2;
            this.panNewEvent.Visible = false;
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Gray;
            this.label23.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label23.Location = new System.Drawing.Point(340, 181);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(93, 24);
            this.label23.TabIndex = 152;
            this.label23.Tag = "";
            this.label23.Text = "No Events";
            this.label23.Visible = false;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.DarkOrange;
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(760, 266);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(127, 18);
            this.label9.TabIndex = 151;
            this.label9.Tag = "";
            this.label9.Text = "Number of Judges:";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.id);
            this.groupBox1.Controls.Add(this.region);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.hidden);
            this.groupBox1.Controls.Add(this.updateskater);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.club);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.naz);
            this.groupBox1.Controls.Add(this.add);
            this.groupBox1.Controls.Add(this.lv2);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.namep);
            this.groupBox1.Controls.Add(this.delete);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.groupBox1.Location = new System.Drawing.Point(14, 234);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(744, 362);
            this.groupBox1.TabIndex = 150;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Skaters List*";
            // 
            // id
            // 
            this.id.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.id.AutoSize = true;
            this.id.BackColor = System.Drawing.Color.Transparent;
            this.id.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.id.ForeColor = System.Drawing.Color.DarkOrange;
            this.id.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.id.Location = new System.Drawing.Point(560, 14);
            this.id.Name = "id";
            this.id.Size = new System.Drawing.Size(17, 16);
            this.id.TabIndex = 157;
            this.id.Tag = "";
            this.id.Text = "id";
            this.id.Visible = false;
            // 
            // region
            // 
            this.region.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.region.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.region.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.region.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.region.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.region.ForeColor = System.Drawing.Color.Black;
            this.region.Location = new System.Drawing.Point(370, 65);
            this.region.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.region.MaxLength = 20;
            this.region.Name = "region";
            this.region.Size = new System.Drawing.Size(144, 26);
            this.region.TabIndex = 3;
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.DarkOrange;
            this.label21.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label21.Location = new System.Drawing.Point(304, 66);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(58, 22);
            this.label21.TabIndex = 156;
            this.label21.Tag = "";
            this.label21.Text = "Region";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // hidden
            // 
            this.hidden.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.hidden.AutoSize = true;
            this.hidden.BackColor = System.Drawing.Color.Transparent;
            this.hidden.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hidden.ForeColor = System.Drawing.Color.DarkOrange;
            this.hidden.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.hidden.Location = new System.Drawing.Point(437, 67);
            this.hidden.Name = "hidden";
            this.hidden.Size = new System.Drawing.Size(0, 22);
            this.hidden.TabIndex = 154;
            this.hidden.Tag = "";
            this.hidden.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // updateskater
            // 
            this.updateskater.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.updateskater.BackColor = System.Drawing.Color.Transparent;
            this.updateskater.Cursor = System.Windows.Forms.Cursors.Hand;
            this.updateskater.Enabled = false;
            this.updateskater.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.updateskater.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.updateskater.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateskater.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateskater.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.updateskater.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.updateskater.Location = new System.Drawing.Point(582, 63);
            this.updateskater.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.updateskater.Name = "updateskater";
            this.updateskater.Size = new System.Drawing.Size(75, 28);
            this.updateskater.TabIndex = 5;
            this.updateskater.Text = "UPDATE";
            this.updateskater.UseVisualStyleBackColor = false;
            this.updateskater.Click += new System.EventHandler(this.updateskater_Click);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.DarkOrange;
            this.label10.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label10.Location = new System.Drawing.Point(674, 14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 16);
            this.label10.TabIndex = 152;
            this.label10.Tag = "";
            this.label10.Text = "3 chars";
            // 
            // club
            // 
            this.club.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.club.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.club.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.club.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.club.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.club.ForeColor = System.Drawing.Color.Black;
            this.club.Location = new System.Drawing.Point(76, 65);
            this.club.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.club.MaxLength = 50;
            this.club.Name = "club";
            this.club.Size = new System.Drawing.Size(212, 26);
            this.club.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DarkOrange;
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(4, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 22);
            this.label6.TabIndex = 144;
            this.label6.Tag = "";
            this.label6.Text = "Skater*";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // naz
            // 
            this.naz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.naz.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.naz.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.naz.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.naz.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.naz.ForeColor = System.Drawing.Color.Black;
            this.naz.Location = new System.Drawing.Point(664, 32);
            this.naz.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.naz.MaxLength = 3;
            this.naz.Name = "naz";
            this.naz.Size = new System.Drawing.Size(62, 26);
            this.naz.TabIndex = 1;
            // 
            // add
            // 
            this.add.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.add.BackColor = System.Drawing.Color.Transparent;
            this.add.Cursor = System.Windows.Forms.Cursors.Hand;
            this.add.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.add.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.add.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.add.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.add.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.add.Location = new System.Drawing.Point(520, 63);
            this.add.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(60, 28);
            this.add.TabIndex = 4;
            this.add.Text = "ADD";
            this.add.UseVisualStyleBackColor = false;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // lv2
            // 
            this.lv2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lv2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lv2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ch1,
            this.ch2,
            this.columnHeader4,
            this.columnHeader1,
            this.columnHeader5,
            this.columnHeader2,
            this.columnHeader10});
            this.lv2.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv2.FullRowSelect = true;
            this.lv2.GridLines = true;
            this.lv2.Location = new System.Drawing.Point(0, 106);
            this.lv2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lv2.MultiSelect = false;
            this.lv2.Name = "lv2";
            this.lv2.ShowGroups = false;
            this.lv2.ShowItemToolTips = true;
            this.lv2.Size = new System.Drawing.Size(744, 256);
            this.lv2.TabIndex = 146;
            this.lv2.UseCompatibleStateImageBehavior = false;
            this.lv2.View = System.Windows.Forms.View.Details;
            this.lv2.SelectedIndexChanged += new System.EventHandler(this.lv2_SelectedIndexChanged);
            // 
            // ch1
            // 
            this.ch1.Text = "#";
            this.ch1.Width = 47;
            // 
            // ch2
            // 
            this.ch2.Text = "Skater ";
            this.ch2.Width = 250;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Club";
            this.columnHeader4.Width = 230;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Region";
            this.columnHeader1.Width = 130;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Nation";
            this.columnHeader5.Width = 50;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Spec";
            this.columnHeader2.Width = 0;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "ID";
            this.columnHeader10.Width = 0;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.DarkOrange;
            this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label8.Location = new System.Drawing.Point(601, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 22);
            this.label8.TabIndex = 145;
            this.label8.Tag = "";
            this.label8.Text = "Nation";
            // 
            // namep
            // 
            this.namep.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.namep.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.namep.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.namep.BackColor = System.Drawing.Color.White;
            this.namep.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.namep.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.namep.Location = new System.Drawing.Point(76, 30);
            this.namep.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.namep.MaxLength = 50;
            this.namep.Name = "namep";
            this.namep.Size = new System.Drawing.Size(504, 26);
            this.namep.TabIndex = 0;
            this.namep.TabIndexChanged += new System.EventHandler(this.namep_TabIndexChanged);
            this.namep.KeyDown += new System.Windows.Forms.KeyEventHandler(this.namep_KeyDown);
            this.namep.Leave += new System.EventHandler(this.namep_Leave);
            // 
            // delete
            // 
            this.delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.delete.BackColor = System.Drawing.Color.Transparent;
            this.delete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.delete.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.delete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.delete.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.delete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.delete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.delete.Location = new System.Drawing.Point(659, 63);
            this.delete.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(67, 28);
            this.delete.TabIndex = 6;
            this.delete.Text = "DELETE";
            this.delete.UseVisualStyleBackColor = false;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkOrange;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(22, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 22);
            this.label1.TabIndex = 65;
            this.label1.Tag = "";
            this.label1.Text = "Club";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pb1
            // 
            this.pb1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pb1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.pb1.Location = new System.Drawing.Point(767, 571);
            this.pb1.Maximum = 7;
            this.pb1.Name = "pb1";
            this.pb1.Size = new System.Drawing.Size(251, 23);
            this.pb1.Step = 1;
            this.pb1.TabIndex = 148;
            this.pb1.Visible = false;
            // 
            // nJudges
            // 
            this.nJudges.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nJudges.BackColor = System.Drawing.Color.White;
            this.nJudges.Font = new System.Drawing.Font("Trebuchet MS", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nJudges.Location = new System.Drawing.Point(890, 256);
            this.nJudges.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.nJudges.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.nJudges.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nJudges.Name = "nJudges";
            this.nJudges.Size = new System.Drawing.Size(38, 32);
            this.nJudges.TabIndex = 15;
            this.nJudges.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nJudges.ValueChanged += new System.EventHandler(this.nJudges_ValueChanged);
            // 
            // confirmNewEvent
            // 
            this.confirmNewEvent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.confirmNewEvent.BackColor = System.Drawing.Color.Transparent;
            this.confirmNewEvent.Cursor = System.Windows.Forms.Cursors.Hand;
            this.confirmNewEvent.Enabled = false;
            this.confirmNewEvent.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.confirmNewEvent.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.confirmNewEvent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.confirmNewEvent.Font = new System.Drawing.Font("Trebuchet MS", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirmNewEvent.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.confirmNewEvent.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.confirmNewEvent.Location = new System.Drawing.Point(764, 521);
            this.confirmNewEvent.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.confirmNewEvent.Name = "confirmNewEvent";
            this.confirmNewEvent.Size = new System.Drawing.Size(255, 75);
            this.confirmNewEvent.TabIndex = 17;
            this.confirmNewEvent.Text = "Insert Event";
            this.confirmNewEvent.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.confirmNewEvent.UseVisualStyleBackColor = false;
            this.confirmNewEvent.Click += new System.EventHandler(this.confirmNewEvent_Click);
            // 
            // lv1
            // 
            this.lv1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lv1.BackColor = System.Drawing.Color.OldLace;
            this.lv1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader6});
            this.lv1.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv1.FullRowSelect = true;
            this.lv1.GridLines = true;
            this.lv1.LargeImageList = this.imageList1;
            this.lv1.Location = new System.Drawing.Point(764, 294);
            this.lv1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lv1.MultiSelect = false;
            this.lv1.Name = "lv1";
            this.lv1.ShowGroups = false;
            this.lv1.ShowItemToolTips = true;
            this.lv1.Size = new System.Drawing.Size(256, 223);
            this.lv1.SmallImageList = this.imageList1;
            this.lv1.TabIndex = 142;
            this.lv1.UseCompatibleStateImageBehavior = false;
            this.lv1.View = System.Windows.Forms.View.SmallIcon;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "";
            this.columnHeader3.Width = 105;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "";
            this.columnHeader6.Width = 154;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(763, 226);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(140, 24);
            this.label7.TabIndex = 133;
            this.label7.Tag = "";
            this.label7.Text = "Officials Panel*";
            // 
            // browse
            // 
            this.browse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browse.BackColor = System.Drawing.Color.Transparent;
            this.browse.Cursor = System.Windows.Forms.Cursors.Hand;
            this.browse.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.browse.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.browse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.browse.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.browse.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.browse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.browse.Location = new System.Drawing.Point(935, 256);
            this.browse.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.browse.Name = "browse";
            this.browse.Size = new System.Drawing.Size(84, 32);
            this.browse.TabIndex = 12;
            this.browse.Text = "Select";
            this.browse.UseVisualStyleBackColor = false;
            this.browse.Click += new System.EventHandler(this.browse_Click);
            // 
            // cb3
            // 
            this.cb3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb3.AutoSize = true;
            this.cb3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb3.FlatAppearance.BorderSize = 0;
            this.cb3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb3.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cb3.Location = new System.Drawing.Point(585, 206);
            this.cb3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb3.Name = "cb3";
            this.cb3.Size = new System.Drawing.Size(147, 28);
            this.cb3.TabIndex = 9;
            this.cb3.Tag = "11";
            this.cb3.Text = "Compulsory 1";
            this.cb3.UseVisualStyleBackColor = true;
            this.cb3.Visible = false;
            // 
            // cb2
            // 
            this.cb2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb2.AutoSize = true;
            this.cb2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb2.FlatAppearance.BorderSize = 0;
            this.cb2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb2.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cb2.Location = new System.Drawing.Point(344, 206);
            this.cb2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb2.Name = "cb2";
            this.cb2.Size = new System.Drawing.Size(64, 28);
            this.cb2.TabIndex = 8;
            this.cb2.Tag = "2";
            this.cb2.Text = "long";
            this.cb2.UseVisualStyleBackColor = true;
            this.cb2.Visible = false;
            // 
            // cb1
            // 
            this.cb1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb1.AutoSize = true;
            this.cb1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb1.FlatAppearance.BorderSize = 0;
            this.cb1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb1.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cb1.Location = new System.Drawing.Point(344, 180);
            this.cb1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb1.Name = "cb1";
            this.cb1.Size = new System.Drawing.Size(72, 28);
            this.cb1.TabIndex = 5;
            this.cb1.Tag = "1";
            this.cb1.Text = "short";
            this.cb1.UseVisualStyleBackColor = true;
            this.cb1.Visible = false;
            // 
            // cbCategory
            // 
            this.cbCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCategory.BackColor = System.Drawing.Color.White;
            this.cbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCategory.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCategory.FormattingEnabled = true;
            this.cbCategory.Location = new System.Drawing.Point(797, 143);
            this.cbCategory.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbCategory.Name = "cbCategory";
            this.cbCategory.Size = new System.Drawing.Size(218, 30);
            this.cbCategory.TabIndex = 5;
            this.cbCategory.SelectedIndexChanged += new System.EventHandler(this.cbCategory_SelectedIndexChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label16.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label16.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label16.Location = new System.Drawing.Point(181, 107);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(31, 24);
            this.label16.TabIndex = 69;
            this.label16.Tag = "";
            this.label16.Text = "To";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label15.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label15.Location = new System.Drawing.Point(791, 107);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(92, 24);
            this.label15.TabIndex = 68;
            this.label15.Tag = "";
            this.label15.Text = "Category*";
            // 
            // cbEvent
            // 
            this.cbEvent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbEvent.BackColor = System.Drawing.Color.White;
            this.cbEvent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEvent.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEvent.FormattingEnabled = true;
            this.cbEvent.Location = new System.Drawing.Point(342, 142);
            this.cbEvent.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbEvent.Name = "cbEvent";
            this.cbEvent.Size = new System.Drawing.Size(414, 30);
            this.cbEvent.TabIndex = 4;
            this.cbEvent.SelectedIndexChanged += new System.EventHandler(this.cbEvent_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label12.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label12.Location = new System.Drawing.Point(337, 107);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(108, 24);
            this.label12.TabIndex = 66;
            this.label12.Tag = "";
            this.label12.Text = "Event type*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(10, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 24);
            this.label3.TabIndex = 62;
            this.label3.Tag = "";
            this.label3.Text = "Date from";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(791, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 24);
            this.label2.TabIndex = 61;
            this.label2.Tag = "";
            this.label2.Text = "Place";
            // 
            // place
            // 
            this.place.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.place.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.place.Location = new System.Drawing.Point(796, 57);
            this.place.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.place.MaxLength = 30;
            this.place.Name = "place";
            this.place.Size = new System.Drawing.Size(218, 26);
            this.place.TabIndex = 1;
            this.place.Leave += new System.EventHandler(this.place_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(10, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 24);
            this.label4.TabIndex = 60;
            this.label4.Tag = "";
            this.label4.Text = "Event name*";
            // 
            // name
            // 
            this.name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.name.BackColor = System.Drawing.Color.White;
            this.name.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name.Location = new System.Drawing.Point(26, 57);
            this.name.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.name.MaxLength = 55;
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(730, 26);
            this.name.TabIndex = 0;
            this.name.Leave += new System.EventHandler(this.name_Leave);
            // 
            // dateFrom
            // 
            this.dateFrom.CalendarMonthBackground = System.Drawing.SystemColors.Info;
            this.dateFrom.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateFrom.Location = new System.Drawing.Point(26, 142);
            this.dateFrom.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.Size = new System.Drawing.Size(112, 26);
            this.dateFrom.TabIndex = 2;
            // 
            // dateTo
            // 
            this.dateTo.CalendarMonthBackground = System.Drawing.SystemColors.Info;
            this.dateTo.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTo.Location = new System.Drawing.Point(185, 142);
            this.dateTo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateTo.Name = "dateTo";
            this.dateTo.Size = new System.Drawing.Size(112, 26);
            this.dateTo.TabIndex = 3;
            // 
            // panLog
            // 
            this.panLog.Controls.Add(this.button6);
            this.panLog.Controls.Add(this.log);
            this.panLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panLog.Location = new System.Drawing.Point(0, 0);
            this.panLog.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panLog.Name = "panLog";
            this.panLog.Size = new System.Drawing.Size(1034, 682);
            this.panLog.TabIndex = 94;
            this.panLog.Visible = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Transparent;
            this.button6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.Black;
            this.button6.Image = global::RollartSystemTech.Properties.Resources.document;
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(912, 6);
            this.button6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(121, 28);
            this.button6.TabIndex = 97;
            this.button6.Text = "Open Full Log";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // log
            // 
            this.log.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.log.BackColor = System.Drawing.Color.White;
            this.log.Font = new System.Drawing.Font("Courier New", 8F);
            this.log.ForeColor = System.Drawing.SystemColors.ControlText;
            this.log.Location = new System.Drawing.Point(5, 39);
            this.log.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.log.Multiline = true;
            this.log.Name = "log";
            this.log.ReadOnly = true;
            this.log.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.log.Size = new System.Drawing.Size(1026, 561);
            this.log.TabIndex = 94;
            // 
            // of1
            // 
            this.of1.FileName = "rolljudge2";
            this.of1.Filter = "File SQLite|*.s3db";
            this.of1.InitialDirectory = "C:\\RollartSystem";
            this.of1.RestoreDirectory = true;
            this.of1.Title = "Restore database";
            // 
            // fd1
            // 
            this.fd1.Description = "Select folder";
            this.fd1.SelectedPath = "C:\\RollartSystem";
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.BackColor = System.Drawing.Color.Transparent;
            this.message.Dock = System.Windows.Forms.DockStyle.Fill;
            this.message.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.message.ForeColor = System.Drawing.Color.Blue;
            this.message.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.message.Location = new System.Drawing.Point(0, 0);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(0, 22);
            this.message.TabIndex = 98;
            this.message.Tag = "";
            // 
            // message2
            // 
            this.message2.BackColor = System.Drawing.Color.Transparent;
            this.message2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.message2.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.message2.ForeColor = System.Drawing.Color.Black;
            this.message2.Location = new System.Drawing.Point(0, 59);
            this.message2.Name = "message2";
            this.message2.Size = new System.Drawing.Size(1207, 20);
            this.message2.TabIndex = 133;
            this.message2.Tag = "";
            this.message2.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.panel2.Controls.Add(this.message2);
            this.panel2.Controls.Add(this.message);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 654);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1207, 79);
            this.panel2.TabIndex = 1;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // sf1
            // 
            this.sf1.Filter = "SQLite database | *.s3db";
            this.sf1.Title = "Save a backup of database";
            // 
            // menu2
            // 
            this.menu2.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menu2.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menu2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newFolderToolStripMenuItem,
            this.importEvent});
            this.menu2.Name = "menu";
            this.menu2.Size = new System.Drawing.Size(151, 64);
            // 
            // newFolderToolStripMenuItem
            // 
            this.newFolderToolStripMenuItem.Image = global::RollartSystemTech.Properties.Resources.folder;
            this.newFolderToolStripMenuItem.Name = "newFolderToolStripMenuItem";
            this.newFolderToolStripMenuItem.Size = new System.Drawing.Size(150, 30);
            this.newFolderToolStripMenuItem.Text = "New folder";
            this.newFolderToolStripMenuItem.Visible = false;
            this.newFolderToolStripMenuItem.Click += new System.EventHandler(this.newFolderToolStripMenuItem_Click);
            // 
            // importEvent
            // 
            this.importEvent.Image = global::RollartSystemTech.Properties.Resources.doc_import_icon_24;
            this.importEvent.Name = "importEvent";
            this.importEvent.Size = new System.Drawing.Size(150, 30);
            this.importEvent.Text = "Import event";
            this.importEvent.Click += new System.EventHandler(this.importEvent_Click_1);
            // 
            // EventsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(95)))), ((int)(((byte)(89)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1207, 733);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "EventsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RollArt System";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EventsForm_FormClosing);
            this.Load += new System.EventHandler(this.EventsForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panStartEvent.ResumeLayout(false);
            this.panStartEvent.PerformLayout();
            this.gara.ResumeLayout(false);
            this.gara.PerformLayout();
            this.panStyle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb)).EndInit();
            this.resultsPanel.ResumeLayout(false);
            this.panNewEvent.ResumeLayout(false);
            this.panNewEvent.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nJudges)).EndInit();
            this.panLog.ResumeLayout(false);
            this.panLog.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.menu2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        //private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel panStartEvent;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buTech;
        private System.Windows.Forms.Button titolo;
        private System.Windows.Forms.Panel panNewEvent;
        private System.Windows.Forms.OpenFileDialog of1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.FolderBrowserDialog fd1;
        private System.Windows.Forms.CheckBox cb3;
        private System.Windows.Forms.CheckBox cb2;
        private System.Windows.Forms.CheckBox cb1;
        private System.Windows.Forms.ComboBox cbCategory;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cbEvent;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox place;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.DateTimePicker dateFrom;
        private System.Windows.Forms.DateTimePicker dateTo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button browse;
        private System.Windows.Forms.ListView lv1;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListView lv2;
        private System.Windows.Forms.ColumnHeader ch1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox club;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button add;
        private System.Windows.Forms.TextBox namep;
        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.ColumnHeader ch2;
        private System.Windows.Forms.Button confirmNewEvent;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.TextBox naz;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.TreeView tv;
        private System.Windows.Forms.Button reload;
        private System.Windows.Forms.Panel gara;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label l3;
        private System.Windows.Forms.Label l2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label l5;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button judges;
        private System.Windows.Forms.Button o1;
        private System.Windows.Forms.Label l6;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label l7;
        private System.Windows.Forms.Label l8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label l9;
        private System.Windows.Forms.Label l11;
        private System.Windows.Forms.Label l10;
        private System.Windows.Forms.Button b1;
        private System.Windows.Forms.Button b2;
        private System.Windows.Forms.Button b6;
        private System.Windows.Forms.Button b5;
        private System.Windows.Forms.Button b4;
        private System.Windows.Forms.Button b3;
        private System.Windows.Forms.Button b9;
        private System.Windows.Forms.Button b8;
        private System.Windows.Forms.Button b7;
        private System.Windows.Forms.ListView lv3;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader judgeName;
        private System.Windows.Forms.Button o3;
        private System.Windows.Forms.Button o2;
        private System.Windows.Forms.Label l20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button deletegare;
        private System.Windows.Forms.Button reset;
        private System.Windows.Forms.Button connect;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panLog;
        private System.Windows.Forms.NumericUpDown nJudges;
        private System.Windows.Forms.Panel panStyle;
        private System.Windows.Forms.Label labPattern;
        private System.Windows.Forms.ComboBox cbPattern;
        private System.Windows.Forms.PictureBox pb;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private RollartSystemTech.Report.ResultReport ResultReport1;
        private RollartSystemTech.Report.PanelReport PanelReport1;
        private RollartSystemTech.Report.FinalReport FinalReport1;
        private System.Windows.Forms.ImageList imageList3;
        private System.Windows.Forms.ProgressBar pb1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox l1;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.TextBox l4;
        private System.Windows.Forms.Panel resultsPanel;
        private System.Windows.Forms.Button classifica;
        private System.Windows.Forms.Button panel;
        private System.Windows.Forms.Button back;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer cr;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.Label message2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button result;
        private System.Windows.Forms.Button deleteall;
        private System.Windows.Forms.Button button3;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ContextMenuStrip menu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button backup;
        private System.Windows.Forms.Button restore;
        private System.Windows.Forms.SaveFileDialog sf1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button updateskater;
        private System.Windows.Forms.Label hidden;
        private System.Windows.Forms.TextBox log;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox region;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ComboBox cbCD1;
        private System.Windows.Forms.ComboBox cbCD2;
        private System.Windows.Forms.ContextMenuStrip menu2;
        private System.Windows.Forms.ToolStripMenuItem importEvent;
        private System.Windows.Forms.ToolStripMenuItem newFolderToolStripMenuItem;
        private System.Windows.Forms.Button check1;
        private System.Windows.Forms.Button check3;
        private System.Windows.Forms.Button check2;
        private System.Windows.Forms.Label id;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button changeName;
        private System.Windows.Forms.Button removeJudge;
        private System.Windows.Forms.Button newJudge;
        private System.Windows.Forms.Button upJudge;
        private System.Windows.Forms.Button downJudge;
        private System.Windows.Forms.ColumnHeader id_judge;
        private System.Windows.Forms.Label segmentName;
        private System.Windows.Forms.ColumnHeader country;
    }
}
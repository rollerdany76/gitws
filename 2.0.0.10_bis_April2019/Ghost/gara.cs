﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Threading;

namespace Ghost
{
    public partial class gara : Form
    {
        int idSegment = 0;
        public static bool cancel = true;
        public static int numPartecipanti = 0;
        public static string nomeGara = "";
        public static int idGaraParams = 0;

        public gara()
        {
            InitializeComponent();
            System.Windows.Forms.Form.CheckForIllegalCrossThreadCalls = false;
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            cancel = true;
            Close();
        }

        private void confirmBtn_Click(object sender, EventArgs e)
        {
            try
            {
                numPartecipanti = lb.Items.Count;
                nomeGara = name.Text;
                cancel = false;

                SQLiteCommand cmd = new SQLiteCommand("select MAX(ID_GaraParams) from GaraParams", Main.conn);
                SQLiteDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (dr[0].ToString().Equals("")) idGaraParams = 1;
                    else idGaraParams = dr.GetInt32(0) + 1;
                }
                using (SQLiteCommand command = Main.conn.CreateCommand())
                {
                    command.CommandText = "INSERT INTO GaraParams(ID_GaraParams,Name,Date,ID_Segment,ID_Specialita,Partecipants)" +
                        " VALUES(@param1,@param2,@param3,@param4,@param5,@param6)";
                    command.Parameters.AddWithValue("@param1", idGaraParams);
                    command.Parameters.AddWithValue("@param2", nomeGara);
                    command.Parameters.AddWithValue("@param3", DateTime.Now.ToString("dd/MM/yyyy"));
                    command.Parameters.AddWithValue("@param4", idSegment);
                    command.Parameters.AddWithValue("@param5", 1);
                    command.Parameters.AddWithValue("@param6", numPartecipanti);
                    command.ExecuteNonQuery();

                    command.CommandText = "INSERT INTO Participants(ID_GaraParams,Num,Nome,Cognome)" +
                        " VALUES(@param1,@param2,@param3,@param4)";
                    command.Parameters.AddWithValue("@param1", idGaraParams);
                    for (int i = 0; i < lb.Items.Count; i++)
                    {
                        command.Parameters.AddWithValue("@param2", lb.Items[i].SubItems[0].Text); // num
                        command.Parameters.AddWithValue("@param3", lb.Items[i].SubItems[1].Text); // nome
                        command.Parameters.AddWithValue("@param4", lb.Items[i].SubItems[2].Text); // surname
                        command.ExecuteNonQuery();
                    }
                }
                Close();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void namep_TextChanged(object sender, EventArgs e)
        {
            if (namep.TextLength != 0) add.Enabled = true;
        }

        private void surname_TextChanged(object sender, EventArgs e)
        {
            if (surname.TextLength != 0) add.Enabled = true;
        }

        private void add_Click(object sender, EventArgs e)
        {
            try
            {
                numPartecipanti++;
                int tempPart = lb.Items.Count;

                lb.Items.Add(new ListViewItem(new string[] { "", "", "" }, -1));
                lb.Items[tempPart].SubItems[0].Text = "" + (tempPart + 1);
                lb.Items[tempPart].SubItems[1].Text = namep.Text;
                lb.Items[tempPart].SubItems[2].Text = surname.Text;

                delete.Enabled = true;
                add.Enabled = false;
                namep.ResetText(); surname.ResetText();
                namep.Focus();
                if (numPartecipanti==1) particip.Text = numPartecipanti + " participant";
                else if (numPartecipanti == 0)
                {
                    particip.Text = "";
                    confirmBtn.Enabled = false;
                }
                else particip.Text = numPartecipanti + " participants";
                confirmBtn.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void delete_Click(object sender, EventArgs e)
        {
            try
            {
                if (lb.SelectedIndices.Count == 0) return;
                if (lb.Items.Count > 0)
                {
                    numPartecipanti--;
                    lb.Items.RemoveAt(lb.SelectedIndices[0]);
                    for (int i = 0; i < lb.Items.Count; i++)
                    {
                        lb.Items[i].SubItems[0].Text = "" + (i + 1);
                    }
                    delete.Enabled = true;
                    if (numPartecipanti == 1) particip.Text = numPartecipanti + " participant";
                    else if (numPartecipanti == 0)
                    {
                        particip.Text = "";
                        confirmBtn.Enabled = false;
                    }
                    else particip.Text = numPartecipanti + " participants";
                    namep.Focus();
                }
                else
                {
                    confirmBtn.Enabled = false;
                    delete.Enabled = false;
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void name_Enter(object sender, EventArgs e)
        {
            name.BackColor = Color.LemonChiffon;
        }

        private void name_Leave(object sender, EventArgs e)
        {
            name.BackColor = Color.White;
        }

        private void namep_Enter(object sender, EventArgs e)
        {
            namep.BackColor = Color.LemonChiffon;
        }

        private void namep_Leave(object sender, EventArgs e)
        {
            namep.BackColor = Color.White;
        }

        private void surname_Enter(object sender, EventArgs e)
        {
            surname.BackColor = Color.LemonChiffon;
        }

        private void surname_Leave(object sender, EventArgs e)
        {
            surname.BackColor = Color.White;
        }

        private void gara_Load(object sender, EventArgs e)
        {
            try
            {
                idSegment = Main.idSegment;
                this.Text = "Event number " + idSegment;
                name.Clear(); namep.Clear(); surname.Clear();
                lb.Items.Clear();
                numPartecipanti = 0;
                switch (idSegment)
                {
                    case 1:
                        title.Text = "SHORT PROGRAM Cat. Seniores"; 
                        break;
                    case 2:
                        title.Text = "LONG PROGRAM Cat. Seniores"; 
                        break;
                    default: 
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

    }
}

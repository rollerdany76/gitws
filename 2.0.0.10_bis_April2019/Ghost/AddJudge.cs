﻿using Ghost;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RollartSystemTech
{
    public partial class AddJudge : Form
    {
        Utility ut = null;
        string uffGiaInseriti = "";

        public AddJudge(string ufficiali)
        {
            InitializeComponent();
            uffGiaInseriti = ufficiali;
        }

        private void AddJudge_Load(object sender, EventArgs e)
        {
            try
            {
                ut = new Utility(null);
                ut.GetGiudici();
                if (Definizioni.giudici != null)
                {
                    var source = new AutoCompleteStringCollection();
                    string[] names = (string[])Definizioni.giudici.ToArray(typeof(string));
                    source.AddRange(names);
                    tp.AutoCompleteCustomSource = source;
                }

                foreach (string uff in uffGiaInseriti.Split(';'))
                {
                    if (uff.Equals("Controller")) rb2.Enabled = false;
                    else if (uff.Equals("Assistant")) rb3.Enabled = false;
                    else if (uff.Equals("Referee")) rb4.Enabled = false;
                }

                if (Definizioni.numJudges == 9)
                {
                    MessageBox.Show("9 judges already inserted!", "RollArt", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    rb1.Enabled = false;
                    if (rb2.Enabled) rb2.Checked = true;
                    else if (rb3.Enabled) rb3.Checked = true;
                    else if (rb4.Enabled) rb4.Checked = true;
                } else 
                    rb1.Text = "Judge " + (Definizioni.numJudges + 1);

                if (!rb1.Enabled && !rb2.Enabled && !rb3.Enabled && !rb4.Enabled)
                    Dispose();
            }
            catch (Exception)
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                err.Text = "";
                if (tp.TextLength == 0 || tp.Text.Trim().Length == 0)
                {
                    tp.BackColor = Color.AliceBlue;
                    tp.Focus();
                    return;
                }

                if (EventsForm.hasSpecialChar(tp.Text))
                {
                    tp.Text = "";
                    err.Text = "Characters not allowed";
                    tp.Focus();
                    return;
                }

                string role = "Judge " + (Definizioni.numJudges + 1);

                //// se ci sono già 9 giudici ed è stato scelto il giudice
                //if (Definizioni.numJudges == 9 && rb1.Checked)
                //{
                //    MessageBox.Show("9 judges already inserted!", "RollArt", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    Dispose();
                //}

                if (rb2.Checked) role = "Controller";
                else if (rb3.Checked) role = "Assistant";
                else if (rb4.Checked) role = "Referee";

                // giudice
                if (rb1.Checked)
                {
                    string idGiudice = "";
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "SELECT ID_Judge FROM Judges " +
                            " WHERE UPPER(Name) = '" + tp.Text.Replace("'", "''") + "'";
                        SQLiteDataReader dr = command.ExecuteReader();

                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                idGiudice = dr[0].ToString();
                            }
                        }
                        else
                        {
                            dr.Close();
                            command.CommandText = "INSERT INTO Judges(Name, Region, Country) " +
                                "VALUES ('" + tp.Text.Replace("'", "''") + "','','')";
                            try
                            {
                                command.ExecuteNonQuery();
                            }
                            catch (Exception)
                            {
                                // giudice già presente
                            }
                            // recupero l'ID del Giudice
                            command.CommandText = "SELECT ID_Judge FROM Judges " +
                                " WHERE UPPER(Name) = '" + tp.Text.Replace("'", "''") + "'";
                            dr = command.ExecuteReader();
                            while (dr.Read())
                            {
                                idGiudice = dr[0].ToString();
                            }
                        }
                        
                    }

                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "INSERT INTO PanelJudge(ID_Judge, ID_GaraParams,Role)" +
                            " VALUES(@param3,@param1,@param2)";
                        command.Parameters.AddWithValue("@param1", Definizioni.idGaraParams);
                        command.Parameters.AddWithValue("@param2", role); // role
                        command.Parameters.AddWithValue("@param3", idGiudice); // id
                        command.ExecuteNonQuery();
                    }

                
                    Definizioni.numJudges = Definizioni.numJudges + 1;

                    // aggiorno il numero di giudici totali per la gara corrente
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "UPDATE GaraParams SET NumJudges = '" + Definizioni.numJudges + "' WHERE " +
                                " ID_GaraParams = " + Definizioni.idGaraParams;
                        command.ExecuteNonQuery();
                    }
                } else
                {
                    string idGiudice = "";
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "SELECT ID_Judge FROM Judges " +
                            " WHERE UPPER(Name) = '" + tp.Text.Replace("'", "''") + "'";
                        SQLiteDataReader dr = command.ExecuteReader();

                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                idGiudice = dr[0].ToString();
                            }
                        }
                        else
                        {
                            dr.Close();
                            command.CommandText = "INSERT INTO Judges(Name, Region, Country) " +
                                "VALUES ('" + tp.Text.Replace("'", "''") + "','','')";
                            try
                            {
                                command.ExecuteNonQuery();
                            }
                            catch (Exception)
                            {
                                // giudice già presente
                            }
                            // recupero l'ID del Giudice
                            command.CommandText = "SELECT ID_Judge FROM Judges " +
                                " WHERE UPPER(Name) = '" + tp.Text.Replace("'", "''") + "'";
                            dr = command.ExecuteReader();
                            while (dr.Read())
                            {
                                idGiudice = dr[0].ToString();
                            }
                        }
                    }

                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "UPDATE PanelJudge SET ID_Judge = '" + idGiudice +
                            "' WHERE Role = '" + role + "' AND ID_GaraParams = " + Definizioni.idGaraParams;
                        
                        command.ExecuteNonQuery();
                    }
                }
                err.Text = "";
                Dispose();
            }
            catch (Exception ex)
            {
                err.Text = "";
                Dispose();
            }
        }
    }

}

﻿using Ghost;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RollartSystemTech
{
    public partial class SplashScreen : Form
    {
        bool flagClose = false;

        public SplashScreen()
        {
            InitializeComponent();
            versione.Text = //System.AppDomain.CurrentDomain.FriendlyName + ". " +
                    Application.ProductName + " vers. " + Application.ProductVersion;
        }

        public int Progress
        {
            get
            {
                return this.progressBar1.Value;
            }
            set
            {
                if (value < progressBar1.Maximum &&
                    value > progressBar1.Minimum)
                {
                    this.progressBar1.Value = value;
                }
            }

        }

        public string UpdateLabel
        {
            get
            {
                label1.Visible = true;
                return this.label1.Text;
            }
            set
            {

                this.label1.Text = value;
            }

        }

        public void InitializeProgress(int min, int max, int actual)
        {
            this.progressBar1.Visible = true;
            this.progressBar1.Minimum = min;
            this.progressBar1.Maximum = max;

            if (actual < progressBar1.Maximum &&
                 actual > progressBar1.Minimum)
            {
                this.progressBar1.Value = actual;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // *** Modifica del 05/12/2018 - 2.0.0.9 *** //
            flagClose = true;
            //Application.Exit();
            System.Environment.Exit(0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                msg.Visible = false;
                if (user.Text.Equals(""))
                {
                    msg.Visible = true;
                    msg.Text = "Insert Username";
                    user.Focus();
                    return;
                }
                if (password.Text.Equals(""))
                {
                    msg.Visible = true;
                    msg.Text = "Insert Password";
                    password.Focus();
                    return;
                }
                int ret = Utility.CheckPassword(user.Text, password.Text);
                if (ret == 0)
                {
                    msg.Visible = false;
                    label1.Visible = true;
                    panel1.Enabled = false;
                    flagClose = true;
                    Close();
                }
                else
                {
                    msg.Visible = true;
                    if (ret == 1)
                    {
                        msg.Text = "Username not found";
                        user.Focus();
                    }
                    else if (ret == 2)
                    {
                        msg.Text = "Password error. Retry";
                        password.Focus();
                    }
                    else if (ret == 3) msg.Text = "Database rolljudge2 error";
                }
            }
            catch (Exception)
            {
                 
            }
        }

        private void password_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                {
                    button1.PerformClick();
                }
            }
            catch (Exception)
            {

            }
        }

        private void SplashScreen_FormClosing(object sender, FormClosingEventArgs e)
        {
            // *** Modifica del 05/12/2018 - 2.0.0.9 *** //
            if (!flagClose)
                e.Cancel = true;
        }
    }
}

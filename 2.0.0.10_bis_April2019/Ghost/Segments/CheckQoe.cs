﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Ghost;
using System.Data.SQLite;
using System.Drawing.Imaging;
using System.Threading;

namespace Ghost
{
    public partial class CheckQoe : Form
    {
        SQLiteDataReader dr = null;
        ListView lvMain = null;
        Utility ut = null;
        bool flagClose = false;

        public CheckQoe(ListView lv, string comp)
        {
            InitializeComponent();
            try
            {
                competitor.Text = comp;
                lvQOE.Columns.Add("Final Value", 100);
                for (int i = 0; i < Definizioni.numJudges; i++) lvQOE.Columns.Add("J" + (i + 1), 50);
                lvMain = lv;
                ut = new Utility(null);

            } catch (Exception ex)
            {
                Utility.WriteLog("CheckQoe: " + ex.Message, "ERROR");
            }
        }

        public void FillListView()
        {
            try
            {
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    string query = "SELECT ValueFinal";
                    for (int i = 0; i < Definizioni.numJudges; i++) query += ",qoe_" + (i + 1);
                    query += " FROM Gara WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                        " AND ID_Segment = " + Definizioni.idSegment + " AND NumPartecipante = " +
                        Definizioni.currentPart + "";
                    command.CommandText = query;
                    dr = command.ExecuteReader();
                    int count = 0;
                    int numElementi = lvMain.Items.Count;
                    foreach (ListViewItem lvi in lvMain.Items)
                    {
                        dr.Read();
                        if (lvi.SubItems.Count == 8)
                        {
                            lvi.SubItems.Add("");
                        }
                        lvi.SubItems.Add(dr[0].ToString()); // valuefinal
                        for (int i = 0; i < Definizioni.numJudges; i++)
                            lvi.SubItems.Add(dr[i + 1].ToString()); // qoe's
                        lvQOE.Items.Add((ListViewItem)lvi.Clone());
                        if (!lvi.Text.Equals(""))
                            lvQOE.Items[count].ImageIndex = 0;
                        count++;
                        
                        lvQOE.Refresh();
                    }
                    lvQOE.Columns[3].Width = 100;
                    //lvQOE.Refresh();

                    //Thread.Sleep(500);

                    // Disabilito per gli elements il min e il max
                    
                }

                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    string query = "SELECT Element,ValueFinal";
                    for (int i = 0; i < Definizioni.numJudges; i++) query += ",qoev_" + (i + 1);
                    query += " FROM Gara WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                        " AND ID_Segment = " + Definizioni.idSegment + " AND NumPartecipante = " +
                        Definizioni.currentPart + " AND ProgressivoEl is null";
                    command.CommandText = query;
                    dr = command.ExecuteReader();
                    
                    // Components
                    for (int i = 0; i < Definizioni.numJudges; i++)
                        lvComp.Columns.Add("J" + (i + 1), 50);
                    int count = 0;
                    while (dr.Read())
                    {
                        if (count == 4) break; // salto il 5° component
                        lvComp.Items[count].SubItems.Add(dr[1].ToString()); // valuefinal
                        for (int i = 0; i < Definizioni.numJudges; i++)
                        {
                            lvComp.Items[count].SubItems.Add(dr[2 + i].ToString());
                        }
                        count++;
                    }
                    lvQOE.Refresh();

                    // Disabilito per i components il min e il max
                    decimal[] comps = new decimal[Definizioni.numJudges];
                    for (int k = 0; k < 4; k++)
                    {
                        for (int i = 0; i < Definizioni.numJudges; i++)
                        {
                            comps[i] = Decimal.Parse(
                                lvComp.Items[k].SubItems[3 + i].Text);
                        }
                        if (Definizioni.numJudges > 3)
                        {
                            int massimo = getMax(comps);
                            int minimo = getMin(comps);

                            lvComp.Items[k].UseItemStyleForSubItems = false;
                            lvComp.Items[k].SubItems[3 + massimo].ForeColor = Color.Gray;
                            lvComp.Items[k].SubItems[3 + minimo].ForeColor = Color.Gray;
                            
                        }
                    }
                    lvQOE.Refresh();

                    int numElementi = lvMain.Items.Count;
                    decimal[] elems = new decimal[Definizioni.numJudges];
                    for (int k = 0; k < numElementi; k++)
                    {
                        for (int i = 0; i < Definizioni.numJudges; i++)
                        {
                            elems[i] = Decimal.Parse(lvQOE.Items[k].SubItems[10 + i].Text);
                        }
                        if (Definizioni.numJudges > 3)
                        {
                            int massimo = getMax(elems);
                            int minimo = getMin(elems);

                            lvQOE.Items[k].UseItemStyleForSubItems = false;
                            lvQOE.Items[k].SubItems[10 + massimo].ForeColor = Color.Gray;
                            lvQOE.Items[k].SubItems[10 + minimo].ForeColor = Color.Gray;

                        }
                    }
                    lvQOE.Refresh();
                }

                

            }
            catch (Exception ex)
            {
                Utility.WriteLog("FillListView: " + ex.Message, "ERROR");
            }
        }

        public void FillForm()
        {
            try
            {
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    string query = "SELECT BaseTech,FinalTech,BaseArtistic,Deductions,Total" + //,Position" +
                    " FROM GaraFinal WHERE ID_GaraParams = " + Definizioni.idGaraParams +
                        " AND ID_Segment = " + Definizioni.idSegment + " AND NumPartecipante = " +
                        Definizioni.currentPart;
                    command.CommandText = query;
                    dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        elements.Text = String.Format("{0:0.00}", decimal.Parse(dr[0].ToString()));
                        elements2.Text = String.Format("{0:0.00}", decimal.Parse(dr[1].ToString()));
                        deductions.Text = String.Format("{0:0.00}", decimal.Parse(dr[3].ToString()));
                        artistic.Text = String.Format("{0:0.00}", decimal.Parse(dr[2].ToString()));
                        total.Text = String.Format("{0:0.00}", decimal.Parse(dr[4].ToString()));
                        //position.Text = dr[5].ToString();
                    }
                }
                using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                {
                    // modifica del 17/05/2016
                    // recupero posizione parziale generale e totale finale
                    command.CommandText = "SELECT TotGara, Position  FROM GaraTotal WHERE ID_GaraParams = "
                        + Definizioni.idGaraParams + " AND ID_Atleta = " + Definizioni.idAtleta;

                    dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        final.Text = String.Format("{0:0.00}", decimal.Parse(dr[0].ToString()));
                        position.Text = dr[1].ToString();
                    }
                    dr.Close();
                }
                //Utility.WriteLog("______________________________________________ ", "ERROR");
                Utility.WriteLog(competitor.Text.ToUpper() + "" +
                                 " BT: " + elements.Text +
                                 " FT: " + elements2.Text +
                                 " CM: " + artistic.Text +
                                 " DD: " + deductions.Text +
                                 " TS: " + total.Text +
                                 " TF: " + final.Text + 
                                 " RK: " + position.Text
                                 , "OK");
            }
            catch (Exception ex)
            {
                Utility.WriteLog("FillForm: " + ex.Message, "ERROR");
            }
        }

        private int getMax(decimal[] myArray)
        {
            decimal massimo = 0;
            int indiceMax = 0;
            for (int k = 0; k < myArray.Length; k++)
            {
                if (myArray[k] > massimo)
                {
                    massimo = myArray[k];
                    indiceMax = k;
                }
            }
            return indiceMax;
        }

        private int getMin(decimal[] myArray)
        {
            decimal minimo = 10;
            int indiceMin = 0;
            for (int k = 0; k < myArray.Length; k++)
            {
                if (myArray[k] < minimo)
                {
                    minimo = myArray[k];
                    indiceMin = k;
                }
            }
            return indiceMin;
        }

        private void CheckQoe_Load(object sender, EventArgs e)
        {
            
        }

        private void next_Click(object sender, EventArgs e)
        {
            try
            {
                flagClose = true;
                Dispose();
            }
            catch (Exception ex)
            {
                Utility.WriteLog("CreateBitmap: " + ex.Message, "ERROR");
                Dispose();
            }
            
        }

        private void lvQOE_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = lvQOE.Columns[e.ColumnIndex].Width;
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void CheckQoe_Shown(object sender, EventArgs e)
        {
            FillListView();

            FillForm();
        }

        private void CheckQoe_FormClosing(object sender, FormClosingEventArgs e)
        {
            // *** Modifica del 05/12/2018 - 2.0.0.9 *** //
            if (!flagClose)
                e.Cancel = true;
        }
    }
}

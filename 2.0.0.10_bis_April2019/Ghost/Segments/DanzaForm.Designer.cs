﻿using System;

namespace Ghost
{
    partial class DanzaForm
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DanzaForm));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.j1 = new System.Windows.Forms.Button();
            this.j2 = new System.Windows.Forms.Button();
            this.j3 = new System.Windows.Forms.Button();
            this.j4 = new System.Windows.Forms.Button();
            this.j5 = new System.Windows.Forms.Button();
            this.j6 = new System.Windows.Forms.Button();
            this.j7 = new System.Windows.Forms.Button();
            this.j8 = new System.Windows.Forms.Button();
            this.j9 = new System.Windows.Forms.Button();
            this.tp = new System.Windows.Forms.Button();
            this.startstop = new System.Windows.Forms.Button();
            this.ltimer = new System.Windows.Forms.Button();
            this.confirm = new System.Windows.Forms.Button();
            this.average = new System.Windows.Forms.Button();
            this.reset = new System.Windows.Forms.Button();
            this.prev = new System.Windows.Forms.Button();
            this.skip = new System.Windows.Forms.Button();
            this.info = new System.Windows.Forms.Button();
            this.quit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gbElements = new System.Windows.Forms.GroupBox();
            this.lv = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.note = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.magg = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.error = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cbVerify = new System.Windows.Forms.CheckBox();
            this.review = new System.Windows.Forms.Button();
            this.asterisco = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonNext = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.total = new System.Windows.Forms.Label();
            this.deductions = new System.Windows.Forms.Label();
            this.elements = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.labelPartial = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gbDed = new System.Windows.Forms.GroupBox();
            this.label34 = new System.Windows.Forms.Label();
            this.ded5 = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.ded3 = new System.Windows.Forms.NumericUpDown();
            this.ded4 = new System.Windows.Forms.NumericUpDown();
            this.label24 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.ded2 = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.ded1 = new System.Windows.Forms.NumericUpDown();
            this.panDance1 = new System.Windows.Forms.Panel();
            this.gbLifts = new System.Windows.Forms.GroupBox();
            this.el647 = new System.Windows.Forms.Button();
            this.el653 = new System.Windows.Forms.Button();
            this.el659 = new System.Windows.Forms.Button();
            this.el660 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.el641 = new System.Windows.Forms.Button();
            this.el269 = new System.Windows.Forms.Button();
            this.el264 = new System.Windows.Forms.Button();
            this.el259 = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.el268 = new System.Windows.Forms.Button();
            this.el263 = new System.Windows.Forms.Button();
            this.el258 = new System.Windows.Forms.Button();
            this.el267 = new System.Windows.Forms.Button();
            this.el266 = new System.Windows.Forms.Button();
            this.el265 = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.el262 = new System.Windows.Forms.Button();
            this.el261 = new System.Windows.Forms.Button();
            this.el260 = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.el257 = new System.Windows.Forms.Button();
            this.el256 = new System.Windows.Forms.Button();
            this.el254 = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.gbPattern = new System.Windows.Forms.GroupBox();
            this.el310 = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.el277 = new System.Windows.Forms.Button();
            this.el273 = new System.Windows.Forms.Button();
            this.el276 = new System.Windows.Forms.Button();
            this.el275 = new System.Windows.Forms.Button();
            this.el274 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.el272 = new System.Windows.Forms.Button();
            this.el271 = new System.Windows.Forms.Button();
            this.el270 = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.labPattern = new System.Windows.Forms.Label();
            this.panPrecision1 = new System.Windows.Forms.Panel();
            this.gbPivoting = new System.Windows.Forms.GroupBox();
            this.panBPivo = new System.Windows.Forms.Panel();
            this.el551 = new System.Windows.Forms.Button();
            this.el554 = new System.Windows.Forms.Button();
            this.el553 = new System.Windows.Forms.Button();
            this.el552 = new System.Windows.Forms.Button();
            this.el555 = new System.Windows.Forms.Button();
            this.el556 = new System.Windows.Forms.Button();
            this.label42 = new System.Windows.Forms.Label();
            this.panLPivo = new System.Windows.Forms.Panel();
            this.el557 = new System.Windows.Forms.Button();
            this.el560 = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.el559 = new System.Windows.Forms.Button();
            this.el558 = new System.Windows.Forms.Button();
            this.el561 = new System.Windows.Forms.Button();
            this.el562 = new System.Windows.Forms.Button();
            this.label44 = new System.Windows.Forms.Label();
            this.gbRotating = new System.Windows.Forms.GroupBox();
            this.panWRota = new System.Windows.Forms.Panel();
            this.el545 = new System.Windows.Forms.Button();
            this.el548 = new System.Windows.Forms.Button();
            this.el547 = new System.Windows.Forms.Button();
            this.el546 = new System.Windows.Forms.Button();
            this.el549 = new System.Windows.Forms.Button();
            this.el550 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.panCRota = new System.Windows.Forms.Panel();
            this.el539 = new System.Windows.Forms.Button();
            this.el542 = new System.Windows.Forms.Button();
            this.el541 = new System.Windows.Forms.Button();
            this.el540 = new System.Windows.Forms.Button();
            this.el543 = new System.Windows.Forms.Button();
            this.el544 = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.gbTravelling = new System.Windows.Forms.GroupBox();
            this.panCTrav = new System.Windows.Forms.Panel();
            this.el515 = new System.Windows.Forms.Button();
            this.el517 = new System.Windows.Forms.Button();
            this.el518 = new System.Windows.Forms.Button();
            this.el519 = new System.Windows.Forms.Button();
            this.el520 = new System.Windows.Forms.Button();
            this.el516 = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.panWTrav = new System.Windows.Forms.Panel();
            this.el521 = new System.Windows.Forms.Button();
            this.el524 = new System.Windows.Forms.Button();
            this.el523 = new System.Windows.Forms.Button();
            this.el525 = new System.Windows.Forms.Button();
            this.el526 = new System.Windows.Forms.Button();
            this.el522 = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panDance2 = new System.Windows.Forms.Panel();
            this.gbSteps = new System.Windows.Forms.GroupBox();
            this.panStepHold = new System.Windows.Forms.Panel();
            this.el308 = new System.Windows.Forms.Button();
            this.el291 = new System.Windows.Forms.Button();
            this.el292 = new System.Windows.Forms.Button();
            this.el293 = new System.Windows.Forms.Button();
            this.el294 = new System.Windows.Forms.Button();
            this.el295 = new System.Windows.Forms.Button();
            this.labNoHold = new System.Windows.Forms.Label();
            this.panChoreo = new System.Windows.Forms.Panel();
            this.el640 = new System.Windows.Forms.Button();
            this.el639 = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.panStraight = new System.Windows.Forms.Panel();
            this.el303 = new System.Windows.Forms.Button();
            this.el304 = new System.Windows.Forms.Button();
            this.el302 = new System.Windows.Forms.Button();
            this.el301 = new System.Windows.Forms.Button();
            this.el305 = new System.Windows.Forms.Button();
            this.el611 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.panCircular = new System.Windows.Forms.Panel();
            this.el306 = new System.Windows.Forms.Button();
            this.el252 = new System.Windows.Forms.Button();
            this.el251 = new System.Windows.Forms.Button();
            this.el250 = new System.Windows.Forms.Button();
            this.el249 = new System.Windows.Forms.Button();
            this.el253 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.panStepNoHold = new System.Windows.Forms.Panel();
            this.el309 = new System.Windows.Forms.Button();
            this.el290 = new System.Windows.Forms.Button();
            this.el287 = new System.Windows.Forms.Button();
            this.el288 = new System.Windows.Forms.Button();
            this.el289 = new System.Windows.Forms.Button();
            this.el286 = new System.Windows.Forms.Button();
            this.labHOld = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.gbSeq = new System.Windows.Forms.GroupBox();
            this.panNoHold = new System.Windows.Forms.Panel();
            this.el307 = new System.Windows.Forms.Button();
            this.el297 = new System.Windows.Forms.Button();
            this.el299 = new System.Windows.Forms.Button();
            this.el296 = new System.Windows.Forms.Button();
            this.el298 = new System.Windows.Forms.Button();
            this.lab1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panHold = new System.Windows.Forms.Panel();
            this.el634 = new System.Windows.Forms.Button();
            this.el328 = new System.Windows.Forms.Button();
            this.el330 = new System.Windows.Forms.Button();
            this.el327 = new System.Windows.Forms.Button();
            this.el329 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panTrav = new System.Windows.Forms.Panel();
            this.el312 = new System.Windows.Forms.Button();
            this.el245 = new System.Windows.Forms.Button();
            this.el248 = new System.Windows.Forms.Button();
            this.el246 = new System.Windows.Forms.Button();
            this.el247 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.panPrecision2 = new System.Windows.Forms.Panel();
            this.gbPrecision = new System.Windows.Forms.GroupBox();
            this.panCInters = new System.Windows.Forms.Panel();
            this.el563 = new System.Windows.Forms.Button();
            this.el564 = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.panPChoreo = new System.Windows.Forms.Panel();
            this.el513 = new System.Windows.Forms.Button();
            this.el514 = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.panBNH = new System.Windows.Forms.Panel();
            this.el500 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.el501 = new System.Windows.Forms.Button();
            this.el502 = new System.Windows.Forms.Button();
            this.el503 = new System.Windows.Forms.Button();
            this.el504 = new System.Windows.Forms.Button();
            this.el505 = new System.Windows.Forms.Button();
            this.panInters = new System.Windows.Forms.Panel();
            this.el506 = new System.Windows.Forms.Button();
            this.el507 = new System.Windows.Forms.Button();
            this.el508 = new System.Windows.Forms.Button();
            this.el509 = new System.Windows.Forms.Button();
            this.el510 = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.panComb = new System.Windows.Forms.Panel();
            this.el511 = new System.Windows.Forms.Button();
            this.el512 = new System.Windows.Forms.Button();
            this.label36 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.gbLinear = new System.Windows.Forms.GroupBox();
            this.panBLinear = new System.Windows.Forms.Panel();
            this.el528 = new System.Windows.Forms.Button();
            this.el527 = new System.Windows.Forms.Button();
            this.el529 = new System.Windows.Forms.Button();
            this.el532 = new System.Windows.Forms.Button();
            this.el530 = new System.Windows.Forms.Button();
            this.el531 = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.panLline = new System.Windows.Forms.Panel();
            this.el533 = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.el534 = new System.Windows.Forms.Button();
            this.el535 = new System.Windows.Forms.Button();
            this.el536 = new System.Windows.Forms.Button();
            this.el537 = new System.Windows.Forms.Button();
            this.el538 = new System.Windows.Forms.Button();
            this.log = new System.Windows.Forms.TextBox();
            this.tt2 = new System.Windows.Forms.ToolTip(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.gbElements.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gbDed.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ded5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded1)).BeginInit();
            this.panDance1.SuspendLayout();
            this.gbLifts.SuspendLayout();
            this.gbPattern.SuspendLayout();
            this.panPrecision1.SuspendLayout();
            this.gbPivoting.SuspendLayout();
            this.panBPivo.SuspendLayout();
            this.panLPivo.SuspendLayout();
            this.gbRotating.SuspendLayout();
            this.panWRota.SuspendLayout();
            this.panCRota.SuspendLayout();
            this.gbTravelling.SuspendLayout();
            this.panCTrav.SuspendLayout();
            this.panWTrav.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panDance2.SuspendLayout();
            this.gbSteps.SuspendLayout();
            this.panStepHold.SuspendLayout();
            this.panChoreo.SuspendLayout();
            this.panStraight.SuspendLayout();
            this.panCircular.SuspendLayout();
            this.panStepNoHold.SuspendLayout();
            this.gbSeq.SuspendLayout();
            this.panNoHold.SuspendLayout();
            this.panHold.SuspendLayout();
            this.panTrav.SuspendLayout();
            this.panPrecision2.SuspendLayout();
            this.gbPrecision.SuspendLayout();
            this.panCInters.SuspendLayout();
            this.panPChoreo.SuspendLayout();
            this.panBNH.SuspendLayout();
            this.panInters.SuspendLayout();
            this.panComb.SuspendLayout();
            this.gbLinear.SuspendLayout();
            this.panBLinear.SuspendLayout();
            this.panLline.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 2, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1213, 822);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.flowLayoutPanel1, 3);
            this.flowLayoutPanel1.Controls.Add(this.j1);
            this.flowLayoutPanel1.Controls.Add(this.j2);
            this.flowLayoutPanel1.Controls.Add(this.j3);
            this.flowLayoutPanel1.Controls.Add(this.j4);
            this.flowLayoutPanel1.Controls.Add(this.j5);
            this.flowLayoutPanel1.Controls.Add(this.j6);
            this.flowLayoutPanel1.Controls.Add(this.j7);
            this.flowLayoutPanel1.Controls.Add(this.j8);
            this.flowLayoutPanel1.Controls.Add(this.j9);
            this.flowLayoutPanel1.Controls.Add(this.tp);
            this.flowLayoutPanel1.Controls.Add(this.startstop);
            this.flowLayoutPanel1.Controls.Add(this.ltimer);
            this.flowLayoutPanel1.Controls.Add(this.confirm);
            this.flowLayoutPanel1.Controls.Add(this.average);
            this.flowLayoutPanel1.Controls.Add(this.reset);
            this.flowLayoutPanel1.Controls.Add(this.prev);
            this.flowLayoutPanel1.Controls.Add(this.skip);
            this.flowLayoutPanel1.Controls.Add(this.info);
            this.flowLayoutPanel1.Controls.Add(this.quit);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 3);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1209, 40);
            this.flowLayoutPanel1.TabIndex = 9;
            // 
            // j1
            // 
            this.j1.AutoEllipsis = true;
            this.j1.BackColor = System.Drawing.Color.Transparent;
            this.j1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.j1.Dock = System.Windows.Forms.DockStyle.Top;
            this.j1.Enabled = false;
            this.j1.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j1.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j1.ForeColor = System.Drawing.Color.Gray;
            this.j1.Location = new System.Drawing.Point(0, 0);
            this.j1.Margin = new System.Windows.Forms.Padding(0);
            this.j1.Name = "j1";
            this.j1.Size = new System.Drawing.Size(40, 40);
            this.j1.TabIndex = 76;
            this.j1.Tag = "1";
            this.j1.Text = "J1";
            this.j1.UseVisualStyleBackColor = false;
            this.j1.Click += new System.EventHandler(this.j1_Click_1);
            this.j1.MouseEnter += new System.EventHandler(this.j1_MouseEnter);
            // 
            // j2
            // 
            this.j2.BackColor = System.Drawing.Color.Transparent;
            this.j2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.j2.Dock = System.Windows.Forms.DockStyle.Top;
            this.j2.Enabled = false;
            this.j2.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j2.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j2.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j2.ForeColor = System.Drawing.Color.Gray;
            this.j2.Location = new System.Drawing.Point(40, 0);
            this.j2.Margin = new System.Windows.Forms.Padding(0);
            this.j2.Name = "j2";
            this.j2.Size = new System.Drawing.Size(40, 40);
            this.j2.TabIndex = 77;
            this.j2.Tag = "2";
            this.j2.Text = "J2";
            this.j2.UseVisualStyleBackColor = false;
            this.j2.Click += new System.EventHandler(this.j1_Click_1);
            this.j2.MouseEnter += new System.EventHandler(this.j2_MouseEnter);
            // 
            // j3
            // 
            this.j3.BackColor = System.Drawing.Color.Transparent;
            this.j3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.j3.Dock = System.Windows.Forms.DockStyle.Top;
            this.j3.Enabled = false;
            this.j3.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j3.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j3.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j3.ForeColor = System.Drawing.Color.Gray;
            this.j3.Location = new System.Drawing.Point(80, 0);
            this.j3.Margin = new System.Windows.Forms.Padding(0);
            this.j3.Name = "j3";
            this.j3.Size = new System.Drawing.Size(40, 40);
            this.j3.TabIndex = 78;
            this.j3.Tag = "3";
            this.j3.Text = "J3";
            this.j3.UseVisualStyleBackColor = false;
            this.j3.Click += new System.EventHandler(this.j1_Click_1);
            this.j3.MouseEnter += new System.EventHandler(this.j3_MouseEnter);
            // 
            // j4
            // 
            this.j4.BackColor = System.Drawing.Color.Transparent;
            this.j4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.j4.Dock = System.Windows.Forms.DockStyle.Top;
            this.j4.Enabled = false;
            this.j4.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j4.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j4.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j4.ForeColor = System.Drawing.Color.Gray;
            this.j4.Location = new System.Drawing.Point(120, 0);
            this.j4.Margin = new System.Windows.Forms.Padding(0);
            this.j4.Name = "j4";
            this.j4.Size = new System.Drawing.Size(40, 40);
            this.j4.TabIndex = 79;
            this.j4.Tag = "1";
            this.j4.Text = "J4";
            this.j4.UseVisualStyleBackColor = false;
            this.j4.Click += new System.EventHandler(this.j1_Click_1);
            this.j4.MouseEnter += new System.EventHandler(this.j4_MouseEnter);
            // 
            // j5
            // 
            this.j5.BackColor = System.Drawing.Color.Transparent;
            this.j5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.j5.Dock = System.Windows.Forms.DockStyle.Top;
            this.j5.Enabled = false;
            this.j5.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j5.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j5.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j5.ForeColor = System.Drawing.Color.Gray;
            this.j5.Location = new System.Drawing.Point(160, 0);
            this.j5.Margin = new System.Windows.Forms.Padding(0);
            this.j5.Name = "j5";
            this.j5.Size = new System.Drawing.Size(40, 40);
            this.j5.TabIndex = 80;
            this.j5.Tag = "2";
            this.j5.Text = "J5";
            this.j5.UseVisualStyleBackColor = false;
            this.j5.Click += new System.EventHandler(this.j1_Click_1);
            this.j5.MouseEnter += new System.EventHandler(this.j5_MouseEnter);
            // 
            // j6
            // 
            this.j6.BackColor = System.Drawing.Color.Transparent;
            this.j6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.j6.Dock = System.Windows.Forms.DockStyle.Top;
            this.j6.Enabled = false;
            this.j6.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j6.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j6.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j6.ForeColor = System.Drawing.Color.Gray;
            this.j6.Location = new System.Drawing.Point(200, 0);
            this.j6.Margin = new System.Windows.Forms.Padding(0);
            this.j6.Name = "j6";
            this.j6.Size = new System.Drawing.Size(40, 40);
            this.j6.TabIndex = 81;
            this.j6.Tag = "3";
            this.j6.Text = "J6";
            this.j6.UseVisualStyleBackColor = false;
            this.j6.Click += new System.EventHandler(this.j1_Click_1);
            this.j6.MouseEnter += new System.EventHandler(this.j6_MouseEnter);
            // 
            // j7
            // 
            this.j7.BackColor = System.Drawing.Color.Transparent;
            this.j7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.j7.Dock = System.Windows.Forms.DockStyle.Top;
            this.j7.Enabled = false;
            this.j7.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j7.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j7.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j7.ForeColor = System.Drawing.Color.Gray;
            this.j7.Location = new System.Drawing.Point(240, 0);
            this.j7.Margin = new System.Windows.Forms.Padding(0);
            this.j7.Name = "j7";
            this.j7.Size = new System.Drawing.Size(40, 40);
            this.j7.TabIndex = 82;
            this.j7.Tag = "1";
            this.j7.Text = "J7";
            this.j7.UseVisualStyleBackColor = false;
            this.j7.Click += new System.EventHandler(this.j1_Click_1);
            this.j7.MouseEnter += new System.EventHandler(this.j7_MouseEnter);
            // 
            // j8
            // 
            this.j8.BackColor = System.Drawing.Color.Transparent;
            this.j8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.j8.Dock = System.Windows.Forms.DockStyle.Top;
            this.j8.Enabled = false;
            this.j8.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j8.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j8.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j8.ForeColor = System.Drawing.Color.Gray;
            this.j8.Location = new System.Drawing.Point(280, 0);
            this.j8.Margin = new System.Windows.Forms.Padding(0);
            this.j8.Name = "j8";
            this.j8.Size = new System.Drawing.Size(40, 40);
            this.j8.TabIndex = 83;
            this.j8.Tag = "2";
            this.j8.Text = "J8";
            this.j8.UseVisualStyleBackColor = false;
            this.j8.Click += new System.EventHandler(this.j1_Click_1);
            this.j8.MouseEnter += new System.EventHandler(this.j8_MouseEnter);
            // 
            // j9
            // 
            this.j9.BackColor = System.Drawing.Color.Transparent;
            this.j9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.j9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.j9.Dock = System.Windows.Forms.DockStyle.Top;
            this.j9.Enabled = false;
            this.j9.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.j9.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.j9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.j9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.j9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.j9.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j9.ForeColor = System.Drawing.Color.Gray;
            this.j9.Location = new System.Drawing.Point(320, 0);
            this.j9.Margin = new System.Windows.Forms.Padding(0);
            this.j9.Name = "j9";
            this.j9.Size = new System.Drawing.Size(40, 40);
            this.j9.TabIndex = 84;
            this.j9.Tag = "3";
            this.j9.Text = "J9";
            this.j9.UseVisualStyleBackColor = false;
            this.j9.Click += new System.EventHandler(this.j1_Click_1);
            this.j9.MouseEnter += new System.EventHandler(this.j9_MouseEnter);
            // 
            // tp
            // 
            this.tp.BackColor = System.Drawing.Color.Transparent;
            this.tp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tp.Cursor = System.Windows.Forms.Cursors.Default;
            this.tp.Dock = System.Windows.Forms.DockStyle.Top;
            this.tp.FlatAppearance.BorderColor = System.Drawing.Color.Aqua;
            this.tp.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.tp.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.tp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.tp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tp.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp.ForeColor = System.Drawing.Color.Aqua;
            this.tp.Location = new System.Drawing.Point(362, 0);
            this.tp.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.tp.Name = "tp";
            this.tp.Size = new System.Drawing.Size(45, 40);
            this.tp.TabIndex = 88;
            this.tp.Tag = "3";
            this.tp.Text = "R";
            this.tp.UseVisualStyleBackColor = false;
            this.tp.Visible = false;
            // 
            // startstop
            // 
            this.startstop.BackColor = System.Drawing.Color.DimGray;
            this.startstop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.startstop.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.startstop.FlatAppearance.BorderSize = 2;
            this.startstop.FlatAppearance.MouseDownBackColor = System.Drawing.Color.YellowGreen;
            this.startstop.FlatAppearance.MouseOverBackColor = System.Drawing.Color.ForestGreen;
            this.startstop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.startstop.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startstop.ForeColor = System.Drawing.Color.White;
            this.startstop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.startstop.Location = new System.Drawing.Point(409, 0);
            this.startstop.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.startstop.Name = "startstop";
            this.startstop.Size = new System.Drawing.Size(90, 40);
            this.startstop.TabIndex = 90;
            this.startstop.Text = "START";
            this.startstop.UseVisualStyleBackColor = false;
            this.startstop.Click += new System.EventHandler(this.startstop_Click);
            // 
            // ltimer
            // 
            this.ltimer.BackColor = System.Drawing.Color.Transparent;
            this.ltimer.Cursor = System.Windows.Forms.Cursors.Default;
            this.ltimer.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.ltimer.FlatAppearance.BorderSize = 2;
            this.ltimer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ltimer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ltimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ltimer.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ltimer.ForeColor = System.Drawing.Color.Magenta;
            this.ltimer.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.ltimer.Location = new System.Drawing.Point(501, 0);
            this.ltimer.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.ltimer.Name = "ltimer";
            this.ltimer.Size = new System.Drawing.Size(80, 40);
            this.ltimer.TabIndex = 98;
            this.ltimer.Text = "00:00";
            this.ltimer.UseVisualStyleBackColor = false;
            // 
            // confirm
            // 
            this.confirm.BackColor = System.Drawing.Color.DimGray;
            this.confirm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.confirm.Enabled = false;
            this.confirm.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.confirm.FlatAppearance.BorderSize = 2;
            this.confirm.FlatAppearance.MouseOverBackColor = System.Drawing.Color.ForestGreen;
            this.confirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.confirm.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirm.ForeColor = System.Drawing.Color.White;
            this.confirm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.confirm.Location = new System.Drawing.Point(583, 0);
            this.confirm.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.confirm.Name = "confirm";
            this.confirm.Size = new System.Drawing.Size(127, 40);
            this.confirm.TabIndex = 95;
            this.confirm.Text = "CONFIRM";
            this.confirm.UseVisualStyleBackColor = false;
            this.confirm.Click += new System.EventHandler(this.button2_Click);
            // 
            // average
            // 
            this.average.BackColor = System.Drawing.Color.DimGray;
            this.average.Cursor = System.Windows.Forms.Cursors.Hand;
            this.average.Enabled = false;
            this.average.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.average.FlatAppearance.BorderSize = 2;
            this.average.FlatAppearance.MouseOverBackColor = System.Drawing.Color.ForestGreen;
            this.average.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.average.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.average.ForeColor = System.Drawing.Color.White;
            this.average.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.average.Location = new System.Drawing.Point(712, 0);
            this.average.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.average.Name = "average";
            this.average.Size = new System.Drawing.Size(126, 40);
            this.average.TabIndex = 111;
            this.average.Text = "AVERAGE";
            this.average.UseVisualStyleBackColor = false;
            this.average.Click += new System.EventHandler(this.average_Click);
            // 
            // reset
            // 
            this.reset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.reset.Cursor = System.Windows.Forms.Cursors.Hand;
            this.reset.Enabled = false;
            this.reset.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.reset.FlatAppearance.BorderSize = 2;
            this.reset.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.reset.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.reset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.reset.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reset.ForeColor = System.Drawing.Color.White;
            this.reset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.reset.Location = new System.Drawing.Point(840, 0);
            this.reset.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.reset.Name = "reset";
            this.reset.Size = new System.Drawing.Size(85, 40);
            this.reset.TabIndex = 109;
            this.reset.Text = "RESET";
            this.reset.UseVisualStyleBackColor = false;
            this.reset.Click += new System.EventHandler(this.reset_Click);
            // 
            // prev
            // 
            this.prev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prev.BackColor = System.Drawing.Color.White;
            this.prev.Cursor = System.Windows.Forms.Cursors.Hand;
            this.prev.Enabled = false;
            this.prev.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.prev.FlatAppearance.BorderSize = 2;
            this.prev.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.prev.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.prev.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.prev.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prev.ForeColor = System.Drawing.Color.White;
            this.prev.Image = global::RollartSystemTech.Properties.Resources.display;
            this.prev.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.prev.Location = new System.Drawing.Point(927, 0);
            this.prev.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.prev.Name = "prev";
            this.prev.Size = new System.Drawing.Size(49, 40);
            this.prev.TabIndex = 112;
            this.tt2.SetToolTip(this.prev, "Display the previous skater score");
            this.prev.UseVisualStyleBackColor = false;
            this.prev.Click += new System.EventHandler(this.prev_Click);
            // 
            // skip
            // 
            this.skip.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.skip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.skip.Cursor = System.Windows.Forms.Cursors.Hand;
            this.skip.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.skip.FlatAppearance.BorderSize = 2;
            this.skip.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Navy;
            this.skip.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Navy;
            this.skip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.skip.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.skip.ForeColor = System.Drawing.Color.White;
            this.skip.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.skip.Location = new System.Drawing.Point(978, 0);
            this.skip.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.skip.Name = "skip";
            this.skip.Size = new System.Drawing.Size(77, 40);
            this.skip.TabIndex = 107;
            this.skip.Text = "SKIP";
            this.tt2.SetToolTip(this.skip, "Skip to the next skater");
            this.skip.UseVisualStyleBackColor = false;
            this.skip.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // info
            // 
            this.info.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.info.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.info.Cursor = System.Windows.Forms.Cursors.Hand;
            this.info.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.info.FlatAppearance.BorderSize = 2;
            this.info.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Purple;
            this.info.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Purple;
            this.info.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.info.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.info.ForeColor = System.Drawing.Color.White;
            this.info.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.info.Location = new System.Drawing.Point(1057, 0);
            this.info.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(70, 40);
            this.info.TabIndex = 106;
            this.info.Text = "INFO";
            this.tt2.SetToolTip(this.info, "Event Info");
            this.info.UseVisualStyleBackColor = false;
            this.info.Click += new System.EventHandler(this.button1_Click_3);
            // 
            // quit
            // 
            this.quit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.quit.BackColor = System.Drawing.Color.Red;
            this.quit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.quit.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.quit.FlatAppearance.BorderSize = 2;
            this.quit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.quit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Maroon;
            this.quit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.quit.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quit.ForeColor = System.Drawing.Color.White;
            this.quit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.quit.Location = new System.Drawing.Point(1129, 0);
            this.quit.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.quit.Name = "quit";
            this.quit.Size = new System.Drawing.Size(69, 40);
            this.quit.TabIndex = 108;
            this.quit.Text = "QUIT";
            this.quit.UseVisualStyleBackColor = false;
            this.quit.Click += new System.EventHandler(this.button2_Click_2);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.gbElements);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(365, 46);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(481, 719);
            this.panel1.TabIndex = 3;
            // 
            // gbElements
            // 
            this.gbElements.Controls.Add(this.lv);
            this.gbElements.Controls.Add(this.error);
            this.gbElements.Controls.Add(this.panel3);
            this.gbElements.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbElements.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbElements.Location = new System.Drawing.Point(0, 0);
            this.gbElements.Name = "gbElements";
            this.gbElements.Size = new System.Drawing.Size(481, 719);
            this.gbElements.TabIndex = 1;
            this.gbElements.TabStop = false;
            // 
            // lv
            // 
            this.lv.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.lv.BackColor = System.Drawing.Color.Black;
            this.lv.BackgroundImageTiled = true;
            this.lv.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.note,
            this.magg});
            this.lv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lv.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv.ForeColor = System.Drawing.Color.White;
            this.lv.FullRowSelect = true;
            this.lv.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lv.Location = new System.Drawing.Point(3, 30);
            this.lv.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.lv.MultiSelect = false;
            this.lv.Name = "lv";
            this.lv.ShowItemToolTips = true;
            this.lv.Size = new System.Drawing.Size(475, 587);
            this.lv.SmallImageList = this.imageList1;
            this.lv.TabIndex = 95;
            this.lv.UseCompatibleStateImageBehavior = false;
            this.lv.View = System.Windows.Forms.View.Details;
            this.lv.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.lv_ColumnWidthChanging);
            this.lv.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lv_MouseClick);
            this.lv.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lv_MouseDoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "";
            this.columnHeader1.Width = 58;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Element";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 100;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Value";
            this.columnHeader3.Width = 63;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Type";
            this.columnHeader4.Width = 106;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "CAT";
            this.columnHeader5.Width = 0;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "NUMCOMBO";
            this.columnHeader6.Width = 0;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "D/U";
            this.columnHeader7.Width = 0;
            // 
            // note
            // 
            this.note.Text = "Note";
            this.note.Width = 92;
            // 
            // magg
            // 
            this.magg.Text = "Time";
            this.magg.Width = 0;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "bullet_green.png");
            this.imageList1.Images.SetKeyName(1, "bullet_red.png");
            this.imageList1.Images.SetKeyName(2, "bullet_yellow.png");
            // 
            // error
            // 
            this.error.AutoEllipsis = true;
            this.error.BackColor = System.Drawing.Color.Yellow;
            this.error.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.error.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.error.Font = new System.Drawing.Font("Arial Narrow", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.error.ForeColor = System.Drawing.Color.Red;
            this.error.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.error.Location = new System.Drawing.Point(3, 617);
            this.error.Name = "error";
            this.error.Size = new System.Drawing.Size(475, 57);
            this.error.TabIndex = 94;
            this.error.Text = "error test";
            this.error.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.error.Visible = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.cbVerify);
            this.panel3.Controls.Add(this.review);
            this.panel3.Controls.Add(this.asterisco);
            this.panel3.Controls.Add(this.cancel);
            this.panel3.Controls.Add(this.back);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(3, 674);
            this.panel3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(475, 42);
            this.panel3.TabIndex = 92;
            // 
            // cbVerify
            // 
            this.cbVerify.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbVerify.BackColor = System.Drawing.Color.Gainsboro;
            this.cbVerify.Checked = true;
            this.cbVerify.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbVerify.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbVerify.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbVerify.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.cbVerify.FlatAppearance.BorderSize = 2;
            this.cbVerify.FlatAppearance.CheckedBackColor = System.Drawing.Color.Lime;
            this.cbVerify.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Yellow;
            this.cbVerify.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Lime;
            this.cbVerify.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbVerify.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVerify.ForeColor = System.Drawing.Color.Black;
            this.cbVerify.Location = new System.Drawing.Point(245, 0);
            this.cbVerify.Name = "cbVerify";
            this.cbVerify.Size = new System.Drawing.Size(103, 42);
            this.cbVerify.TabIndex = 177;
            this.cbVerify.Text = "Check";
            this.cbVerify.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbVerify.UseVisualStyleBackColor = false;
            // 
            // review
            // 
            this.review.BackColor = System.Drawing.Color.DarkRed;
            this.review.Cursor = System.Windows.Forms.Cursors.Hand;
            this.review.Dock = System.Windows.Forms.DockStyle.Right;
            this.review.Enabled = false;
            this.review.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.review.FlatAppearance.BorderSize = 2;
            this.review.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.review.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.review.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.review.ForeColor = System.Drawing.Color.Yellow;
            this.review.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.review.Location = new System.Drawing.Point(348, 0);
            this.review.Name = "review";
            this.review.Size = new System.Drawing.Size(95, 42);
            this.review.TabIndex = 178;
            this.review.Text = "Review";
            this.review.UseVisualStyleBackColor = false;
            this.review.Click += new System.EventHandler(this.edit_Click);
            // 
            // asterisco
            // 
            this.asterisco.BackColor = System.Drawing.Color.Blue;
            this.asterisco.Cursor = System.Windows.Forms.Cursors.Hand;
            this.asterisco.Dock = System.Windows.Forms.DockStyle.Right;
            this.asterisco.Enabled = false;
            this.asterisco.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.asterisco.FlatAppearance.BorderSize = 2;
            this.asterisco.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.asterisco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.asterisco.Font = new System.Drawing.Font("Arial Narrow", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.asterisco.ForeColor = System.Drawing.Color.Yellow;
            this.asterisco.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.asterisco.Location = new System.Drawing.Point(443, 0);
            this.asterisco.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.asterisco.Name = "asterisco";
            this.asterisco.Size = new System.Drawing.Size(32, 42);
            this.asterisco.TabIndex = 180;
            this.asterisco.Text = "*";
            this.tt2.SetToolTip(this.asterisco, "Add/Remove * to the selected element");
            this.asterisco.UseVisualStyleBackColor = false;
            this.asterisco.Click += new System.EventHandler(this.asterisco_Click);
            // 
            // cancel
            // 
            this.cancel.BackColor = System.Drawing.Color.DimGray;
            this.cancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cancel.Dock = System.Windows.Forms.DockStyle.Left;
            this.cancel.Enabled = false;
            this.cancel.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.cancel.FlatAppearance.BorderSize = 2;
            this.cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancel.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancel.ForeColor = System.Drawing.Color.White;
            this.cancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cancel.Location = new System.Drawing.Point(146, 0);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(99, 42);
            this.cancel.TabIndex = 96;
            this.cancel.Text = "Cancel";
            this.cancel.UseVisualStyleBackColor = false;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // back
            // 
            this.back.BackColor = System.Drawing.Color.DimGray;
            this.back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.back.Dock = System.Windows.Forms.DockStyle.Left;
            this.back.Enabled = false;
            this.back.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.back.FlatAppearance.BorderSize = 2;
            this.back.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.back.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.back.ForeColor = System.Drawing.Color.White;
            this.back.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.back.Location = new System.Drawing.Point(0, 0);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(146, 42);
            this.back.TabIndex = 94;
            this.back.Text = "Delete Last";
            this.back.UseVisualStyleBackColor = false;
            this.back.Click += new System.EventHandler(this.clear_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 3);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 689F));
            this.tableLayoutPanel2.Controls.Add(this.buttonNext, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel5, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelPartial, 4, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 771);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel1.SetRowSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1207, 48);
            this.tableLayoutPanel2.TabIndex = 10;
            // 
            // buttonNext
            // 
            this.buttonNext.AutoEllipsis = true;
            this.buttonNext.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonNext.BackColor = System.Drawing.Color.DarkSlateGray;
            this.buttonNext.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonNext.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.buttonNext.FlatAppearance.BorderSize = 0;
            this.buttonNext.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonNext.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNext.Font = new System.Drawing.Font("Arial Narrow", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNext.ForeColor = System.Drawing.Color.Yellow;
            this.buttonNext.Location = new System.Drawing.Point(364, 0);
            this.buttonNext.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(639, 48);
            this.buttonNext.TabIndex = 105;
            this.buttonNext.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonNext.UseVisualStyleBackColor = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.total);
            this.panel5.Controls.Add(this.deductions);
            this.panel5.Controls.Add(this.elements);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(356, 42);
            this.panel5.TabIndex = 101;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(5, 24);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 20);
            this.label13.TabIndex = 93;
            this.label13.Text = "Base technical";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // total
            // 
            this.total.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.total.AutoSize = true;
            this.total.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total.ForeColor = System.Drawing.Color.Lime;
            this.total.Location = new System.Drawing.Point(170, 1);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(51, 29);
            this.total.TabIndex = 96;
            this.total.Text = "0.00";
            this.total.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // deductions
            // 
            this.deductions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.deductions.AutoSize = true;
            this.deductions.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deductions.ForeColor = System.Drawing.Color.White;
            this.deductions.Location = new System.Drawing.Point(98, 1);
            this.deductions.Name = "deductions";
            this.deductions.Size = new System.Drawing.Size(51, 29);
            this.deductions.TabIndex = 95;
            this.deductions.Text = "0.00";
            this.deductions.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // elements
            // 
            this.elements.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.elements.AutoSize = true;
            this.elements.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.elements.ForeColor = System.Drawing.Color.White;
            this.elements.Location = new System.Drawing.Point(20, 1);
            this.elements.Name = "elements";
            this.elements.Size = new System.Drawing.Size(51, 29);
            this.elements.TabIndex = 94;
            this.elements.Text = "0.00";
            this.elements.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Lime;
            this.label11.Location = new System.Drawing.Point(173, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 22);
            this.label11.TabIndex = 91;
            this.label11.Text = "Total";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(91, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 20);
            this.label12.TabIndex = 92;
            this.label12.Text = "Deductions";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPartial
            // 
            this.labelPartial.AutoSize = true;
            this.labelPartial.BackColor = System.Drawing.Color.Transparent;
            this.labelPartial.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPartial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelPartial.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPartial.ForeColor = System.Drawing.Color.Aqua;
            this.labelPartial.Location = new System.Drawing.Point(1007, 0);
            this.labelPartial.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPartial.Name = "labelPartial";
            this.labelPartial.Size = new System.Drawing.Size(127, 48);
            this.labelPartial.TabIndex = 233;
            this.labelPartial.Tag = "6";
            this.labelPartial.Text = "Partial RANK:\r\nPartial SCORE:";
            this.labelPartial.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gbDed);
            this.panel2.Controls.Add(this.panDance1);
            this.panel2.Controls.Add(this.panPrecision1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 46);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(357, 719);
            this.panel2.TabIndex = 11;
            // 
            // gbDed
            // 
            this.gbDed.Controls.Add(this.label34);
            this.gbDed.Controls.Add(this.ded5);
            this.gbDed.Controls.Add(this.label2);
            this.gbDed.Controls.Add(this.ded3);
            this.gbDed.Controls.Add(this.ded4);
            this.gbDed.Controls.Add(this.label24);
            this.gbDed.Controls.Add(this.label10);
            this.gbDed.Controls.Add(this.label22);
            this.gbDed.Controls.Add(this.ded2);
            this.gbDed.Controls.Add(this.label23);
            this.gbDed.Controls.Add(this.ded1);
            this.gbDed.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gbDed.Location = new System.Drawing.Point(0, 549);
            this.gbDed.Name = "gbDed";
            this.gbDed.Size = new System.Drawing.Size(357, 170);
            this.gbDed.TabIndex = 4;
            this.gbDed.TabStop = false;
            // 
            // label34
            // 
            this.label34.AutoEllipsis = true;
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Black;
            this.label34.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Red;
            this.label34.Location = new System.Drawing.Point(5, 17);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(175, 36);
            this.label34.TabIndex = 129;
            this.label34.Text = "Deductions";
            // 
            // ded5
            // 
            this.ded5.BackColor = System.Drawing.Color.Black;
            this.ded5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ded5.Font = new System.Drawing.Font("Arial Narrow", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded5.ForeColor = System.Drawing.Color.Red;
            this.ded5.Location = new System.Drawing.Point(281, 136);
            this.ded5.Name = "ded5";
            this.ded5.Size = new System.Drawing.Size(48, 25);
            this.ded5.TabIndex = 171;
            this.ded5.ValueChanged += new System.EventHandler(this.ded5_ValueChanged);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(94, 134);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(175, 23);
            this.label2.TabIndex = 166;
            this.label2.Text = "COSTUME VIOLATION";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ded3
            // 
            this.ded3.BackColor = System.Drawing.Color.Black;
            this.ded3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ded3.Font = new System.Drawing.Font("Arial Narrow", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded3.ForeColor = System.Drawing.Color.Red;
            this.ded3.Location = new System.Drawing.Point(281, 84);
            this.ded3.Name = "ded3";
            this.ded3.Size = new System.Drawing.Size(48, 25);
            this.ded3.TabIndex = 169;
            this.ded3.ValueChanged += new System.EventHandler(this.ded3_ValueChanged);
            // 
            // ded4
            // 
            this.ded4.BackColor = System.Drawing.Color.Black;
            this.ded4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ded4.Font = new System.Drawing.Font("Arial Narrow", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded4.ForeColor = System.Drawing.Color.Red;
            this.ded4.Location = new System.Drawing.Point(281, 58);
            this.ded4.Name = "ded4";
            this.ded4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ded4.Size = new System.Drawing.Size(48, 25);
            this.ded4.TabIndex = 170;
            this.ded4.ValueChanged += new System.EventHandler(this.ded4_ValueChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label24.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.White;
            this.label24.Location = new System.Drawing.Point(193, 29);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(76, 29);
            this.label24.TabIndex = 162;
            this.label24.Tag = "6";
            this.label24.Text = "FALLS";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label10.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(15, 57);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label10.Size = new System.Drawing.Size(254, 23);
            this.label10.TabIndex = 165;
            this.label10.Text = "ILLEGAL/MISSING ELEMENT";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label22.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(134, 82);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(135, 23);
            this.label22.TabIndex = 164;
            this.label22.Text = "TIME VIOLATION";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ded2
            // 
            this.ded2.BackColor = System.Drawing.Color.Black;
            this.ded2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ded2.DecimalPlaces = 1;
            this.ded2.Font = new System.Drawing.Font("Arial Narrow", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded2.ForeColor = System.Drawing.Color.Red;
            this.ded2.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.ded2.Location = new System.Drawing.Point(281, 110);
            this.ded2.Name = "ded2";
            this.ded2.Size = new System.Drawing.Size(48, 25);
            this.ded2.TabIndex = 168;
            this.ded2.ValueChanged += new System.EventHandler(this.ded2_ValueChanged);
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label23.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(122, 108);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(147, 23);
            this.label23.TabIndex = 163;
            this.label23.Text = "MUSIC VIOLATION";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ded1
            // 
            this.ded1.BackColor = System.Drawing.Color.Black;
            this.ded1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ded1.Font = new System.Drawing.Font("Arial Narrow", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded1.ForeColor = System.Drawing.Color.Red;
            this.ded1.Location = new System.Drawing.Point(281, 31);
            this.ded1.Name = "ded1";
            this.ded1.Size = new System.Drawing.Size(48, 25);
            this.ded1.TabIndex = 167;
            this.ded1.ValueChanged += new System.EventHandler(this.ded1_ValueChanged);
            // 
            // panDance1
            // 
            this.panDance1.Controls.Add(this.gbLifts);
            this.panDance1.Controls.Add(this.gbPattern);
            this.panDance1.Location = new System.Drawing.Point(3, 13);
            this.panDance1.Name = "panDance1";
            this.panDance1.Size = new System.Drawing.Size(356, 445);
            this.panDance1.TabIndex = 10;
            this.panDance1.Visible = false;
            // 
            // gbLifts
            // 
            this.gbLifts.BackColor = System.Drawing.Color.Transparent;
            this.gbLifts.Controls.Add(this.el647);
            this.gbLifts.Controls.Add(this.el653);
            this.gbLifts.Controls.Add(this.el659);
            this.gbLifts.Controls.Add(this.el660);
            this.gbLifts.Controls.Add(this.label1);
            this.gbLifts.Controls.Add(this.el641);
            this.gbLifts.Controls.Add(this.el269);
            this.gbLifts.Controls.Add(this.el264);
            this.gbLifts.Controls.Add(this.el259);
            this.gbLifts.Controls.Add(this.label32);
            this.gbLifts.Controls.Add(this.el268);
            this.gbLifts.Controls.Add(this.el263);
            this.gbLifts.Controls.Add(this.el258);
            this.gbLifts.Controls.Add(this.el267);
            this.gbLifts.Controls.Add(this.el266);
            this.gbLifts.Controls.Add(this.el265);
            this.gbLifts.Controls.Add(this.label29);
            this.gbLifts.Controls.Add(this.el262);
            this.gbLifts.Controls.Add(this.el261);
            this.gbLifts.Controls.Add(this.el260);
            this.gbLifts.Controls.Add(this.label30);
            this.gbLifts.Controls.Add(this.el257);
            this.gbLifts.Controls.Add(this.el256);
            this.gbLifts.Controls.Add(this.el254);
            this.gbLifts.Controls.Add(this.label31);
            this.gbLifts.Cursor = System.Windows.Forms.Cursors.Default;
            this.gbLifts.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbLifts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbLifts.Font = new System.Drawing.Font("Arial Narrow", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbLifts.ForeColor = System.Drawing.Color.Yellow;
            this.gbLifts.Location = new System.Drawing.Point(0, 187);
            this.gbLifts.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gbLifts.Name = "gbLifts";
            this.gbLifts.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gbLifts.Size = new System.Drawing.Size(356, 237);
            this.gbLifts.TabIndex = 6;
            this.gbLifts.TabStop = false;
            this.gbLifts.Tag = "2";
            // 
            // el647
            // 
            this.el647.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el647.BackColor = System.Drawing.Color.Transparent;
            this.el647.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el647.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el647.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el647.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el647.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el647.ForeColor = System.Drawing.Color.Yellow;
            this.el647.Location = new System.Drawing.Point(145, 110);
            this.el647.Margin = new System.Windows.Forms.Padding(0);
            this.el647.Name = "el647";
            this.el647.Size = new System.Drawing.Size(26, 32);
            this.el647.TabIndex = 184;
            this.el647.Text = "0";
            this.el647.UseVisualStyleBackColor = false;
            this.el647.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el653
            // 
            this.el653.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el653.BackColor = System.Drawing.Color.Transparent;
            this.el653.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el653.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el653.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el653.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el653.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el653.ForeColor = System.Drawing.Color.Yellow;
            this.el653.Location = new System.Drawing.Point(145, 146);
            this.el653.Margin = new System.Windows.Forms.Padding(0);
            this.el653.Name = "el653";
            this.el653.Size = new System.Drawing.Size(26, 32);
            this.el653.TabIndex = 183;
            this.el653.Text = "0";
            this.el653.UseVisualStyleBackColor = false;
            this.el653.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el659
            // 
            this.el659.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el659.BackColor = System.Drawing.Color.Transparent;
            this.el659.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el659.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el659.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el659.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el659.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el659.ForeColor = System.Drawing.Color.Yellow;
            this.el659.Location = new System.Drawing.Point(145, 183);
            this.el659.Margin = new System.Windows.Forms.Padding(0);
            this.el659.Name = "el659";
            this.el659.Size = new System.Drawing.Size(26, 32);
            this.el659.TabIndex = 182;
            this.el659.Text = "0";
            this.el659.UseVisualStyleBackColor = false;
            this.el659.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el660
            // 
            this.el660.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el660.BackColor = System.Drawing.Color.Transparent;
            this.el660.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el660.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el660.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el660.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el660.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el660.ForeColor = System.Drawing.Color.Yellow;
            this.el660.Location = new System.Drawing.Point(178, 183);
            this.el660.Margin = new System.Windows.Forms.Padding(0);
            this.el660.Name = "el660";
            this.el660.Size = new System.Drawing.Size(26, 32);
            this.el660.TabIndex = 181;
            this.el660.Text = "1";
            this.el660.UseVisualStyleBackColor = false;
            this.el660.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Yellow;
            this.label1.Location = new System.Drawing.Point(12, 182);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(255, 36);
            this.label1.TabIndex = 180;
            this.label1.Text = "CHOREO";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el641
            // 
            this.el641.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el641.BackColor = System.Drawing.Color.Transparent;
            this.el641.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el641.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el641.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el641.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el641.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el641.ForeColor = System.Drawing.Color.Yellow;
            this.el641.Location = new System.Drawing.Point(145, 74);
            this.el641.Margin = new System.Windows.Forms.Padding(0);
            this.el641.Name = "el641";
            this.el641.Size = new System.Drawing.Size(26, 32);
            this.el641.TabIndex = 132;
            this.el641.Text = "0";
            this.el641.UseVisualStyleBackColor = false;
            this.el641.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el269
            // 
            this.el269.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el269.BackColor = System.Drawing.Color.Transparent;
            this.el269.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el269.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el269.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el269.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el269.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el269.ForeColor = System.Drawing.Color.Yellow;
            this.el269.Location = new System.Drawing.Point(306, 146);
            this.el269.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el269.Name = "el269";
            this.el269.Size = new System.Drawing.Size(26, 32);
            this.el269.TabIndex = 179;
            this.el269.Text = "5";
            this.el269.UseVisualStyleBackColor = false;
            this.el269.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el264
            // 
            this.el264.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el264.BackColor = System.Drawing.Color.Transparent;
            this.el264.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el264.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el264.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el264.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el264.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el264.ForeColor = System.Drawing.Color.Yellow;
            this.el264.Location = new System.Drawing.Point(306, 110);
            this.el264.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el264.Name = "el264";
            this.el264.Size = new System.Drawing.Size(26, 32);
            this.el264.TabIndex = 178;
            this.el264.Text = "5";
            this.el264.UseVisualStyleBackColor = false;
            this.el264.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el259
            // 
            this.el259.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el259.BackColor = System.Drawing.Color.Transparent;
            this.el259.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el259.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el259.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el259.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el259.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el259.ForeColor = System.Drawing.Color.Yellow;
            this.el259.Location = new System.Drawing.Point(306, 74);
            this.el259.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el259.Name = "el259";
            this.el259.Size = new System.Drawing.Size(26, 32);
            this.el259.TabIndex = 177;
            this.el259.Text = "5";
            this.el259.UseVisualStyleBackColor = false;
            this.el259.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label32
            // 
            this.label32.AutoEllipsis = true;
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Lime;
            this.label32.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(9, 25);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(174, 36);
            this.label32.TabIndex = 128;
            this.label32.Text = "Dance Lifts";
            // 
            // el268
            // 
            this.el268.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el268.BackColor = System.Drawing.Color.Transparent;
            this.el268.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el268.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el268.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el268.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el268.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el268.ForeColor = System.Drawing.Color.Yellow;
            this.el268.Location = new System.Drawing.Point(274, 146);
            this.el268.Margin = new System.Windows.Forms.Padding(0);
            this.el268.Name = "el268";
            this.el268.Size = new System.Drawing.Size(26, 32);
            this.el268.TabIndex = 105;
            this.el268.Text = "4";
            this.el268.UseVisualStyleBackColor = false;
            this.el268.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el263
            // 
            this.el263.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el263.BackColor = System.Drawing.Color.Transparent;
            this.el263.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el263.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el263.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el263.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el263.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el263.ForeColor = System.Drawing.Color.Yellow;
            this.el263.Location = new System.Drawing.Point(274, 110);
            this.el263.Margin = new System.Windows.Forms.Padding(0);
            this.el263.Name = "el263";
            this.el263.Size = new System.Drawing.Size(26, 32);
            this.el263.TabIndex = 104;
            this.el263.Text = "4";
            this.el263.UseVisualStyleBackColor = false;
            this.el263.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el258
            // 
            this.el258.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el258.BackColor = System.Drawing.Color.Transparent;
            this.el258.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el258.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el258.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el258.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el258.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el258.ForeColor = System.Drawing.Color.Yellow;
            this.el258.Location = new System.Drawing.Point(274, 74);
            this.el258.Margin = new System.Windows.Forms.Padding(0);
            this.el258.Name = "el258";
            this.el258.Size = new System.Drawing.Size(26, 32);
            this.el258.TabIndex = 103;
            this.el258.Text = "4";
            this.el258.UseVisualStyleBackColor = false;
            this.el258.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el267
            // 
            this.el267.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el267.BackColor = System.Drawing.Color.Transparent;
            this.el267.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el267.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el267.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el267.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el267.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el267.ForeColor = System.Drawing.Color.Yellow;
            this.el267.Location = new System.Drawing.Point(242, 146);
            this.el267.Margin = new System.Windows.Forms.Padding(0);
            this.el267.Name = "el267";
            this.el267.Size = new System.Drawing.Size(26, 32);
            this.el267.TabIndex = 75;
            this.el267.Text = "3";
            this.el267.UseVisualStyleBackColor = false;
            this.el267.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el266
            // 
            this.el266.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el266.BackColor = System.Drawing.Color.Transparent;
            this.el266.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el266.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el266.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el266.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el266.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el266.ForeColor = System.Drawing.Color.Yellow;
            this.el266.Location = new System.Drawing.Point(210, 146);
            this.el266.Margin = new System.Windows.Forms.Padding(0);
            this.el266.Name = "el266";
            this.el266.Size = new System.Drawing.Size(26, 32);
            this.el266.TabIndex = 74;
            this.el266.Text = "2";
            this.el266.UseVisualStyleBackColor = false;
            this.el266.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el265
            // 
            this.el265.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el265.BackColor = System.Drawing.Color.Transparent;
            this.el265.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el265.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el265.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el265.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el265.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el265.ForeColor = System.Drawing.Color.Yellow;
            this.el265.Location = new System.Drawing.Point(178, 146);
            this.el265.Margin = new System.Windows.Forms.Padding(0);
            this.el265.Name = "el265";
            this.el265.Size = new System.Drawing.Size(26, 32);
            this.el265.TabIndex = 73;
            this.el265.Text = "1";
            this.el265.UseVisualStyleBackColor = false;
            this.el265.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label29.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Lime;
            this.label29.Location = new System.Drawing.Point(12, 145);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(255, 36);
            this.label29.TabIndex = 72;
            this.label29.Text = "COMBINATION";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el262
            // 
            this.el262.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el262.BackColor = System.Drawing.Color.Transparent;
            this.el262.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el262.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el262.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el262.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el262.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el262.ForeColor = System.Drawing.Color.Yellow;
            this.el262.Location = new System.Drawing.Point(242, 110);
            this.el262.Margin = new System.Windows.Forms.Padding(0);
            this.el262.Name = "el262";
            this.el262.Size = new System.Drawing.Size(26, 32);
            this.el262.TabIndex = 69;
            this.el262.Text = "3";
            this.el262.UseVisualStyleBackColor = false;
            this.el262.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el261
            // 
            this.el261.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el261.BackColor = System.Drawing.Color.Transparent;
            this.el261.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el261.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el261.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el261.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el261.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el261.ForeColor = System.Drawing.Color.Yellow;
            this.el261.Location = new System.Drawing.Point(210, 110);
            this.el261.Margin = new System.Windows.Forms.Padding(0);
            this.el261.Name = "el261";
            this.el261.Size = new System.Drawing.Size(26, 32);
            this.el261.TabIndex = 68;
            this.el261.Text = "2";
            this.el261.UseVisualStyleBackColor = false;
            this.el261.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el260
            // 
            this.el260.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el260.BackColor = System.Drawing.Color.Transparent;
            this.el260.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el260.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el260.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el260.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el260.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el260.ForeColor = System.Drawing.Color.Yellow;
            this.el260.Location = new System.Drawing.Point(178, 110);
            this.el260.Margin = new System.Windows.Forms.Padding(0);
            this.el260.Name = "el260";
            this.el260.Size = new System.Drawing.Size(26, 32);
            this.el260.TabIndex = 67;
            this.el260.Text = "1";
            this.el260.UseVisualStyleBackColor = false;
            this.el260.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label30.Font = new System.Drawing.Font("Arial Narrow", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label30.Location = new System.Drawing.Point(12, 108);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(255, 36);
            this.label30.TabIndex = 66;
            this.label30.Text = "ROTATIONAL";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el257
            // 
            this.el257.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el257.BackColor = System.Drawing.Color.Transparent;
            this.el257.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el257.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el257.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el257.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el257.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el257.ForeColor = System.Drawing.Color.Yellow;
            this.el257.Location = new System.Drawing.Point(242, 74);
            this.el257.Margin = new System.Windows.Forms.Padding(0);
            this.el257.Name = "el257";
            this.el257.Size = new System.Drawing.Size(26, 32);
            this.el257.TabIndex = 63;
            this.el257.Text = "3";
            this.el257.UseVisualStyleBackColor = false;
            this.el257.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el256
            // 
            this.el256.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el256.BackColor = System.Drawing.Color.Transparent;
            this.el256.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el256.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el256.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el256.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el256.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el256.ForeColor = System.Drawing.Color.Yellow;
            this.el256.Location = new System.Drawing.Point(210, 74);
            this.el256.Margin = new System.Windows.Forms.Padding(0);
            this.el256.Name = "el256";
            this.el256.Size = new System.Drawing.Size(26, 32);
            this.el256.TabIndex = 62;
            this.el256.Text = "2";
            this.el256.UseVisualStyleBackColor = false;
            this.el256.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el254
            // 
            this.el254.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el254.BackColor = System.Drawing.Color.Transparent;
            this.el254.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el254.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el254.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el254.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el254.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el254.ForeColor = System.Drawing.Color.Yellow;
            this.el254.Location = new System.Drawing.Point(178, 74);
            this.el254.Margin = new System.Windows.Forms.Padding(0);
            this.el254.Name = "el254";
            this.el254.Size = new System.Drawing.Size(26, 32);
            this.el254.TabIndex = 61;
            this.el254.Text = "1";
            this.el254.UseVisualStyleBackColor = false;
            this.el254.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label31.Font = new System.Drawing.Font("Arial Narrow", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label31.Location = new System.Drawing.Point(12, 71);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(255, 36);
            this.label31.TabIndex = 37;
            this.label31.Tag = "6";
            this.label31.Text = "STATIONARY";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbPattern
            // 
            this.gbPattern.BackColor = System.Drawing.Color.Transparent;
            this.gbPattern.Controls.Add(this.el310);
            this.gbPattern.Controls.Add(this.label33);
            this.gbPattern.Controls.Add(this.el277);
            this.gbPattern.Controls.Add(this.el273);
            this.gbPattern.Controls.Add(this.el276);
            this.gbPattern.Controls.Add(this.el275);
            this.gbPattern.Controls.Add(this.el274);
            this.gbPattern.Controls.Add(this.label17);
            this.gbPattern.Controls.Add(this.el272);
            this.gbPattern.Controls.Add(this.el271);
            this.gbPattern.Controls.Add(this.el270);
            this.gbPattern.Controls.Add(this.label16);
            this.gbPattern.Controls.Add(this.labPattern);
            this.gbPattern.Cursor = System.Windows.Forms.Cursors.Default;
            this.gbPattern.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbPattern.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbPattern.Font = new System.Drawing.Font("Arial Narrow", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbPattern.ForeColor = System.Drawing.Color.Yellow;
            this.gbPattern.Location = new System.Drawing.Point(0, 0);
            this.gbPattern.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gbPattern.Name = "gbPattern";
            this.gbPattern.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gbPattern.Size = new System.Drawing.Size(356, 187);
            this.gbPattern.TabIndex = 5;
            this.gbPattern.TabStop = false;
            this.gbPattern.Tag = "1";
            // 
            // el310
            // 
            this.el310.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el310.BackColor = System.Drawing.Color.Transparent;
            this.el310.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el310.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el310.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el310.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el310.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el310.ForeColor = System.Drawing.Color.Yellow;
            this.el310.Location = new System.Drawing.Point(307, 25);
            this.el310.Margin = new System.Windows.Forms.Padding(0);
            this.el310.Name = "el310";
            this.el310.Size = new System.Drawing.Size(26, 32);
            this.el310.TabIndex = 131;
            this.el310.Text = "0";
            this.el310.UseVisualStyleBackColor = false;
            this.el310.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label33
            // 
            this.label33.AutoEllipsis = true;
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Aqua;
            this.label33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(10, 24);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(117, 36);
            this.label33.TabIndex = 129;
            this.label33.Text = "Pattern";
            // 
            // el277
            // 
            this.el277.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el277.BackColor = System.Drawing.Color.Transparent;
            this.el277.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el277.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el277.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el277.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el277.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el277.ForeColor = System.Drawing.Color.Yellow;
            this.el277.Location = new System.Drawing.Point(307, 129);
            this.el277.Margin = new System.Windows.Forms.Padding(0);
            this.el277.Name = "el277";
            this.el277.Size = new System.Drawing.Size(26, 32);
            this.el277.TabIndex = 104;
            this.el277.Text = "4";
            this.el277.UseVisualStyleBackColor = false;
            this.el277.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el273
            // 
            this.el273.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el273.BackColor = System.Drawing.Color.Transparent;
            this.el273.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el273.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el273.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el273.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el273.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el273.ForeColor = System.Drawing.Color.Yellow;
            this.el273.Location = new System.Drawing.Point(307, 91);
            this.el273.Margin = new System.Windows.Forms.Padding(0);
            this.el273.Name = "el273";
            this.el273.Size = new System.Drawing.Size(26, 32);
            this.el273.TabIndex = 103;
            this.el273.Text = "4";
            this.el273.UseVisualStyleBackColor = false;
            this.el273.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el276
            // 
            this.el276.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el276.BackColor = System.Drawing.Color.Transparent;
            this.el276.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el276.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el276.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el276.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el276.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el276.ForeColor = System.Drawing.Color.Yellow;
            this.el276.Location = new System.Drawing.Point(275, 129);
            this.el276.Margin = new System.Windows.Forms.Padding(0);
            this.el276.Name = "el276";
            this.el276.Size = new System.Drawing.Size(26, 32);
            this.el276.TabIndex = 69;
            this.el276.Text = "3";
            this.el276.UseVisualStyleBackColor = false;
            this.el276.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el275
            // 
            this.el275.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el275.BackColor = System.Drawing.Color.Transparent;
            this.el275.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el275.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el275.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el275.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el275.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el275.ForeColor = System.Drawing.Color.Yellow;
            this.el275.Location = new System.Drawing.Point(243, 129);
            this.el275.Margin = new System.Windows.Forms.Padding(0);
            this.el275.Name = "el275";
            this.el275.Size = new System.Drawing.Size(26, 32);
            this.el275.TabIndex = 68;
            this.el275.Text = "2";
            this.el275.UseVisualStyleBackColor = false;
            this.el275.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el274
            // 
            this.el274.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el274.BackColor = System.Drawing.Color.Transparent;
            this.el274.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el274.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el274.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el274.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el274.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el274.ForeColor = System.Drawing.Color.Yellow;
            this.el274.Location = new System.Drawing.Point(211, 129);
            this.el274.Margin = new System.Windows.Forms.Padding(0);
            this.el274.Name = "el274";
            this.el274.Size = new System.Drawing.Size(26, 32);
            this.el274.TabIndex = 67;
            this.el274.Text = "1";
            this.el274.UseVisualStyleBackColor = false;
            this.el274.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label17.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label17.Location = new System.Drawing.Point(16, 124);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(255, 36);
            this.label17.TabIndex = 66;
            this.label17.Text = "S E C T I O N  2";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el272
            // 
            this.el272.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el272.BackColor = System.Drawing.Color.Transparent;
            this.el272.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el272.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el272.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el272.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el272.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el272.ForeColor = System.Drawing.Color.Yellow;
            this.el272.Location = new System.Drawing.Point(275, 91);
            this.el272.Margin = new System.Windows.Forms.Padding(0);
            this.el272.Name = "el272";
            this.el272.Size = new System.Drawing.Size(26, 32);
            this.el272.TabIndex = 63;
            this.el272.Text = "3";
            this.el272.UseVisualStyleBackColor = false;
            this.el272.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el271
            // 
            this.el271.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el271.BackColor = System.Drawing.Color.Transparent;
            this.el271.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el271.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el271.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el271.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el271.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el271.ForeColor = System.Drawing.Color.Yellow;
            this.el271.Location = new System.Drawing.Point(243, 91);
            this.el271.Margin = new System.Windows.Forms.Padding(0);
            this.el271.Name = "el271";
            this.el271.Size = new System.Drawing.Size(26, 32);
            this.el271.TabIndex = 62;
            this.el271.Text = "2";
            this.el271.UseVisualStyleBackColor = false;
            this.el271.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el270
            // 
            this.el270.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el270.BackColor = System.Drawing.Color.Transparent;
            this.el270.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el270.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el270.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el270.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el270.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el270.ForeColor = System.Drawing.Color.Yellow;
            this.el270.Location = new System.Drawing.Point(211, 91);
            this.el270.Margin = new System.Windows.Forms.Padding(0);
            this.el270.Name = "el270";
            this.el270.Size = new System.Drawing.Size(26, 32);
            this.el270.TabIndex = 61;
            this.el270.Text = "1";
            this.el270.UseVisualStyleBackColor = false;
            this.el270.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label16.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label16.Location = new System.Drawing.Point(16, 87);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(255, 36);
            this.label16.TabIndex = 37;
            this.label16.Tag = "6";
            this.label16.Text = "S E C T I O N  1";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labPattern
            // 
            this.labPattern.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labPattern.AutoEllipsis = true;
            this.labPattern.BackColor = System.Drawing.Color.Transparent;
            this.labPattern.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labPattern.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPattern.ForeColor = System.Drawing.Color.Aqua;
            this.labPattern.Location = new System.Drawing.Point(26, 61);
            this.labPattern.Name = "labPattern";
            this.labPattern.Size = new System.Drawing.Size(312, 26);
            this.labPattern.TabIndex = 130;
            this.labPattern.Text = "Quick Step";
            this.labPattern.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panPrecision1
            // 
            this.panPrecision1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panPrecision1.Controls.Add(this.gbPivoting);
            this.panPrecision1.Controls.Add(this.gbRotating);
            this.panPrecision1.Controls.Add(this.gbTravelling);
            this.panPrecision1.Location = new System.Drawing.Point(2, -2);
            this.panPrecision1.Name = "panPrecision1";
            this.panPrecision1.Size = new System.Drawing.Size(357, 430);
            this.panPrecision1.TabIndex = 9;
            this.panPrecision1.Visible = false;
            // 
            // gbPivoting
            // 
            this.gbPivoting.BackColor = System.Drawing.Color.Transparent;
            this.gbPivoting.Controls.Add(this.panBPivo);
            this.gbPivoting.Controls.Add(this.panLPivo);
            this.gbPivoting.Controls.Add(this.label44);
            this.gbPivoting.Cursor = System.Windows.Forms.Cursors.Default;
            this.gbPivoting.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbPivoting.Enabled = false;
            this.gbPivoting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbPivoting.Font = new System.Drawing.Font("Arial Narrow", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbPivoting.ForeColor = System.Drawing.Color.Yellow;
            this.gbPivoting.Location = new System.Drawing.Point(0, 279);
            this.gbPivoting.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gbPivoting.Name = "gbPivoting";
            this.gbPivoting.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gbPivoting.Size = new System.Drawing.Size(357, 142);
            this.gbPivoting.TabIndex = 10;
            this.gbPivoting.TabStop = false;
            this.gbPivoting.Tag = "1";
            // 
            // panBPivo
            // 
            this.panBPivo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panBPivo.Controls.Add(this.el551);
            this.panBPivo.Controls.Add(this.el554);
            this.panBPivo.Controls.Add(this.el553);
            this.panBPivo.Controls.Add(this.el552);
            this.panBPivo.Controls.Add(this.el555);
            this.panBPivo.Controls.Add(this.el556);
            this.panBPivo.Controls.Add(this.label42);
            this.panBPivo.Location = new System.Drawing.Point(16, 98);
            this.panBPivo.Name = "panBPivo";
            this.panBPivo.Size = new System.Drawing.Size(334, 39);
            this.panBPivo.TabIndex = 139;
            // 
            // el551
            // 
            this.el551.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el551.BackColor = System.Drawing.Color.Transparent;
            this.el551.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el551.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el551.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el551.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el551.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el551.ForeColor = System.Drawing.Color.Yellow;
            this.el551.Location = new System.Drawing.Point(135, 3);
            this.el551.Margin = new System.Windows.Forms.Padding(0);
            this.el551.Name = "el551";
            this.el551.Size = new System.Drawing.Size(26, 32);
            this.el551.TabIndex = 136;
            this.el551.Text = "0";
            this.el551.UseVisualStyleBackColor = false;
            this.el551.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el554
            // 
            this.el554.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el554.BackColor = System.Drawing.Color.Transparent;
            this.el554.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el554.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el554.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el554.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el554.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el554.ForeColor = System.Drawing.Color.Yellow;
            this.el554.Location = new System.Drawing.Point(233, 3);
            this.el554.Margin = new System.Windows.Forms.Padding(0);
            this.el554.Name = "el554";
            this.el554.Size = new System.Drawing.Size(26, 32);
            this.el554.TabIndex = 68;
            this.el554.Text = "2";
            this.el554.UseVisualStyleBackColor = false;
            this.el554.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el553
            // 
            this.el553.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el553.BackColor = System.Drawing.Color.Transparent;
            this.el553.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el553.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el553.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el553.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el553.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el553.ForeColor = System.Drawing.Color.Yellow;
            this.el553.Location = new System.Drawing.Point(201, 3);
            this.el553.Margin = new System.Windows.Forms.Padding(0);
            this.el553.Name = "el553";
            this.el553.Size = new System.Drawing.Size(26, 32);
            this.el553.TabIndex = 67;
            this.el553.Text = "1";
            this.el553.UseVisualStyleBackColor = false;
            this.el553.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el552
            // 
            this.el552.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el552.BackColor = System.Drawing.Color.Transparent;
            this.el552.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el552.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el552.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el552.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el552.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el552.ForeColor = System.Drawing.Color.Yellow;
            this.el552.Location = new System.Drawing.Point(168, 3);
            this.el552.Margin = new System.Windows.Forms.Padding(0);
            this.el552.Name = "el552";
            this.el552.Size = new System.Drawing.Size(26, 32);
            this.el552.TabIndex = 135;
            this.el552.Text = "B";
            this.el552.UseVisualStyleBackColor = false;
            this.el552.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el555
            // 
            this.el555.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el555.BackColor = System.Drawing.Color.Transparent;
            this.el555.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el555.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el555.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el555.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el555.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el555.ForeColor = System.Drawing.Color.Yellow;
            this.el555.Location = new System.Drawing.Point(265, 3);
            this.el555.Margin = new System.Windows.Forms.Padding(0);
            this.el555.Name = "el555";
            this.el555.Size = new System.Drawing.Size(26, 32);
            this.el555.TabIndex = 69;
            this.el555.Text = "3";
            this.el555.UseVisualStyleBackColor = false;
            this.el555.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el556
            // 
            this.el556.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el556.BackColor = System.Drawing.Color.Transparent;
            this.el556.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el556.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el556.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el556.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el556.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el556.ForeColor = System.Drawing.Color.Yellow;
            this.el556.Location = new System.Drawing.Point(297, 3);
            this.el556.Margin = new System.Windows.Forms.Padding(0);
            this.el556.Name = "el556";
            this.el556.Size = new System.Drawing.Size(26, 32);
            this.el556.TabIndex = 104;
            this.el556.Text = "4";
            this.el556.UseVisualStyleBackColor = false;
            this.el556.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label42
            // 
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Dock = System.Windows.Forms.DockStyle.Left;
            this.label42.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label42.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label42.Location = new System.Drawing.Point(0, 0);
            this.label42.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(125, 39);
            this.label42.TabIndex = 66;
            this.label42.Text = "BLOCK";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panLPivo
            // 
            this.panLPivo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panLPivo.Controls.Add(this.el557);
            this.panLPivo.Controls.Add(this.el560);
            this.panLPivo.Controls.Add(this.label43);
            this.panLPivo.Controls.Add(this.el559);
            this.panLPivo.Controls.Add(this.el558);
            this.panLPivo.Controls.Add(this.el561);
            this.panLPivo.Controls.Add(this.el562);
            this.panLPivo.Location = new System.Drawing.Point(16, 56);
            this.panLPivo.Name = "panLPivo";
            this.panLPivo.Size = new System.Drawing.Size(331, 39);
            this.panLPivo.TabIndex = 141;
            // 
            // el557
            // 
            this.el557.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el557.BackColor = System.Drawing.Color.Transparent;
            this.el557.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el557.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el557.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el557.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el557.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el557.ForeColor = System.Drawing.Color.Yellow;
            this.el557.Location = new System.Drawing.Point(135, 7);
            this.el557.Margin = new System.Windows.Forms.Padding(0);
            this.el557.Name = "el557";
            this.el557.Size = new System.Drawing.Size(26, 32);
            this.el557.TabIndex = 136;
            this.el557.Text = "0";
            this.el557.UseVisualStyleBackColor = false;
            this.el557.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el560
            // 
            this.el560.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el560.BackColor = System.Drawing.Color.Transparent;
            this.el560.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el560.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el560.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el560.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el560.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el560.ForeColor = System.Drawing.Color.Yellow;
            this.el560.Location = new System.Drawing.Point(233, 7);
            this.el560.Margin = new System.Windows.Forms.Padding(0);
            this.el560.Name = "el560";
            this.el560.Size = new System.Drawing.Size(26, 32);
            this.el560.TabIndex = 68;
            this.el560.Text = "2";
            this.el560.UseVisualStyleBackColor = false;
            this.el560.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label43
            // 
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Dock = System.Windows.Forms.DockStyle.Left;
            this.label43.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label43.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label43.Location = new System.Drawing.Point(0, 0);
            this.label43.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(143, 39);
            this.label43.TabIndex = 66;
            this.label43.Text = "LINE";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el559
            // 
            this.el559.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el559.BackColor = System.Drawing.Color.Transparent;
            this.el559.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el559.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el559.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el559.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el559.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el559.ForeColor = System.Drawing.Color.Yellow;
            this.el559.Location = new System.Drawing.Point(201, 7);
            this.el559.Margin = new System.Windows.Forms.Padding(0);
            this.el559.Name = "el559";
            this.el559.Size = new System.Drawing.Size(26, 32);
            this.el559.TabIndex = 67;
            this.el559.Text = "1";
            this.el559.UseVisualStyleBackColor = false;
            this.el559.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el558
            // 
            this.el558.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el558.BackColor = System.Drawing.Color.Transparent;
            this.el558.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el558.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el558.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el558.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el558.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el558.ForeColor = System.Drawing.Color.Yellow;
            this.el558.Location = new System.Drawing.Point(168, 7);
            this.el558.Margin = new System.Windows.Forms.Padding(0);
            this.el558.Name = "el558";
            this.el558.Size = new System.Drawing.Size(26, 32);
            this.el558.TabIndex = 135;
            this.el558.Text = "B";
            this.el558.UseVisualStyleBackColor = false;
            this.el558.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el561
            // 
            this.el561.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el561.BackColor = System.Drawing.Color.Transparent;
            this.el561.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el561.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el561.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el561.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el561.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el561.ForeColor = System.Drawing.Color.Yellow;
            this.el561.Location = new System.Drawing.Point(265, 7);
            this.el561.Margin = new System.Windows.Forms.Padding(0);
            this.el561.Name = "el561";
            this.el561.Size = new System.Drawing.Size(26, 32);
            this.el561.TabIndex = 69;
            this.el561.Text = "3";
            this.el561.UseVisualStyleBackColor = false;
            this.el561.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el562
            // 
            this.el562.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el562.BackColor = System.Drawing.Color.Transparent;
            this.el562.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el562.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el562.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el562.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el562.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el562.ForeColor = System.Drawing.Color.Yellow;
            this.el562.Location = new System.Drawing.Point(297, 7);
            this.el562.Margin = new System.Windows.Forms.Padding(0);
            this.el562.Name = "el562";
            this.el562.Size = new System.Drawing.Size(26, 32);
            this.el562.TabIndex = 104;
            this.el562.Text = "4";
            this.el562.UseVisualStyleBackColor = false;
            this.el562.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label44
            // 
            this.label44.AutoEllipsis = true;
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Orange;
            this.label44.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(11, 22);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(119, 31);
            this.label44.TabIndex = 129;
            this.label44.Text = "Pivoting";
            // 
            // gbRotating
            // 
            this.gbRotating.BackColor = System.Drawing.Color.Transparent;
            this.gbRotating.Controls.Add(this.panWRota);
            this.gbRotating.Controls.Add(this.panCRota);
            this.gbRotating.Controls.Add(this.label15);
            this.gbRotating.Cursor = System.Windows.Forms.Cursors.Default;
            this.gbRotating.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbRotating.Enabled = false;
            this.gbRotating.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbRotating.Font = new System.Drawing.Font("Arial Narrow", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbRotating.ForeColor = System.Drawing.Color.Yellow;
            this.gbRotating.Location = new System.Drawing.Point(0, 139);
            this.gbRotating.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gbRotating.Name = "gbRotating";
            this.gbRotating.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gbRotating.Size = new System.Drawing.Size(357, 140);
            this.gbRotating.TabIndex = 9;
            this.gbRotating.TabStop = false;
            this.gbRotating.Tag = "1";
            // 
            // panWRota
            // 
            this.panWRota.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panWRota.Controls.Add(this.el545);
            this.panWRota.Controls.Add(this.el548);
            this.panWRota.Controls.Add(this.el547);
            this.panWRota.Controls.Add(this.el546);
            this.panWRota.Controls.Add(this.el549);
            this.panWRota.Controls.Add(this.el550);
            this.panWRota.Controls.Add(this.label25);
            this.panWRota.Location = new System.Drawing.Point(16, 94);
            this.panWRota.Name = "panWRota";
            this.panWRota.Size = new System.Drawing.Size(334, 39);
            this.panWRota.TabIndex = 139;
            // 
            // el545
            // 
            this.el545.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el545.BackColor = System.Drawing.Color.Transparent;
            this.el545.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el545.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el545.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el545.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el545.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el545.ForeColor = System.Drawing.Color.Yellow;
            this.el545.Location = new System.Drawing.Point(135, 3);
            this.el545.Margin = new System.Windows.Forms.Padding(0);
            this.el545.Name = "el545";
            this.el545.Size = new System.Drawing.Size(26, 32);
            this.el545.TabIndex = 136;
            this.el545.Text = "0";
            this.el545.UseVisualStyleBackColor = false;
            this.el545.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el548
            // 
            this.el548.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el548.BackColor = System.Drawing.Color.Transparent;
            this.el548.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el548.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el548.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el548.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el548.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el548.ForeColor = System.Drawing.Color.Yellow;
            this.el548.Location = new System.Drawing.Point(233, 3);
            this.el548.Margin = new System.Windows.Forms.Padding(0);
            this.el548.Name = "el548";
            this.el548.Size = new System.Drawing.Size(26, 32);
            this.el548.TabIndex = 68;
            this.el548.Text = "2";
            this.el548.UseVisualStyleBackColor = false;
            this.el548.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el547
            // 
            this.el547.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el547.BackColor = System.Drawing.Color.Transparent;
            this.el547.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el547.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el547.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el547.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el547.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el547.ForeColor = System.Drawing.Color.Yellow;
            this.el547.Location = new System.Drawing.Point(201, 3);
            this.el547.Margin = new System.Windows.Forms.Padding(0);
            this.el547.Name = "el547";
            this.el547.Size = new System.Drawing.Size(26, 32);
            this.el547.TabIndex = 67;
            this.el547.Text = "1";
            this.el547.UseVisualStyleBackColor = false;
            this.el547.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el546
            // 
            this.el546.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el546.BackColor = System.Drawing.Color.Transparent;
            this.el546.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el546.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el546.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el546.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el546.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el546.ForeColor = System.Drawing.Color.Yellow;
            this.el546.Location = new System.Drawing.Point(168, 3);
            this.el546.Margin = new System.Windows.Forms.Padding(0);
            this.el546.Name = "el546";
            this.el546.Size = new System.Drawing.Size(26, 32);
            this.el546.TabIndex = 135;
            this.el546.Text = "B";
            this.el546.UseVisualStyleBackColor = false;
            this.el546.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el549
            // 
            this.el549.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el549.BackColor = System.Drawing.Color.Transparent;
            this.el549.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el549.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el549.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el549.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el549.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el549.ForeColor = System.Drawing.Color.Yellow;
            this.el549.Location = new System.Drawing.Point(265, 3);
            this.el549.Margin = new System.Windows.Forms.Padding(0);
            this.el549.Name = "el549";
            this.el549.Size = new System.Drawing.Size(26, 32);
            this.el549.TabIndex = 69;
            this.el549.Text = "3";
            this.el549.UseVisualStyleBackColor = false;
            this.el549.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el550
            // 
            this.el550.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el550.BackColor = System.Drawing.Color.Transparent;
            this.el550.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el550.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el550.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el550.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el550.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el550.ForeColor = System.Drawing.Color.Yellow;
            this.el550.Location = new System.Drawing.Point(297, 3);
            this.el550.Margin = new System.Windows.Forms.Padding(0);
            this.el550.Name = "el550";
            this.el550.Size = new System.Drawing.Size(26, 32);
            this.el550.TabIndex = 104;
            this.el550.Text = "4";
            this.el550.UseVisualStyleBackColor = false;
            this.el550.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Dock = System.Windows.Forms.DockStyle.Left;
            this.label25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label25.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label25.Location = new System.Drawing.Point(0, 0);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(125, 39);
            this.label25.TabIndex = 66;
            this.label25.Text = "WHEEL";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panCRota
            // 
            this.panCRota.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panCRota.Controls.Add(this.el539);
            this.panCRota.Controls.Add(this.el542);
            this.panCRota.Controls.Add(this.el541);
            this.panCRota.Controls.Add(this.el540);
            this.panCRota.Controls.Add(this.el543);
            this.panCRota.Controls.Add(this.el544);
            this.panCRota.Controls.Add(this.label27);
            this.panCRota.Location = new System.Drawing.Point(16, 52);
            this.panCRota.Name = "panCRota";
            this.panCRota.Size = new System.Drawing.Size(331, 39);
            this.panCRota.TabIndex = 141;
            // 
            // el539
            // 
            this.el539.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el539.BackColor = System.Drawing.Color.Transparent;
            this.el539.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el539.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el539.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el539.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el539.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el539.ForeColor = System.Drawing.Color.Yellow;
            this.el539.Location = new System.Drawing.Point(135, 7);
            this.el539.Margin = new System.Windows.Forms.Padding(0);
            this.el539.Name = "el539";
            this.el539.Size = new System.Drawing.Size(26, 32);
            this.el539.TabIndex = 136;
            this.el539.Text = "0";
            this.el539.UseVisualStyleBackColor = false;
            this.el539.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el542
            // 
            this.el542.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el542.BackColor = System.Drawing.Color.Transparent;
            this.el542.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el542.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el542.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el542.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el542.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el542.ForeColor = System.Drawing.Color.Yellow;
            this.el542.Location = new System.Drawing.Point(233, 7);
            this.el542.Margin = new System.Windows.Forms.Padding(0);
            this.el542.Name = "el542";
            this.el542.Size = new System.Drawing.Size(26, 32);
            this.el542.TabIndex = 68;
            this.el542.Text = "2";
            this.el542.UseVisualStyleBackColor = false;
            this.el542.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el541
            // 
            this.el541.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el541.BackColor = System.Drawing.Color.Transparent;
            this.el541.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el541.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el541.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el541.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el541.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el541.ForeColor = System.Drawing.Color.Yellow;
            this.el541.Location = new System.Drawing.Point(201, 7);
            this.el541.Margin = new System.Windows.Forms.Padding(0);
            this.el541.Name = "el541";
            this.el541.Size = new System.Drawing.Size(26, 32);
            this.el541.TabIndex = 67;
            this.el541.Text = "1";
            this.el541.UseVisualStyleBackColor = false;
            this.el541.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el540
            // 
            this.el540.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el540.BackColor = System.Drawing.Color.Transparent;
            this.el540.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el540.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el540.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el540.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el540.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el540.ForeColor = System.Drawing.Color.Yellow;
            this.el540.Location = new System.Drawing.Point(168, 7);
            this.el540.Margin = new System.Windows.Forms.Padding(0);
            this.el540.Name = "el540";
            this.el540.Size = new System.Drawing.Size(26, 32);
            this.el540.TabIndex = 135;
            this.el540.Text = "B";
            this.el540.UseVisualStyleBackColor = false;
            this.el540.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el543
            // 
            this.el543.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el543.BackColor = System.Drawing.Color.Transparent;
            this.el543.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el543.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el543.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el543.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el543.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el543.ForeColor = System.Drawing.Color.Yellow;
            this.el543.Location = new System.Drawing.Point(265, 7);
            this.el543.Margin = new System.Windows.Forms.Padding(0);
            this.el543.Name = "el543";
            this.el543.Size = new System.Drawing.Size(26, 32);
            this.el543.TabIndex = 69;
            this.el543.Text = "3";
            this.el543.UseVisualStyleBackColor = false;
            this.el543.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el544
            // 
            this.el544.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el544.BackColor = System.Drawing.Color.Transparent;
            this.el544.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el544.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el544.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el544.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el544.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el544.ForeColor = System.Drawing.Color.Yellow;
            this.el544.Location = new System.Drawing.Point(297, 7);
            this.el544.Margin = new System.Windows.Forms.Padding(0);
            this.el544.Name = "el544";
            this.el544.Size = new System.Drawing.Size(26, 32);
            this.el544.TabIndex = 104;
            this.el544.Text = "4";
            this.el544.UseVisualStyleBackColor = false;
            this.el544.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Dock = System.Windows.Forms.DockStyle.Left;
            this.label27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label27.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.label27.Location = new System.Drawing.Point(0, 0);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(143, 39);
            this.label27.TabIndex = 66;
            this.label27.Text = "CIRCLE";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.AutoEllipsis = true;
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.LightSteelBlue;
            this.label15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(10, 20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(124, 31);
            this.label15.TabIndex = 129;
            this.label15.Text = "Rotating";
            // 
            // gbTravelling
            // 
            this.gbTravelling.BackColor = System.Drawing.Color.Transparent;
            this.gbTravelling.Controls.Add(this.panCTrav);
            this.gbTravelling.Controls.Add(this.panWTrav);
            this.gbTravelling.Controls.Add(this.label19);
            this.gbTravelling.Cursor = System.Windows.Forms.Cursors.Default;
            this.gbTravelling.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbTravelling.Enabled = false;
            this.gbTravelling.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbTravelling.Font = new System.Drawing.Font("Arial Narrow", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbTravelling.ForeColor = System.Drawing.Color.Yellow;
            this.gbTravelling.Location = new System.Drawing.Point(0, 0);
            this.gbTravelling.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gbTravelling.Name = "gbTravelling";
            this.gbTravelling.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gbTravelling.Size = new System.Drawing.Size(357, 139);
            this.gbTravelling.TabIndex = 8;
            this.gbTravelling.TabStop = false;
            this.gbTravelling.Tag = "1";
            // 
            // panCTrav
            // 
            this.panCTrav.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panCTrav.Controls.Add(this.el515);
            this.panCTrav.Controls.Add(this.el517);
            this.panCTrav.Controls.Add(this.el518);
            this.panCTrav.Controls.Add(this.el519);
            this.panCTrav.Controls.Add(this.el520);
            this.panCTrav.Controls.Add(this.el516);
            this.panCTrav.Controls.Add(this.label28);
            this.panCTrav.Location = new System.Drawing.Point(16, 52);
            this.panCTrav.Name = "panCTrav";
            this.panCTrav.Size = new System.Drawing.Size(331, 39);
            this.panCTrav.TabIndex = 140;
            // 
            // el515
            // 
            this.el515.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el515.BackColor = System.Drawing.Color.Transparent;
            this.el515.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el515.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el515.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el515.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el515.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el515.ForeColor = System.Drawing.Color.Yellow;
            this.el515.Location = new System.Drawing.Point(135, 5);
            this.el515.Margin = new System.Windows.Forms.Padding(0);
            this.el515.Name = "el515";
            this.el515.Size = new System.Drawing.Size(26, 32);
            this.el515.TabIndex = 134;
            this.el515.Text = "0";
            this.el515.UseVisualStyleBackColor = false;
            this.el515.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el517
            // 
            this.el517.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el517.BackColor = System.Drawing.Color.Transparent;
            this.el517.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el517.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el517.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el517.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el517.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el517.ForeColor = System.Drawing.Color.Yellow;
            this.el517.Location = new System.Drawing.Point(201, 5);
            this.el517.Margin = new System.Windows.Forms.Padding(0);
            this.el517.Name = "el517";
            this.el517.Size = new System.Drawing.Size(26, 32);
            this.el517.TabIndex = 61;
            this.el517.Text = "1";
            this.el517.UseVisualStyleBackColor = false;
            this.el517.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el518
            // 
            this.el518.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el518.BackColor = System.Drawing.Color.Transparent;
            this.el518.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el518.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el518.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el518.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el518.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el518.ForeColor = System.Drawing.Color.Yellow;
            this.el518.Location = new System.Drawing.Point(233, 5);
            this.el518.Margin = new System.Windows.Forms.Padding(0);
            this.el518.Name = "el518";
            this.el518.Size = new System.Drawing.Size(26, 32);
            this.el518.TabIndex = 62;
            this.el518.Text = "2";
            this.el518.UseVisualStyleBackColor = false;
            this.el518.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el519
            // 
            this.el519.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el519.BackColor = System.Drawing.Color.Transparent;
            this.el519.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el519.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el519.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el519.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el519.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el519.ForeColor = System.Drawing.Color.Yellow;
            this.el519.Location = new System.Drawing.Point(265, 5);
            this.el519.Margin = new System.Windows.Forms.Padding(0);
            this.el519.Name = "el519";
            this.el519.Size = new System.Drawing.Size(26, 32);
            this.el519.TabIndex = 63;
            this.el519.Text = "3";
            this.el519.UseVisualStyleBackColor = false;
            this.el519.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el520
            // 
            this.el520.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el520.BackColor = System.Drawing.Color.Transparent;
            this.el520.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el520.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el520.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el520.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el520.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el520.ForeColor = System.Drawing.Color.Yellow;
            this.el520.Location = new System.Drawing.Point(297, 5);
            this.el520.Margin = new System.Windows.Forms.Padding(0);
            this.el520.Name = "el520";
            this.el520.Size = new System.Drawing.Size(26, 32);
            this.el520.TabIndex = 103;
            this.el520.Text = "4";
            this.el520.UseVisualStyleBackColor = false;
            this.el520.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el516
            // 
            this.el516.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el516.BackColor = System.Drawing.Color.Transparent;
            this.el516.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el516.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el516.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el516.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el516.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el516.ForeColor = System.Drawing.Color.Yellow;
            this.el516.Location = new System.Drawing.Point(168, 5);
            this.el516.Margin = new System.Windows.Forms.Padding(0);
            this.el516.Name = "el516";
            this.el516.Size = new System.Drawing.Size(26, 32);
            this.el516.TabIndex = 133;
            this.el516.Text = "B";
            this.el516.UseVisualStyleBackColor = false;
            this.el516.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Dock = System.Windows.Forms.DockStyle.Left;
            this.label28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label28.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Aqua;
            this.label28.Location = new System.Drawing.Point(0, 0);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(143, 39);
            this.label28.TabIndex = 37;
            this.label28.Tag = "6";
            this.label28.Text = "CIRCLE";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panWTrav
            // 
            this.panWTrav.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panWTrav.Controls.Add(this.el521);
            this.panWTrav.Controls.Add(this.el524);
            this.panWTrav.Controls.Add(this.el523);
            this.panWTrav.Controls.Add(this.el525);
            this.panWTrav.Controls.Add(this.el526);
            this.panWTrav.Controls.Add(this.el522);
            this.panWTrav.Controls.Add(this.label26);
            this.panWTrav.Location = new System.Drawing.Point(16, 90);
            this.panWTrav.Name = "panWTrav";
            this.panWTrav.Size = new System.Drawing.Size(334, 39);
            this.panWTrav.TabIndex = 138;
            // 
            // el521
            // 
            this.el521.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el521.BackColor = System.Drawing.Color.Transparent;
            this.el521.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el521.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el521.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el521.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el521.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el521.ForeColor = System.Drawing.Color.Yellow;
            this.el521.Location = new System.Drawing.Point(135, 6);
            this.el521.Margin = new System.Windows.Forms.Padding(0);
            this.el521.Name = "el521";
            this.el521.Size = new System.Drawing.Size(26, 32);
            this.el521.TabIndex = 134;
            this.el521.Text = "0";
            this.el521.UseVisualStyleBackColor = false;
            this.el521.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el524
            // 
            this.el524.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el524.BackColor = System.Drawing.Color.Transparent;
            this.el524.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el524.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el524.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el524.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el524.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el524.ForeColor = System.Drawing.Color.Yellow;
            this.el524.Location = new System.Drawing.Point(233, 6);
            this.el524.Margin = new System.Windows.Forms.Padding(0);
            this.el524.Name = "el524";
            this.el524.Size = new System.Drawing.Size(26, 32);
            this.el524.TabIndex = 62;
            this.el524.Text = "2";
            this.el524.UseVisualStyleBackColor = false;
            this.el524.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el523
            // 
            this.el523.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el523.BackColor = System.Drawing.Color.Transparent;
            this.el523.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el523.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el523.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el523.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el523.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el523.ForeColor = System.Drawing.Color.Yellow;
            this.el523.Location = new System.Drawing.Point(201, 6);
            this.el523.Margin = new System.Windows.Forms.Padding(0);
            this.el523.Name = "el523";
            this.el523.Size = new System.Drawing.Size(26, 32);
            this.el523.TabIndex = 61;
            this.el523.Text = "1";
            this.el523.UseVisualStyleBackColor = false;
            this.el523.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el525
            // 
            this.el525.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el525.BackColor = System.Drawing.Color.Transparent;
            this.el525.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el525.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el525.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el525.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el525.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el525.ForeColor = System.Drawing.Color.Yellow;
            this.el525.Location = new System.Drawing.Point(265, 6);
            this.el525.Margin = new System.Windows.Forms.Padding(0);
            this.el525.Name = "el525";
            this.el525.Size = new System.Drawing.Size(26, 32);
            this.el525.TabIndex = 63;
            this.el525.Text = "3";
            this.el525.UseVisualStyleBackColor = false;
            this.el525.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el526
            // 
            this.el526.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el526.BackColor = System.Drawing.Color.Transparent;
            this.el526.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el526.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el526.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el526.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el526.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el526.ForeColor = System.Drawing.Color.Yellow;
            this.el526.Location = new System.Drawing.Point(297, 6);
            this.el526.Margin = new System.Windows.Forms.Padding(0);
            this.el526.Name = "el526";
            this.el526.Size = new System.Drawing.Size(26, 32);
            this.el526.TabIndex = 103;
            this.el526.Text = "4";
            this.el526.UseVisualStyleBackColor = false;
            this.el526.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el522
            // 
            this.el522.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el522.BackColor = System.Drawing.Color.Transparent;
            this.el522.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el522.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el522.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el522.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el522.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el522.ForeColor = System.Drawing.Color.Yellow;
            this.el522.Location = new System.Drawing.Point(168, 6);
            this.el522.Margin = new System.Windows.Forms.Padding(0);
            this.el522.Name = "el522";
            this.el522.Size = new System.Drawing.Size(26, 32);
            this.el522.TabIndex = 133;
            this.el522.Text = "B";
            this.el522.UseVisualStyleBackColor = false;
            this.el522.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Dock = System.Windows.Forms.DockStyle.Left;
            this.label26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label26.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label26.Location = new System.Drawing.Point(0, 0);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(125, 39);
            this.label26.TabIndex = 37;
            this.label26.Tag = "6";
            this.label26.Text = "WHEEL";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            this.label19.AutoEllipsis = true;
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Aqua;
            this.label19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(11, 22);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(142, 31);
            this.label19.TabIndex = 129;
            this.label19.Text = "Travelling";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panDance2);
            this.panel6.Controls.Add(this.panPrecision2);
            this.panel6.Controls.Add(this.log);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(851, 46);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(359, 719);
            this.panel6.TabIndex = 12;
            // 
            // panDance2
            // 
            this.panDance2.Controls.Add(this.gbSteps);
            this.panDance2.Controls.Add(this.gbSeq);
            this.panDance2.Location = new System.Drawing.Point(2, 74);
            this.panDance2.Name = "panDance2";
            this.panDance2.Size = new System.Drawing.Size(357, 526);
            this.panDance2.TabIndex = 78;
            this.panDance2.Visible = false;
            // 
            // gbSteps
            // 
            this.gbSteps.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.gbSteps.Controls.Add(this.panStepHold);
            this.gbSteps.Controls.Add(this.panChoreo);
            this.gbSteps.Controls.Add(this.panStraight);
            this.gbSteps.Controls.Add(this.panCircular);
            this.gbSteps.Controls.Add(this.panStepNoHold);
            this.gbSteps.Controls.Add(this.label6);
            this.gbSteps.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbSteps.Location = new System.Drawing.Point(0, 234);
            this.gbSteps.Name = "gbSteps";
            this.gbSteps.Size = new System.Drawing.Size(357, 292);
            this.gbSteps.TabIndex = 74;
            this.gbSteps.TabStop = false;
            // 
            // panStepHold
            // 
            this.panStepHold.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panStepHold.Controls.Add(this.el308);
            this.panStepHold.Controls.Add(this.el291);
            this.panStepHold.Controls.Add(this.el292);
            this.panStepHold.Controls.Add(this.el293);
            this.panStepHold.Controls.Add(this.el294);
            this.panStepHold.Controls.Add(this.el295);
            this.panStepHold.Controls.Add(this.labNoHold);
            this.panStepHold.Location = new System.Drawing.Point(12, 227);
            this.panStepHold.Name = "panStepHold";
            this.panStepHold.Size = new System.Drawing.Size(334, 46);
            this.panStepHold.TabIndex = 193;
            // 
            // el308
            // 
            this.el308.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el308.BackColor = System.Drawing.Color.Transparent;
            this.el308.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el308.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el308.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el308.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el308.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el308.ForeColor = System.Drawing.Color.Yellow;
            this.el308.Location = new System.Drawing.Point(136, 5);
            this.el308.Margin = new System.Windows.Forms.Padding(0);
            this.el308.Name = "el308";
            this.el308.Size = new System.Drawing.Size(26, 32);
            this.el308.TabIndex = 181;
            this.el308.Text = "0";
            this.el308.UseVisualStyleBackColor = false;
            this.el308.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el291
            // 
            this.el291.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el291.BackColor = System.Drawing.Color.Transparent;
            this.el291.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el291.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el291.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el291.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el291.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el291.ForeColor = System.Drawing.Color.Yellow;
            this.el291.Location = new System.Drawing.Point(169, 5);
            this.el291.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el291.Name = "el291";
            this.el291.Size = new System.Drawing.Size(26, 32);
            this.el291.TabIndex = 171;
            this.el291.Text = "1";
            this.el291.UseVisualStyleBackColor = false;
            this.el291.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el292
            // 
            this.el292.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el292.BackColor = System.Drawing.Color.Transparent;
            this.el292.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el292.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el292.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el292.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el292.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el292.ForeColor = System.Drawing.Color.Yellow;
            this.el292.Location = new System.Drawing.Point(201, 5);
            this.el292.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el292.Name = "el292";
            this.el292.Size = new System.Drawing.Size(26, 32);
            this.el292.TabIndex = 172;
            this.el292.Text = "2";
            this.el292.UseVisualStyleBackColor = false;
            this.el292.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el293
            // 
            this.el293.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el293.BackColor = System.Drawing.Color.Transparent;
            this.el293.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el293.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el293.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el293.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el293.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el293.ForeColor = System.Drawing.Color.Yellow;
            this.el293.Location = new System.Drawing.Point(233, 5);
            this.el293.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el293.Name = "el293";
            this.el293.Size = new System.Drawing.Size(26, 32);
            this.el293.TabIndex = 173;
            this.el293.Text = "3";
            this.el293.UseVisualStyleBackColor = false;
            this.el293.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el294
            // 
            this.el294.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el294.BackColor = System.Drawing.Color.Transparent;
            this.el294.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el294.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el294.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el294.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el294.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el294.ForeColor = System.Drawing.Color.Yellow;
            this.el294.Location = new System.Drawing.Point(265, 5);
            this.el294.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el294.Name = "el294";
            this.el294.Size = new System.Drawing.Size(26, 32);
            this.el294.TabIndex = 174;
            this.el294.Text = "4";
            this.el294.UseVisualStyleBackColor = false;
            this.el294.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el295
            // 
            this.el295.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el295.BackColor = System.Drawing.Color.Transparent;
            this.el295.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el295.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el295.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el295.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el295.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el295.ForeColor = System.Drawing.Color.Yellow;
            this.el295.Location = new System.Drawing.Point(297, 5);
            this.el295.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el295.Name = "el295";
            this.el295.Size = new System.Drawing.Size(26, 32);
            this.el295.TabIndex = 176;
            this.el295.Text = "5";
            this.el295.UseVisualStyleBackColor = false;
            this.el295.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // labNoHold
            // 
            this.labNoHold.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labNoHold.BackColor = System.Drawing.Color.Transparent;
            this.labNoHold.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labNoHold.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNoHold.ForeColor = System.Drawing.Color.HotPink;
            this.labNoHold.Location = new System.Drawing.Point(-2, 3);
            this.labNoHold.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labNoHold.Name = "labNoHold";
            this.labNoHold.Size = new System.Drawing.Size(249, 36);
            this.labNoHold.TabIndex = 145;
            this.labNoHold.Text = "HOLD";
            this.labNoHold.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panChoreo
            // 
            this.panChoreo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panChoreo.Controls.Add(this.el640);
            this.panChoreo.Controls.Add(this.el639);
            this.panChoreo.Controls.Add(this.label14);
            this.panChoreo.Location = new System.Drawing.Point(12, 147);
            this.panChoreo.Name = "panChoreo";
            this.panChoreo.Size = new System.Drawing.Size(212, 39);
            this.panChoreo.TabIndex = 192;
            // 
            // el640
            // 
            this.el640.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el640.BackColor = System.Drawing.Color.Transparent;
            this.el640.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el640.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el640.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el640.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el640.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el640.ForeColor = System.Drawing.Color.Yellow;
            this.el640.Location = new System.Drawing.Point(169, 4);
            this.el640.Margin = new System.Windows.Forms.Padding(0);
            this.el640.Name = "el640";
            this.el640.Size = new System.Drawing.Size(26, 32);
            this.el640.TabIndex = 189;
            this.el640.Text = "1";
            this.el640.UseVisualStyleBackColor = false;
            this.el640.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el639
            // 
            this.el639.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el639.BackColor = System.Drawing.Color.Transparent;
            this.el639.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el639.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el639.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el639.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el639.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el639.ForeColor = System.Drawing.Color.Yellow;
            this.el639.Location = new System.Drawing.Point(136, 4);
            this.el639.Margin = new System.Windows.Forms.Padding(0);
            this.el639.Name = "el639";
            this.el639.Size = new System.Drawing.Size(26, 32);
            this.el639.TabIndex = 188;
            this.el639.Text = "0";
            this.el639.UseVisualStyleBackColor = false;
            this.el639.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label14.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(-2, 0);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(214, 36);
            this.label14.TabIndex = 187;
            this.label14.Tag = "6";
            this.label14.Text = "CHOREO";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panStraight
            // 
            this.panStraight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panStraight.Controls.Add(this.el303);
            this.panStraight.Controls.Add(this.el304);
            this.panStraight.Controls.Add(this.el302);
            this.panStraight.Controls.Add(this.el301);
            this.panStraight.Controls.Add(this.el305);
            this.panStraight.Controls.Add(this.el611);
            this.panStraight.Controls.Add(this.label7);
            this.panStraight.Location = new System.Drawing.Point(12, 105);
            this.panStraight.Name = "panStraight";
            this.panStraight.Size = new System.Drawing.Size(331, 41);
            this.panStraight.TabIndex = 191;
            // 
            // el303
            // 
            this.el303.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el303.BackColor = System.Drawing.Color.Transparent;
            this.el303.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el303.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el303.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el303.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el303.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el303.ForeColor = System.Drawing.Color.Yellow;
            this.el303.Location = new System.Drawing.Point(233, 4);
            this.el303.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el303.Name = "el303";
            this.el303.Size = new System.Drawing.Size(26, 32);
            this.el303.TabIndex = 183;
            this.el303.Text = "3";
            this.el303.UseVisualStyleBackColor = false;
            this.el303.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el304
            // 
            this.el304.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el304.BackColor = System.Drawing.Color.Transparent;
            this.el304.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el304.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el304.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el304.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el304.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el304.ForeColor = System.Drawing.Color.Yellow;
            this.el304.Location = new System.Drawing.Point(265, 4);
            this.el304.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el304.Name = "el304";
            this.el304.Size = new System.Drawing.Size(26, 32);
            this.el304.TabIndex = 184;
            this.el304.Text = "4";
            this.el304.UseVisualStyleBackColor = false;
            this.el304.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el302
            // 
            this.el302.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el302.BackColor = System.Drawing.Color.Transparent;
            this.el302.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el302.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el302.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el302.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el302.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el302.ForeColor = System.Drawing.Color.Yellow;
            this.el302.Location = new System.Drawing.Point(201, 4);
            this.el302.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el302.Name = "el302";
            this.el302.Size = new System.Drawing.Size(26, 32);
            this.el302.TabIndex = 182;
            this.el302.Text = "2";
            this.el302.UseVisualStyleBackColor = false;
            this.el302.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el301
            // 
            this.el301.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el301.BackColor = System.Drawing.Color.Transparent;
            this.el301.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el301.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el301.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el301.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el301.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el301.ForeColor = System.Drawing.Color.Yellow;
            this.el301.Location = new System.Drawing.Point(169, 4);
            this.el301.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el301.Name = "el301";
            this.el301.Size = new System.Drawing.Size(26, 32);
            this.el301.TabIndex = 181;
            this.el301.Text = "1";
            this.el301.UseVisualStyleBackColor = false;
            this.el301.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el305
            // 
            this.el305.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el305.BackColor = System.Drawing.Color.Transparent;
            this.el305.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el305.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el305.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el305.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el305.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el305.ForeColor = System.Drawing.Color.Yellow;
            this.el305.Location = new System.Drawing.Point(297, 4);
            this.el305.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el305.Name = "el305";
            this.el305.Size = new System.Drawing.Size(26, 32);
            this.el305.TabIndex = 185;
            this.el305.Text = "5";
            this.el305.UseVisualStyleBackColor = false;
            this.el305.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el611
            // 
            this.el611.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el611.BackColor = System.Drawing.Color.Transparent;
            this.el611.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el611.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el611.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el611.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el611.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el611.ForeColor = System.Drawing.Color.Yellow;
            this.el611.Location = new System.Drawing.Point(136, 5);
            this.el611.Margin = new System.Windows.Forms.Padding(0);
            this.el611.Name = "el611";
            this.el611.Size = new System.Drawing.Size(26, 32);
            this.el611.TabIndex = 186;
            this.el611.Text = "0";
            this.el611.UseVisualStyleBackColor = false;
            this.el611.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Magenta;
            this.label7.Location = new System.Drawing.Point(-2, 2);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(225, 36);
            this.label7.TabIndex = 38;
            this.label7.Tag = "6";
            this.label7.Text = "STRAIGHT";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panCircular
            // 
            this.panCircular.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panCircular.Controls.Add(this.el306);
            this.panCircular.Controls.Add(this.el252);
            this.panCircular.Controls.Add(this.el251);
            this.panCircular.Controls.Add(this.el250);
            this.panCircular.Controls.Add(this.el249);
            this.panCircular.Controls.Add(this.el253);
            this.panCircular.Controls.Add(this.label8);
            this.panCircular.Location = new System.Drawing.Point(12, 67);
            this.panCircular.Name = "panCircular";
            this.panCircular.Size = new System.Drawing.Size(332, 39);
            this.panCircular.TabIndex = 190;
            // 
            // el306
            // 
            this.el306.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el306.BackColor = System.Drawing.Color.Transparent;
            this.el306.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el306.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el306.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el306.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el306.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el306.ForeColor = System.Drawing.Color.Yellow;
            this.el306.Location = new System.Drawing.Point(136, 3);
            this.el306.Margin = new System.Windows.Forms.Padding(0);
            this.el306.Name = "el306";
            this.el306.Size = new System.Drawing.Size(26, 32);
            this.el306.TabIndex = 180;
            this.el306.Text = "0";
            this.el306.UseVisualStyleBackColor = false;
            this.el306.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el252
            // 
            this.el252.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el252.BackColor = System.Drawing.Color.Transparent;
            this.el252.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el252.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el252.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el252.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el252.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el252.ForeColor = System.Drawing.Color.Yellow;
            this.el252.Location = new System.Drawing.Point(265, 3);
            this.el252.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el252.Name = "el252";
            this.el252.Size = new System.Drawing.Size(26, 32);
            this.el252.TabIndex = 61;
            this.el252.Text = "4";
            this.el252.UseVisualStyleBackColor = false;
            this.el252.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el251
            // 
            this.el251.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el251.BackColor = System.Drawing.Color.Transparent;
            this.el251.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el251.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el251.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el251.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el251.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el251.ForeColor = System.Drawing.Color.Yellow;
            this.el251.Location = new System.Drawing.Point(233, 3);
            this.el251.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el251.Name = "el251";
            this.el251.Size = new System.Drawing.Size(26, 32);
            this.el251.TabIndex = 60;
            this.el251.Text = "3";
            this.el251.UseVisualStyleBackColor = false;
            this.el251.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el250
            // 
            this.el250.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el250.BackColor = System.Drawing.Color.Transparent;
            this.el250.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el250.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el250.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el250.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el250.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el250.ForeColor = System.Drawing.Color.Yellow;
            this.el250.Location = new System.Drawing.Point(201, 3);
            this.el250.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el250.Name = "el250";
            this.el250.Size = new System.Drawing.Size(26, 32);
            this.el250.TabIndex = 59;
            this.el250.Text = "2";
            this.el250.UseVisualStyleBackColor = false;
            this.el250.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el249
            // 
            this.el249.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el249.BackColor = System.Drawing.Color.Transparent;
            this.el249.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el249.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el249.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el249.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el249.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el249.ForeColor = System.Drawing.Color.Yellow;
            this.el249.Location = new System.Drawing.Point(169, 3);
            this.el249.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el249.Name = "el249";
            this.el249.Size = new System.Drawing.Size(26, 32);
            this.el249.TabIndex = 58;
            this.el249.Text = "1";
            this.el249.UseVisualStyleBackColor = false;
            this.el249.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el253
            // 
            this.el253.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el253.BackColor = System.Drawing.Color.Transparent;
            this.el253.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el253.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el253.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el253.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el253.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el253.ForeColor = System.Drawing.Color.Yellow;
            this.el253.Location = new System.Drawing.Point(297, 3);
            this.el253.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el253.Name = "el253";
            this.el253.Size = new System.Drawing.Size(26, 32);
            this.el253.TabIndex = 62;
            this.el253.Text = "5";
            this.el253.UseVisualStyleBackColor = false;
            this.el253.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Thistle;
            this.label8.Location = new System.Drawing.Point(-2, 1);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(148, 36);
            this.label8.TabIndex = 178;
            this.label8.Tag = "6";
            this.label8.Text = "CIRCULAR";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panStepNoHold
            // 
            this.panStepNoHold.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panStepNoHold.Controls.Add(this.el309);
            this.panStepNoHold.Controls.Add(this.el290);
            this.panStepNoHold.Controls.Add(this.el287);
            this.panStepNoHold.Controls.Add(this.el288);
            this.panStepNoHold.Controls.Add(this.el289);
            this.panStepNoHold.Controls.Add(this.el286);
            this.panStepNoHold.Controls.Add(this.labHOld);
            this.panStepNoHold.Location = new System.Drawing.Point(12, 187);
            this.panStepNoHold.Name = "panStepNoHold";
            this.panStepNoHold.Size = new System.Drawing.Size(335, 41);
            this.panStepNoHold.TabIndex = 177;
            // 
            // el309
            // 
            this.el309.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el309.BackColor = System.Drawing.Color.Transparent;
            this.el309.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el309.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el309.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el309.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el309.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el309.ForeColor = System.Drawing.Color.Yellow;
            this.el309.Location = new System.Drawing.Point(136, 5);
            this.el309.Margin = new System.Windows.Forms.Padding(0);
            this.el309.Name = "el309";
            this.el309.Size = new System.Drawing.Size(26, 32);
            this.el309.TabIndex = 180;
            this.el309.Text = "0";
            this.el309.UseVisualStyleBackColor = false;
            this.el309.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el290
            // 
            this.el290.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el290.BackColor = System.Drawing.Color.Transparent;
            this.el290.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el290.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el290.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el290.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el290.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el290.ForeColor = System.Drawing.Color.Yellow;
            this.el290.Location = new System.Drawing.Point(297, 5);
            this.el290.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el290.Name = "el290";
            this.el290.Size = new System.Drawing.Size(26, 32);
            this.el290.TabIndex = 175;
            this.el290.Text = "5";
            this.el290.UseVisualStyleBackColor = false;
            this.el290.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el287
            // 
            this.el287.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el287.BackColor = System.Drawing.Color.Transparent;
            this.el287.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el287.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el287.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el287.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el287.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el287.ForeColor = System.Drawing.Color.Yellow;
            this.el287.Location = new System.Drawing.Point(201, 5);
            this.el287.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el287.Name = "el287";
            this.el287.Size = new System.Drawing.Size(26, 32);
            this.el287.TabIndex = 168;
            this.el287.Text = "2";
            this.el287.UseVisualStyleBackColor = false;
            this.el287.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el288
            // 
            this.el288.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el288.BackColor = System.Drawing.Color.Transparent;
            this.el288.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el288.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el288.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el288.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el288.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el288.ForeColor = System.Drawing.Color.Yellow;
            this.el288.Location = new System.Drawing.Point(233, 5);
            this.el288.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el288.Name = "el288";
            this.el288.Size = new System.Drawing.Size(26, 32);
            this.el288.TabIndex = 169;
            this.el288.Text = "3";
            this.el288.UseVisualStyleBackColor = false;
            this.el288.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el289
            // 
            this.el289.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el289.BackColor = System.Drawing.Color.Transparent;
            this.el289.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el289.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el289.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el289.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el289.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el289.ForeColor = System.Drawing.Color.Yellow;
            this.el289.Location = new System.Drawing.Point(265, 5);
            this.el289.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el289.Name = "el289";
            this.el289.Size = new System.Drawing.Size(26, 32);
            this.el289.TabIndex = 170;
            this.el289.Text = "4";
            this.el289.UseVisualStyleBackColor = false;
            this.el289.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el286
            // 
            this.el286.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el286.BackColor = System.Drawing.Color.Transparent;
            this.el286.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el286.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el286.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el286.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el286.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el286.ForeColor = System.Drawing.Color.Yellow;
            this.el286.Location = new System.Drawing.Point(169, 5);
            this.el286.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el286.Name = "el286";
            this.el286.Size = new System.Drawing.Size(26, 32);
            this.el286.TabIndex = 167;
            this.el286.Text = "1";
            this.el286.UseVisualStyleBackColor = false;
            this.el286.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // labHOld
            // 
            this.labHOld.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labHOld.BackColor = System.Drawing.Color.Transparent;
            this.labHOld.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labHOld.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labHOld.ForeColor = System.Drawing.Color.Pink;
            this.labHOld.Location = new System.Drawing.Point(-2, 5);
            this.labHOld.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labHOld.Name = "labHOld";
            this.labHOld.Size = new System.Drawing.Size(267, 36);
            this.labHOld.TabIndex = 143;
            this.labHOld.Text = "NO HOLD";
            this.labHOld.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoEllipsis = true;
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(267, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 36);
            this.label6.TabIndex = 176;
            this.label6.Text = "Steps";
            // 
            // gbSeq
            // 
            this.gbSeq.BackColor = System.Drawing.Color.Transparent;
            this.gbSeq.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.gbSeq.Controls.Add(this.panNoHold);
            this.gbSeq.Controls.Add(this.panHold);
            this.gbSeq.Controls.Add(this.panTrav);
            this.gbSeq.Controls.Add(this.label35);
            this.gbSeq.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbSeq.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbSeq.Location = new System.Drawing.Point(0, 0);
            this.gbSeq.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gbSeq.Name = "gbSeq";
            this.gbSeq.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gbSeq.Size = new System.Drawing.Size(357, 234);
            this.gbSeq.TabIndex = 63;
            this.gbSeq.TabStop = false;
            this.gbSeq.Tag = "1";
            // 
            // panNoHold
            // 
            this.panNoHold.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panNoHold.Controls.Add(this.el307);
            this.panNoHold.Controls.Add(this.el297);
            this.panNoHold.Controls.Add(this.el299);
            this.panNoHold.Controls.Add(this.el296);
            this.panNoHold.Controls.Add(this.el298);
            this.panNoHold.Controls.Add(this.lab1);
            this.panNoHold.Controls.Add(this.label4);
            this.panNoHold.Location = new System.Drawing.Point(16, 116);
            this.panNoHold.Name = "panNoHold";
            this.panNoHold.Size = new System.Drawing.Size(320, 40);
            this.panNoHold.TabIndex = 180;
            // 
            // el307
            // 
            this.el307.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el307.BackColor = System.Drawing.Color.Transparent;
            this.el307.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el307.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el307.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el307.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el307.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el307.ForeColor = System.Drawing.Color.Yellow;
            this.el307.Location = new System.Drawing.Point(163, 6);
            this.el307.Margin = new System.Windows.Forms.Padding(0);
            this.el307.Name = "el307";
            this.el307.Size = new System.Drawing.Size(26, 32);
            this.el307.TabIndex = 179;
            this.el307.Text = "0";
            this.el307.UseVisualStyleBackColor = false;
            this.el307.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el297
            // 
            this.el297.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el297.BackColor = System.Drawing.Color.Transparent;
            this.el297.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el297.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el297.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el297.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el297.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el297.ForeColor = System.Drawing.Color.Yellow;
            this.el297.Location = new System.Drawing.Point(227, 6);
            this.el297.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el297.Name = "el297";
            this.el297.Size = new System.Drawing.Size(26, 32);
            this.el297.TabIndex = 43;
            this.el297.Text = "2";
            this.el297.UseVisualStyleBackColor = false;
            this.el297.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el299
            // 
            this.el299.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el299.BackColor = System.Drawing.Color.Transparent;
            this.el299.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el299.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el299.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el299.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el299.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el299.ForeColor = System.Drawing.Color.Yellow;
            this.el299.Location = new System.Drawing.Point(291, 6);
            this.el299.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el299.Name = "el299";
            this.el299.Size = new System.Drawing.Size(26, 32);
            this.el299.TabIndex = 45;
            this.el299.Text = "4";
            this.el299.UseVisualStyleBackColor = false;
            this.el299.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el296
            // 
            this.el296.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el296.BackColor = System.Drawing.Color.Transparent;
            this.el296.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el296.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el296.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el296.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el296.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el296.ForeColor = System.Drawing.Color.Yellow;
            this.el296.Location = new System.Drawing.Point(195, 6);
            this.el296.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el296.Name = "el296";
            this.el296.Size = new System.Drawing.Size(26, 32);
            this.el296.TabIndex = 42;
            this.el296.Text = "1";
            this.el296.UseVisualStyleBackColor = false;
            this.el296.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el298
            // 
            this.el298.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el298.BackColor = System.Drawing.Color.Transparent;
            this.el298.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el298.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el298.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el298.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el298.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el298.ForeColor = System.Drawing.Color.Yellow;
            this.el298.Location = new System.Drawing.Point(259, 6);
            this.el298.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el298.Name = "el298";
            this.el298.Size = new System.Drawing.Size(26, 32);
            this.el298.TabIndex = 44;
            this.el298.Text = "3";
            this.el298.UseVisualStyleBackColor = false;
            this.el298.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // lab1
            // 
            this.lab1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lab1.BackColor = System.Drawing.Color.Transparent;
            this.lab1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lab1.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab1.ForeColor = System.Drawing.Color.Gold;
            this.lab1.Location = new System.Drawing.Point(3, 19);
            this.lab1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lab1.Name = "lab1";
            this.lab1.Size = new System.Drawing.Size(137, 24);
            this.lab1.TabIndex = 167;
            this.lab1.Tag = "6";
            this.lab1.Text = "No hold synchronized";
            this.lab1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Gold;
            this.label4.Location = new System.Drawing.Point(1, -9);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(180, 36);
            this.label4.TabIndex = 142;
            this.label4.Tag = "6";
            this.label4.Text = "CLUSTER";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panHold
            // 
            this.panHold.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panHold.Controls.Add(this.el634);
            this.panHold.Controls.Add(this.el328);
            this.panHold.Controls.Add(this.el330);
            this.panHold.Controls.Add(this.el327);
            this.panHold.Controls.Add(this.el329);
            this.panHold.Controls.Add(this.label9);
            this.panHold.Controls.Add(this.label5);
            this.panHold.Location = new System.Drawing.Point(16, 158);
            this.panHold.Name = "panHold";
            this.panHold.Size = new System.Drawing.Size(321, 58);
            this.panHold.TabIndex = 179;
            // 
            // el634
            // 
            this.el634.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el634.BackColor = System.Drawing.Color.Transparent;
            this.el634.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el634.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el634.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el634.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el634.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el634.ForeColor = System.Drawing.Color.Yellow;
            this.el634.Location = new System.Drawing.Point(163, 5);
            this.el634.Margin = new System.Windows.Forms.Padding(0);
            this.el634.Name = "el634";
            this.el634.Size = new System.Drawing.Size(26, 32);
            this.el634.TabIndex = 186;
            this.el634.Text = "0";
            this.el634.UseVisualStyleBackColor = false;
            this.el634.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el328
            // 
            this.el328.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el328.BackColor = System.Drawing.Color.Transparent;
            this.el328.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el328.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el328.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el328.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el328.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el328.ForeColor = System.Drawing.Color.Yellow;
            this.el328.Location = new System.Drawing.Point(227, 5);
            this.el328.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el328.Name = "el328";
            this.el328.Size = new System.Drawing.Size(26, 32);
            this.el328.TabIndex = 181;
            this.el328.Text = "2";
            this.el328.UseVisualStyleBackColor = false;
            this.el328.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el330
            // 
            this.el330.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el330.BackColor = System.Drawing.Color.Transparent;
            this.el330.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el330.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el330.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el330.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el330.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el330.ForeColor = System.Drawing.Color.Yellow;
            this.el330.Location = new System.Drawing.Point(291, 5);
            this.el330.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el330.Name = "el330";
            this.el330.Size = new System.Drawing.Size(26, 32);
            this.el330.TabIndex = 183;
            this.el330.Text = "4";
            this.el330.UseVisualStyleBackColor = false;
            this.el330.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el327
            // 
            this.el327.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el327.BackColor = System.Drawing.Color.Transparent;
            this.el327.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el327.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el327.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el327.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el327.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el327.ForeColor = System.Drawing.Color.Yellow;
            this.el327.Location = new System.Drawing.Point(195, 5);
            this.el327.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el327.Name = "el327";
            this.el327.Size = new System.Drawing.Size(26, 32);
            this.el327.TabIndex = 180;
            this.el327.Text = "1";
            this.el327.UseVisualStyleBackColor = false;
            this.el327.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el329
            // 
            this.el329.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el329.BackColor = System.Drawing.Color.Transparent;
            this.el329.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el329.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el329.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el329.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el329.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el329.ForeColor = System.Drawing.Color.Yellow;
            this.el329.Location = new System.Drawing.Point(259, 5);
            this.el329.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el329.Name = "el329";
            this.el329.Size = new System.Drawing.Size(26, 32);
            this.el329.TabIndex = 182;
            this.el329.Text = "3";
            this.el329.UseVisualStyleBackColor = false;
            this.el329.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label9.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Goldenrod;
            this.label9.Location = new System.Drawing.Point(1, -1);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(194, 36);
            this.label9.TabIndex = 184;
            this.label9.Tag = "6";
            this.label9.Text = "CLUSTER";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Goldenrod;
            this.label5.Location = new System.Drawing.Point(3, 26);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 24);
            this.label5.TabIndex = 185;
            this.label5.Tag = "6";
            this.label5.Text = "Hold synchronized";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panTrav
            // 
            this.panTrav.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panTrav.Controls.Add(this.el312);
            this.panTrav.Controls.Add(this.el245);
            this.panTrav.Controls.Add(this.el248);
            this.panTrav.Controls.Add(this.el246);
            this.panTrav.Controls.Add(this.el247);
            this.panTrav.Controls.Add(this.label3);
            this.panTrav.Location = new System.Drawing.Point(17, 76);
            this.panTrav.Name = "panTrav";
            this.panTrav.Size = new System.Drawing.Size(318, 38);
            this.panTrav.TabIndex = 178;
            // 
            // el312
            // 
            this.el312.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el312.BackColor = System.Drawing.Color.Transparent;
            this.el312.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el312.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el312.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el312.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el312.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el312.ForeColor = System.Drawing.Color.Yellow;
            this.el312.Location = new System.Drawing.Point(162, 5);
            this.el312.Margin = new System.Windows.Forms.Padding(0);
            this.el312.Name = "el312";
            this.el312.Size = new System.Drawing.Size(26, 32);
            this.el312.TabIndex = 178;
            this.el312.Text = "0";
            this.el312.UseVisualStyleBackColor = false;
            this.el312.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el245
            // 
            this.el245.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el245.BackColor = System.Drawing.Color.Transparent;
            this.el245.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el245.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el245.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el245.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el245.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el245.ForeColor = System.Drawing.Color.Yellow;
            this.el245.Location = new System.Drawing.Point(194, 5);
            this.el245.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el245.Name = "el245";
            this.el245.Size = new System.Drawing.Size(26, 32);
            this.el245.TabIndex = 52;
            this.el245.Text = "1";
            this.el245.UseVisualStyleBackColor = false;
            this.el245.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el248
            // 
            this.el248.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el248.BackColor = System.Drawing.Color.Transparent;
            this.el248.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el248.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el248.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el248.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el248.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el248.ForeColor = System.Drawing.Color.Yellow;
            this.el248.Location = new System.Drawing.Point(290, 5);
            this.el248.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el248.Name = "el248";
            this.el248.Size = new System.Drawing.Size(26, 32);
            this.el248.TabIndex = 55;
            this.el248.Text = "4";
            this.el248.UseVisualStyleBackColor = false;
            this.el248.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el246
            // 
            this.el246.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el246.BackColor = System.Drawing.Color.Transparent;
            this.el246.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el246.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el246.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el246.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el246.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el246.ForeColor = System.Drawing.Color.Yellow;
            this.el246.Location = new System.Drawing.Point(226, 5);
            this.el246.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el246.Name = "el246";
            this.el246.Size = new System.Drawing.Size(26, 32);
            this.el246.TabIndex = 53;
            this.el246.Text = "2";
            this.el246.UseVisualStyleBackColor = false;
            this.el246.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el247
            // 
            this.el247.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el247.BackColor = System.Drawing.Color.Transparent;
            this.el247.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el247.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el247.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el247.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el247.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el247.ForeColor = System.Drawing.Color.Yellow;
            this.el247.Location = new System.Drawing.Point(258, 5);
            this.el247.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el247.Name = "el247";
            this.el247.Size = new System.Drawing.Size(26, 32);
            this.el247.TabIndex = 54;
            this.el247.Text = "3";
            this.el247.UseVisualStyleBackColor = false;
            this.el247.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label3.Location = new System.Drawing.Point(0, -1);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(184, 36);
            this.label3.TabIndex = 38;
            this.label3.Tag = "6";
            this.label3.Text = "TRAVELLING";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label35
            // 
            this.label35.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label35.AutoEllipsis = true;
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label35.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(203, 24);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(173, 36);
            this.label35.TabIndex = 147;
            this.label35.Text = "Sequences";
            // 
            // panPrecision2
            // 
            this.panPrecision2.Controls.Add(this.gbPrecision);
            this.panPrecision2.Controls.Add(this.gbLinear);
            this.panPrecision2.Location = new System.Drawing.Point(8, 3);
            this.panPrecision2.Name = "panPrecision2";
            this.panPrecision2.Size = new System.Drawing.Size(351, 486);
            this.panPrecision2.TabIndex = 77;
            this.panPrecision2.Visible = false;
            // 
            // gbPrecision
            // 
            this.gbPrecision.BackColor = System.Drawing.Color.Transparent;
            this.gbPrecision.Controls.Add(this.panCInters);
            this.gbPrecision.Controls.Add(this.panPChoreo);
            this.gbPrecision.Controls.Add(this.panBNH);
            this.gbPrecision.Controls.Add(this.panInters);
            this.gbPrecision.Controls.Add(this.panComb);
            this.gbPrecision.Controls.Add(this.label40);
            this.gbPrecision.Cursor = System.Windows.Forms.Cursors.Default;
            this.gbPrecision.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbPrecision.Enabled = false;
            this.gbPrecision.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbPrecision.Font = new System.Drawing.Font("Arial Narrow", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbPrecision.ForeColor = System.Drawing.Color.Yellow;
            this.gbPrecision.Location = new System.Drawing.Point(0, 188);
            this.gbPrecision.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gbPrecision.Name = "gbPrecision";
            this.gbPrecision.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gbPrecision.Size = new System.Drawing.Size(351, 285);
            this.gbPrecision.TabIndex = 9;
            this.gbPrecision.TabStop = false;
            this.gbPrecision.Tag = "2";
            // 
            // panCInters
            // 
            this.panCInters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panCInters.Controls.Add(this.el563);
            this.panCInters.Controls.Add(this.el564);
            this.panCInters.Controls.Add(this.label41);
            this.panCInters.Location = new System.Drawing.Point(21, 144);
            this.panCInters.Name = "panCInters";
            this.panCInters.Size = new System.Drawing.Size(313, 43);
            this.panCInters.TabIndex = 191;
            this.panCInters.Paint += new System.Windows.Forms.PaintEventHandler(this.panCInters_Paint);
            // 
            // el563
            // 
            this.el563.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el563.BackColor = System.Drawing.Color.Transparent;
            this.el563.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el563.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el563.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el563.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el563.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el563.ForeColor = System.Drawing.Color.Yellow;
            this.el563.Location = new System.Drawing.Point(125, 5);
            this.el563.Margin = new System.Windows.Forms.Padding(0);
            this.el563.Name = "el563";
            this.el563.Size = new System.Drawing.Size(26, 32);
            this.el563.TabIndex = 132;
            this.el563.Text = "0";
            this.el563.UseVisualStyleBackColor = false;
            this.el563.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el564
            // 
            this.el564.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el564.BackColor = System.Drawing.Color.Transparent;
            this.el564.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el564.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el564.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el564.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el564.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el564.ForeColor = System.Drawing.Color.Yellow;
            this.el564.Location = new System.Drawing.Point(158, 5);
            this.el564.Margin = new System.Windows.Forms.Padding(0);
            this.el564.Name = "el564";
            this.el564.Size = new System.Drawing.Size(26, 32);
            this.el564.TabIndex = 61;
            this.el564.Text = "B";
            this.el564.UseVisualStyleBackColor = false;
            this.el564.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label41
            // 
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Dock = System.Windows.Forms.DockStyle.Left;
            this.label41.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label41.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label41.Location = new System.Drawing.Point(0, 0);
            this.label41.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(123, 43);
            this.label41.TabIndex = 66;
            this.label41.Text = "Creative INTERSECTION";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panPChoreo
            // 
            this.panPChoreo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panPChoreo.Controls.Add(this.el513);
            this.panPChoreo.Controls.Add(this.el514);
            this.panPChoreo.Controls.Add(this.label39);
            this.panPChoreo.ForeColor = System.Drawing.Color.White;
            this.panPChoreo.Location = new System.Drawing.Point(21, 226);
            this.panPChoreo.Name = "panPChoreo";
            this.panPChoreo.Size = new System.Drawing.Size(313, 39);
            this.panPChoreo.TabIndex = 192;
            this.panPChoreo.Paint += new System.Windows.Forms.PaintEventHandler(this.panPChoreo_Paint);
            // 
            // el513
            // 
            this.el513.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el513.BackColor = System.Drawing.Color.Transparent;
            this.el513.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el513.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el513.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el513.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el513.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el513.ForeColor = System.Drawing.Color.Yellow;
            this.el513.Location = new System.Drawing.Point(125, 3);
            this.el513.Margin = new System.Windows.Forms.Padding(0);
            this.el513.Name = "el513";
            this.el513.Size = new System.Drawing.Size(26, 32);
            this.el513.TabIndex = 132;
            this.el513.Text = "0";
            this.el513.UseVisualStyleBackColor = false;
            this.el513.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el514
            // 
            this.el514.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el514.BackColor = System.Drawing.Color.Transparent;
            this.el514.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el514.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el514.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el514.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el514.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el514.ForeColor = System.Drawing.Color.Yellow;
            this.el514.Location = new System.Drawing.Point(158, 3);
            this.el514.Margin = new System.Windows.Forms.Padding(0);
            this.el514.Name = "el514";
            this.el514.Size = new System.Drawing.Size(26, 32);
            this.el514.TabIndex = 61;
            this.el514.Text = "B";
            this.el514.UseVisualStyleBackColor = false;
            this.el514.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label39
            // 
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Dock = System.Windows.Forms.DockStyle.Left;
            this.label39.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label39.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.White;
            this.label39.Location = new System.Drawing.Point(0, 0);
            this.label39.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(156, 39);
            this.label39.TabIndex = 186;
            this.label39.Text = "CREATIVE";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panBNH
            // 
            this.panBNH.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panBNH.Controls.Add(this.el500);
            this.panBNH.Controls.Add(this.label21);
            this.panBNH.Controls.Add(this.el501);
            this.panBNH.Controls.Add(this.el502);
            this.panBNH.Controls.Add(this.el503);
            this.panBNH.Controls.Add(this.el504);
            this.panBNH.Controls.Add(this.el505);
            this.panBNH.Location = new System.Drawing.Point(21, 64);
            this.panBNH.Name = "panBNH";
            this.panBNH.Size = new System.Drawing.Size(313, 39);
            this.panBNH.TabIndex = 184;
            this.panBNH.Paint += new System.Windows.Forms.PaintEventHandler(this.panBNH_Paint);
            // 
            // el500
            // 
            this.el500.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el500.BackColor = System.Drawing.Color.Transparent;
            this.el500.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el500.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el500.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el500.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el500.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el500.ForeColor = System.Drawing.Color.Yellow;
            this.el500.Location = new System.Drawing.Point(125, 6);
            this.el500.Margin = new System.Windows.Forms.Padding(0);
            this.el500.Name = "el500";
            this.el500.Size = new System.Drawing.Size(26, 32);
            this.el500.TabIndex = 132;
            this.el500.Text = "0";
            this.el500.UseVisualStyleBackColor = false;
            this.el500.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label21
            // 
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Dock = System.Windows.Forms.DockStyle.Left;
            this.label21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label21.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.LavenderBlush;
            this.label21.Location = new System.Drawing.Point(0, 0);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(129, 39);
            this.label21.TabIndex = 37;
            this.label21.Tag = "6";
            this.label21.Text = "NO HOLD";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el501
            // 
            this.el501.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el501.BackColor = System.Drawing.Color.Transparent;
            this.el501.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el501.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el501.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el501.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el501.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el501.ForeColor = System.Drawing.Color.Yellow;
            this.el501.Location = new System.Drawing.Point(158, 6);
            this.el501.Margin = new System.Windows.Forms.Padding(0);
            this.el501.Name = "el501";
            this.el501.Size = new System.Drawing.Size(26, 32);
            this.el501.TabIndex = 61;
            this.el501.Text = "B";
            this.el501.UseVisualStyleBackColor = false;
            this.el501.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el502
            // 
            this.el502.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el502.BackColor = System.Drawing.Color.Transparent;
            this.el502.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el502.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el502.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el502.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el502.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el502.ForeColor = System.Drawing.Color.Yellow;
            this.el502.Location = new System.Drawing.Point(190, 6);
            this.el502.Margin = new System.Windows.Forms.Padding(0);
            this.el502.Name = "el502";
            this.el502.Size = new System.Drawing.Size(26, 32);
            this.el502.TabIndex = 62;
            this.el502.Text = "1";
            this.el502.UseVisualStyleBackColor = false;
            this.el502.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el503
            // 
            this.el503.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el503.BackColor = System.Drawing.Color.Transparent;
            this.el503.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el503.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el503.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el503.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el503.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el503.ForeColor = System.Drawing.Color.Yellow;
            this.el503.Location = new System.Drawing.Point(222, 6);
            this.el503.Margin = new System.Windows.Forms.Padding(0);
            this.el503.Name = "el503";
            this.el503.Size = new System.Drawing.Size(26, 32);
            this.el503.TabIndex = 63;
            this.el503.Text = "2";
            this.el503.UseVisualStyleBackColor = false;
            this.el503.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el504
            // 
            this.el504.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el504.BackColor = System.Drawing.Color.Transparent;
            this.el504.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el504.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el504.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el504.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el504.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el504.ForeColor = System.Drawing.Color.Yellow;
            this.el504.Location = new System.Drawing.Point(254, 6);
            this.el504.Margin = new System.Windows.Forms.Padding(0);
            this.el504.Name = "el504";
            this.el504.Size = new System.Drawing.Size(26, 32);
            this.el504.TabIndex = 103;
            this.el504.Text = "3";
            this.el504.UseVisualStyleBackColor = false;
            this.el504.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el505
            // 
            this.el505.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el505.BackColor = System.Drawing.Color.Transparent;
            this.el505.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el505.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el505.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el505.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el505.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el505.ForeColor = System.Drawing.Color.Yellow;
            this.el505.Location = new System.Drawing.Point(286, 6);
            this.el505.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el505.Name = "el505";
            this.el505.Size = new System.Drawing.Size(26, 32);
            this.el505.TabIndex = 177;
            this.el505.Text = "4";
            this.el505.UseVisualStyleBackColor = false;
            this.el505.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // panInters
            // 
            this.panInters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panInters.Controls.Add(this.el506);
            this.panInters.Controls.Add(this.el507);
            this.panInters.Controls.Add(this.el508);
            this.panInters.Controls.Add(this.el509);
            this.panInters.Controls.Add(this.el510);
            this.panInters.Controls.Add(this.label37);
            this.panInters.Location = new System.Drawing.Point(21, 105);
            this.panInters.Name = "panInters";
            this.panInters.Size = new System.Drawing.Size(313, 39);
            this.panInters.TabIndex = 190;
            this.panInters.Paint += new System.Windows.Forms.PaintEventHandler(this.panInters_Paint);
            // 
            // el506
            // 
            this.el506.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el506.BackColor = System.Drawing.Color.Transparent;
            this.el506.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el506.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el506.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el506.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el506.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el506.ForeColor = System.Drawing.Color.Yellow;
            this.el506.Location = new System.Drawing.Point(125, 4);
            this.el506.Margin = new System.Windows.Forms.Padding(0);
            this.el506.Name = "el506";
            this.el506.Size = new System.Drawing.Size(26, 32);
            this.el506.TabIndex = 132;
            this.el506.Text = "0";
            this.el506.UseVisualStyleBackColor = false;
            this.el506.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el507
            // 
            this.el507.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el507.BackColor = System.Drawing.Color.Transparent;
            this.el507.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el507.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el507.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el507.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el507.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el507.ForeColor = System.Drawing.Color.Yellow;
            this.el507.Location = new System.Drawing.Point(158, 4);
            this.el507.Margin = new System.Windows.Forms.Padding(0);
            this.el507.Name = "el507";
            this.el507.Size = new System.Drawing.Size(26, 32);
            this.el507.TabIndex = 61;
            this.el507.Text = "B";
            this.el507.UseVisualStyleBackColor = false;
            this.el507.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el508
            // 
            this.el508.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el508.BackColor = System.Drawing.Color.Transparent;
            this.el508.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el508.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el508.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el508.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el508.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el508.ForeColor = System.Drawing.Color.Yellow;
            this.el508.Location = new System.Drawing.Point(190, 4);
            this.el508.Margin = new System.Windows.Forms.Padding(0);
            this.el508.Name = "el508";
            this.el508.Size = new System.Drawing.Size(26, 32);
            this.el508.TabIndex = 62;
            this.el508.Text = "1";
            this.el508.UseVisualStyleBackColor = false;
            this.el508.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el509
            // 
            this.el509.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el509.BackColor = System.Drawing.Color.Transparent;
            this.el509.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el509.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el509.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el509.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el509.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el509.ForeColor = System.Drawing.Color.Yellow;
            this.el509.Location = new System.Drawing.Point(222, 4);
            this.el509.Margin = new System.Windows.Forms.Padding(0);
            this.el509.Name = "el509";
            this.el509.Size = new System.Drawing.Size(26, 32);
            this.el509.TabIndex = 63;
            this.el509.Text = "2";
            this.el509.UseVisualStyleBackColor = false;
            this.el509.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el510
            // 
            this.el510.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el510.BackColor = System.Drawing.Color.Transparent;
            this.el510.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el510.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el510.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el510.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el510.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el510.ForeColor = System.Drawing.Color.Yellow;
            this.el510.Location = new System.Drawing.Point(254, 4);
            this.el510.Margin = new System.Windows.Forms.Padding(0);
            this.el510.Name = "el510";
            this.el510.Size = new System.Drawing.Size(26, 32);
            this.el510.TabIndex = 103;
            this.el510.Text = "3";
            this.el510.UseVisualStyleBackColor = false;
            this.el510.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label37
            // 
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Dock = System.Windows.Forms.DockStyle.Left;
            this.label37.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label37.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label37.Location = new System.Drawing.Point(0, 0);
            this.label37.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(235, 39);
            this.label37.TabIndex = 66;
            this.label37.Text = "INTERSECTION";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panComb
            // 
            this.panComb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panComb.Controls.Add(this.el511);
            this.panComb.Controls.Add(this.el512);
            this.panComb.Controls.Add(this.label36);
            this.panComb.Location = new System.Drawing.Point(21, 187);
            this.panComb.Name = "panComb";
            this.panComb.Size = new System.Drawing.Size(313, 39);
            this.panComb.TabIndex = 191;
            this.panComb.Paint += new System.Windows.Forms.PaintEventHandler(this.panComb_Paint);
            // 
            // el511
            // 
            this.el511.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el511.BackColor = System.Drawing.Color.Transparent;
            this.el511.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el511.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el511.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el511.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el511.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el511.ForeColor = System.Drawing.Color.Yellow;
            this.el511.Location = new System.Drawing.Point(125, 2);
            this.el511.Margin = new System.Windows.Forms.Padding(0);
            this.el511.Name = "el511";
            this.el511.Size = new System.Drawing.Size(26, 32);
            this.el511.TabIndex = 132;
            this.el511.Text = "0";
            this.el511.UseVisualStyleBackColor = false;
            this.el511.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el512
            // 
            this.el512.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el512.BackColor = System.Drawing.Color.Transparent;
            this.el512.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el512.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el512.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el512.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el512.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el512.ForeColor = System.Drawing.Color.Yellow;
            this.el512.Location = new System.Drawing.Point(158, 2);
            this.el512.Margin = new System.Windows.Forms.Padding(0);
            this.el512.Name = "el512";
            this.el512.Size = new System.Drawing.Size(26, 32);
            this.el512.TabIndex = 61;
            this.el512.Text = "B";
            this.el512.UseVisualStyleBackColor = false;
            this.el512.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label36
            // 
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Dock = System.Windows.Forms.DockStyle.Left;
            this.label36.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label36.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Fuchsia;
            this.label36.Location = new System.Drawing.Point(0, 0);
            this.label36.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(174, 39);
            this.label36.TabIndex = 133;
            this.label36.Text = "COMBINED";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label40
            // 
            this.label40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label40.AutoEllipsis = true;
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Magenta;
            this.label40.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(230, 21);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(135, 31);
            this.label40.TabIndex = 129;
            this.label40.Text = "Precision";
            // 
            // gbLinear
            // 
            this.gbLinear.AutoSize = true;
            this.gbLinear.BackColor = System.Drawing.Color.Transparent;
            this.gbLinear.Controls.Add(this.panBLinear);
            this.gbLinear.Controls.Add(this.label18);
            this.gbLinear.Controls.Add(this.panLline);
            this.gbLinear.Cursor = System.Windows.Forms.Cursors.Default;
            this.gbLinear.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbLinear.Enabled = false;
            this.gbLinear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbLinear.Font = new System.Drawing.Font("Arial Narrow", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbLinear.ForeColor = System.Drawing.Color.Yellow;
            this.gbLinear.Location = new System.Drawing.Point(0, 0);
            this.gbLinear.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gbLinear.Name = "gbLinear";
            this.gbLinear.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gbLinear.Size = new System.Drawing.Size(351, 188);
            this.gbLinear.TabIndex = 8;
            this.gbLinear.TabStop = false;
            this.gbLinear.Tag = "2";
            this.gbLinear.Enter += new System.EventHandler(this.gbLinear_Enter);
            // 
            // panBLinear
            // 
            this.panBLinear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panBLinear.Controls.Add(this.el528);
            this.panBLinear.Controls.Add(this.el527);
            this.panBLinear.Controls.Add(this.el529);
            this.panBLinear.Controls.Add(this.el532);
            this.panBLinear.Controls.Add(this.el530);
            this.panBLinear.Controls.Add(this.el531);
            this.panBLinear.Controls.Add(this.label20);
            this.panBLinear.Location = new System.Drawing.Point(21, 116);
            this.panBLinear.Name = "panBLinear";
            this.panBLinear.Size = new System.Drawing.Size(313, 39);
            this.panBLinear.TabIndex = 185;
            // 
            // el528
            // 
            this.el528.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el528.BackColor = System.Drawing.Color.Transparent;
            this.el528.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el528.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el528.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el528.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el528.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el528.ForeColor = System.Drawing.Color.Yellow;
            this.el528.Location = new System.Drawing.Point(158, 2);
            this.el528.Margin = new System.Windows.Forms.Padding(0);
            this.el528.Name = "el528";
            this.el528.Size = new System.Drawing.Size(26, 32);
            this.el528.TabIndex = 178;
            this.el528.Text = "B";
            this.el528.UseVisualStyleBackColor = false;
            this.el528.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el527
            // 
            this.el527.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el527.BackColor = System.Drawing.Color.Transparent;
            this.el527.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el527.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el527.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el527.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el527.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el527.ForeColor = System.Drawing.Color.Yellow;
            this.el527.Location = new System.Drawing.Point(125, 2);
            this.el527.Margin = new System.Windows.Forms.Padding(0);
            this.el527.Name = "el527";
            this.el527.Size = new System.Drawing.Size(26, 32);
            this.el527.TabIndex = 182;
            this.el527.Text = "0";
            this.el527.UseVisualStyleBackColor = false;
            this.el527.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el529
            // 
            this.el529.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el529.BackColor = System.Drawing.Color.Transparent;
            this.el529.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el529.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el529.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el529.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el529.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el529.ForeColor = System.Drawing.Color.Yellow;
            this.el529.Location = new System.Drawing.Point(190, 2);
            this.el529.Margin = new System.Windows.Forms.Padding(0);
            this.el529.Name = "el529";
            this.el529.Size = new System.Drawing.Size(26, 32);
            this.el529.TabIndex = 179;
            this.el529.Text = "1";
            this.el529.UseVisualStyleBackColor = false;
            this.el529.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el532
            // 
            this.el532.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el532.BackColor = System.Drawing.Color.Transparent;
            this.el532.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el532.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el532.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el532.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el532.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el532.ForeColor = System.Drawing.Color.Yellow;
            this.el532.Location = new System.Drawing.Point(287, 2);
            this.el532.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el532.Name = "el532";
            this.el532.Size = new System.Drawing.Size(26, 32);
            this.el532.TabIndex = 183;
            this.el532.Text = "4";
            this.el532.UseVisualStyleBackColor = false;
            this.el532.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el530
            // 
            this.el530.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el530.BackColor = System.Drawing.Color.Transparent;
            this.el530.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el530.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el530.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el530.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el530.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el530.ForeColor = System.Drawing.Color.Yellow;
            this.el530.Location = new System.Drawing.Point(222, 2);
            this.el530.Margin = new System.Windows.Forms.Padding(0);
            this.el530.Name = "el530";
            this.el530.Size = new System.Drawing.Size(26, 32);
            this.el530.TabIndex = 180;
            this.el530.Text = "2";
            this.el530.UseVisualStyleBackColor = false;
            this.el530.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el531
            // 
            this.el531.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el531.BackColor = System.Drawing.Color.Transparent;
            this.el531.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el531.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el531.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el531.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el531.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el531.ForeColor = System.Drawing.Color.Yellow;
            this.el531.Location = new System.Drawing.Point(254, 2);
            this.el531.Margin = new System.Windows.Forms.Padding(0);
            this.el531.Name = "el531";
            this.el531.Size = new System.Drawing.Size(26, 32);
            this.el531.TabIndex = 181;
            this.el531.Text = "3";
            this.el531.UseVisualStyleBackColor = false;
            this.el531.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Dock = System.Windows.Forms.DockStyle.Left;
            this.label20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label20.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label20.Location = new System.Drawing.Point(0, 0);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(134, 39);
            this.label20.TabIndex = 66;
            this.label20.Text = "BLOCK";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoEllipsis = true;
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Lime;
            this.label18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(255, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(104, 36);
            this.label18.TabIndex = 128;
            this.label18.Text = "Linear";
            // 
            // panLline
            // 
            this.panLline.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panLline.Controls.Add(this.el533);
            this.panLline.Controls.Add(this.label38);
            this.panLline.Controls.Add(this.el534);
            this.panLline.Controls.Add(this.el535);
            this.panLline.Controls.Add(this.el536);
            this.panLline.Controls.Add(this.el537);
            this.panLline.Controls.Add(this.el538);
            this.panLline.Location = new System.Drawing.Point(21, 74);
            this.panLline.Name = "panLline";
            this.panLline.Size = new System.Drawing.Size(313, 39);
            this.panLline.TabIndex = 189;
            // 
            // el533
            // 
            this.el533.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el533.BackColor = System.Drawing.Color.Transparent;
            this.el533.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el533.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el533.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el533.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el533.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el533.ForeColor = System.Drawing.Color.Yellow;
            this.el533.Location = new System.Drawing.Point(125, 5);
            this.el533.Margin = new System.Windows.Forms.Padding(0);
            this.el533.Name = "el533";
            this.el533.Size = new System.Drawing.Size(26, 32);
            this.el533.TabIndex = 132;
            this.el533.Text = "0";
            this.el533.UseVisualStyleBackColor = false;
            this.el533.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // label38
            // 
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Dock = System.Windows.Forms.DockStyle.Left;
            this.label38.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label38.Font = new System.Drawing.Font("Arial Narrow", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label38.Location = new System.Drawing.Point(0, 0);
            this.label38.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(103, 39);
            this.label38.TabIndex = 37;
            this.label38.Tag = "6";
            this.label38.Text = "L I N E";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // el534
            // 
            this.el534.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el534.BackColor = System.Drawing.Color.Transparent;
            this.el534.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el534.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el534.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el534.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el534.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el534.ForeColor = System.Drawing.Color.Yellow;
            this.el534.Location = new System.Drawing.Point(158, 5);
            this.el534.Margin = new System.Windows.Forms.Padding(0);
            this.el534.Name = "el534";
            this.el534.Size = new System.Drawing.Size(26, 32);
            this.el534.TabIndex = 61;
            this.el534.Text = "B";
            this.el534.UseVisualStyleBackColor = false;
            this.el534.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el535
            // 
            this.el535.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el535.BackColor = System.Drawing.Color.Transparent;
            this.el535.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el535.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el535.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el535.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el535.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el535.ForeColor = System.Drawing.Color.Yellow;
            this.el535.Location = new System.Drawing.Point(190, 5);
            this.el535.Margin = new System.Windows.Forms.Padding(0);
            this.el535.Name = "el535";
            this.el535.Size = new System.Drawing.Size(26, 32);
            this.el535.TabIndex = 62;
            this.el535.Text = "1";
            this.el535.UseVisualStyleBackColor = false;
            this.el535.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el536
            // 
            this.el536.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el536.BackColor = System.Drawing.Color.Transparent;
            this.el536.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el536.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el536.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el536.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el536.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el536.ForeColor = System.Drawing.Color.Yellow;
            this.el536.Location = new System.Drawing.Point(222, 5);
            this.el536.Margin = new System.Windows.Forms.Padding(0);
            this.el536.Name = "el536";
            this.el536.Size = new System.Drawing.Size(26, 32);
            this.el536.TabIndex = 63;
            this.el536.Text = "2";
            this.el536.UseVisualStyleBackColor = false;
            this.el536.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el537
            // 
            this.el537.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el537.BackColor = System.Drawing.Color.Transparent;
            this.el537.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el537.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el537.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el537.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el537.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el537.ForeColor = System.Drawing.Color.Yellow;
            this.el537.Location = new System.Drawing.Point(254, 5);
            this.el537.Margin = new System.Windows.Forms.Padding(0);
            this.el537.Name = "el537";
            this.el537.Size = new System.Drawing.Size(26, 32);
            this.el537.TabIndex = 103;
            this.el537.Text = "3";
            this.el537.UseVisualStyleBackColor = false;
            this.el537.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // el538
            // 
            this.el538.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.el538.BackColor = System.Drawing.Color.Transparent;
            this.el538.Cursor = System.Windows.Forms.Cursors.Hand;
            this.el538.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.el538.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.el538.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.el538.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.el538.ForeColor = System.Drawing.Color.Yellow;
            this.el538.Location = new System.Drawing.Point(287, 5);
            this.el538.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.el538.Name = "el538";
            this.el538.Size = new System.Drawing.Size(26, 32);
            this.el538.TabIndex = 177;
            this.el538.Text = "4";
            this.el538.UseVisualStyleBackColor = false;
            this.el538.Click += new System.EventHandler(this.InsertPassi_Click);
            // 
            // log
            // 
            this.log.BackColor = System.Drawing.Color.DarkSlateGray;
            this.log.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.log.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.log.Font = new System.Drawing.Font("Courier New", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.log.ForeColor = System.Drawing.Color.LightGray;
            this.log.Location = new System.Drawing.Point(0, 617);
            this.log.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.log.Multiline = true;
            this.log.Name = "log";
            this.log.ReadOnly = true;
            this.log.Size = new System.Drawing.Size(359, 102);
            this.log.TabIndex = 74;
            // 
            // tt2
            // 
            this.tt2.IsBalloon = true;
            this.tt2.ShowAlways = true;
            this.tt2.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.tt2.ToolTipTitle = "Description:";
            // 
            // DanzaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(1213, 822);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "DanzaForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Dance - ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DanzaForm_FormClosing);
            this.Load += new System.EventHandler(this.SPSingoloForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.gbElements.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.gbDed.ResumeLayout(false);
            this.gbDed.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ded5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ded1)).EndInit();
            this.panDance1.ResumeLayout(false);
            this.gbLifts.ResumeLayout(false);
            this.gbLifts.PerformLayout();
            this.gbPattern.ResumeLayout(false);
            this.gbPattern.PerformLayout();
            this.panPrecision1.ResumeLayout(false);
            this.gbPivoting.ResumeLayout(false);
            this.gbPivoting.PerformLayout();
            this.panBPivo.ResumeLayout(false);
            this.panLPivo.ResumeLayout(false);
            this.gbRotating.ResumeLayout(false);
            this.gbRotating.PerformLayout();
            this.panWRota.ResumeLayout(false);
            this.panCRota.ResumeLayout(false);
            this.gbTravelling.ResumeLayout(false);
            this.gbTravelling.PerformLayout();
            this.panCTrav.ResumeLayout(false);
            this.panWTrav.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panDance2.ResumeLayout(false);
            this.gbSteps.ResumeLayout(false);
            this.gbSteps.PerformLayout();
            this.panStepHold.ResumeLayout(false);
            this.panChoreo.ResumeLayout(false);
            this.panStraight.ResumeLayout(false);
            this.panCircular.ResumeLayout(false);
            this.panStepNoHold.ResumeLayout(false);
            this.gbSeq.ResumeLayout(false);
            this.gbSeq.PerformLayout();
            this.panNoHold.ResumeLayout(false);
            this.panHold.ResumeLayout(false);
            this.panTrav.ResumeLayout(false);
            this.panPrecision2.ResumeLayout(false);
            this.panPrecision2.PerformLayout();
            this.gbPrecision.ResumeLayout(false);
            this.gbPrecision.PerformLayout();
            this.panCInters.ResumeLayout(false);
            this.panPChoreo.ResumeLayout(false);
            this.panBNH.ResumeLayout(false);
            this.panInters.ResumeLayout(false);
            this.panComb.ResumeLayout(false);
            this.gbLinear.ResumeLayout(false);
            this.gbLinear.PerformLayout();
            this.panBLinear.ResumeLayout(false);
            this.panLline.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.GroupBox gbElements;
        private System.Windows.Forms.Button j9;
        private System.Windows.Forms.Button j8;
        private System.Windows.Forms.Button j7;
        private System.Windows.Forms.Button j6;
        private System.Windows.Forms.Button j5;
        private System.Windows.Forms.Button j4;
        private System.Windows.Forms.Button j3;
        private System.Windows.Forms.Button j2;
        private System.Windows.Forms.Button j1;
        private System.Windows.Forms.Button tp;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ToolTip tt2;
        private System.Windows.Forms.Button startstop;
        private System.Windows.Forms.Button confirm;
        private System.Windows.Forms.Button ltimer;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label total;
        private System.Windows.Forms.Label deductions;
        private System.Windows.Forms.Label elements;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ListView lv;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader note;
        private System.Windows.Forms.Label error;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.ColumnHeader magg;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Button skip;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button info;
        private System.Windows.Forms.CheckBox cbVerify;
        private System.Windows.Forms.Button quit;
        private System.Windows.Forms.GroupBox gbPattern;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button el277;
        private System.Windows.Forms.Button el273;
        private System.Windows.Forms.Button el276;
        private System.Windows.Forms.Button el275;
        private System.Windows.Forms.Button el274;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button el272;
        private System.Windows.Forms.Button el271;
        private System.Windows.Forms.Button el270;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labPattern;
        private System.Windows.Forms.GroupBox gbSeq;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button el248;
        private System.Windows.Forms.Button el247;
        private System.Windows.Forms.Button el246;
        private System.Windows.Forms.Button el245;
        private System.Windows.Forms.Button el299;
        private System.Windows.Forms.Button el298;
        private System.Windows.Forms.Button el297;
        private System.Windows.Forms.Button el296;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labHOld;
        private System.Windows.Forms.Label labNoHold;
        private System.Windows.Forms.Button el286;
        private System.Windows.Forms.Button el289;
        private System.Windows.Forms.Button el288;
        private System.Windows.Forms.Button el287;
        private System.Windows.Forms.Button el291;
        private System.Windows.Forms.Button el294;
        private System.Windows.Forms.Button el293;
        private System.Windows.Forms.Button el292;
        private System.Windows.Forms.Button el295;
        private System.Windows.Forms.Button el290;
        private System.Windows.Forms.GroupBox gbSteps;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button el253;
        private System.Windows.Forms.Button el249;
        private System.Windows.Forms.Button el250;
        private System.Windows.Forms.Button el251;
        private System.Windows.Forms.Button el252;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox gbLifts;
        private System.Windows.Forms.Button el269;
        private System.Windows.Forms.Button el264;
        private System.Windows.Forms.Button el259;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Button el268;
        private System.Windows.Forms.Button el263;
        private System.Windows.Forms.Button el258;
        private System.Windows.Forms.Button el267;
        private System.Windows.Forms.Button el266;
        private System.Windows.Forms.Button el265;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button el262;
        private System.Windows.Forms.Button el261;
        private System.Windows.Forms.Button el260;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button el257;
        private System.Windows.Forms.Button el256;
        private System.Windows.Forms.Button el254;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox log;
        private System.Windows.Forms.Panel panStepNoHold;
        private System.Windows.Forms.Panel panTrav;
        private System.Windows.Forms.Label lab1;
        private System.Windows.Forms.GroupBox gbDed;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.NumericUpDown ded5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown ded3;
        private System.Windows.Forms.NumericUpDown ded4;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.NumericUpDown ded2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown ded1;
        private System.Windows.Forms.Button el305;
        private System.Windows.Forms.Button el301;
        private System.Windows.Forms.Button el302;
        private System.Windows.Forms.Button el303;
        private System.Windows.Forms.Button el304;
        private System.Windows.Forms.Button el306;
        private System.Windows.Forms.Button el641;
        private System.Windows.Forms.Button el310;
        private System.Windows.Forms.Button el307;
        private System.Windows.Forms.Button el312;
        private System.Windows.Forms.Button el308;
        private System.Windows.Forms.Button el309;
        private System.Windows.Forms.Label labelPartial;
        private System.Windows.Forms.Button el660;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button el634;
        private System.Windows.Forms.Button el328;
        private System.Windows.Forms.Button el327;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button el329;
        private System.Windows.Forms.Button el330;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button el640;
        private System.Windows.Forms.Button el639;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button el611;
        private System.Windows.Forms.Button el647;
        private System.Windows.Forms.Button el653;
        private System.Windows.Forms.Button el659;
        private System.Windows.Forms.Panel panHold;
        private System.Windows.Forms.Panel panCircular;
        private System.Windows.Forms.Panel panNoHold;
        private System.Windows.Forms.Panel panChoreo;
        private System.Windows.Forms.Panel panStraight;
        private System.Windows.Forms.Panel panStepHold;
        private System.Windows.Forms.Button reset;
        private System.Windows.Forms.Panel panDance1;
        private System.Windows.Forms.Panel panPrecision1;
        private System.Windows.Forms.GroupBox gbRotating;
        private System.Windows.Forms.Button el539;
        private System.Windows.Forms.Button el540;
        private System.Windows.Forms.Button el515;
        private System.Windows.Forms.Button el516;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button el544;
        private System.Windows.Forms.Button el520;
        private System.Windows.Forms.Button el543;
        private System.Windows.Forms.Button el542;
        private System.Windows.Forms.Button el541;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button el519;
        private System.Windows.Forms.Button el518;
        private System.Windows.Forms.Button el517;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.GroupBox gbTravelling;
        private System.Windows.Forms.Button el545;
        private System.Windows.Forms.Button el546;
        private System.Windows.Forms.Button el521;
        private System.Windows.Forms.Button el522;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button el550;
        private System.Windows.Forms.Button el526;
        private System.Windows.Forms.Button el549;
        private System.Windows.Forms.Button el548;
        private System.Windows.Forms.Button el547;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button el525;
        private System.Windows.Forms.Button el524;
        private System.Windows.Forms.Button el523;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panPrecision2;
        private System.Windows.Forms.GroupBox gbPrecision;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button el533;
        private System.Windows.Forms.Button el538;
        private System.Windows.Forms.Button el537;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button el536;
        private System.Windows.Forms.Button el535;
        private System.Windows.Forms.Button el534;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.GroupBox gbLinear;
        private System.Windows.Forms.Button el527;
        private System.Windows.Forms.Button el532;
        private System.Windows.Forms.Button el531;
        private System.Windows.Forms.Button el530;
        private System.Windows.Forms.Button el529;
        private System.Windows.Forms.Button el528;
        private System.Windows.Forms.Button el500;
        private System.Windows.Forms.Button el505;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button el504;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button el503;
        private System.Windows.Forms.Button el502;
        private System.Windows.Forms.Button el501;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panDance2;
        private System.Windows.Forms.Panel panWTrav;
        private System.Windows.Forms.Panel panWRota;
        private System.Windows.Forms.Panel panCTrav;
        private System.Windows.Forms.Button average;
        private System.Windows.Forms.Panel panCRota;
        private System.Windows.Forms.Panel panLline;
        private System.Windows.Forms.Panel panBLinear;
        private System.Windows.Forms.Panel panBNH;
        private System.Windows.Forms.Panel panPChoreo;
        private System.Windows.Forms.Button el513;
        private System.Windows.Forms.Button el514;
        private System.Windows.Forms.Panel panInters;
        private System.Windows.Forms.Button el506;
        private System.Windows.Forms.Button el507;
        private System.Windows.Forms.Button el508;
        private System.Windows.Forms.Button el509;
        private System.Windows.Forms.Button el510;
        private System.Windows.Forms.Panel panComb;
        private System.Windows.Forms.Button el511;
        private System.Windows.Forms.Button el512;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Button review;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button asterisco;
        private System.Windows.Forms.GroupBox gbPivoting;
        private System.Windows.Forms.Panel panBPivo;
        private System.Windows.Forms.Button el554;
        private System.Windows.Forms.Button el551;
        private System.Windows.Forms.Button el553;
        private System.Windows.Forms.Button el552;
        private System.Windows.Forms.Button el555;
        private System.Windows.Forms.Button el556;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Panel panLPivo;
        private System.Windows.Forms.Button el560;
        private System.Windows.Forms.Button el557;
        private System.Windows.Forms.Button el559;
        private System.Windows.Forms.Button el558;
        private System.Windows.Forms.Button el561;
        private System.Windows.Forms.Button el562;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Panel panCInters;
        private System.Windows.Forms.Button el563;
        private System.Windows.Forms.Button el564;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Button prev;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Ghost;

namespace RollartSystemTech
{
    public partial class Info : Form
    {
        public Info()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void Info_Load(object sender, EventArgs e)
        {
            try
            {
                int numItem = 0;
                TimeSpan time;

                lv.Items.Clear();
                label1.Text = Definizioni.dettaglioGara.Split('@')[0] + "\r\n";
                label1.Text += Definizioni.dettaglioGara.Split('@')[1] + "\r\n";
                label1.Text += Definizioni.dettaglioGara.Split('@')[2] + "\r\n";
                label1.Text += Definizioni.dettaglioGara.Split('@')[3] + "\r\n";
                label1.Text += Definizioni.dettaglioGara.Split('@')[4] + "\r\n";
                label1.Text += Definizioni.dettaglioGara.Split('@')[5] + "\r\n";
                label1.Text += Definizioni.dettaglioGara.Split('@')[6] + "\r\n";
                //label1.Text += Definizioni.dettaglioGara.Split('@')[7] + "\r\n";

                ListViewItem lvi = null;
                
                switch (Definizioni.idSpecialita)
                {
                        // SINGOLI
                    case 1:
                    case 2:
                        time = TimeSpan.FromSeconds(double.Parse(Definizioni.paramSegment[4].ToString()));
                        label1.Text += time.ToString() + "\r\n";

                        label1.Text += Definizioni.factor + "\r\n" + Definizioni.dettaglioGara.Split('@')[7];
                        lv.Groups.Add(new ListViewGroup("Jumps",HorizontalAlignment.Left));
                        lv.Groups.Add(new ListViewGroup("Spins", HorizontalAlignment.Left));
                        lv.Groups.Add(new ListViewGroup("Steps", HorizontalAlignment.Left));

                        lvi = new ListViewItem(new[] { "Total Jumps", "" + Definizioni.paramSegment[1]});
                        lv.Items.Add(lvi);
                        lv.Items[0].Group = lv.Groups[0];
                        lvi = new ListViewItem(new[] { "Solo Jumps", "" + Definizioni.paramSegment[5]});
                        lv.Items.Add(lvi);
                        lv.Items[1].Group = lv.Groups[0];
                        lvi = new ListViewItem(new[] { "Combo Jumps", "" + Definizioni.paramSegment[6] });
                        lv.Items.Add(lvi);
                        lv.Items[2].Group = lv.Groups[0];
                        lvi = new ListViewItem(new[] { "Max Combo Jumps", "" + Definizioni.paramSegment[7] });
                        lv.Items.Add(lvi);
                        lv.Items[3].Group = lv.Groups[0];
                        lvi = new ListViewItem(new[] { "Min Combo Jumps", "" + Definizioni.paramSegment[8] });
                        lv.Items.Add(lvi);
                        lv.Items[4].Group = lv.Groups[0];
                        lvi = new ListViewItem(new[] { "Total Spins", "" + Definizioni.paramSegment[2] });
                        lv.Items.Add(lvi);
                        lv.Items[5].Group = lv.Groups[1];
                        lvi = new ListViewItem(new[] { "Solo Spins", "" + Definizioni.paramSegment[9] });
                        lv.Items.Add(lvi);
                        lv.Items[6].Group = lv.Groups[1];
                        lvi = new ListViewItem(new[] { "Combo Spins", "" + Definizioni.paramSegment[10] });
                        lv.Items.Add(lvi);
                        lv.Items[7].Group = lv.Groups[1];
                        lvi = new ListViewItem(new[] { "Step Sequence", "" + Definizioni.paramSegment[3] });
                        lv.Items.Add(lvi);
                        lv.Items[8].Group = lv.Groups[2];
                        break;
                        // COPPIE ARTISTICO
                    case 3:
                        time = TimeSpan.FromSeconds(double.Parse(Definizioni.paramSegment[0].ToString()));
                        label1.Text += time.ToString() + "\r\n";

                        label1.Text += Definizioni.factor + "\r\n" + Definizioni.dettaglioGara.Split('@')[7];
                        lv.Groups.Add(new ListViewGroup("Jumps",HorizontalAlignment.Left));
                        lv.Groups.Add(new ListViewGroup("Spins", HorizontalAlignment.Left));
                        lv.Groups.Add(new ListViewGroup("Death Spirals", HorizontalAlignment.Left));
                        lv.Groups.Add(new ListViewGroup("Lifts", HorizontalAlignment.Left));
                        lv.Groups.Add(new ListViewGroup("Steps", HorizontalAlignment.Left));

                        lvi = new ListViewItem(new[] { "Twist Jumps", "" + Definizioni.paramSegment[5]});
                        lv.Items.Add(lvi);
                        lv.Items[0].Group = lv.Groups[0];
                        lvi = new ListViewItem(new[] { "Throw Jumps", "" + Definizioni.paramSegment[6]});
                        lv.Items.Add(lvi);
                        lv.Items[1].Group = lv.Groups[0];
                        lvi = new ListViewItem(new[] { "Side Jumps", "" + Definizioni.paramSegment[7] });
                        lv.Items.Add(lvi);
                        lv.Items[2].Group = lv.Groups[0];
                        
                        lvi = new ListViewItem(new[] { "Position Lifts", "" + Definizioni.paramSegment[3] });
                        lv.Items.Add(lvi);
                        lv.Items[3].Group = lv.Groups[3];
                        lvi = new ListViewItem(new[] { "Combo Lifts", "" + Definizioni.paramSegment[4] });
                        lv.Items.Add(lvi);
                        lv.Items[4].Group = lv.Groups[3];
                        
                        lvi = new ListViewItem(new[] { "Contact Spins", "" + Definizioni.paramSegment[9] });
                        lv.Items.Add(lvi);
                        lv.Items[5].Group = lv.Groups[1];
                        lvi = new ListViewItem(new[] { "Side Spins", "" + Definizioni.paramSegment[10] });
                        lv.Items.Add(lvi);
                        lv.Items[6].Group = lv.Groups[1];
                        lvi = new ListViewItem(new[] { "Death Spirals", "" + Definizioni.paramSegment[11] });
                        lv.Items.Add(lvi);
                        lv.Items[7].Group = lv.Groups[2];
                        lvi = new ListViewItem(new[] { "Step Sequence", "" + Definizioni.paramSegment[12] });
                        lv.Items.Add(lvi);
                        lv.Items[8].Group = lv.Groups[4];
                        break;
                        // DANZA, SOLO DANCE
                    case 4:
                    case 5:
                    case 6:
                        time = TimeSpan.FromSeconds(double.Parse(Definizioni.paramSegment[0].ToString()));
                        label1.Text += time.ToString() + "\r\n";

                        label1.Text += Definizioni.factor + "\r\n" + Definizioni.dettaglioGara.Split('@')[7];
                        lv.Groups.Add(new ListViewGroup("Pattern - Sequences - Lifts - Spins",HorizontalAlignment.Left));

                        if (!Definizioni.paramSegment[4].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "Pattern", "" + Definizioni.paramSegment[4] });
                            lv.Items.Add(lvi);
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }

                        if (!Definizioni.paramSegment[2].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "No Hold Sequence (Dance)", "" + Definizioni.paramSegment[2] });
                            lv.Items.Add(lvi);
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        if (!Definizioni.paramSegment[3].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "Hold Sequence (Dance)", "" + Definizioni.paramSegment[3] });
                            lv.Items.Add(lvi);
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        if (!Definizioni.paramSegment[7].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "Travelling Sequence", "" + Definizioni.paramSegment[7] });
                            lv.Items.Add(lvi); 
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        if (!Definizioni.paramSegment[6].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "No Hold Cluster Sequence", "" + Definizioni.paramSegment[6] });
                            lv.Items.Add(lvi); 
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        if (!Definizioni.paramSegment[12].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "Hold Cluster Sequence", "" + Definizioni.paramSegment[12] });
                            lv.Items.Add(lvi);
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        if (!Definizioni.paramSegment[5].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "Dance Lift", "" + Definizioni.paramSegment[5] });
                            lv.Items.Add(lvi); 
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        if (!Definizioni.paramSegment[11].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "Choreo", "" + Definizioni.paramSegment[11] });
                            lv.Items.Add(lvi);
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        if (!Definizioni.paramSegment[8].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "Circular/Straight Sequence", "" + Definizioni.paramSegment[8] });
                            lv.Items.Add(lvi);
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        break;
                        // PRECISION
                    case 7:

                        time = TimeSpan.FromSeconds(double.Parse(Definizioni.paramSegment[0].ToString()));
                        label1.Text += time.ToString() + "\r\n";
                        
                    //label1.Text += string.Format("{0:00}:{1:00}",(seconds/60)%60,seconds%60) + "\r\n";
                        label1.Text += Definizioni.factor + "\r\n" + Definizioni.dettaglioGara.Split('@')[7];
                        lv.Groups.Add(new ListViewGroup("Precision elements",HorizontalAlignment.Left));
                        
                        if (!Definizioni.paramSegment[2].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "No Hold Element", "" + Definizioni.paramSegment[2] });
                            lv.Items.Add(lvi);
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        if (!Definizioni.paramSegment[3].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "Linear Block", "" + Definizioni.paramSegment[3] });
                            lv.Items.Add(lvi);
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        if (!Definizioni.paramSegment[4].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "Travelling Wheels", "" + Definizioni.paramSegment[4] });
                            lv.Items.Add(lvi); 
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        if (!Definizioni.paramSegment[5].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "Rotating Wheels", "" + Definizioni.paramSegment[5] });
                            lv.Items.Add(lvi); 
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        if (!Definizioni.paramSegment[6].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "Travelling Circles", "" + Definizioni.paramSegment[6] });
                            lv.Items.Add(lvi); 
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        if (!Definizioni.paramSegment[7].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "Rotating Circles", "" + Definizioni.paramSegment[7] });
                            lv.Items.Add(lvi);
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        if (!Definizioni.paramSegment[8].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "Linear Line", "" + Definizioni.paramSegment[8] });
                            lv.Items.Add(lvi);
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        if (!Definizioni.paramSegment[9].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "Intersections", "" + Definizioni.paramSegment[9] });
                            lv.Items.Add(lvi);
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        if (!Definizioni.paramSegment[10].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "Combined Element", "" + Definizioni.paramSegment[10] });
                            lv.Items.Add(lvi);
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        if (!Definizioni.paramSegment[11].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "Creative Element", "" + Definizioni.paramSegment[11] });
                            lv.Items.Add(lvi);
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        if (!Definizioni.paramSegment[12].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "Pivoting Line", "" + Definizioni.paramSegment[12] });
                            lv.Items.Add(lvi);
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        if (!Definizioni.paramSegment[13].Equals(0))
                        {
                            lvi = new ListViewItem(new[] { "Pivoting Block", "" + Definizioni.paramSegment[13] });
                            lv.Items.Add(lvi);
                            lv.Items[numItem].Group = lv.Groups[0]; numItem++;
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
                      
        }
    }
}

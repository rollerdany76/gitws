﻿namespace Ghost
{
    partial class ScoreToScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rank = new System.Windows.Forms.Label();
            this.total = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.final = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tech = new System.Windows.Forms.Label();
            this.techEl = new System.Windows.Forms.Label();
            this.comp = new System.Windows.Forms.Label();
            this.ded = new System.Windows.Forms.Label();
            this.name2 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // rank
            // 
            this.rank.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rank.BackColor = System.Drawing.Color.Red;
            this.rank.Font = new System.Drawing.Font("Trebuchet MS", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rank.ForeColor = System.Drawing.Color.Yellow;
            this.rank.Location = new System.Drawing.Point(4, 522);
            this.rank.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.rank.Name = "rank";
            this.rank.Size = new System.Drawing.Size(152, 180);
            this.rank.TabIndex = 100;
            this.rank.Text = "99";
            this.rank.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // total
            // 
            this.total.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.total.BackColor = System.Drawing.Color.Red;
            this.total.Font = new System.Drawing.Font("Trebuchet MS", 44F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total.ForeColor = System.Drawing.Color.Yellow;
            this.total.Location = new System.Drawing.Point(1300, 482);
            this.total.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(292, 99);
            this.total.TabIndex = 101;
            this.total.Text = "999,99";
            this.total.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(1544, 4);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(52, 40);
            this.button1.TabIndex = 105;
            this.button1.Text = "-->";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Trebuchet MS", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.label3.Location = new System.Drawing.Point(738, 888);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(349, 56);
            this.label3.TabIndex = 109;
            this.label3.Text = "Components";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Trebuchet MS", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.label1.Location = new System.Drawing.Point(1190, 888);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(372, 56);
            this.label1.TabIndex = 110;
            this.label1.Text = "Deductions";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // name
            // 
            this.name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.name.BackColor = System.Drawing.Color.Gainsboro;
            this.name.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.name.Cursor = System.Windows.Forms.Cursors.Default;
            this.name.Font = new System.Drawing.Font("Trebuchet MS", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.name.Location = new System.Drawing.Point(159, 495);
            this.name.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.name.Multiline = true;
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(1138, 240);
            this.name.TabIndex = 112;
            this.name.Text = "Test name";
            // 
            // final
            // 
            this.final.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.final.BackColor = System.Drawing.Color.Red;
            this.final.Font = new System.Drawing.Font("Trebuchet MS", 44F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.final.ForeColor = System.Drawing.Color.Yellow;
            this.final.Location = new System.Drawing.Point(1300, 651);
            this.final.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.final.Name = "final";
            this.final.Size = new System.Drawing.Size(292, 99);
            this.final.TabIndex = 113;
            this.final.Text = "999,99";
            this.final.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Trebuchet MS", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.label5.Location = new System.Drawing.Point(1299, 444);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(286, 41);
            this.label5.TabIndex = 114;
            this.label5.Text = "SEGMENT SCORE";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Trebuchet MS", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.label6.Location = new System.Drawing.Point(1304, 614);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(286, 41);
            this.label6.TabIndex = 115;
            this.label6.Text = "TOTAL SCORE";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tech
            // 
            this.tech.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tech.BackColor = System.Drawing.Color.Transparent;
            this.tech.Font = new System.Drawing.Font("Trebuchet MS", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tech.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(202)))), ((int)(((byte)(11)))));
            this.tech.Location = new System.Drawing.Point(2, 889);
            this.tech.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tech.Name = "tech";
            this.tech.Size = new System.Drawing.Size(505, 55);
            this.tech.TabIndex = 94;
            this.tech.Text = "Technical Elements";
            this.tech.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // techEl
            // 
            this.techEl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.techEl.BackColor = System.Drawing.Color.Transparent;
            this.techEl.Font = new System.Drawing.Font("Trebuchet MS", 50.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.techEl.ForeColor = System.Drawing.Color.Yellow;
            this.techEl.Location = new System.Drawing.Point(15, 792);
            this.techEl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.techEl.Name = "techEl";
            this.techEl.Size = new System.Drawing.Size(331, 108);
            this.techEl.TabIndex = 102;
            this.techEl.Text = "888,88";
            this.techEl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comp
            // 
            this.comp.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.comp.BackColor = System.Drawing.Color.Transparent;
            this.comp.Font = new System.Drawing.Font("Trebuchet MS", 50.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comp.ForeColor = System.Drawing.Color.Yellow;
            this.comp.Location = new System.Drawing.Point(762, 776);
            this.comp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.comp.Name = "comp";
            this.comp.Size = new System.Drawing.Size(360, 130);
            this.comp.TabIndex = 103;
            this.comp.Text = "888,88";
            this.comp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ded
            // 
            this.ded.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ded.BackColor = System.Drawing.Color.Transparent;
            this.ded.Font = new System.Drawing.Font("Trebuchet MS", 50.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ded.ForeColor = System.Drawing.Color.Yellow;
            this.ded.Location = new System.Drawing.Point(1375, 776);
            this.ded.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ded.Name = "ded";
            this.ded.Size = new System.Drawing.Size(210, 130);
            this.ded.TabIndex = 104;
            this.ded.Text = "-2,5";
            this.ded.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // name2
            // 
            this.name2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.name2.AutoEllipsis = true;
            this.name2.BackColor = System.Drawing.Color.Gainsboro;
            this.name2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.name2.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.name2.Location = new System.Drawing.Point(159, 495);
            this.name2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.name2.Name = "name2";
            this.name2.Size = new System.Drawing.Size(1138, 240);
            this.name2.TabIndex = 98;
            this.name2.Text = "Prova nome - Prova nome 2";
            this.name2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.name2.Visible = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.DimGray;
            this.pictureBox2.BackgroundImage = global::RollartSystemTech.Properties.Resources.WS_Logo_OnDark_Horz;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(45, 35);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(822, 391);
            this.pictureBox2.TabIndex = 117;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(1145, 35);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(391, 391);
            this.pictureBox3.TabIndex = 118;
            this.pictureBox3.TabStop = false;
            // 
            // ScoreToScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(89)))), ((int)(((byte)(79)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1600, 965);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.final);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.name);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ded);
            this.Controls.Add(this.comp);
            this.Controls.Add(this.techEl);
            this.Controls.Add(this.total);
            this.Controls.Add(this.rank);
            this.Controls.Add(this.name2);
            this.Controls.Add(this.tech);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.Blue;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ScoreToScreen";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "ScoreToScreen";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ScoreToScreen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label rank;
        private System.Windows.Forms.Label total;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.Label final;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label tech;
        private System.Windows.Forms.Label techEl;
        private System.Windows.Forms.Label comp;
        private System.Windows.Forms.Label ded;
        private System.Windows.Forms.Label name2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Threading;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Collections;
using System.Xml.Linq;
using System.Xml;
using RollartSystemTech;
using System.Net.NetworkInformation;

namespace Ghost
{
    public partial class DanzaForm : Form
    {
        /**********************************************************************************************
        * [0] = durata            [1] = factor         [2] = NoHoldStepSeq  [3] = DanceHoldStepSequence
        * [4] = PatternDanceSeq   [5] = DanceLift      [6] = NoHoldClustSeq [7] = TravellingSeq 
        * [8] = StraightStepSeq   [9] = CircularStepSeq[10] = NumElements   [11] = ChoreoStep
        * [12] = Par3        
        * ********************************************************************************************
        * [0] = durata            [1] = factor         [2] = NoHoldBlock    [3] = LinearBlock
        * [4] = WheelsTravelling  [5] = WheelsRotating [6] = CirclesTravell [7] = CirclesRotating
        * [8] = LinearLine        [9] = Intersections  [10] = CombinedEl    [11] = ChoreoStep
        * ********************************************************************************************/
        TimeSpan time = new TimeSpan();

        // Colori
        Color rigaHoldSeq = Color.OrangeRed;
        Color rigaPattern = Color.Aquamarine;
        Color rigaLifts = Color.GreenYellow;
        Color rigaTravClust= Color.NavajoWhite;
        Color rigaSteps = Color.Magenta;
        Color rigaSpins = Color.White;
        Color rigaDed = Color.Red;

        Color rigaTrav = Color.Aqua;
        Color rigaRota = Color.LightSteelBlue;
        Color rigaPivo = Color.Orange;
        Color rigaLine = Color.Lime;
        Color rigaPrec = Color.Magenta;
        Color oldForeColor;
        ToolTip tt = null;
 
        Utility ut = null;
		int currentElem = 0, progEl = 0, tolleranza = 5;
        decimal secMetaDisco = 0;
        //double secondsAfterFinish = 0;
		bool addAsterisco = false, exitCheck = false;
        Thread thrCheckGiudici = null;
        bool flagClose = false;

        public DanzaForm()
        {
            InitializeComponent();
            System.Windows.Forms.Form.CheckForIllegalCrossThreadCalls = false;
        }

        private void SPSingoloForm_Load(object sender, EventArgs e)
        {
            try
            {
                Definizioni.currentForm = this;
                ut = new Utility(null);
                tt = new ToolTip();
                this.Text = Definizioni.resources["panel1"].ToString() +
                    Definizioni.idGaraParams + "/" + Definizioni.idSegment + " - " +
                    Definizioni.dettaglioGara.Split('@')[3] + " " + Definizioni.dettaglioGara.Split('@')[4];

                labPattern.Text = Definizioni.currentPattern;

                if (Definizioni.idSegment == 6) // precision
                {
                    panPrecision1.Visible = true;
                    panPrecision1.Dock = DockStyle.Top;
                    panPrecision2.Visible = true;
                    panPrecision2.Dock = DockStyle.Top;
                    ut.LoadValuesFromDBForAll(this.Controls, log, tt2, "SegmentParamsPrecision");
                }
                else // danza e solo dance
                {
                    panDance1.Visible = true;
                    panDance1.Dock = DockStyle.Top;
                    panDance2.Visible = true;
                    panDance2.Dock = DockStyle.Top;

                    /*
                     *  Argentine Tango
                        Argentine Tango (Solo)
                        Association Waltz
                        Blues
                        Canasta Tango
                        Carlos Tango
                        Castel March
                        City Blues
                        Denver Shuffle
                        Easy Paso
                        Fourteen Step
                        Harris Tango
                        Kilian
                        Italian Foxtrot
                        La Vista Cha Cha
                        Midnight Blues
                        Paso Doble
                        Quick Step
                        Rocker Foxtrot
                        Shaken Samba
                        Starlight
                        Tango Delancha
                        Tango Delanco
                        Terenzi
                        Tudor Waltz
                        Viennese Waltz
                        Westminster Waltz
                     * */
                    switch (Definizioni.currentPattern)
                    {
                        case "Argentine Tango":
                            el270.Name = "el769"; el271.Name = "el770"; el272.Name = "el771"; el273.Name = "el772";
                            el274.Name = "el773"; el275.Name = "el774"; el276.Name = "el775"; el277.Name = "el776";
                            break;
                        case "Argentine Tango (Solo)":
                            el270.Name = "el761"; el271.Name = "el762"; el272.Name = "el763"; el273.Name = "el764";
                            el274.Name = "el765"; el275.Name = "el766"; el276.Name = "el767"; el277.Name = "el768";
                            break;
                        case "Association Waltz":
                            el270.Name = "el880"; el271.Name = "el881"; el272.Name = "el882"; el273.Name = "el883";
                            el274.Name = "el884"; el275.Name = "el885"; el276.Name = "el886"; el277.Name = "el887";
                            break;
                        case "Blues":
                            el270.Name = "el709"; el271.Name = "el710"; el272.Name = "el711"; el273.Name = "el712";
                            el274.Name = "el713"; el275.Name = "el714"; el276.Name = "el715"; el277.Name = "el716";
                            break;
                        case "Carlos Tango":
                            el270.Name = "el820"; el271.Name = "el821"; el272.Name = "el822"; el273.Name = "el823";
                            el274.Visible = false; el275.Visible = false; el276.Visible = false; el277.Visible = false;
                            label17.Visible = false;
                            break;
                        case "Canasta Tango":
                            el270.Name = "el840"; el271.Name = "el841"; el272.Name = "el842"; el273.Name = "el843";
                            el274.Visible = false; el275.Visible = false; el276.Visible = false; el277.Visible = false;
                            label17.Visible = false;
                            break;
                        case "Castel March":
                            el270.Name = "el801"; el271.Name = "el802"; el272.Name = "el803"; el273.Name = "el804";
                            el274.Name = "el805"; el275.Name = "el806"; el276.Name = "el807"; el277.Name = "el808";
                            break;
                        case "City Blues":
                            el270.Name = "el810"; el271.Name = "el811"; el272.Name = "el812"; el273.Name = "el813";
                            el274.Visible = false; el275.Visible = false; el276.Visible = false; el277.Visible = false;
                            label17.Visible = false;
                            break;
                        case "Denver Shuffle":
                            el270.Name = "el850"; el271.Name = "el851"; el272.Name = "el852"; el273.Name = "el853";
                            el274.Visible = false; el275.Visible = false; el276.Visible = false; el277.Visible = false;
                            label17.Visible = false;
                            break;
                        case "Easy Paso":
                            el270.Name = "el870"; el271.Name = "el871"; el272.Name = "el872"; el273.Name = "el873";
                            el274.Visible = false; el275.Visible = false; el276.Visible = false; el277.Visible = false;
                            label17.Visible = false;
                            break;
                        case "Fourteen Step":
                            el270.Name = "el701"; el271.Name = "el702"; el272.Name = "el703"; el273.Name = "el704";
                            el274.Visible = false; el275.Visible = false; el276.Visible = false; el277.Visible = false;
                            label17.Visible = false;
                            break;
                        case "Harris Tango":
                            el270.Name = "el400"; el271.Name = "el401"; el272.Name = "el402"; el273.Name = "el403";
                            el274.Name = "el404"; el275.Name = "el405"; el276.Name = "el406"; el277.Name = "el407";
                            break;
                        case "Italian Foxtrot Solo":
                            el270.Name = "el785"; el271.Name = "el786"; el272.Name = "el787"; el273.Name = "el788";
                            el274.Name = "el789"; el275.Name = "el790"; el276.Name = "el791"; el277.Name = "el792";
                            break;
                        case "La Vista Cha Cha":
                            el270.Name = "el830"; el271.Name = "el831"; el272.Name = "el832"; el273.Name = "el833";
                            el274.Visible = false; el275.Visible = false; el276.Visible = false; el277.Visible = false;
                            label17.Visible = false;
                            break;
                        case "Kilian":
                            el270.Name = "el890"; el271.Name = "el891"; el272.Name = "el892"; el273.Name = "el893";
                            el274.Name = "el894"; el275.Name = "el895"; el276.Name = "el896"; el277.Name = "el897";
                            break;
                        case "Midnight Blues":
                            el270.Name = "el416"; el271.Name = "el417"; el272.Name = "el418"; el273.Name = "el419";
                            el274.Name = "el420"; el275.Name = "el421"; el276.Name = "el422"; el277.Name = "el423";
                            break;
                        case "Paso Doble":
                            el270.Name = "el757"; el271.Name = "el758"; el272.Name = "el759"; el273.Name = "el760";
                            el274.Visible = false; el275.Visible = false; el276.Visible = false; el277.Visible = false;
                            label17.Visible = false;
                            break;
                        case "Quick Step":
                            el270.Name = "el270"; el271.Name = "el271"; el272.Name = "el272"; el273.Name = "el273";
                            el274.Name = "el274"; el275.Name = "el275"; el276.Name = "el276"; el277.Name = "el277";
                            break;
                        case "Rocker Foxtrot":
                            el270.Name = "el705"; el271.Name = "el706"; el272.Name = "el707"; el273.Name = "el708";
                            el274.Visible = false; el275.Visible = false; el276.Visible = false; el277.Visible = false;
                            label17.Visible = false;
                            break;
                        case "Terenzi":
                            el270.Name = "el717"; el271.Name = "el718"; el272.Name = "el719"; el273.Name = "el720";
                            el274.Name = "el721"; el275.Name = "el722"; el276.Name = "el723"; el277.Name = "el724";
                            break;
                        case "Starlight Waltz":
                            el270.Name = "el278"; el271.Name = "el279"; el272.Name = "el280"; el273.Name = "el281";
                            el274.Name = "el282"; el275.Name = "el283"; el276.Name = "el284"; el277.Name = "el285";
                            break;
                        case "Tango Delanco":
                            el270.Name = "el408"; el271.Name = "el409"; el272.Name = "el410"; el273.Name = "el411";
                            el274.Name = "el412"; el275.Name = "el413"; el276.Name = "el414"; el277.Name = "el415";
                            break;
                        case "Shaken Samba":
                            el270.Name = "el900"; el271.Name = "el901"; el272.Name = "el902"; el273.Name = "el903";
                            el274.Name = "el904"; el275.Name = "el905"; el276.Name = "el906"; el277.Name = "el907";
                            break;
                        case "Tango Delancha":
                            el270.Name = "el910"; el271.Name = "el911"; el272.Name = "el912"; el273.Name = "el913";
                            el274.Name = "el914"; el275.Name = "el915"; el276.Name = "el916"; el277.Name = "el917";
                            break;
                        case "Tudor Waltz":
                            el270.Name = "el860"; el271.Name = "el861"; el272.Name = "el862"; el273.Name = "el863";
                            el274.Visible = false; el275.Visible = false; el276.Visible = false; el277.Visible = false;
                            label17.Visible = false;
                            break;
                        case "Viennese Waltz":
                            el270.Name = "el753"; el271.Name = "el754"; el272.Name = "el755"; el273.Name = "el756";
                            el274.Visible = false; el275.Visible = false; el276.Visible = false; el277.Visible = false;
                            label17.Visible = false;
                            break;
                        case "Westminster Waltz":
                            el270.Name = "el749"; el271.Name = "el750"; el272.Name = "el751"; el273.Name = "el752";
                            el274.Visible = false; el275.Visible = false; el276.Visible = false; el277.Visible = false;
                            label17.Visible = false;
                            break;
                    }

                    ut.LoadValuesFromDBForAll(this.Controls, log, tt2, "SegmentParamsDance");

                    if (Definizioni.idSegment == 11 || Definizioni.idSegment == 12) // obbligatori
                    {
                        gbLifts.Visible = false;
                        gbSeq.Visible = false;
                        gbSteps.Visible = false;
                    }

                    if (Definizioni.idSegment == 5) // free dance
                    {
                        gbPattern.Visible = false;
                    }
                }

                NewSegment();
                secMetaDisco = Definizioni.paramSegment[0] / 2;
				
				// nuovo thread per la verifica dei giudici
                thrCheckGiudici = new Thread(CheckGiudici);
                thrCheckGiudici.Start();
                this.MinimumSize = new System.Drawing.Size(1225, 853);

                if (Definizioni.idSegment == 5 || Definizioni.idSegment == 6) tolleranza = 10;
            }
            catch (Exception ex) {
                Utility.WriteLog("LoadForm Dance: " + ex.Message, "ERROR");
            }

        }
        
		private void CheckGiudici()
        {
            //bool judgeDisconnected = false;
            while (true)
            {
                if (exitCheck)
                    return;

                //judgeDisconnected = false;
                for (int i = 0; i < Definizioni.numJudges; i++)
                {
                    if (this == null)
                        return;

                    try
                    {
                        if (EventsForm.jclient.list[i].state.Equals("1"))
                        {
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).Enabled = true;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).ForeColor = Color.Lime;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).FlatAppearance.BorderColor = Color.Lime;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).BackColor = Color.Transparent;
                        }
                        else // interfaccia giudice non avviata o chiusa
                        {
                            //judgeDisconnected = true;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).Enabled = true;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).ForeColor = Color.Red;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).FlatAppearance.BorderColor = Color.Red;
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).BackColor = Color.Transparent;
                        }

                        // ping
                        if (!Utility.PingGiudice(i))
                        {
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).Font =
                                 new Font(((Button)this.Controls.Find("j" + (i + 1), true)[0]).Font, FontStyle.Regular | FontStyle.Underline);

                        }
                        else
                        {
                            ((Button)this.Controls.Find("j" + (i + 1), true)[0]).Font =
                                 new Font(((Button)this.Controls.Find("j" + (i + 1), true)[0]).Font, FontStyle.Regular);
                        }

                    }
                    catch (Exception)
                    {
                    }

                }

                //if (judgeDisconnected)
                //{
                //    if (this.Enabled) this.Enabled = false;
                //    timer1.Stop();
                //}
                //else
                //{
                //    if (!this.Enabled) this.Enabled = true;
                //    timer1.Start();
                //}

                if (RollartSystemTech.Properties.Settings.Default.PingTime > 0)
                    Thread.Sleep(RollartSystemTech.Properties.Settings.Default.PingTime);
            }
        }
		
        #region GESTIONE SEGMENTO
        // INIZIO SEGMENTO
        private void startstop_Click(object sender, EventArgs e)
        {
            try
            {
                if (startstop.Text == "START")
                {
                    EnableGroupBox();
                    confirm.Enabled = false;
                    average.Enabled = false;
                    skip.Enabled = false;
                    prev.Enabled = false;
                    reset.Enabled = false;
                    startstop.Text = "STOP";
                    timer1.Start();
                }
                else
                {
                    DisableGroupBox();
                    confirm.Enabled = true;
                    average.Enabled = true;
                    skip.Enabled = true;
                    prev.Enabled = true;
                    reset.Enabled = true;
                    startstop.Text = "START";
                    timer1.Stop();
                } 
                
            }
            catch (Exception ex)
            {
                Utility.WriteLog("StartStop: " + ex.Message, "ERROR");
            }
        }
        
        // NUOVO SEGMENTO
        public void NewSegment()
        {
            try
            {
                progEl = 0;
                lv.Visible = true;
                // fine gara
                if (EndCompetition()) return;
                if (Definizioni.idSpecialita == 4) // Danza
                {
                    panCircular.Enabled = false;
                    panStraight.Enabled = false;
                    panChoreo.Enabled = false;
                }
                else if (Definizioni.idSpecialita == 5 || Definizioni.idSpecialita == 6)// solo dance
                {
                    panStepNoHold.Enabled = false;
                    panStepHold.Enabled = false;
                    gbLifts.Visible = false;
                    panHold.Enabled = false;
                    
                }
                Definizioni.elemSegment = new decimal[30];
                ded1.Value = 0; ded2.Value = 0; ded3.Value = 0; ded4.Value = 0; ded5.Value = 0;
                total.Text = String.Format("{0:0.00}", 0); //"0,00";
                deductions.Text = String.Format("{0:0.00}", 0); //"0,00";
                elements.Text = String.Format("{0:0.00}", 0); //"0,00";
                Definizioni.currentPart = Definizioni.lastPart + 1;
                Definizioni.totale = 0; Definizioni.deductionsDec = 0; Definizioni.elementsDec = 0;
                Definizioni.compAlreadyInserted = false;
                currentElem = 0;
                error.Text = "";
                ltimer.Text = "00:00";
                time = TimeSpan.Zero;
                ltimer.ForeColor = Color.Magenta;
                
                Utility.SendBroadcast("<CONNECT/>");
                Thread.Sleep(500);
                //Utility.CheckGiudici(this.Controls);
                Utility.SendBroadcast(
                    "<START Gara ='" + Definizioni.idGaraParams + "'" +
                          " Segment='" + Definizioni.idSegment + "'" +
                          " Partecipante='" + Definizioni.currentPart + "'" +
                          // **** 1.0.4 Modifica del 27/06/2018 **** //
                          // Invio al giudice anche la categoria e la disciplina
                          " Category='" + Definizioni.idCategoria + "'" +
                          " Discipline='" + Definizioni.idSpecialita + "'" +
                    "/>");
                ut.LoadParticipant(log, buttonNext, labelPartial);
                lv.Items.Clear();
                DisableGroupBox();
                startstop.Text = "START";
                startstop.Enabled = true;
                confirm.Enabled = false;
                average.Enabled = false;

                // 2.0.0.10
                Definizioni.arDeductions = new decimal[6];
                Definizioni.skipped = false;

                if (Definizioni.currentPart == 1) prev.Enabled = false;

                Utility.WriteLog("___________________________________________________________________", "OK");
                Utility.WriteLog(" " + buttonNext.Text.ToUpper(), "OK");
                Utility.WriteLog("___________________________________________________________________", "OK");
            }
            catch (Exception ex)
            {
                Utility.WriteLog("NewSegment: " + ex.Message, "ERROR");
            }
        }

        // FINE GARA
        public bool EndCompetition()
        {
            try
            {
                // fine gara
                if (Definizioni.currentPart == Definizioni.numPart)
                {
                    // invio classifica al sistema video
                    //ut.SendToVideoFinale();
                    Utility.SendBroadcast("<DISCONNECT/>");

                    //aggiorno a Y il campo COMPLETED di GaraParams
                    using (SQLiteCommand command = Definizioni.conn.CreateCommand())
                    {
                        command.CommandText = "UPDATE GaraParams SET Completed = 'Y' " +
                            " WHERE ID_GaraParams = " + Definizioni.idGaraParams
                            + " AND ID_Segment = " + Definizioni.idSegment;
                        command.ExecuteNonQuery();
                    }
                    
                    //Utility.SendBroadcast("<QUIT/>");
                    
                    Definizioni.currentPart = 0;
                    Dispose();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("EndCompetition: " + ex.Message, "ERROR");
                return false;
            }
        }

        // CARICA IMPOSTAZIONI
        public void LoadDescriptions()
        {
            try
            {
                XmlDocument xml = new XmlDocument();
                xml.Load("settings.xml");
                XmlNode root = xml.DocumentElement;

                // steps
                int numSteps = xml.SelectNodes("/Settings/Steps/*").Count;
                XmlNodeList nodiSteps = xml.SelectNodes("/Settings/Steps/*");
                for (int i = 0; i < numSteps; i++)
                {

                    tt2.SetToolTip(((Button)this.Controls.Find("s" + (i + 1), true)[0])
                        , nodiSteps.Item(i).SelectNodes("@Desc").Item(0).Value);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("LoadDescriptions: " + ex.Message, "ERROR");
            }
        }

        #endregion

        #region STARTSTOP, AVANTI, CONFERMA, BACK, AGGIORNA; ESCI
        
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                // reset elementi
                Definizioni.elemSegment = new decimal[30];
                foreach (ListViewItem item in lv.Items)
                {
                    item.SubItems[0].Text = "";
                    item.SubItems[1].Text = "";
                    item.SubItems[2].Text = "";
                    item.SubItems[3].Text = "";

                }
                ded1.Value = 0; ded2.Value = 0; ded3.Value = 0; ded4.Value = 0; ded5.Value = 0;
                elements.Text = String.Format("{0:0.00}", 0); //"0,00";
                deductions.Text = String.Format("{0:0.00}", 0); //"0,00";
                total.Text = String.Format("{0:0.00}", 0); //"0,00";

                Definizioni.totale = 0;
                currentElem = 0;
                lv.Items[currentElem].Selected = true;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("button1: " + ex.Message, "ERROR");
            }
        }

        //CONFIRM ENTRIES
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                // Verifico gli elementi 
                //if (!CheckBeforeConfirm()) return;

                // salvo sul log ciò che mi aspetto dai giudici
                SaveOnLog();

                DisableGroupBox();
                confirm.Enabled = false;
                back.Enabled = false;
                review.Enabled = false;
                startstop.Enabled = false;
                asterisco.Enabled = false;

                // Confermo il segmento
                exitCheck = true;
                Thread.Sleep(500);

                // 2.0.0.10 - modifica 26/01/2019
                Utility.SendBroadcast("<DEDUCTIONS ded1='" + ded1.Value + "' ded2='" + ded2.Value +
                "' ded3='" + ded3.Value + "' ded4='" + ded4.Value + "' ded5='" + ded5.Value + "' />");

                if (ut.ConfirmSegment())
                {
                    NewSegment();
                    error.Visible = false;
                }
                else
                {
                    startstop.Enabled = true;
                    confirm.Enabled = true;
                    Definizioni.connectedJudges = new Boolean[Definizioni.numJudges];
                    Definizioni.stopFromWaiting = false;
                    Definizioni.exitFromWaiting = false;
                    //for (int i = 0; i < Definizioni.connectedJudges.Length; i++)
                    //{
                    //    ((Button)this.Controls.Find("j" + (i + 1), true)[0]).Enabled = true;
                    //    ((Button)this.Controls.Find("j" + (i + 1), true)[0]).ForeColor = Color.Lime;
                    //    ((Button)this.Controls.Find("j" + (i + 1), true)[0]).FlatAppearance.BorderColor = Color.Lime;
                    //    ((Button)this.Controls.Find("j" + (i + 1), true)[0]).BackColor = Color.Transparent;
                    //}
                }
				thrCheckGiudici = new Thread(CheckGiudici);
                thrCheckGiudici.Start();
            }
            catch (Exception ex)
            {
                Utility.WriteLog("ConfirmEntries: " + ex.StackTrace, "ERROR");
                error.Visible = false;
            }
        }
   
        private void clear_Click(object sender, EventArgs e)
        {
            back.Enabled = false;
            ClearElement();
            back.Enabled = true;
        }

        // RESTART
        private void reset_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = MessageBox.Show(
                Definizioni.resources["panel70"].ToString(), "Warning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    ut.RollbackCompetitor(log);
                    NewSegment();
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("RESET: " + ex.Message, "ERROR");
            }
        }

        public void SaveOnLog()
        {
            try
            {
                string elementsXML = "[ELEMENTS ";
                for (int i = 0; i < lv.Items.Count; i++)
                {
                    elementsXML += lv.Items[i].SubItems[1].Text + ";";
                }
                elementsXML += "][DEDUCTIONS: " + deductions.Text + "]";
                Utility.WriteLog(elementsXML, "");
            }
            catch (Exception ex)
            {
                Utility.WriteLog("SaveOnLog: " + ex.Message, "ERROR");
            }
        }

        // ASK COMPONENTS For Average
        private void average_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(
            Definizioni.resources["panel90"].ToString(), "Warning",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                DisableGroupBox();
                exitCheck = true;
                Thread.Sleep(500);
                ut.AskAverage();
                //Definizioni.connectedJudges = new Boolean[Definizioni.numJudges];
            }
            exitCheck = false;
            thrCheckGiudici = new Thread(CheckGiudici);
            thrCheckGiudici.Start();
        }

        // AGGIORNA ENTRY
        private void lv_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                foreach (ListViewItem lvi in lv.Items)
                {
                    lvi.BackColor = Color.Black;
                }
                cancel.Enabled = true;
                if (lv.SelectedItems[0] == null) return;
                Definizioni.updating = true; //modalità updating
                
                ListViewItem selectedItem = lv.SelectedItems[0];
                string numEl = selectedItem.SubItems[0].Text;
                string codeEl = selectedItem.SubItems[1].Text;
                string valEl = selectedItem.SubItems[2].Text;
                string typeEl = selectedItem.SubItems[3].Text;
                string cat = selectedItem.SubItems[4].Text;
                string numcombo = selectedItem.SubItems[5].Text;
                string du = selectedItem.SubItems[6].Text;
                int indexElement = selectedItem.Index;
                selectedItem.BackColor = Color.Yellow;
                oldForeColor = selectedItem.ForeColor;
                selectedItem.ForeColor = Color.Red;
                //oldForeColor = selectedItem.ForeColor;

                DisableGroupBox();
				// *** Modifica del 06/07/2018 - 1.0.4 *** //
                
                deductions.Enabled = false;
                flowLayoutPanel1.Enabled = false;
                review.Enabled = false;
                asterisco.Enabled = false;
                //startstop.Enabled = false;
                //confirm.Enabled = false;
                //deductions.Enabled = false;
                //average.Enabled = false;
                //skip.Enabled = false;
                back.Enabled = false;
                // *** //

                selectedItem.Selected = false;
                currentElem = indexElement;

                if (typeEl.Equals("Pattern")) // pattern
                {
                    gbPattern.Enabled = true;
                }
                else if (typeEl.Contains("Lift")) // lifts
                {
                    gbLifts.Enabled = true;
                }
                else if (typeEl.Equals("StepsStra")) // straight
                {
                    panStraight.Enabled = true;
                }
                else if (typeEl.Equals("StepsCirc")) // circular
                {
                    panCircular.Enabled = true;
                }
                else if (typeEl.Equals("StepsChor")) // choreo
                {
                    panChoreo.Enabled = true;
                }
                else if (typeEl.Equals("StepsNH")) // steps no hold
                {
                    panStepNoHold.Enabled = true;
                }
                else if (typeEl.Equals("StepsH")) // steps hold
                {
                    panStepHold.Enabled = true;
                }
                else if (typeEl.Equals("TravClust")) // travelling
                {
                    panTrav.Enabled = true;
                }
                else if (typeEl.Equals("ClustNoHold")) // cluster no hold
                {
                    panNoHold.Enabled = true;
                }
                else if (typeEl.Equals("ClustHold")) // cluster hold
                {
                    panHold.Enabled = true;
                }

                // Precision
                else if (typeEl.Equals("LinearLine")) // Linear Line
                {
                    gbLinear.Enabled = true;
                    panLline.Enabled = true;
                }
                else if (typeEl.Equals("LinearBlock")) // BlocksLi
                {
                    gbLinear.Enabled = true;
                    panBLinear.Enabled = true;
                }
                else if (typeEl.Equals("Intersection")) // Inters
                {
                    gbPrecision.Enabled = true;
                    panInters.Enabled = true;
                    panCInters.Enabled = true;
                }
                else if (typeEl.Equals("IntersCreative")) // Inters
                {
                    gbPrecision.Enabled = true;
                    panInters.Enabled = true;
                    panCInters.Enabled = true;
                }
                else if (typeEl.Equals("Combined")) // Combined
                {
                    gbPrecision.Enabled = true;
                    panComb.Enabled = true;
                    panCInters.Enabled = false;
                }
                else if (typeEl.Equals("Creative")) // Creative
                {
                    gbPrecision.Enabled = true;
                    panPChoreo.Enabled = true;
                    panCInters.Enabled = false;
                }
                else if (typeEl.Equals("No Hold")) // No Hold
                {
                    gbPrecision.Enabled = true;
                    panBNH.Enabled = true;
                    panCInters.Enabled = false;
                }

                else if (typeEl.Equals("TravWheel")) // Wheels Tr
                {
                    gbTravelling.Enabled = true;
                    panWTrav.Enabled = true;
                }
                else if (typeEl.Equals("RotatingWheel")) // Wheels Rot
                {
                    gbRotating.Enabled = true;
                    panWRota.Enabled = true;
                }
                else if (typeEl.Equals("TravCircle")) // CirclesTr
                {
                    gbTravelling.Enabled = true;
                    panCTrav.Enabled = true;
                }
                else if (typeEl.Equals("RotatingCircle")) // CirclesRo
                {
                    gbRotating.Enabled = true;
                    panCRota.Enabled = true;
                }
                else if (typeEl.Equals("PivotingLine")) // pivoting line
                {
                    gbPivoting.Enabled = true;
                    panLPivo.Enabled = true;
                }
                else if (typeEl.Equals("PivotingBlock")) // pivoting block
                {
                    gbPivoting.Enabled = true;
                    panBPivo.Enabled = true;
                }


            }

            catch (Exception ex)
            {
                Utility.WriteLog("MouseDoubleClick: " + ex.Message, "ERROR");
            }
        }

        // ANNULLA AGGIORNAMENTO
        private void cancel_Click(object sender, EventArgs e)
        {
            ResetAfterInsertion();
        }

        // QUIT
        private void button2_Click_2(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(
                Definizioni.resources["panel8"].ToString(), "Warning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                Utility.SendBroadcast("<DISCONNECT/>");
                // faccio rollback ed elimino i dati dell'ultimo competitor
                ut.RollbackCompetitor(log);
				exitCheck = true;
                flagClose = true;
                Dispose();
            }
        }

        // SKIP
        private void button1_Click_1(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(Definizioni.resources["panel9"].ToString(), "Warning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                ut.UpdateLastPartecipant();

                // 2.0.0.10 - 27/01/2019 modifica per non considerare l'atleta mancante nella classifica finale
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    cmd.CommandText = "UPDATE GaraTotal SET Position = '-'" +
                        " WHERE ID_GARAPARAMS = " + Definizioni.idGaraParams +
                        " AND ID_Atleta = " + Definizioni.idAtleta;
                    cmd.ExecuteReader();
                }

                NewSegment();

                Definizioni.skipped = true;
            }
        }
        #endregion

        #region FUNZIONI DI INTERFACCIA
        public bool CheckProgram(Button step, string parent)
        {
            error.Text = "";
            error.Visible = false;
            if (!cbVerify.Checked) return true;
            if (Definizioni.updating) return true;

            string codeEl = ((DataRow)step.Tag)[3].ToString();
            switch (parent)
            {
                // Patterns
                case "gbPattern":
                    if (int.Parse(Definizioni.elemSegment[4].ToString()) >= 
                        int.Parse(Definizioni.paramSegment[4].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel52"].ToString();
                        return false;
                    }
                    break;
                // Lifts
                case "gbLifts":
                    if (int.Parse(Definizioni.elemSegment[5].ToString()) >=
                        int.Parse(Definizioni.paramSegment[5].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel53"].ToString();
                        return false;
                    }
                    break;
                // Clusters
                case "panTrav":
                    if (int.Parse(Definizioni.elemSegment[7].ToString()) >=
                    int.Parse(Definizioni.paramSegment[7].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel56"].ToString();
                        return false;
                    }
                    break;
                case "panNoHold":
                    if (int.Parse(Definizioni.elemSegment[6].ToString()) >=
                    int.Parse(Definizioni.paramSegment[6].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel57"].ToString();
                        return false;
                    }
                    break;
                case "panHold":
                    if (int.Parse(Definizioni.elemSegment[12].ToString()) >=
                    int.Parse(Definizioni.paramSegment[12].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel57"].ToString();
                        return false;
                    }
                    break;
                // Steps
                case "panCircular":
                    if (int.Parse(Definizioni.elemSegment[9].ToString()) >=
                        int.Parse(Definizioni.paramSegment[9].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel55"].ToString();
                        return false;
                    }
                    break;
                case "panStraight":
                    if (int.Parse(Definizioni.elemSegment[8].ToString()) >=
                        int.Parse(Definizioni.paramSegment[8].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel55"].ToString();
                        return false;
                    }
                    break;
                case "panChoreo":
                    if (int.Parse(Definizioni.elemSegment[11].ToString()) >=
                        int.Parse(Definizioni.paramSegment[11].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel19"].ToString();
                        return false;
                    }
                    break;
                case "panStepNoHold":
                    if (int.Parse(Definizioni.elemSegment[2].ToString()) >=
                        int.Parse(Definizioni.paramSegment[2].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel59"].ToString();
                        return false;
                    }
                    break;
                case "panStepHold":
                    if (int.Parse(Definizioni.elemSegment[3].ToString()) >=
                        int.Parse(Definizioni.paramSegment[3].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel60"].ToString();
                        return false;
                    }
                    break;
                // Precision
                case "panBNH":
                    if (int.Parse(Definizioni.elemSegment[2].ToString()) >=
                        int.Parse(Definizioni.paramSegment[2].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel80"].ToString();
                        return false;
                    }
                    break;
                case "panBLinear":
                    if (int.Parse(Definizioni.elemSegment[3].ToString()) >=
                        int.Parse(Definizioni.paramSegment[3].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel81"].ToString();
                        return false;
                    }
                    break;
                case "panWTrav":
                    if (int.Parse(Definizioni.elemSegment[4].ToString()) >=
                        int.Parse(Definizioni.paramSegment[4].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel82"].ToString();
                        return false;
                    }
                    break;
                case "panWRota":
                    if (int.Parse(Definizioni.elemSegment[5].ToString()) >=
                        int.Parse(Definizioni.paramSegment[5].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel83"].ToString();
                        return false;
                    }
                    break;
                case "panCTrav":
                    if (int.Parse(Definizioni.elemSegment[6].ToString()) >=
                        int.Parse(Definizioni.paramSegment[6].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel84"].ToString();
                        return false;
                    }
                    break;
                case "panCRota":
                    if (int.Parse(Definizioni.elemSegment[7].ToString()) >=
                        int.Parse(Definizioni.paramSegment[7].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel85"].ToString();
                        return false;
                    }
                    break;
                case "panLline":
                    if (int.Parse(Definizioni.elemSegment[8].ToString()) >=
                        int.Parse(Definizioni.paramSegment[8].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel86"].ToString();
                        return false;
                    }
                    break;
                case "panInters":
                    if (int.Parse(Definizioni.elemSegment[9].ToString()) >=
                        int.Parse(Definizioni.paramSegment[9].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel87"].ToString();
                        return false;
                    }
                    break;
                case "panCInters":
                    if (int.Parse(Definizioni.elemSegment[9].ToString()) >=
                        int.Parse(Definizioni.paramSegment[9].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel87"].ToString();
                        return false;
                    }
                    break;
                case "panComb":
                    if (int.Parse(Definizioni.elemSegment[10].ToString()) >=
                        int.Parse(Definizioni.paramSegment[10].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel88"].ToString();
                        return false;
                    }
                    break;
                case "panPChoreo":
                    if (int.Parse(Definizioni.elemSegment[11].ToString()) >=
                        int.Parse(Definizioni.paramSegment[11].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel89"].ToString();
                        return false;
                    }
                    break;
                case "panLPivo":
                    if (int.Parse(Definizioni.elemSegment[12].ToString()) >=
                        int.Parse(Definizioni.paramSegment[12].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel92"].ToString();
                        return false;
                    }
                    break;
                case "panBPivo":
                    if (int.Parse(Definizioni.elemSegment[13].ToString()) >=
                        int.Parse(Definizioni.paramSegment[13].ToString()))
                    {
                        error.Visible = true;
                        error.Text = Definizioni.resources["panel93"].ToString();
                        return false;
                    }
                    break;
            }
            return true;
        }

        public bool CheckBeforeConfirm()
        {
            /********************************************************
            * Controllo gli elementi inseriti
            * [2]  = NoHoldStepSeq     [3]  = DanceHoldStepSequence
            * [4]  = PatternDanceSeq   [5]  = DanceLift      
            * [6]  = NoHoldClustSeq    [7]  = TravellingSeq 
            * [8]  = StraightStepSeq   [9]  = CircularStepSeq 
            * [11] = Choreo Step       [12] = HoldClustSeq      
            * ******************************************************/
            string message = "";
            int numElemViol = 0;
            int numPattern = 0, numLifts = 0;
            int numTrav = 0, numClusterNoHold = 0, numClusterHold = 0;
            int numCirc = 0, numStra = 0, numChoreo = 0, numStepsH = 0, numStepsNH = 0;
            foreach (ListViewItem lvi in lv.Items)
            {
                if (lvi.SubItems[3].Text.Contains("Pattern"))          numPattern++;
                else if (lvi.SubItems[3].Text.Equals("Lift"))     numLifts++;
                else if (lvi.SubItems[3].Text.Equals("TravClust"))   numTrav++;
                else if (lvi.SubItems[3].Text.Equals("ClustNoHold")) numClusterNoHold++;
                else if (lvi.SubItems[3].Text.Equals("ClustHold"))   numClusterHold++;
                else if (lvi.SubItems[3].Text.Equals("StepsCirc"))     numCirc++;
                else if (lvi.SubItems[3].Text.Equals("StepsStra"))     numStra++;
                else if (lvi.SubItems[3].Text.Equals("StepsChor"))     numChoreo++;
                else if (lvi.SubItems[3].Text.Equals("StepsNH"))       numStepsNH++;
                else if (lvi.SubItems[3].Text.Equals("StepsH"))        numStepsH++;
            }

            /***********************************************************************************
            *  FREE DANCE - Dance
             *  1.	Stationary lift.
                2.	Rotational lift.
                3.	Combo lift.
                4.	Choreographic lift.
                5.	No hold step sequence.
                6.	Hold step sequence.
                7.	No hold synchronized cluster sequence.
                8.	Hold cluster sequence.
                9.	Synchronized travelling sequence.

              FREE DANCE - Solo Dance
             *  1.	One straight step sequence (will be decided each year).
                2.	One circular or serpentine step sequence (will be decided each year). 
                3.	One travelling sequence (see couple dance).
                4.	One cluster sequence (see couple dance).
                5.	Choreographic step sequence.
            * **********************************************************************************/

            if (Definizioni.idSegment == 5 && Definizioni.idSpecialita == 4) // danza
            {
                message += "\r\n*******DANCE ELEMENTS********\r\n\r\n";
                //if (numStepsNH < Definizioni.paramSegment[2])
                //{ numElemViol++; message += Definizioni.resources["panel61"].ToString() + "\r\n"; }

                //if (numStepsH < Definizioni.paramSegment[3])
                //{ numElemViol++; message += Definizioni.resources["panel62"].ToString() + "\r\n"; }

                //if (numLifts < Definizioni.paramSegment[4])
                //{ numElemViol++; message += Definizioni.resources["panel63"].ToString().Replace("3", Definizioni.paramSegment[4] + "") + "\r\n"; }

                //if (numTrav < Definizioni.paramSegment[7])
                //{ numElemViol++; message += Definizioni.resources["panel65"].ToString() + "\r\n"; }

                //if (numClusterNoHold < Definizioni.paramSegment[6])
                //{ numElemViol++; message += Definizioni.resources["panel64"].ToString() + "\r\n"; }

                //if (numClusterHold < Definizioni.paramSegment[12])
                //{ numElemViol++; message += Definizioni.resources["panel64"].ToString() + "\r\n"; }
            }
            else if (Definizioni.idSegment == 5 && Definizioni.idSpecialita != 4)// solo dance
            {
                message += "\r\n*******SOLO DANCE ELEMENTS********\r\n\r\n";

                if (numTrav < Definizioni.paramSegment[7])
                { numElemViol++; message += Definizioni.resources["panel65"].ToString() + "\r\n"; }

                if (numClusterNoHold < Definizioni.paramSegment[6])
                { numElemViol++; message += Definizioni.resources["panel64"].ToString() + "\r\n"; }

                if (numCirc < Definizioni.paramSegment[9])
                { numElemViol++; message += Definizioni.resources["panel66"].ToString() + "\r\n"; }

                if (numStra < Definizioni.paramSegment[8])
                { numElemViol++; message += Definizioni.resources["panel76"].ToString() + "\r\n"; }

                if (numChoreo < Definizioni.paramSegment[11])
                { numElemViol++; message += Definizioni.resources["panel77"].ToString() + "\r\n"; }
            }

            /***********************************************************************************
            *  STYLE DANCE - Dance
                1.	One no hold step sequence.
                2.	One hold step sequence.
                3.	One no hold synchronized cluster sequence.
                4.	One hold cluster sequence.
                5.	No hold synchronized travelling sequence.
                6.	One dance lift (the kind of lift will be chosen each year).
                7.	One pattern dance sequence (compulsory dance).

             * STYLE DANCE - Solo Dance
                1.	One straight step sequence (will be decided each year).
                2.	One circular or serpentine step sequence (will be decided each year). 
                3.	One travelling sequence (see couple dance).
                4.	One cluster sequence (see couple dance).
                5.	Choreographic step sequence.
            * **********************************************************************************/
            if (Definizioni.idSegment == 4 && Definizioni.idSpecialita == 4) 
            {
                message += "\r\n*******DANCE ELEMENTS********\r\n\r\n";

                if (numLifts < Definizioni.paramSegment[5])
                { numElemViol++; message += Definizioni.resources["panel67"].ToString() + "\r\n"; }

                if (numPattern < Definizioni.paramSegment[4])
                { numElemViol++; message += Definizioni.resources["panel68"].ToString() + "\r\n"; }
                

            }
            else if (Definizioni.idSegment == 4 && Definizioni.idSpecialita != 4)// solo dance
            {
                message += "\r\n*******SOLO DANCE ELEMENTS********\r\n\r\n";

                if (numPattern < Definizioni.paramSegment[4])
                { numElemViol++; message += Definizioni.resources["panel68"].ToString() + "\r\n"; }
            }

            if (numElemViol > 0)
            {
                if (MessageBox.Show(numElemViol + Definizioni.resources["panel33"].ToString() + "\r\n" +
                    message + "\r\n" + Definizioni.resources["panel34"].ToString(), "Confirm",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    return true;
                else return false;
            }
            return true;
        }

        public void ResetAfterInsertion()
        {
            if (Definizioni.updating)
                lv.Items[currentElem].ForeColor = oldForeColor;
            Definizioni.updating = false;
            lv.Items[currentElem].BackColor = Color.Black;
            
            lv.Items[currentElem].Selected = false;
            currentElem = lv.Items.Count;

            EnableGroupBox();

            if (lv.Items.Count > 0) back.Enabled = true;
            else back.Enabled = false;

            // *** Modifica del 06/07/2018 - 1.0.4 *** //
            flowLayoutPanel1.Enabled = true;
            cancel.Enabled = false;
            startstop.Enabled = true;
            deductions.Enabled = true;

            // *** //
            //cancel.Enabled = false;
            //startstop.Enabled = true; 
            //confirm.Enabled = false; 
            //deductions.Enabled = true;
            //average.Enabled = false;

            WriteLog();

            Thread.Sleep(100);
            //Thread.Sleep(500);
            Cursor.Show();
        }

        public void WriteLog()
        {
        
            if (Definizioni.idSegment == 6) // precision
            {
                /* ********************************************************************************************
                * [0] = durata            [1] = factor         [2] = NoHoldBlock    [3] = LinearBlock
                * [4] = WheelsTravelling  [5] = WheelsRotating [6] = CirclesTravell [7] = CirclesRotating
                * [8] = LinearLine        [9] = Intersections  [10] = CombinedEl    [11] = ChoreoStep
                * ********************************************************************************************/
                log.Text =  "No Hold Block     = " + Definizioni.elemSegment[2] + "\r\n" +
                            "Linear Block      = " + Definizioni.elemSegment[3] + " | " +
                            "Wheel Travelling  = " + Definizioni.elemSegment[4] + "\r\n" +
                            "Wheel Rotating    = " + Definizioni.elemSegment[5] + " | " +
                            "Circle Travelling = " + Definizioni.elemSegment[6] + "\r\n" +
                            "Circle Rotating   = " + Definizioni.elemSegment[7] + " | " +
                            "Line Pivoting     = " + Definizioni.elemSegment[12] + "\r\n" +
                            "Block Pivoting    = " + Definizioni.elemSegment[13] + " | " +
                            "Linear Line       = " + Definizioni.elemSegment[8] + "\r\n" +
                            "Intersections     = " + Definizioni.elemSegment[9] + " | " +
                            "Combined Element  = " + Definizioni.elemSegment[10] + "\r\n" +
                            "Creative Element  = " + Definizioni.elemSegment[11] + "";
            }
            else // Dance e solo dance
            {
                /***********************************************************************************************
                * [0] = durata            [1] = factor         [2] = NoHoldStepSeq  [3] = DanceHoldStepSequence
                * [4] = PatternDanceSeq   [5] = DanceLift      [6] = NoHoldClustSeq [7] = TravellingSeq 
                * [8] = StraightStepSeq   [9] = CircularStepSeq[10] = NumElements   [11] = Choreo
                * [12] = HoldClustSeq        
                * **********************************************************************************************/
                log.Text = "Total Elements    = " + Definizioni.elemSegment[10] + " | " +
                           "No Hold Step Seq  = " + Definizioni.elemSegment[2] + "\r\n" +
                           "Hold Step Seq     = " + Definizioni.elemSegment[3] + " | " +
                           "Pattern Seq       = " + Definizioni.elemSegment[4] + "\r\n" +
                           "Dance Lifts       = " + Definizioni.elemSegment[5] + " | " +
                           "No Hold Cluster   = " + Definizioni.elemSegment[6] + "\r\n" +
                           "Travelling Seq    = " + Definizioni.elemSegment[7] + " | " +
                           "Straight          = " + Definizioni.elemSegment[8] + "\r\n" +
                           "Circular          = " + Definizioni.elemSegment[9] + " | " +
                           "Choreo Step       = " + Definizioni.elemSegment[11] + "\r\n" +
                           "Hold Cluster Seq  = " + Definizioni.elemSegment[12] + "";
            }      
        
            log.SelectionStart = log.Text.Length;
            log.ScrollToCaret();
        }
 
        public void ClearElement()
        {
            try
            {
                Cursor.Hide();
                int count = lv.Items.Count;
                if (count == 0)
                {
                    Cursor.Show();
                    return;
                }
                ListViewItem lastItem = lv.Items[count - 1];
                string numEl = lastItem.SubItems[0].Text;
                string codeEl = lastItem.SubItems[1].Text;
                string valEl = lastItem.SubItems[2].Text;
                string typeEl = lastItem.SubItems[3].Text;
                string cat = lastItem.SubItems[4].Text;
                string numcombo = lastItem.SubItems[5].Text;
                string du = lastItem.SubItems[6].Text;
                string note = lastItem.SubItems[7].Text;

                if (Definizioni.idSegment != 6)
                    Definizioni.elemSegment[10] = int.Parse(Definizioni.elemSegment[10].ToString()) - 1;
                if (typeEl.Equals("Pattern")) 
                    Definizioni.elemSegment[4] = int.Parse(Definizioni.elemSegment[4].ToString()) - 1;
                else if (typeEl.Contains("Lift")) 
                    Definizioni.elemSegment[5] = int.Parse(Definizioni.elemSegment[5].ToString()) - 1;
                else if (typeEl.Equals("StepsStra")) // straight
                    Definizioni.elemSegment[8] = int.Parse(Definizioni.elemSegment[8].ToString()) - 1;
                else if (typeEl.Equals("StepsCirc")) // circular
                    Definizioni.elemSegment[9] = int.Parse(Definizioni.elemSegment[9].ToString()) - 1;
                else if (typeEl.Equals("StepsChor")) // choreo
                    Definizioni.elemSegment[11] = int.Parse(Definizioni.elemSegment[11].ToString()) - 1;
                else if (typeEl.Equals("StepsNH")) // steps no hold
                    Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) - 1;
                else if (typeEl.Equals("StepsH")) // steps hold
                    Definizioni.elemSegment[3] = int.Parse(Definizioni.elemSegment[3].ToString()) - 1;
                else if (typeEl.Equals("TravClust")) // travelling
                    Definizioni.elemSegment[7] = int.Parse(Definizioni.elemSegment[7].ToString()) - 1;
                else if (typeEl.Equals("ClustNoHold")) // cluster no hold
                    Definizioni.elemSegment[6] = int.Parse(Definizioni.elemSegment[6].ToString()) - 1;
                else if (typeEl.Equals("ClustHold")) // cluster hold
                    Definizioni.elemSegment[12] = int.Parse(Definizioni.elemSegment[12].ToString()) - 1;
                    // PRECISION
                    else if (typeEl.Equals("No Hold")) // block no hold
                    Definizioni.elemSegment[2] = int.Parse(Definizioni.elemSegment[2].ToString()) - 1;
                    else if (typeEl.Equals("LinearBlock")) // block linear
                    Definizioni.elemSegment[3] = int.Parse(Definizioni.elemSegment[3].ToString()) - 1;
                    else if (typeEl.Equals("TravWheel")) // wheels travelling
                    Definizioni.elemSegment[4] = int.Parse(Definizioni.elemSegment[4].ToString()) - 1;
                    else if (typeEl.Equals("RotatingWheel")) // wheels rotating
                    Definizioni.elemSegment[5] = int.Parse(Definizioni.elemSegment[5].ToString()) - 1;
                    else if (typeEl.Equals("TravCircle")) // Circles travelling
                    Definizioni.elemSegment[6] = int.Parse(Definizioni.elemSegment[6].ToString()) - 1;
                    else if (typeEl.Equals("RotatingCircle")) // Circles rotating
                    Definizioni.elemSegment[7] = int.Parse(Definizioni.elemSegment[7].ToString()) - 1;
                    else if (typeEl.Equals("PivotingLine")) // Line pivoting
                    Definizioni.elemSegment[12] = int.Parse(Definizioni.elemSegment[12].ToString()) - 1;
                    else if (typeEl.Equals("PivotingBlock")) // Block Pivoting
                    Definizioni.elemSegment[13] = int.Parse(Definizioni.elemSegment[13].ToString()) - 1;
                    else if (typeEl.Equals("LinearLine")) // Linear line
                    Definizioni.elemSegment[8] = int.Parse(Definizioni.elemSegment[8].ToString()) - 1;
                    else if (typeEl.Equals("Intersection")) // intersection
                    Definizioni.elemSegment[9] = int.Parse(Definizioni.elemSegment[9].ToString()) - 1;
                    else if (typeEl.Equals("IntersCreative")) // intersection
                    Definizioni.elemSegment[9] = int.Parse(Definizioni.elemSegment[9].ToString()) - 1;
                    else if (typeEl.Equals("Combined")) // combined
                    Definizioni.elemSegment[10] = int.Parse(Definizioni.elemSegment[10].ToString()) - 1;
                    else if (typeEl.Equals("Creative")) // creative
                    Definizioni.elemSegment[11] = int.Parse(Definizioni.elemSegment[11].ToString()) - 1;
                progEl--;

                currentElem--;
                lv.Items[count - 1].Remove();

                Utility.SendBroadcast("<ANNULLA/>"); // invio il comando di annulla ai giudici
                Utility.ScrivoElementoOnDB(lv, log, true);
                
                // Aggiorno i totali
                Utility.AggiornoTotali(log, lv, deductions, elements, total);

                if (count > 0) back.Enabled = true;
                else back.Enabled = false;

                WriteLog();
                Thread.Sleep(200);
                Cursor.Show();
                error.ResetText(); error.Visible = false;
            }

            catch (Exception ex)
            {
                Cursor.Show();
                Utility.WriteLog("ClearElement: " + ex.Message, "ERROR");
            }
        }
        #endregion

        #region PASSI, CLUSTER, DANCESPINS, ...

        /**********************************************************************************************
        *  Steps
        **********************************************************************************************
        * [0] = durata            [1] = factor         [2] = NoHoldStepSeq  [3] = DanceHoldStepSequence
        * [4] = PatternDanceSeq   [5] = DanceLift      [6] = NoHoldClustSeq [7] = TravellingSeq 
        * [8] = StraightStepSeq   [9] = CircularStepSeq[10] = NumElements   [11] = ChoreoStep
        * [12] = Par3        
        * ********************************************************************************************
        * [0] = durata            [1] = factor         [2] = NoHoldBlock    [3] = LinearBlock
        * [4] = WheelsTravelling  [5] = WheelsRotating [6] = CirclesTravell [7] = CirclesRotating
        * [8] = LinearLine        [9] = Intersections  [10] = CombinedEl    [11] = Creative
        * [12] = LinePivoting     [13] = BlockPivoting
        * ********************************************************************************************/
        private void InsertPassi_Click(object sender, EventArgs e)
        {
            try
            {
                var element = (Button)sender;
                InsertPassi(element);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void InsertPassi(Button steps)
        {
            try
            {
                string typeEl = "", codeTypeEl = "", parent = "";
                int indexEl = 0;
                addAsterisco = false;
                Color colorRow = Color.White;
                Cursor.Hide();
                parent = steps.Parent.Name;

                switch (parent)
                {
                    // Patterns
                    case "gbPattern":
                        typeEl = "Pattern"; codeTypeEl = "DP";
                        colorRow = rigaPattern; 
                        indexEl = 4;
                        break;
                    // Lifts
                    case "gbLifts":
                        typeEl = "Lift"; codeTypeEl = "DLif";
                        colorRow = rigaLifts;
                        indexEl = 5;
                        break;
                    // Sequences
                    case "panTrav":
                        typeEl = "TravClust"; codeTypeEl = "Tra";
                        colorRow = rigaTravClust;
                        indexEl = 7;
                        break;
                    case "panNoHold":
                        typeEl = "ClustNoHold"; codeTypeEl = "DSeq";
                        colorRow = rigaTravClust;
                        indexEl = 6;
                        break;
                    case "panHold":
                        typeEl = "ClustHold"; codeTypeEl = "DSeq";
                        colorRow = rigaTravClust;
                        indexEl = 12;
                        break;
                    // Steps
                    case "panCircular":
                        typeEl = "StepsCirc"; codeTypeEl = "DSt";
                        colorRow = rigaSteps;
                        indexEl = 9;
                        break;
                    case "panStraight":
                        typeEl = "StepsStra"; codeTypeEl = "DSt";
                        colorRow = rigaSteps;
                        indexEl = 8;
                        break;
                    case "panChoreo":
                        typeEl = "StepsChor"; codeTypeEl = "DSt";
                        colorRow = rigaSteps;
                        indexEl = 11;
                        break;
                    case "panStepNoHold":
                        typeEl = "StepsNH"; codeTypeEl = "DSt";
                        colorRow = rigaSteps;
                        indexEl = 2;
                        break;
                    case "panStepHold":
                        typeEl = "StepsH"; codeTypeEl = "DSt";
                        colorRow = rigaSteps;
                        indexEl = 3;
                        break;
                    // precision
                    // TRAVELLING
                    case "panWTrav":
                        typeEl = "TravWheel"; codeTypeEl = "WTr";
                        colorRow = rigaTrav;
                        indexEl = 4;
                        break;
                    case "panCTrav":
                        typeEl = "TravCircle"; codeTypeEl = "CTr";
                        colorRow = rigaTrav;
                        indexEl = 6;
                        break;
                    // ROTATING
                    case "panWRota":
                        typeEl = "RotatingWheel"; codeTypeEl = "WRo";
                        colorRow = rigaRota;
                        indexEl = 5;
                        break;
                    case "panCRota":
                        typeEl = "RotatingCircle"; codeTypeEl = "CRo";
                        colorRow = rigaRota;
                        indexEl = 7;
                        break;
                    // PIVOTING
                    case "panLPivo":
                        typeEl = "PivotingLine"; codeTypeEl = "LPi";
                        colorRow = rigaPivo;
                        indexEl = 12;
                        break;
                    case "panBPivo":
                        typeEl = "PivotingBlock"; codeTypeEl = "BPi";
                        colorRow = rigaPivo;
                        indexEl = 13;
                        break;
                    // LINEAR
                    case "panLline":
                        typeEl = "LinearLine"; codeTypeEl = "LLi";
                        colorRow = rigaLine;
                        indexEl = 8;
                        break;
                    case "panBLinear":
                        typeEl = "LinearBlock"; codeTypeEl = "LBl";
                        colorRow = rigaLine;
                        indexEl = 3;
                        break;
                    // PRECISION
                    case "panBNH":
                        typeEl = "No Hold"; codeTypeEl = "BNh";
                        colorRow = rigaPrec;
                        indexEl = 2;
                        break;
                    case "panInters":
                        typeEl = "Intersection"; codeTypeEl = "Int";
                        colorRow = rigaPrec;
                        indexEl = 9;
                        break;
                    case "panCInters":
                        typeEl = "IntersCreative"; codeTypeEl = "IntC";
                        colorRow = rigaPrec;
                        indexEl = 9;
                        break;
                    case "panComb":
                        typeEl = "Combined"; codeTypeEl = "Com";
                        colorRow = rigaPrec;
                        indexEl = 10;
                        break;
                    case "panPChoreo":
                        typeEl = "Creative"; codeTypeEl = "Cre";
                        colorRow = rigaPrec;
                        indexEl = 11;
                        break;
                }

                if (!CheckProgram(steps, parent))
                {
                    addAsterisco = true;
                }


                if (!Definizioni.updating)
                {
                    Definizioni.elemSegment[indexEl] = int.Parse(Definizioni.elemSegment[indexEl].ToString()) + 1;
                    lv.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "", "", "", "" }, -1));
                }
                else
                {
                    // %%% modifica del 18/10/2018 - 2.0
                    if (lv.Items[currentElem].SubItems[7].Text.Contains("*"))
                        addAsterisco = true;
                    lv.Items[currentElem].SubItems[7].Text = "";
                }
                lv.Items[currentElem].SubItems[1].Text = ((DataRow)steps.Tag)[3].ToString();
                lv.Items[currentElem].SubItems[2].Text = ((DataRow)steps.Tag)[7].ToString();
                lv.Items[currentElem].SubItems[3].Text = typeEl;
                lv.Items[currentElem].SubItems[4].Text = codeTypeEl;
                lv.Items[currentElem].SubItems[5].Text = "0";
                lv.Items[currentElem].SubItems[6].Text = "";
                lv.Items[currentElem].ToolTipText = ((DataRow)steps.Tag)[1].ToString();
                lv.Items[currentElem].ForeColor = colorRow;
                if (!Definizioni.updating)
                {
                    if (Definizioni.idSegment != 6)
                        Definizioni.elemSegment[10] = int.Parse(Definizioni.elemSegment[10].ToString()) + 1;
                    progEl++;
                }
                if (!Definizioni.updating)
                    lv.Items[currentElem].Text = progEl + "";

                if (addAsterisco) lv.Items[currentElem].SubItems[7].Text = "(*)";

                lv.Items[currentElem].Selected = true;
                if (Utility.InvioDati(lv, log))
                    Utility.ScrivoElementoOnDB(lv, log, false);
                // aggiorno totali
                if (!addAsterisco)
                    Utility.AggiornoTotali(log, lv, deductions, elements, total);

                ResetAfterInsertion();
            }
            catch (Exception ex)
            {
                Cursor.Show();
                Utility.WriteLog("InsertSteps: " + ex.Message, "ERROR");
            }
        }

        #endregion

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                double durata = Convert.ToDouble(Definizioni.paramSegment[0]);

                time = time.Add(TimeSpan.FromMilliseconds(1000));
                ltimer.Text = string.Format("{0:00}:{1:00}", time.Minutes,
                    time.Seconds);

                if (time.TotalSeconds < (durata - tolleranza))
                {
                    // lascio lo stesso colore
                }
                else if (time.TotalSeconds >= (durata - tolleranza) &&
                         time.TotalSeconds <= (durata + tolleranza))
                {
                    ltimer.ForeColor = Color.Lime;
                }
                else if (time.TotalSeconds > (durata + tolleranza))// oltre la tolleranza
                {
                    ltimer.ForeColor = Color.Red;
                }

                
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Timer1_Tick: " + ex.Message, "ERROR");
            }
        }

        #region DEDUCTIONS
        /**********************************************************
         *  DEDUCTIONS
         * ********************************************************/
        private void ManageDeductions(NumericUpDown control)
        {
            try
            {
                deductions.Text = "-" + (ded1.Value + ded2.Value + ded3.Value + ded4.Value + ded5.Value).ToString("F");
                Definizioni.deductionsDec = decimal.Parse(deductions.Text);
                total.Text = String.Format("{0:0.00}", Definizioni.elementsDec + Definizioni.deductionsDec);
                
                // 2.0.0.10 - gestisco le tipologie di deductions per i report finali
                string numeroDed = control.Name.Substring(control.Name.Length - 1);
                Definizioni.arDeductions[int.Parse(numeroDed) - 1] = decimal.Parse(control.Value.ToString("F"));
            }
            catch (Exception ex)
            {
                Utility.WriteLog("ManageDeductions: " + ex.Message, "ERROR");
            }
        }
        private void ded1_ValueChanged(object sender, EventArgs e)
        {
            ManageDeductions(ded1);
        }
        private void ded4_ValueChanged(object sender, EventArgs e)
        {
            ManageDeductions(ded4);
        }
        private void ded2_ValueChanged(object sender, EventArgs e)
        {
            ManageDeductions(ded2);
        }
        private void ded5_ValueChanged(object sender, EventArgs e)
        {
            ManageDeductions(ded5);
        }
        private void ded3_ValueChanged(object sender, EventArgs e)
        {
            ManageDeductions(ded3);
        }

        private void next_Click_1(object sender, EventArgs e)
        {
            NewSegment();
        }
        #endregion
        
        #region CONTROLLI
        public void DisableGroupBox()
        {

            if (Definizioni.idSpecialita == 7)
            {
                gbLinear.Enabled = false;
                gbTravelling.Enabled = false;
                gbPrecision.Enabled = false;
                gbLinear.Enabled = false;
                gbRotating.Enabled = false;
                gbPivoting.Enabled = false;
                panBLinear.Enabled = false;
                panBNH.Enabled = false;
                panPChoreo.Enabled = false;
                panComb.Enabled = false;
                panCInters.Enabled = false;
                panInters.Enabled = false;
                panLline.Enabled = false;
                panCTrav.Enabled = false;
                panCRota.Enabled = false;
                panWRota.Enabled = false;
                panWTrav.Enabled = false;
                panBPivo.Enabled = false;
                panLPivo.Enabled = false;
            }
            else
            {
                gbPattern.Enabled = false;
                gbLifts.Enabled = false;
                panTrav.Enabled = false;
                panNoHold.Enabled = false;
                panHold.Enabled = false;
                panStepNoHold.Enabled = false;
                panStepHold.Enabled = false;
                panCircular.Enabled = false;
                panStraight.Enabled = false;
                panChoreo.Enabled = false;
            }
        }

        public void EnableGroupBox()
        {
            if (Definizioni.idSpecialita == 4) // Danza
            {
                gbPattern.Enabled = true;
                gbLifts.Enabled = true;
                panTrav.Enabled = true;
                panNoHold.Enabled = true;

                panStepNoHold.Enabled = true;
                panStepHold.Enabled = true;
                panHold.Enabled = true;
            }
            else if (Definizioni.idSpecialita == 7) // Precision
            {
                gbTravelling.Enabled = true;
                gbPrecision.Enabled = true;
                gbLinear.Enabled = true;
                gbRotating.Enabled = true;
                gbPivoting.Enabled = true;
                panBLinear.Enabled = true;
                panCInters.Enabled = true;
                panBNH.Enabled = true;
                panChoreo.Enabled = true;
                panPChoreo.Enabled = true;
                panComb.Enabled = true;
                panInters.Enabled = true;
                panLline.Enabled = true;
                panCTrav.Enabled = true;
                panCRota.Enabled = true;
                panWRota.Enabled = true;
                panWTrav.Enabled = true;
                panBPivo.Enabled = true;
                panLPivo.Enabled = true;
            }
            else // solo dance
            {
                gbPattern.Enabled = true;
                gbLifts.Enabled = true;
                panTrav.Enabled = true;
                panNoHold.Enabled = true;
                panCircular.Enabled = true;
                panStraight.Enabled = true;
                panChoreo.Enabled = true;
            }
        }

        private void lv_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = lv.Columns[e.ColumnIndex].Width;
        }

        // Blocco la larghezza delle colonne
        private void lv_ColumnWidthChanging_1(object sender, ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = lv.Columns[e.ColumnIndex].Width;
        }

        private void lv_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
                e.Item.Selected = false;
        }

        private void Tooltips(Button j)
        {
            tt.ToolTipTitle = j.Name;
            if (j.BackColor == Color.Yellow)
                tt.SetToolTip(j, Definizioni.resources["waiting"].ToString());
            else if (j.BackColor == Color.Transparent && j.ForeColor == Color.Lime)
            {
                tt.SetToolTip(j, Definizioni.resources["connected"].ToString() + "\r\n" );
                tt.ForeColor = Color.Black;
                tt.ToolTipIcon = ToolTipIcon.Info;
            }
            else if (j.BackColor == Color.Transparent && j.ForeColor == Color.Red)
            {
                tt.SetToolTip(j, Definizioni.resources["notconnected"].ToString() + "\r\n");
                tt.ForeColor = Color.Black;
                tt.ToolTipIcon = ToolTipIcon.Warning;
            }
            if (j.Font.Underline)
            {
                tt.SetToolTip(j, "Ping timeout");
                tt.ForeColor = Color.Black;
                tt.ToolTipIcon = ToolTipIcon.Warning;
            }
        }
        
        private void j1_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j1);
        }

        private void j2_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j2);
        }

        private void j3_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j3);
        }

        private void j4_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j4);
        }

        private void j5_Click(object sender, EventArgs e)
        {
            Tooltips(j5);
        }

        private void j6_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j6);
        }

        private void j7_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j7);
        }

        private void j8_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j8);
        }

        private void j9_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j9);
        }

        private void lv_MouseClick(object sender, MouseEventArgs e)
        {
            if (lv.SelectedItems.Count != 0)
            {
                review.Enabled = true;
                asterisco.Enabled = true;
            }
            else
            {
                review.Enabled = false;
                asterisco.Enabled = false;
            }
        }

		private void lv_MouseDown(object sender, MouseEventArgs e)
        {
            ListViewItem selection = lv.GetItemAt(e.X, e.Y);

     	    if (selection != null)
		    {
                //edit.Enabled = true;
            }
            else
            {
                //edit.Enabled = false;
            }
        }
        private void button1_Click_3(object sender, EventArgs e)
        {
            Info inf = new Info();
            inf.ShowDialog();
        }

		// REVIEW
        private void edit_Click(object sender, EventArgs e)
        {
            try
            {
                if (lv.SelectedItems[0] == null) return;
                int indexSelectedItem = lv.SelectedItems[0].Index;

                if (lv.Items[indexSelectedItem].ImageIndex == 2) // già in Review
                {
                    lv.Items[indexSelectedItem].ImageIndex = 0;
                    lv.Items[indexSelectedItem].ToolTipText =
                        lv.Items[indexSelectedItem].ToolTipText.Split('-')[0];
                }
                else
                {
                    lv.Items[indexSelectedItem].ImageIndex = 2;
                    lv.Items[indexSelectedItem].ToolTipText =
                        lv.Items[indexSelectedItem].ToolTipText + " - REVIEW";
                }
                review.Enabled = false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Review: " + ex.Message, "ERROR");
            }
        }

        private void j1_Click_1(object sender, EventArgs e)
        {
            Utility.SendToSingleJudge("<HURRYUP/>", int.Parse(((Button)sender).Tag.ToString()));
        }

        private void j5_MouseEnter(object sender, EventArgs e)
        {
            Tooltips(j5);
        }

        private void asterisco_Click(object sender, EventArgs e)
        {
            try
            {
                if (lv.SelectedItems[0] == null) return;
                int indexSelectedItem = lv.SelectedItems[0].Index;

                if (lv.Items[indexSelectedItem].SubItems[7].Text.Contains("(*)")) // già asteriscato
                {
                    lv.Items[indexSelectedItem].SubItems[7].Text =
                        lv.Items[indexSelectedItem].SubItems[7].Text.Replace("(*)", ""); // elimino asterisco
                    Utility.UpdateElementConAsterisco(indexSelectedItem + 1, false);
                }
                else
                {
                    lv.Items[indexSelectedItem].SubItems[7].Text = "(*)";
                    Utility.UpdateElementConAsterisco(indexSelectedItem + 1, true);
                }
                asterisco.Enabled = false;

                Utility.AggiornoTotali(log, lv, deductions, elements, total); 
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Add *: " + ex.Message, "ERROR");
            }
        }

        private void panPChoreo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panComb_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panCInters_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panInters_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panBNH_Paint(object sender, PaintEventArgs e)
        {

        }

        private void gbLinear_Enter(object sender, EventArgs e)
        {

        }

        private void DanzaForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // *** Modifica del 05/12/2018 - 2.0.0.9 *** //
            if (!flagClose)
                e.Cancel = true;
        }

        private void prev_Click(object sender, EventArgs e)
        {
            if (Definizioni.currentPart == 1 || Definizioni.skipped) return;
            DialogResult result = MessageBox.Show("Display the previous skater scores?", "Warning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                Definizioni.currentPart = Definizioni.currentPart - 1;
                ut.DisplayScore();
                Definizioni.currentPart = Definizioni.currentPart + 1;
            }
        }

        private void j1_Click(object sender, EventArgs e)
        {
            //Utility.SendToSingleJudge("<HURRYUP/>", int.Parse(((Button)sender).Tag.ToString()));
        }
		// VERIFY SEGMENT
        private void cbVerify_CheckedChanged(object sender, EventArgs e)
        {
            if (cbVerify.Checked)
            {
                cbVerify.FlatAppearance.MouseOverBackColor = Color.Lime;
            }
            else cbVerify.FlatAppearance.MouseOverBackColor = Color.Gainsboro;
        }
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Ghost;

namespace RollartSystemTech
{
    public partial class CheckScores : Form
    {
        int segment = 0, currentSkater = 1, totalSkaters = 0, numJudges = 0, idSpec = 0, idCat = 0, catElement = 0;
        string spec, cat, bonus = "", pen = "";
        DataTable dtSkaters = null, dtElements = null;
        List<DataRow> listElements = null;
        SQLiteDataReader sqdr = null, sqdrElement = null;
        DataGridViewRow oldDr = null;

        public CheckScores(int idSegm)
        {
            InitializeComponent();
            segment = idSegm;
        }

        // update Element
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                message.Text = "";
                // mi salvo la vecchia datarow
                //oldDr = dgElements.SelectedRows[0];

                // inserisco percentuale 
                if (comboPerc.SelectedIndex > 0)
                {
                    if (!dgElements.SelectedRows[0].Cells[27].Value.ToString().Contains("%"))
                        dgElements.SelectedRows[0].Cells[27].Value += "%";
                    // aggiungo la percentuale al base value 
                    decimal valore = decimal.Parse(dgElements.SelectedRows[0].Cells[6].Value.ToString());
                    decimal perc = decimal.Parse(comboPerc.SelectedItem.ToString().Split('%')[0].TrimEnd());
                    dgElements.SelectedRows[0].Cells[7].Value = valore +
                        Math.Round(((valore * perc) / 100), 2); 
                    ImpostoNuoviColori();
                }
                else // tolgo percentuale
                {
                    if (dgElements.SelectedRows[0].Cells[27].Value.ToString().Contains("%"))
                    {
                        dgElements.SelectedRows[0].Cells[27].Value =
                            dgElements.SelectedRows[0].Cells[27].Value.ToString().Replace("%", "");
                        // rimuovo la percentuale al base value 
                        decimal valore = decimal.Parse(dgElements.SelectedRows[0].Cells[6].Value.ToString());
                        decimal perc = decimal.Parse(comboPerc.SelectedItem.ToString().Split('%')[0].TrimEnd());
                        dgElements.SelectedRows[0].Cells[7].Value = valore +
                            Math.Round(((valore * perc) / 100), 2);
                        RipristinoColori();
                    }
                }

                // Inserisco T
                if (cb4.Checked)
                {
                    if (!dgElements.SelectedRows[0].Cells[27].Value.ToString().Contains("T"))
                        dgElements.SelectedRows[0].Cells[27].Value += "T";
                    // aggiungo il 10% dall base value e finalvalue
                    decimal valore = decimal.Parse(dgElements.SelectedRows[0].Cells[6].Value.ToString());
                    dgElements.SelectedRows[0].Cells[7].Value = valore + 
                        Math.Round(((valore * 10) / 100), 2); // 10%
                    ImpostoNuoviColori();
                }
                else // tolgo T
                {
                    if (dgElements.SelectedRows[0].Cells[27].Value.ToString().Contains("T"))
                    {
                        dgElements.SelectedRows[0].Cells[27].Value =
                        dgElements.SelectedRows[0].Cells[27].Value.ToString().Replace("T", "");
                        // tolgo il 10% dall base value e finalvalue
                        decimal valore = decimal.Parse(dgElements.SelectedRows[0].Cells[6].Value.ToString());
                        dgElements.SelectedRows[0].Cells[7].Value = valore -
                            Math.Round(((valore * 10) / 100), 2); // 10%
                        RipristinoColori();
                    }
                }

                // Inserisco B
                if (cb2.Checked)
                {
                    dgElements.SelectedRows[0].Cells[27].Value = "+";
                    // aggiungo 2 sia al valore base che final
                    dgElements.SelectedRows[0].Cells[6].Value =
                        (decimal.Parse(dgElements.SelectedRows[0].Cells[6].Value.ToString()) + 2.0m);
                    dgElements.SelectedRows[0].Cells[7].Value =
                        (decimal.Parse(dgElements.SelectedRows[0].Cells[7].Value.ToString()) + 2.0m);
                    ImpostoNuoviColori();

                }
                else // tolgo B
                {
                    if (dgElements.SelectedRows[0].Cells[27].Value.ToString().Contains("+"))
                    {
                        dgElements.SelectedRows[0].Cells[27].Value =
                            dgElements.SelectedRows[0].Cells[27].Value.ToString().Replace("+", "");
                        dgElements.SelectedRows[0].Cells[7].Value =
                            (decimal.Parse(dgElements.SelectedRows[0].Cells[6].Value.ToString()) - 2.0m);
                        dgElements.SelectedRows[0].Cells[7].Value =
                            (decimal.Parse(dgElements.SelectedRows[0].Cells[7].Value.ToString()) - 2.0m);
                        RipristinoColori();
                    }
                }

                // Inserisco *
                if (cb3.Checked)
                {
                    dgElements.SelectedRows[0].Cells[27].Value = "*";
                    dgElements.SelectedRows[0].Cells[7].Value = DBNull.Value;
                    ImpostoNuoviColori();
                }
                else // tolgo asterisco
                {
                    if (dgElements.SelectedRows[0].Cells[27].Value.ToString().Contains("*"))
                    {
                        dgElements.SelectedRows[0].Cells[27].Value =
                        dgElements.SelectedRows[0].Cells[27].Value.ToString().Replace("*", "");
                        RipristinoColori();
                    }
                }

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void RipristinoColori()
        {
            dgElements.SelectedRows[0].Cells[27].Style.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dgElements.SelectedRows[0].Cells[27].Style.SelectionForeColor = Color.White;
            dgElements.SelectedRows[0].Cells[27].Style.ForeColor = Color.Black;
            dgElements.SelectedRows[0].Cells[7].Style.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dgElements.SelectedRows[0].Cells[7].Style.SelectionForeColor = Color.White;
            dgElements.SelectedRows[0].Cells[7].Style.ForeColor = Color.Black;
        }

        private void ImpostoNuoviColori()
        {
            dgElements.SelectedRows[0].Cells[27].Style.SelectionBackColor = Color.Yellow;
            dgElements.SelectedRows[0].Cells[27].Style.SelectionForeColor = Color.Red;
            dgElements.SelectedRows[0].Cells[27].Style.ForeColor = Color.Red;
            dgElements.SelectedRows[0].Cells[7].Style.SelectionBackColor = Color.Yellow;
            dgElements.SelectedRows[0].Cells[7].Style.SelectionForeColor = Color.Red;
            dgElements.SelectedRows[0].Cells[7].Style.ForeColor = Color.Red;
        }

            // faccio UNDO, ripristino datarow originale
        private void button2_Click(object sender, EventArgs e)
        {
            int i = 0;
            try
            {
                message.Text = "";
                int indexElement = dgElements.SelectedRows[0].Index;
                for (i=0;i< dgElements.Columns.Count; i++)
                {
                    dgElements.Rows[indexElement].Cells[i].Value =
                        dtElements.Rows[indexElement].ItemArray[i].ToString();

                }
                dgElements.Rows[indexElement].Cells[4].Value =
                        dtElements.Rows[indexElement].ItemArray[29].ToString();
                dgElements.Rows[indexElement].Cells[5].Value =
                        dtElements.Rows[indexElement].ItemArray[30].ToString();
                dgElements.Rows[indexElement].Cells[6].Value =
                        dtElements.Rows[indexElement].ItemArray[31].ToString();
                dgElements.Rows[indexElement].Cells[7].Value =
                        dtElements.Rows[indexElement].ItemArray[32].ToString();
                dgElements.Rows[indexElement].Cells[27].Value =
                        dtElements.Rows[indexElement].ItemArray[33].ToString();
                RipristinoColori();

            }
            catch (Exception ex)
            {
                message.Text = "Element " + i + ": " + ex.Message;
            }
        }

        private void dgElements_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                
                
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void numElement_Click(object sender, EventArgs e)
        {

        }

        private void CheckScores_Load(object sender, EventArgs e)
        {
            try
            {
                comboPerc.SelectedIndex = 0;

                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    dtSkaters = new DataTable();
                    cmd.CommandText =
                        " SELECT P.Position, A.Name, A.Country, P.NumPartecipante" +
                        " FROM GaraFinal P, Athletes A " +
                        " WHERE A.ID_ATLETA = P.ID_ATLETA" +
                        " AND P.ID_GARAPARAMS = " + Definizioni.idGaraParams +
                        " AND P.ID_SEGMENT = " + segment + 
                        " ORDER BY P.Position";
                    sqdr = cmd.ExecuteReader();
                    dtSkaters.Load(sqdr);
                }
                // recupero info gara, numero giudici
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    cmd.CommandText =
                        " SELECT NumJudges, S.ID_Specialita, S.Name, C.Name, C.ID_Category" +
                        " FROM GaraParams A, Specialita S, Category C " +
                        " WHERE A.ID_GARAPARAMS = " + Definizioni.idGaraParams +
                        " AND A.ID_SPECIALITA = S.ID_SPECIALITA" +
                        " AND A.ID_CATEGORY = C.ID_CATEGORY" +
                        " AND A.ID_SEGMENT = " + segment;
                    sqdr = cmd.ExecuteReader();
                    while (sqdr.Read())
                    {
                        numJudges = int.Parse(sqdr[0].ToString());
                        idSpec = int.Parse(sqdr[1].ToString());
                        spec = sqdr[2].ToString();
                        cat = sqdr[3].ToString();
                        idCat = int.Parse(sqdr[4].ToString());
                    }
                    sqdr.Close();
                }
                totalSkaters = dtSkaters.Rows.Count;
                GetSkater(currentSkater);

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            currentSkater = 1;
            GetSkater(currentSkater);
        }

        
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (currentSkater == totalSkaters) return;
            currentSkater++;
            GetSkater(currentSkater);
            
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            currentSkater = totalSkaters;
            GetSkater(currentSkater);
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (currentSkater == 1) return;
            currentSkater--;
            GetSkater(currentSkater);
            
        }

        private void GetSkater(int numSkater)
        {
            try
            {
                rank.Text = dtSkaters.Rows[numSkater - 1][0].ToString();
                numSkaters.Text = "of {" + totalSkaters + "}";
                name.Text = dtSkaters.Rows[numSkater -1][1].ToString() + " (" +
                    dtSkaters.Rows[numSkater - 1][2] + ")";
                numPartecipante.Text = dtSkaters.Rows[numSkater - 1][3].ToString();

                // recupero dettaglio segmento
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    dtElements = new DataTable();
                    cmd.CommandText =
                        " SELECT * FROM Gara WHERE ID_GARAPARAMS = " + Definizioni.idGaraParams +
                        " AND ID_SEGMENT = " + segment + "" + 
                        " AND NumPartecipante = '" + numPartecipante + 
                        "' AND ProgressivoEl not like 'comp%'";

                    sqdr = cmd.ExecuteReader();
                    dtElements.Load(sqdr);

                    dgElements.Columns.Clear();
                    dgElements.DataSource = dtElements;
                    dtElements.Columns.Add("elementOld");
                    dtElements.Columns.Add("penOld");
                    dtElements.Columns.Add("valueOld");
                    dtElements.Columns.Add("valueFinalOld");
                    dtElements.Columns.Add("bonusOld");

                    dgElements.Columns[0].Visible = false;
                    dgElements.Columns[1].Visible = false;
                    dgElements.Columns[2].Visible = false;
                    dgElements.Columns[8].Visible = false; // numcombo
                    for (int i = 0; i < (9 - numJudges); i++)
                    {
                        dgElements.Columns[9 + numJudges + i].Visible = false;
                    }
                    for (int i = 0; i < numJudges; i++)
                    {
                        dgElements.Columns[9 + i].Visible = true;
                        dgElements.Columns[9 + i].HeaderText = "Judge " + (i + 1);
                    }
                    for (int i = 0; i < (9); i++)
                    {
                        dgElements.Columns[18 + i].Visible = false;
                    }
                    dgElements.Columns[27].HeaderText = "Bonus/*";
                    dgElements.Columns[28].Visible = false; // progrCombo

                    // aggiungo altra colonna con ValueFinal 
                    //dgElements.Columns.Add("valueFinalOld", "old");
                    
                    int count = 0;
                    foreach (DataRow dr in dtElements.Rows)
                    {
                        dgElements.Rows[count].Cells[29].Value = dgElements.Rows[count].Cells[4].Value;
                        dtElements.Rows[count][29] = dgElements.Rows[count].Cells[4].Value;
                        dgElements.Rows[count].Cells[30].Value = dgElements.Rows[count].Cells[5].Value;
                        dtElements.Rows[count][30] = dgElements.Rows[count].Cells[5].Value;
                        dgElements.Rows[count].Cells[31].Value = dgElements.Rows[count].Cells[6].Value;
                        dtElements.Rows[count][31] = dgElements.Rows[count].Cells[6].Value;
                        dgElements.Rows[count].Cells[32].Value = dgElements.Rows[count].Cells[7].Value;
                        dtElements.Rows[count][32] = dgElements.Rows[count].Cells[7].Value;
                        dgElements.Rows[count].Cells[33].Value = dgElements.Rows[count].Cells[27].Value;
                        dtElements.Rows[count][33] = dgElements.Rows[count].Cells[27].Value;
                        count++;
                    }
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        // elemento selezionato
        private void dgElements_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                string element = "";

                if (dgElements.SelectedRows.Count > 0)
                {
                    pen = dgElements.SelectedRows[0].Cells[5].Value.ToString();
                    bonus = dgElements.SelectedRows[0].Cells[27].Value.ToString();
                    // recupero dettaglio elemento e categoria
                    decimal valueEl = 0;
                    using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                    {

                        cmd.CommandText =
                            " SELECT Name, ID_ElementsCat, Base FROM Elements WHERE" +
                            " Code = '" + dgElements.SelectedRows[0].Cells[4].Value + "'";

                        sqdr = cmd.ExecuteReader();
                        while (sqdr.Read())
                        {
                            gb.Text = "Element " + (dgElements.SelectedRows[0].Index + 1) + ". ";
                                //+ sqdr[0].ToString() + " " + dgElements.SelectedRows[0].Cells[5].Value +
                                //"" + dgElements.SelectedRows[0].Cells[27].Value;
                            catElement = int.Parse(sqdr[1].ToString());
                            label2.Text = catElement + "";
                            element = dgElements.SelectedRows[0].Cells[4].Value + " - " +
                                sqdr[0].ToString();
                            valueEl = decimal.Parse(sqdr[2].ToString());
                        }
                        sqdr.Close();
                    }

                    // penalty
                    if (pen.Equals("<")) rb1.Checked = true;
                    else if (pen.Equals("<<")) rb2.Checked = true;
                    else if (pen.Equals("<<<")) rb3.Checked = true;
                    else rb4.Checked = true;
                    // bonus
                    if (bonus.Contains("*")) cb3.Checked = true; else cb3.Checked = false;
                    if (bonus.Contains("B")) cb2.Checked = true; else cb2.Checked = false;
                    if (bonus.Contains("T")) cb4.Checked = true; else cb4.Checked = false;
                    // combo
                    if (!dgElements.SelectedRows[0].Cells[8].Value.ToString().Equals("0"))
                    {
                        cb1.Checked = true;
                        cb1.Text = "Combo " + dgElements.SelectedRows[0].Cells[8].Value.ToString();
                    }
                    else
                    {
                        cb1.Checked = false;
                        cb1.Text = "Combo ";
                    }

                    DisableControls();
                    switch (catElement)
                    {
                        case 1: // jumps
                        case 4: // throw jumps
                        case 8: // twist jumps
                            EnableControls();
                            cb2.Enabled = false;
                            break;
                        case 2:// spins
                        case 7://contact spins
                            cb1.Enabled = true; cb2.Enabled = true; comboPerc.Enabled = true;
                            break;
                        default:
                            break;
                    }

                    // recupero tutti gli elementi della stessa categoria
                    
                    using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                    {
                        cmd.CommandText =
                            " SELECT Name, Code, Base FROM Elements WHERE" +
                            " ID_ElementsCat = " + catElement + "";
                        sqdr = cmd.ExecuteReader();
                        cbElements.Items.Clear();
                        while (sqdr.Read())
                        {
                            cbElements.Items.Add(sqdr[1].ToString() + " - " + sqdr[0].ToString());
                            
                        }
                        sqdr.Close();

                        cbElements.SelectedItem = element;
                    }

                    if (bonus.Contains("%"))
                    {
                        decimal valueElfinal = decimal.Parse(dgElements.SelectedRows[0].Cells[6].Value.ToString());
                        comboPerc.SelectedItem = (Utility.GetPercentage(valueElfinal, valueEl, 0)) + "%";
                    } else
                    {
                        comboPerc.SelectedItem = "0%";
                    }
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

        private void EnableControls()
        {
            rb1.Enabled = true; rb2.Enabled = true; rb3.Enabled = true;
            cb1.Enabled = true; cb2.Enabled = true; comboPerc.Enabled = true;
            cb4.Enabled = true; rb4.Enabled = true;
        }
        private void DisableControls()
        {
            rb1.Enabled = false; rb2.Enabled = false; rb3.Enabled = false;
            cb1.Enabled = false; cb2.Enabled = false; comboPerc.Enabled = false;
            cb4.Enabled = false; rb4.Enabled = false;
        }


        private void cbElements_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                switch (catElement)
                {
                    case 1: // jumps
                    case 4: // throw jumps
                    case 8: // twist jumps
                        // elemento nullo
                        if (cbElements.SelectedItem.ToString().StartsWith("N"))
                        {
                            cb4.Enabled = false;
                            comboPerc.Enabled = false;
                            rb1.Enabled = false; rb2.Enabled = false;
                            rb3.Enabled = false; rb4.Enabled = false;
                        } else
                        {
                            cb4.Enabled = true;
                            comboPerc.Enabled = true;
                            rb1.Enabled = true; rb2.Enabled = true;
                            rb3.Enabled = true; rb4.Enabled = true;
                        }
                        break;
                    case 2:
                    case 7:
                        // elemento nullo
                        if (cbElements.SelectedItem.ToString().StartsWith("N"))
                        {
                            cb4.Enabled = false; cb2.Enabled = false;
                            comboPerc.Enabled = false;
                        }
                        else
                        {
                            cb4.Enabled = true; cb2.Enabled = true;
                            comboPerc.Enabled = true;
                        }
                        break;
                    default:
                        break;
                }
                // recupero dettaglio elemento
                using (SQLiteCommand cmd = Definizioni.conn.CreateCommand())
                {
                    if (cbElements.SelectedIndex > 0)
                    {
                        cmd.CommandText =
                            " SELECT * FROM Elements WHERE" +
                            " Code = '" + cbElements.SelectedItem.ToString().Split('-')[0].TrimEnd() + "'";
                        sqdrElement = cmd.ExecuteReader();
                        while (sqdrElement.Read())
                        {
                            element.Tag = sqdrElement;
                            element.Text = sqdrElement[7].ToString();
                        } 
                        //sqdr.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

    }
}

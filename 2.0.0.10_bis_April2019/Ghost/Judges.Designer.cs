﻿namespace Ghost
{
    partial class Judges
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tp = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataop = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.ass = new System.Windows.Forms.TextBox();
            this.ju7 = new System.Windows.Forms.Label();
            this.ju6 = new System.Windows.Forms.Label();
            this.j6 = new System.Windows.Forms.TextBox();
            this.ju5 = new System.Windows.Forms.Label();
            this.j5 = new System.Windows.Forms.TextBox();
            this.ju4 = new System.Windows.Forms.Label();
            this.j4 = new System.Windows.Forms.TextBox();
            this.ju3 = new System.Windows.Forms.Label();
            this.j3 = new System.Windows.Forms.TextBox();
            this.ju2 = new System.Windows.Forms.Label();
            this.j2 = new System.Windows.Forms.TextBox();
            this.ju1 = new System.Windows.Forms.Label();
            this.j1 = new System.Windows.Forms.TextBox();
            this.ju9 = new System.Windows.Forms.Label();
            this.j9 = new System.Windows.Forms.TextBox();
            this.ju8 = new System.Windows.Forms.Label();
            this.j8 = new System.Windows.Forms.TextBox();
            this.j7 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.browse = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.referee = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.contr = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tp
            // 
            this.tp.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.tp.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.tp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tp.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp.Location = new System.Drawing.Point(182, 36);
            this.tp.Name = "tp";
            this.tp.Size = new System.Drawing.Size(335, 25);
            this.tp.TabIndex = 0;
            this.tp.TextChanged += new System.EventHandler(this.tp_TextChanged);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Location = new System.Drawing.Point(12, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 29);
            this.label1.TabIndex = 82;
            this.label1.Tag = "";
            this.label1.Text = "Technical Specialist *";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dataop
            // 
            this.dataop.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.dataop.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.dataop.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.dataop.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataop.Location = new System.Drawing.Point(182, 71);
            this.dataop.Name = "dataop";
            this.dataop.Size = new System.Drawing.Size(335, 25);
            this.dataop.TabIndex = 1;
            this.dataop.TextChanged += new System.EventHandler(this.dataop_TextChanged);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Location = new System.Drawing.Point(51, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 27);
            this.label2.TabIndex = 84;
            this.label2.Tag = "";
            this.label2.Text = "Data operator *";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label12.Location = new System.Drawing.Point(97, 101);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 28);
            this.label12.TabIndex = 86;
            this.label12.Tag = "";
            this.label12.Text = "Assistant";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ass
            // 
            this.ass.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ass.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ass.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ass.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ass.Location = new System.Drawing.Point(182, 104);
            this.ass.Name = "ass";
            this.ass.Size = new System.Drawing.Size(335, 25);
            this.ass.TabIndex = 2;
            // 
            // ju7
            // 
            this.ju7.AutoSize = true;
            this.ju7.BackColor = System.Drawing.Color.Transparent;
            this.ju7.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ju7.ForeColor = System.Drawing.Color.Black;
            this.ju7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ju7.Location = new System.Drawing.Point(69, 224);
            this.ju7.Name = "ju7";
            this.ju7.Size = new System.Drawing.Size(102, 17);
            this.ju7.TabIndex = 116;
            this.ju7.Tag = "";
            this.ju7.Text = "        Judge n° 7";
            this.ju7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ju6
            // 
            this.ju6.AutoSize = true;
            this.ju6.BackColor = System.Drawing.Color.Transparent;
            this.ju6.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ju6.ForeColor = System.Drawing.Color.Black;
            this.ju6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ju6.Location = new System.Drawing.Point(69, 195);
            this.ju6.Name = "ju6";
            this.ju6.Size = new System.Drawing.Size(102, 17);
            this.ju6.TabIndex = 114;
            this.ju6.Tag = "";
            this.ju6.Text = "        Judge n° 6";
            this.ju6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // j6
            // 
            this.j6.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.j6.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.j6.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.j6.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j6.Location = new System.Drawing.Point(182, 196);
            this.j6.Name = "j6";
            this.j6.Size = new System.Drawing.Size(335, 25);
            this.j6.TabIndex = 6;
            this.j6.TextChanged += new System.EventHandler(this.j1_TextChanged);
            // 
            // ju5
            // 
            this.ju5.AutoSize = true;
            this.ju5.BackColor = System.Drawing.Color.Transparent;
            this.ju5.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ju5.ForeColor = System.Drawing.Color.Black;
            this.ju5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ju5.Location = new System.Drawing.Point(69, 166);
            this.ju5.Name = "ju5";
            this.ju5.Size = new System.Drawing.Size(102, 17);
            this.ju5.TabIndex = 112;
            this.ju5.Tag = "";
            this.ju5.Text = "        Judge n° 5";
            this.ju5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // j5
            // 
            this.j5.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.j5.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.j5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.j5.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j5.Location = new System.Drawing.Point(182, 167);
            this.j5.Name = "j5";
            this.j5.Size = new System.Drawing.Size(335, 25);
            this.j5.TabIndex = 5;
            this.j5.TextChanged += new System.EventHandler(this.j1_TextChanged);
            // 
            // ju4
            // 
            this.ju4.AutoSize = true;
            this.ju4.BackColor = System.Drawing.Color.Transparent;
            this.ju4.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ju4.ForeColor = System.Drawing.Color.Black;
            this.ju4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ju4.Location = new System.Drawing.Point(69, 137);
            this.ju4.Name = "ju4";
            this.ju4.Size = new System.Drawing.Size(102, 17);
            this.ju4.TabIndex = 110;
            this.ju4.Tag = "";
            this.ju4.Text = "        Judge n° 4";
            this.ju4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // j4
            // 
            this.j4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.j4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.j4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.j4.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j4.Location = new System.Drawing.Point(182, 138);
            this.j4.Name = "j4";
            this.j4.Size = new System.Drawing.Size(335, 25);
            this.j4.TabIndex = 4;
            this.j4.TextChanged += new System.EventHandler(this.j1_TextChanged);
            // 
            // ju3
            // 
            this.ju3.AutoSize = true;
            this.ju3.BackColor = System.Drawing.Color.Transparent;
            this.ju3.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ju3.ForeColor = System.Drawing.Color.Black;
            this.ju3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ju3.Location = new System.Drawing.Point(69, 108);
            this.ju3.Name = "ju3";
            this.ju3.Size = new System.Drawing.Size(102, 17);
            this.ju3.TabIndex = 108;
            this.ju3.Tag = "";
            this.ju3.Text = "        Judge n° 3";
            this.ju3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // j3
            // 
            this.j3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.j3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.j3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.j3.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j3.Location = new System.Drawing.Point(182, 109);
            this.j3.Name = "j3";
            this.j3.Size = new System.Drawing.Size(335, 25);
            this.j3.TabIndex = 3;
            this.j3.TextChanged += new System.EventHandler(this.j1_TextChanged);
            // 
            // ju2
            // 
            this.ju2.AutoSize = true;
            this.ju2.BackColor = System.Drawing.Color.Transparent;
            this.ju2.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ju2.ForeColor = System.Drawing.Color.Black;
            this.ju2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ju2.Location = new System.Drawing.Point(69, 79);
            this.ju2.Name = "ju2";
            this.ju2.Size = new System.Drawing.Size(102, 17);
            this.ju2.TabIndex = 106;
            this.ju2.Tag = "";
            this.ju2.Text = "        Judge n° 2";
            this.ju2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // j2
            // 
            this.j2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.j2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.j2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.j2.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j2.Location = new System.Drawing.Point(182, 80);
            this.j2.Name = "j2";
            this.j2.Size = new System.Drawing.Size(335, 25);
            this.j2.TabIndex = 2;
            this.j2.TextChanged += new System.EventHandler(this.j1_TextChanged);
            // 
            // ju1
            // 
            this.ju1.AutoSize = true;
            this.ju1.BackColor = System.Drawing.Color.Transparent;
            this.ju1.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ju1.ForeColor = System.Drawing.Color.Black;
            this.ju1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ju1.Location = new System.Drawing.Point(69, 50);
            this.ju1.Name = "ju1";
            this.ju1.Size = new System.Drawing.Size(102, 17);
            this.ju1.TabIndex = 104;
            this.ju1.Tag = "";
            this.ju1.Text = "        Judge n° 1";
            this.ju1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // j1
            // 
            this.j1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.j1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.j1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.j1.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j1.Location = new System.Drawing.Point(182, 50);
            this.j1.Name = "j1";
            this.j1.Size = new System.Drawing.Size(335, 25);
            this.j1.TabIndex = 1;
            this.j1.TextChanged += new System.EventHandler(this.j1_TextChanged);
            // 
            // ju9
            // 
            this.ju9.AutoSize = true;
            this.ju9.BackColor = System.Drawing.Color.Transparent;
            this.ju9.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ju9.ForeColor = System.Drawing.Color.Black;
            this.ju9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ju9.Location = new System.Drawing.Point(69, 282);
            this.ju9.Name = "ju9";
            this.ju9.Size = new System.Drawing.Size(102, 17);
            this.ju9.TabIndex = 102;
            this.ju9.Tag = "";
            this.ju9.Text = "        Judge n° 9";
            this.ju9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // j9
            // 
            this.j9.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.j9.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.j9.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.j9.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j9.Location = new System.Drawing.Point(182, 283);
            this.j9.Name = "j9";
            this.j9.Size = new System.Drawing.Size(335, 25);
            this.j9.TabIndex = 9;
            this.j9.TextChanged += new System.EventHandler(this.j1_TextChanged);
            // 
            // ju8
            // 
            this.ju8.AutoSize = true;
            this.ju8.BackColor = System.Drawing.Color.Transparent;
            this.ju8.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ju8.ForeColor = System.Drawing.Color.Black;
            this.ju8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ju8.Location = new System.Drawing.Point(69, 253);
            this.ju8.Name = "ju8";
            this.ju8.Size = new System.Drawing.Size(102, 17);
            this.ju8.TabIndex = 100;
            this.ju8.Tag = "";
            this.ju8.Text = "        Judge n° 8";
            this.ju8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // j8
            // 
            this.j8.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.j8.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.j8.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.j8.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j8.Location = new System.Drawing.Point(182, 254);
            this.j8.Name = "j8";
            this.j8.Size = new System.Drawing.Size(335, 25);
            this.j8.TabIndex = 8;
            this.j8.TextChanged += new System.EventHandler(this.j1_TextChanged);
            // 
            // j7
            // 
            this.j7.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.j7.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.j7.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.j7.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j7.Location = new System.Drawing.Point(182, 225);
            this.j7.Name = "j7";
            this.j7.Size = new System.Drawing.Size(335, 25);
            this.j7.TabIndex = 7;
            this.j7.TextChanged += new System.EventHandler(this.j1_TextChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.AntiqueWhite;
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.contr);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.ass);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.tp);
            this.panel1.Controls.Add(this.dataop);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(532, 197);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DimGray;
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.browse);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 514);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(532, 67);
            this.panel2.TabIndex = 2;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button2.FlatAppearance.BorderSize = 2;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(290, 9);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(107, 49);
            this.button2.TabIndex = 2;
            this.button2.Text = "Clear All";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button1.FlatAppearance.BorderSize = 2;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(403, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(114, 49);
            this.button1.TabIndex = 0;
            this.button1.Text = "Confirm";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.confirmNewEvent_Click);
            // 
            // browse
            // 
            this.browse.BackColor = System.Drawing.Color.Transparent;
            this.browse.Cursor = System.Windows.Forms.Cursors.Hand;
            this.browse.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.browse.FlatAppearance.BorderSize = 2;
            this.browse.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.browse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.browse.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.browse.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.browse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.browse.Location = new System.Drawing.Point(21, 9);
            this.browse.Name = "browse";
            this.browse.Size = new System.Drawing.Size(88, 49);
            this.browse.TabIndex = 1;
            this.browse.Text = "Close";
            this.browse.UseVisualStyleBackColor = false;
            this.browse.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.ju7);
            this.panel3.Controls.Add(this.referee);
            this.panel3.Controls.Add(this.ju6);
            this.panel3.Controls.Add(this.ju1);
            this.panel3.Controls.Add(this.j6);
            this.panel3.Controls.Add(this.j7);
            this.panel3.Controls.Add(this.ju5);
            this.panel3.Controls.Add(this.j8);
            this.panel3.Controls.Add(this.j5);
            this.panel3.Controls.Add(this.ju8);
            this.panel3.Controls.Add(this.ju4);
            this.panel3.Controls.Add(this.j9);
            this.panel3.Controls.Add(this.j4);
            this.panel3.Controls.Add(this.ju9);
            this.panel3.Controls.Add(this.ju3);
            this.panel3.Controls.Add(this.j1);
            this.panel3.Controls.Add(this.j3);
            this.panel3.Controls.Add(this.j2);
            this.panel3.Controls.Add(this.ju2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 197);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(532, 317);
            this.panel3.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label11.Location = new System.Drawing.Point(6, 12);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(164, 28);
            this.label11.TabIndex = 92;
            this.label11.Tag = "";
            this.label11.Text = "Referee";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // referee
            // 
            this.referee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.referee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.referee.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.referee.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.referee.Location = new System.Drawing.Point(182, 15);
            this.referee.Name = "referee";
            this.referee.Size = new System.Drawing.Size(335, 25);
            this.referee.TabIndex = 0;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label14.Location = new System.Drawing.Point(87, 134);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(94, 29);
            this.label14.TabIndex = 90;
            this.label14.Tag = "";
            this.label14.Text = "Controller";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // contr
            // 
            this.contr.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.contr.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.contr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.contr.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contr.Location = new System.Drawing.Point(182, 137);
            this.contr.Name = "contr";
            this.contr.Size = new System.Drawing.Size(335, 25);
            this.contr.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label15.Dock = System.Windows.Forms.DockStyle.Left;
            this.label15.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label15.Location = new System.Drawing.Point(0, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(22, 197);
            this.label15.TabIndex = 91;
            this.label15.Tag = "";
            this.label15.Text = "TECH PANEL";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label16.Dock = System.Windows.Forms.DockStyle.Left;
            this.label16.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label16.Location = new System.Drawing.Point(0, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(22, 317);
            this.label16.TabIndex = 117;
            this.label16.Tag = "";
            this.label16.Text = "JUDGES PANEL";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Judges
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(532, 581);
            this.ControlBox = false;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Judges";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Insert officials...";
            this.Load += new System.EventHandler(this.Judges_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox dataop;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label ju6;
        private System.Windows.Forms.TextBox j6;
        private System.Windows.Forms.Label ju5;
        private System.Windows.Forms.TextBox j5;
        private System.Windows.Forms.Label ju4;
        private System.Windows.Forms.TextBox j4;
        private System.Windows.Forms.Label ju3;
        private System.Windows.Forms.TextBox j3;
        private System.Windows.Forms.Label ju2;
        private System.Windows.Forms.TextBox j2;
        private System.Windows.Forms.Label ju1;
        private System.Windows.Forms.TextBox j1;
        private System.Windows.Forms.Label ju9;
        private System.Windows.Forms.TextBox j9;
        private System.Windows.Forms.Label ju8;
        private System.Windows.Forms.TextBox j8;
        private System.Windows.Forms.TextBox j7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox ass;
        private System.Windows.Forms.Label ju7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button browse;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox referee;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox contr;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
    }
}